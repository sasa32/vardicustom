<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/rli_link_workflow.php

$dictionary['Task']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sasa_puntos_de_ventas_tasks_1_Tasks.php

// created: 2020-06-19 14:27:46
$dictionary["Task"]["fields"]["sasa_puntos_de_ventas_tasks_1"] = array (
  'name' => 'sasa_puntos_de_ventas_tasks_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_tasks_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'side' => 'right',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["sasa_puntos_de_ventas_tasks_1_name"] = array (
  'name' => 'sasa_puntos_de_ventas_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_tasks_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida"] = array (
  'name' => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_tasks_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_name.php

 // created: 2020-09-11 15:02:03
$dictionary['Task']['fields']['name']['audited']=true;
$dictionary['Task']['fields']['name']['massupdate']=false;
$dictionary['Task']['fields']['name']['hidemassupdate']=false;
$dictionary['Task']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['name']['merge_filter']='disabled';
$dictionary['Task']['fields']['name']['unified_search']=false;
$dictionary['Task']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.45',
  'searchable' => true,
);
$dictionary['Task']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2020-09-11 15:06:22
$dictionary['Task']['fields']['date_modified']['audited']=true;
$dictionary['Task']['fields']['date_modified']['hidemassupdate']=false;
$dictionary['Task']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Task']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_modified']['unified_search']=false;
$dictionary['Task']['fields']['date_modified']['calculated']=false;
$dictionary['Task']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_description.php

 // created: 2020-09-11 15:09:39
$dictionary['Task']['fields']['description']['audited']=true;
$dictionary['Task']['fields']['description']['massupdate']=false;
$dictionary['Task']['fields']['description']['hidemassupdate']=false;
$dictionary['Task']['fields']['description']['comments']='Full text of the note';
$dictionary['Task']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['description']['merge_filter']='disabled';
$dictionary['Task']['fields']['description']['unified_search']=false;
$dictionary['Task']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.56',
  'searchable' => true,
);
$dictionary['Task']['fields']['description']['calculated']=false;
$dictionary['Task']['fields']['description']['rows']='6';
$dictionary['Task']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_date_start.php

 // created: 2020-09-11 15:14:46
$dictionary['Task']['fields']['date_start']['audited']=true;
$dictionary['Task']['fields']['date_start']['massupdate']=true;
$dictionary['Task']['fields']['date_start']['hidemassupdate']=false;
$dictionary['Task']['fields']['date_start']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_start']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['date_start']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_start']['unified_search']=false;
$dictionary['Task']['fields']['date_start']['calculated']=false;
$dictionary['Task']['fields']['date_start']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_priority.php

 // created: 2020-09-11 15:22:07
$dictionary['Task']['fields']['priority']['default']='High';
$dictionary['Task']['fields']['priority']['required']=false;
$dictionary['Task']['fields']['priority']['audited']=false;
$dictionary['Task']['fields']['priority']['massupdate']=true;
$dictionary['Task']['fields']['priority']['hidemassupdate']=false;
$dictionary['Task']['fields']['priority']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['priority']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['priority']['merge_filter']='disabled';
$dictionary['Task']['fields']['priority']['unified_search']=false;
$dictionary['Task']['fields']['priority']['calculated']=false;
$dictionary['Task']['fields']['priority']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_date_due.php

 // created: 2020-09-11 17:07:55
$dictionary['Task']['fields']['date_due']['audited']=true;
$dictionary['Task']['fields']['date_due']['massupdate']=true;
$dictionary['Task']['fields']['date_due']['hidemassupdate']=false;
$dictionary['Task']['fields']['date_due']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_due']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['date_due']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_due']['unified_search']=false;
$dictionary['Task']['fields']['date_due']['calculated']=false;
$dictionary['Task']['fields']['date_due']['enable_range_search']='1';
$dictionary['Task']['fields']['date_due']['full_text_search']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_parent_type.php

 // created: 2020-09-11 17:11:58
$dictionary['Task']['fields']['parent_type']['audited']=false;
$dictionary['Task']['fields']['parent_type']['massupdate']=false;
$dictionary['Task']['fields']['parent_type']['hidemassupdate']=false;
$dictionary['Task']['fields']['parent_type']['comments']='';
$dictionary['Task']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['parent_type']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Task']['fields']['parent_type']['unified_search']=false;
$dictionary['Task']['fields']['parent_type']['calculated']=false;
$dictionary['Task']['fields']['parent_type']['len']=255;
$dictionary['Task']['fields']['parent_type']['options']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_parent_name.php

 // created: 2020-09-11 17:11:58
$dictionary['Task']['fields']['parent_name']['audited']=false;
$dictionary['Task']['fields']['parent_name']['massupdate']=false;
$dictionary['Task']['fields']['parent_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['parent_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['parent_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['parent_name']['unified_search']=false;
$dictionary['Task']['fields']['parent_name']['calculated']=false;
$dictionary['Task']['fields']['parent_name']['related_fields']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_parent_id.php

 // created: 2020-09-11 17:11:58
$dictionary['Task']['fields']['parent_id']['audited']=false;
$dictionary['Task']['fields']['parent_id']['massupdate']=false;
$dictionary['Task']['fields']['parent_id']['hidemassupdate']=false;
$dictionary['Task']['fields']['parent_id']['comments']='';
$dictionary['Task']['fields']['parent_id']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['parent_id']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['Task']['fields']['parent_id']['unified_search']=false;
$dictionary['Task']['fields']['parent_id']['calculated']=false;
$dictionary['Task']['fields']['parent_id']['len']=255;
$dictionary['Task']['fields']['parent_id']['reportable']=true;
$dictionary['Task']['fields']['parent_id']['group']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_status.php

 // created: 2020-09-14 16:04:39
$dictionary['Task']['fields']['status']['audited']=true;
$dictionary['Task']['fields']['status']['massupdate']=true;
$dictionary['Task']['fields']['status']['hidemassupdate']=false;
$dictionary['Task']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['status']['merge_filter']='disabled';
$dictionary['Task']['fields']['status']['unified_search']=false;
$dictionary['Task']['fields']['status']['calculated']=false;
$dictionary['Task']['fields']['status']['dependency']=false;
$dictionary['Task']['fields']['status']['full_text_search']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sasa_habeas_data_tasks_1_Tasks.php

// created: 2020-11-03 19:25:16
$dictionary["Task"]["fields"]["sasa_habeas_data_tasks_1"] = array (
  'name' => 'sasa_habeas_data_tasks_1',
  'type' => 'link',
  'relationship' => 'sasa_habeas_data_tasks_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'side' => 'right',
  'vname' => 'LBL_SASA_HABEAS_DATA_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["sasa_habeas_data_tasks_1_name"] = array (
  'name' => 'sasa_habeas_data_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_HABEAS_DATA_TASKS_1_FROM_SASA_HABEAS_DATA_TITLE',
  'save' => true,
  'id_name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'link' => 'sasa_habeas_data_tasks_1',
  'table' => 'sasa_habeas_data',
  'module' => 'SASA_Habeas_Data',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["sasa_habeas_data_tasks_1sasa_habeas_data_ida"] = array (
  'name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_HABEAS_DATA_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'link' => 'sasa_habeas_data_tasks_1',
  'table' => 'sasa_habeas_data',
  'module' => 'SASA_Habeas_Data',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_tipo_de_tarea_c.php

 // created: 2021-11-16 21:30:54
$dictionary['Task']['fields']['sasa_tipo_de_tarea_c']['labelValue']='Tipo de tarea';
$dictionary['Task']['fields']['sasa_tipo_de_tarea_c']['dependency']='';
$dictionary['Task']['fields']['sasa_tipo_de_tarea_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_respuesta_c.php

 // created: 2021-11-16 21:30:55
$dictionary['Task']['fields']['sasa_respuesta_c']['labelValue']='Respuesta';
$dictionary['Task']['fields']['sasa_respuesta_c']['dependency']='equal($sasa_tipo_de_tarea_c,"1")';
$dictionary['Task']['fields']['sasa_respuesta_c']['visibility_grid']='';

 
?>

<?php
// created: 2023-02-08 11:06:10
$viewdefs['Tasks']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        1 => 'date_start',
        2 => 'date_due',
        3 => 'status',
        4 => 'assigned_user_name',
        5 => 'description',
        6 => 
        array (
          'name' => 'sasa_tipo_de_tarea_c',
          'label' => 'LBL_SASA_TIPO_DE_TAREA_C',
        ),
        7 => 
        array (
          'name' => 'sasa_respuesta_c',
          'label' => 'LBL_SASA_RESPUESTA_C',
        ),
        8 => 'parent_name',
        9 => 
        array (
          'name' => 'sasa_puntos_de_ventas_tasks_1_name',
          'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
        ),
        10 => 
        array (
          'name' => 'created_by_name',
          'readonly' => true,
          'label' => 'LBL_CREATED',
        ),
        11 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        12 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
        13 => 
        array (
          'name' => 'modified_by_name',
          'readonly' => true,
          'label' => 'LBL_MODIFIED',
        ),
        14 => 'team_name',
      ),
    ),
  ),
);
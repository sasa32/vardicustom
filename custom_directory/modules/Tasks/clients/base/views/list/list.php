<?php
// created: 2023-02-08 11:06:10
$viewdefs['Tasks']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'link' => true,
          'label' => 'LBL_LIST_SUBJECT',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'status',
          'label' => 'LBL_LIST_STATUS',
          'link' => false,
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'parent_name',
          'label' => 'LBL_LIST_RELATED_TO',
          'dynamic_module' => 'PARENT_TYPE',
          'id' => 'PARENT_ID',
          'link' => true,
          'enabled' => true,
          'default' => true,
          'sortable' => false,
          'ACLTag' => 'PARENT',
          'related_fields' => 
          array (
            0 => 'parent_id',
            1 => 'parent_type',
          ),
        ),
        3 => 
        array (
          'name' => 'date_start',
          'label' => 'LBL_LIST_START_DATE',
          'css_class' => 'overflow-visible',
          'link' => false,
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'date_due',
          'label' => 'LBL_LIST_DUE_DATE',
          'type' => 'datetimecombo-colorcoded',
          'css_class' => 'overflow-visible',
          'completed_status_value' => 'Completed',
          'link' => false,
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
          'id' => 'ASSIGNED_USER_ID',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'date_modified',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
        ),
        8 => 
        array (
          'name' => 'contact_name',
          'label' => 'LBL_LIST_CONTACT',
          'link' => true,
          'id' => 'CONTACT_ID',
          'module' => 'Contacts',
          'enabled' => true,
          'default' => false,
          'ACLTag' => 'CONTACT',
          'related_fields' => 
          array (
            0 => 'contact_id',
          ),
        ),
        9 => 
        array (
          'name' => 'sasa_tipo_de_tarea_c',
          'label' => 'LBL_SASA_TIPO_DE_TAREA_C',
          'enabled' => true,
          'default' => false,
        ),
        10 => 
        array (
          'name' => 'sasa_respuesta_c',
          'label' => 'LBL_SASA_RESPUESTA_C',
          'enabled' => true,
          'default' => false,
        ),
        11 => 
        array (
          'name' => 'sasa_puntos_de_ventas_tasks_1_name',
          'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
          'enabled' => true,
          'id' => 'SASA_PUNTOS_DE_VENTAS_TASKS_1SASA_PUNTOS_DE_VENTAS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => false,
        ),
        12 => 
        array (
          'name' => 'created_by_name',
          'label' => 'LBL_CREATED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'CREATED_BY',
          'link' => true,
          'default' => false,
        ),
      ),
    ),
  ),
);
<?php
// created: 2020-09-14 16:29:29
$viewdefs['Tasks']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'parent_name' => 
    array (
    ),
    'sasa_puntos_de_ventas_tasks_1_name' => 
    array (
    ),
    'status' => 
    array (
    ),
    'date_start' => 
    array (
    ),
    'date_due' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);
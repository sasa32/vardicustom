<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_TasksAftersave
{
	function after_save($bean, $event, $arguments)
	{	
		/*
		Tarea 17435 https://sasaconsultoria.sugarondemand.com/index.php#Tasks/34a87d98-46fb-11ec-b137-02dfd714a754
		*/
		try{

			if (!isset($bean->logic_hooks_tasks_after_save_ignore_update) || $bean->logic_hooks_tasks_after_save_ignore_update === false){//antiloop
				$bean->logic_hooks_tasks_after_save_ignore_update = true;//antiloop

				global $db;

				//Desarrollo para contar el número de gestiones que se le hicieron al lead/Caso.
				if ($bean->parent_type=="Leads" || $bean->parent_type=="Cases") {
					$Modulo = strtolower($bean->parent_type);
					$querycountTasks = "SELECT COUNT(*) AS conteo FROM(SELECT calls.id AS conteo FROM calls INNER JOIN $Modulo ON calls.parent_id={$Modulo}.id WHERE calls.deleted=0 AND {$Modulo}.deleted=0 AND calls.parent_id='{$bean->parent_id}' AND calls.status='Held' UNION SELECT tasks.id AS conteo FROM tasks INNER JOIN $Modulo ON tasks.parent_id={$Modulo}.id WHERE tasks.deleted=0 AND {$Modulo}.deleted=0 AND tasks.parent_id='{$bean->parent_id}' AND tasks.status='Completed')tconteo";
					
					$resultquery = $db->query($querycountTasks);
					$conteoquery = $GLOBALS['db']->fetchByAssoc($resultquery);
					$RecordRelated = BeanFactory::retrieveBean($bean->parent_type, $bean->parent_id, array('disable_row_level_security' => true));
					$RecordRelated->sasa_numerogestiones_c = $conteoquery['conteo'];
					$RecordRelated->save();
				}

			}

			
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Notas: ".$e->getMessage()); 
		}
	}
}
?>
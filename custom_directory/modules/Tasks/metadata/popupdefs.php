<?php
$popupMeta = array (
    'moduleMain' => 'Tasks',
    'varName' => 'Tasks',
    'orderBy' => 'tasks.name',
    'whereClauses' => array (
  'name' => 'tasks.name',
  'contact_name' => 'tasks.contact_name',
  'current_user_only' => 'tasks.current_user_only',
  'status' => 'tasks.status',
  'parent_name' => 'tasks.parent_name',
  'date_start' => 'tasks.date_start',
  'date_due' => 'tasks.date_due',
  'assigned_user_name' => 'tasks.assigned_user_name',
  'date_entered' => 'tasks.date_entered',
  'created_by_name' => 'tasks.created_by_name',
  0 => 'tasks.0',
),
    'searchInputs' => array (
  1 => 'name',
  3 => 'status',
  4 => 'contact_name',
  5 => 'current_user_only',
  6 => 'parent_name',
  7 => 'date_start',
  8 => 'date_due',
  9 => 'assigned_user_name',
  10 => 'date_entered',
  11 => 'created_by_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'contact_name' => 
  array (
    'name' => 'contact_name',
    'label' => 'LBL_CONTACT_NAME',
    'type' => 'name',
    'width' => '10',
  ),
  'current_user_only' => 
  array (
    'name' => 'current_user_only',
    'label' => 'LBL_CURRENT_USER_FILTER',
    'type' => 'bool',
    'width' => '10',
  ),
  'status' => 
  array (
    'name' => 'status',
    'width' => '10',
  ),
  'parent_name' => 
  array (
    'type' => 'parent',
    'label' => 'LBL_LIST_RELATED_TO',
    'width' => '10',
    'name' => 'parent_name',
  ),
  'date_start' => 
  array (
    'type' => 'datetimecombo',
    'studio' => 
    array (
      'required' => true,
      'no_duplicate' => true,
    ),
    'label' => 'LBL_START_DATE',
    'width' => 10,
    'name' => 'date_start',
  ),
  'date_due' => 
  array (
    'type' => 'datetimecombo',
    'studio' => 
    array (
      'required' => true,
      'no_duplicate' => true,
    ),
    'label' => 'LBL_DUE_DATE',
    'width' => 10,
    'name' => 'date_due',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'related_fields' => 
    array (
      0 => 'assigned_user_id',
    ),
    'label' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'name' => 'assigned_user_name',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'name' => 'date_entered',
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'readonly' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => 10,
    'name' => 'created_by_name',
  ),
  0 => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => 10,
  ),
),
);

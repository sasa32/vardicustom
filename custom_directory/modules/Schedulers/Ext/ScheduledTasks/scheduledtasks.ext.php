<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_calcula_edad_contact.php

$job_strings[] = 'tarea_calcula_edad_contact';
function tarea_calcula_edad_contact(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_calcula_edad_contact. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		global $db;			
		/*
		* https://sasaconsultoria.sugarondemand.com/#Tasks/ea1b09b2-9c42-11ea-a89d-02dfd714a754
		*/
		$query = "
		UPDATE
		    contacts_cstm
		LEFT JOIN contacts ON id = id_c
		SET
		    sasa_edad_c = TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) 
		";
		$result = $db->query($query);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
		}
		else{
			$row = $db->fetchByAssoc($result);
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_calcula_edad_contact. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_sendsigruerror.php

/*
Para dar satisfacción a la propuesta de adjuntos https://docs.google.com/document/d/1wusr7EdzZcbNCEl6g2ipYG0olQfuKkhMjCY0BXNJiTo/edit se requiere realizar la tarea programada que eliminé los adjuntos cuando ya tengamos la URL del repositorio, la frecuencia será todos los domingos.

*/
//use Sugarcrm\Sugarcrm\ProcessManager\Registry;
$job_strings[] = 'tarea_sendsigruerror';
function tarea_sendsigruerror(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_sendsigruerror. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return = true;
	try{
		//Esta tarea programada resuleve el problema que tienen los servicios de SIGRU cuando retornan un error y se debe se intentar nuevamente en 5 minutos.
		global $db;
		//Query para identificar los adjutnos que se pueden eliminar
		$querydata = "SELECT * FROM log_error_send_sigru";

		$result = $db->query($querydata);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$querydata}"); 
		}else{
			//Recorrer las notas que tienen adjuntos
			while($row = $db->fetchByAssoc($result)) {
				$module = strtolower($row['module']);
				$querystatusrecord = "SELECT date_modified FROM {$module}";
				$resultstatusrecord = $db->query($querystatusrecord);
				$record = $db->fetchByAssoc($resultstatusrecord);
				//Ver si el registro ya tuvo una peticion luego del reporte de error
				$fechaerror = strtotime($row['fecha']);
				$datemodifiedrecord = strtotime($record['date_modified']);
				//Si la fecha de error es mayor a la de modificacion del registro, quiere decir que el registro no tuvo envios exitosos luego del reporte de error
				if ($fechaerror >= $datemodifiedrecord) {
					//Continuar y enviar nuevamente la petición a sigru
					require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
					$sendsigru = new Send_Data_Sigru();
					$responseSIGRU = $sendsigru->data($row['record'],$row['module']);
					//Si retorno nuevamente el error dejar el registro y esperar a que la tarea programada lo vuleva a ejecutar en caso contrario eleminar el registro de la tabla log_error_send_sigru
					if ($responseSIGRU['status']=='200') {
						$GLOBALS['log']->security("Registro enviado luego del error: ".$row['record']." Modulo: ".$row['module']);
						//Query para elimianr el registro de la tabla log_error_send_sigru
						$db->query("DELETE FROM log_error_send_sigru WHERE record='{$row['record']}'");
					}else{
						$GLOBALS['log']->security("Registro enviado Sigue retornando error del servicio: ".$row['record']." Modulo: ".$row['module']);
					}
				}else{
					//Eliminar el registro ya tuvo una modificación y envio a sigru luego del reporte de error
					$db->query("DELETE FROM log_error_send_sigru WHERE record='{$row['record']}'");
				}
			}
		}

	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage());
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_sendsigruerror. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/TareaConversionLeads.php


$job_strings[] = 'TareaConversionLeads';
function TareaConversionLeads()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaConversionLeads. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;
    //Query
    $QueryNumId = "SELECT leads.id AS IdLead, accounts.id AS IdAccount, leads.date_entered CreacionLead, accounts.date_entered CreacionAccount FROM leads_cstm INNER JOIN accounts_cstm ON leads_cstm.sasa_numero_documento_c=accounts_cstm.sasa_numero_documento_c INNER JOIN leads ON leads.id=leads_cstm.id_c INNER JOIN accounts ON accounts_cstm.id_c=accounts.id WHERE leads.converted=0 AND leads_cstm.sasa_numero_documento_c IS NOT NULL AND leads.deleted=0 AND accounts.deleted=0";

    $result = $db->query($QueryNumId);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$QueryNumId}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            //$GLOBALS['log']->security("Lead: {$row['IdLead']} Account {$row['IdAccount']}");
            $Lead = BeanFactory::getBean("Leads", $row['IdLead'], array('disable_row_level_security' => true)); 
            $Account = BeanFactory::getBean("Accounts", $row['IdAccount'], array('disable_row_level_security' => true));
            /*$Lead->status = 'D';
            $Lead->converted = true;
            $Lead->account_id = $Account->id;
            $Lead->account_name = $Account->name;
            $Lead->save();*/
            $fecha_lead = strtotime($row['CreacionLead']);
            $fecha_account = strtotime($row['CreacionAccount']);

            if ($fecha_lead > $fecha_account) {
                $result1 = $db->query("UPDATE leads SET status='D', converted=1, account_id='{$Account->id}', account_name='{$Account->name}' WHERE id='{$Lead->id}'");
            }else{
                $result1 = $db->query("UPDATE leads SET status='I', converted=1, account_id='{$Account->id}', account_name='{$Account->name}' WHERE id='{$Lead->id}'");
            }

            
        }
    }

    
    ConversionEmails();
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaConversionLeads. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}

function ConversionEmails(){
    global $db;
    $querysecundario = "SELECT accounts.id IDACCOUNT, t1.IDLEAD, accounts.date_entered CreacionAccount, t1.CreacionLead FROM accounts INNER JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id=accounts.id AND email_addr_bean_rel.deleted=0 INNER JOIN (SELECT email_addr_bean_rel.email_address_id EMAILLEAD, leads.id IDLEAD, leads.first_name NOMBRE, leads.last_name APELLIDO, leads.date_entered CreacionLead FROM leads INNER JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id=leads.id AND email_addr_bean_rel.deleted=0 WHERE leads.converted=0 AND email_addr_bean_rel.primary_address=1)t1 ON t1.EMAILLEAD=email_addr_bean_rel.email_address_id WHERE accounts.deleted=0 AND email_addr_bean_rel.primary_address=1 AND CONCAT(t1.NOMBRE,' ',t1.APELLIDO)=accounts.name LIMIT 10000";
    $resultemail = $db->query($querysecundario);
    while ($row2 = $db->fetchByAssoc($resultemail)) {
        //$GLOBALS['log']->security("EMAILSLead: {$row2['IDLEAD']} Account {$row2['IDACCOUNT']}");
        $Lead = BeanFactory::getBean("Leads", $row2['IDLEAD'], array('disable_row_level_security' => true)); 
        $Account = BeanFactory::getBean("Accounts", $row2['IDACCOUNT'], array('disable_row_level_security' => true));
        /*$Lead->status = 'D';
        $Lead->converted = true;
        $Lead->account_id = $Account->id;
        $Lead->account_name = $Account->name;
        $Lead->save();*/
        $fecha_lead = strtotime($row2['CreacionLead']);
        $fecha_account = strtotime($row2['CreacionAccount']);

        if ($fecha_lead > $fecha_account) {
            $result3 = $db->query("UPDATE leads SET status='D', converted=1, account_id='{$Account->id}', account_name='{$Account->name}' WHERE id='{$Lead->id}'");
        }else{
            $result3 = $db->query("UPDATE leads SET status='I', converted=1, account_id='{$Account->id}', account_name='{$Account->name}' WHERE id='{$Lead->id}'");
        }
    }
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_mantenimiento_db_tablas.php

/*
* Funcion para planificador de tareas
* Ejecuta una seria de queries
*/
$job_strings[] = 'tarea_mantenimiento_db_tablas';

function tarea_mantenimiento_db_tablas(){
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_mantenimiento_db_tablas. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$queries = array();	
		$temp_file_location="custom/tarea_mantenimiento_db_tablas.txt";
		
		/*
		* Generando queries de limpieza staticos
		*/
		$queries = tarea_mantenimiento_db_procesar_archivo_sql_deleted($temp_file_location);
				
		/*
		* Procesando queries
		*/		
		tarea_mantenimiento_db_procesar_querys_deleted($queries);

	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_mantenimiento_db_tablas. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}

/*
* Lee un archivo *.sql, luego convierte cada query separando por ";" en un array, y ejecuta cada uno de forma individual.
*/
function tarea_mantenimiento_db_procesar_archivo_sql_deleted($temp_file_location = ""){
	$queries = array();			
	$GLOBALS['log']->security( "Cargando archivo {$temp_file_location}");
	try {
		$uploadFile = new UploadFile();
		$uploadFile->temp_file_location = $temp_file_location;
		$fieldsmetadata = explode("\n", $uploadFile->get_file_contents());
		$queries = $fieldsmetadata;

		foreach ($queries as $key => $value) {
			$GLOBALS['log']->security( "Sql: LLave: {$key} Sql: {$value}");
		}
	} 
	catch (Exception $e) {
	    	$GLOBALS['log']->security("ERROR: fallo la carga del archivo '{$temp_file_location}' ".$e->getMessage()); 
	}	
	return $queries;	
}

/*
* Ejecuta una serie de querys en un array
*/
function tarea_mantenimiento_db_procesar_querys_deleted($queries=array()){		
	try {
		$return_queries=array();
		if(!empty($queries)){
			$count=0;
			foreach ($queries as $k=> $query) {
				$rows = array();
				$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
				if(!empty(trim($query))){
					global $db;		
					$count=$count+1;	
					$getAffectedRowCount=0;
					$getRowCount=0;
					$result = $db->query($query);	
										
					if(is_bool($result) !== true){
						$lastDbError = $db->lastDbError($result);				
						if(!empty($lastDbError)){
							$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
						}
						else{													
							while($row = $db->fetchRow($result)){
								$rows[] = $row;				
							}				
							$getAffectedRowCount = "Filas Afectadas: " . print_r( $db->getAffectedRowCount($result) ,true) . " " . print_r( $db->affected_rows ,true);
							$getRowCount = "Registros Seleccionados: " . print_r( $db->getRowCount($result) ,true) . " " . print_r($db->select_rows,true);																		
						}
					}										
					$GLOBALS['log']->security("\n\nresultado query ({$k}): {$getAffectedRowCount}, {$getRowCount} "); 
				}
				$return_queries	= array_merge($return_queries,$rows);
			}
			$GLOBALS['log']->security("\n\nQueries ejecutados: $count\n\n");
		}
		return $return_queries;
	} 
	catch (Exception $e) {
	    	$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/TempTareaURLHD.php


$job_strings[] = 'TempTareaURLHD';
function TempTareaURLHD()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TempTareaURLHD. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;

    //Query para identificar los HD que tienen un token pero aun no lo tienen en el campo url del registro, esto es porque los reenvios no mapeaban este valor
    $queryInicial = "SELECT sasa_habeas_data_cstm.id_c, sasa_habeas_data_cstm.sasa_soporte_autorizacion_c,log_send_sigru.record, log_send_sigru.jsonsent, log_send_sigru.responsesigru from sasa_habeas_data_cstm INNER JOIN log_send_sigru ON sasa_habeas_data_cstm.id_c=log_send_sigru.record WHERE sasa_habeas_data_cstm.sasa_soporte_autorizacion_c='http://templatephp71.sugarcrmcolombia.com/visualizador?nota=' AND log_send_sigru.status='200' AND log_send_sigru.responsesigru !='{\"codigo\":\"-2\",\"mensaje\":\"ERROR: null\"} ' GROUP BY sasa_habeas_data_cstm.id_c";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $jsonresponse = json_decode($row['responsesigru'],true);
            if ($jsonresponse['token']!=null) {
                $urlToken = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$jsonresponse['token'];
                $db->query("UPDATE sasa_habeas_data_cstm SET sasa_soporte_autorizacion_c='{$urlToken}' WHERE id_c='{$row['id_c']}'");
            }else{
                
            }
            
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TempTareaURLHD. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/TareaPatchGestincomercial.php


$job_strings[] = 'TareaPatchGestincomercial';
function TareaPatchGestincomercial()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaPatchGestincomercial. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;

    //Query para identificar los leads que no tienen fecha fin poblado y si lo deben tener
    $queryInicialFachaFin = "SELECT leads_cstm.id_c, leads_audit.date_created FROM leads_cstm INNER JOIN leads_audit ON leads_cstm.id_c=leads_audit.parent_id WHERE (leads_cstm.sasa_fechafingestionld_c ='' OR leads_cstm.sasa_fechafingestionld_c IS NULL) AND leads_audit.field_name='status' AND (leads_audit.after_value_string='C' OR leads_audit.after_value_string='G' OR leads_audit.after_value_string='F' OR leads_audit.after_value_string='J') AND leads_audit.date_created > '2021-12-05 00:00:00'";

    $resultfechafin = $db->query($queryInicialFachaFin);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicialFachaFin}"); 
    }else{
        while ($row = $db->fetchByAssoc($resultfechafin)) {
            $db->query("UPDATE leads_cstm SET sasa_fechafingestionld_c='{$row['date_created']}' WHERE id_c='{$row['id_c']}'");
        }
    }

    //Query para identificar los leads que no tienen fecha inicio poblado y si lo deben tener
    $queryFechaInicio = "SELECT leads_cstm.id_c, calls_audit.date_created FROM leads_cstm INNER JOIN calls ON leads_cstm.id_c=calls.parent_id INNER JOIN calls_cstm ON calls.id=calls_cstm.id_c INNER JOIN calls_audit ON calls.id=calls_audit.parent_id WHERE (leads_cstm.sasa_fechainiciogestionld_c='' OR leads_cstm.sasa_fechainiciogestionld_c IS NULL) AND (calls_cstm.sasa_controlproceso_c='1' OR calls_cstm.sasa_controlproceso_c='4' OR calls_cstm.sasa_controlproceso_c='7') AND calls_audit.field_name='status' AND calls_audit.after_value_string!='Planned' AND calls.status!='Planned' AND calls.date_entered > '2021-12-05 00:00:00'";

    $resultfechainicio = $db->query($queryFechaInicio);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicialFachaFin}"); 
    }else{
        while ($row = $db->fetchByAssoc($resultfechainicio)) {
            $db->query("UPDATE leads_cstm SET sasa_fechainiciogestionld_c='{$row['date_created']}' WHERE id_c='{$row['id_c']}'");
        }
    }

    //Query para identificar los casos que no tienen fecha fin poblado y si lo deben tener
    $queryInicialFachaFinCases = "SELECT cases_cstm.id_c, cases_audit.date_created FROM cases_cstm INNER JOIN cases_audit ON cases_cstm.id_c=cases_audit.parent_id WHERE (cases_cstm.sasa_fechafingestioncs_c ='' OR cases_cstm.sasa_fechafingestioncs_c IS NULL) AND cases_audit.field_name='status' AND (cases_audit.after_value_string='Rejected' OR cases_audit.after_value_string='Closed') AND (cases_cstm.sasa_motivo_c='74' OR cases_cstm.sasa_motivo_c='73' OR cases_cstm.sasa_motivo_c='71' OR cases_cstm.sasa_motivo_c='68' OR cases_cstm.sasa_motivo_c='204') AND cases_audit.date_created > '2021-12-05 00:00:00'";

    $resultfechafincases = $db->query($queryInicialFachaFinCases);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicialFachaFin}"); 
    }else{
        while ($row = $db->fetchByAssoc($resultfechafincases)) {
            $db->query("UPDATE cases_cstm SET sasa_fechafingestioncs_c='{$row['date_created']}' WHERE id_c='{$row['id_c']}'");
        }
    }

    //Query para identificar los casos que no tienen fecha Inicio poblado y si lo deben tener
    $queryInicialFachaInicioCases = "SELECT cases_cstm.id_c, calls_audit.date_created FROM cases_cstm INNER JOIN calls ON cases_cstm.id_c=calls.parent_id INNER JOIN calls_cstm ON calls.id=calls_cstm.id_c INNER JOIN calls_audit ON calls.id=calls_audit.parent_id WHERE (cases_cstm.sasa_fechainiciogestioncs_c='' OR cases_cstm.sasa_fechainiciogestioncs_c IS NULL) AND (calls_cstm.sasa_controlproceso_c='1' OR calls_cstm.sasa_controlproceso_c='4' OR calls_cstm.sasa_controlproceso_c='7') AND (cases_cstm.sasa_motivo_c='74' OR cases_cstm.sasa_motivo_c='73' OR cases_cstm.sasa_motivo_c='71' OR cases_cstm.sasa_motivo_c='68' OR cases_cstm.sasa_motivo_c='204') AND calls_audit.field_name='status' AND calls_audit.after_value_string!='Planned' AND calls.status!='Planned' AND calls.date_entered > '2021-12-05 00:00:00'";

    $resultfechainiciocases = $db->query($queryInicialFachaInicioCases);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicialFachaFin}"); 
    }else{
        while ($row = $db->fetchByAssoc($resultfechainiciocases)) {
            $db->query("UPDATE cases_cstm SET sasa_fechainiciogestioncs_c='{$row['date_created']}' WHERE id_c='{$row['id_c']}'");
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaPatchGestincomercial. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/TempTareaEnviaHDcola.php


$job_strings[] = 'TempTareaEnviaHDcola';
function TempTareaEnviaHDcola()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TempTareaEnviaHDcola. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;
    //Query para identificar los HD que no se han enviado y que aún tienen un adjunto.
    $queryInicial = "SELECT IFNULL(sasa_habeas_data.id,'') primaryid ,IFNULL(sasa_habeas_data.name,'') sasa_habeas_data_name ,sasa_habeas_data_cstm.sasa_adjuntohabeasdata_c SASA_HABEAS_DATA_CSTM_165A8E,sasa_habeas_data_cstm.sasa_soporte_autorizacion_c SASA_HABEAS_DATA_CSTM_1521AF FROM sasa_habeas_data LEFT JOIN sasa_habeas_data_cstm sasa_habeas_data_cstm ON sasa_habeas_data.id = sasa_habeas_data_cstm.id_c WHERE ((((coalesce(LENGTH(sasa_habeas_data_cstm.sasa_adjuntohabeasdata_c), 0) <> 0)))) AND sasa_habeas_data.deleted=0 LIMIT 5000";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $Record = BeanFactory::getBean('SASA_Habeas_Data', $row['primaryid'], array('disable_row_level_security' => true));
            
            if (!empty($Record->id)) {
                //$Record->save();
                //Identificar el modulo para construir el array
                //Continuar y enviar nuevamente la petición a sigru
                require_once("custom/modules/integration_sigru/Send_Data_Sigru_Habeas.php");
                $sendsigru = new Send_Data_Sigru_Habeas();
                $repuesta = $sendsigru->data($row['primaryid'],"SASA_Habeas_Data","POST");
                $repuesta = json_decode($repuesta['Response'],true);
                if ($repuesta['token']!= null) {
                    $urlToken = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$repuesta['token'];
                    $db->query("UPDATE sasa_habeas_data_cstm SET sasa_soporte_autorizacion_c='{$urlToken}' WHERE id_c='{$row['primaryid']}'");
                }
            }
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TempTareaEnviaHDcola. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/Temp_tarea_huerfanos.php


$job_strings[] = 'Temp_tarea_huerfanos';
function Temp_tarea_huerfanos()
{
    $GLOBALS['log']->security("\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Temp_tarea_huerfanos. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    global $db;

    $queryInicial = "SELECT sasa_habeas_data.id idHD FROM sasa_habeas_data LEFT JOIN leads_sasa_habeas_data_1_c l1_1 ON sasa_habeas_data.id=l1_1.leads_sasa_habeas_data_1sasa_habeas_data_idb AND l1_1.deleted=0 LEFT JOIN leads l1 ON l1.id=l1_1.leads_sasa_habeas_data_1leads_ida AND l1.deleted=0 LEFT JOIN contacts_sasa_habeas_data_1_c l2_1 ON sasa_habeas_data.id=l2_1.contacts_sasa_habeas_data_1sasa_habeas_data_idb AND l2_1.deleted=0 LEFT JOIN contacts l2 ON l2.id=l2_1.contacts_sasa_habeas_data_1contacts_ida AND l2.deleted=0 LEFT JOIN users l3 ON sasa_habeas_data.created_by=l3.id AND l3.deleted=0 WHERE ((((coalesce(LENGTH(l1.id), 0) = 0)) AND ((coalesce(LENGTH(l2.id), 0) = 0)))) AND sasa_habeas_data.deleted=0";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        //Recorrer los HD huerfanos
        while($row = $db->fetchByAssoc($result)) {
            $HD = BeanFactory::getBean('SASA_Habeas_Data', $row['idHD'],array('disable_row_level_security' => true));

            $querygetTasksRelated= "SELECT sasa_habeas_data_tasks_1tasks_idb IDTask from sasa_habeas_data_tasks_1_c WHERE sasa_habeas_data_tasks_1sasa_habeas_data_ida='{$row['idHD']}'";
            $resultgetTasksRelated = $db->query($querygetTasksRelated);
            while ($row2 = $db->fetchByAssoc($resultgetTasksRelated)) {
                
                $Task = BeanFactory::getBean('Tasks', $row2['IDTask'],array('disable_row_level_security' => true));
                $Task->mark_deleted($row2['IDTask']);
                $Task->save();
            }
            $HD->mark_deleted($row['idHD']);
            $HD->save();

        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Temp_tarea_huerfanos. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/LeaveLast500Records.php


$job_strings[] = 'LeaveLast500Records';


function LeaveLast500Records()
{
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Leave last 500 records Account, leads, cases. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    $limitValor = 500;

    //Ejectuo la funcion para cada modulo
    //ejecutaQuerys("cases");
    //ejecutaQuerys("accounts");
    //ejecutaQuerys("leads");

    ejecutaQueryModulos("SELECT l.id AS ID_MODULOS, l.date_modified
        FROM leads l
        LEFT JOIN(SELECT l1.id AS id_C, lc.sasa_nombres_c, lc.sasa_primerapellido_c
        FROM  leads l1 
        INNER JOIN leads_cstm lc ON lc.id_c = l1.id
        WHERE l1.deleted = 0 AND (lc.sasa_nombres_c != '' OR lc.sasa_nombres_c IS NOT NULL) AND (lc.sasa_primerapellido_c != '' OR lc.sasa_primerapellido_c IS NOT NULL)
        ORDER BY l1.date_modified DESC 
        LIMIT 500) AS t1 ON t1.id_C = l.id
        WHERE t1.id_C IS NULL AND l.deleted = 0 LIMIT {$limitValor}","Leads");

    ejecutaQueryModulos("SELECT c.id AS ID_MODULOS
        FROM cases c
        LEFT JOIN (
        SELECT c1.id AS ID_C
        FROM  cases c1 
        WHERE c1.deleted = 0 AND c1.account_id != ''
        ORDER BY c1.date_modified DESC 
        LIMIT 500 ) AS tabla ON tabla.ID_C = c.id
        WHERE tabla.ID_C IS NULL AND c.deleted = 0
        LIMIT {$limitValor}","Cases");

    /*ejecutaQueryModulos("SELECT c.id AS ID_MODULOS
        FROM contacts c
        LEFT JOIN(SELECT c1.id AS id_C
        FROM  contacts c1 
        INNER JOIN accounts_contacts ac1 ON ac1.contact_id = c1.id
        INNER JOIN accounts a1 ON a1.id = ac1.account_id
        WHERE c1.deleted = 0 AND a1.deleted = 0 AND ac1.deleted = 0
        ORDER BY c1.date_modified DESC LIMIT 500) AS t1 ON t1.id_C = c.id 
        WHERE t1.id_C IS NULL AND c.deleted = 0
        ORDER BY c.date_modified
        LIMIT {$limitValor}","Contacts");

    ejecutaQueryModulos("SELECT hd1.id AS ID_MODULOS
        FROM sasa_habeas_data hd1
        LEFT JOIN (
        (SELECT hd.id AS ID_HD, hd.name, l.first_name, hd.date_modified
        FROM sasa_habeas_data hd 
        INNER JOIN leads_sasa_habeas_data_1_c lhd ON lhd.leads_sasa_habeas_data_1sasa_habeas_data_idb = hd.id
        INNER JOIN leads l ON l.id = lhd.leads_sasa_habeas_data_1leads_ida
        WHERE hd.deleted = 0 AND l.deleted = 0 AND lhd.deleted = 0 
        ORDER BY hd.date_modified DESC
        LIMIT 500)

        UNION

        (SELECT hd.id AS ID_HD, hd.name, c.first_name, hd.date_modified
        FROM sasa_habeas_data hd 
        INNER JOIN contacts_sasa_habeas_data_1_c chd ON chd.contacts_sasa_habeas_data_1sasa_habeas_data_idb = hd.id
        INNER JOIN contacts c ON c.id = chd.contacts_sasa_habeas_data_1contacts_ida
        WHERE hd.deleted = 0 AND c.deleted = 0 AND chd.deleted = 0 
        ORDER BY hd.date_modified DESC
        LIMIT 500) 
        ) AS t1 ON t1.ID_HD = hd1.id
        WHERE t1.ID_HD IS NULL AND hd1.deleted = 0
        LIMIT {$limitValor}","SASA_Habeas_Data");*/

    ejecutaQueryModulos("SELECT t1.id AS ID_MODULOS
        FROM tasks t1
        LEFT JOIN ( 
        (SELECT t.id AS ID_T, t.name, t.parent_type, t.parent_id
        FROM tasks t
        WHERE t.parent_id != '' AND t.deleted = 0
        ORDER BY t.date_modified
        LIMIT 500)

        UNION

        (SELECT t.id AS ID_T, t.name, t.parent_type, t.parent_id
        FROM tasks t 
        INNER JOIN sasa_habeas_data_tasks_1_c hdt ON hdt.sasa_habeas_data_tasks_1tasks_idb = t.id
        WHERE t.deleted = 0 AND hdt.deleted = 0
        ORDER BY t.date_modified
        LIMIT 500) ) AS tabla1 ON tabla1.ID_T = t1.id
        WHERE tabla1.ID_T IS NULL AND t1.deleted = 0
        LIMIT {$limitValor}","Tasks");

    ejecutaQueryModulos("SELECT unc1.id AS ID_MODULOS
        FROM sasa_unidadnegclienteprospect unc1
        LEFT JOIN (
        (SELECT unc.id AS ID_UNC 
        FROM sasa_unidadnegclienteprospect unc
        INNER JOIN sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c unr ON unr.sasa_unida14dfrospect_idb = unc.id
        INNER JOIN sasa_unidad_de_negocio sun ON sun.id = unr.sasa_unida3a6anegocio_ida
        WHERE unc.deleted = 0 AND unr.deleted = 0 AND sun.deleted = 0
        ORDER BY unc.date_modified
        LIMIT 500)

        UNION

        (SELECT unc.id AS ID_UNC 
        FROM sasa_unidadnegclienteprospect unc
        INNER JOIN accounts_sasa_unidadnegclienteprospect_1_c aunc ON aunc.accounts_saf31rospect_idb = unc.id
        INNER JOIN accounts a ON a.id = aunc.accounts_sasa_unidadnegclienteprospect_1accounts_ida
        WHERE unc.deleted = 0 AND aunc.deleted = 0 AND a.deleted = 0
        ORDER BY unc.date_modified
        LIMIT 500)
        ) AS tabla1 ON tabla1.ID_UNC = unc1.id
        WHERE tabla1.ID_UNC IS NULL AND unc1.deleted = 0    
        LIMIT {$limitValor}","SASA_UnidadNegClienteProspect");

 
    

    $GLOBALS['log']->security("---------------TAREA EJECUTADA-----------------------");

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Leave last 500 records Account, leads, cases ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}

function ejecutaQuerys($tabla)
{
    $cantidad = 0;

        $GLOBALS['log']->security("TABLA=>" . $tabla);
        
        $query = "SELECT a.id AS ID_MODULOS, a.date_modified
                FROM {$tabla} a
                LEFT JOIN(SELECT a1.id AS id_C FROM  {$tabla} a1 WHERE a1.deleted = 0
                    ORDER BY a1.date_modified DESC LIMIT 500 ) AS t1 ON t1.id_C = a.id
                WHERE t1.id_C IS NULL AND a.deleted = 0 LIMIT 1000";
    
        //$GLOBALS['log']->security("QUERY=>" . $query);
   
        $result = $GLOBALS['db']->query($query);

        $cant = mysqli_num_rows($result);
        //Coloco la primera letra mayuscula para usar el metodo
        $modulo = ucfirst($tabla); 

        $GLOBALS['log']->security("MODULO=>" . $modulo);

        while ($row = $GLOBALS['db']->fetchByAssoc($result)) 
        {
            // code...
            $cantidad ++;
            //$GLOBALS['log']->security("ID MODULOS=>" . $row['ID_MODULOS']);
            
            //Retrieve bean
            $bean = BeanFactory::retrieveBean($modulo, $row['ID_MODULOS']);

            //Set deleted to true
            $bean->mark_deleted($row['ID_MODULOS']);

            //Save
            $bean->save();
        }

        $GLOBALS['log']->security("CONTADOR ID MODULOS=>" . $cantidad);
}

function ejecutaQueryModulos($query, $modulo){

        $cantidad = 0;

        $GLOBALS['log']->security("modulo=>" . $modulo);
        
        //$GLOBALS['log']->security("QUERY=>" . $query);
        
        $result = $GLOBALS['db']->query($query);

        $cant = mysqli_num_rows($result);


       while ($row = $GLOBALS['db']->fetchByAssoc($result)) 
        {
            // code...
            $cantidad ++;
            
            //Retrieve bean
            $bean = BeanFactory::retrieveBean($modulo, $row['ID_MODULOS']);

            //Set deleted to true
            $bean->mark_deleted($row['ID_MODULOS']);

            //Save
            $bean->save();
        }

        $GLOBALS['log']->security("CONTADOR ID {$modulo}=>" . $cantidad);

}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/TareaReenviosHabeasData.php


$job_strings[] = 'TareaReenviosHabeasData';
function TareaReenviosHabeasData()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaReenviosHabeasData. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;
    //Query para identificar los registros con error 400, este error hacia parte de la caida del servicio por parte de sigru
    $queryInicial = "SELECT log_send_sigru.*, t1.id t1_id, t1.record t1_record, t1.fechamax, t1.module t1_module,t1.status t1_status FROM log_send_sigru INNER JOIN (select id, record, MAX(CAST(fecha AS CHAR)) AS fechamax, module, status from log_send_sigru WHERE log_send_sigru.fecha BETWEEN '2021-11-01 00:00:01' AND now() GROUP BY record)t1 ON log_send_sigru.fecha=t1.fechamax AND log_send_sigru.record=t1.record WHERE ((log_send_sigru.status='400' OR log_send_sigru.status='405' OR log_send_sigru.status IS NULL OR log_send_sigru.status = '' OR log_send_sigru.status='504' OR log_send_sigru.status='401') AND (log_send_sigru.module='SASA_Habeas_Data')) LIMIT 5000";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $Record = BeanFactory::getBean($row['module'], $row['record'], array('disable_row_level_security' => true));
            
            if (!empty($Record->id)) {
                //$Record->save();
                //Identificar el modulo para construir el array
                //Continuar y enviar nuevamente la petición a sigru
                require_once("custom/modules/integration_sigru/Send_Data_Sigru_Habeas.php");
                $sendsigru = new Send_Data_Sigru_Habeas();
                $repuesta = $sendsigru->data($row['record'],$row['module'],"POST");
                $repuesta = json_decode($repuesta['Response'],true);
                if ($repuesta['token']!= null) {
                    $urlToken = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$repuesta['token'];
                    $db->query("UPDATE sasa_habeas_data_cstm SET sasa_soporte_autorizacion_c='{$urlToken}' WHERE id_c='{$row['record']}'");
                }
            }
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaReenviosHabeasData. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/DeleteLeads.php


$job_strings[] = 'DeleteLeads';

function DeleteLeads()
{
    $GLOBALS['log']->security("\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Delete Lead Team Field.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    $arrayLeads = array(
        "203495a0-134b-11ed-a125-02b0ee514bc7",
        "cb09759e-134c-11ed-99d1-022c40d974e6",
        "917784aa-134d-11ed-bda6-0630baf9dcb8",
        "0f047558-134f-11ed-ab0c-06141b6603a5",
        "1fc6241c-1350-11ed-ae1e-022c40d974e6",
        "77c7352e-1351-11ed-95c8-06141b6603a5",
        "07016976-137f-11ed-8e44-02ae981fbade",
        "c4337d14-134c-11ed-9803-02ae981fbade",
        "8e4d9bde-134d-11ed-aebe-022c40d974e6",
        "0b64ee3c-134f-11ed-ac1b-0630baf9dcb8",
        "16dfac56-1350-11ed-8f12-02b0ee514bc7",
        "72377dc6-1351-11ed-824f-06141b6603a5",
        "1a0e18b0-1353-11ed-93eb-022c40d974e6",
        "47d7aa66-134b-11ed-a808-02b0ee514bc7",
        "03f611fa-134d-11ed-a9ab-02ae981fbade",
        "cbf25772-134d-11ed-a99e-022c40d974e6",
        "46424572-134f-11ed-8d13-02b0ee514bc7",
        "47864842-1350-11ed-b931-02ae981fbade",
        "a338b2be-1351-11ed-940e-02ae981fbade",
        "4cab48ae-134b-11ed-83ab-06141b6603a5",
        "0eeb1416-134d-11ed-a27a-06141b6603a5",
        "d50d6be4-134d-11ed-8ebc-022c40d974e6",
        "595eb032-134f-11ed-9189-06141b6603a5",
        "61989618-1350-11ed-b186-06141b6603a5",
        "bd89e110-1351-11ed-a5b2-02ae981fbade",
        "4bdb09e6-134b-11ed-b1f9-022c40d974e6",
        "07f0fd6a-134d-11ed-b358-022c40d974e6",
        "d303dd9c-134d-11ed-bfb8-02ae981fbade",
        "4f44b650-134f-11ed-bfdd-06141b6603a5",
        "5b9c0498-1350-11ed-96b4-022c40d974e6",
        "b4de5ac8-1351-11ed-9f34-02ae981fbade",
        "0584cb08-134b-11ed-9c9b-0630baf9dcb8",
        "d6fdc31e-134c-11ed-94ee-02b0ee514bc7",
        "a07d086c-134d-11ed-b6a3-02b0ee514bc7",
        "1dc1115a-134f-11ed-b63d-02ae981fbade",
        "2840b33c-1350-11ed-87b3-069e4f790264",
        "86d3d824-1351-11ed-ab8d-06141b6603a5",
        "15c8a2d0-137f-11ed-b8d9-022c40d974e6",
        "261cbbdc-134b-11ed-adbf-02b0ee514bc7",
        "c1297092-134c-11ed-b675-022c40d974e6",
        "301337e2-134b-11ed-9371-02b0ee514bc7",
        "f739204c-134c-11ed-83ef-0630baf9dcb8",
        "be29c74c-134d-11ed-8bb7-06141b6603a5",
        "357ab850-134f-11ed-a1b5-02ae981fbade",
        "46ba2172-1350-11ed-977b-06141b6603a5",
        "a152fffe-1351-11ed-a312-02ae981fbade",
        "261d0a1a-134b-11ed-8d43-02b0ee514bc7",
        "e8f569aa-134c-11ed-9c34-02b0ee514bc7",
        "afc61d68-134d-11ed-9a27-069e4f790264",
        "2dc0c1ea-134f-11ed-9794-02b0ee514bc7",
        "3607098a-1350-11ed-8238-022c40d974e6",
        "8ffde976-1351-11ed-b2df-069e4f790264",
        "39f59784-1353-11ed-aab8-02ae981fbade",
        "f00c6cd2-1353-11ed-bb5c-069e4f790264",
        "e3a23038-1355-11ed-ad0a-022c40d974e6",
        "f3187ab0-1367-11ed-931a-06141b6603a5",
        "38f94394-1371-11ed-bafa-02ae981fbade",
        "5beeaaae-1373-11ed-a008-02ae981fbade",
        "4948c4b6-134b-11ed-8106-022c40d974e6",
        "0609e282-134d-11ed-aae9-069e4f790264",
        "cf8d6322-134d-11ed-843e-02ae981fbade",
        "4d01461a-134f-11ed-8e73-0630baf9dcb8",
        "5b162512-1350-11ed-889b-0630baf9dcb8",
        "b6beec18-1351-11ed-b496-069e4f790264",
        "125524ac-134d-11ed-9766-06141b6603a5",
        "d9f1abca-134d-11ed-be7d-06141b6603a5",
        "55c26270-134f-11ed-9ba5-022c40d974e6",
        "625625ca-1350-11ed-b32a-0630baf9dcb8",
        "bbd5a868-1351-11ed-865c-069e4f790264",
        "517ec676-134b-11ed-83f6-06141b6603a5",
        "0e224f22-134d-11ed-9c5b-022c40d974e6",
        "d548b834-134d-11ed-b717-02ae981fbade",
        "5b13c90c-1350-11ed-a991-02ae981fbade",
        "b43d2536-1351-11ed-bd1f-069e4f790264",
        "22fd1aa0-134b-11ed-90a8-022c40d974e6",
        "f39cb962-134c-11ed-b05f-022c40d974e6",
        "be00c860-134d-11ed-9059-0630baf9dcb8",
        "36657ae8-134f-11ed-ad91-069e4f790264",
        "424527e0-1350-11ed-8e59-0630baf9dcb8",
        "98512df4-1351-11ed-bf18-0630baf9dcb8",
        "212bfec6-134b-11ed-9d09-0630baf9dcb8",
        "c9dffd78-134c-11ed-9372-02ae981fbade",
        "959ce930-134d-11ed-8220-022c40d974e6",
        "1502eff2-134f-11ed-9bae-06141b6603a5",
        "28cad940-1350-11ed-8e03-02b0ee514bc7",
        "823a07ca-1351-11ed-bdff-06141b6603a5",
        "137e31b6-137f-11ed-8e5b-0630baf9dcb8",
        "1480d2fa-134b-11ed-9443-069e4f790264",
        "dec829f4-134c-11ed-b9a7-0630baf9dcb8",
        "a346d37a-134d-11ed-b4f7-069e4f790264",
        "20e0dc3a-134f-11ed-80a4-0630baf9dcb8",
        "2a6a4344-1350-11ed-8c7b-022c40d974e6",
        "841aadd8-1351-11ed-a4d7-02b0ee514bc7",
        "2c85c7d6-1353-11ed-a123-02ae981fbade",
        "e9e1c0c8-1353-11ed-8f33-022c40d974e6",
        "8cd70d2e-1354-11ed-adc7-0630baf9dcb8",
        "de199cd2-1355-11ed-a1b8-022c40d974e6",
        "eec71f22-1360-11ed-8bac-0630baf9dcb8",
        "fd293788-1367-11ed-8436-06141b6603a5",
        "925dc340-1369-11ed-91cb-02ae981fbade",
        "14af6c00-134b-11ed-bcf8-02b0ee514bc7",
        "dc177688-134c-11ed-81b4-06141b6603a5",
        "a5d69c10-134d-11ed-8ab8-02ae981fbade",
        "24bb3a80-134f-11ed-bf86-02ae981fbade",
        "29a63c92-1350-11ed-ad18-0630baf9dcb8",
        "84da882e-1351-11ed-8f59-0630baf9dcb8",
        "30496486-1353-11ed-ad08-02ae981fbade",
        "ec0109e0-1353-11ed-80af-02b0ee514bc7",
        "8fc8093e-1354-11ed-8f6d-069e4f790264",
        "de039040-1355-11ed-a5e7-06141b6603a5",
        "028ae154-1368-11ed-baa3-02b0ee514bc7",
        "46d0afb6-1371-11ed-936f-06141b6603a5",
        "6b1bebfe-1373-11ed-8aab-02b0ee514bc7",
        "f30fa6fe-1373-11ed-bcaa-0630baf9dcb8",
        "2d6905de-1374-11ed-bb79-02ae981fbade",
        "ea5f8fb4-1374-11ed-b3fa-06141b6603a5",
        "2cd5879c-134b-11ed-ada8-02ae981fbade",
        "ed5370dc-134c-11ed-ba09-02b0ee514bc7",
        "b306539e-134d-11ed-ab9e-02ae981fbade",
        "2ed4a34e-134f-11ed-a8ab-02ae981fbade",
        "34072796-1350-11ed-8ffd-069e4f790264",
        "8c0524b0-1351-11ed-9f10-022c40d974e6",
        "322445be-1353-11ed-9a00-0630baf9dcb8",
        "f31dcaf6-1353-11ed-a995-022c40d974e6",
        "9359e4aa-1354-11ed-886c-02ae981fbade",
        "e9bbe3b0-1355-11ed-8705-06141b6603a5",
        "ebab488e-1367-11ed-9e4b-0630baf9dcb8",
        "0c107c7e-134b-11ed-9820-069e4f790264",
        "d7faeada-134c-11ed-b2ad-02b0ee514bc7",
        "9c68ec00-134d-11ed-868c-069e4f790264",
        "135fa8de-134f-11ed-8f3d-069e4f790264",
        "1d651da4-1350-11ed-91e9-06141b6603a5",
        "76c4a17a-1351-11ed-9092-02b0ee514bc7",
        "1efdbdbc-1353-11ed-b4f1-02b0ee514bc7",
        "de03433a-1353-11ed-b927-06141b6603a5",
        "8182e510-1354-11ed-9ef3-02ae981fbade",
        "d0646946-1355-11ed-89fc-069e4f790264",
        "e2088cd0-1360-11ed-b020-02ae981fbade",
        "f3810562-1367-11ed-8e8d-06141b6603a5",
        "39a32828-1371-11ed-88b8-0630baf9dcb8",
        "5b3cb8da-1373-11ed-8e1a-02b0ee514bc7",
        "cd56c266-134c-11ed-aec6-022c40d974e6",
        "9400432e-134d-11ed-998c-022c40d974e6",
        "0e5020d0-134f-11ed-8900-02b0ee514bc7",
        "16235290-1350-11ed-9601-06141b6603a5",
        "75ad840a-1351-11ed-8932-022c40d974e6",
        "1de5f7a0-1353-11ed-94c9-022c40d974e6",
        "17ed4c66-134b-11ed-99b5-0630baf9dcb8",
        "b4512cf6-134d-11ed-8856-02b0ee514bc7",
        "3d67a820-134f-11ed-8ea6-02ae981fbade",
        "4fce238a-1350-11ed-9f91-06141b6603a5",
        "ab8f0f76-1351-11ed-a09f-022c40d974e6",
        "09c33eb4-134d-11ed-96e0-02ae981fbade",
        "d1df5b62-134d-11ed-93e3-069e4f790264",
        "4e72dda6-134f-11ed-acd3-02b0ee514bc7",
        "577b5a1c-1350-11ed-9660-02ae981fbade",
        "b6c0900e-1351-11ed-adf7-02b0ee514bc7",
        "31832a4c-134b-11ed-8d48-02b0ee514bc7",
        "d0fb21a0-134c-11ed-8af1-0630baf9dcb8",
        "9c6b970c-134d-11ed-9739-02ae981fbade",
        "1bc3f0c0-134f-11ed-a61a-022c40d974e6",
        "257e940c-1350-11ed-9f59-069e4f790264",
        "7ad3b27e-1351-11ed-ba23-06141b6603a5",
        "0b5b3aa6-137f-11ed-9143-02ae981fbade",
        "f8e1c0e8-134c-11ed-a188-022c40d974e6",
        "c4fcf3dc-134d-11ed-850a-022c40d974e6",
        "3ea005f2-134f-11ed-b1a2-02b0ee514bc7",
        "5062f76c-1350-11ed-874e-02ae981fbade",
        "a147376e-1351-11ed-b400-02b0ee514bc7",
        "2979394a-134b-11ed-bbd2-06141b6603a5",
        "c1b2cc2a-134c-11ed-b58b-022c40d974e6",
        "0c0abcc6-134b-11ed-9f4c-0630baf9dcb8",
        "e305c18e-134c-11ed-840f-02ae981fbade",
        "ab067246-134d-11ed-a8dd-069e4f790264",
        "286eef5a-134f-11ed-8549-02b0ee514bc7",
        "34bf4a4c-1350-11ed-b95b-06141b6603a5",
        "8e9df0ee-1351-11ed-9bd1-02b0ee514bc7",
        "127e0270-134b-11ed-bd32-069e4f790264",
        "e908ecc8-134c-11ed-93f2-02ae981fbade",
        "b187e168-134d-11ed-b4ec-06141b6603a5",
        "2d85e426-134f-11ed-8559-02b0ee514bc7",
        "396af3ca-1350-11ed-983c-02b0ee514bc7",
        "9267498c-1351-11ed-8a5e-069e4f790264",
        "4423db24-134b-11ed-8230-069e4f790264",
        "fd7b0ac4-134c-11ed-b810-02ae981fbade",
        "432e72ac-134f-11ed-8297-069e4f790264",
        "4980fab6-1350-11ed-ae96-022c40d974e6",
        "9fed395e-1351-11ed-b147-0630baf9dcb8",
        "25cdc3e2-134b-11ed-bffb-022c40d974e6",
        "e93a1a8c-134c-11ed-9015-06141b6603a5",
        "adc348c4-134d-11ed-a1fb-06141b6603a5",
        "2ab30c7e-134f-11ed-a206-0630baf9dcb8",
        "34c049a6-1350-11ed-81ec-06141b6603a5",
        "8ff8c978-1351-11ed-9f94-02ae981fbade",
        "3c90b6c2-1353-11ed-8b96-02b0ee514bc7",
        "f12bc02c-1353-11ed-a354-02b0ee514bc7",
        "97cb7882-1354-11ed-985d-02ae981fbade",
        "e1f409f0-1355-11ed-9a35-0630baf9dcb8",
        "f2df7ba4-1360-11ed-8fd6-02ae981fbade",
        "07114998-1368-11ed-b25d-02ae981fbade",
        "4acf90b4-1371-11ed-a181-0630baf9dcb8",
        "6d698740-1373-11ed-a128-022c40d974e6",
        "f429a21a-1373-11ed-a10f-02ae981fbade",
        "2bccecc2-1374-11ed-bc35-02b0ee514bc7",
        "e6b9ea26-1374-11ed-8e5e-022c40d974e6",
        "18a89bec-134b-11ed-83a1-02ae981fbade",
        "ef8ae42a-134c-11ed-882e-022c40d974e6",
        "34f8bd6e-134f-11ed-8a87-02b0ee514bc7",
        "3e041af6-1350-11ed-882d-02ae981fbade",
        "98bae758-1351-11ed-ae4d-022c40d974e6",
        "098b2562-134b-11ed-af7a-06141b6603a5",
        "d802d3da-134c-11ed-89c9-02ae981fbade",
        "9d652060-134d-11ed-b937-06141b6603a5",
        "24df4d5c-1350-11ed-9f0e-069e4f790264",
        "7c798d1a-1351-11ed-8e6a-06141b6603a5",
        "24970364-1353-11ed-b88e-022c40d974e6",
        "e76b30ae-1353-11ed-91e8-069e4f790264",
        "8a689364-1354-11ed-a045-06141b6603a5",
        "e3897ca4-1360-11ed-b7bd-02ae981fbade",
        "fdbfd47c-1367-11ed-b6d8-02ae981fbade",
        "46061bc0-1371-11ed-9acf-0630baf9dcb8",
        "68a40474-1373-11ed-ba95-02b0ee514bc7",
        "f0f7f236-1373-11ed-8db1-02b0ee514bc7",
        "28d29b2a-1374-11ed-8553-06141b6603a5",
        "eaf87d82-1374-11ed-a576-069e4f790264",
        "1c778cce-134b-11ed-8ea7-069e4f790264",
        "c783ca28-134c-11ed-9f92-0630baf9dcb8",
        "8f57f6e6-134d-11ed-9bb5-069e4f790264",
        "0adeee9a-134f-11ed-a100-0630baf9dcb8",
        "1995b652-1350-11ed-bd55-06141b6603a5",
        "753bbe74-1351-11ed-a3be-0630baf9dcb8",
        "09a4bb92-137f-11ed-aff2-022c40d974e6",
        "15646a36-134d-11ed-b269-02b0ee514bc7",
        "dac40e6c-134d-11ed-ab57-02b0ee514bc7",
        "5fab4f6c-1350-11ed-89fe-02ae981fbade",
        "b8ed8f12-1351-11ed-968d-02ae981fbade",
        "f1700090-134c-11ed-b732-0630baf9dcb8",
        "bafea3bc-134d-11ed-b14e-022c40d974e6",
        "38abb7d6-134f-11ed-abd6-06141b6603a5",
        "43c4e9ca-1350-11ed-a85b-02b0ee514bc7",
        "adee47a0-1351-11ed-9d81-02b0ee514bc7",
        "0a616ff0-134b-11ed-a478-022c40d974e6",
        "d331e5a8-134c-11ed-9bad-02b0ee514bc7",
        "9a710a90-134d-11ed-a107-06141b6603a5",
        "191fd636-134f-11ed-938a-02b0ee514bc7",
        "1fd2f386-1350-11ed-8d78-0630baf9dcb8",
        "78666a5e-1351-11ed-b372-069e4f790264",
        "1bb5ddf6-1353-11ed-a048-069e4f790264",
        "db70fbd0-1353-11ed-b884-02ae981fbade",
        "1c86acea-134b-11ed-acd5-0630baf9dcb8",
        "c7337b54-134c-11ed-96a2-0630baf9dcb8",
        "8f05dea6-134d-11ed-bdd2-02ae981fbade",
        "0c6c2f7a-134f-11ed-bd07-069e4f790264",
        "1a1ed054-1350-11ed-be09-02b0ee514bc7",
        "7195844e-1351-11ed-824a-02ae981fbade",
        "1acb6d98-1353-11ed-9dcd-02ae981fbade",
        "0dfe6762-134b-11ed-8d25-022c40d974e6",
        "e0fa058a-134c-11ed-9c60-022c40d974e6",
        "a8940974-134d-11ed-999b-0630baf9dcb8",
        "25e78864-134f-11ed-be79-02b0ee514bc7",
        "2f0ec8e8-1350-11ed-bccd-0630baf9dcb8",
        "863f3e58-1351-11ed-acca-069e4f790264",
        "1b18023a-137f-11ed-bdf4-069e4f790264",
        "0a670ff0-134b-11ed-9ede-0630baf9dcb8",
        "dc2a2544-134c-11ed-bb7c-02ae981fbade",
        "a7530484-134d-11ed-8946-069e4f790264",
        "1fa1e814-134f-11ed-a4d3-022c40d974e6",
        "2b8faaa2-1350-11ed-b8c5-02b0ee514bc7",
        "88bd9044-1351-11ed-8de3-069e4f790264",
        "16eaebdc-137f-11ed-9cbb-06141b6603a5",
        "15866c14-134b-11ed-81d8-022c40d974e6",
        "dd917630-134c-11ed-8fc0-0630baf9dcb8",
        "a47bbc60-134d-11ed-b567-02b0ee514bc7",
        "23e4daf8-134f-11ed-b7a0-0630baf9dcb8",
        "2d5d361a-1350-11ed-8bfd-022c40d974e6",
        "8ab80a8c-1351-11ed-b835-022c40d974e6",
        "35758fb6-1353-11ed-9f07-022c40d974e6",
        "f718aa90-1353-11ed-8db9-022c40d974e6",
        "9adddeb6-1354-11ed-898e-0630baf9dcb8",
        "dfd33b82-1355-11ed-9761-06141b6603a5",
        "eaafa1ca-1360-11ed-9960-06141b6603a5",
        "007a666e-1368-11ed-804b-022c40d974e6",
        "456ced42-1371-11ed-99c8-022c40d974e6",
        "6807c29e-1373-11ed-acc7-02b0ee514bc7",
        "ec045c6a-1373-11ed-b381-02b0ee514bc7",
        "220d75bc-1374-11ed-ba2c-0630baf9dcb8",
        "0dd78ef8-134b-11ed-8745-022c40d974e6",
        "e4249d06-134c-11ed-b7a3-02ae981fbade",
        "ac356604-134d-11ed-a08d-0630baf9dcb8",
        "251ffc36-134f-11ed-8a56-06141b6603a5",
        "31bde682-1350-11ed-b58d-02b0ee514bc7",
        "8fef89b2-1351-11ed-a561-06141b6603a5",
        "2042186a-134b-11ed-958f-06141b6603a5",
        "cab6524c-134c-11ed-b7bb-0630baf9dcb8",
        "935c04bc-134d-11ed-ba49-022c40d974e6",
        "1110c874-134f-11ed-9e6b-06141b6603a5",
        "1c48f3e6-1350-11ed-87bc-069e4f790264",
        "78809c44-1351-11ed-a3a8-0630baf9dcb8",
        "0618de54-137f-11ed-9ef9-02b0ee514bc7",
        "1474dcac-134b-11ed-893d-069e4f790264",
        "e662b3d2-134c-11ed-a1dd-02b0ee514bc7",
        "ac8a0a1a-134d-11ed-a0fb-02ae981fbade",
        "2f90aa26-134f-11ed-88db-069e4f790264",
        "39cb2056-1350-11ed-b7c7-02ae981fbade",
        "94f5363c-1351-11ed-87d1-06141b6603a5",
        "0905bb78-134d-11ed-8ba7-069e4f790264",
        "d0471e48-134d-11ed-908a-069e4f790264",
        "4d195656-134f-11ed-a035-02ae981fbade",
        "551abe20-1350-11ed-88a4-02b0ee514bc7",
        "b0717240-1351-11ed-a050-02ae981fbade",
        "4a3839b0-134b-11ed-86fc-02b0ee514bc7",
        "cb60e8dc-134d-11ed-bd7d-02b0ee514bc7",
        "44e34ed8-134f-11ed-ac39-06141b6603a5",
        "4c8fd452-1350-11ed-a65e-06141b6603a5",
        "a5e67708-1351-11ed-9196-02b0ee514bc7",
        "1db6e292-134b-11ed-8267-06141b6603a5",
        "c8d48584-134c-11ed-aaa0-022c40d974e6",
        "93ec9770-134d-11ed-8707-0630baf9dcb8",
        "0deeded8-134f-11ed-aef0-06141b6603a5",
        "155c4d8a-1350-11ed-91d4-0630baf9dcb8",
        "0de035bc-134b-11ed-889a-02ae981fbade",
        "d7fbaeb6-134c-11ed-a051-022c40d974e6",
        "9cde463a-134d-11ed-9c92-02b0ee514bc7",
        "1da3a94e-134f-11ed-80be-0630baf9dcb8",
        "297d6ce0-1350-11ed-8446-06141b6603a5",
        "85003380-1351-11ed-b237-069e4f790264",
        "2b885ae2-1353-11ed-a024-069e4f790264",
        "e9917618-1353-11ed-8240-022c40d974e6",
        "8ba16620-1354-11ed-b1f4-02ae981fbade",
        "dd1c84b6-1355-11ed-9b5a-069e4f790264",
        "e8762dd4-1360-11ed-95bc-02ae981fbade",
        "fbc66ab4-1367-11ed-9d49-069e4f790264",
        "427d63dc-1371-11ed-b7a5-02ae981fbade",
        "64e55f22-1373-11ed-a915-022c40d974e6",
        "eeabb706-1373-11ed-a3c8-06141b6603a5",
        "26a37090-1374-11ed-81b4-069e4f790264",
        "e3580e4e-1374-11ed-b805-0630baf9dcb8",
        "2da3fd8e-134b-11ed-85c4-069e4f790264",
        "eec7358e-134c-11ed-aece-0630baf9dcb8",
        "323aa42a-134f-11ed-a223-022c40d974e6",
        "3c8e2c20-1350-11ed-9023-0630baf9dcb8",
        "17ecfe52-1353-11ed-9a5b-0630baf9dcb8",
        "0517dd04-134b-11ed-bdd3-02b0ee514bc7",
        "cf1c4bca-134c-11ed-b5c4-06141b6603a5",
        "99279d02-134d-11ed-b2ec-0630baf9dcb8",
        "18263644-134f-11ed-98b8-022c40d974e6",
        "26bca214-1350-11ed-abcd-06141b6603a5",
        "81674006-1351-11ed-886e-02b0ee514bc7",
        "29a76650-1353-11ed-b96c-0630baf9dcb8",
        "e658ef1c-1353-11ed-9d43-06141b6603a5",
        "8df90fa4-1354-11ed-a626-0630baf9dcb8",
        "dd0a15ec-1355-11ed-a1fe-02ae981fbade",
        "e9d33f50-1360-11ed-b843-0630baf9dcb8",
        "021f877e-1368-11ed-8f53-022c40d974e6",
        "48830bb0-1371-11ed-94b2-02ae981fbade",
        "6af78c14-1373-11ed-909b-0630baf9dcb8",
        "f2f2deac-1373-11ed-bfd0-02ae981fbade",
        "2a48a274-1374-11ed-aad0-06141b6603a5",
        "e3cc62f8-1374-11ed-8ac0-022c40d974e6",
        "23af0864-134b-11ed-8011-02b0ee514bc7",
        "f57a5dca-134c-11ed-8387-069e4f790264",
        "b9750036-134d-11ed-8500-0630baf9dcb8",
        "376601c4-134f-11ed-acbd-06141b6603a5",
        "45bba50c-1350-11ed-a35e-069e4f790264",
        "9c084824-1351-11ed-9de9-0630baf9dcb8",
        "28baabf6-134b-11ed-abf8-022c40d974e6",
        "c38f2278-134c-11ed-9571-06141b6603a5",
        "18746da4-134b-11ed-91f5-02b0ee514bc7",
        "e0a9130a-134c-11ed-9988-0630baf9dcb8",
        "aa60ecf4-134d-11ed-972c-02b0ee514bc7",
        "2a77f5c6-134f-11ed-82b7-02b0ee514bc7",
        "30a47306-1350-11ed-b521-02ae981fbade",
        "916c13aa-1351-11ed-aa2a-02ae981fbade",
        "3ba5ebd8-1353-11ed-9e31-069e4f790264",
        "f0258438-1353-11ed-bbce-02b0ee514bc7",
        "913984c8-1354-11ed-af60-022c40d974e6",
        "e971a2dc-1355-11ed-9641-0630baf9dcb8",
        "eb033cf2-1367-11ed-94ba-06141b6603a5",
        "2b9e7294-134b-11ed-85b6-0630baf9dcb8",
        "c69b0ab8-134c-11ed-a0ba-02ae981fbade",
        "8c5d3faa-134d-11ed-a037-0630baf9dcb8",
        "55b0f85e-134b-11ed-a343-06141b6603a5",
        "14b7a242-134d-11ed-b7de-06141b6603a5",
        "d8cde380-134d-11ed-a63f-02ae981fbade",
        "54c92e30-134f-11ed-ab5d-06141b6603a5",
        "0d9682f0-134b-11ed-a1ec-02b0ee514bc7",
        "e200b44c-134c-11ed-9e44-06141b6603a5",
        "a9f2fa50-134d-11ed-a273-02b0ee514bc7",
        "24b2a104-134f-11ed-8bfd-0630baf9dcb8",
        "31bec16a-1350-11ed-ab86-069e4f790264",
        "9036ccfa-1351-11ed-ae96-022c40d974e6",
        "d23f0e50-134c-11ed-a05f-069e4f790264",
        "97a8c6c2-134d-11ed-8ded-02b0ee514bc7",
        "148475b4-134f-11ed-91c4-0630baf9dcb8",
        "1de571e8-1350-11ed-b0fb-0630baf9dcb8",
        "74bb58f6-1351-11ed-99a5-069e4f790264",
        "1ff7f73c-1353-11ed-a8a9-06141b6603a5",
        "df9437ae-1353-11ed-b596-02b0ee514bc7",
        "8765a6c0-1354-11ed-9e29-02b0ee514bc7",
        "d6091e00-1355-11ed-80f6-0630baf9dcb8",
        "e463832c-1360-11ed-901e-022c40d974e6",
        "fbd9d4c8-1367-11ed-a7c0-0630baf9dcb8",
        "43948192-1371-11ed-86fd-069e4f790264",
        "657e420a-1373-11ed-aa7c-02b0ee514bc7",
        "eafb102a-1373-11ed-a13c-02ae981fbade",
        "24585b20-1374-11ed-a05a-06141b6603a5",
        "08bcb916-134b-11ed-9611-02b0ee514bc7",
        "d7f875de-134c-11ed-904d-06141b6603a5",
        "9f6a3a9e-134d-11ed-aa93-02b0ee514bc7",
        "17e4fc88-134f-11ed-bfa8-0630baf9dcb8",
        "235f30c8-1350-11ed-96f9-02b0ee514bc7",
        "7ed3edc6-1351-11ed-9d90-02b0ee514bc7",
        "10a24cde-137f-11ed-af55-0630baf9dcb8",
        "0acf59ca-134b-11ed-86d0-02ae981fbade",
        "d54d8a18-134c-11ed-a982-02b0ee514bc7",
        "9bb9e37c-134d-11ed-894b-02ae981fbade",
        "130b084c-134f-11ed-b871-069e4f790264",
        "1bf9da2c-1350-11ed-8d9b-02ae981fbade",
        "79a38e38-1351-11ed-a584-02b0ee514bc7",
        "221f9876-1353-11ed-a168-02b0ee514bc7",
        "dd9cfaf8-1353-11ed-bde4-0630baf9dcb8",
        "812ef93c-1354-11ed-96cb-02b0ee514bc7",
        "cf6bb292-1355-11ed-b577-06141b6603a5",
        "e19632b6-1360-11ed-954b-06141b6603a5",
        "f3fb9084-1367-11ed-aeee-02b0ee514bc7",
        "4c655fa8-1371-11ed-a277-069e4f790264",
        "6ebd24c6-1373-11ed-933a-0630baf9dcb8",
        "f5e46252-1373-11ed-8a21-06141b6603a5",
        "2d4fc02e-1374-11ed-864e-022c40d974e6",
        "e9ae440c-1374-11ed-9790-06141b6603a5",
        "efbfe986-134c-11ed-a78f-0630baf9dcb8",
        "b352dbd8-134d-11ed-bc63-0630baf9dcb8",
        "3223aa54-134f-11ed-9e3f-06141b6603a5",
        "3b89462a-1350-11ed-9237-0630baf9dcb8",
        "93f3b650-1351-11ed-9097-06141b6603a5",
        "3a8e87e6-1353-11ed-b8f9-02b0ee514bc7",
        "f029f25c-1353-11ed-9755-02ae981fbade",
        "922ac4be-1354-11ed-a599-022c40d974e6",
        "e905a46a-1355-11ed-b399-02b0ee514bc7",
        "ece87aa0-1367-11ed-9a29-02b0ee514bc7",
        "25251f12-134b-11ed-b345-06141b6603a5",
        "efe3c928-134c-11ed-b311-022c40d974e6",
        "b83fdad8-134d-11ed-8a94-02b0ee514bc7",
        "322604fc-134f-11ed-bdac-06141b6603a5",
        "4028dcfe-1350-11ed-8e27-02ae981fbade",
        "96cc6e3a-1351-11ed-b242-069e4f790264",
        "4ade42ce-134b-11ed-afb4-02ae981fbade",
        "03e0e5fa-134d-11ed-82f8-02ae981fbade",
        "4a2d0bae-134f-11ed-9f61-02b0ee514bc7",
        "9f3c8262-1351-11ed-9591-069e4f790264",
        "cc62e4de-134c-11ed-ab89-02ae981fbade",
        "94e716d2-134d-11ed-86e6-06141b6603a5",
        "12333e26-134f-11ed-b5a8-069e4f790264",
        "1a5e2b00-1350-11ed-a6c5-0630baf9dcb8",
        "72f35d8e-1351-11ed-8a93-02ae981fbade",
        "190707ba-1353-11ed-84fe-02ae981fbade",
        "4bbda766-134b-11ed-82fa-02ae981fbade",
        "074efcea-134d-11ed-88c2-022c40d974e6",
        "d1ceddf0-134d-11ed-ba55-0630baf9dcb8",
        "57cb5b4e-134f-11ed-a05b-02b0ee514bc7",
        "60da2bc4-1350-11ed-9f3e-06141b6603a5",
        "b9abd670-1351-11ed-a223-0630baf9dcb8",
        "13d73c9a-134b-11ed-aa80-022c40d974e6",
        "dbd40452-134c-11ed-a8a5-069e4f790264",
        "a1e40e4e-134d-11ed-b19b-0630baf9dcb8",
        "1b93006e-134f-11ed-be77-022c40d974e6",
        "23ea3c9a-1350-11ed-8a42-022c40d974e6",
        "80feeeac-1351-11ed-be61-06141b6603a5",
        "2aec8f72-1353-11ed-8250-069e4f790264",
        "ed271bf2-1353-11ed-befe-022c40d974e6",
        "98b4ba9c-1354-11ed-b301-069e4f790264",
        "e1990848-1355-11ed-a905-06141b6603a5",
        "f2b209c6-1360-11ed-821e-0630baf9dcb8",
        "0614a1fc-1368-11ed-9332-06141b6603a5",
        "bc8272aa-136a-11ed-822c-0630baf9dcb8",
        "e423c73c-134c-11ed-abed-06141b6603a5",
        "aa071be8-134d-11ed-b48b-02ae981fbade",
        "24bcdcf0-134f-11ed-b71a-02ae981fbade",
        "2ffc4942-1350-11ed-b4b4-02b0ee514bc7",
        "8659d966-1351-11ed-923a-02ae981fbade",
        "2cfe0a5c-1353-11ed-9207-069e4f790264",
        "ea7e4402-1353-11ed-8898-06141b6603a5",
        "90b1ba7a-1354-11ed-a334-02ae981fbade",
        "ec3df3a8-1355-11ed-8505-02ae981fbade",
        "ef38feba-1367-11ed-ba65-02ae981fbade",
        "95b1e1da-1368-11ed-b822-02ae981fbade",
        "10004008-134b-11ed-bcab-06141b6603a5",
        "d6573eb8-134c-11ed-a6a7-022c40d974e6",
        "9e10b506-134d-11ed-b5f7-06141b6603a5",
        "1d65e744-134f-11ed-b34b-0630baf9dcb8",
        "28c12742-1350-11ed-81c6-0630baf9dcb8",
        "825a2a28-1351-11ed-8d5c-02b0ee514bc7",
        "2a537990-1353-11ed-bd05-0630baf9dcb8",
        "e803de4e-1353-11ed-9a65-02b0ee514bc7",
        "8d460ddc-1354-11ed-978f-069e4f790264",
        "dac54bf8-1355-11ed-a8a0-02ae981fbade",
        "ec6a5a50-1360-11ed-91b3-02ae981fbade",
        "044e27ee-1368-11ed-8245-0630baf9dcb8",
        "4ce5b450-1371-11ed-a7b1-02b0ee514bc7",
        "6fe0e158-1373-11ed-b2f5-02b0ee514bc7",
        "f651e886-1373-11ed-89cc-02ae981fbade",
        "2e9716f8-1374-11ed-b422-02b0ee514bc7",
        "ffe6b6d2-134c-11ed-8da0-02b0ee514bc7",
        "43ca32be-134f-11ed-8f01-0630baf9dcb8",
        "4ad5499e-1350-11ed-89db-02b0ee514bc7",
        "a697ead8-1351-11ed-9acd-02ae981fbade",
        "16630d68-134b-11ed-8ab1-06141b6603a5",
        "e2149d36-134c-11ed-8292-022c40d974e6",
        "a7ffd7cc-134d-11ed-ae6c-02ae981fbade",
        "26caaefa-134f-11ed-a174-022c40d974e6",
        "2edac106-1350-11ed-a0c3-022c40d974e6",
        "87b4ea4e-1351-11ed-9298-02b0ee514bc7",
        "2e59a73a-1353-11ed-9622-02ae981fbade",
        "ed2aa16e-1353-11ed-bb54-069e4f790264",
        "96487708-1354-11ed-9c1e-02b0ee514bc7",
        "e3fc7de0-1355-11ed-b7c0-022c40d974e6",
        "f1f0790a-1360-11ed-ab19-02b0ee514bc7",
        "04e99026-1368-11ed-874e-02ae981fbade",
        "4b9d341a-1371-11ed-947f-069e4f790264",
        "6dfee10a-1373-11ed-9939-02ae981fbade",
        "f170dee4-1373-11ed-995d-06141b6603a5",
        "285d83f8-1374-11ed-80bd-022c40d974e6",
        "e63c8266-1374-11ed-8fbb-02b0ee514bc7",
        "5e012948-134b-11ed-bc14-0630baf9dcb8",
        "1836468a-134d-11ed-a760-02ae981fbade",
        "dffe75de-134d-11ed-ae6f-06141b6603a5",
        "5da07482-134f-11ed-8181-022c40d974e6",
        "c1b71a32-1351-11ed-b5fb-02b0ee514bc7",
        "3f6cb4b6-134b-11ed-9d94-06141b6603a5",
        "d3adf63e-134c-11ed-8166-06141b6603a5",
        "9b61cb7e-134d-11ed-b7c2-02ae981fbade",
        "149f5866-134f-11ed-aee7-02ae981fbade",
        "1de73b18-1350-11ed-8b6b-022c40d974e6",
        "74a9393c-1351-11ed-9a96-069e4f790264",
        "09bb493e-137f-11ed-92ac-069e4f790264",
        "cfd55bf6-134c-11ed-b1da-0630baf9dcb8",
        "96bd983c-134d-11ed-8771-06141b6603a5",
        "15e37dec-134f-11ed-9ec1-06141b6603a5",
        "21af3322-1350-11ed-be47-022c40d974e6",
        "7e3b5944-1351-11ed-a9ce-0630baf9dcb8",
        "25b57e24-1353-11ed-8f58-06141b6603a5",
        "e232f95a-1353-11ed-b659-02ae981fbade",
        "833351d8-1354-11ed-8205-06141b6603a5",
        "d28a67e8-1355-11ed-99b5-0630baf9dcb8",
        "e1956034-1360-11ed-99cd-06141b6603a5",
        "f2beeb62-1367-11ed-8c42-0630baf9dcb8",
        "38b0594a-1371-11ed-bc3a-02ae981fbade",
        "5c190394-1373-11ed-92e2-02b0ee514bc7",
        "4ee0d4a4-134b-11ed-929b-069e4f790264",
        "d1db9004-134d-11ed-88f9-06141b6603a5",
        "50b62992-134f-11ed-b887-022c40d974e6",
        "5d208622-1350-11ed-bc4d-02b0ee514bc7",
        "b5070568-1351-11ed-8d77-02ae981fbade",
        "0186489a-134d-11ed-aa8c-06141b6603a5",
        "cd7bca88-134d-11ed-954c-06141b6603a5",
        "49512b48-134f-11ed-8b3a-0630baf9dcb8",
        "afb3b566-1351-11ed-9455-069e4f790264",
        "3bb2bf00-134b-11ed-b782-02ae981fbade",
        "fac53e3a-134c-11ed-bb55-069e4f790264",
        "c3617a84-134d-11ed-9dd2-0630baf9dcb8",
        "3f3f8bcc-134f-11ed-9ddc-06141b6603a5",
        "51ae34a6-1350-11ed-98ad-02ae981fbade",
        "ae63cf16-1351-11ed-b8d5-02b0ee514bc7",
        "4a54e344-134b-11ed-ba08-069e4f790264",
        "09c28528-134d-11ed-ad68-02b0ee514bc7",
        "d03dcf14-134d-11ed-be61-0630baf9dcb8",
        "4ffcc920-134f-11ed-9e89-069e4f790264",
        "5c3838ea-1350-11ed-8f1c-069e4f790264",
        "b50cedc0-1351-11ed-9997-022c40d974e6",
        "58dc8a84-134b-11ed-a512-06141b6603a5",
        "172298f2-134d-11ed-b472-02ae981fbade",
        "5b845c5e-134f-11ed-9bec-02b0ee514bc7",
        "66826604-1350-11ed-bec7-069e4f790264",
        "bfdb52aa-1351-11ed-833b-022c40d974e6",
        "e53957e0-134c-11ed-871b-06141b6603a5",
        "b2604d96-134d-11ed-805f-069e4f790264",
        "2a6a5a92-134f-11ed-9d80-06141b6603a5",
        "33ec3a1c-1350-11ed-82b0-06141b6603a5",
        "8aa593b6-1351-11ed-a2ef-06141b6603a5",
        "2d93def4-134b-11ed-8a35-069e4f790264",
        "f729ec1c-134c-11ed-bf82-02b0ee514bc7",
        "becd6c26-134d-11ed-bce3-02b0ee514bc7",
        "3ab411c2-134f-11ed-86b8-02ae981fbade",
        "45096dc4-1350-11ed-8806-0630baf9dcb8",
        "9ed16a18-1351-11ed-bd9e-0630baf9dcb8",
        "510d2e44-134b-11ed-874f-02ae981fbade",
        "10857f00-134d-11ed-85a8-06141b6603a5",
        "dd15c2aa-134d-11ed-9539-0630baf9dcb8",
        "53e189f4-134f-11ed-953d-02b0ee514bc7",
        "591ccf90-1350-11ed-81db-02ae981fbade",
        "b343cee6-1351-11ed-9d02-02ae981fbade",
        "2473fcdc-134b-11ed-a993-0630baf9dcb8",
        "f49786a8-134c-11ed-9672-069e4f790264",
        "b7b63cc4-134d-11ed-ae68-02b0ee514bc7",
        "33aa6322-134f-11ed-8fc8-069e4f790264",
        "3be3db80-1350-11ed-b8f3-022c40d974e6",
        "924855e0-1351-11ed-ab21-02b0ee514bc7",
        "3b8368b8-134b-11ed-b4e2-02b0ee514bc7",
        "fbb06ed2-134c-11ed-9e22-02b0ee514bc7",
        "c9576494-134d-11ed-ba62-069e4f790264",
        "46a1d578-134f-11ed-b8f9-022c40d974e6",
        "9ed54a70-1351-11ed-b18e-06141b6603a5",
        "19183290-134b-11ed-9d7f-02ae981fbade",
        "e9923a0a-134c-11ed-ab7b-02b0ee514bc7",
        "b0f3889c-134d-11ed-abf1-02ae981fbade",
        "2d11dc3e-134f-11ed-b4f1-02ae981fbade",
        "39684fda-1350-11ed-a9d7-02ae981fbade",
        "90bec59c-1351-11ed-8454-0630baf9dcb8",
        "11618240-134b-11ed-8098-069e4f790264",
        "e43ec596-134c-11ed-84dc-02ae981fbade",
        "afa9b2f4-134d-11ed-aab4-02ae981fbade",
        "2bf34680-134f-11ed-ba99-02ae981fbade",
        "388dbc76-1350-11ed-af57-069e4f790264",
        "9312eda0-1351-11ed-a85a-02ae981fbade",
        "f0650150-134c-11ed-8121-02ae981fbade",
        "b9dec7f0-134d-11ed-ae94-022c40d974e6",
        "387a60c8-134f-11ed-8c59-02ae981fbade",
        "4291f8fe-1350-11ed-a048-02ae981fbade",
        "af9753bc-1351-11ed-b26a-02ae981fbade",
        "37220bd0-134b-11ed-85ad-06141b6603a5",
        "cdf6e520-134c-11ed-9cd6-02b0ee514bc7",
        "90d21d12-134d-11ed-8dba-02b0ee514bc7",
        "10739b80-134f-11ed-b057-02ae981fbade",
        "19c6cd64-1350-11ed-b468-0630baf9dcb8",
        "7580afd4-1351-11ed-bf9c-06141b6603a5",
        "0914d608-137f-11ed-b915-02ae981fbade",
        "481a217a-134b-11ed-afd5-0630baf9dcb8",
        "05a50768-134d-11ed-9dcc-02b0ee514bc7",
        "cd75690e-134d-11ed-915f-06141b6603a5",
        "471350f4-134f-11ed-a85e-022c40d974e6",
        "acce4262-1351-11ed-acf9-069e4f790264",
        "25758132-134b-11ed-ad5b-022c40d974e6",
        "f0780df4-134c-11ed-8ea7-02b0ee514bc7",
        "ba26d5c2-134d-11ed-9779-02ae981fbade",
        "3a4055b6-134f-11ed-a555-022c40d974e6",
        "40278642-1350-11ed-8bb6-022c40d974e6",
        "97267c22-1351-11ed-a55c-06141b6603a5",
        "3228883e-134b-11ed-a746-0630baf9dcb8",
        "d17ed2ca-134c-11ed-84bf-02b0ee514bc7",
        "98dc8ed4-134d-11ed-a6fe-06141b6603a5",
        "15a99ee2-134f-11ed-8a2c-02b0ee514bc7",
        "226b2398-1350-11ed-9071-0630baf9dcb8",
        "7d26c91c-1351-11ed-bf7e-02b0ee514bc7",
        "10b214f2-137f-11ed-abf2-069e4f790264",
        "4932803e-134b-11ed-87b1-022c40d974e6",
        "05eab542-134d-11ed-b4fa-0630baf9dcb8",
        "ce09f57e-134d-11ed-a1f3-02b0ee514bc7",
        "494b45d4-134f-11ed-b9fe-02b0ee514bc7",
        "5258b908-1350-11ed-b278-022c40d974e6",
        "acbbc9ca-1351-11ed-b88d-022c40d974e6",
        "136c19d8-134b-11ed-a53b-02b0ee514bc7",
        "e696caf0-134c-11ed-8e66-06141b6603a5",
        "ae664b1e-134d-11ed-9b4b-022c40d974e6",
        "2a7f4d8a-134f-11ed-8298-069e4f790264",
        "341a6aa4-1350-11ed-80a3-069e4f790264",
        "92520482-1351-11ed-9ca3-0630baf9dcb8",
        "275e1d92-134b-11ed-b66c-02b0ee514bc7",
        "c72d64c6-134c-11ed-b88b-06141b6603a5",
        "8e0c3ed2-134d-11ed-bfcc-0630baf9dcb8",
        "0b66c9d2-134f-11ed-9d19-06141b6603a5",
        "1643ec3a-1350-11ed-9708-02b0ee514bc7",
        "723d7186-1351-11ed-8f6e-06141b6603a5",
        "03d0ff28-137f-11ed-80bf-06141b6603a5",
        "069fd988-134b-11ed-a239-02b0ee514bc7",
        "dbcfc400-134c-11ed-b796-06141b6603a5",
        "a1e35314-134d-11ed-8392-022c40d974e6",
        "19637cf6-134f-11ed-9017-069e4f790264",
        "22af2cb4-1350-11ed-9e30-069e4f790264",
        "7c7399f0-1351-11ed-8b4a-0630baf9dcb8",
        "0c2d3a4c-137f-11ed-a168-022c40d974e6",
        "33b21ea4-134b-11ed-affd-02ae981fbade",
        "ccca4b92-134c-11ed-8983-069e4f790264",
        "96bb320e-134d-11ed-ac1e-0630baf9dcb8",
        "11e04a2c-134f-11ed-beec-022c40d974e6",
        "1d96cebc-1350-11ed-8ce9-06141b6603a5",
        "76179e1c-1351-11ed-be75-069e4f790264",
        "0ea93a5a-137f-11ed-b0e7-06141b6603a5",
        "34e3aa72-134b-11ed-a706-02b0ee514bc7",
        "d053a6e6-134c-11ed-9c40-0630baf9dcb8",
        "980e08e8-134d-11ed-9738-0630baf9dcb8",
        "1361b20a-134f-11ed-b209-022c40d974e6",
        "1b99286c-1350-11ed-b4c2-022c40d974e6",
        "772db34a-1351-11ed-9f6b-022c40d974e6",
        "0a7272b2-137f-11ed-ba03-02b0ee514bc7",
        "5115042a-134b-11ed-8d18-06141b6603a5",
        "0d4c5430-134d-11ed-b761-022c40d974e6",
        "d32f15ac-134d-11ed-83f3-02ae981fbade",
        "4ff7de92-134f-11ed-a360-06141b6603a5",
        "591e665c-1350-11ed-a1fb-02ae981fbade",
        "b34868ac-1351-11ed-b427-02ae981fbade",
        "e18eee48-1360-11ed-9151-06141b6603a5",
        "dbd69fc8-134c-11ed-89e1-069e4f790264",
        "a1cab2aa-134d-11ed-b3c3-02b0ee514bc7",
        "1bea0058-134f-11ed-a414-0630baf9dcb8",
        "22ac5a16-1350-11ed-b020-02b0ee514bc7",
        "7d99b8be-1351-11ed-9699-0630baf9dcb8",
        "230dfe80-1353-11ed-92c3-022c40d974e6",
        "dc6765e2-1353-11ed-b6db-02b0ee514bc7",
        "83b8ba44-1354-11ed-8cef-02b0ee514bc7",
        "d30541d4-1355-11ed-94f0-0630baf9dcb8",
        "f0f93d0a-1367-11ed-9b82-022c40d974e6",
        "3f80bb70-1371-11ed-81ef-069e4f790264",
        "6012cf84-1373-11ed-a5ad-022c40d974e6",
        "e7ce6050-1373-11ed-9919-022c40d974e6",
        "0996a9f0-134b-11ed-b6b8-022c40d974e6",
        "cf853d10-134c-11ed-8eb9-06141b6603a5",
        "95ff95f8-134d-11ed-bdce-02ae981fbade",
        "17252b4c-134f-11ed-9965-02ae981fbade",
        "1f9c9638-1350-11ed-964f-06141b6603a5",
        "77eee79a-1351-11ed-8fe5-02b0ee514bc7",
        "1d66d254-1353-11ed-a204-022c40d974e6",
        "dc258276-1353-11ed-99d8-0630baf9dcb8",
        "c550e9ca-134c-11ed-9b62-06141b6603a5",
        "8f7e9c6a-134d-11ed-9d02-022c40d974e6",
        "0c1b024e-134f-11ed-8908-06141b6603a5",
        "1cde5972-1350-11ed-9dd1-022c40d974e6",
        "756762ea-1351-11ed-aee3-02b0ee514bc7",
        "07592896-137f-11ed-aae5-06141b6603a5",
        "2ca196a8-134b-11ed-b054-02b0ee514bc7",
        "f478ed1a-134c-11ed-ac2c-06141b6603a5",
        "b7a69f62-134d-11ed-b24b-06141b6603a5",
        "338efd76-134f-11ed-a8e5-06141b6603a5",
        "3c71ea2e-1350-11ed-b06c-06141b6603a5",
        "9cf908a4-1351-11ed-aac7-0630baf9dcb8",
        "09877944-134b-11ed-b2fb-069e4f790264",
        "d8cd7ce8-134c-11ed-9b56-02b0ee514bc7",
        "a1481c3c-134d-11ed-b273-06141b6603a5",
        "1a079e12-134f-11ed-9adc-02ae981fbade",
        "2a4a32ca-1350-11ed-a703-06141b6603a5",
        "83486846-1351-11ed-8c7e-02b0ee514bc7",
        "1231d1aa-137f-11ed-a017-0630baf9dcb8",
        "34fc95c8-134b-11ed-b5ad-069e4f790264",
        "cd57ed6c-134c-11ed-a760-02ae981fbade",
        "9550d9d2-134d-11ed-a1b0-02ae981fbade",
        "10babcc2-134f-11ed-a9f8-022c40d974e6",
        "1a6ed19e-1350-11ed-865f-0630baf9dcb8",
        "72dac710-1351-11ed-9f0e-022c40d974e6",
        "033bf7d4-137f-11ed-870e-0630baf9dcb8",
        "dd79767a-134c-11ed-8f4a-06141b6603a5",
        "a6694510-134d-11ed-941a-069e4f790264",
        "2325bc0e-134f-11ed-83ef-0630baf9dcb8",
        "2df07b32-1350-11ed-9b95-069e4f790264",
        "874914cc-1351-11ed-8159-02ae981fbade",
        "1b8230b0-137f-11ed-b72a-02b0ee514bc7",
        "09a6f8fa-134b-11ed-8090-0630baf9dcb8",
        "deed23c6-134c-11ed-8b80-02b0ee514bc7",
        "a545c4f6-134d-11ed-b9bc-06141b6603a5",
        "21abf2bc-134f-11ed-9a56-06141b6603a5",
        "2d7e6eac-1350-11ed-acad-0630baf9dcb8",
        "86c72e30-1351-11ed-b18b-02ae981fbade",
        "167d54f0-137f-11ed-909b-022c40d974e6",
        "4e719dc8-134b-11ed-81aa-02ae981fbade",
        "10735d20-134d-11ed-a596-02ae981fbade",
        "dbac4092-134d-11ed-b2e3-02b0ee514bc7",
        "58f841b2-134f-11ed-8fd5-022c40d974e6",
        "60e11a10-1350-11ed-8ad0-022c40d974e6",
        "b9e72b1c-1351-11ed-bdc4-06141b6603a5",
        "30df2cf8-134b-11ed-a83c-02ae981fbade",
        "91cb32ae-134c-11ed-9b3a-069e4f790264",
        "04ac3482-134b-11ed-8830-06141b6603a5",
        "d112b4a0-134c-11ed-9aa9-0630baf9dcb8",
        "9b50ac7c-134d-11ed-ae08-069e4f790264",
        "1650096c-134f-11ed-8db1-06141b6603a5",
        "224a163a-1350-11ed-9c02-069e4f790264",
        "7d8f64b8-1351-11ed-b959-02b0ee514bc7",
        "2297693c-1353-11ed-a1cb-02b0ee514bc7",
        "dd04c0f8-1353-11ed-a095-02ae981fbade",
        "82cf85a4-1354-11ed-8ea4-0630baf9dcb8",
        "cf5d84ce-1355-11ed-abdd-06141b6603a5",
        "e16b6e1e-1360-11ed-81fa-02ae981fbade",
        "efd8242c-1367-11ed-876d-02b0ee514bc7",
        "3ab1c51c-1371-11ed-b245-06141b6603a5",
        "5e4aee98-1373-11ed-96fd-069e4f790264",
        "e6ab4468-1373-11ed-8e0e-02ae981fbade",
        "164d69f4-134b-11ed-90cb-02b0ee514bc7",
        "ecb1c9ee-134c-11ed-b934-069e4f790264",
        "b343460a-134d-11ed-9239-069e4f790264",
        "316e6b3a-134f-11ed-82ce-06141b6603a5",
        "39b1bcba-1350-11ed-94cc-0630baf9dcb8",
        "926284b0-1351-11ed-a6e9-06141b6603a5",
        "1c78a460-134b-11ed-90c0-022c40d974e6",
        "c7b1a772-134c-11ed-ba17-02b0ee514bc7",
        "906d11b0-134d-11ed-947f-06141b6603a5",
        "0ad55326-134f-11ed-9bc5-022c40d974e6",
        "2b83a1ee-134b-11ed-ad43-022c40d974e6",
        "c696ccb4-134c-11ed-8af8-02ae981fbade",
        "8ca79bf4-134d-11ed-9b56-06141b6603a5",
        "4b9ba1f2-134b-11ed-b62e-02b0ee514bc7",
        "0715bb06-134d-11ed-bf1e-0630baf9dcb8",
        "d75144c0-134d-11ed-b8df-0630baf9dcb8",
        "54b24b34-134f-11ed-94d8-02b0ee514bc7",
        "5e189d26-1350-11ed-9fba-06141b6603a5",
        "24f669ba-134b-11ed-a2bd-069e4f790264",
        "e55e9154-134c-11ed-88c8-0630baf9dcb8",
        "adb0d2e8-134d-11ed-a475-02ae981fbade",
        "300476e0-134f-11ed-8768-02b0ee514bc7",
        "396480da-1350-11ed-ac28-02b0ee514bc7",
        "90ad0686-1351-11ed-b48c-02b0ee514bc7",
        "38566c8c-1353-11ed-a21c-0630baf9dcb8",
        "9a17def0-1354-11ed-b18d-0630baf9dcb8",
        "e587a518-1355-11ed-808b-02b0ee514bc7",
        "ee3f5e6e-1367-11ed-9beb-022c40d974e6",
        "90d75126-1369-11ed-a5fe-069e4f790264",
        "29186fca-134b-11ed-b505-02ae981fbade",
        "c4f191b4-134c-11ed-8b85-022c40d974e6",
        "2dd9e886-134b-11ed-9705-022c40d974e6",
        "eebd37b4-134c-11ed-b30d-06141b6603a5",
        "b25fc614-134d-11ed-9582-02b0ee514bc7",
        "2d09e448-134f-11ed-9d50-02ae981fbade",
        "3b61bdda-1350-11ed-8982-022c40d974e6",
        "16dcccc2-1353-11ed-b43e-02b0ee514bc7",
        "15712142-134b-11ed-8daf-0630baf9dcb8",
        "de753794-134c-11ed-b8f3-02b0ee514bc7",
        "a65eecb4-134d-11ed-a51c-069e4f790264",
        "20875cd2-134f-11ed-ac13-06141b6603a5",
        "2a467072-1350-11ed-94b2-02ae981fbade",
        "834cd61a-1351-11ed-9ed1-02ae981fbade",
        "28928ed4-1353-11ed-80ac-022c40d974e6",
        "e4d7951c-1353-11ed-9d41-0630baf9dcb8",
        "89671a80-1354-11ed-8dbf-069e4f790264",
        "d5042f54-1355-11ed-8440-0630baf9dcb8",
        "f6a7faf2-1367-11ed-ab87-0630baf9dcb8",
        "91934c14-1369-11ed-b221-0630baf9dcb8",
        "2af43fc2-134b-11ed-a195-0630baf9dcb8",
        "c24573ae-134c-11ed-ae73-02b0ee514bc7",
        "43f3a17a-134b-11ed-8d84-06141b6603a5",
        "c581a28a-134d-11ed-ac44-02ae981fbade",
        "40ddaca2-134f-11ed-9d38-02ae981fbade",
        "4fd721d8-1350-11ed-aa1d-02ae981fbade",
        "a341dd08-1351-11ed-bca7-06141b6603a5",
        "04fb72b8-134b-11ed-8ad3-069e4f790264",
        "cda30fb8-134c-11ed-abb8-06141b6603a5",
        "90679f64-134d-11ed-a11e-06141b6603a5",
        "3013a768-134b-11ed-b75c-022c40d974e6",
        "c05fc494-134d-11ed-8cd0-02b0ee514bc7",
        "3a449c84-134f-11ed-91c5-02b0ee514bc7",
        "41a2e886-1350-11ed-9cfc-0630baf9dcb8",
        "9963992a-1351-11ed-9707-06141b6603a5",
        "197654b0-134b-11ed-a860-069e4f790264",
        "eaf5b412-134c-11ed-88bb-06141b6603a5",
        "b448ca20-134d-11ed-835a-02ae981fbade",
        "31913a3e-134f-11ed-ac61-02ae981fbade",
        "3e3ed97a-1350-11ed-bb19-02b0ee514bc7",
        "9943a8d6-1351-11ed-86d3-02ae981fbade",
        "2fba3e9e-134b-11ed-a7c1-0630baf9dcb8",
        "9f5a6f7c-1354-11ed-befc-0630baf9dcb8",
        "f17c98a0-134c-11ed-9ce6-069e4f790264",
        "b62d8042-134d-11ed-939c-069e4f790264",
        "3cdcc1aa-1350-11ed-bae2-06141b6603a5",
        "3c902e00-1353-11ed-a824-0630baf9dcb8",
        "fa46120c-1353-11ed-8425-02b0ee514bc7",
        "ed1c1f66-1355-11ed-9fd9-0630baf9dcb8",
        "0895ce38-1368-11ed-bcb1-0630baf9dcb8",
        "63a7eb8a-1368-11ed-a297-06141b6603a5",
        "1faeb250-134b-11ed-8e46-022c40d974e6",
        "c9e30eaa-134c-11ed-982a-022c40d974e6",
        "92c105f2-134d-11ed-bda1-022c40d974e6",
        "0ebae5a0-134f-11ed-92e1-0630baf9dcb8",
        "2085b110-1350-11ed-b646-0630baf9dcb8",
        "7e3652dc-1351-11ed-8a0a-02ae981fbade",
        "0f2f4d3e-137f-11ed-b112-02ae981fbade",
        "dc4b1830-134c-11ed-8317-0630baf9dcb8",
        "a798dfea-134d-11ed-bd13-069e4f790264",
        "263773a6-134f-11ed-b042-069e4f790264",
        "2e450fee-1350-11ed-aa26-06141b6603a5",
        "884ec8da-1351-11ed-ba36-06141b6603a5",
        "1983314c-137f-11ed-8816-06141b6603a5",
        "1a485686-134b-11ed-87b1-022c40d974e6",
        "eb5e46ee-134c-11ed-b2e3-022c40d974e6",
        "3004e828-134f-11ed-b604-02b0ee514bc7",
        "3a013556-1350-11ed-8bf0-06141b6603a5",
        "94f02714-1351-11ed-8426-02ae981fbade",
        "1e0d0460-134b-11ed-9792-069e4f790264",
        "c9285c4a-134c-11ed-8e7f-06141b6603a5",
        "93e0f492-134d-11ed-a611-022c40d974e6",
        "0dedcf48-134f-11ed-8397-02ae981fbade",
        "157d8f18-1350-11ed-b08a-0630baf9dcb8",
        "44beaaf0-134b-11ed-acca-0630baf9dcb8",
        "024c45d6-134d-11ed-9aca-02b0ee514bc7",
        "c9719422-134d-11ed-b6d5-0630baf9dcb8",
        "41675038-134f-11ed-905b-022c40d974e6",
        "4d3a765a-1350-11ed-874b-069e4f790264",
        "a4bf4ecc-1351-11ed-a6a2-069e4f790264",
        "3cd6d7e2-1353-11ed-9e0c-02ae981fbade",
        "2fd47e12-134b-11ed-9408-02ae981fbade",
        "f154dc66-134c-11ed-940a-022c40d974e6",
        "3391e7a2-134f-11ed-b8b5-02b0ee514bc7",
        "3e825dd0-1350-11ed-a2ce-0630baf9dcb8",
        "f9ac7e44-1353-11ed-84f5-02ae981fbade",
        "9e5a5434-1354-11ed-9927-069e4f790264",
        "ec566ffa-1355-11ed-a50c-022c40d974e6",
        "089649bc-1368-11ed-98cc-02ae981fbade",
        "6435597a-1368-11ed-ac6f-069e4f790264",
        "5b779b8a-134b-11ed-b98e-02b0ee514bc7",
        "ab955160-134c-11ed-8d66-02ae981fbade",
        "46557f38-134b-11ed-ba82-022c40d974e6",
        "ff75b3ba-134c-11ed-ac32-02ae981fbade",
        "c851cfc6-134d-11ed-b156-06141b6603a5",
        "4313ed7e-134f-11ed-8dbd-0630baf9dcb8",
        "4b68749e-1350-11ed-8d3a-02ae981fbade",
        "9ee14956-1351-11ed-ad62-022c40d974e6",
        "4106dc20-134b-11ed-a66e-02b0ee514bc7",
        "d5b1373e-134c-11ed-8c44-069e4f790264",
        "9fc58e62-134d-11ed-90ce-02ae981fbade",
        "1d77c446-134f-11ed-92ab-06141b6603a5",
        "26af48ee-1350-11ed-bb33-06141b6603a5",
        "83c30254-1351-11ed-95d7-02b0ee514bc7",
        "ecad891a-134c-11ed-a2cf-069e4f790264",
        "b16a27fe-134d-11ed-9cbc-0630baf9dcb8",
        "2eff0f58-134f-11ed-bc22-069e4f790264",
        "3407afb8-1350-11ed-8990-06141b6603a5",
        "8b7a5a38-1351-11ed-b499-0630baf9dcb8",
        "335b5562-1353-11ed-82c8-02ae981fbade",
        "f475b71a-1353-11ed-ba1d-06141b6603a5",
        "9d142b22-1354-11ed-979b-02b0ee514bc7",
        "e664ba2a-1355-11ed-b944-02b0ee514bc7",
        "eeae0334-1360-11ed-821c-02b0ee514bc7",
        "fd2e8bb6-1367-11ed-b008-02ae981fbade",
        "bbaef8bc-136a-11ed-9151-06141b6603a5",
        "2bcf7ea2-134b-11ed-8e40-022c40d974e6",
        "c3e0e22e-134d-11ed-bf84-02ae981fbade",
        "40087622-134f-11ed-b89e-069e4f790264",
        "51b4ea58-1350-11ed-8cea-02b0ee514bc7",
        "a8499408-1351-11ed-a8c3-02ae981fbade",
        "3e4a4d64-134b-11ed-8959-069e4f790264",
        "d31c5f4e-134c-11ed-b667-069e4f790264",
        "9a076efa-134d-11ed-adf1-02b0ee514bc7",
        "14aaabda-134f-11ed-afcf-06141b6603a5",
        "1ee8547a-1350-11ed-84f7-02ae981fbade",
        "7a2d9d3a-1351-11ed-98e9-022c40d974e6",
        "0e2e48fe-137f-11ed-9b6c-069e4f790264",
        "1070e37e-134d-11ed-b460-022c40d974e6",
        "d9d78a06-134d-11ed-b2e0-02ae981fbade",
        "54b50694-134f-11ed-8cd3-06141b6603a5",
        "62b4ac94-1350-11ed-83d7-022c40d974e6",
        "b9b4972e-1351-11ed-9feb-02b0ee514bc7",
        "11b0cbca-134b-11ed-ac07-02b0ee514bc7",
        "df8aa240-134c-11ed-ba40-069e4f790264",
        "ac3e4936-134d-11ed-a76d-069e4f790264",
        "26cf388a-134f-11ed-83db-02ae981fbade",
        "330760c2-1350-11ed-9a36-02b0ee514bc7",
        "8db01e6e-1351-11ed-baff-0630baf9dcb8",
        "370d51b0-1353-11ed-97d0-069e4f790264",
        "f2040860-1353-11ed-b450-06141b6603a5",
        "9653119a-1354-11ed-b92b-06141b6603a5",
        "dfcd09a6-1355-11ed-a0da-02b0ee514bc7",
        "ea442062-1360-11ed-9dc9-02b0ee514bc7",
        "010a5832-1368-11ed-9b87-069e4f790264",
        "48816616-1371-11ed-a581-02b0ee514bc7",
        "69777eda-1373-11ed-b587-02ae981fbade",
        "f1e29232-1373-11ed-822e-022c40d974e6",
        "2a2d8a52-1374-11ed-9228-069e4f790264",
        "e1077fc6-1374-11ed-ae60-069e4f790264",
        "16442ff4-134d-11ed-b36c-02b0ee514bc7",
        "582c1786-134f-11ed-8cae-022c40d974e6",
        "641388d0-1350-11ed-addb-022c40d974e6",
        "bce71ed0-1351-11ed-a0d6-02b0ee514bc7",
        "4ce5491e-134b-11ed-9802-06141b6603a5",
        "0ee6f4bc-134d-11ed-ab90-06141b6603a5",
        "d47989b0-134d-11ed-8f40-06141b6603a5",
        "514fed20-134f-11ed-ae04-022c40d974e6",
        "581d914c-1350-11ed-98ca-02ae981fbade",
        "b25bb764-1351-11ed-affa-022c40d974e6",
        "eda53994-134c-11ed-85f3-0630baf9dcb8",
        "b35dd24a-134d-11ed-a581-02b0ee514bc7",
        "3221213a-134f-11ed-9c7d-06141b6603a5",
        "3f812d92-1350-11ed-be00-022c40d974e6",
        "9ad0cf76-1351-11ed-8ef0-06141b6603a5",
        "0b9b1414-134d-11ed-8dcf-0630baf9dcb8",
        "d1ebc244-134d-11ed-93d5-069e4f790264",
        "4d231a2e-134f-11ed-a3f3-022c40d974e6",
        "564db22a-1350-11ed-baf7-02b0ee514bc7",
        "b09e24ca-1351-11ed-a784-0630baf9dcb8",
        "1a41da0e-134b-11ed-a346-02ae981fbade",
        "e204eaf8-134c-11ed-8fa0-02ae981fbade",
        "ac40bbe4-134d-11ed-8724-06141b6603a5",
        "2a6e88ce-134f-11ed-91d4-0630baf9dcb8",
        "2ff629b8-1350-11ed-94b6-022c40d974e6",
        "86773312-1351-11ed-841b-0630baf9dcb8",
        "2d3c9fe2-1353-11ed-b0e3-0630baf9dcb8",
        "ee616f90-1353-11ed-99ee-02b0ee514bc7",
        "93f64994-1354-11ed-9018-02ae981fbade",
        "eb403dc6-1355-11ed-9aaf-069e4f790264",
        "ee7175b6-1367-11ed-ac28-02ae981fbade",
        "37db74d2-1371-11ed-8c96-02ae981fbade",
        "5c4fafe8-1373-11ed-97d0-02b0ee514bc7",
        "d955814c-134c-11ed-9664-022c40d974e6",
        "a5ef5f0c-134d-11ed-8f36-02b0ee514bc7",
        "23ea4b14-134f-11ed-be1a-02ae981fbade",
        "2a3dc1a2-1350-11ed-8123-0630baf9dcb8",
        "82466e16-1351-11ed-a059-0630baf9dcb8",
        "13f28fde-137f-11ed-b3cf-0630baf9dcb8",
        "f6f2fa4a-134c-11ed-9afe-069e4f790264",
        "36fefc18-134f-11ed-bbc9-022c40d974e6",
        "459a3836-1350-11ed-8719-022c40d974e6",
        "ab28af92-1351-11ed-b943-02ae981fbade",
        "cfa31f24-134c-11ed-a094-022c40d974e6",
        "974b3b88-134d-11ed-ae14-02b0ee514bc7",
        "123bc9d8-134f-11ed-8ee9-06141b6603a5",
        "1a3e0ece-1350-11ed-b2da-02b0ee514bc7",
        "70b9b68a-1351-11ed-8235-0630baf9dcb8",
        "049cfc54-137f-11ed-8de7-0630baf9dcb8",
        "47cd06ba-134b-11ed-b5c6-022c40d974e6",
        "cac695a2-134d-11ed-a404-022c40d974e6",
        "4a1e41be-134f-11ed-8994-022c40d974e6",
        "a6543b94-1351-11ed-bcae-0630baf9dcb8",
        "5115c504-134b-11ed-9678-069e4f790264",
        "0cbc4aa2-134d-11ed-b4f4-069e4f790264",
        "d5accfc2-134d-11ed-b230-06141b6603a5",
        "4e6a6464-134f-11ed-ad64-069e4f790264",
        "57d6c8ca-1350-11ed-9626-02b0ee514bc7",
        "b21b690c-1351-11ed-a3d1-069e4f790264",
        "13276838-134b-11ed-bea0-0630baf9dcb8",
        "dd6a7d1e-134c-11ed-8268-022c40d974e6",
        "a3186e36-134d-11ed-b5bd-022c40d974e6",
        "2337ef46-134f-11ed-aaac-0630baf9dcb8",
        "2c7caed8-1350-11ed-9164-02ae981fbade",
        "8853b3cc-1351-11ed-ad7b-06141b6603a5",
        "2de3f0b2-1353-11ed-aa90-02ae981fbade",
        "eb3394c4-1353-11ed-9eef-069e4f790264",
        "8fb9de22-1354-11ed-a0e6-02ae981fbade",
        "dac3c86e-1355-11ed-b52f-06141b6603a5",
        "ebd82c7a-1360-11ed-a781-02b0ee514bc7",
        "026b8a20-1368-11ed-a570-022c40d974e6",
        "47374960-1371-11ed-b3d3-069e4f790264",
        "671a379a-1373-11ed-a63d-022c40d974e6",
        "ee4478ac-1373-11ed-ad95-06141b6603a5",
        "23216cb0-1374-11ed-a4a4-02ae981fbade",
        "2b623e28-134b-11ed-ba38-02b0ee514bc7",
        "c6926d90-134c-11ed-ad3b-0630baf9dcb8",
        "8cda482e-134d-11ed-aa41-0630baf9dcb8",
        "2e56a772-134b-11ed-bf3b-06141b6603a5",
        "f56f0d08-134c-11ed-aeb2-02b0ee514bc7",
        "bf33a36a-134d-11ed-8cd0-02b0ee514bc7",
        "3d6e9e28-134f-11ed-b699-06141b6603a5",
        "4fe4103c-1350-11ed-94f0-069e4f790264",
        "a6ca1d50-1351-11ed-b5b2-02b0ee514bc7",
        "43feddd8-134b-11ed-8cc7-06141b6603a5",
        "fd74a8c8-134c-11ed-9b97-02ae981fbade",
        "c5aaa176-134d-11ed-a2d2-06141b6603a5",
        "4341a89a-134f-11ed-9b7f-069e4f790264",
        "49cd60a4-1350-11ed-8af3-069e4f790264",
        "3953e81a-134b-11ed-af20-02ae981fbade",
        "f8ee7342-134c-11ed-9d2e-0630baf9dcb8",
        "bec9758a-134d-11ed-b124-02ae981fbade",
        "3bd21432-134f-11ed-ac38-02b0ee514bc7",
        "4fd6619e-1350-11ed-9425-02b0ee514bc7",
        "9ca4823e-1351-11ed-a5a5-02b0ee514bc7",
        "0582b0f2-134b-11ed-82cb-0630baf9dcb8",
        "ce264306-134c-11ed-9358-069e4f790264",
        "918dbcd4-134d-11ed-9d87-02ae981fbade",
        "0fe07882-134f-11ed-bd11-022c40d974e6",
        "1cf57e18-1350-11ed-a1bc-022c40d974e6",
        "760eea60-1351-11ed-9c9e-06141b6603a5",
        "1bfe9244-1353-11ed-ae05-0630baf9dcb8",
        "dbe8c0de-1353-11ed-ae97-06141b6603a5",
        "3d0c4510-134b-11ed-9fbd-02ae981fbade",
        "d3bf1f7c-134c-11ed-8518-069e4f790264",
        "9b0fc25c-134d-11ed-87d5-02ae981fbade",
        "13f7a616-134f-11ed-897f-0630baf9dcb8",
        "1d9f9eb6-1350-11ed-8e82-06141b6603a5",
        "767da9be-1351-11ed-b67e-06141b6603a5",
        "095f0174-137f-11ed-845e-022c40d974e6",
        "1dd65c6c-134b-11ed-bb45-022c40d974e6",
        "c998e514-134c-11ed-b8b9-0630baf9dcb8",
        "957b507c-134d-11ed-a818-02ae981fbade",
        "10cda10c-134f-11ed-81e5-0630baf9dcb8",
        "1cbd9e76-1350-11ed-ab88-069e4f790264",
        "7ad33c4a-1351-11ed-8277-02b0ee514bc7",
        "20ea541e-1353-11ed-b6a5-069e4f790264",
        "dfa749de-1353-11ed-a789-02b0ee514bc7",
        "89ce4e3a-1354-11ed-988b-022c40d974e6",
        "d7ffd096-1355-11ed-94f8-022c40d974e6",
        "e65c0fbe-1360-11ed-bbda-0630baf9dcb8",
        "f717e1e6-1367-11ed-8f1b-06141b6603a5",
        "4142df4c-1371-11ed-9291-022c40d974e6",
        "64984aa2-1373-11ed-bcc4-069e4f790264",
        "eb5eccf0-1373-11ed-b7fe-02b0ee514bc7",
        "24e1f8a8-1374-11ed-b531-0630baf9dcb8",
        "084dd9d8-134b-11ed-9725-02b0ee514bc7",
        "d4ec0a90-134c-11ed-be6d-069e4f790264",
        "9f84a140-134d-11ed-8da0-022c40d974e6",
        "1937ab1c-134f-11ed-aee7-0630baf9dcb8",
        "2081c348-1350-11ed-b4e8-02b0ee514bc7",
        "7f5aba86-1351-11ed-ad37-0630baf9dcb8",
        "25fa8ece-1353-11ed-b0f3-06141b6603a5",
        "e331662a-1353-11ed-9e29-069e4f790264",
        "8625fa76-1354-11ed-a7ec-069e4f790264",
        "d1edabe2-1355-11ed-91e8-06141b6603a5",
        "e2a852b0-1360-11ed-90c1-022c40d974e6",
        "f5c57cfe-1367-11ed-b883-0630baf9dcb8",
        "4009fe94-1371-11ed-b6ab-022c40d974e6",
        "6289b7b4-1373-11ed-99d9-069e4f790264",
        "ef079012-1373-11ed-bbdd-06141b6603a5",
        "269c2ef2-1374-11ed-93d6-0630baf9dcb8",
        "e400f2ca-1374-11ed-91ec-06141b6603a5",
        "d9c5d582-134c-11ed-9063-02ae981fbade",
        "a566c048-134d-11ed-b48a-069e4f790264",
        "21dd81e2-134f-11ed-b556-02b0ee514bc7",
        "2d1e4d9c-1350-11ed-bee9-0630baf9dcb8",
        "8af98052-1351-11ed-b13e-02b0ee514bc7",
        "f77331e0-1353-11ed-bc9b-06141b6603a5",
        "9b974b08-1354-11ed-83d9-069e4f790264",
        "e0b114fc-1355-11ed-a5f9-02ae981fbade",
        "f38a5d4e-1360-11ed-a817-069e4f790264",
        "07d6486a-1368-11ed-b02e-02b0ee514bc7",
        "4b38b4ea-1371-11ed-b822-02b0ee514bc7",
        "6dc82af2-1373-11ed-9379-0630baf9dcb8",
        "efc4e694-1373-11ed-b028-02ae981fbade",
        "26e8490e-1374-11ed-a969-069e4f790264",
        "e528fada-1374-11ed-9d39-02b0ee514bc7",
        "380dce76-134b-11ed-99ef-069e4f790264",
        "c1d6eeba-134d-11ed-aeb0-0630baf9dcb8",
        "3df3a280-134f-11ed-80c8-0630baf9dcb8",
        "47d4f1f4-1350-11ed-9d04-069e4f790264",
        "a46a65e2-1351-11ed-aa06-0630baf9dcb8",
        "1a20d70a-134b-11ed-b96c-02ae981fbade",
        "e196f516-134c-11ed-97af-02ae981fbade",
        "a795e556-134d-11ed-bbe3-069e4f790264",
        "217a0d24-134f-11ed-9599-02b0ee514bc7",
        "31bf353c-1350-11ed-ac99-02ae981fbade",
        "8f67a218-1351-11ed-8caf-0630baf9dcb8",
        "398efd62-1353-11ed-94ea-06141b6603a5",
        "f8d5dd44-1353-11ed-b534-022c40d974e6",
        "9d1907fa-1354-11ed-9d53-069e4f790264",
        "e49c4fc8-1355-11ed-836d-022c40d974e6",
        "f20d84c8-1360-11ed-a47e-06141b6603a5",
        "0570500c-1368-11ed-9c91-02ae981fbade",
        "4a8ddbd8-1371-11ed-8366-02ae981fbade",
        "6c1b2c04-1373-11ed-87a5-02ae981fbade",
        "f328d598-1373-11ed-bd0b-06141b6603a5",
        "2af1c110-1374-11ed-b42e-0630baf9dcb8",
        "e51d5e6e-1374-11ed-b2b4-06141b6603a5",
        "056b2234-134b-11ed-b7f5-02ae981fbade",
        "d6bd4eba-134c-11ed-b543-022c40d974e6",
        "a09d7b6a-134d-11ed-aaad-069e4f790264",
        "1d948ac2-134f-11ed-817c-06141b6603a5",
        "27098098-1350-11ed-beaa-069e4f790264",
        "8050c804-1351-11ed-a12e-0630baf9dcb8",
        "12c4823e-137f-11ed-b0ea-069e4f790264",
        "11f7a9da-134d-11ed-931d-02ae981fbade",
        "de1efda6-134d-11ed-9b1b-022c40d974e6",
        "528f0f9a-134f-11ed-ab0a-02ae981fbade",
        "5a4b6930-1350-11ed-aea1-02b0ee514bc7",
        "b7c27850-1351-11ed-bad8-02ae981fbade",
        "16f37ec0-134b-11ed-b1f8-02ae981fbade",
        "e3a86f9c-134c-11ed-bfd1-02ae981fbade",
        "a9ea722c-134d-11ed-91de-06141b6603a5",
        "25b7d9fc-134f-11ed-a415-06141b6603a5",
        "2ec11468-1350-11ed-9652-069e4f790264",
        "875ef4fe-1351-11ed-891b-022c40d974e6",
        "310bc8f0-1353-11ed-9c93-069e4f790264",
        "eda7f6f0-1353-11ed-9827-0630baf9dcb8",
        "938d6172-1354-11ed-9bea-069e4f790264",
        "eb3809a8-1355-11ed-a8bd-02b0ee514bc7",
        "ef32fdc6-1367-11ed-8443-022c40d974e6",
        "3b5a8b48-1371-11ed-a820-02ae981fbade",
        "5ed71e40-1373-11ed-914f-022c40d974e6",
        "e7dfa126-1373-11ed-85de-02b0ee514bc7",
        "58cb7c6c-134b-11ed-8a97-02b0ee514bc7",
        "171f8392-134d-11ed-8aa7-06141b6603a5",
        "de788862-134d-11ed-8b8e-02b0ee514bc7",
        "5a491172-134f-11ed-a8f3-02ae981fbade",
        "bf78df30-1351-11ed-9ceb-06141b6603a5",
        "cbc0eef4-134c-11ed-8ecf-022c40d974e6",
        "92fb7fca-134d-11ed-b5a6-02b0ee514bc7",
        "0f58052e-134f-11ed-935c-022c40d974e6",
        "1e9388c8-1350-11ed-bdb3-022c40d974e6",
        "79cf62ce-1351-11ed-a0ba-022c40d974e6",
        "25f5829e-1353-11ed-adb9-06141b6603a5",
        "e2456176-1353-11ed-b6bc-069e4f790264",
        "8425c5a8-1354-11ed-b91f-022c40d974e6",
        "d6433806-1355-11ed-809a-02ae981fbade",
        "e512db92-1360-11ed-bcbf-02b0ee514bc7",
        "00330828-1368-11ed-b0fd-022c40d974e6",
        "447acbe8-1371-11ed-a649-06141b6603a5",
        "6a625702-1373-11ed-9fc4-02ae981fbade",
        "f48a865c-1373-11ed-9aeb-022c40d974e6",
        "2c563b80-1374-11ed-bc6b-06141b6603a5",
        "e78779be-1374-11ed-bfc0-069e4f790264",
        "024dcb4a-134d-11ed-91e8-022c40d974e6",
        "cd69603c-134d-11ed-8d9b-02ae981fbade",
        "48837f54-134f-11ed-9e93-0630baf9dcb8",
        "5656f056-1350-11ed-af0b-022c40d974e6",
        "b5e10056-1351-11ed-8dae-02ae981fbade",
        "2961155e-134b-11ed-b000-02b0ee514bc7",
        "c21e894c-134c-11ed-a70f-0630baf9dcb8",
        "1258e4f4-134b-11ed-8ec6-06141b6603a5",
        "dd700eaa-134c-11ed-83ff-069e4f790264",
        "a40c50a0-134d-11ed-897e-02b0ee514bc7",
        "223bc9b4-134f-11ed-9f67-022c40d974e6",
        "2ca3f808-1350-11ed-9afd-02ae981fbade",
        "88cb3226-1351-11ed-a8ff-0630baf9dcb8",
        "31d67f8c-1353-11ed-9c6c-02ae981fbade",
        "f3f40814-1353-11ed-a42d-069e4f790264",
        "98b77bc4-1354-11ed-9406-02b0ee514bc7",
        "e2b793fc-1355-11ed-b7f1-02ae981fbade",
        "f0ce4ae8-1360-11ed-96dc-0630baf9dcb8",
        "fec28874-1367-11ed-9402-0630baf9dcb8",
        "4798f886-1371-11ed-9dc0-0630baf9dcb8",
        "675a17c0-1373-11ed-b267-069e4f790264",
        "e9950862-1373-11ed-8889-0630baf9dcb8",
        "20c5b818-1374-11ed-93f4-022c40d974e6",
        "37ebcd94-134b-11ed-93f0-02ae981fbade",
        "cf364ebc-134c-11ed-ac6f-022c40d974e6",
        "9bde6f94-134d-11ed-bfb6-06141b6603a5",
        "1966f336-134f-11ed-978f-069e4f790264",
        "21fd2a32-1350-11ed-be15-06141b6603a5",
        "7ce7a692-1351-11ed-b030-02b0ee514bc7",
        "0dda2dbe-137f-11ed-a2bd-069e4f790264",
        "29203174-134b-11ed-a4d5-02b0ee514bc7",
        "c3152676-134c-11ed-85a3-06141b6603a5",
        "1078b024-134b-11ed-a08e-022c40d974e6",
        "e23197a6-134c-11ed-832b-02ae981fbade",
        "a9f734da-134d-11ed-8a0d-0630baf9dcb8",
        "24ca556a-134f-11ed-8d67-0630baf9dcb8",
        "30acd546-1350-11ed-bcbf-069e4f790264",
        "8637286c-1351-11ed-8bd1-02ae981fbade",
        "1ac20e48-137f-11ed-8027-02ae981fbade",
        "24116874-134b-11ed-8419-0630baf9dcb8",
        "e7203074-134c-11ed-ba5a-02ae981fbade",
        "b0e77430-134d-11ed-a690-022c40d974e6",
        "317953ce-134f-11ed-a073-022c40d974e6",
        "3b7a4b2a-1350-11ed-8e27-02b0ee514bc7",
        "92622da8-1351-11ed-94fd-06141b6603a5",
        "37111d72-1353-11ed-84cf-0630baf9dcb8",
        "f1aa9870-1353-11ed-b78e-02ae981fbade",
        "94c30b82-1354-11ed-a05e-069e4f790264",
        "e0055e8c-1355-11ed-9cd3-06141b6603a5",
        "eb2c0b2a-1360-11ed-b469-02ae981fbade",
        "034980fa-1368-11ed-82de-06141b6603a5",
        "90eca760-1369-11ed-9c4d-02ae981fbade",
        "22477038-134b-11ed-af7b-06141b6603a5",
        "ec157058-134c-11ed-8a78-069e4f790264",
        "b5802816-134d-11ed-bc1a-06141b6603a5",
        "317c9d5e-134f-11ed-994b-0630baf9dcb8",
        "3b6f6f70-1350-11ed-a125-02b0ee514bc7",
        "172af79e-1353-11ed-abc2-069e4f790264",
        "32eb0468-134b-11ed-afc2-0630baf9dcb8",
        "d245797a-134c-11ed-b3a3-069e4f790264",
        "994ae30c-134d-11ed-8ab5-069e4f790264",
        "172fe582-134f-11ed-83c5-06141b6603a5",
        "2031ce2e-1350-11ed-b895-06141b6603a5",
        "798c6578-1351-11ed-ad65-022c40d974e6",
        "0d6701b8-137f-11ed-aab3-069e4f790264",
        "1177dd1a-134b-11ed-b046-02b0ee514bc7",
        "d8e22d3c-134c-11ed-80d7-02ae981fbade",
        "9f694922-134d-11ed-997c-022c40d974e6",
        "1aeea14a-134f-11ed-aca1-02ae981fbade",
        "27613c66-1350-11ed-8e56-06141b6603a5",
        "817b1874-1351-11ed-a2ac-0630baf9dcb8",
        "27530ca6-1353-11ed-a648-069e4f790264",
        "e55dc84e-1353-11ed-bd41-02ae981fbade",
        "8a427ecc-1354-11ed-97ff-0630baf9dcb8",
        "d81ec488-1355-11ed-bbb2-069e4f790264",
        "e726fcce-1360-11ed-ba0f-069e4f790264",
        "f88b2998-1367-11ed-b46a-06141b6603a5",
        "3cfeca68-1371-11ed-b585-022c40d974e6",
        "5d993202-1373-11ed-8b64-022c40d974e6",
        "e4ca25d8-1373-11ed-be7b-069e4f790264",
        "2f2f70de-134b-11ed-a98e-02b0ee514bc7",
        "914884ee-134c-11ed-ac58-02ae981fbade",
        "cb83e8b0-134c-11ed-b906-02b0ee514bc7",
        "9284dd84-134d-11ed-bc57-02ae981fbade",
        "0e8c024e-134f-11ed-8bc0-02ae981fbade",
        "17e0e08e-1350-11ed-be40-0630baf9dcb8",
        "71955348-1351-11ed-bbc9-0630baf9dcb8",
        "04ed61da-137f-11ed-b070-02b0ee514bc7",
        "1874b70a-134b-11ed-ab38-02ae981fbade",
        "a74524a4-134d-11ed-8f2b-02ae981fbade",
        "211003ac-134f-11ed-9bf7-069e4f790264",
        "2b81bb54-1350-11ed-b1d0-06141b6603a5",
        "8957b412-1351-11ed-b044-02ae981fbade",
        "2fb34f46-1353-11ed-9bf7-0630baf9dcb8",
        "e816847c-1353-11ed-a651-022c40d974e6",
        "8ae640fc-1354-11ed-bb2e-0630baf9dcb8",
        "dc62df70-1355-11ed-8ae4-02ae981fbade",
        "e90670f6-1360-11ed-ba07-022c40d974e6",
        "fbab75c4-1367-11ed-ab07-069e4f790264",
        "41e81868-1371-11ed-b7a6-022c40d974e6",
        "61a1993e-1373-11ed-ab1f-06141b6603a5",
        "ea71f4d4-1373-11ed-bd7f-02ae981fbade",
        "291dd8d8-1374-11ed-9ffe-0630baf9dcb8",
        "e5ad0348-1374-11ed-bb83-069e4f790264",
        "3a298ed4-134b-11ed-b747-022c40d974e6",
        "fb6364ac-134c-11ed-8f53-022c40d974e6",
        "c78cda68-134d-11ed-87fe-022c40d974e6",
        "4341fe1c-134f-11ed-8c3d-0630baf9dcb8",
        "4ace105c-1350-11ed-8ed1-02b0ee514bc7",
        "a88ac4c8-1351-11ed-a8b6-06141b6603a5",
        "c54aadbc-134c-11ed-b1b1-02ae981fbade",
        "8ea503f6-134d-11ed-bd58-06141b6603a5",
        "12b16788-134f-11ed-b86c-069e4f790264",
        "1b18827a-1350-11ed-b7c9-0630baf9dcb8",
        "77a0513e-1351-11ed-8d67-0630baf9dcb8",
        "1cc83400-1353-11ed-99f8-022c40d974e6",
        "1982556a-134d-11ed-ba2d-02b0ee514bc7",
        "e1c537b8-134d-11ed-a669-06141b6603a5",
        "5d9a2640-134f-11ed-a853-0630baf9dcb8",
        "6947a890-1350-11ed-9570-02b0ee514bc7",
        "c22d671e-1351-11ed-8895-069e4f790264",
        "394b1d0c-134b-11ed-9af5-02b0ee514bc7",
        "fd4df3a4-134c-11ed-b45d-069e4f790264",
        "caeffd16-134d-11ed-af7c-06141b6603a5",
        "475d1e00-134f-11ed-9b93-069e4f790264",
        "52825b00-1350-11ed-84f5-06141b6603a5",
        "a334800e-1351-11ed-8b8e-02ae981fbade",
        "e789e50a-134c-11ed-82e3-02ae981fbade",
        "afa1c95e-134d-11ed-9d0f-02b0ee514bc7",
        "2fe931a0-134f-11ed-8f8b-02ae981fbade",
        "39593676-1350-11ed-9013-022c40d974e6",
        "92fe0d2c-1351-11ed-aa23-02ae981fbade",
        "37398bae-1353-11ed-9af9-069e4f790264",
        "f287c3b2-1353-11ed-a329-02ae981fbade",
        "9271a91a-1354-11ed-b83a-022c40d974e6",
        "e8d05846-1355-11ed-869c-069e4f790264",
        "ec30da12-1367-11ed-a670-0630baf9dcb8",
        "3717b826-1371-11ed-8c67-069e4f790264",
        "5b01600a-1373-11ed-a675-069e4f790264",
        "1fbb9dc6-134b-11ed-8f13-02b0ee514bc7",
        "ca00ffe6-134c-11ed-9328-06141b6603a5",
        "94259980-134d-11ed-abab-022c40d974e6",
        "0eaa6310-134f-11ed-a9e9-069e4f790264",
        "17d45008-1350-11ed-ad3c-06141b6603a5",
        "72012280-1351-11ed-be82-02b0ee514bc7",
        "19602278-1353-11ed-aac7-02b0ee514bc7",
        "1db7801c-134b-11ed-a33a-02ae981fbade",
        "c82aae24-134c-11ed-b77e-069e4f790264",
        "8fee85ac-134d-11ed-a120-022c40d974e6",
        "0de3b38c-134f-11ed-ab89-022c40d974e6",
        "14ea1fda-1350-11ed-99a7-022c40d974e6",
        "14b91960-134d-11ed-b829-02b0ee514bc7",
        "d8c640b2-134d-11ed-b86f-06141b6603a5",
        "54afb4d2-134f-11ed-a608-069e4f790264",
        "5f6639fe-1350-11ed-8054-02ae981fbade",
        "b7ddcb3c-1351-11ed-9285-069e4f790264",
        "24ac71c0-134b-11ed-92e6-069e4f790264",
        "eb764546-134c-11ed-94b6-06141b6603a5",
        "b4479d6c-134d-11ed-9001-02ae981fbade",
        "31618776-134f-11ed-a8f4-06141b6603a5",
        "37ff189a-1350-11ed-addb-0630baf9dcb8",
        "940c3c8e-1351-11ed-8093-02ae981fbade",
        "38f3f07e-1353-11ed-b8a6-022c40d974e6",
        "f847ed22-1353-11ed-a2bf-06141b6603a5",
        "9c51eae4-1354-11ed-a65e-06141b6603a5",
        "e22929c8-1355-11ed-92c9-02b0ee514bc7",
        "f0a48fa0-1360-11ed-86ae-02b0ee514bc7",
        "0444b592-1368-11ed-9a13-06141b6603a5",
        "11c840ca-1369-11ed-adcd-0630baf9dcb8",
        "0e883a46-134b-11ed-ada9-0630baf9dcb8",
        "dac5e990-134c-11ed-80a1-02ae981fbade",
        "a30feb3a-134d-11ed-8b67-0630baf9dcb8",
        "1f3d800e-134f-11ed-af42-02b0ee514bc7",
        "24d3fa1a-1350-11ed-a688-06141b6603a5",
        "7ccc6f3a-1351-11ed-970d-02ae981fbade",
        "2435bd16-1353-11ed-90a9-02ae981fbade",
        "e153ac28-1353-11ed-9aec-02ae981fbade",
        "873e7dde-1354-11ed-8a9b-06141b6603a5",
        "d0abbab2-1355-11ed-b900-0630baf9dcb8",
        "e2495cf6-1360-11ed-9f2d-06141b6603a5",
        "f550fdfc-1367-11ed-82b7-069e4f790264",
        "3f671940-1371-11ed-98e7-02ae981fbade",
        "60a2951a-1373-11ed-bf0c-022c40d974e6",
        "e5698a7e-1373-11ed-9141-02b0ee514bc7",
        "06344380-134b-11ed-b590-02b0ee514bc7",
        "d7f29ac4-134c-11ed-a4d8-02b0ee514bc7",
        "9e32a08a-134d-11ed-92fe-06141b6603a5",
        "1afd861a-134f-11ed-a693-02b0ee514bc7",
        "2573f556-1350-11ed-a2c0-0630baf9dcb8",
        "7f638760-1351-11ed-a70c-02b0ee514bc7",
        "11c01092-137f-11ed-8d4d-022c40d974e6",
        "146e637c-134b-11ed-a9df-022c40d974e6",
        "e6806648-134c-11ed-a68b-069e4f790264",
        "ada2e64c-134d-11ed-ad9a-022c40d974e6",
        "2edd3c20-134f-11ed-bd0b-02b0ee514bc7",
        "331b0f96-1350-11ed-8344-06141b6603a5",
        "8f5820fe-1351-11ed-bc79-02ae981fbade",
        "23326a0c-134b-11ed-a8f6-02ae981fbade",
        "b871c692-134d-11ed-b1b6-069e4f790264",
        "31b4d426-134f-11ed-a8dc-02b0ee514bc7",
        "3ee7ce04-1350-11ed-87cb-022c40d974e6",
        "9ae5958c-1351-11ed-a29f-022c40d974e6",
        "d27f655e-134c-11ed-9a43-06141b6603a5",
        "97dd5e8c-134d-11ed-aa9c-069e4f790264",
        "14ab319a-134f-11ed-a663-02b0ee514bc7",
        "1e17fad2-1350-11ed-95fa-022c40d974e6",
        "760fdf88-1351-11ed-9cd0-022c40d974e6",
        "1efc3ef6-1353-11ed-8b96-02b0ee514bc7",
        "de9b1f0c-1353-11ed-ab50-022c40d974e6",
        "84e21424-1354-11ed-a293-069e4f790264",
        "d3c90f6a-1355-11ed-869d-022c40d974e6",
        "e24792b8-1360-11ed-a4be-02b0ee514bc7",
        "f4e6006a-1367-11ed-afcc-022c40d974e6",
        "3ccfbf84-1371-11ed-9a57-069e4f790264",
        "5dac4626-1373-11ed-8787-069e4f790264",
        "e43503cc-1373-11ed-8b22-022c40d974e6",
        "fae8f168-134c-11ed-a655-0630baf9dcb8",
        "c2ecb442-134d-11ed-b70e-069e4f790264",
        "3d8ee4da-134f-11ed-aba5-069e4f790264",
        "50809bd2-1350-11ed-b887-02b0ee514bc7",
        "aad49858-1351-11ed-a043-069e4f790264",
        "5d4abdb6-134b-11ed-a686-022c40d974e6",
        "1979c102-134d-11ed-a844-02b0ee514bc7",
        "e1f3d6f4-134d-11ed-87f3-022c40d974e6",
        "5c8056d0-134f-11ed-aae2-0630baf9dcb8",
        "687014de-1350-11ed-8b65-069e4f790264",
        "c3529114-1351-11ed-86cc-0630baf9dcb8",
        "c4e5d0f4-134c-11ed-8ebb-022c40d974e6",
        "9093a73a-134d-11ed-b69d-02b0ee514bc7",
        "0c75d462-134f-11ed-b5ae-069e4f790264",
        "178ba8d0-1350-11ed-be7b-02b0ee514bc7",
        "7198e13e-1351-11ed-a180-069e4f790264",
        "05dc304e-137f-11ed-95ca-022c40d974e6",
        "25d01494-134b-11ed-b202-02ae981fbade",
        "e9d96c90-134c-11ed-9feb-02b0ee514bc7",
        "ae6dff62-134d-11ed-bb13-06141b6603a5",
        "2b5b4e20-134f-11ed-be7a-02b0ee514bc7",
        "37be0850-1350-11ed-bb11-02b0ee514bc7",
        "93c6c582-1351-11ed-9371-02ae981fbade",
        "3858c040-1353-11ed-b139-06141b6603a5",
        "f5660abc-1353-11ed-bd3b-02b0ee514bc7",
        "99b493d6-1354-11ed-8167-06141b6603a5",
        "e6c96d44-1355-11ed-998a-06141b6603a5",
        "efd13d94-1360-11ed-9d60-069e4f790264",
        "fdc5f6e0-1367-11ed-ab4b-022c40d974e6",
        "453bc370-1371-11ed-935c-069e4f790264",
        "672ae66c-1373-11ed-95b4-022c40d974e6",
        "e8a1b05e-1373-11ed-baf3-02b0ee514bc7",
        "58d9c22c-134b-11ed-b003-02ae981fbade",
        "17bff642-134d-11ed-ba23-069e4f790264",
        "5c593438-134f-11ed-837b-06141b6603a5",
        "6656358e-1350-11ed-9f96-022c40d974e6",
        "c07ad73a-1351-11ed-8760-02ae981fbade",
        "c8ba0740-134c-11ed-9568-069e4f790264",
        "8f4c2f14-134d-11ed-ba47-022c40d974e6",
        "0cc45ace-134f-11ed-88f3-022c40d974e6",
        "185864d8-1350-11ed-80a7-069e4f790264",
        "72e846d8-1351-11ed-9a75-06141b6603a5",
        "184e7ace-1353-11ed-af8c-06141b6603a5",
        "08023dde-134b-11ed-bfe5-069e4f790264",
        "db1caad2-134c-11ed-bb8d-02b0ee514bc7",
        "a31b33b4-134d-11ed-bb0c-02ae981fbade",
        "1e5d0f60-134f-11ed-bfa1-022c40d974e6",
        "28661212-1350-11ed-b9fd-022c40d974e6",
        "80de5d0e-1351-11ed-9e4e-02ae981fbade",
        "1523fec4-137f-11ed-84ad-02b0ee514bc7",
        "410929c6-134b-11ed-81f8-022c40d974e6",
        "d55b83e8-134c-11ed-be75-02b0ee514bc7",
        "9d803fc6-134d-11ed-b60a-06141b6603a5",
        "1b38223e-134f-11ed-9e5b-02b0ee514bc7",
        "26154d5c-1350-11ed-8bf5-02ae981fbade",
        "84f0d368-1351-11ed-abf6-02ae981fbade",
        "1abeaf0a-137f-11ed-9ea9-06141b6603a5",
        "4ef5474a-134b-11ed-bee1-022c40d974e6",
        "13269302-134d-11ed-bcf6-069e4f790264",
        "de6548d8-134d-11ed-a9e8-02ae981fbade",
        "6491e068-1350-11ed-acd9-02ae981fbade",
        "bf7ab0e4-1351-11ed-aa1f-0630baf9dcb8",
        "06970b3c-134b-11ed-acc1-06141b6603a5",
        "d068db10-134c-11ed-9aca-069e4f790264",
        "9a160d5c-134d-11ed-80de-06141b6603a5",
        "159d3f76-134f-11ed-8aa4-02ae981fbade",
        "23c1d084-1350-11ed-8228-022c40d974e6",
        "7f9f3760-1351-11ed-9ebe-06141b6603a5",
        "289563ac-1353-11ed-ae82-069e4f790264",
        "e3f6f3e0-1353-11ed-bac1-0630baf9dcb8",
        "858b9fc6-1354-11ed-88b2-0630baf9dcb8",
        "d44e42a2-1355-11ed-8327-02ae981fbade",
        "e3cee406-1360-11ed-aeda-02b0ee514bc7",
        "f90eee5e-1367-11ed-9fa1-0630baf9dcb8",
        "3d90f000-1371-11ed-8988-02ae981fbade",
        "6394ebc4-1373-11ed-a7c0-069e4f790264",
        "ee83bb98-1373-11ed-9d91-0630baf9dcb8",
        "23b607f8-1374-11ed-a58f-02ae981fbade",
        "2adfe8b0-134b-11ed-ac87-069e4f790264",
        "c3873df6-134c-11ed-950b-069e4f790264",
        "22e4dada-134b-11ed-b163-022c40d974e6",
        "f5bc2598-134c-11ed-835f-06141b6603a5",
        "c067d3b4-134d-11ed-93c8-022c40d974e6",
        "3ba6cc14-134f-11ed-aabd-069e4f790264",
        "4de4cf9c-1350-11ed-be72-06141b6603a5",
        "9f479a1c-1351-11ed-8562-022c40d974e6",
        "073b041c-134b-11ed-9107-06141b6603a5",
        "de70849c-134c-11ed-b133-02ae981fbade",
        "a3a421e2-134d-11ed-8718-0630baf9dcb8",
        "20d2361c-134f-11ed-937c-0630baf9dcb8",
        "2c3d4a40-1350-11ed-b703-069e4f790264",
        "89519e9c-1351-11ed-bbba-02b0ee514bc7",
        "19dbb5e2-137f-11ed-9325-0630baf9dcb8",
        "0acf8260-134b-11ed-b39c-06141b6603a5",
        "de7096b2-134c-11ed-b5d1-02b0ee514bc7",
        "a4664d76-134d-11ed-90e5-0630baf9dcb8",
        "20fca9e2-134f-11ed-831e-02ae981fbade",
        "2cc171f8-1350-11ed-98a9-022c40d974e6",
        "88f51b0e-1351-11ed-8b3a-022c40d974e6",
        "177ca4e6-137f-11ed-ba85-069e4f790264",
        "5aee0a3c-134b-11ed-b73a-06141b6603a5",
        "197d3a30-134d-11ed-b6c4-02ae981fbade",
        "e1ef8144-134d-11ed-a48a-022c40d974e6",
        "5dd1c8e8-134f-11ed-870c-02ae981fbade",
        "67b18dd4-1350-11ed-bda0-06141b6603a5",
        "c11d6ab8-1351-11ed-84e2-022c40d974e6",
        "f50d2e60-1353-11ed-ad91-02ae981fbade",
        "22fd273e-134b-11ed-b49f-069e4f790264",
        "ac90c0f8-134d-11ed-9473-02ae981fbade",
        "28734334-134f-11ed-9ae4-02ae981fbade",
        "33084cee-1350-11ed-aec4-0630baf9dcb8",
        "8e9b1220-1351-11ed-98cd-0630baf9dcb8",
        "32cb0408-1353-11ed-958f-06141b6603a5",
        "9bb13540-1354-11ed-8be5-02ae981fbade",
        "e839e596-1355-11ed-a539-06141b6603a5",
        "e9ead280-1367-11ed-b2dd-02ae981fbade",
        "0fc5bc58-134b-11ed-84d6-02ae981fbade",
        "dff1e8ba-134c-11ed-bdfa-0630baf9dcb8",
        "aa5232f4-134d-11ed-94de-0630baf9dcb8",
        "27587082-134f-11ed-94e3-02ae981fbade",
        "2f2898b8-1350-11ed-83e3-06141b6603a5",
        "8ac2751c-1351-11ed-9c86-069e4f790264",
        "33eb8268-1353-11ed-9503-02ae981fbade",
        "e2cb8384-134c-11ed-b240-02b0ee514bc7",
        "27ddaedc-134f-11ed-b307-0630baf9dcb8",
        "33520118-1350-11ed-8729-02ae981fbade",
        "8f2206e0-1351-11ed-a189-022c40d974e6",
        "f9e429c0-1353-11ed-bfbd-069e4f790264",
        "9d62a3f6-1354-11ed-a272-02ae981fbade",
        "e708b3a0-1355-11ed-a9cc-022c40d974e6",
        "ea767c7c-1367-11ed-8812-02ae981fbade",
        "0a840a5e-134d-11ed-a800-022c40d974e6",
        "d663ddd4-134d-11ed-8e50-069e4f790264",
        "562ae340-134f-11ed-8463-02b0ee514bc7",
        "64151876-1350-11ed-88b9-02ae981fbade",
        "be100e8e-1351-11ed-8ea9-022c40d974e6",
        "1585bdbe-134b-11ed-bf9c-06141b6603a5",
        "e9250368-134c-11ed-90c0-069e4f790264",
        "ae7907e0-134d-11ed-a03c-02ae981fbade",
        "35076058-134f-11ed-9cc8-0630baf9dcb8",
        "3b76d206-1350-11ed-8f2a-02b0ee514bc7",
        "93e4b4ac-1351-11ed-966d-069e4f790264",
        "2696bcc0-134b-11ed-94e9-06141b6603a5",
        "c1afc804-134c-11ed-97b9-02b0ee514bc7",
        "8958d93e-1354-11ed-8131-0630baf9dcb8",
        "e72ed232-134c-11ed-86ef-02ae981fbade",
        "b0de8870-134d-11ed-bbdd-0630baf9dcb8",
        "31706192-134f-11ed-b1be-0630baf9dcb8",
        "388f4cb2-1350-11ed-b4af-06141b6603a5",
        "26a8ed66-1353-11ed-bca3-02ae981fbade",
        "e5013c32-1353-11ed-a2d1-022c40d974e6",
        "d7314f6e-1355-11ed-8ed9-069e4f790264",
        "e4ab5832-1360-11ed-9af5-02b0ee514bc7",
        "fae63f2a-1367-11ed-9d09-02b0ee514bc7",
        "3e7fd756-1371-11ed-863d-02b0ee514bc7",
        "62f0b70c-1373-11ed-af29-02b0ee514bc7",
        "eccc62fa-1373-11ed-a044-069e4f790264",
        "22896168-1374-11ed-b3fd-02b0ee514bc7",
        "262f3744-134b-11ed-9483-069e4f790264",
        "f1b71ebc-134c-11ed-b40d-02ae981fbade",
        "bdca956a-134d-11ed-b120-02b0ee514bc7",
        "36867464-134f-11ed-9a7b-022c40d974e6",
        "43c7025a-1350-11ed-91d1-06141b6603a5",
        "a33abafa-1351-11ed-bf1d-02ae981fbade",
        "cc248e5a-134c-11ed-90d8-02b0ee514bc7",
        "9478ad00-134d-11ed-a419-022c40d974e6",
        "2161ca3e-134f-11ed-a142-06141b6603a5",
        "3228f67a-1350-11ed-a5e1-022c40d974e6",
        "8dcac656-1351-11ed-be21-069e4f790264",
        "234e9f60-134b-11ed-a1e4-02ae981fbade",
        "ecb1a388-134c-11ed-8203-069e4f790264",
        "b246f79c-134d-11ed-8cb2-0630baf9dcb8",
        "2c057aa8-134f-11ed-af47-069e4f790264",
        "36a52174-1350-11ed-a575-02b0ee514bc7",
        "15dcec12-1353-11ed-9258-02b0ee514bc7",
        "e79e9aae-134c-11ed-95c6-02ae981fbade",
        "af106a90-134d-11ed-a609-02b0ee514bc7",
        "2e9b88ca-134f-11ed-a4e1-06141b6603a5",
        "3607a2dc-1350-11ed-8437-06141b6603a5",
        "8d77a0e8-1351-11ed-b4fb-022c40d974e6",
        "e2720d72-0da2-11ed-9b61-0630baf9dcb8",
        "e2720d72-0da2-11ed-9b61-0630baf9dcb8",
        "c4404508-134c-11ed-8c38-069e4f790264",
        "8d86f1aa-134d-11ed-ae50-069e4f790264",
        "0b75a6dc-134f-11ed-a5b1-022c40d974e6",
        "1a46aaac-1350-11ed-ad81-0630baf9dcb8",
        "6fe30d06-1351-11ed-9ce7-069e4f790264",
        "07178140-134b-11ed-950a-069e4f790264",
        "da800646-134c-11ed-b61e-06141b6603a5",
        "a30d2ec2-134d-11ed-a7ad-022c40d974e6",
        "1f3e79d2-134f-11ed-9452-06141b6603a5",
        "2a8575ce-1350-11ed-9705-069e4f790264",
        "8249a504-1351-11ed-b5cb-02b0ee514bc7",
        "14a85fc6-137f-11ed-bcba-02ae981fbade",
        "4a5d5560-134b-11ed-8d45-022c40d974e6",
        "0a39e474-134d-11ed-b01a-06141b6603a5",
        "550ce35a-134f-11ed-a55f-022c40d974e6",
        "5f3556ae-1350-11ed-916f-022c40d974e6",
        "3cae1c6a-134b-11ed-926b-02ae981fbade",
        "d2debacc-134c-11ed-a8db-022c40d974e6",
        "98d59584-134d-11ed-80ec-02ae981fbade",
        "168142d4-134f-11ed-aac4-022c40d974e6",
        "22927a38-1350-11ed-98ac-0630baf9dcb8",
        "7c47a9a8-1351-11ed-a9ee-06141b6603a5",
        "0c39c53c-137f-11ed-b657-069e4f790264",
        "13db2b34-134b-11ed-873b-0630baf9dcb8",
        "e7cac138-134c-11ed-9db3-02b0ee514bc7",
        "adc0204a-134d-11ed-9cd4-02b0ee514bc7",
        "2866442c-134f-11ed-9bb4-06141b6603a5",
        "360a7566-1350-11ed-ba57-022c40d974e6",
        "8c93f1b8-1351-11ed-aca3-022c40d974e6",
        "103e2e22-134b-11ed-aaec-02b0ee514bc7",
        "e0a5cb0a-134c-11ed-aa24-022c40d974e6",
        "aafa13c0-134d-11ed-bfb3-02b0ee514bc7",
        "2941fa6c-134f-11ed-af4a-02ae981fbade",
        "324a0360-1350-11ed-b9d8-022c40d974e6",
        "8b75d968-1351-11ed-b54e-02ae981fbade",
        "40d193ee-134b-11ed-8ba6-0630baf9dcb8",
        "d53464ca-134c-11ed-93ae-02ae981fbade",
        "9d3ace96-134d-11ed-9e3a-0630baf9dcb8",
        "1bd5d65a-134f-11ed-9068-06141b6603a5",
        "23f7c3ce-1350-11ed-b514-06141b6603a5",
        "7f5d6498-1351-11ed-99a2-02ae981fbade",
        "1110d190-137f-11ed-81da-06141b6603a5",
        "43fe6e20-134b-11ed-b19e-0630baf9dcb8",
        "ff73cc8a-134c-11ed-994f-02b0ee514bc7",
        "cbea7520-134d-11ed-be45-022c40d974e6",
        "4665dbfe-134f-11ed-b946-0630baf9dcb8",
        "49b10026-1350-11ed-b1fc-0630baf9dcb8",
        "ab99beb2-1351-11ed-ac82-022c40d974e6",
        "d33f7b36-1375-11ed-8b60-02ae981fbade",
        "ebfc1d5a-1375-11ed-955d-02ae981fbade",
        "0ddd7ebe-1376-11ed-be7b-022c40d974e6",
        "6cc4cc2e-1377-11ed-b27c-02ae981fbade",
        "10750190-134b-11ed-afdc-0630baf9dcb8",
        "d664e658-134c-11ed-8b69-02ae981fbade",
        "9eca2298-134d-11ed-8acb-0630baf9dcb8",
        "1ea77d3e-134f-11ed-882e-02b0ee514bc7",
        "24d81f50-1350-11ed-b521-02ae981fbade",
        "7ad539be-1351-11ed-bffc-0630baf9dcb8",
        "21bfe26e-1353-11ed-b776-06141b6603a5",
        "dd2caee2-1353-11ed-b20a-0630baf9dcb8",
        "804160dc-1354-11ed-b645-0630baf9dcb8",
        "d016a88c-1355-11ed-b9a8-022c40d974e6",
        "e1630e36-1360-11ed-974a-06141b6603a5",
        "f0a48fa8-1367-11ed-b36c-02b0ee514bc7",
        "3c8ef86e-1371-11ed-9504-069e4f790264",
        "5fb37f52-1373-11ed-b492-02ae981fbade",
        "e5ebbcb0-1373-11ed-b491-02b0ee514bc7",
        "17b2fcbe-134b-11ed-9b04-02b0ee514bc7",
        "ec92b3b0-134c-11ed-bac3-06141b6603a5",
        "b276c9a4-134d-11ed-a6ec-02ae981fbade",
        "2bff8440-134f-11ed-a9ee-02ae981fbade",
        "379c3270-1350-11ed-96cd-069e4f790264",
        "956d91ae-1351-11ed-a489-02ae981fbade",
        "a1d3347a-134d-11ed-8846-0630baf9dcb8",
        "1d52edce-134f-11ed-a9f5-022c40d974e6",
        "285eafa4-1350-11ed-aaa6-02b0ee514bc7",
        "85df5f2e-1351-11ed-9d6c-0630baf9dcb8",
        "2fb094a4-1353-11ed-a699-0630baf9dcb8",
        "e89c3838-1353-11ed-9f6e-06141b6603a5",
        "8da1c3fc-1354-11ed-9e8e-02b0ee514bc7",
        "db1d72c4-1355-11ed-ae26-0630baf9dcb8",
        "eff99492-1360-11ed-90f1-06141b6603a5",
        "fe7ec60c-1367-11ed-a043-0630baf9dcb8",
        "49bedbb2-1371-11ed-8417-02ae981fbade",
        "6bbd0bba-1373-11ed-a502-06141b6603a5",
        "f5436dc0-1373-11ed-ae79-02b0ee514bc7",
        "2d6dc04c-1374-11ed-9e76-069e4f790264",
        "e79e9022-1374-11ed-9f18-0630baf9dcb8",
        "212da4a6-134b-11ed-a2f5-06141b6603a5",
        "caccc680-134c-11ed-88da-0630baf9dcb8",
        "91b429f0-134d-11ed-8b46-022c40d974e6",
        "10b9ab52-134f-11ed-b9a5-06141b6603a5",
        "18f8a06a-1350-11ed-96b1-06141b6603a5",
        "740c5838-1351-11ed-89ad-022c40d974e6",
        "20d63ce0-1353-11ed-a160-06141b6603a5",
        "e11998bc-1353-11ed-bfbf-022c40d974e6",
        "867aa8e6-1354-11ed-8ee2-0630baf9dcb8",
        "d25ba066-1355-11ed-9e07-02b0ee514bc7",
        "e1da6d50-1360-11ed-8702-022c40d974e6",
        "f240a39c-1367-11ed-8e2d-02ae981fbade",
        "3a3bedba-1371-11ed-8599-06141b6603a5",
        "5c4d41d6-1373-11ed-9079-06141b6603a5",
        "d23c2da2-134c-11ed-8cb1-02ae981fbade",
        "9a037516-134d-11ed-8f39-02b0ee514bc7",
        "18290428-134f-11ed-8dfb-02b0ee514bc7",
        "259f30f4-1350-11ed-ac67-02ae981fbade",
        "80d62b48-1351-11ed-9c6f-02ae981fbade",
        "2b470704-1353-11ed-9512-0630baf9dcb8",
        "e8d38176-1353-11ed-9592-02b0ee514bc7",
        "8fdc52a4-1354-11ed-b75e-02b0ee514bc7",
        "d9fa52cc-1355-11ed-8045-02b0ee514bc7",
        "e7c63636-1360-11ed-a75c-02b0ee514bc7",
        "f7a9e8e8-1367-11ed-9ffa-022c40d974e6",
        "4250923a-1371-11ed-9148-06141b6603a5",
        "61d6f7f0-1373-11ed-99e5-022c40d974e6",
        "ea21291e-1373-11ed-9e7c-022c40d974e6",
        "2182daec-1374-11ed-82be-02b0ee514bc7",
    );

    foreach ($arrayLeads as $id) {
        $Leads = BeanFactory::getBean('Leads', $id,array('disable_row_level_security' => true));
        if ( !empty($Leads->id) ) {
            $Leads->mark_deleted("$id");
            $Leads->save();
        }
    }
   
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("--======> Tarea Delete Leads Ejecutada <======-- ".date('Y-m-d H:i:s'));
    $GLOBALS['log']->security("******************************************************");

    return true;
}



?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/TareaProcess_Listcstm.php


$job_strings[] = 'TareaProcess_Listcstm';
function TareaProcess_Listcstm()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaProcess_Listcstm. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;

    //Query para buscar los valores de listas desplegables pendientes por insertar en Sugar
    $queryInicial = "SELECT * FROM list_custom WHERE status='Pending'";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        require_once('modules/Studio/DropDowns/DropDownHelper.php');
        while ($row = $db->fetchByAssoc($result)) {
            $arr_valuesList = json_decode($row['values_list'],true);
            $GLOBALS['log']->security("OPT ".$row['list']);
            $GLOBALS['log']->security("VALUES ".print_r($arr_valuesList,true));
            save_custom_dropdown_strings(array($row['list'] => $arr_valuesList),"es_ES",true);
            //Query para actualizar el estado
            $Query_Update_Status = "UPDATE list_custom SET status='Processed' WHERE id='{$row['id']}'";
            //Ejecutar para insertar datos
            $GLOBALS['db']->query($Query_Update_Status);
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaProcess_Listcstm. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/TareaReenvios.php


$job_strings[] = 'TareaReenvios';
function TareaReenvios()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaReenvios. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;
    //Query para identificar los registros con error 400, este error hacia parte de la caida del servicio por parte de sigru
    $queryInicial = "SELECT log_send_sigru.*, t1.id t1_id, t1.record t1_record, t1.fechamax, t1.module t1_module,t1.status t1_status FROM log_send_sigru INNER JOIN (select id, record, MAX(CAST(fecha AS CHAR)) AS fechamax, module, status from log_send_sigru WHERE log_send_sigru.fecha BETWEEN '2021-11-12 00:00:01' AND now() GROUP BY record)t1 ON log_send_sigru.fecha=t1.fechamax AND log_send_sigru.record=t1.record WHERE (log_send_sigru.status='400' OR log_send_sigru.status='405' OR log_send_sigru.status IS NULL OR log_send_sigru.status = '' OR log_send_sigru.status='500' OR log_send_sigru.status='504' OR log_send_sigru.status='401') AND (log_send_sigru.module='Accounts' OR log_send_sigru.module='Leads' OR log_send_sigru.module='Contacts') ORDER BY `t1_status` DESC";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $Record = BeanFactory::getBean($row['module'], $row['record'], array('disable_row_level_security' => true));
            $GLOBALS['log']->security("Registro ".$Record->name." Modulo ".$row['module']." id: ".$Record->id);
            if (!empty($Record->id)) {
                //$Record->save();
                //Identificar el modulo para construir el array
                //Continuar y enviar nuevamente la petición a sigru
                require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
                $sendsigru = new Send_Data_Sigru();
                $responseSIGRU = $sendsigru->data($row['record'],$row['module']);
                
            }else{
                $GLOBALS['log']->security("El registro a enviar ya está eliminado o no existe");
            }
        }
    }
    //Ejecutar reenvios de cuentas 200 pero no recibidas por SIGRU
    reenviosstatus200();
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaReenvios. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}

//Funcion para reenvios de cuentas con status http 200 en respuesta pero no se procesó la cuenta 
function reenviosstatus200(){
    global $db;
    //
    $queryInicial = "SELECT * FROM log_send_sigru WHERE log_send_sigru.responsesigru LIKE '%No se modifico o agrego el cliente%' AND log_send_sigru.status='200' GROUP BY log_send_sigru.record LIMIT 100";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $Record = BeanFactory::getBean($row['module'], $row['record'], array('disable_row_level_security' => true));
            $GLOBALS['log']->security("Registro ".$Record->name." Modulo ".$row['module']." id: ".$Record->id);
            if (!empty($Record->id)) {
                //$Record->save();
                //Identificar el modulo para construir el array
                //Continuar y enviar nuevamente la petición a sigru
                require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
                $sendsigru = new Send_Data_Sigru();
                $responseSIGRU = $sendsigru->data($row['record'],$row['module']);
                
            }else{
                $GLOBALS['log']->security("El registro a enviar ya está eliminado o no existe");
            }
        }
    }
}




?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/eliminaradjuntosgv.php

/*
Para dar satisfacción a la propuesta de adjuntos https://docs.google.com/document/d/1wusr7EdzZcbNCEl6g2ipYG0olQfuKkhMjCY0BXNJiTo/edit se requiere realizar la tarea programada que eliminé los adjuntos cuando ya tengamos la URL del repositorio, la frecuencia será todos los domingos.

*/
//use Sugarcrm\Sugarcrm\ProcessManager\Registry;
$job_strings[] = 'eliminaradjuntosgv';
function eliminaradjuntosgv(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-eliminaradjuntosgv. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return = true;
	try{
		global $db;
		//Query para identificar los adjutnos que se pueden eliminar
		$querynotas = "";
		$queries = array("Notes"=>"SELECT notes.id, n2.id IDPadre FROM notes INNER JOIN notes n2 ON notes.note_parent_id=n2.id INNER JOIN notes_cstm n2c ON n2.id=n2c.id_c WHERE (notes.note_parent_id!='' OR notes.note_parent_id IS NOT NULL) AND (notes.filename!='' OR notes.filename IS NOT NULL) AND (n2c.sasa_soporte_adjunto_c IS NOT NULL OR n2c.sasa_soporte_adjunto_c!='') AND n2c.sasa_soporte_adjunto_c!='http://templatephp71.sugarcrmcolombia.com/visualizador?nota='",
			"SASA_Habeas_Data" => "SELECT id_c as id FROM sasa_habeas_data_cstm INNER JOIN sasa_habeas_data ON sasa_habeas_data_cstm.id_c=sasa_habeas_data.id WHERE sasa_habeas_data.deleted=0 AND (sasa_habeas_data_cstm.sasa_adjuntohabeasdata_c!='' OR sasa_habeas_data_cstm.sasa_adjuntohabeasdata_c IS NOT NULL) AND sasa_habeas_data_cstm.sasa_soporte_autorizacion_c!='http://templatephp71.sugarcrmcolombia.com/visualizador?nota='"
		);

		foreach ($queries as $key => $value) {
			$result = $db->query($value);
			$lastDbError = $db->lastDbError();				
			if(!empty($lastDbError)){
				$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$querynotas}"); 
			}else{
				//Recorrer las notas que tienen adjuntos
				while($row = $db->fetchByAssoc($result)) {
					//Inatancias objeto de notas
					$bean = BeanFactory::getBean($key, $row["id"]);
					if ($key=='Notes') {
						$bean->mark_deleted($row["id"]);
						//$borrado = $bean->deleteAttachment() ? 'Si' : 'No';
					}else{
						//Validar si existe nota
						if(!empty($bean->id)){
							require_once("include/upload_file.php");
							$file = new UploadFile();
	                		$repuesta = $file->unlink_file($row["id"]);
	                		$UpdateFile = $db->query("UPDATE sasa_habeas_data_cstm SET sasa_adjuntohabeasdata_c=NULL WHERE id_c='{$row["id"]}'");
							//Borrar adjunto de la nota instanciada en objeto bean
							//$borrado = $bean->deleteAttachment() ? 'Si' : 'No';
							$GLOBALS['log']->security("Borrado : {$borrado} Id Nota: {$row["id"]}");
						}else{
							$GLOBALS['log']->security("ERROR: No se encontro la nota de id {$row["id"]}"); 
						}
					}
				}
			}
		}

		

	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage());
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-eliminaradjuntosgv. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n");
	return $return;
}

?>

<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_UnidadesxCyPAftersave
{
	function before_save($bean, $event, $arguments)
	{	
		/*
		Logic Hook para poblar automaticamente la esctutura geografica de relacion
		*/
		try{

			if (!isset($bean->logic_hooks_unidadescxp_after_save_ignore_update) || $bean->logic_hooks_unidadescxp_after_save_ignore_update === false){//antiloop
				$bean->logic_hooks_unidadescxp_after_save_ignore_update = true;//antiloop

				if (!empty($bean->sasa_unida3a6anegocio_ida)){
					$lenidunidad = strlen($bean->sasa_unida3a6anegocio_ida);
					if ($lenidunidad < 10) {
						$resultunidad = $GLOBALS['db']->query("SELECT id, name FROM sasa_unidad_de_negocio INNER JOIN sasa_unidad_de_negocio_cstm ON sasa_unidad_de_negocio.id=sasa_unidad_de_negocio_cstm.id_c WHERE sasa_unidad_de_negocio_cstm.sasa_codunidnegocio_c='{$bean->sasa_unida3a6anegocio_ida}' AND deleted=0");
						$sasa_cod_unidad =  $GLOBALS['db']->fetchByAssoc($resultunidad);	

						if ($bean->load_relationship('sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1')){
							$relatedBeans = $bean->sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1->add($sasa_cod_unidad["id"]);
							$bean->sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1 ="";
							$bean->sasa_unida3a6anegocio_ida = $sasa_cod_unidad["id"];
						}
					}
				}

				if (!empty($bean->sasa_linea2_c)){
					$result = $GLOBALS['db']->query("SELECT id, name from sasa_vehiculos INNER JOIN sasa_vehiculos_cstm ON sasa_vehiculos.id=sasa_vehiculos_cstm.id_c WHERE sas_cd_line_vehi_c='{$bean->sasa_linea2_c}' AND deleted=0");
					$sasa_cod_linea =  $GLOBALS['db']->fetchByAssoc($result);	
					
					if ($bean->load_relationship('sasa_vehiculos_sasa_unidadnegclienteprospect_1')){
						$relatedBeans = $bean->sasa_vehiculos_sasa_unidadnegclienteprospect_1->add($sasa_cod_linea["id"]);
						$bean->sasa_vehiculos_sasa_unidadnegclienteprospect_1 ="";
					}
				}

				//Mapeo del codigo de la vitrina con el punto de atención (modulo)
				if (!empty($bean->sasa_vitrina_c)){
					$result = $GLOBALS['db']->query("SELECT id, name from sasa_puntos_de_ventas INNER JOIN sasa_puntos_de_ventas_cstm ON sasa_puntos_de_ventas.id=sasa_puntos_de_ventas_cstm.id_c WHERE sasa_puntos_de_ventas_cstm.sasa_codpuntoatencion_c='{$bean->sasa_vitrina_c}' AND deleted=0");
					$sasa_cod_vitrina =  $GLOBALS['db']->fetchByAssoc($result);	
					
					if ($bean->load_relationship('sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1')){
						$relatedBeans = $bean->sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1->add($sasa_cod_vitrina["id"]);
						$bean->sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1 ="";
					}
				}

				//Cambiar el estado a Cliente/Prospecto existente cuando el cliente ya esté prospectado en la misma unidad de negocio de otra manera dejarlo en asignado.
				if (!empty($bean->sasa_unida3a6anegocio_ida)) {
					//Instanciar objeto de unidad
					$UnidadNeg = BeanFactory::retrieveBean("sasa_Unidad_de_Negocio", $bean->sasa_unida3a6anegocio_ida, array('disable_row_level_security' => true));
					//Validar si tieen codigo y si la UxCP tiene una cuenta relacionada
					if (!empty($UnidadNeg->sasa_codunidnegocio_c) && !empty($bean->accounts_sasa_unidadnegclienteprospect_1accounts_ida)) {
						//Query para buscar el lead 
						$queryCP = "SELECT leads.id, leads_cstm.sasa_unidad_de_negocio_c, ucstm.id_c, leads.status, ucstm.sasa_codunidnegocio_c FROM leads INNER JOIN leads_cstm ON leads_cstm.id_c=leads.id INNER JOIN accounts ON leads.account_id=accounts.id INNER JOIN accounts_sasa_unidadnegclienteprospect_1_c ua ON ua.accounts_sasa_unidadnegclienteprospect_1accounts_ida=accounts.id INNER JOIN sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c uu ON uu.sasa_unida14dfrospect_idb=ua.accounts_saf31rospect_idb INNER JOIN sasa_unidad_de_negocio_cstm ucstm ON ucstm.id_c=uu.sasa_unida3a6anegocio_ida WHERE leads.account_id IS NOT null AND leads.deleted=0 AND accounts.deleted=0 AND uu.deleted=0 AND ua.deleted=0 AND accounts.id='{$bean->accounts_sasa_unidadnegclienteprospect_1accounts_ida}' AND ucstm.sasa_codunidnegocio_c=leads_cstm.sasa_unidad_de_negocio_c";
						$ResultqueryCP =  $GLOBALS['db']->query($queryCP);
						$FecthqueryCP =  $GLOBALS['db']->fetchByAssoc($ResultqueryCP);
						//Validar el cambio de estado
						if ($FecthqueryCP['status']!="D" && ($FecthqueryCP['id']!=null || $FecthqueryCP['id']!="")) {
							//Actualizar estado a D
							//$GLOBALS['db']->query("UPDATE leads SET leads.status='D' WHERE leads.account_id='{$bean->accounts_sasa_unidadnegclienteprospect_1accounts_ida}' AND leads.deleted=0");
						}
					}
				}

				$bean->save();
			}
			
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Lineas de Vehiculos: ".$e->getMessage()); 
		}
	}
}
?>
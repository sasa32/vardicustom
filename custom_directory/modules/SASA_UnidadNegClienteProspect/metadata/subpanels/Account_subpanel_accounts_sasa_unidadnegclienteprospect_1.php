<?php
// created: 2020-05-22 19:44:13
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'sasa_origen_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SASA_ORIGEN_C',
    'width' => 10,
  ),
  'sasa_detalle_origen_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SASA_DETALLE_ORIGEN_C',
    'width' => 10,
  ),
);
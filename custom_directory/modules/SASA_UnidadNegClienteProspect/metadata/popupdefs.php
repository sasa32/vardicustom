<?php
$popupMeta = array (
    'moduleMain' => 'SASA_UnidadNegClienteProspect',
    'varName' => 'SASA_UnidadNegClienteProspect',
    'orderBy' => 'sasa_unidadnegclienteprospect.name',
    'whereClauses' => array (
  'name' => 'sasa_unidadnegclienteprospect.name',
  'accounts_sasa_unidadnegclienteprospect_1_name' => 'sasa_unidadnegclienteprospect.accounts_sasa_unidadnegclienteprospect_1_name',
  'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name' => 'sasa_unidadnegclienteprospect.sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name',
  'sasa_origen_c' => 'sasa_unidadnegclienteprospect_cstm.sasa_origen_c',
  'sasa_detalle_origen_c' => 'sasa_unidadnegclienteprospect_cstm.sasa_detalle_origen_c',
  'date_entered' => 'sasa_unidadnegclienteprospect.date_entered',
  'date_modified' => 'sasa_unidadnegclienteprospect.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'accounts_sasa_unidadnegclienteprospect_1_name',
  5 => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name',
  6 => 'sasa_origen_c',
  7 => 'sasa_detalle_origen_c',
  8 => 'date_entered',
  9 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'accounts_sasa_unidadnegclienteprospect_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1ACCOUNTS_IDA',
    'width' => '10',
    'name' => 'accounts_sasa_unidadnegclienteprospect_1_name',
  ),
  'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
    'id' => 'SASA_UNIDA3A6ANEGOCIO_IDA',
    'width' => '10',
    'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name',
  ),
  'sasa_origen_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_ORIGEN_C',
    'width' => '10',
    'name' => 'sasa_origen_c',
  ),
  'sasa_detalle_origen_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_DETALLE_ORIGEN_C',
    'width' => '10',
    'name' => 'sasa_detalle_origen_c',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10',
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10',
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1ACCOUNTS_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'accounts_sasa_unidadnegclienteprospect_1_name',
  ),
  'SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
    'id' => 'SASA_UNIDA3A6ANEGOCIO_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name',
  ),
  'SASA_ORIGEN_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_ORIGEN_C',
    'width' => 10,
    'name' => 'sasa_origen_c',
  ),
  'SASA_DETALLE_ORIGEN_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_DETALLE_ORIGEN_C',
    'width' => 10,
    'name' => 'sasa_detalle_origen_c',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
    'name' => 'date_entered',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
    'name' => 'date_modified',
  ),
),
);

<?php  
	
	/**
	 * 
	 */
	require_once('include/SugarQuery/SugarQuery.php');
	class Loader_Unidades{
		var $return;
		var $db;
		
		function __construct($data_array){
			$this->return = array();
			$this->db = DBManagerFactory::getInstance();
			$this->main_unidades($data_array);
		}

		public function main_unidades($data_array){
			try{
				foreach ($data_array as $key => $value) {
					$validate = $this->validate_record($value);
					if($validate['band']){
						$status = $this->create_unidadcyp($value);
						$this->return[] = $status;
					}else{
						$this->return[] =array('Error' => '120', 'Description' => implode(' - ', $validate['error']));
					}
				}
			}catch(Exception $e){
				return array('Error' => '100', 'Description' => $e->getMessage());
			}
		}

		public function create_unidadcyp($record){
			
			if ($record['id_register_c']!=null) {
				$sql_get_unidadcyp= "SELECT id FROM sasa_unidadnegclienteprospect INNER JOIN sasa_unidadnegclienteprospect_cstm ON sasa_unidadnegclienteprospect.id=sasa_unidadnegclienteprospect_cstm.id_c WHERE sasa_lote_c='{$record['sasa_lote_c']}' AND id_register_c='{$record['id_register_c']}' AND deleted=0";
			}

			$unidadcyp =  $this->db->query($sql_get_unidadcyp);
			while($row = $this->db->fetchByAssoc($unidadcyp)){
				$unidadcyp_state_id = $row['id'];
			}

			if (!empty($unidadcyp_state_id)) {
				$status = "Update";
				$Unidad = BeanFactory::getBean('SASA_UnidadNegClienteProspect', $unidadcyp_state_id, array('disable_row_level_security' => true));
			}else{
				$status = "Created";
				$Unidad = BeanFactory::newBean('SASA_UnidadNegClienteProspect');
			}
			
			

			foreach ($record as $key => $value) {
				if ($value != "(null)") {
					$Unidad->{$key} = $value;
					switch ($key) {
						case 'cliente':
							$numeroCuenta = rtrim($value);
							$sql_get_account= $this->db->query("SELECT id FROM accounts INNER JOIN accounts_cstm ON accounts.id=accounts_cstm.id_c WHERE accounts_cstm.sasa_numero_documento_c='{$numeroCuenta}' AND deleted=0");
							$id_cuenta = $this->db->fetchByAssoc($sql_get_account);
							$Unidad->accounts_sasa_unidadnegclienteprospect_1accounts_ida = $id_cuenta['id'];
							break;
						case 'unidad':
							$numeroUnidad = rtrim($value);
							$sql_get_unidad= $this->db->query("SELECT id from sasa_unidad_de_negocio INNER JOIN sasa_unidad_de_negocio_cstm ON sasa_unidad_de_negocio.id=sasa_unidad_de_negocio_cstm.id_c WHERE sasa_unidad_de_negocio_cstm.sasa_codunidnegocio_c='{$numeroUnidad}' AND deleted=0");
							$id_unidad = $this->db->fetchByAssoc($sql_get_unidad);
							$Unidad->sasa_unida3a6anegocio_ida = $id_unidad['id'];
							break;
						case 'usuasig':
							$Unidad->assigned_user_id = "9239165d-cc92-48c5-8b67-67f343671abb";
							break;
						case 'creado':
							$Unidad->created_by = "9239165d-cc92-48c5-8b67-67f343671abb";
							break;
						case 'linea':
							$codlinea = rtrim($value);
							$sql_get_linea= $this->db->query("SELECT id FROM sasa_vehiculos INNER JOIN sasa_vehiculos_cstm ON sasa_vehiculos.id=sasa_vehiculos_cstm.id_c WHERE sasa_vehiculos_cstm.sas_cd_line_vehi_c='{$codlinea}' AND deleted=0");
							$id_linea = $this->db->fetchByAssoc($sql_get_linea);
							$Unidad->sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida = $id_linea['id'];
							break;
						case 'vitrina':
							$codvitrina = rtrim($value);
							$sql_get_vitrina= $this->db->query("SELECT id FROM sasa_puntos_de_ventas INNER JOIN sasa_puntos_de_ventas_cstm ON sasa_puntos_de_ventas.id=sasa_puntos_de_ventas_cstm.id_c WHERE sasa_puntos_de_ventas_cstm.sasa_codpuntoatencion_c='{$codvitrina}' AND deleted=0");
							$id_vitrina = $this->db->fetchByAssoc($sql_get_vitrina);
							$Unidad->sasa_punto200b_ventas_ida = $id_vitrina['id'];
							break;
						default:
							# code...
							break;
					}
				}	
			}

			$Unidad->save();

			return array('status' => $status, 'id' => $Unidad->id); 
		}


		public function validate_record($record){
			$validate_array = array('band' => true);

			return $validate_array;
		}

		public function get_return(){
			return $this->return;
		}
	}


?>
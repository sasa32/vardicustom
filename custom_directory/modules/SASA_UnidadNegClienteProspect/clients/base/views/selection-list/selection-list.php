<?php
$module_name = 'SASA_UnidadNegClienteProspect';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'accounts_sasa_unidadnegclienteprospect_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'id' => 'ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1ACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name',
                'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
                'enabled' => true,
                'id' => 'SASA_UNIDA3A6ANEGOCIO_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sasa_origen_c',
                'label' => 'LBL_SASA_ORIGEN_C',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_detalle_origen_c',
                'label' => 'LBL_SASA_DETALLE_ORIGEN_C',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              6 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => true,
                'name' => 'date_modified',
                'readonly' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);

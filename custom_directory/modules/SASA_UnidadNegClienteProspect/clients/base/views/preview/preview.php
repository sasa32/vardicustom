<?php
$module_name = 'SASA_UnidadNegClienteProspect';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'accounts_sasa_unidadnegclienteprospect_1_name',
              ),
              1 => 
              array (
                'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tipo_de_cliente_c',
                'label' => 'LBL_SASA_TIPO_DE_CLIENTE_C',
              ),
              3 => 
              array (
                'name' => 'sasa_origen_c',
                'label' => 'LBL_SASA_ORIGEN_C',
              ),
              4 => 
              array (
                'name' => 'sasa_detalle_origen_c',
                'label' => 'LBL_SASA_DETALLE_ORIGEN_C',
              ),
              5 => 
              array (
                'name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1_name',
                'label' => 'LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_VEHICULOS_TITLE',
              ),
              6 => 
              array (
                'name' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_name',
                'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
              ),
              7 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              8 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
          'useTabs' => false,
        ),
      ),
    ),
  ),
);

<?php
// created: 2021-04-19 03:07:44
$viewdefs['SASA_UnidadNegClienteProspect']['base']['view']['subpanel-for-accounts-accounts_sasa_unidadnegclienteprospect_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_tipo_de_cliente_c',
          'label' => 'LBL_SASA_TIPO_DE_CLIENTE_C',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_origen_c',
          'label' => 'LBL_SASA_ORIGEN_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_detalle_origen_c',
          'label' => 'LBL_SASA_DETALLE_ORIGEN_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_asesor_c',
          'label' => 'LBL_SASA_ASESOR_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_name',
          'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
          'enabled' => true,
          'id' => 'SASA_PUNTO200B_VENTAS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);
<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sasa_vehiculos_sasa_unidadnegclienteprospect_1_SASA_UnidadNegClienteProspect.php

// created: 2020-06-11 20:27:26
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_vehiculos_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'side' => 'right',
  'vname' => 'LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'id_name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
  'link-type' => 'one',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_vehiculos_sasa_unidadnegclienteprospect_1_name"] = array (
  'name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_VEHICULOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'name',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida"] = array (
  'name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE_ID',
  'id_name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_SASA_UnidadNegClienteProspect.php

// created: 2021-03-05 13:31:31
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'side' => 'right',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'id_name' => 'sasa_punto200b_ventas_ida',
  'link-type' => 'one',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_name"] = array (
  'name' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_punto200b_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'name',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_punto200b_ventas_ida"] = array (
  'name' => 'sasa_punto200b_ventas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE_ID',
  'id_name' => 'sasa_punto200b_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sugarfield_sasa_origen_c.php

 // created: 2020-05-16 17:57:33
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_origen_c']['labelValue']='Origen';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_origen_c']['dependency']='';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_origen_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/accounts_sasa_unidadnegclienteprospect_1_SASA_UnidadNegClienteProspect.php

// created: 2020-05-21 08:12:31
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["accounts_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'accounts_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'id_name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["accounts_sasa_unidadnegclienteprospect_1_name"] = array (
  'name' => 'accounts_sasa_unidadnegclienteprospect_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'link' => 'accounts_sasa_unidadnegclienteprospect_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["accounts_sasa_unidadnegclienteprospect_1accounts_ida"] = array (
  'name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE_ID',
  'id_name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'link' => 'accounts_sasa_unidadnegclienteprospect_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_SASA_UnidadNegClienteProspect.php

// created: 2020-05-21 08:13:25
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'sasa_Unidad_de_Negocio',
  'bean_name' => 'sasa_Unidad_de_Negocio',
  'side' => 'right',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'id_name' => 'sasa_unida3a6anegocio_ida',
  'link-type' => 'one',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name"] = array (
  'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'save' => true,
  'id_name' => 'sasa_unida3a6anegocio_ida',
  'link' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'table' => 'sasa_unidad_de_negocio',
  'module' => 'sasa_Unidad_de_Negocio',
  'rname' => 'name',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_unida3a6anegocio_ida"] = array (
  'name' => 'sasa_unida3a6anegocio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE_ID',
  'id_name' => 'sasa_unida3a6anegocio_ida',
  'link' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'table' => 'sasa_unidad_de_negocio',
  'module' => 'sasa_Unidad_de_Negocio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sugarfield_name.php

 // created: 2020-05-27 16:21:24
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['len']='255';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['audited']=false;
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['massupdate']=false;
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['importable']='false';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['duplicate_merge']='disabled';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['merge_filter']='disabled';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['unified_search']=false;
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['calculated']='1';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['formula']='concat(related($sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1,"name")," - ",related($accounts_sasa_unidadnegclienteprospect_1,"name"))';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sugarfield_sasa_vitrina_c.php

 // created: 2020-06-11 14:39:16

 
?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sugarfield_sasa_asesor_c.php

 // created: 2020-06-11 14:39:17

 
?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sugarfield_sasa_linea2_c.php

 // created: 2020-08-26 12:03:35

 
?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/full_text_search_admin.php

 // created: 2020-08-28 14:29:24
$dictionary['SASA_UnidadNegClienteProspect']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sugarfield_sasa_tipo_de_cliente_c.php

 // created: 2021-04-15 15:53:14
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_tipo_de_cliente_c']['labelValue']='Tipo de Cliente';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_tipo_de_cliente_c']['dependency']='';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_tipo_de_cliente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_UnidadNegClienteProspect/Ext/Vardefs/sugarfield_sasa_detalle_origen_c.php

 // created: 2021-04-21 14:49:43
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_detalle_origen_c']['labelValue']='Detalle de Origen';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_detalle_origen_c']['dependency']='';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_detalle_origen_c']['visibility_grid']=array (
  'trigger' => 'sasa_origen_c',
  'values' => 
  array (
    1 => 
    array (
    ),
    2 => 
    array (
    ),
    3 => 
    array (
      0 => '',
      1 => '71',
    ),
    4 => 
    array (
      0 => '',
      1 => '1',
      2 => '3',
      3 => '2',
      4 => '4',
      5 => '5',
      6 => '73',
      7 => '74',
    ),
    5 => 
    array (
      0 => '',
      1 => '6',
      2 => '7',
      3 => '11',
      4 => '8',
      5 => '47',
      6 => '10',
    ),
    6 => 
    array (
      0 => '',
      1 => '15',
      2 => '12',
      3 => '13',
      4 => '16',
    ),
    8 => 
    array (
    ),
    9 => 
    array (
    ),
    10 => 
    array (
    ),
    11 => 
    array (
    ),
    13 => 
    array (
    ),
    14 => 
    array (
    ),
    15 => 
    array (
      0 => '',
      1 => '70',
    ),
    16 => 
    array (
    ),
    17 => 
    array (
    ),
    18 => 
    array (
      0 => '',
      1 => '18',
      2 => '57',
    ),
    19 => 
    array (
      0 => '',
      1 => '20',
      2 => '21',
    ),
    20 => 
    array (
      0 => '',
      1 => '23',
      2 => '22',
      3 => '79',
      4 => '69',
      5 => '67',
      6 => '68',
      7 => '25',
      8 => '24',
      9 => '76',
      10 => '77',
      11 => '78',
    ),
    21 => 
    array (
    ),
    22 => 
    array (
      0 => '',
      1 => '27',
      2 => '75',
      3 => '80',
    ),
    23 => 
    array (
      0 => '',
      1 => '28',
    ),
    24 => 
    array (
    ),
    25 => 
    array (
      0 => '',
      1 => '42',
      2 => '43',
      3 => '34',
      4 => '44',
    ),
    26 => 
    array (
    ),
    28 => 
    array (
    ),
    29 => 
    array (
      0 => '',
      1 => '31',
      2 => '32',
      3 => '52',
    ),
    30 => 
    array (
    ),
    31 => 
    array (
    ),
    32 => 
    array (
    ),
    33 => 
    array (
    ),
    34 => 
    array (
      0 => '',
      1 => '39',
    ),
    35 => 
    array (
    ),
    36 => 
    array (
      0 => '',
      1 => '40',
      2 => '51',
    ),
    37 => 
    array (
      0 => '',
      1 => '58',
      2 => '64',
    ),
    38 => 
    array (
      0 => '',
      1 => '45',
      2 => '50',
      3 => '46',
      4 => '72',
    ),
    39 => 
    array (
      0 => '',
      1 => '54',
      2 => '56',
      3 => '59',
      4 => '60',
      5 => '55',
    ),
    40 => 
    array (
      0 => '',
      1 => '49',
      2 => '9',
      3 => '81',
      4 => '48',
    ),
    41 => 
    array (
      0 => '',
      1 => '61',
      2 => '62',
    ),
    42 => 
    array (
      0 => '',
      1 => '63',
      2 => '64',
    ),
    43 => 
    array (
      0 => '',
      1 => '65',
      2 => '66',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>

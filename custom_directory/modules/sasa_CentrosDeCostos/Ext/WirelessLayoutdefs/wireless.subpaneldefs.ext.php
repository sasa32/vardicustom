<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_CentrosDeCostos/Ext/WirelessLayoutdefs/sasa_centrosdecostos_sasa_puntos_de_ventas_1_sasa_CentrosDeCostos.php

 // created: 2022-10-04 17:32:58
$layout_defs["sasa_CentrosDeCostos"]["subpanel_setup"]['sasa_centrosdecostos_sasa_puntos_de_ventas_1'] = array (
  'order' => 100,
  'module' => 'sasa_Puntos_de_Ventas',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'get_subpanel_data' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_CentrosDeCostos/Ext/WirelessLayoutdefs/sasa_centrosdecostos_cases_1_sasa_CentrosDeCostos.php

 // created: 2022-10-05 15:14:01
$layout_defs["sasa_CentrosDeCostos"]["subpanel_setup"]['sasa_centrosdecostos_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_centrosdecostos_cases_1',
);

?>

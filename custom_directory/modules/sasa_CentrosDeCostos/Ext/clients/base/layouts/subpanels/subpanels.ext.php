<?php
// WARNING: The contents of this file are auto-generated.


// created: 2022-10-05 15:14:01
$viewdefs['sasa_CentrosDeCostos']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_centrosdecostos_cases_1',
  ),
);

// created: 2022-10-04 17:32:58
$viewdefs['sasa_CentrosDeCostos']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  ),
);
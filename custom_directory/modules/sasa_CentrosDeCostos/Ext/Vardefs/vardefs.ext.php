<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_CentrosDeCostos/Ext/Vardefs/sugarfield_sasa_fechacierre_c.php

 // created: 2022-10-04 16:57:24
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_fechacierre_c']['labelValue']='Fecha de cierre';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_fechacierre_c']['enforced']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_fechacierre_c']['dependency']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_fechacierre_c']['required_formula']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_fechacierre_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_CentrosDeCostos/Ext/Vardefs/sasa_centrosdecostos_sasa_puntos_de_ventas_1_sasa_CentrosDeCostos.php

// created: 2022-10-04 17:32:58
$dictionary["sasa_CentrosDeCostos"]["fields"]["sasa_centrosdecostos_sasa_puntos_de_ventas_1"] = array (
  'name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'type' => 'link',
  'relationship' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_CENTROSDECOSTOS_TITLE',
  'id_name' => 'sasa_centr37aaecostos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_CentrosDeCostos/Ext/Vardefs/sasa_centrosdecostos_cases_1_sasa_CentrosDeCostos.php

// created: 2022-10-05 15:14:01
$dictionary["sasa_CentrosDeCostos"]["fields"]["sasa_centrosdecostos_cases_1"] = array (
  'name' => 'sasa_centrosdecostos_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_centrosdecostos_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_SASA_CENTROSDECOSTOS_TITLE',
  'id_name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_CentrosDeCostos/Ext/Vardefs/sugarfield_sasa_codcentrodecostos_c.php

 // created: 2022-12-05 17:33:27
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['labelValue']='Código Centro de Costos';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['enforced']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['dependency']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['required_formula']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_CentrosDeCostos/Ext/Vardefs/sugarfield_sasa_direccion_c.php

 // created: 2022-12-05 17:35:43
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['labelValue']='Dirección';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['enforced']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['dependency']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['required_formula']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_CentrosDeCostos/Ext/Vardefs/sugarfield_sasa_codsucursal_c.php

 // created: 2022-12-07 14:23:53
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['labelValue']='Código Sucursal';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['enforced']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['dependency']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['required_formula']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['readonly_formula']='';

 
?>

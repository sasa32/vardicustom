<?php
// created: 2022-12-05 17:56:06
$viewdefs['sasa_CentrosDeCostos']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'sasa_codcentrodecostos_c' => 
    array (
    ),
    'sasa_codsucursal_c' => 
    array (
    ),
    'sasa_direccion_c' => 
    array (
    ),
    'sasa_fechacierre_c' => 
    array (
    ),
    'description' => 
    array (
    ),
    'tag' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'assigned_user_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
  ),
);
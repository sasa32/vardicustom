<?php
$popupMeta = array (
    'moduleMain' => 'sasa_CentrosDeCostos',
    'varName' => 'sasa_CentrosDeCostos',
    'orderBy' => 'sasa_centrosdecostos.name',
    'whereClauses' => array (
  'name' => 'sasa_centrosdecostos.name',
  'sasa_codcentrodecostos_c' => 'sasa_centrosdecostos_cstm.sasa_codcentrodecostos_c',
  'sasa_codsucursal_c' => 'sasa_centrosdecostos_cstm.sasa_codsucursal_c',
  'sasa_direccion_c' => 'sasa_centrosdecostos_cstm.sasa_direccion_c',
  'sasa_fechacierre_c' => 'sasa_centrosdecostos_cstm.sasa_fechacierre_c',
  'favorites_only' => 'sasa_centrosdecostos.favorites_only',
  'assigned_user_name' => 'sasa_centrosdecostos.assigned_user_name',
  'date_entered' => 'sasa_centrosdecostos.date_entered',
  'date_modified' => 'sasa_centrosdecostos.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sasa_codcentrodecostos_c',
  5 => 'sasa_codsucursal_c',
  6 => 'sasa_direccion_c',
  7 => 'sasa_fechacierre_c',
  8 => 'favorites_only',
  9 => 'assigned_user_name',
  10 => 'date_entered',
  11 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'sasa_codcentrodecostos_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'int',
    'label' => 'LBL_SASA_CODCENTRODECOSTOS_C',
    'width' => 10,
    'name' => 'sasa_codcentrodecostos_c',
  ),
  'sasa_codsucursal_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'label' => 'LBL_SASA_CODSUCURSAL_C',
    'width' => 10,
    'name' => 'sasa_codsucursal_c',
  ),
  'sasa_direccion_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'label' => 'LBL_SASA_DIRECCION_C',
    'width' => 10,
    'name' => 'sasa_direccion_c',
  ),
  'sasa_fechacierre_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'date',
    'label' => 'LBL_SASA_FECHACIERRE_C',
    'width' => 10,
    'name' => 'sasa_fechacierre_c',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => 10,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'related_fields' => 
    array (
      0 => 'assigned_user_id',
    ),
    'label' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'name' => 'assigned_user_name',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SASA_CODCENTRODECOSTOS_C' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'int',
    'label' => 'LBL_SASA_CODCENTRODECOSTOS_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_CODSUCURSAL_C' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'label' => 'LBL_SASA_CODSUCURSAL_C',
    'width' => 10,
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'TEAM_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_TEAM',
    'default' => true,
    'name' => 'team_name',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
),
);

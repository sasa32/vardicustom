<?php
$popupMeta = array (
    'moduleMain' => 'sasa_Paises',
    'varName' => 'sasa_Paises',
    'orderBy' => 'sasa_paises.name',
    'whereClauses' => array (
  'name' => 'sasa_paises.name',
  'sasa_codigopais_c' => 'sasa_paises.sasa_codigopais_c',
  'sasa_estado_c' => 'sasa_paises.sasa_estado_c',
  'date_entered' => 'sasa_paises.date_entered',
  'description' => 'sasa_paises.description',
  'date_modified' => 'sasa_paises.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sasa_codigopais_c',
  6 => 'sasa_estado_c',
  7 => 'date_entered',
  8 => 'description',
  9 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'name',
    'label' => 'LBL_NAME',
    'width' => '10',
    'name' => 'name',
  ),
  'sasa_codigopais_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_CODIGOPAIS_C',
    'width' => '10',
    'name' => 'sasa_codigopais_c',
  ),
  'sasa_estado_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_ESTADO_C',
    'width' => '10',
    'name' => 'sasa_estado_c',
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'name' => 'description',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10',
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'name',
    'label' => 'LBL_NAME',
    'width' => 10,
    'default' => true,
    'name' => 'name',
  ),
  'SASA_CODIGOPAIS_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_SASA_CODIGOPAIS_C',
    'width' => 10,
    'name' => 'sasa_codigopais_c',
  ),
  'SASA_ESTADO_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_ESTADO_C',
    'width' => 10,
    'name' => 'sasa_estado_c',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
    'name' => 'date_entered',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
),
);

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Layoutdefs/sasa_paises_accounts_1_sasa_Paises.php

 // created: 2020-05-15 15:40:50
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_accounts_1'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_paises_accounts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Layoutdefs/sasa_paises_contacts_1_sasa_Paises.php

 // created: 2020-05-15 15:41:17
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_paises_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Layoutdefs/sasa_paises_leads_1_sasa_Paises.php

 // created: 2020-05-15 15:41:37
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_paises_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Layoutdefs/sasa_paises_sasa_departamentos_1_sasa_Paises.php

 // created: 2020-05-15 15:55:44
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_sasa_departamentos_1'] = array (
  'order' => 100,
  'module' => 'sasa_Departamentos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'get_subpanel_data' => 'sasa_paises_sasa_departamentos_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Layoutdefs/sasa_paises_accounts_2_sasa_Paises.php

 // created: 2020-05-15 16:04:27
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_accounts_2'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_paises_accounts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Layoutdefs/sasa_paises_contacts_2_sasa_Paises.php

 // created: 2020-05-15 16:04:53
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_contacts_2'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_paises_contacts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Layoutdefs/sasa_paises_leads_2_sasa_Paises.php

 // created: 2020-05-15 16:05:13
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_leads_2'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_LEADS_2_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_paises_leads_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>

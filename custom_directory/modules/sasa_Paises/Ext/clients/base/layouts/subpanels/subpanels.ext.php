<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-05-15 15:40:50
$viewdefs['sasa_Paises']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PAISES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_paises_accounts_1',
  ),
);

// created: 2020-05-15 16:04:27
$viewdefs['sasa_Paises']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PAISES_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_paises_accounts_2',
  ),
);

// created: 2020-05-15 15:41:17
$viewdefs['sasa_Paises']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PAISES_CONTACTS_1_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_paises_contacts_1',
  ),
);

// created: 2020-05-15 16:04:53
$viewdefs['sasa_Paises']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_paises_contacts_2',
  ),
);

// created: 2020-05-15 15:41:37
$viewdefs['sasa_Paises']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PAISES_LEADS_1_FROM_LEADS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_paises_leads_1',
  ),
);

// created: 2020-05-15 16:05:13
$viewdefs['sasa_Paises']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PAISES_LEADS_2_FROM_LEADS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_paises_leads_2',
  ),
);

// created: 2020-05-15 15:55:44
$viewdefs['sasa_Paises']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_paises_sasa_departamentos_1',
  ),
);
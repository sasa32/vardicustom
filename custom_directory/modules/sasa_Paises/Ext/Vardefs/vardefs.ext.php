<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Vardefs/sasa_paises_accounts_1_sasa_Paises.php

// created: 2020-05-15 15:40:50
$dictionary["sasa_Paises"]["fields"]["sasa_paises_accounts_1"] = array (
  'name' => 'sasa_paises_accounts_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_1_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_accounts_1sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Vardefs/sasa_paises_contacts_1_sasa_Paises.php

// created: 2020-05-15 15:41:17
$dictionary["sasa_Paises"]["fields"]["sasa_paises_contacts_1"] = array (
  'name' => 'sasa_paises_contacts_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_1_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_contacts_1sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Vardefs/sasa_paises_leads_1_sasa_Paises.php

// created: 2020-05-15 15:41:37
$dictionary["sasa_Paises"]["fields"]["sasa_paises_leads_1"] = array (
  'name' => 'sasa_paises_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_PAISES_LEADS_1_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_leads_1sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Vardefs/sasa_paises_sasa_departamentos_1_sasa_Paises.php

// created: 2020-05-15 15:55:44
$dictionary["sasa_Paises"]["fields"]["sasa_paises_sasa_departamentos_1"] = array (
  'name' => 'sasa_paises_sasa_departamentos_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_sasa_departamentos_1',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'vname' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Vardefs/sasa_paises_accounts_2_sasa_Paises.php

// created: 2020-05-15 16:04:27
$dictionary["sasa_Paises"]["fields"]["sasa_paises_accounts_2"] = array (
  'name' => 'sasa_paises_accounts_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_accounts_2',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_2_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_accounts_2sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Vardefs/sasa_paises_contacts_2_sasa_Paises.php

// created: 2020-05-15 16:04:53
$dictionary["sasa_Paises"]["fields"]["sasa_paises_contacts_2"] = array (
  'name' => 'sasa_paises_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_contacts_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Vardefs/sasa_paises_leads_2_sasa_Paises.php

// created: 2020-05-15 16:05:13
$dictionary["sasa_Paises"]["fields"]["sasa_paises_leads_2"] = array (
  'name' => 'sasa_paises_leads_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_leads_2',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_PAISES_LEADS_2_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_leads_2sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Vardefs/sugarfield_prueba_c.php

 // created: 2021-04-04 16:07:14
$dictionary['sasa_Paises']['fields']['prueba_c']['labelValue']='Prueba';
$dictionary['sasa_Paises']['fields']['prueba_c']['required_formula']='';
$dictionary['sasa_Paises']['fields']['prueba_c']['readonly_formula']='';
$dictionary['sasa_Paises']['fields']['prueba_c']['visibility_grid']=array (
  'trigger' => 'sasa_estado_c',
  'values' => 
  array (
    '' => 
    array (
      0 => 'Other',
    ),
    'A' => 
    array (
      0 => 'Other',
    ),
    'I' => 
    array (
      0 => 'Other',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Paises/Ext/Vardefs/sugarfield_prueba_c-visibility_grid.php

$dictionary['sasa_Paises']['fields']['prueba_c']['visibility_grid']=array (
	'trigger' => 'sasa_estado_c',
	'values' => 
	array (
		'' => 
		array (
			0 => 'Other',
			1 => 'Customer',
		),
		'A' => 
		array (
			0 => 'Other',
			1 => 'Integrator',
			2 => 'Customer',
			3 => 'Other',
			4 => 'Press',
		),
		'I' => 
		array (
			0 => 'Other',
			1 => 'Partner',
			2 => 'Prospect',
			3 => 'Press',
		),
	),
);

?>

<?php 
use Sugarcrm\Sugarcrm\Security\HttpClient\ExternalResourceClient;
use Sugarcrm\Sugarcrm\Security\HttpClient\RequestException;
Class Rest{

	var $conf;
	var $moduletoken;
	var $data;

	function __construct($conf,$moduletoken,$data){
		$this->conf = $conf;
		$this->moduletoken = $moduletoken;
		$this->data = $data;

	}
	//Funcion para consumir el servicio de obtener token de Sigru
	public function GettokenSigru(){
		//Definir el servicio a consumir
		switch ($this->moduletoken) {
			case 'cliente':
				$url = "{$this->conf['Sigru']['url']}token/tokenSugar/?usuario={$this->conf['Sigru']['username']}&clave={$this->conf['Sigru']['password']}";
				break;
			case 'contacto':
				$url = "{$this->conf['Sigru']['url']}token/tokenSugar/?usuario={$this->conf['Sigru']['username']}&clave={$this->conf['Sigru']['password']}";
				break;
			case 'notes':
				$url = "{$this->conf['Sigru']['url']}token/tokenSugar/?usuario={$this->conf['Sigru']['username']}&clave={$this->conf['Sigru']['password']}";
				break;
			case 'cases':
				$url = "{$this->conf['Sigru']['url']}token/tokenSugar/?usuario={$this->conf['Sigru']['username']}&clave={$this->conf['Sigru']['password']}";
				break;
			case 'leads':
				$url = "{$this->conf['Sigru']['url']}token/tokenSugar/?usuario={$this->conf['Sigru']['username']}&clave={$this->conf['Sigru']['password']}";
				break;
			case 'habeas':
				$url = "{$this->conf['Sigru']['url']}token/tokenSugar/?usuario={$this->conf['Sigru']['username']}&clave={$this->conf['Sigru']['password']}";
				break;
			default:
				// code...
				break;
		}

		$GLOBALS['log']->security("URLTOken ".$url);	
    	// Set timeout to 60 seconds and 10 max redirects
		$response = (new ExternalResourceClient(60, 10))->get($url);
		$httpCode = $response->getStatusCode();
		if ($httpCode >= 400) {
		    $GLOBALS['log']->log("fatal", "Request failed with status: " . $httpCode);
		    throw new \SugarApiException("Request failed with status: " . $httpCode, null, null, $httpCode);
		}
		
		return $response->getBody()->getContents();
	}

	public function send_json($metodo=null){
		//Definir el servicio a consumir
		if ($this->moduletoken == 'contacto') {
			$url = "{$this->conf['Sigru']['url']}{$this->moduletoken}/crearOrModificarContacto";
			$method = "PUT";
		}
		if ($this->moduletoken == 'cliente') {
			$url = "{$this->conf['Sigru']['url']}{$this->moduletoken}/crearOrModificarCliente";
			$method = "PUT";
		}
		if ($this->moduletoken == 'leads') {
			$url = "{$this->conf['Sigru']['url']}{$this->moduletoken}/crearLead";
			$method = "POST";
		}
		if ($this->moduletoken == 'notes') {
			$url = "{$this->conf['Sigru']['url']}peticionesQuejasReclamos/crearAvancePQRF";
			$method = "POST";
		}
		if ($this->moduletoken == 'cases') {
			if ($metodo=="POST") {
				$url = "{$this->conf['Sigru']['url']}peticionesQuejasReclamos/crearPQRF";
				$method = "POST";
			}else{
				$url = "{$this->conf['Sigru']['url']}peticionesQuejasReclamos/actualizarPQRF";
				$method = "PUT";
			}	
		}
		if ($this->moduletoken == 'habeas') {
			if ($metodo=='POST') {
				$url = "{$this->conf['Sigru']['url']}habeasData/crearPreferencias/";
				$method = "POST";
			}else{
				$url = "{$this->conf['Sigru']['url']}habeasData/actualizarPreferencias/";
				$method = "PUT";
			}
		}
		if ($this->moduletoken =='deletecontact') {
			$url = "{$this->conf['Sigru']['url']}contacto/eliminarContacto";
			$method = "DELETE";
		}
		$GLOBALS['log']->security("URLservices ".$url." $method ".$method);
		//Obtener token Sigru
		$token = $this->GettokenSigru();
		if(!empty($token)){

			try {
				$lowermethod = strtolower($method);
				if ($method=="POST") {
					$response = (new ExternalResourceClient())->post($url, $this->data, ['Content-Type' => 'application/json','token'=>$token]);
				}
				if ($method=="PUT") {
					$response = (new ExternalResourceClient())->put($url, $this->data, ['Content-Type' => 'application/json','token'=>$token]);
				}
				if ($method=="DELETE") {
					$response = (new ExternalResourceClient())->delete($url, $this->data, ['Content-Type' => 'application/json','token'=>$token]);
				}
			    
			} catch (RequestException $e) {
			    throw new \SugarApiExceptionError($e->getMessage());
			}

			$parsed = !empty($response) ? json_decode($response->getBody()->getContents(), true) : null;

			

			$respuesta = array("Response"=>json_encode($parsed,true),"status"=>$response->getStatusCode());

			return $respuesta;

		}else{
			return array('Error' => "No hay token");
		}
	}
}

 ?>
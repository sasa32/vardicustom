<?php 

require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

/**
 * 
 */
class DeleteContact
{
	
	function __construct($id,$module)
	{
		require("custom/modules/integration_sigru/conf/confsigru.php");
		require_once("custom/modules/integration_sigru/Rest.php");
		//Instanciando el objeto bean del modulo (Cuentas o Contactos)
		$Record = BeanFactory::getBean($module,$id,array('disable_row_level_security' => true));

		$arrayaccount = $this->buildArrayContactDelete($Record);
		$json = $jsoncontactdelete = json_encode($arrayaccount);
		$rest = new Rest($conf,"deletecontact",$jsoncontactdelete);
		$respuesta = $rest->send_json();
		//$GLOBALS['log']->security("RespuestaDelete: ".print_r($respuesta,true));
		if ($respuesta['status']=='404') {
			preg_match("/<body[^>]*>(.*?)<\/body>/is", $respuesta['Response'], $matches);
			//$GLOBALS['log']->security("BodyRespuestaERROR: ".$matches[1]);
			$jsonerror = json_decode($matches[1]);
			if ($jsonerror->codigo == "3") {
				$this->write_table_log_error($Record,$module,$json,$matches);
			}
		}
		
		$this->write_table_log($Record,$module,$json,$respuesta);
		return $respuesta;
	}

	function buildArrayContactDelete($Record){
		$arratData = array();


		$GetAccountJuridica = $GLOBALS['db']->query("SELECT accounts_cstm.id_c, accounts_cstm.sasa_tipo_persona_c, accounts_cstm.sasa_numero_documento_c from accounts_contacts INNER JOIN accounts_cstm ON accounts_contacts.account_id=accounts_cstm.id_c where accounts_contacts.contact_id='{$Record->id}' AND accounts_cstm.sasa_tipo_persona_c='J'");
		$CuentaJuridica =  $GLOBALS['db']->fetchByAssoc($GetAccountJuridica);
		
		$arratData['sasa_nit_empresa'] = $CuentaJuridica['sasa_numero_documento_c'];
		$arratData['sasa_numero_documento_c']= $Record->sasa_numero_documento_c;

		return $arratData;
	}

	//Función para escribir en una tabla log los resultados de los consumos.
	public function write_table_log($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_send_sigru (
			id CHAR(36) PRIMARY KEY,
			fecha datetime,
			record VARCHAR(36),
			module VARCHAR(36),
			jsonsent text,
			responsesigru text,
			status VARCHAR(36))";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		//Insertar logs
		$respuestaSIGRU = addslashes($responsesigru['Response']);
		$QueryInsert = "INSERT INTO log_send_sigru (id, fecha, record,module,jsonsent,responsesigru,status) 
		VALUES (UUID(),'$fechaenvio','{$Record->id}','{$module}','{$jsonsent}','{$respuestaSIGRU}','{$responsesigru['status']}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}

	public function write_table_log_error($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_error_send_sigru (
			record VARCHAR(36)PRIMARY KEY,
			fecha datetime,
			module VARCHAR(36),
			jsonsent text,
			responsesigru text)";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		$respuestaSIGRU = addslashes($responsesigru[1]);
		//Insertar logs
		$QueryInsert = "REPLACE INTO log_error_send_sigru (fecha, record,module,jsonsent,responsesigru) 
		VALUES (now(),'{$Record->id}','{$module}','{$jsonsent}','{$respuestaSIGRU}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}
}

 ?>
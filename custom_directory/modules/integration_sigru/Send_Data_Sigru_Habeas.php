<?php 

require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

class Send_Data_Sigru_Habeas 
{
	//Funcion principal
	public function data($id,$module,$method){
		require("custom/modules/integration_sigru/conf/confsigru.php");
		require_once("custom/modules/integration_sigru/Rest.php");
		//Instanciando el objeto bean del modulo (Cuentas o Contactos)
		$Record = BeanFactory::getBean($module,$id,array('disable_row_level_security' => true));
		//Traer la estrcutura de campos definida por vardi 
		require_once("custom/modules/integration_sigru/conf/fields.php");
		$array_fields = new Get_Fields();
		$array_fields = $array_fields->arrayfields($module);
		
		//Identificar el modulo para construir el array
		if ($module == "SASA_Habeas_Data") {
			//$this->validate_statusServices($module,"habeasData");
			$arrayhabeas= $this->buildArrayHabeas($array_fields,$Record);
			$json = $jsonhabeas = json_encode($arrayhabeas);
			$rest = new Rest($conf,"habeas",$jsonhabeas);
			$respuesta = $rest->send_json($method);
			//$GLOBALS['log']->security("JSONHD ".$json);
			//$GLOBALS['log']->security("RESPONSE ".$respuesta['Response']);
		}
		if ($respuesta['status']=='404') {
			preg_match("/<body[^>]*>(.*?)<\/body>/is", $respuesta['Response'], $matches);
			//$GLOBALS['log']->security("BodyRespuestaERROR: ".$matches[1]);
			$jsonerror = json_decode($matches[1]);
			if ($jsonerror->codigo == "3") {
				$this->write_table_log_error($Record,$module,$json,$matches);
			}
		}
		$this->write_table_log($Record,$module,$json,$respuesta);
		return $respuesta;
	}
	public function validate_statusServices($module,$servicio){
		/*require_once 'modules/Emails/Email.php';
        require_once 'include/SugarPHPMailer.php';

        $curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://sigruweb.dinissan.com.co/ClienteExternaSugarNegocio/".$servicio."/status/",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 60,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json"
				),
			));
			
			$response= curl_exec($curl);
			$err = curl_error($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);

			if ($httpcode!='200') {
				$email_obj = new Email();
	            $defaults = $email_obj->getSystemDefaultEmail();
	            $mail = new SugarPHPMailer();
	            $mail->setMailerForSystem();
	            $mail->IsHTML(true);
	            $mail->From = $defaults["email"];
	            $mail->FromName = $defaults["name"];
	            $mail->Subject = "Error del servicio de ".$servicio;
	            $mail->Body = "Ha ocurrido un error en el servicio de ".$servicio. " por favor verificar con el area técnica";
	            $mail->prepForOutbound();
	            $mail->AddAddress("5@sasaconsultoria.com");
	             $GLOBALS['log']->security("ENVIAR EMAIL DE ERROR DE SERVICIO");
	             if (@$mail->Send()) {
	              }
			}
			$GLOBALS['log']->security("STATUS ".$httpcode);
			return $httpcode;*/

	}

	//Función para construir el array de cuentas, en este se hacen las validaciones de campos como traducción de valores
	public function buildArrayHabeas($array_fields,$Record){
		
		$arratData = array();
		foreach ($array_fields as $key => $campo) {
			$arratData[$campo] = $Record->$campo;
			switch ($campo) {
				case 'nit':
					if ($Record->leads_sasa_habeas_data_1leads_ida !="") {
						$idrecordrelated = $Record->leads_sasa_habeas_data_1leads_ida;
						$modulerelated= "Leads";
					}elseif ($Record->contacts_sasa_habeas_data_1contacts_ida !="") {
						$idrecordrelated = $Record->contacts_sasa_habeas_data_1contacts_ida;
						$modulerelated= "Contacts";
					}

					$RecordRelated = BeanFactory::getBean($modulerelated,$idrecordrelated,array('disable_row_level_security' => true));

					if ($RecordRelated->sasa_numero_documento_c != "") {
						$arratData['nit'] = $RecordRelated->sasa_numero_documento_c;
					}else{
						$arratData['nit'] = $RecordRelated->sasa_num_docu_juridica_c;
					}
					break;
				case 'idSuga':
					$arratData['idSuga'] = $Record->id;
					break;
				case 'obs':
					if ($Record->description == null || $Record->description == "") {
						$arratData['obs'] = "obs,.";
					}else{
						$arratData['obs'] = $Record->description.",..";
					}
					break;
				case 'canal':
					$arratData['canal'] = $Record->sasa_fuente_autorizacion_c;
					break;
				case 'cias':
					$arratData['cias'] = null;
					break;
				case 'codPrefCont':
					/*$canales = str_replace("^", "", $Record->sasa_canales_autorizados_c);
					$canales = explode(",", $canales);
					$arratData['codPrefCont'] = $canales;*/

					$preferencias = ["1","2","3","4","5","6","7","0","8","9"];
					$arratData['codPrefCont'] = $preferencias;
					
					break;
				case 'prefCont':

					$preferencias = ["1","2","3","4","5","6","7","0","8","9"];

					$canalesSugar = str_replace("^", "", $Record->sasa_canales_autorizados_c);
					$canalesSugar = explode(",", $canalesSugar);
					
					$CanalesAEnviar = [];
					foreach ($preferencias as $key => $value) {
						if (in_array($value, $canalesSugar) && $value!="0") {
							array_push($CanalesAEnviar, "S");
						}elseif (in_array($value, $canalesSugar) && $value=="0") {
							array_push($CanalesAEnviar, "S");
						}else{
							array_push($CanalesAEnviar, "N");
						}
					}
					$arratData['prefCont'] = $CanalesAEnviar;
					break;
				case 'principa':
					$arratData['principa'] = $Record->sasa_auto_contactacion_c;
					break;
				case 'ip':
					$arratData['ip'] = "127.0.0.1";
					break;
				case 'usuario':
					$arratData['usuario'] = $Record->modified_by_name;
					break;
				case 'crearArchivo':
					if ($Record->sasa_adjuntohabeasdata_c == "" || $Record->sasa_adjuntohabeasdata_c == null) {
						$arratData['crearArchivo'] = false;
					}else{
						$arratData['crearArchivo'] = true;
					}

					if ($Record->documents_sasa_habeas_data_1documents_ida == "060e244e-7930-11ec-97e6-06be4ff698d4") {
						$arratData['crearArchivo'] = true;
					}
					
					break;
				case 'codFormato':
					$DocumentRelated = BeanFactory::getBean('Documents',$Record->documents_sasa_habeas_data_1documents_ida,array('disable_row_level_security' => true));
					//$arratData['codFormato'] = $DocumentRelated->sasa_codigo_c;
					$arratData['codFormato'] = null;

					break;
				case 'bytes':
					require_once 'include/upload_file.php';
					if ($Record->documents_sasa_habeas_data_1documents_ida == "060e244e-7930-11ec-97e6-06be4ff698d4") {
						$DocumentRelated = BeanFactory::getBean('Documents',$Record->documents_sasa_habeas_data_1documents_ida,array('disable_row_level_security' => true));
						if ($Record->sasa_adjuntohabeasdata_c != "") {
							$file = new UploadFile();
							//get the file location
							$file->temp_file_location = UploadFile::get_upload_path($Record->id);
							$file_contents = $file->get_file_contents();
							$file2 = base64_encode($file_contents);
							

							$arratData['bytes'] = $file2;
						}else{
							$GLOBALS['log']->security("ENTRO HD");
							$file = new UploadFile();
							//get the file location
							$file->temp_file_location = UploadFile::realpath(UploadFile::get_upload_path($DocumentRelated->document_revision_id));
							$file_contents = $file->get_file_contents();
							$file2 = base64_encode($file_contents);
							

							$arratData['bytes'] = $file2;
						}
						
					}else{
						if ($Record->sasa_adjuntohabeasdata_c != "") {
							$file = new UploadFile();
							//get the file location
							$file->temp_file_location = UploadFile::get_upload_path($Record->id);
							$file_contents = $file->get_file_contents();
							$file2 = base64_encode($file_contents);
							

							$arratData['bytes'] = $file2;
						}else{
							$arratData['bytes'] = null;
						}
					}
					
					break;
				case 'sExt':
					if ($Record->documents_sasa_habeas_data_1documents_ida == "060e244e-7930-11ec-97e6-06be4ff698d4") {
						$DocumentRelated = BeanFactory::getBean('Documents',$Record->documents_sasa_habeas_data_1documents_ida,array('disable_row_level_security' => true));
						$ext = str_replace(" ", "", $DocumentRelated->filename);
						$ext = explode(".", $ext);
						$extfinal = end($ext);

						$arratData['sExt'] = $extfinal;
					}else{
						if ($Record->sasa_adjuntohabeasdata_c != "") {
							$ext = str_replace(" ", "", $Record->sasa_adjuntohabeasdata_c);
							$ext = explode(".", $ext);
							$extfinal = end($ext);

							$arratData['sExt'] = $extfinal;
						}else{
							$arratData['sExt'] = null;
						}
					}
					break;
				default:
					# code...
					break;
			}
		}
		return $arratData;
	}


	//Función para escribir en una tabla log los resultados de los consumos.
	public function write_table_log($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_send_sigru (
			id CHAR(36) PRIMARY KEY,
			fecha datetime,
			record VARCHAR(36),
			module VARCHAR(36),
			jsonsent text,
			responsesigru text,
			status VARCHAR(36))";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		$jsonsent2 = json_decode($jsonsent,true);
		unset($jsonsent2['bytes']);
		$jsonsent2 = json_encode($jsonsent2);

		//Insertar logs
		$QueryInsert = "INSERT INTO log_send_sigru (id, fecha, record,module,jsonsent,responsesigru,status) 
		VALUES (UUID(),'$fechaenvio','{$Record->id}','{$module}','{$jsonsent2}','{$responsesigru['Response']}','{$responsesigru['status']}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}

	public function write_table_log_error($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_error_send_sigru (
			record VARCHAR(36)PRIMARY KEY,
			fecha datetime,
			module VARCHAR(36),
			jsonsent text,
			responsesigru text)";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		//Insertar logs
		$QueryInsert = "REPLACE INTO log_error_send_sigru (fecha, record,module,jsonsent,responsesigru) 
		VALUES (now(),'{$Record->id}','{$module}','{$jsonsent}','{$responsesigru[1]}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}
}


 ?>
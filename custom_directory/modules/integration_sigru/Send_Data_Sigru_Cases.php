<?php 

require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

class Send_Data_Sigru_Cases
{
	//Funcion principal
	public function data($id,$module,$method){
		require_once("custom/modules/integration_sigru/conf/confsigru.php");
		require_once("custom/modules/integration_sigru/Rest.php");
		//Instanciando el objeto bean del modulo (Cuentas o Contactos)
		$Record = BeanFactory::getBean($module,$id,array('disable_row_level_security' => true));
		//Traer la estrcutura de campos definida por vardi 
		require_once("custom/modules/integration_sigru/conf/fields.php");
		$array_fields = new Get_Fields();
		$array_fields = $array_fields->arrayfields($module);

		//Identificar el modulo para construir el array
		if ($module == "Cases") {
			$arraycases = $this->buildArrayCases($array_fields,$Record);
			$json = $jsoncases = json_encode($arraycases);
			$rest = new Rest($conf,"cases",$jsoncases);
			$respuesta = $rest->send_json($method);
			$GLOBALS['log']->security(print_r($jsoncases,true));
			$GLOBALS['log']->security(print_r($respuesta,true));
		}
		//$GLOBALS['log']->security("JSON ".print_r($arraycases,true));
		if ($respuesta['status']=='404') {
			preg_match("/<body[^>]*>(.*?)<\/body>/is", $respuesta['Response'], $matches);
			//$GLOBALS['log']->security("BodyRespuestaERROR: ".$matches[1]);
			$jsonerror = json_decode($matches[1]);
			if ($jsonerror->codigo == "3") {
				$this->write_table_log_error($Record,$module,$json,$matches);
			}
		}
		
		$this->write_table_log($Record,$module,$json,$respuesta);
		return $respuesta;
	}

	public function buildArrayCases($array_fields,$Record){
		$arratData = array();

		foreach ($array_fields as $key => $campo) {
			$arratData[$campo] = $Record->$campo;
			switch ($campo) {
				case 'sasa_motivo_c':
					$unidadnegocio = substr($Record->cd_uneg_cont_c, -2);
					$subunidad = substr($Record->cd_area_c, -2);
					$querymotivo = $GLOBALS['db']->query("SELECT * FROM diccionario_motivos WHERE compania = '{$Record->cd_cia_c}' AND unidadnegocio='{$unidadnegocio}' AND subunidad='{$subunidad}' AND codigo_motivo_sugar='{$Record->sasa_motivo_c}' AND tipo_sugar='{$Record->sasa_tipo_c}' AND estado='A' LIMIT 1;");
					$MotivoDiccionario =  $GLOBALS['db']->fetchByAssoc($querymotivo);
					$arratData['sasa_motivo_c'] = $MotivoDiccionario['codigo'];
					//$GLOBALS['log']->security("TIPO {$MotivoDiccionario['tipo']} arr {$arratData['sasa_tipo_c']}");
					break;
				case 'sasa_tipo_c':
					$querytipo = $GLOBALS['db']->query("SELECT * FROM diccionario_motivos WHERE tipo_sugar='{$Record->sasa_tipo_c}' AND estado='A' LIMIT 1;");
					$TipoDiccionario =  $GLOBALS['db']->fetchByAssoc($querytipo);
					$arratData['sasa_tipo_c'] = $TipoDiccionario['tipo'];
					break;
				case 'date_entered':
					$date = new DateTime($Record->date_entered);
					$fechacreacion = $date->format('c');
					$arratData['date_entered'] = $fechacreacion;
					break;
				case 'date_modified':
					$date = new DateTime($Record->date_modified);
					$fechamodificacion = $date->format('c');
					$arratData['date_modified'] = $fechamodificacion;
					break;
				case 'sasa_cod_puntoaten_c':
					$Punto_De_Atencion = BeanFactory::getBean('sasa_Puntos_de_Ventas',$Record->sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida,array('disable_row_level_security' => true));
						$arratData['sasa_cod_puntoaten_c'] = $Punto_De_Atencion->sasa_codpuntoatencion_c;
					break;
				case 'sasa_cd_sucu':
					$Centro_Costos = BeanFactory::getBean('sasa_CentrosDeCostos',$Record->sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida,array('disable_row_level_security' => true));
						$arratData['sasa_cd_sucu'] = $Centro_Costos->sasa_codsucursal_c;
					break;
				case 'sasa_centrocostos_c':
					$Centro_Costos = BeanFactory::getBean('sasa_CentrosDeCostos',$Record->sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida,array('disable_row_level_security' => true));
						$arratData['sasa_centrocostos_c'] = $Centro_Costos->sasa_codcentrodecostos_c;
					break;
				//Mapeo de los campos de la Cuenta o Contacto
				case 'sasa_nu_nit_clie':
				$GLOBALS['log']->security("COntacto ".$Record->contacts_cases_1contacts_ida);
					if ($Record->account_id=="c5f0a53e-8cc3-11eb-b3f2-0286beac7abe") {
						$Record_related = BeanFactory::getBean('Contacts',$Record->contacts_cases_1contacts_ida,array('disable_row_level_security' => true));
						$relacion = "Contacts";
					}elseif ($Record->account_id=="a2590d86-50f6-11eb-9d24-0286beac7abe") {
						$Record_related = BeanFactory::getBean('Leads',$Record->leads_cases_1leads_ida,array('disable_row_level_security' => true));
						$relacion = "Leads";
					}else{
						$Record_related = BeanFactory::getBean('Accounts',$Record->account_id,array('disable_row_level_security' => true));
						$relacion = "Accounts";
					}
					
						$arratData['sasa_nu_nit_clie'] = $Record_related->sasa_numero_documento_c;
					break;
				case 'sasa_de_nomb':
						$arratData['sasa_de_nomb'] = $Record_related->sasa_nombres_c;
					break;
				case 'sasa_de_apel':
						$arratData['sasa_de_apel'] = $Record_related->sasa_primerapellido_c." ".$Record_related->sasa_last_name_2_c;
					break;
				case 'sasa_de_dire':
					if ($relacion == "Accounts") {
						$arratData['sasa_de_dire'] = $Record_related->billing_address_street;
					}else{
						$arratData['sasa_de_dire'] = $Record_related->primary_address_street;
					}
						
					break;
				case 'sasa_cd_ciud':
					if ($relacion == "Accounts") {
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record_related->sasa_municipios_accounts_1sasa_municipios_ida,array('disable_row_level_security' => true));
					}elseif ($relacion == "Leads") {
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record_related->sasa_municipios_leads_1sasa_municipios_ida,array('disable_row_level_security' => true));
					}else{
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record_related->sasa_municipios_contacts_1sasa_municipios_ida,array('disable_row_level_security' => true));
					}
					$arratData['sasa_cd_ciud'] = $Ciudad->sasa_codigomunicipio_c;
					break;
				case 'sasa_nu_tel_uno':
					if ($relacion == "Accounts") {
						$arratData['sasa_nu_tel_uno'] = $Record_related->sasa_phone_office_c;
					}else{
						$arratData['sasa_nu_tel_uno'] = $Record_related->sasa_phone_home_c;
					}
						
					break;
				case 'sasa_nu_tel_dos':
					if ($relacion == "Accounts") {
						$arratData['sasa_nu_tel_dos'] = $Record_related->sasa_phone_alternate_c;
					}else{
						$arratData['sasa_nu_tel_dos'] = $Record_related->sasa_phone_work_c;
					}
						
					break;
				case 'sasa_nu_celu':
					if ($relacion == "Accounts") {
						$arratData['sasa_nu_celu'] = $Record_related->sasa_cel_principal_c;
					}else{
						$arratData['sasa_nu_celu'] = $Record_related->sasa_phone_mobile_c;
					}
						
					break;
				case 'sasa_de_dire_elec':
						$arratData['sasa_de_dire_elec'] = $Record_related->email1;
					break;
				case 'sasa_cd_logi_upd':
					$arratData['sasa_cd_logi_upd'] = $Record->modified_by_name;
					break;
				case 'sasa_cd_logi_cre':
					$User = BeanFactory::getBean('Users',$Record->created_by,array('disable_row_level_security' => true));
					$arratData['sasa_cd_logi_cre'] = $User->user_name;
					break;
				default:
					# code...
					break;
			}
		}

		return $arratData;
	}

	//Función para escribir en una tabla log los resultados de los consumos.
	public function write_table_log($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_send_sigru (
			id CHAR(36) PRIMARY KEY,
			fecha datetime,
			record VARCHAR(36),
			module VARCHAR(36),
			jsonsent text,
			responsesigru text,
			status VARCHAR(36))";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		//Insertar logs
		$QueryInsert = "INSERT INTO log_send_sigru (id, fecha, record,module,jsonsent,responsesigru,status) 
		VALUES (UUID(),'$fechaenvio','{$Record->id}','{$module}','{$jsonsent}','{$responsesigru['Response']}','{$responsesigru['status']}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}

	public function write_table_log_error($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_error_send_sigru (
			record VARCHAR(36)PRIMARY KEY,
			fecha datetime,
			module VARCHAR(36),
			jsonsent text,
			responsesigru text)";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		//Insertar logs
		$QueryInsert = "REPLACE INTO log_error_send_sigru (fecha, record,module,jsonsent,responsesigru) 
		VALUES (now(),'{$Record->id}','{$module}','{$jsonsent}','{$responsesigru[1]}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}

}


 ?>
<?php 

require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

class Send_Data_Sigru 
{
	//Funcion principal
	public function data($id,$module){
		
		require("custom/modules/integration_sigru/conf/confsigru.php");
		require_once("custom/modules/integration_sigru/Rest.php");
		//Instanciando el objeto bean del modulo (Cuentas o Contactos)
		$Record = BeanFactory::getBean($module,$id,array('disable_row_level_security' => true));

		//Traer la estrcutura de campos definida por vardi 
		require_once("custom/modules/integration_sigru/conf/fields.php");
		$array_fields = new Get_Fields();
		$array_fields = $array_fields->arrayfields($module);
		
		//Identificar el modulo para construir el array
		if ($module == "Accounts") {
			//$this->validate_statusServices($module,"cliente");
			$arrayaccount = $this->buildArrayAccount($array_fields,$Record);
			$json = $jsonaccount = json_encode($arrayaccount);
			$rest = new Rest($conf,"cliente",$jsonaccount);
		}
		if ($module == "Contacts") {
			//$this->validate_statusServices($module,"contacto");
			$arraycontact = $this->BuilArrayContact($array_fields,$Record);
			$json = $jsoncontact = json_encode($arraycontact);
			$rest = new Rest($conf,"contacto",$jsoncontact);
		}
		if ($module == "Leads") {
			//$this->validate_statusServices($module,"leads");
			$arrayleads = $this->BuilArrayLeads($array_fields,$Record);
			$json = $jsonleads = json_encode($arrayleads);
			$rest = new Rest($conf,"leads",$jsonleads);
		}
		$respuesta = $rest->send_json();
		$GLOBALS['log']->security("Respuesta: ".print_r($respuesta,true));
		if ($respuesta['status']=='404') {
			preg_match("/<body[^>]*>(.*?)<\/body>/is", $respuesta['Response'], $matches);
			//$GLOBALS['log']->security("BodyRespuestaERROR: ".$matches[1]);
			$jsonerror = json_decode($matches[1]);
			if ($jsonerror->codigo == "3") {
				$this->write_table_log_error($Record,$module,$json,$matches);
			}
		}
		
		$this->write_table_log($Record,$module,$json,$respuesta);
		return $respuesta;
	}

	public function validate_statusServices($module,$servicio){
		/*require_once 'modules/Emails/Email.php';
        require_once 'include/SugarPHPMailer.php';

        $curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://sigruweb.dinissan.com.co/ClienteExternaSugarNegocio/".$servicio."/status/",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 60,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json"
				),
			));
			
			$response= curl_exec($curl);
			$err = curl_error($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);

			if ($httpcode!='200') {
				$email_obj = new Email();
	            $defaults = $email_obj->getSystemDefaultEmail();
	            $mail = new SugarPHPMailer();
	            $mail->setMailerForSystem();
	            $mail->IsHTML(true);
	            $mail->From = $defaults["email"];
	            $mail->FromName = $defaults["name"];
	            $mail->Subject = "Error del servicio de ".$servicio;
	            $mail->Body = "Ha ocurrido un error en el servicio de ".$servicio. " por favor verificar con el area técnica";
	            $mail->prepForOutbound();
	            $mail->AddAddress("5@sasaconsultoria.com");
	             $GLOBALS['log']->security("ENVIAR EMAIL DE ERROR DE SERVICIO");
	             if (@$mail->Send()) {
	              }
			}
			$GLOBALS['log']->security("STATUS ".$httpcode);
			return $httpcode;*/

	}

	//Función para construir el array de cuentas, en este se hacen las validaciones de campos como traducción de valores
	public function buildArrayAccount($array_fields,$Record){
		$tipo = array("sasa_tipotel1_c","sasa_tipotel2_c","sasa_tipotel3_c","sasa_tipodirec1_c","sasa_tipodirec2_c","sasa_tipodirec3_c");
		$arratData = array();
		foreach ($array_fields as $key => $campo) {
				$arratData[$campo] = $Record->$campo;
				switch ($campo) {
					//Campos del contacto
					case 'sasa_nivel_educativo_c':
						/*$queryContact= $GLOBALS['db']->query("SELECT * FROM contacts_cstm WHERE contacts_cstm.sasa_numero_documento_c='{$Record->sasa_numero_documento_c}' AND contacts_cstm.sasa_tipo_documento_c='{$Record->sasa_tipo_documento_c}'");
						$Contacto =  $GLOBALS['db']->fetchByAssoc($queryContact);*/

						$Contact = BeanFactory::newBean("Contacts");
						$Contact->retrieve_by_string_fields( 
							array( 
								'sasa_numero_documento_c' => $Record->sasa_numero_documento_c, 
								'sasa_tipo_documento_c' => $Record->sasa_tipo_documento_c
							) 
						);
						$arratData['sasa_nivel_educativo_c'] = $Contact->sasa_nivel_educativo_c;
						break;
					case 'sasa_profesion_c':
						$arratData['sasa_profesion_c'] = $Contact->sasa_profesion_c;
						break;
					case 'sasa_ocupacion_c':
						$arratData['sasa_ocupacion_c'] = $Contact->sasa_ocupacion_c;
						break;
					case 'sasa_cargo_c':
						$arratData['sasa_cargo_c'] = $Contact->sasa_cargo_c;
						break;
					case 'sasa_tipo_de_vivienda_c':
						$arratData['sasa_tipo_de_vivienda_c'] = $Contact->sasa_tipo_de_vivienda_c;
						break;
					case 'sasa_estrato_c':
						$arratData['sasa_estrato_c'] = $Contact->sasa_estrato_c;
						break;
					case 'sasa_estado_civil_c':
						$arratData['sasa_estado_civil_c'] = $Contact->sasa_estado_civil_c;
						break;
					case 'sasa_tiene_hijos_c':
						$arratData['sasa_tiene_hijos_c'] = $Contact->sasa_tiene_hijos_c;
						break;
					case 'sasa_numero_hijos_c':
						$arratData['sasa_numero_hijos_c'] = $Contact->sasa_numero_hijos_c;
						break;
					case 'birthdate':
						$arratData['birthdate'] = $Contact->birthdate;
						break;
					case 'sasa_edad_c':
						$arratData['sasa_edad_c'] = $Contact->sasa_edad_c;
						break;
					case 'sasa_genero_c':
						$arratData['sasa_genero_c'] = $Contact->sasa_genero_c;
						break;
					case 'sasa_cliente_fallecido_c':
						$arratData['sasa_cliente_fallecido_c'] = $Contact->sasa_cliente_fallecido_c;
						break;
					//************************** FIN CAMPOS CONTACTOS ************************************

					//Ciudad de Tel 1
					case 'sasa_ciudad_tel_principal_2_c':
						$queryciudadtel1 = $GLOBALS['db']->query("SELECT sasa_municipios.sasa_codigomunicipio_c from sasa_municipios_accounts_3_c INNER JOIN sasa_municipios ON sasa_municipios.id=sasa_municipios_accounts_3_c.sasa_municipios_accounts_3sasa_municipios_ida where sasa_municipios_accounts_3_c.sasa_municipios_accounts_3accounts_idb='{$Record->id}' AND sasa_municipios.deleted=0 AND sasa_municipios_accounts_3_c.deleted=0");
						$Ciudadtel1 =  $GLOBALS['db']->fetchByAssoc($queryciudadtel1);
						
						if ($Record->sasa_phone_office_c == "") {
							$arratData['sasa_ciudad_tel_principal_2_c'] = "11001";
						}else{
							$arratData['sasa_ciudad_tel_principal_2_c'] = $Ciudadtel1['sasa_codigomunicipio_c'];
						}

						// $Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_accounts_3sasa_municipios_ida,array('disable_row_level_security' => true));
						// $arratData['sasa_ciudad_tel_principal_2_c'] = $Ciudad->sasa_codigomunicipio_c;;
						break;
					//Ciudad de Tel 2
					case 'sasa_ciudad_tel_alternativ_2_c':
						$queryciudadtel2 = $GLOBALS['db']->query("SELECT sasa_municipios.sasa_codigomunicipio_c from sasa_municipios_accounts_4_c INNER JOIN sasa_municipios ON sasa_municipios.id=sasa_municipios_accounts_4_c.sasa_municipios_accounts_4sasa_municipios_ida where sasa_municipios_accounts_4_c.sasa_municipios_accounts_4accounts_idb='{$Record->id}' AND sasa_municipios.deleted=0 AND sasa_municipios_accounts_4_c.deleted=0");
						$Ciudadtel2 =  $GLOBALS['db']->fetchByAssoc($queryciudadtel2);
						$arratData['sasa_ciudad_tel_alternativ_2_c'] = $Ciudadtel2['sasa_codigomunicipio_c'];
						// $Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_accounts_4sasa_municipios_ida,array('disable_row_level_security' => true));
						// $arratData['sasa_ciudad_tel_alternativ_2_c'] = $Ciudad->sasa_codigomunicipio_c;
						break;
					//Ciudad de Tel 3
					case 'sasa_ciudad_tel_3_c':
						$queryciudadtel3 = $GLOBALS['db']->query("SELECT sasa_municipios.sasa_codigomunicipio_c from sasa_municipios_accounts_5_c INNER JOIN sasa_municipios ON sasa_municipios.id=sasa_municipios_accounts_5_c.sasa_municipios_accounts_5sasa_municipios_ida where sasa_municipios_accounts_5_c.sasa_municipios_accounts_5accounts_idb='{$Record->id}' AND sasa_municipios.deleted=0 AND sasa_municipios_accounts_5_c.deleted=0");
						$Ciudadtel3 =  $GLOBALS['db']->fetchByAssoc($queryciudadtel3);
						$arratData['sasa_ciudad_tel_3_c'] = $Ciudadtel3['sasa_codigomunicipio_c'];
						/*$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_accounts_5sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['sasa_ciudad_tel_3_c'] = $Ciudad->sasa_codigomunicipio_c;*/
						break;
					//Ciudad de Direccion 1
					case 'sasa_municipio_principal_2_c':
						$queryciudad1 = $GLOBALS['db']->query("SELECT sasa_municipios.sasa_codigomunicipio_c from sasa_municipios_accounts_1_c INNER JOIN sasa_municipios ON sasa_municipios.id=sasa_municipios_accounts_1_c.sasa_municipios_accounts_1sasa_municipios_ida where sasa_municipios_accounts_1_c.sasa_municipios_accounts_1accounts_idb='{$Record->id}' AND sasa_municipios.deleted=0 AND sasa_municipios_accounts_1_c.deleted=0");
						$Ciudad1 =  $GLOBALS['db']->fetchByAssoc($queryciudad1);
						//Llevar la ciudad del tel1 en caso de que no tenga dirección 1
						if ($Ciudad1['sasa_codigomunicipio_c'] != "" || $Ciudad1['sasa_codigomunicipio_c'] != null) {
							$arratData['sasa_municipio_principal_2_c'] = $Ciudad1['sasa_codigomunicipio_c'];
						}else{
							$queryciudadtel1 = $GLOBALS['db']->query("SELECT sasa_municipios.sasa_codigomunicipio_c from sasa_municipios_accounts_3_c INNER JOIN sasa_municipios ON sasa_municipios.id=sasa_municipios_accounts_3_c.sasa_municipios_accounts_3sasa_municipios_ida where sasa_municipios_accounts_3_c.sasa_municipios_accounts_3accounts_idb='{$Record->id}' AND sasa_municipios.deleted=0 AND sasa_municipios_accounts_3_c.deleted=0");
							$Ciudadtel1 =  $GLOBALS['db']->fetchByAssoc($queryciudadtel1);
							$arratData['sasa_municipio_principal_2_c'] = $Ciudadtel1['sasa_codigomunicipio_c'];
						}
						/*$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_accounts_1sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['sasa_municipio_principal_2_c'] = $Ciudad->sasa_codigomunicipio_c;*/
						break;
					//Ciudad de Direccion 2
					case 'sasa_municipio_secundario_2_c':
						$queryciudad2 = $GLOBALS['db']->query("SELECT sasa_municipios.sasa_codigomunicipio_c from sasa_municipios_accounts_2_c INNER JOIN sasa_municipios ON sasa_municipios.id=sasa_municipios_accounts_2_c.sasa_municipios_accounts_2sasa_municipios_ida where sasa_municipios_accounts_2_c.sasa_municipios_accounts_2accounts_idb='{$Record->id}' AND sasa_municipios.deleted=0 AND sasa_municipios_accounts_2_c.deleted=0");
						$Ciudad2 =  $GLOBALS['db']->fetchByAssoc($queryciudad2);
						$arratData['sasa_municipio_secundario_2_c'] = $Ciudad2['sasa_codigomunicipio_c'];
						/*$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_accounts_2sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['sasa_municipio_secundario_2_c'] = $Ciudad->sasa_codigomunicipio_c;*/
						break;
					//Ciudad de Direccion 3
					case 'sasa_municipio_3_c':
						$queryciudad3 = $GLOBALS['db']->query("SELECT sasa_municipios.sasa_codigomunicipio_c from sasa_municipios_accounts_6_c INNER JOIN sasa_municipios ON sasa_municipios.id=sasa_municipios_accounts_6_c.sasa_municipios_accounts_6sasa_municipios_ida where sasa_municipios_accounts_6_c.sasa_municipios_accounts_6accounts_idb='{$Record->id}' AND sasa_municipios.deleted=0 AND sasa_municipios_accounts_6_c.deleted=0");
						$Ciudad3 =  $GLOBALS['db']->fetchByAssoc($queryciudad3);
						$arratData['sasa_municipio_3_c'] = $Ciudad3['sasa_codigomunicipio_c'];
						/*$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_accounts_6sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['sasa_municipio_3_c'] = $Ciudad->sasa_codigomunicipio_c;*/
						break;
					//Mapeo de campos de telefono ya que no estan iguales a los que se tienen en sugar.
					case 'sasa_tel_phone_other_c':
						$arratData['sasa_tel_phone_other_c'] = $Record->sasa_phone_other_c;
						break;
					case 'sasa_cel_otro_c':
						$arratData['sasa_cel_otro_c'] = $Record->sasa_celular_otro_c;
						break;
					//Construir objeto de email, hay reglas que se debieron implementar como el true o el false ya que el objeto de sugar retorna 0 o 1 en lugar de tru o false para las opciones del email
					case 'listEmail':

						$query = $GLOBALS['db']->query("SELECT IF(email_addresses.invalid_email=0, 'false', 'true')invalid_email, IF(email_addresses.opt_out=0, 'false', 'true')opt_out, IF(email_addr_bean_rel.primary_address=0, 'false', 'true')primary_address, email_addresses.email_address FROM email_addr_bean_rel INNER JOIN email_addresses ON email_addr_bean_rel.email_address_id=email_addresses.id WHERE bean_id='{$Record->id}' AND bean_module = 'Accounts' and email_addr_bean_rel.deleted = 0");
						$num = $query->num_rows;
						
						while ($row = $GLOBALS['db']->fetchByAssoc($query)) {
							$arratData['listEmail'][]=array("email_address"=>$row['email_address'],"invalid_email"=>$row['invalid_email'],"opt_out"=>$row['opt_out'],"primary_address"=>$row['primary_address']);
						}
						break;
					case 'sasa_fuente_c':
						$arratData['sasa_fuente_c'] = "Sugar";
						break;
					default:
						# code...
						break;
				}
				//Traducción de tipos de telefono y direcciones
				foreach ($tipo as $key => $value) {
					if ($campo==$value) {
						if ($Record->$campo=="C"){ 
							$arratData[$value] = "1";
						}elseif ($Record->$campo=="OF") {
							$arratData[$value] = "2";
						}elseif ($Record->$campo=="OT") {
							$arratData[$value] = "6";
						}
					}
				}
		}
		return $arratData;
	}
	//Función para construir el array de Contactos, en este se hacen las validaciones de campos como traducción de valores
	public function BuilArrayContact($array_fields,$Record){
		$tipo = array("sasa_tipotel1_c","sasa_tipotel2_c","sasa_tipotel3_c","sasa_tipodirec1_c","sasa_tipodirec2_c","sasa_tipodirec3_c");
		$arratData = array();
		foreach ($array_fields as $key => $campo) {
				$arratData[$campo] = $Record->$campo;
				switch ($campo) {
					//Asignación de Nit de la empresa
					case 'sasa_nit_empresa':
						$GetAccountJuridica = $GLOBALS['db']->query("SELECT accounts_cstm.id_c, accounts_cstm.sasa_tipo_persona_c, accounts_cstm.sasa_numero_documento_c from accounts_contacts INNER JOIN accounts_cstm ON accounts_contacts.account_id=accounts_cstm.id_c where accounts_contacts.contact_id='{$Record->id}' AND accounts_cstm.sasa_tipo_persona_c='J'");
						$CuentaJuridica =  $GLOBALS['db']->fetchByAssoc($GetAccountJuridica);
						
						$arratData['sasa_nit_empresa'] = $CuentaJuridica['sasa_numero_documento_c'];
						break;
					//Ciudad de Tel 1
					case 'sasa_ciudad_tel_principal_2_c':
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_contacts_3sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['sasa_ciudad_tel_principal_2_c'] = $Ciudad->sasa_codigomunicipio_c;;
						break;
					//Ciudad de Tel 2
					case 'sasa_ciudad_tel_oficina_2_c':
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_contacts_4sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['sasa_ciudad_tel_oficina_2_c'] = $Ciudad->sasa_codigomunicipio_c;
						break;
					//Ciudad de Tel 3
					case 'sasa_ciudad_tel_oficina_3_c':
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_contacts_5sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['sasa_ciudad_tel_oficina_3_c'] = $Ciudad->sasa_codigomunicipio_c;
						break;
					//Ciudad de Direccion 1
					case 'sasa_municipio_principal_2_c':
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_contacts_1sasa_municipios_ida,array('disable_row_level_security' => true));
						if (empty($Ciudad->sasa_codigomunicipio_c)) {
							//Validar si la ciudad del telefono no está vacia
							if ($arratData['sasa_ciudad_tel_principal_2_c'] != "" || $arratData['sasa_ciudad_tel_principal_2_c']!= null) {
								$arratData['sasa_municipio_principal_2_c'] = $arratData['sasa_ciudad_tel_principal_2_c'];
							}else{
								//Si esta vacio ir a la cuenta relacionada para extarer la ciudad 1
								$GetAccountJuridica = $GLOBALS['db']->query("SELECT accounts_cstm.id_c, accounts_cstm.sasa_tipo_persona_c, accounts_cstm.sasa_numero_documento_c from accounts_contacts INNER JOIN accounts_cstm ON accounts_contacts.account_id=accounts_cstm.id_c where accounts_contacts.contact_id='{$Record->id}' AND accounts_contacts.deleted=0");
								while($row = $GLOBALS['db']->fetchByAssoc($GetAccountJuridica)){
									$GLOBALS['log']->security("Cuentas: ".$row['id_c']);
									$Account = BeanFactory::retrieveBean('Accounts', $row['id_c'], array('disable_row_level_security' => true));
									$Ciudaddire1 = BeanFactory::getBean('sasa_Municipios',$Account->sasa_municipios_accounts_1sasa_municipios_ida,array('disable_row_level_security' => true));
									$Ciudadtel1 = BeanFactory::getBean('sasa_Municipios',$Account->sasa_municipios_accounts_3sasa_municipios_ida,array('disable_row_level_security' => true));
									$GLOBALS['log']->security("Muni: ".$Ciudaddire1->sasa_codigomunicipio_c);
									$GLOBALS['log']->security("tele: ".$Ciudadtel1->sasa_codigomunicipio_c);
									if (!empty($Ciudaddire1->sasa_codigomunicipio_c)) {
										$arratData['sasa_municipio_principal_2_c'] = $Ciudaddire1->sasa_codigomunicipio_c;
										break;
									}elseif (!empty($Ciudadtel1->sasa_codigomunicipio_c)) {
										$arratData['sasa_municipio_principal_2_c'] = $Ciudadtel1->sasa_codigomunicipio_c;
										break;
									}
								}
								
							}
						}else{
							$arratData['sasa_municipio_principal_2_c'] = $Ciudad->sasa_codigomunicipio_c;
						}
						
						break;
					//Ciudad de Direccion 2
					case 'sasa_municipio_secundario_2_c':
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_contacts_2sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['sasa_municipio_secundario_2_c'] = $Ciudad->sasa_codigomunicipio_c;
						break;
					//Ciudad de Direccion 3
					case 'sasa_municipio_3_c':
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_contacts_6sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['sasa_municipio_3_c'] = $Ciudad->sasa_codigomunicipio_c;
						break;
					//Mapeo de campos de telefono ya que no estan iguales a los que se tienen en sugar.
					//Celular 1
					case 'sasa_cel_principal_c':
						$arratData['sasa_cel_principal_c'] = $Record->sasa_phone_mobile_c;
						break;
					//Telefono 1
					case 'sasa_phone_office_c':
						$arratData['sasa_phone_office_c'] = $Record->sasa_phone_home_c;
						break;
					//Telefono 2
					case 'sasa_phone_alternate_c':
						$arratData['sasa_phone_alternate_c'] = $Record->sasa_phone_work_c;
						break;
					//Construir objeto de email, hay reglas que se debieron implementar como el true o el false ya que el objeto de suhar retorna 0 o 1 en lugar de tru o false para las opciones del email
					case 'listEmail':
						$optEmail = array("invalid_email","opt_out","primary_address");
						foreach ($Record->email as $key => $value) {
							for ($i=0; $i < count($Record->email); $i++) {
								if ($i==$key) {
									foreach ($optEmail as $llave => $valor) {
										switch ($valor) {
											case 'invalid_email':
												if ($value[$valor]==0) {
													$invalid_email = "false";
												}elseif ($value[$valor]==1) {
													$invalid_email = "true";
												}
												break;
											case 'opt_out':
												if ($value[$valor]==0) {
													$opt_out = "false";
												}elseif ($value[$valor]==1) {
													$opt_out = "true";
												}
												break;
											case 'primary_address':
												if ($value[$valor]==0) {
													$primary_address = "false";
												}elseif ($value[$valor]==1) {
													$primary_address = "true";
												}
												break;
											default:
												# code...
												break;
										}
										$arratData['listEmail'][$i]=array("email_address"=>$value['email_address'],"invalid_email"=>$invalid_email,"opt_out"=>$opt_out,"primary_address"=>$primary_address);
									}
								}
							}
						}
						break;
					case 'sasa_mascota_c':
						$replacestrmascotas = str_replace("^", "", $Record->sasa_mascota_c);
						$mascotas = explode(",", $replacestrmascotas);
						$arratData['sasa_mascota_c'] = array();
						//Asignar valores al array
						for ($i=0; $i <count($mascotas) ; $i++) {
							array_push($arratData['sasa_mascota_c'],$mascotas[$i]);
						}
						break;
					case 'sasa_deportes_c':
						$replacestrdeportes = str_replace("^", "", $Record->sasa_deportes_c);
						$deportes = explode(",", $replacestrdeportes);
						$arratData['sasa_deportes_c'] = array();
						//Asignar valores al array
						for ($i=0; $i <count($deportes) ; $i++) {
							array_push($arratData['sasa_deportes_c'],$deportes[$i]);
						}
						break;
					case 'sasa_musica_c':
						$replacestrmusica = str_replace("^", "", $Record->sasa_musica_c);
						$musica = explode(",", $replacestrmusica);
						$arratData['sasa_musica_c'] = array();
						//Asignar valores al array
						for ($i=0; $i <count($musica) ; $i++) {
							array_push($arratData['sasa_musica_c'],$musica[$i]);
						}
						break;
					case 'sasa_fuente_c':
						$arratData['sasa_fuente_c'] = "Sugar";
						break;
					default:
						# code...
						break;
				}
				//Traducción de tipos de telefono y direcciones
				foreach ($tipo as $key => $value) {
					if ($campo==$value) {
						if ($Record->$campo=="C"){ 
							$arratData[$value] = "1";
						}elseif ($Record->$campo=="OF") {
							$arratData[$value] = "2";
						}elseif ($Record->$campo=="OT") {
							$arratData[$value] = "6";
						}
					}
				}
		}
		return $arratData;
	}
	//Función para construir el array de Leads, en este se hacen las validaciones de campos como traducción de valores
	public function BuilArrayLeads($array_fields,$Record){
		$arratData = array();
		foreach ($array_fields as $key => $campo) {
				$arratData[$campo] = $Record->$campo;
				switch ($campo) {
					case 'sasa_tipo_persona_c':
						$arratData['sasa_tipo_persona_c'] = $Record->sasa_tipo_de_persona_c;
						break;
					case 'sasa_numero_documento_c':
						if ($Record->sasa_tipo_de_persona_c == "J") {
							$arratData['sasa_numero_documento_c'] = $Record->sasa_num_docu_juridica_c;
						}else{
							$arratData['sasa_numero_documento_c'] = $Record->sasa_numero_documento_c;
						}
						break;
					//Asignación de Nit de la empresa
					case 'compannia':
						$arratData['compannia'] = $Record->sasa_compania_c;
						break;
					case 'unidad_de_negocio_primaria':
						$arratData['unidad_de_negocio_primaria'] = $Record->sasa_unidad_de_negocio_c;
						break;
					case 'sasa_razon_social':
						$arratData['sasa_razon_social'] = $Record->account_name;
						break;
					//Pasar el email principal
					case 'email':
						$arratData['email'] = $Record->email1;
						break;
					//Pasar el código de la linea de interés y el nombre de la marca.
					case 'sasa_linea_interes_c':
						$LineaVehiculo = BeanFactory::getBean('sasa_vehiculos',$Record->sasa_vehiculos_leads_1sasa_vehiculos_ida,array('disable_row_level_security' => true));
						$Marca = BeanFactory::getBean('sasa_Marcas',$LineaVehiculo->sasa_marcas_sasa_vehiculos_1sasa_marcas_ida,array('disable_row_level_security' => true));
						$arratData['sasa_linea_interes_c'] = $LineaVehiculo->sas_cd_line_vehi_c;
						$arratData['sasa_marca_interes_c'] = $Marca->name;
						break;
					//Traducción de los telefonos
					case 'phone_home':
						$arratData['phone_home'] = $Record->sasa_phone_home_c;
						break;
					case 'phone_mobile':
						$arratData['phone_mobile'] = $Record->sasa_phone_mobile_c;
						break;
					case 'unidad_de_negocio_secundaria':
						$arratData['unidad_de_negocio_secundaria'] = $Record->cd_area_c;
						break;
					case 'primary_address_city':
						$Ciudad = BeanFactory::getBean('sasa_Municipios',$Record->sasa_municipios_leads_1sasa_municipios_ida,array('disable_row_level_security' => true));
						$arratData['primary_address_city'] = $Ciudad->sasa_codigomunicipio_c;
						
						$Departamento = BeanFactory::getBean('sasa_Departamentos',$Ciudad->sasa_departamentos_sasa_municipios_1sasa_departamentos_ida,array('disable_row_level_security' => true));
						$arratData['primary_address_state'] = $Departamento->sasa_codigodepartamento_c;
						break;
					default:
						# code...
						break;
				}
		}
		return $arratData;
	}

	//Función para escribir en una tabla log los resultados de los consumos.
	public function write_table_log($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_send_sigru (
			id CHAR(36) PRIMARY KEY,
			fecha datetime,
			record VARCHAR(36),
			module VARCHAR(36),
			jsonsent text,
			responsesigru text,
			status VARCHAR(36))";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		//Insertar logs
		$respuestaSIGRU = addslashes($responsesigru['Response']);
		$QueryInsert = "INSERT INTO log_send_sigru (id, fecha, record,module,jsonsent,responsesigru,status) 
		VALUES (UUID(),'$fechaenvio','{$Record->id}','{$module}','{$jsonsent}','{$respuestaSIGRU}','{$responsesigru['status']}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}

	public function write_table_log_error($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_error_send_sigru (
			record VARCHAR(36)PRIMARY KEY,
			fecha datetime,
			module VARCHAR(36),
			jsonsent text,
			responsesigru text)";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		$respuestaSIGRU = addslashes($responsesigru[1]);
		//Insertar logs
		$QueryInsert = "REPLACE INTO log_error_send_sigru (fecha, record,module,jsonsent,responsesigru) 
		VALUES (now(),'{$Record->id}','{$module}','{$jsonsent}','{$respuestaSIGRU}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}
}


 ?>
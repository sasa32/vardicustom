<?php 

require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

class Send_Data_Sigru_Notes 
{
	//Funcion principal
	public function data($id,$module,$id_adjuntos=null,$extfile=null, $file_name=null){
		require("custom/modules/integration_sigru/conf/confsigru.php");
		require_once("custom/modules/integration_sigru/Rest.php");
		//Instanciando el objeto bean del modulo (Cuentas o Contactos)
		$Record = BeanFactory::getBean($module,$id,array('disable_row_level_security' => true));
		//Traer la estrcutura de campos definida por vardi 
		require_once("custom/modules/integration_sigru/conf/fields.php");
		$array_fields = new Get_Fields();
		$array_fields = $array_fields->arrayfields($module);
		
		//Identificar el modulo para construir el array
		if ($module == "Notes") {
			$arraynotes= $this->buildArrayNotes($array_fields,$Record,$id_adjuntos,$extfile,$file_name);
			$json = $jsonnotes = json_encode($arraynotes);
			$rest = new Rest($conf,"notes",$jsonnotes);
			$respuesta = $rest->send_json();
			//$GLOBALS['log']->security(print_r($jsonnotes,true));
		}
		$GLOBALS['log']->security(print_r($respuesta,true));
		if ($respuesta['status']=='404') {
			preg_match("/<body[^>]*>(.*?)<\/body>/is", $respuesta['Response'], $matches);
			//$GLOBALS['log']->security("BodyRespuestaERROR: ".$matches[1]);
			$jsonerror = json_decode($matches[1]);
			if ($jsonerror->codigo == "3") {
				$this->write_table_log_error($Record,$module,$json,$matches);
			}
		}
		$this->write_table_log($Record,$module,$json,$respuesta);
		return $respuesta;
	}

	//Función para construir el array de cuentas, en este se hacen las validaciones de campos como traducción de valores
	public function buildArrayNotes($array_fields,$Record,$id_adjuntos,$extfile,$file_name){
		
		$arratData = array();
		foreach ($array_fields as $key => $campo) {
			$arratData[$campo] = $Record->$campo;
			switch ($campo) {
				case 'id':
					if ($id_adjuntos != null) {
						$arratData['id'] = $id_adjuntos;
					}else{
						$arratData['id'] = $Record->id;
					}
					break;
				// case 'name':
				// 	$arratData['name'] .= " ".$file_name;
				// 	break;
				case 'sasa_id_avan':
					$arratData['sasa_id_avan'] = $Record->sasa_id_avan_c;
					break;
				case 'sasa_id_tipo':
					$arratData['sasa_id_tipo'] = $Record->sasa_id_tipo_c;
					break;
				case 'date_entered':
					$FechaCreacion = new DateTime($Record->date_entered);
						
					$arratData['date_entered'] = $FechaCreacion->format("Y-m-d")."T".$FechaCreacion->format("H:i:s");
					break;
				case 'date_modified':
					$FechaModi = new DateTime($Record->date_modified);
						
					$arratData['date_modified'] = $FechaModi->format("Y-m-d")."T".$FechaModi->format("H:i:s");
					break;
				case 'sasa_usuario_avance_c':
					$User = BeanFactory::getBean('Users',$Record->created_by,array('disable_row_level_security' => true));
					$arratData['sasa_usuario_avance_c'] = $User->user_name;
					break;
				case 'documentoSoporteAvancePQRFDTO':
					if ($id_adjuntos != null) {
						$file = new UploadFile();
							//get the file location
							$file->temp_file_location = UploadFile::get_upload_path($id_adjuntos);
							$file_contents = $file->get_file_contents();
							$file2 = base64_encode($file_contents);
							//$GLOBALS['log']->security("Respuesta2".print_r($file2,true));
							$jsonsend = new stdClass();
							$jsonsend->bytesDocumentoAdjunto = $file2;
							$jsonsend->extensionDocumentoAdjunto = $extfile;

							$arratData['documentoSoporteAvancePQRFDTO'] = $jsonsend;
					}else{
						$arratData['documentoSoporteAvancePQRFDTO'] = null;
					}
					break;
				
				default:
					# code...
					break;
			}
		}
		return $arratData;
	}


	//Función para escribir en una tabla log los resultados de los consumos.
	public function write_table_log($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_send_sigru (
			id CHAR(36) PRIMARY KEY,
			fecha datetime,
			record VARCHAR(36),
			module VARCHAR(36),
			jsonsent text,
			responsesigru text,
			status VARCHAR(36))";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		$jsonsent2 = json_decode($jsonsent,true);
		unset($jsonsent2['bytesDocumentoAdjunto']);
		$jsonsent2 = json_encode($jsonsent2);

		//Insertar logs
		$QueryInsert = "INSERT INTO log_send_sigru (id, fecha, record,module,jsonsent,responsesigru,status) 
		VALUES (UUID(),'$fechaenvio','{$Record->id}','{$module}','{$jsonsent2}','{$responsesigru['Response']}','{$responsesigru['status']}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}

	public function write_table_log_error($Record,$module,$jsonsent,$responsesigru){
		//Crear tabla log
		$queryCreateTable="CREATE TABLE IF NOT EXISTS log_error_send_sigru (
			record VARCHAR(36)PRIMARY KEY,
			fecha datetime,
			module VARCHAR(36),
			jsonsent text,
			responsesigru text)";
		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($queryCreateTable);

		$fechaenvio = date('Y-m-d H:i:s');

		//Insertar logs
		$QueryInsert = "REPLACE INTO log_error_send_sigru (fecha, record,module,jsonsent,responsesigru) 
		VALUES (now(),'{$Record->id}','{$module}','{$jsonsent}','{$responsesigru[1]}')";
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);
	}
}


 ?>
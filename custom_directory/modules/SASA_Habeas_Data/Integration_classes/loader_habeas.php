<?php  
	
	/**
	 * 
	 */
	require_once('include/SugarQuery/SugarQuery.php');
	class Loader_Habeas{
		var $return;
		var $db;
		var $nuco;
		var $datev;
		
		function __construct($data_array){
			$this->return = array();
			$this->db = DBManagerFactory::getInstance();
			$this->main_habeas($data_array);
		}

		public function main_habeas($data_array){
			try{
				foreach ($data_array as $key => $value) {
					$validate = $this->validate_record($value);
					if($validate['band']){
						//$status = $this->create_habeas($value);
						$status = $this->valida_habeassindoc($value);
						$this->return[] = $status;
					}else{
						$this->return[] =array('Error' => '120', 'Description' => implode(' - ', $validate['error']));
					}
				}
			}catch(Exception $e){
				return array('Error' => '100', 'Description' => $e->getMessage());
			}
		}

		public function create_habeas($record){
			
			if ($record['id_register_c']!=null) {
				$sql_get_habeas= "SELECT id FROM sasa_habeas_data INNER JOIN sasa_habeas_data_cstm ON sasa_habeas_data.id=sasa_habeas_data_cstm.id_c WHERE sasa_lote_c='{$record['sasa_lote_c']}' AND id_register_c='{$record['id_register_c']}' AND deleted=0";
			}

			$habeasq =  $this->db->query($sql_get_habeas);
			while($row = $this->db->fetchByAssoc($habeasq)){
				$habeas_state_id = $row['id'];
			}

			if (!empty($habeas_state_id)) {
				$status = "Update";
				$Habeas = BeanFactory::getBean('SASA_Habeas_Data', $habeas_state_id, array('disable_row_level_security' => true));
			}else{
				$status = "Created";
				$Habeas = BeanFactory::newBean('SASA_Habeas_Data');
			}

			
			

			foreach ($record as $key => $value) {
				if ($value == "(null)") {
					$Habeas->{$key} = null;
				}else{
					$Habeas->{$key} = $value;
					switch ($key) {
						case 'contacto':
							$numerocontacto = rtrim($value);
							$this->nuco = $numerocontacto;
							
							$sql_get_contact= $this->db->query("SELECT id FROM contacts INNER JOIN contacts_cstm ON contacts.id=contacts_cstm.id_c WHERE contacts_cstm.sasa_numero_documento_c='{$numerocontacto}' AND deleted=0");
							$id_contact = $this->db->fetchByAssoc($sql_get_contact);
							$Habeas->contacts_sasa_habeas_data_1contacts_ida = $id_contact['id'];
							break;
						case 'lead':
							$numerolead = rtrim($value);
							$sql_get_lead= $this->db->query("SELECT id FROM leads INNER JOIN leads_cstm ON leads.id=leads_cstm.id_c WHERE (leads_cstm.sasa_numero_documento_c='{$numerolead}' OR leads_cstm.sasa_num_docu_juridica_c='{$numerolead}') AND deleted=0");
							$id_lead = $this->db->fetchByAssoc($sql_get_lead);
							$Habeas->leads_sasa_habeas_data_1leads_ida = $id_lead['id'];
							break;
						case 'sasa_estado_autorizacion_c':
							if ($value=="VIGENTE") {
								$Habeas->sasa_estado_autorizacion_c = "1";
							}
							break;
						case 'sasa_canales_autorizados_c':
							$canaleslimit = explode(";", $value);
								
							$canales = array();
							foreach ($canaleslimit as $valor) {
							    //Agrega ^ a cada valor ya que el campo es selección multiple
							    $canales[] = "^".$valor."^";
							}
							
							$canales = implode(',', $canales);
							$Habeas->sasa_canales_autorizados_c = $canales;
							break;
						case 'date_entered':
							$date = date("d-m-Y", strtotime($value));
							$date = new DateTime($date);
							$dateformat = $date->format('Y-m-d');
							$Habeas->creacionsigru_c = $dateformat;
							$this->datev = $dateformat;
							//$GLOBALS['log']->security("NU ".$this->nuco);
							$formato = $this->validate_unidad($this->nuco,$this->datev,$codcompania);
							$Document = BeanFactory::newBean("Documents");
							$Document->retrieve_by_string_fields( 
								array(
									'sasa_codigo_c' => $formato
								) 
							);
							$Habeas->documents_sasa_habeas_data_1documents_ida=$Document->id;
							break;
						case 'compania':
							$codcompania= rtrim($value);
							$query_get_compania = $this->db->query("SELECT sasa_companias.id FROM sasa_companias_cstm INNER JOIN sasa_companias ON sasa_companias_cstm.id_c=sasa_companias.id where sasa_companias.deleted=0 AND sasa_companias_cstm.sasa_cod_c='{$codcompania}'");
							$id_compania = $this->db->fetchByAssoc($query_get_compania);
							$Habeas->sasa_companias_sasa_habeas_data_1sasa_companias_ida = $id_compania['id'];

							//Buscar en unidadesCyP
							//if ($this->nuco != "") {

								
								//$GLOBALS['log']->security("Num ".$this->nuco." COM ".$codcompania." FORMATO ".$Document->sasa_codigo_c." IDFormat ".$Document->id);
							//}
							
							break;
						default:
							# code...
							break;
					}
				}
			}

			$Habeas->save();

			return array('status' => $status, 'id' => $Habeas->id); 
		}

		public function valida_habeassindoc($record){
			
			if ($record['id_suga']!= null || $record['id_suga'] != "") {
				$GLOBALS['log']->security("fe_auto ".$record['fe_auto']);
				$Habeas = BeanFactory::getBean('SASA_Habeas_Data', $record['id_suga'], array('disable_row_level_security' => true));
				$status = "Update";
				foreach ($record as $key => $value) {
						switch ($key) {
						case 'nu_nit_clie':
							$numerocontacto = rtrim($value);
							$this->nuco = $numerocontacto;
							break;
						case 'fe_auto':
							$date = date("Y-m-d H:i:s", strtotime($value));
							$date = new DateTime($date);
							$dateformat = $date->format('Y-m-d');
							//$Habeas->creacionsigru_c = $dateformat;
							$this->datev = $dateformat;

							$queryCompa = $this->db->query("SELECT sasa_companias_cstm.sasa_cod_c from sasa_companias_sasa_habeas_data_1_c INNER JOIN sasa_companias_cstm ON sasa_companias_sasa_habeas_data_1_c.sasa_companias_sasa_habeas_data_1sasa_companias_ida=sasa_companias_cstm.id_c WHERE sasa_companias_sasa_habeas_data_1_c.sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb='{$record['id_suga']}'");
							$resul_queryCompa = $this->db->fetchByAssoc($queryCompa);

							
							$formato = $this->validate_unidad($this->nuco,$this->datev,$resul_queryCompa['sasa_cod_c']);
							$GLOBALS['log']->security("formato ".$formato);
							$Document = BeanFactory::newBean("Documents");
							$Document->retrieve_by_string_fields(
								array(
									'sasa_codigo_c' => $formato
								)
							);
							$Habeas->documents_sasa_habeas_data_1documents_ida=$Document->id;
							break;
						case 'sasa_lote_c':
							$Habeas->sasa_lote_c=$value;
							break;
						
						default:
							// code...
							break;
					}
				}
				
				$Habeas->save();
				return array('status' => $status, 'id' => $Habeas->id);
			}
			
		}

		public function validate_unidad($numid, $datev, $compania){
			//$fecha_compa = strtotime("2016-05-19");
			$GLOBALS['log']->security("NumID ".$numid." datev ".$datev." $compania ".$compania);
			$fecha_corte= new DateTime("2020-12-18");
			$newdate= new DateTime($datev);
			if($newdate >= $fecha_corte){
			 	$resulta = "Es mayor o igual";
			 	$formato = "VARDI1";
			}else{
				$resulta = "Es menor";
				if ($compania == "1" || $compania == "2") {
					//Query para identificar si el contacto tiene una cuenta cuya unidadCyP sea de 120
					$query_validacom1y2 = $this->db->query("SELECT accounts_cstm.id_c, accounts_sasa_unidadnegclienteprospect_1_c.accounts_saf31rospect_idb, sasa_unidad_de_negocio_cstm.sasa_codunidnegocio_c FROM contacts_cstm LEFT JOIN accounts_contacts ON accounts_contacts.contact_id=contacts_cstm.id_c LEFT JOIN accounts_cstm ON accounts_cstm.id_c=accounts_contacts.account_id LEFT JOIN accounts_sasa_unidadnegclienteprospect_1_c ON accounts_cstm.id_c=accounts_sasa_unidadnegclienteprospect_1_c.accounts_sasa_unidadnegclienteprospect_1accounts_ida LEFT JOIN sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c ON sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c.sasa_unida14dfrospect_idb=accounts_sasa_unidadnegclienteprospect_1_c.accounts_saf31rospect_idb LEFT JOIN sasa_unidad_de_negocio_cstm ON sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c.sasa_unida3a6anegocio_ida=sasa_unidad_de_negocio_cstm.id_c WHERE contacts_cstm.sasa_numero_documento_c='{$numid}' AND sasa_unidad_de_negocio_cstm.sasa_codunidnegocio_c='120' AND accounts_sasa_unidadnegclienteprospect_1_c.deleted=0 AND sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c.deleted=0");
					$resul_query = $this->db->fetchByAssoc($query_validacom1y2);
					//Si lo tiene poner el documento 1_DINISSAN_MQ de lo contario el 1_DINISSAN
					if ($resul_query['id_c'] != null || $resul_query['id_c'] != "") {
						$formato = "DINISMAQ";
					}else{
						$formato = "DINISVEH";
					}
				}else{
					//Evaluar compania 3
					//Query para identificar si el contacto tiene una cuenta cuya unidadCyP sea de 120
					$query_validacom3 = $this->db->query("SELECT accounts_cstm.id_c, accounts_sasa_unidadnegclienteprospect_1_c.accounts_saf31rospect_idb, sasa_unidad_de_negocio_cstm.sasa_codunidnegocio_c FROM contacts_cstm LEFT JOIN accounts_contacts ON accounts_contacts.contact_id=contacts_cstm.id_c LEFT JOIN accounts_cstm ON accounts_cstm.id_c=accounts_contacts.account_id LEFT JOIN accounts_sasa_unidadnegclienteprospect_1_c ON accounts_cstm.id_c=accounts_sasa_unidadnegclienteprospect_1_c.accounts_sasa_unidadnegclienteprospect_1accounts_ida LEFT JOIN sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c ON sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c.sasa_unida14dfrospect_idb=accounts_sasa_unidadnegclienteprospect_1_c.accounts_saf31rospect_idb LEFT JOIN sasa_unidad_de_negocio_cstm ON sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c.sasa_unida3a6anegocio_ida=sasa_unidad_de_negocio_cstm.id_c WHERE contacts_cstm.sasa_numero_documento_c='{$numid}' AND sasa_unidad_de_negocio_cstm.sasa_codunidnegocio_c='310' AND accounts_sasa_unidadnegclienteprospect_1_c.deleted=0 AND sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c.deleted=0");
					$resul_query = $this->db->fetchByAssoc($query_validacom3);
					//Si lo tiene poner el documento 1_COM_USADOS de lo contario el 1_COM_NUEVOS
					if ($resul_query['id_c'] != null || $resul_query['id_c'] != "") {
						$formato = "COMUSADO";
					}else{
						$formato = "COMNUEVO";
					}
				}
				

			}

			$GLOBALS['log']->security("Num ".$numid." COM ".$compania." Date ".$datev." Resulta ".$resulta." FORMATO ".$formato);
			return $formato;
		}


		public function validate_record($record){
			$validate_array = array('band' => true);

			return $validate_array;
		}

		public function get_return(){
			return $this->return;
		}
	}


?>
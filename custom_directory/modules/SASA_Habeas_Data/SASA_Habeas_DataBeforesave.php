<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once "custom/include/api/cstmSugarApiExceptionError.php";
class SASA_Habeas_DataBeforesave
{
	function before_save($bean, $event, $arguments)
	{	
		/*
		Logic Hook para poblar automaticamente la esctutura geografica de relacion
		*/
		/*if ($bean->contacts_sasa_habeas_data_1contacts_ida=="" && $bean->leads_sasa_habeas_data_1leads_ida=="") {
			throw new SugarApiExceptionInvalidParameter("Debe realcionar un contacto o un lead");
		}*/
		//Relacionar siempre el formato de habeas data actual
		if ($bean->documents_sasa_habeas_data_1documents_ida != "060e244e-7930-11ec-97e6-06be4ff698d4") {
			$documentquery = $GLOBALS['db']->query("SELECT id FROM documents WHERE document_name LIKE 'GRUPOVARDI%%(VIGENTE)'");
			$iddocument = $GLOBALS['db']->fetchByAssoc($documentquery);
			$bean->documents_sasa_habeas_data_1documents_ida=$iddocument['id'];
		}
		

		try{
			
			if ($bean->sasa_auto_contactacion_c == "1") {
				$bean->sasa_canales_autorizados_c = "^1^,^2^,^3^,^4^,^5^,^6^,^7^,^8^,^9^";
			}
			if ($bean->sasa_auto_contactacion_c == "0") {
				$bean->sasa_canales_autorizados_c = "^0^";
			}

			
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook estructura_direccion: ".$e->getMessage()); 
		}
	}
}
?>
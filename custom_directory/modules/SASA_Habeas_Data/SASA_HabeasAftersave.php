<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_HabeasAftersave
{
	function after_save($bean, $event, $arguments)
	{	
		/*
		Logic Hook para poblar automaticamente la esctutura geografica de relacion
		*/
		try{
			if (!isset($bean->sasahabeas_after_save_ignore_update) || $bean->sasahabeas_after_save_ignore_update === false){//antiloop
				$bean->sasahabeas_after_save_ignore_update = true;//antiloop
					
				if (!empty($bean->sasa_companias_sasa_habeas_data_1sasa_companias_ida)){
					$resultcompania = $GLOBALS['db']->query("SELECT id FROM sasa_companias INNER JOIN sasa_companias_cstm ON sasa_companias.id=sasa_companias_cstm.id_c WHERE sasa_companias_cstm.sasa_cod_c='{$bean->sasa_companias_sasa_habeas_data_1sasa_companias_ida}' AND sasa_companias.deleted=0");
					$sasa_cod_compania =  $GLOBALS['db']->fetchByAssoc($resultcompania);	
					
					if ($bean->load_relationship('sasa_companias_sasa_habeas_data_1')){
						$relatedBeans = $bean->sasa_companias_sasa_habeas_data_1->add($sasa_cod_compania["id"]);
						$bean->sasa_companias_sasa_habeas_data_1 ="";
					}
				}
				
				$bean->save();	

				if ($bean->modified_by_name != "Integracion1") {
					//Consumir SIgru Habeas
					$Lead = BeanFactory::getBean('Leads',$bean->leads_sasa_habeas_data_1leads_ida,array('disable_row_level_security' => true));

					//Enviar habeas data solo cuando el lead relacionado tenga una llamada
					if (!empty($Lead->id)) {
						if ($bean->sasa_estado_autorizacion_c == "1") {
							//Query para identificar si el lead relacionado contiene una llamada realizada
							$query_calls_related_lead = $GLOBALS['db']->query("SELECT * FROM calls WHERE calls.status='Held' AND calls.deleted=0 AND calls.parent_type='Leads' AND calls.parent_id='{$Lead->id}'");

							$get_calls_related_lead = mysqli_fetch_array($query_calls_related_lead);
							if ($get_calls_related_lead['id'] != null) {
								//Enviar Habeas data
								if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) {
						            require_once("custom/modules/integration_sigru/Send_Data_Sigru_Habeas.php");
									$sendsigru = new Send_Data_Sigru_Habeas();
									$repuesta = $sendsigru->data($bean->id,$bean->module_name,"POST");
						        }else{
						            require_once("custom/modules/integration_sigru/Send_Data_Sigru_Habeas.php");
									$sendsigru = new Send_Data_Sigru_Habeas();
									$repuesta = $sendsigru->data($bean->id,$bean->module_name,"PUT");
						        }
							}
						}
						
					}else{
						if ($bean->sasa_estado_autorizacion_c == "1") {
							$GLOBALS['log']->security("\nConsumir SIgru Habeas\n");
							if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) {
					            require_once("custom/modules/integration_sigru/Send_Data_Sigru_Habeas.php");
								$sendsigru = new Send_Data_Sigru_Habeas();
								$repuesta = $sendsigru->data($bean->id,$bean->module_name,"POST");
					        }else{
					            require_once("custom/modules/integration_sigru/Send_Data_Sigru_Habeas.php");
								$sendsigru = new Send_Data_Sigru_Habeas();
								$repuesta = $sendsigru->data($bean->id,$bean->module_name,"PUT");
					        }
						}
					}

					//Varible para identificar si la nota ya tiene una URL
					$urlvisualizador= "http://templatephp";
					$pos = strpos($bean->sasa_soporte_autorizacion_c, $urlvisualizador);
					if ($repuesta) {
						$bean->sasa_soporte_autorizacion_c = "";
						//if ($pos === false) {
							$repuesta = json_decode($repuesta['Response'],true);
							//sasa_url_formt_hd_c no tiene una URL de visualizador establecida (http://templatephp)
							//Si no tiene se concatena el id de la nota enviado por SIGRU con la URL habilitada para el visualizador de adjuntos SIGRU
							if ($repuesta['token']!= null) {
								$bean->sasa_soporte_autorizacion_c = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$repuesta['token'];
								$bean->save();
							}
							
						//}
					}

				}else{
					//Varible para identificar si la nota ya tiene una URL
					$urlvisualizador= "http://templatephp";
					$pos = strpos($bean->sasa_soporte_autorizacion_c, $urlvisualizador);
					//validar si el sasa_soporte_autorizacion_c que envían no esta vacio.
					if ($bean->sasa_soporte_autorizacion_c != "" || $bean->sasa_soporte_autorizacion_c != null) {
						if ($pos === false) {
							$bean->sasa_soporte_autorizacion_c = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$bean->sasa_soporte_autorizacion_c;
							$bean->save();
						}
					}
				}


				//Validar si un habeas data esta duplicado para dejar solo uno de ellos, se va a identificar por el usaurio creador y la fecha de creación
				if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) {
					if ($bean->leads_sasa_habeas_data_1leads_ida != "" || $bean->leads_sasa_habeas_data_1leads_ida != null) {
						$tabla = "leads_sasa_habeas_data_1_c";
						$columna1 = "leads_sasa_habeas_data_1sasa_habeas_data_idb";
						$columna2 = "leads_sasa_habeas_data_1leads_ida";
						$idrecordrelated = $bean->leads_sasa_habeas_data_1leads_ida;
					}else{
						$tabla = "contacts_sasa_habeas_data_1_c";
						$columna1 = "contacts_sasa_habeas_data_1sasa_habeas_data_idb";
						$columna2 = "contacts_sasa_habeas_data_1contacts_ida";
						$idrecordrelated = $bean->contacts_sasa_habeas_data_1contacts_ida;
					}

					$queryhabeasreciente = $GLOBALS['db']->query("SELECT t1.date_modified, t1.{$columna1} idhabeas FROM {$tabla} t1 INNER JOIN sasa_habeas_data ON t1.{$columna1}=sasa_habeas_data.id WHERE t1.{$columna2} = '{$idrecordrelated}' AND t1.deleted=0 AND sasa_habeas_data.deleted=0 ORDER BY `t1`.`date_modified` DESC LIMIT 1");
					$resulthabeasreciente = $GLOBALS['db']->fetchByAssoc($queryhabeasreciente);
					$queryUpdateEstado = "UPDATE sasa_habeas_data_cstm INNER JOIN {$tabla} t1 ON t1.{$columna1}=sasa_habeas_data_cstm.id_c SET sasa_habeas_data_cstm.sasa_estado_autorizacion_c='2' WHERE t1.{$columna2} = '{$idrecordrelated}' AND sasa_habeas_data_cstm.id_c !='{$resulthabeasreciente['idhabeas']}'";
					$GLOBALS['log']->security($queryUpdateEstado);
					$result = $GLOBALS['db']->query($queryUpdateEstado);
		        }
				
				
			}
			
			
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Lineas de Vehiculos: ".$e->getMessage()); 
		}
	}
}
?>

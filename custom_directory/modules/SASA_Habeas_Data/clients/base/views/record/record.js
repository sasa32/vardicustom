({
    extendsFrom: 'RecordView',
    initialize: function(options) {
	var self=this;
	este = self;
	self._super('initialize', [options]);

		self.model.once("sync",
			function() {
				self.model.on(
					"change:sasa_auto_contactacion_c",
					self._Validar_auto_contactacion,
					self
				);
				self.model.on(
					"change:sasa_fuente_autorizacion_c",
					self._Validar_fuente_autorizacion,
					self
				);
				//console.log("olaKace");
			},

			self
		);
    },
	_Validar_auto_contactacion: function(fields, errors, callback) {
    	//console.log("cambio");
    	var self=this;
    	if (self.model.get('sasa_auto_contactacion_c')=='2') {
    		//console.log("Y es dos");
    		self.model.set('sasa_canales_autorizados_c','');
    	}
    },
    _Validar_fuente_autorizacion: function(fields, errors, callback) {
    	//console.log("cambioFuente");
    	var self=this;
    	if (self.model.get('sasa_fuente_autorizacion_c')=='W') {
    		//console.log("Y es dos");
    		self.model.set('sasa_auto_contactacion_c','1');
    		self.model.set('sasa_estado_autorizacion_c','1');
    		self.model.set('sasa_revision_c','V');
    		self.model.set('documents_sasa_habeas_data_1documents_ida','060e244e-7930-11ec-97e6-06be4ff698d4');
    		self.model.set('documents_sasa_habeas_data_1_name','HABEAS DATA SITIO WEB');
    	}
    }
    
    
})

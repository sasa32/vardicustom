<?php
$module_name = 'SASA_Habeas_Data';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'documents_sasa_habeas_data_1_name',
              ),
              1 => 
              array (
                'name' => 'sasa_companias_sasa_habeas_data_1_name',
              ),
              2 => 
              array (
                'name' => 'contacts_sasa_habeas_data_1_name',
              ),
              3 => 
              array (
                'name' => 'leads_sasa_habeas_data_1_name',
              ),
              4 => 
              array (
                'name' => 'sasa_estado_autorizacion_c',
                'label' => 'LBL_SASA_ESTADO_AUTORIZACION_C',
              ),
              5 => 
              array (
                'name' => 'sasa_revision_c',
                'label' => 'LBL_SASA_REVISION_C',
              ),
              6 => 
              array (
                'name' => 'sasa_canales_autorizados_c',
                'label' => 'LBL_SASA_CANALES_AUTORIZADOS_C',
              ),
              7 => 
              array (
                'name' => 'sasa_fuente_autorizacion_c',
                'label' => 'LBL_SASA_FUENTE_AUTORIZACION_C',
              ),
              8 => 
              array (
                'name' => 'sasa_soporte_autorizacion_c',
                'label' => 'LBL_SASA_SOPORTE_AUTORIZACION_C',
              ),
              9 => 
              array (
                'name' => 'description',
              ),
              10 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              11 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);

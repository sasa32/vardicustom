<?php
$module_name = 'SASA_Habeas_Data';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'recorddashlet' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'documents_sasa_habeas_data_1_name',
              ),
              1 => 
              array (
                'name' => 'sasa_companias_sasa_habeas_data_1_name',
              ),
              2 => 
              array (
                'name' => 'contacts_sasa_habeas_data_1_name',
              ),
              3 => 
              array (
                'name' => 'leads_sasa_habeas_data_1_name',
              ),
              4 => 
              array (
                'name' => 'sasa_estado_autorizacion_c',
                'label' => 'LBL_SASA_ESTADO_AUTORIZACION_C',
              ),
              5 => 
              array (
                'name' => 'sasa_revision_c',
                'label' => 'LBL_SASA_REVISION_C',
              ),
              6 => 
              array (
                'name' => 'sasa_auto_contactacion_c',
                'label' => 'LBL_SASA_AUTO_CONTACTACION_C',
              ),
              7 => 
              array (
                'name' => 'sasa_canales_autorizados_c',
                'label' => 'LBL_SASA_CANALES_AUTORIZADOS_C',
              ),
              8 => 
              array (
                'name' => 'sasa_fuente_autorizacion_c',
                'label' => 'LBL_SASA_FUENTE_AUTORIZACION_C',
              ),
              9 => 
              array (
                'name' => 'sasa_soporte_autorizacion_c',
                'label' => 'LBL_SASA_SOPORTE_AUTORIZACION_C',
              ),
              10 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              11 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              12 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);

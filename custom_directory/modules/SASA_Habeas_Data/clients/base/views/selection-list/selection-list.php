<?php
$module_name = 'SASA_Habeas_Data';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_estado_autorizacion_c',
                'label' => 'LBL_SASA_ESTADO_AUTORIZACION_C',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'contacts_sasa_habeas_data_1_name',
                'label' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_CONTACTS_TITLE',
                'enabled' => true,
                'id' => 'CONTACTS_SASA_HABEAS_DATA_1CONTACTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'leads_sasa_habeas_data_1_name',
                'label' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_LEADS_TITLE',
                'enabled' => true,
                'id' => 'LEADS_SASA_HABEAS_DATA_1LEADS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_canal_preferencia_c',
                'label' => 'LBL_SASA_CANAL_PREFERENCIA_C',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'sasa_canales_autorizados_c',
                'label' => 'LBL_SASA_CANALES_AUTORIZADOS_C',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              7 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => true,
                'name' => 'date_modified',
                'readonly' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);

<?php
$module_name = 'SASA_Habeas_Data';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'detail' => 
      array (
        'templateMeta' => 
        array (
          'form' => 
          array (
            'buttons' => 
            array (
              0 => 'EDIT',
              1 => 'DUPLICATE',
              2 => 'DELETE',
            ),
          ),
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 
              array (
                'name' => 'documents_sasa_habeas_data_1_name',
                'label' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_DOCUMENTS_TITLE',
              ),
              2 => 
              array (
                'name' => 'sasa_habeas_data_sasa_companias_1_name',
                'label' => 'LBL_SASA_HABEAS_DATA_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE',
              ),
              3 => 
              array (
                'name' => 'sasa_estado_autorizacion_c',
                'label' => 'LBL_SASA_ESTADO_AUTORIZACION_C',
              ),
              4 => 
              array (
                'name' => 'sasa_revision_c',
                'label' => 'LBL_SASA_REVISION_C',
              ),
              5 => 
              array (
                'name' => 'sasa_auto_contactacion_c',
                'label' => 'LBL_SASA_AUTO_CONTACTACION_C',
              ),
              6 => 
              array (
                'name' => 'sasa_canales_autorizados_c',
                'label' => 'LBL_SASA_CANALES_AUTORIZADOS_C',
              ),
              7 => 
              array (
                'name' => 'sasa_fuente_autorizacion_c',
                'label' => 'LBL_SASA_FUENTE_AUTORIZACION_C',
              ),
              8 => 
              array (
                'name' => 'sasa_soporte_autorizacion_c',
                'label' => 'LBL_SASA_SOPORTE_AUTORIZACION_C',
              ),
              9 => 
              array (
                'name' => 'sasa_habeas_data_leads_1_name',
                'label' => 'LBL_SASA_HABEAS_DATA_LEADS_1_FROM_LEADS_TITLE',
              ),
              10 => 
              array (
                'name' => 'sasa_habeas_data_contacts_1_name',
                'label' => 'LBL_SASA_HABEAS_DATA_CONTACTS_1_FROM_CONTACTS_TITLE',
              ),
              11 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
              ),
              12 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              13 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);

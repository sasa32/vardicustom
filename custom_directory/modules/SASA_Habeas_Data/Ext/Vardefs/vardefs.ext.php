<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2020-07-28 14:37:29
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['audited']=true;
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['unified_search']=false;
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['calculated']=false;
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sasa_companias_sasa_habeas_data_1_SASA_Habeas_Data.php

// created: 2020-07-29 18:44:33
$dictionary["SASA_Habeas_Data"]["fields"]["sasa_companias_sasa_habeas_data_1"] = array (
  'name' => 'sasa_companias_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'sasa_companias_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'sasa_Companias',
  'bean_name' => 'sasa_Companias',
  'side' => 'right',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'id_name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'link-type' => 'one',
);
$dictionary["SASA_Habeas_Data"]["fields"]["sasa_companias_sasa_habeas_data_1_name"] = array (
  'name' => 'sasa_companias_sasa_habeas_data_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_COMPANIAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'link' => 'sasa_companias_sasa_habeas_data_1',
  'table' => 'sasa_companias',
  'module' => 'sasa_Companias',
  'rname' => 'name',
);
$dictionary["SASA_Habeas_Data"]["fields"]["sasa_companias_sasa_habeas_data_1sasa_companias_ida"] = array (
  'name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE_ID',
  'id_name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'link' => 'sasa_companias_sasa_habeas_data_1',
  'table' => 'sasa_companias',
  'module' => 'sasa_Companias',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/documents_sasa_habeas_data_1_SASA_Habeas_Data.php

// created: 2020-07-29 16:58:08
$dictionary["SASA_Habeas_Data"]["fields"]["documents_sasa_habeas_data_1"] = array (
  'name' => 'documents_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'documents_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'id_name' => 'documents_sasa_habeas_data_1documents_ida',
  'link-type' => 'one',
);
$dictionary["SASA_Habeas_Data"]["fields"]["documents_sasa_habeas_data_1_name"] = array (
  'name' => 'documents_sasa_habeas_data_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_DOCUMENTS_TITLE',
  'save' => true,
  'id_name' => 'documents_sasa_habeas_data_1documents_ida',
  'link' => 'documents_sasa_habeas_data_1',
  'table' => 'documents',
  'module' => 'Documents',
  'rname' => 'document_name',
);
$dictionary["SASA_Habeas_Data"]["fields"]["documents_sasa_habeas_data_1documents_ida"] = array (
  'name' => 'documents_sasa_habeas_data_1documents_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE_ID',
  'id_name' => 'documents_sasa_habeas_data_1documents_ida',
  'link' => 'documents_sasa_habeas_data_1',
  'table' => 'documents',
  'module' => 'Documents',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/leads_sasa_habeas_data_1_SASA_Habeas_Data.php

// created: 2020-07-30 11:17:08
$dictionary["SASA_Habeas_Data"]["fields"]["leads_sasa_habeas_data_1"] = array (
  'name' => 'leads_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'leads_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'side' => 'right',
  'vname' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'id_name' => 'leads_sasa_habeas_data_1leads_ida',
  'link-type' => 'one',
);
$dictionary["SASA_Habeas_Data"]["fields"]["leads_sasa_habeas_data_1_name"] = array (
  'name' => 'leads_sasa_habeas_data_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_sasa_habeas_data_1leads_ida',
  'link' => 'leads_sasa_habeas_data_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["SASA_Habeas_Data"]["fields"]["leads_sasa_habeas_data_1leads_ida"] = array (
  'name' => 'leads_sasa_habeas_data_1leads_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE_ID',
  'id_name' => 'leads_sasa_habeas_data_1leads_ida',
  'link' => 'leads_sasa_habeas_data_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/full_text_search_admin.php

 // created: 2020-08-28 14:29:24
$dictionary['SASA_Habeas_Data']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/contacts_sasa_habeas_data_1_SASA_Habeas_Data.php

// created: 2020-07-30 11:16:31
$dictionary["SASA_Habeas_Data"]["fields"]["contacts_sasa_habeas_data_1"] = array (
  'name' => 'contacts_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'contacts_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'id_name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["SASA_Habeas_Data"]["fields"]["contacts_sasa_habeas_data_1_name"] = array (
  'name' => 'contacts_sasa_habeas_data_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'link' => 'contacts_sasa_habeas_data_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["SASA_Habeas_Data"]["fields"]["contacts_sasa_habeas_data_1contacts_ida"] = array (
  'name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE_ID',
  'id_name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'link' => 'contacts_sasa_habeas_data_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sasa_habeas_data_tasks_1_SASA_Habeas_Data.php

// created: 2020-11-03 14:24:42
$dictionary["SASA_Habeas_Data"]["fields"]["sasa_habeas_data_tasks_1"] = array (
  'name' => 'sasa_habeas_data_tasks_1',
  'type' => 'link',
  'relationship' => 'sasa_habeas_data_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_SASA_HABEAS_DATA_TASKS_1_FROM_SASA_HABEAS_DATA_TITLE',
  'id_name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_sasa_canal_preferencia_c.php

 // created: 2020-07-28 14:57:52
$dictionary['SASA_Habeas_Data']['fields']['sasa_canal_preferencia_c']['labelValue']='Canal de Preferencia';
$dictionary['SASA_Habeas_Data']['fields']['sasa_canal_preferencia_c']['dependency']='';
$dictionary['SASA_Habeas_Data']['fields']['sasa_canal_preferencia_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_name.php

 // created: 2020-07-29 18:46:37
$dictionary['SASA_Habeas_Data']['fields']['name']['len']='255';
$dictionary['SASA_Habeas_Data']['fields']['name']['audited']=true;
$dictionary['SASA_Habeas_Data']['fields']['name']['massupdate']=false;
$dictionary['SASA_Habeas_Data']['fields']['name']['unified_search']=false;
$dictionary['SASA_Habeas_Data']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['SASA_Habeas_Data']['fields']['name']['calculated']='1';
$dictionary['SASA_Habeas_Data']['fields']['name']['importable']='false';
$dictionary['SASA_Habeas_Data']['fields']['name']['duplicate_merge']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['SASA_Habeas_Data']['fields']['name']['merge_filter']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['name']['formula']='concat(related($documents_sasa_habeas_data_1,"name")," -",related($leads_sasa_habeas_data_1,"name"),related($contacts_sasa_habeas_data_1,"name")," - ",toString($date_entered))';
$dictionary['SASA_Habeas_Data']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_documents_sasa_habeas_data_1documents_ida.php

 // created: 2021-07-07 20:20:15
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['name']='documents_sasa_habeas_data_1documents_ida';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['type']='id';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['source']='non-db';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['vname']='LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE_ID';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['id_name']='documents_sasa_habeas_data_1documents_ida';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['link']='documents_sasa_habeas_data_1';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['table']='documents';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['module']='Documents';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['rname']='id';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['reportable']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['side']='right';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['massupdate']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['duplicate_merge']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['hideacl']=true;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['audited']=false;

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_documents_sasa_habeas_data_1_name.php

 // created: 2021-07-07 20:20:15
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['audited']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['hidemassupdate']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['duplicate_merge']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['duplicate_merge_dom_value']=0;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['merge_filter']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['calculated']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['vname']='LBL_DOCUMENTS_SASA_HABEAS_DATA_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_sasa_adjuntohabeasdata_c.php

 // created: 2021-07-09 14:33:28

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_description.php

 // created: 2020-07-28 14:38:26
$dictionary['SASA_Habeas_Data']['fields']['description']['audited']=true;
$dictionary['SASA_Habeas_Data']['fields']['description']['required']=true;
$dictionary['SASA_Habeas_Data']['fields']['description']['massupdate']=false;
$dictionary['SASA_Habeas_Data']['fields']['description']['comments']='Full text of the note';
$dictionary['SASA_Habeas_Data']['fields']['description']['duplicate_merge']='enabled';
$dictionary['SASA_Habeas_Data']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['SASA_Habeas_Data']['fields']['description']['merge_filter']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['description']['unified_search']=false;
$dictionary['SASA_Habeas_Data']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['SASA_Habeas_Data']['fields']['description']['calculated']=false;
$dictionary['SASA_Habeas_Data']['fields']['description']['rows']='6';
$dictionary['SASA_Habeas_Data']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_sasa_auto_contactacion_c.php

 // created: 2021-03-17 15:25:09
$dictionary['SASA_Habeas_Data']['fields']['sasa_auto_contactacion_c']['labelValue']='¿Autoriza Contactación?';
$dictionary['SASA_Habeas_Data']['fields']['sasa_auto_contactacion_c']['dependency']='';
$dictionary['SASA_Habeas_Data']['fields']['sasa_auto_contactacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_sasa_canales_autorizados_c.php

 // created: 2021-03-17 15:25:09
$dictionary['SASA_Habeas_Data']['fields']['sasa_canales_autorizados_c']['labelValue']='Canales Autorizados';
$dictionary['SASA_Habeas_Data']['fields']['sasa_canales_autorizados_c']['dependency']='';
$dictionary['SASA_Habeas_Data']['fields']['sasa_canales_autorizados_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_sasa_revision_c.php

 // created: 2021-03-17 15:25:09
$dictionary['SASA_Habeas_Data']['fields']['sasa_revision_c']['labelValue']='Revisión';
$dictionary['SASA_Habeas_Data']['fields']['sasa_revision_c']['dependency']='';
$dictionary['SASA_Habeas_Data']['fields']['sasa_revision_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_sasa_fuente_autorizacion_c.php

 // created: 2021-03-17 15:25:09
$dictionary['SASA_Habeas_Data']['fields']['sasa_fuente_autorizacion_c']['labelValue']='Fuente de la Autorización';
$dictionary['SASA_Habeas_Data']['fields']['sasa_fuente_autorizacion_c']['dependency']='';
$dictionary['SASA_Habeas_Data']['fields']['sasa_fuente_autorizacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_sasa_estado_autorizacion_c.php

 // created: 2021-03-17 15:25:09
$dictionary['SASA_Habeas_Data']['fields']['sasa_estado_autorizacion_c']['labelValue']='Estado de la Autorización';
$dictionary['SASA_Habeas_Data']['fields']['sasa_estado_autorizacion_c']['dependency']='';
$dictionary['SASA_Habeas_Data']['fields']['sasa_estado_autorizacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Vardefs/sugarfield_sasa_soporte_autorizacion_c.php

 // created: 2021-03-17 15:25:09
$dictionary['SASA_Habeas_Data']['fields']['sasa_soporte_autorizacion_c']['labelValue']='Soporte de la Autorización';
$dictionary['SASA_Habeas_Data']['fields']['sasa_soporte_autorizacion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['SASA_Habeas_Data']['fields']['sasa_soporte_autorizacion_c']['dependency']='';

 
?>

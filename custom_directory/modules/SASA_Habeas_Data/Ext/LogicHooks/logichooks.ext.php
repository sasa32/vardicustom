<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/LogicHooks/SASA_Habeas_DataBeforesave.php

$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'estructura_direccionAccounts',

	//The PHP file where your class is located.
	'custom/modules/SASA_Habeas_Data/SASA_Habeas_DataBeforesave.php',

	//The class the method is in.
	'SASA_Habeas_DataBeforesave',

	//The method to call.
	'before_save'
);


?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/LogicHooks/SASA_HabeasAftersave.php

$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'SASA_HabeasAftersave',

	//The PHP file where your class is located.
	'custom/modules/SASA_Habeas_Data/SASA_HabeasAftersave.php',

	//The class the method is in.
	'SASA_HabeasAftersave',

	//The method to call.
	'after_save'
);


?>

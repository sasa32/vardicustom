<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Language/es_LA.customsasa_habeas_data_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_HABEAS_DATA_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documentos';
$mod_strings['LBL_SASA_HABEAS_DATA_DOCUMENTS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Documentos';

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Language/es_LA.customdocuments_sasa_habeas_data_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_DOCUMENTS_TITLE'] = 'Documentos';
$mod_strings['LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Documentos';

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Language/es_LA.customsasa_habeas_data_contacts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_HABEAS_DATA_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_SASA_HABEAS_DATA_CONTACTS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Contactos';

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Language/es_LA.customsasa_habeas_data_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_HABEAS_DATA_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_HABEAS_DATA_LEADS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Leads';

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Language/es_LA.customsasa_habeas_data_sasa_companias_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_HABEAS_DATA_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE'] = 'Compañia';
$mod_strings['LBL_SASA_HABEAS_DATA_SASA_COMPANIAS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Compañia';

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Language/es_LA.customsasa_companias_sasa_habeas_data_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_COMPANIAS_TITLE'] = 'Compañias';
$mod_strings['LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Compañias';

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Language/es_LA.customleads_sasa_habeas_data_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LEADS_SASA_HABEAS_DATA_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_LEADS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Leads';

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Language/es_LA.customcontacts_sasa_habeas_data_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Contactos';

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Language/es_LA.customsasa_habeas_data_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_HABEAS_DATA_TASKS_1_FROM_TASKS_TITLE'] = 'Tareas';
$mod_strings['LBL_SASA_HABEAS_DATA_TASKS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Tareas';

?>

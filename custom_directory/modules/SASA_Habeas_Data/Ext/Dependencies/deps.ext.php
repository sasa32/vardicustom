<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Dependencies/dependenciessetvalue.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
/*$dependencies['SASA_Habeas_Data']['dependenciessetvalue'] = array(
	'hooks' => array("edit"),
	'triggerFields' => array('sasa_auto_contactacion_c'),
	//'trigger' => 'or(equal($sasa_auto_contactacion_c,1),and(equal($sasa_auto_contactacion_c,2),not(equal($sasa_canales_autorizados_c,""))))',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => '
					ifElse(equal($sasa_auto_contactacion_c,1),createList("CORREO ELECTRONICO","DIRECCION FISICA","LLAMADA","SMS","TELEFONO FIJO","WHATSAPP"),createList(""))
				',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => '
					ifElse(equal($sasa_auto_contactacion_c,2),createList(""),$sasa_canales_autorizados_c)
				',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => '
					ifElse(equal($sasa_auto_contactacion_c,3),createList("NINGUNA"),$sasa_canales_autorizados_c)
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);
*/
?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Dependencies/dependenciesSetVisibilityContactsLead.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['SASA_Habeas_Data']['dependenciesSetVisibilityContactsLead'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('contacts_sasa_habeas_data_1_name','leads_sasa_habeas_data_1_name'),
	//'trigger' => 'equal($sasa_auto_contactacion_c,1)',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'contacts_sasa_habeas_data_1_name',
				'value' => 'equal($leads_sasa_habeas_data_1_name,"")',
			),
		),
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'leads_sasa_habeas_data_1_name',
				'value' => 'equal($contacts_sasa_habeas_data_1_name,"")',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'contacts_sasa_habeas_data_1_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'leads_sasa_habeas_data_1_name',
				'value' => 'false'
			)
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/SASA_Habeas_Data/Ext/Dependencies/dependenciesCanalesAutorizados.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['SASA_Habeas_Data']['dependenciesCanalesAutorizados'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_auto_contactacion_c'),
	//'trigger' => 'equal($sasa_auto_contactacion_c,1)',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => '
					or(equal($sasa_auto_contactacion_c,1),equal($sasa_auto_contactacion_c,2),equal($sasa_auto_contactacion_c,0))
				',
			),
		),
		array(
			'name' => 'ReadOnly',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => 'or(equal($sasa_auto_contactacion_c,1),equal($sasa_auto_contactacion_c,0))',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => '
					equal($sasa_auto_contactacion_c,2)
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => 'false'
			)
		),
		array(
			'name' => 'ReadOnly',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => 'false',
			),
		),
	)
);

?>

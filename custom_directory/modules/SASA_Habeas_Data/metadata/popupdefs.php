<?php
$popupMeta = array (
    'moduleMain' => 'SASA_Habeas_Data',
    'varName' => 'SASA_Habeas_Data',
    'orderBy' => 'sasa_habeas_data.name',
    'whereClauses' => array (
  'name' => 'sasa_habeas_data.name',
  'sasa_estado_autorizacion_c' => 'sasa_habeas_data_cstm.sasa_estado_autorizacion_c',
  'sasa_canales_autorizados_c' => 'sasa_habeas_data_cstm.sasa_canales_autorizados_c',
  'date_entered' => 'sasa_habeas_data.date_entered',
  'date_modified' => 'sasa_habeas_data.date_modified',
  'contacts_sasa_habeas_data_1_name' => 'sasa_habeas_data.contacts_sasa_habeas_data_1_name',
  'leads_sasa_habeas_data_1_name' => 'sasa_habeas_data.leads_sasa_habeas_data_1_name',
  'sasa_auto_contactacion_c' => 'sasa_habeas_data_cstm.sasa_auto_contactacion_c',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sasa_estado_autorizacion_c',
  8 => 'sasa_canales_autorizados_c',
  9 => 'date_entered',
  10 => 'date_modified',
  11 => 'contacts_sasa_habeas_data_1_name',
  12 => 'leads_sasa_habeas_data_1_name',
  13 => 'sasa_auto_contactacion_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'sasa_estado_autorizacion_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_ESTADO_AUTORIZACION_C',
    'width' => '10',
    'name' => 'sasa_estado_autorizacion_c',
  ),
  'contacts_sasa_habeas_data_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_SASA_HABEAS_DATA_1CONTACTS_IDA',
    'width' => '10',
    'name' => 'contacts_sasa_habeas_data_1_name',
  ),
  'leads_sasa_habeas_data_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_LEADS_TITLE',
    'id' => 'LEADS_SASA_HABEAS_DATA_1LEADS_IDA',
    'width' => '10',
    'name' => 'leads_sasa_habeas_data_1_name',
  ),
  'sasa_auto_contactacion_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_AUTO_CONTACTACION_C',
    'width' => 10,
    'name' => 'sasa_auto_contactacion_c',
  ),
  'sasa_canales_autorizados_c' => 
  array (
    'type' => 'multienum',
    'label' => 'LBL_SASA_CANALES_AUTORIZADOS_C',
    'width' => '10',
    'name' => 'sasa_canales_autorizados_c',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10',
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10',
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SASA_ESTADO_AUTORIZACION_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_ESTADO_AUTORIZACION_C',
    'width' => 10,
    'name' => 'sasa_estado_autorizacion_c',
  ),
  'CONTACTS_SASA_HABEAS_DATA_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_SASA_HABEAS_DATA_1CONTACTS_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'contacts_sasa_habeas_data_1_name',
  ),
  'LEADS_SASA_HABEAS_DATA_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_LEADS_TITLE',
    'id' => 'LEADS_SASA_HABEAS_DATA_1LEADS_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'leads_sasa_habeas_data_1_name',
  ),
  'SASA_CANALES_AUTORIZADOS_C' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'label' => 'LBL_SASA_CANALES_AUTORIZADOS_C',
    'width' => 10,
    'name' => 'sasa_canales_autorizados_c',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
    'name' => 'date_entered',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
    'name' => 'date_modified',
  ),
),
);

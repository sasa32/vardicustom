<?php
// created: ' . date('Y-m-d H:i:s')
$subpanel_layout['list_fields']['name']['vname'] = 'LBL_NAME';
$subpanel_layout['list_fields']['name']['widget_class'] = 'SubPanelDetailViewLink';
$subpanel_layout['list_fields']['name']['width'] = 10;
$subpanel_layout['list_fields']['name']['default'] = true;
$subpanel_layout['list_fields']['sasa_canales_autorizados_c']['type'] = 'multienum';
$subpanel_layout['list_fields']['sasa_canales_autorizados_c']['default'] = true;
$subpanel_layout['list_fields']['sasa_canales_autorizados_c']['vname'] = 'LBL_SASA_CANALES_AUTORIZADOS_C';
$subpanel_layout['list_fields']['sasa_canales_autorizados_c']['width'] = 10;
$subpanel_layout['list_fields']['date_entered']['type'] = 'datetime';
$subpanel_layout['list_fields']['date_entered']['studio']['portaleditview'] = false;
$subpanel_layout['list_fields']['date_entered']['readonly'] = true;
$subpanel_layout['list_fields']['date_entered']['vname'] = 'LBL_DATE_ENTERED';
$subpanel_layout['list_fields']['date_entered']['width'] = 10;
$subpanel_layout['list_fields']['date_entered']['default'] = true;
$subpanel_layout['list_fields']['date_modified']['vname'] = 'LBL_DATE_MODIFIED';
$subpanel_layout['list_fields']['date_modified']['width'] = 10;
$subpanel_layout['list_fields']['date_modified']['default'] = true;

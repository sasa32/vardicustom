<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-05-15 15:42:47
$viewdefs['sasa_Departamentos']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_departamentos_accounts_1',
  ),
);

// created: 2020-05-15 16:05:56
$viewdefs['sasa_Departamentos']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_departamentos_accounts_2',
  ),
);

// created: 2020-05-15 15:43:16
$viewdefs['sasa_Departamentos']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_departamentos_contacts_1',
  ),
);

// created: 2020-05-15 16:06:23
$viewdefs['sasa_Departamentos']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_departamentos_contacts_2',
  ),
);

// created: 2020-05-15 15:43:53
$viewdefs['sasa_Departamentos']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_LEADS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_departamentos_leads_1',
  ),
);

// created: 2020-05-15 16:07:49
$viewdefs['sasa_Departamentos']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_LEADS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_departamentos_leads_2',
  ),
);

// created: 2020-05-15 15:42:24
$viewdefs['sasa_Departamentos']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_departamentos_sasa_municipios_1',
  ),
);
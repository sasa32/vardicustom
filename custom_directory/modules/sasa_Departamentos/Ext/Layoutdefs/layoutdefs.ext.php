<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Layoutdefs/sasa_departamentos_sasa_municipios_1_sasa_Departamentos.php

 // created: 2020-05-15 15:42:24
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_sasa_municipios_1'] = array (
  'order' => 100,
  'module' => 'sasa_Municipios',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_sasa_municipios_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Layoutdefs/sasa_departamentos_accounts_1_sasa_Departamentos.php

 // created: 2020-05-15 15:42:47
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_accounts_1'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_accounts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Layoutdefs/sasa_departamentos_contacts_1_sasa_Departamentos.php

 // created: 2020-05-15 15:43:16
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Layoutdefs/sasa_departamentos_leads_1_sasa_Departamentos.php

 // created: 2020-05-15 15:43:53
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Layoutdefs/sasa_departamentos_accounts_2_sasa_Departamentos.php

 // created: 2020-05-15 16:05:56
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_accounts_2'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_accounts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Layoutdefs/sasa_departamentos_contacts_2_sasa_Departamentos.php

 // created: 2020-05-15 16:06:23
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_contacts_2'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_contacts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Layoutdefs/sasa_departamentos_leads_2_sasa_Departamentos.php

 // created: 2020-05-15 16:07:49
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_leads_2'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_leads_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>

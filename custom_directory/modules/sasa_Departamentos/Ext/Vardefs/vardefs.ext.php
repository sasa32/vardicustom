<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Vardefs/sasa_departamentos_sasa_municipios_1_sasa_Departamentos.php

// created: 2020-05-15 15:42:24
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_sasa_municipios_1"] = array (
  'name' => 'sasa_departamentos_sasa_municipios_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_sasa_municipios_1',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Vardefs/sasa_departamentos_accounts_1_sasa_Departamentos.php

// created: 2020-05-15 15:42:47
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_accounts_1"] = array (
  'name' => 'sasa_departamentos_accounts_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_accounts_1sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Vardefs/sasa_departamentos_contacts_1_sasa_Departamentos.php

// created: 2020-05-15 15:43:16
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_contacts_1"] = array (
  'name' => 'sasa_departamentos_contacts_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_contacts_1sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Vardefs/sasa_departamentos_leads_1_sasa_Departamentos.php

// created: 2020-05-15 15:43:53
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_leads_1"] = array (
  'name' => 'sasa_departamentos_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_leads_1sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Vardefs/sasa_paises_sasa_departamentos_1_sasa_Departamentos.php

// created: 2020-05-15 15:55:44
$dictionary["sasa_Departamentos"]["fields"]["sasa_paises_sasa_departamentos_1"] = array (
  'name' => 'sasa_paises_sasa_departamentos_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_sasa_departamentos_1',
  'source' => 'non-db',
  'module' => 'sasa_Paises',
  'bean_name' => 'sasa_Paises',
  'side' => 'right',
  'vname' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
  'link-type' => 'one',
);
$dictionary["sasa_Departamentos"]["fields"]["sasa_paises_sasa_departamentos_1_name"] = array (
  'name' => 'sasa_paises_sasa_departamentos_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_PAISES_TITLE',
  'save' => true,
  'id_name' => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
  'link' => 'sasa_paises_sasa_departamentos_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'name',
);
$dictionary["sasa_Departamentos"]["fields"]["sasa_paises_sasa_departamentos_1sasa_paises_ida"] = array (
  'name' => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_DEPARTAMENTOS_TITLE_ID',
  'id_name' => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
  'link' => 'sasa_paises_sasa_departamentos_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Vardefs/sasa_departamentos_accounts_2_sasa_Departamentos.php

// created: 2020-05-15 16:05:56
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_accounts_2"] = array (
  'name' => 'sasa_departamentos_accounts_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_accounts_2',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_accounts_2sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Vardefs/sasa_departamentos_contacts_2_sasa_Departamentos.php

// created: 2020-05-15 16:06:23
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_contacts_2"] = array (
  'name' => 'sasa_departamentos_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_contacts_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Departamentos/Ext/Vardefs/sasa_departamentos_leads_2_sasa_Departamentos.php

// created: 2020-05-15 16:07:49
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_leads_2"] = array (
  'name' => 'sasa_departamentos_leads_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_leads_2',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>

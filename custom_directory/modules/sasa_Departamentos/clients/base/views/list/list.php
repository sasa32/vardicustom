<?php
$module_name = 'sasa_Departamentos';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_codigodepartamento_c',
                'label' => 'LBL_SASA_CODIGODEPARTAMENTO_C',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_estado_c',
                'label' => 'LBL_SASA_ESTADO_C',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sasa_paises_sasa_departamentos_1_name',
                'label' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_PAISES_TITLE',
                'enabled' => true,
                'id' => 'SASA_PAISES_SASA_DEPARTAMENTOS_1SASA_PAISES_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);

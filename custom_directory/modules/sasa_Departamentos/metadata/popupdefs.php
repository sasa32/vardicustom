<?php
$popupMeta = array (
    'moduleMain' => 'sasa_Departamentos',
    'varName' => 'sasa_Departamentos',
    'orderBy' => 'sasa_departamentos.name',
    'whereClauses' => array (
  'name' => 'sasa_departamentos.name',
  'sasa_codigodepartamento_c' => 'sasa_departamentos.sasa_codigodepartamento_c',
  'sasa_estado_c' => 'sasa_departamentos.sasa_estado_c',
  'date_entered' => 'sasa_departamentos.date_entered',
  'sasa_paises_sasa_departamentos_1_name' => 'sasa_departamentos.sasa_paises_sasa_departamentos_1_name',
  'description' => 'sasa_departamentos.description',
  'date_modified' => 'sasa_departamentos.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sasa_codigodepartamento_c',
  6 => 'sasa_estado_c',
  7 => 'date_entered',
  8 => 'sasa_paises_sasa_departamentos_1_name',
  9 => 'description',
  10 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'name',
    'label' => 'LBL_NAME',
    'width' => '10',
    'name' => 'name',
  ),
  'sasa_codigodepartamento_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_CODIGODEPARTAMENTO_C',
    'width' => '10',
    'name' => 'sasa_codigodepartamento_c',
  ),
  'sasa_estado_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_ESTADO_C',
    'width' => '10',
    'name' => 'sasa_estado_c',
  ),
  'sasa_paises_sasa_departamentos_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_PAISES_TITLE',
    'id' => 'SASA_PAISES_SASA_DEPARTAMENTOS_1SASA_PAISES_IDA',
    'width' => 10,
    'name' => 'sasa_paises_sasa_departamentos_1_name',
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'name' => 'description',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10',
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'name',
    'label' => 'LBL_NAME',
    'width' => 10,
    'default' => true,
    'name' => 'name',
  ),
  'SASA_CODIGODEPARTAMENTO_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_SASA_CODIGODEPARTAMENTO_C',
    'width' => 10,
    'name' => 'sasa_codigodepartamento_c',
  ),
  'SASA_ESTADO_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_ESTADO_C',
    'width' => 10,
    'name' => 'sasa_estado_c',
  ),
  'SASA_PAISES_SASA_DEPARTAMENTOS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_PAISES_TITLE',
    'id' => 'SASA_PAISES_SASA_DEPARTAMENTOS_1SASA_PAISES_IDA',
    'width' => 10,
    'default' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
    'name' => 'date_entered',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
),
);

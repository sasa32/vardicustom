<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Vardefs/sasa_companias_sasa_unidad_de_negocio_1_sasa_Companias.php

// created: 2020-05-15 23:20:17
$dictionary["sasa_Companias"]["fields"]["sasa_companias_sasa_unidad_de_negocio_1"] = array (
  'name' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'type' => 'link',
  'relationship' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'source' => 'non-db',
  'module' => 'sasa_Unidad_de_Negocio',
  'bean_name' => 'sasa_Unidad_de_Negocio',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE',
  'id_name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Vardefs/sugarfield_name.php

 // created: 2020-05-16 20:30:06
$dictionary['sasa_Companias']['fields']['name']['len']='255';
$dictionary['sasa_Companias']['fields']['name']['audited']=false;
$dictionary['sasa_Companias']['fields']['name']['massupdate']=false;
$dictionary['sasa_Companias']['fields']['name']['unified_search']=false;
$dictionary['sasa_Companias']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_Companias']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Vardefs/sugarfield_sasa_cod_c.php

 // created: 2020-05-16 20:42:22
$dictionary['sasa_Companias']['fields']['sasa_cod_c']['labelValue']='Código Compañía';
$dictionary['sasa_Companias']['fields']['sasa_cod_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_Companias']['fields']['sasa_cod_c']['enforced']='1';
$dictionary['sasa_Companias']['fields']['sasa_cod_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2020-05-16 20:56:33
$dictionary['sasa_Companias']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['sasa_Companias']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['sasa_Companias']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Vardefs/documents_sasa_companias_1_sasa_Companias.php

// created: 2020-07-29 16:42:01
$dictionary["sasa_Companias"]["fields"]["documents_sasa_companias_1"] = array (
  'name' => 'documents_sasa_companias_1',
  'type' => 'link',
  'relationship' => 'documents_sasa_companias_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'documents_sasa_companias_1documents_ida',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Vardefs/sasa_companias_sasa_habeas_data_1_sasa_Companias.php

// created: 2020-07-29 23:30:52
$dictionary["sasa_Companias"]["fields"]["sasa_companias_sasa_habeas_data_1"] = array (
  'name' => 'sasa_companias_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'sasa_companias_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_COMPANIAS_TITLE',
  'id_name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>

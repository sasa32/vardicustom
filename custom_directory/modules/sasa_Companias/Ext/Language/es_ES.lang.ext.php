<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Language/es_ES.gvcompaniascustomfieldsv001-20200515.php


  $mod_strings['LBL_SASA_COD_C'] = 'Código Compañía';
  $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';

  
?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Language/es_ES.customsasa_companias_sasa_unidad_de_negocio_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Language/es_ES.customsasa_companias_sasa_unidad_de_negocio_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE'] = 'Unidad de Negocio';
$mod_strings['LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE'] = 'Unidad de Negocio';

?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE'] = 'Unidad de Negocio';
$mod_strings['LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE'] = 'Unidad de Negocio';


?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Compañía';
$mod_strings['LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE'] = 'Unidades de Negocio';
$mod_strings['LBL_SASA_COMPANIAS_FOCUS_DRAWER_DASHBOARD'] = 'Compañías Panel de enfoque';
$mod_strings['LBL_SASA_COMPANIAS_RECORD_DASHBOARD'] = 'Compañias Cuadro de mando del registro';
$mod_strings['LNK_NEW_RECORD'] = 'Crear Compañía';
$mod_strings['LNK_LIST'] = 'Vista Compañías';
$mod_strings['LBL_MODULE_NAME'] = 'Compañías';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Compañía';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Compañía';
$mod_strings['LNK_IMPORT_VCARD'] = 'Importar Compañía vCard';
$mod_strings['LNK_IMPORT_SASA_COMPANIAS'] = 'Importar Compañías';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Compañías Lista';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Buscar Compañía';
$mod_strings['LBL_SASA_COMPANIAS_SUBPANEL_TITLE'] = 'Compañías';

?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Language/es_ES.customdocuments_sasa_companias_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_DOCUMENTS_TITLE'] = 'Documentos';
$mod_strings['LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE'] = 'Documentos';

?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Language/es_ES.customsasa_habeas_data_sasa_companias_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_HABEAS_DATA_SASA_COMPANIAS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_SASA_HABEAS_DATA_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE'] = 'Habeas Data';

?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Language/es_ES.customsasa_companias_sasa_habeas_data_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_COMPANIAS_TITLE'] = 'Habeas Data';

?>

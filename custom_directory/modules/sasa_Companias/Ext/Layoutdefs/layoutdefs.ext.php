<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Layoutdefs/sasa_companias_sasa_unidad_de_negocio_1_sasa_Companias.php

 // created: 2020-05-15 23:20:17
$layout_defs["sasa_Companias"]["subpanel_setup"]['sasa_companias_sasa_unidad_de_negocio_1'] = array (
  'order' => 100,
  'module' => 'sasa_Unidad_de_Negocio',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'get_subpanel_data' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Layoutdefs/documents_sasa_companias_1_sasa_Companias.php

 // created: 2020-07-29 16:42:01
$layout_defs["sasa_Companias"]["subpanel_setup"]['documents_sasa_companias_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'documents_sasa_companias_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Layoutdefs/sasa_companias_sasa_habeas_data_1_sasa_Companias.php

 // created: 2020-07-29 23:30:52
$layout_defs["sasa_Companias"]["subpanel_setup"]['sasa_companias_sasa_habeas_data_1'] = array (
  'order' => 100,
  'module' => 'SASA_Habeas_Data',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'get_subpanel_data' => 'sasa_companias_sasa_habeas_data_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Companias/Ext/Layoutdefs/_overridesasa_Companias_subpanel_sasa_companias_sasa_habeas_data_1.php

//auto-generated file DO NOT EDIT
$layout_defs['sasa_Companias']['subpanel_setup']['sasa_companias_sasa_habeas_data_1']['override_subpanel_name'] = 'sasa_Companias_subpanel_sasa_companias_sasa_habeas_data_1';

?>

<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-07-29 16:42:01
$viewdefs['sasa_Companias']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'documents_sasa_companias_1',
  ),
);

// created: 2020-07-29 23:30:52
$viewdefs['sasa_Companias']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'context' => 
  array (
    'link' => 'sasa_companias_sasa_habeas_data_1',
  ),
);

// created: 2020-05-15 23:20:17
$viewdefs['sasa_Companias']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'context' => 
  array (
    'link' => 'sasa_companias_sasa_unidad_de_negocio_1',
  ),
);
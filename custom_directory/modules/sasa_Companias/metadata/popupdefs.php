<?php
$popupMeta = array (
    'moduleMain' => 'sasa_Companias',
    'varName' => 'sasa_Companias',
    'orderBy' => 'sasa_companias.name',
    'whereClauses' => array (
  'name' => 'sasa_companias.name',
  'sasa_cod_c' => 'sasa_companias_cstm.sasa_cod_c',
  'date_entered' => 'sasa_companias.date_entered',
  'date_modified' => 'sasa_companias.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sasa_cod_c',
  5 => 'date_entered',
  6 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'sasa_cod_c' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_COD_C',
    'width' => 10,
    'name' => 'sasa_cod_c',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SASA_COD_C' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_COD_C',
    'width' => 10,
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
),
);

<?php
$module_name = 'sasa_Companias';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_cod_c',
                'label' => 'LBL_SASA_COD_C',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Reports/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LEADS'] = 'Leads';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['DEFAULT_REPORT_TITLE_18'] = 'Leads por Toma de Contacto';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_ACCOUNTS'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto';
$mod_strings['LBL_ACCOUNT_REPORTS'] = 'Informes de Cliente y Prospectos';
$mod_strings['LBL_MY_TEAM_ACCOUNT_REPORTS'] = 'Informes de Cliente y Prospectos de Mi Equipo';
$mod_strings['LBL_MY_ACCOUNT_REPORTS'] = 'Informes de Mis Cliente y Prospectos';
$mod_strings['LBL_PUBLISHED_ACCOUNT_REPORTS'] = 'Informes de Cliente y Prospectos Publicados';
$mod_strings['DEFAULT_REPORT_TITLE_4'] = 'Lista de Cliente y Prospectos Cliente';
$mod_strings['DEFAULT_REPORT_TITLE_16'] = 'Clientes y Prospectos por Tipo por Industria';
$mod_strings['DEFAULT_REPORT_TITLE_43'] = 'Propietarios de Cliente y Prospectos Cliente';
$mod_strings['DEFAULT_REPORT_TITLE_44'] = 'Mis Nuevas Clientes y Prospectos Cliente';
$mod_strings['LBL_OPPORTUNITIES'] = 'Cotizaciones';
$mod_strings['DEFAULT_REPORT_TITLE_6'] = 'Cotizaciones por Toma de Contacto';
$mod_strings['DEFAULT_REPORT_TITLE_17'] = 'Cotizaciones Ganadas por Toma de Contacto';
$mod_strings['DEFAULT_REPORT_TITLE_45'] = 'Cotizaciones Por Etapa de Ventas';
$mod_strings['DEFAULT_REPORT_TITLE_46'] = 'Cotizaciones Por Tipo';
$mod_strings['DEFAULT_REPORT_TITLE_50'] = 'Cotizaciones Ganadas Por Cuenta';
$mod_strings['DEFAULT_REPORT_TITLE_51'] = 'Cotizaciones Ganadas Por Usuario';
$mod_strings['DEFAULT_REPORT_TITLE_52'] = 'Todas las Cotizaciones Abiertas';
$mod_strings['DEFAULT_REPORT_TITLE_53'] = 'Todas las Cotizaciones Cerradas';
$mod_strings['DEFAULT_REPORT_TITLE_56'] = 'Distribución de Productos de Cotizaciones para Pasado, Actual, Próximo período de tiempo por mes.';
$mod_strings['DEFAULT_REPORT_TITLE_57'] = 'Etapas de Ventas de Cotizaciones para Pasado, Actual, Próximo período de tiempo por mes.';
$mod_strings['DEFAULT_REPORT_TITLE_59'] = 'Cotizaciones Por Etapas de Ventas para Informes en período actual';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Cotización';
$mod_strings['LBL_OPPORTUNITY'] = 'Cotización';

?>

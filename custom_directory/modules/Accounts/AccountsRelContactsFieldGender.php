<?php

    if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

    class before_save_class_AccountsRelContactsFieldGender
    {

        function before_save_method_AccountsRelContactsFieldGender($bean, $event, $arguments)
        {
            
            //logic
            $GLOBALS['log']->security("\n\n\n\n\n");
            $GLOBALS['log']->security("******************************************************************");
            $GLOBALS['log']->security("INICIO LOGIC HOOK VALIDATE GENDER CONTACT " . date("F j, Y, g:i a"));
            $GLOBALS['log']->security("******************************************************************");
            //Previene el Event Loop
            if (!isset($bean->before_save_method_AccountsRelContactsFieldGender) || $bean->before_save_method_AccountsRelContactsFieldGender === false) {
                
                 $tipo_documento = $bean->sasa_tipo_documento_c;       
                 $numero_documento = $bean->sasa_numero_documento_c;
                 $tipo_persona = $bean->sasa_tipo_persona_c;
                 $id_cuenta = $bean->id;

                 $GLOBALS['log']->security("id Cuenta=>" . $id);

                    
                    $cantidaContactos = rel_contacts_account_gender($id_cuenta);

                   if(!empty($cantidaContactos) || $cantidaContactos >= 1){
                        $GLOBALS['log']->security("Contactos con genero vacio");
                        throw new SugarApiExceptionInvalidParameter("El campo género esta vacío, cantidad contactos: ". $cantidaContactos .". Favor diligenciarlo.");
                    }else{
                        $GLOBALS['log']->security("Contactos ok");
                    }
                
                        
            }//if
              
      
            $GLOBALS['log']->security("*****************************************************************");
            $GLOBALS['log']->security("FIN LOGIC HOOK VALIDATE GENDER CONTACT " . date("F j, Y, g:i a"));
            $GLOBALS['log']->security("*****************************************************************");
             

        }//method

 
    }//class


    function rel_contacts_account_gender($id){

        //$GLOBALS['log']->security("ingreso a la función ");
        //$GLOBALS['log']->security("id cuenta: " . $id);

        $genero;

                $queryRelContactAccount = "
                SELECT 
                a.name, 
                acs.sasa_tipo_persona_c, 
                cc.sasa_nombres_c AS nombre,  
                cc.sasa_tipo_documento_c,
                cc.sasa_numero_documento_c,
                cc.sasa_genero_c
                FROM accounts_contacts ac 
                INNER JOIN contacts c ON c.id = ac.contact_id
                INNER JOIN contacts_cstm cc ON cc.id_c = c.id
                INNER JOIN accounts a ON a.id = ac.account_id
                INNER JOIN accounts_cstm acs ON acs.id_c = a.id
                WHERE ac.account_id = '{$id}' AND (cc.sasa_genero_c IS NULL OR cc.sasa_genero_c = '') AND a.deleted = 0 AND c.deleted = 0";

                $result = $GLOBALS['db']->query($queryRelContactAccount);

                $cantidadQuery = mysqli_num_rows($result);

                $GLOBALS['log']->security("***Cantidad Consulta=>***" . $cantidadQuery);

                 while ($row = $GLOBALS['db']->fetchByAssoc($result))
                {
                    $GLOBALS['log']->security("Contacto: ". $row['nombre']." Genero: " . $row['genero']);
                    //$genero = $row['genero'];
                }

                return $cantidadQuery ;

    }
       
?>
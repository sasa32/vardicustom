<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class logic_hooks_accounts
{
	/*
	logic_hooks_accounts before_save 
	*/	
	function before_save(&$bean, $event, $arguments)
	{	
		
		if (!empty($bean->name)){
			$name = $bean->name;
			$bean->name = strtoupper($name);
		}
		/*
		*https://sasaconsultoria.sugarondemand.com/index.php#Tasks/d0030608-74ea-11ea-86e4-02fb8f607ac4
		*/
		if (empty($bean->team_id)){
			$bean->team_id = 1;
		}	
		
		if (empty($bean->team_set_id)){
			$bean->team_set_id = 1;
		}	
		
		if (empty($bean->acl_team_set_id)){
			$bean->acl_team_set_id = 1;
		}	

		if (empty($bean->created_by)){
			$bean->created_by = 1;
		}
		
		if (empty($bean->assigned_user_id)){
			$bean->assigned_user_id = $bean->created_by;
		}
		
		/*
		*https://sasaconsultoria.sugarondemand.com/index.php#Tasks/485a65ca-74ea-11ea-b889-06ab610ac1b0
		*/
		//Ciudad ppal
		if (!empty($bean->sasa_municipio_principal_2_c)){
			$result = $GLOBALS['db']->query("
			SELECT 
				id 
			FROM 
				sasa_municipios 
			WHERE 
				sasa_codigomunicipio_c LIKE '{$bean->sasa_municipio_principal_2_c}' 
				AND deleted=0 limit 1
			");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);	
			
			if ($bean->load_relationship('sasa_municipios_accounts_1')){
				$relatedBeans = $bean->sasa_municipios_accounts_1->add($sasa_municipios["id"]);
				$bean->sasa_municipio_principal_2_c ="";
			}
		}	
		
		//ciudad sec			
		if (!empty($bean->sasa_municipio_secundario_2_c)){
			$result = $GLOBALS['db']->query("
			SELECT 
				id 
			FROM 
				sasa_municipios 
			WHERE 
				sasa_codigomunicipio_c LIKE '{$bean->sasa_municipio_secundario_2_c}' 
				AND deleted=0 limit 1
			");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);	
			
			if ($bean->load_relationship('sasa_municipios_accounts_2')){
				$relatedBeans = $bean->sasa_municipios_accounts_2->add($sasa_municipios["id"]);
				$bean->sasa_municipio_secundario_2_c ="";
			}
		}

		//Ciudad 3
		if (!empty($bean->sasa_municipio_3_c)){
			$result = $GLOBALS['db']->query("
			SELECT 
				id 
			FROM 
				sasa_municipios 
			WHERE 
				sasa_codigomunicipio_c LIKE '{$bean->sasa_municipio_3_c}' 
				AND deleted=0 limit 1
			");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);	
			
			if ($bean->load_relationship('sasa_municipios_accounts_6')){
				$relatedBeans = $bean->sasa_municipios_accounts_6->add($sasa_municipios["id"]);
				$bean->sasa_municipio_3_c ="";
			}
		}	
		
		//ciudad telefono ppal
		if (!empty($bean->sasa_ciudad_tel_principal_2_c)){//Ciudad Teléfono Principal
			$result = $GLOBALS['db']->query("
				SELECT 
					id 
				FROM 
					sasa_municipios 
				WHERE 
					sasa_codigomunicipio_c LIKE '{$bean->sasa_ciudad_tel_principal_2_c}' 
					AND deleted=0 limit 1");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);
			
			if ($bean->load_relationship('sasa_municipios_accounts_3')){
				$relatedBeans = $bean->sasa_municipios_accounts_3->add($sasa_municipios["id"]);
				$bean->sasa_ciudad_tel_principal_2_c ="";
			}
			$bean->sasa_municipios_id1_c = $sasa_municipios["id"];
		}	
		
		//ciudad telefono alternativo
		if (!empty($bean->sasa_ciudad_tel_alternativ_2_c)){//Ciudad Teléfono Alternativo 
			$GLOBALS['log']->security("Tel Alter Accounts: ".$bean->sasa_ciudad_tel_alternativ_2_c);
			$result = $GLOBALS['db']->query("
			SELECT 
				id 
			FROM 
				sasa_municipios 
			WHERE 
				sasa_codigomunicipio_c LIKE '{$bean->sasa_ciudad_tel_alternativ_2_c}' 
				AND deleted=0 limit 1
			");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);
			
			if ($bean->load_relationship('sasa_municipios_accounts_4')){
				$relatedBeans = $bean->sasa_municipios_accounts_4->add($sasa_municipios["id"]);
				$bean->sasa_ciudad_tel_alternativ_2_c ="";
			}
			$bean->sasa_municipios_id_c = $sasa_municipios["id"];				
		}
		
		//ciudad telefono 3
		
		if (!empty($bean->sasa_ciudad_tel_3_c)){//Ciudad Teléfono 3
			
			$result = $GLOBALS['db']->query("
			SELECT 
				id 
			FROM 
				sasa_municipios 
			WHERE 
				sasa_codigomunicipio_c LIKE '{$bean->sasa_ciudad_tel_3_c}' 
				AND deleted=0 limit 1
			");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);
			
			if ($bean->load_relationship('sasa_municipios_accounts_5')){
				$relatedBeans = $bean->sasa_municipios_accounts_5->add($sasa_municipios["id"]);
				$bean->sasa_ciudad_tel_3_c ="";
			}	
			$bean->sasa_municipios_id_c = $sasa_municipios["id"];			
		}
			
		if (!isset($bean->logic_hooks_accounts_before_save_ignore_update) || $bean->logic_hooks_accounts_before_save_ignore_update === false){//antiloop
			$bean->logic_hooks_accounts_before_save_ignore_update = true;//antiloop
		
			/*
			* https://sasaconsultoria.sugarondemand.com/#Tasks/b3b62e86-5428-11ea-b5fd-02dfd714a754
			* Funcionalidad para validar si al guardar un registro este tiene diligenciado un dato de contactación tales como un email, celular principal o teléfono principal,
			*/
			if( empty(trim($bean->sasa_cel_principal_c)) and empty(trim($bean->sasa_phone_office_c)) and empty($bean->emailAddress->addresses) ){
				//throw new SugarApiExceptionInvalidParameter("Debe ingresar al menos un dato de contacto");//usar http_code 404 en lugar de 422 para que no salga error de conexion					
			}
			
			/*
			* Telefonos solo numeros (entero)
			*/
			$int_fields_error = false;
			$int_fields = array(
				trim($bean->sasa_cel_principal_c),
				trim($bean->sasa_phone_office_c),
				trim($bean->sasa_phone_alternate_c),
				trim($bean->sasa_cel_alternativo_c),
				trim($bean->sasa_phone_other_c),
				trim($sasa_celular_otro_c),
			);	
			foreach( $int_fields  as $key => $int_field){  
				if(!empty($int_field)){
					if(!preg_match('/^\d+$/',$int_field)){ //https://www.codexpedia.com/php/php-checking-if-a-variable-contains-only-digits/
						$int_fields_error = true;	
					}
				}
			}	
			if($int_fields_error){ 
				throw new SugarApiExceptionInvalidParameter("Debe ingresar sólo números en los teléfonos de contacto");//usar http_code 404 en lugar de 422 para que no salga error de conexion	
			}
			
			/*
			* https://sasaconsultoria.sugarondemand.com/index.php#Tasks/43e5491c-74ed-11ea-ba4c-06ab610ac1b0
			*/			
			if($bean->sasa_tipo_persona_c == "N"){//solo persona natural	
				$GLOBALS['log']->security("\nBeforesave accounts\n");
				$Contact = BeanFactory::newBean("Contacts");
				$Contact->retrieve_by_string_fields( 
					array( 
						'sasa_numero_documento_c' => $bean->sasa_numero_documento_c, 
						'sasa_tipo_documento_c' => $bean->sasa_tipo_documento_c
					) 
				); 
				if(!empty($Contact->id)){	
					$contact = BeanFactory::getBean("Contacts", $Contact->id, array('disable_row_level_security' => true));
					//https://onedrive.live.com/edit.aspx?cid=fde22c13c46e678b&page=view&resid=FDE22C13C46E678B!754&parId=FDE22C13C46E678B!563&app=Excel
					if($bean->date_modified != $contact->date_modified){ //para evitar saving loop						
						$contact->logic_hooks_contacts_before_save_ignore_update = true;//antiloop
						
						$contact->primary_address_city = $bean->billing_address_city;
						$contact->primary_address_country = $bean->billing_address_country;
						$contact->primary_address_postalcode = $bean->billing_address_postalcode;
						$contact->primary_address_state = $bean->billing_address_state;
						$contact->primary_address_street = $bean->billing_address_street;
						$contact->alt_address_city = $bean->shipping_address_city;
						$contact->alt_address_country = $bean->shipping_address_country;
						$contact->alt_address_postalcode = $bean->shipping_address_postalcode;
						$contact->alt_address_state = $bean->shipping_address_state;
						$contact->alt_address_street = $bean->shipping_address_street;
						$contact->description = $bean->description;
						$contact->email = $bean->email;
						$contact->facebook = $bean->facebook;
						$contact->twitter = $bean->twitter;
						$contact->phone_mobile = $bean->sasa_celular_principal_c;
						$contact->sasa_celular_alternativo_c = $bean->sasa_celular_alternativo_c;
						$contact->phone_other = $bean->phone_alternate;
						$contact->sasa_ciudad_tel_alternativ_c = $bean->sasa_ciudad_tel_alternativ_c;
						$contact->sasa_municipios_id1_c=$bean->sasa_municipios_id_c;//relacion ciudad
						$contact->sasa_extension_c = $bean->sasa_extension_c;
						$contact->sasa_tipo_documento_c = $bean->sasa_tipo_documento_c;
						$contact->sasa_numero_documento_c = $bean->sasa_numero_documento_c; 
						
						//ajustes https://sasaconsultoria.sugarondemand.com/#Tasks/cb4e1270-6e13-11ea-9318-065a187ca958
						$contact->sasa_phone_mobile_c = $bean->sasa_cel_principal_c;
						$contact->sasa_cel_alternativo_c=$bean->sasa_cel_alternativo_c;
						$contact->sasa_phone_other_c=$bean->sasa_phone_other_c;
						$contact->sasa_phone_home_c=$bean->sasa_phone_office_c;
						$contact->sasa_phone_work_c=$bean->sasa_phone_alternate_c;
						$contact->sasa_celular_otro_c=$bean->sasa_celular_otro_c;

						$contact->sasa_tipotel1_c=$bean->sasa_tipotel1_c;
						$contact->sasa_tipotel2_c=$bean->sasa_tipotel2_c;
						$contact->sasa_tipotel3_c=$bean->sasa_tipotel3_c;
						$contact->sasa_ciudad3_c=$bean->sasa_ciudad3_c;
						$contact->sasa_departamento3_c=$bean->sasa_departamento3_c;
						$contact->sasa_pais3_c=$bean->sasa_pais3_c;
						$contact->sasa_tipodirec1_c=$bean->sasa_tipodirec1_c;
						$contact->sasa_tipodirec2_c=$bean->sasa_tipodirec2_c;
						$contact->sasa_tipodirec3_c=$bean->sasa_tipodirec3_c;
						$contact->sasa_direccion3_c=$bean->sasa_direccion3_c;
						$contact->sasa_codigopostal3_c=$bean->sasa_codigopostal3_c;
						$contact->sasa_codigopostal3_c=$bean->sasa_codigopostal3_c;
						//Se comentaron estas lineas de mapeo de ciudades porque estaba probocando errores en las relaciones
						/*$contact->sasa_municipios_contacts_6sasa_municipios_ida=$bean->sasa_municipios_accounts_6sasa_municipios_ida;
						$contact->sasa_municipios_contacts_4sasa_municipios_ida=$bean->sasa_municipios_accounts_4sasa_municipios_ida;
						$contact->sasa_municipios_contacts_5sasa_municipios_ida=$bean->sasa_municipios_accounts_5sasa_municipios_ida;*/
						//https://sasaconsultoria.sugarondemand.com/#Tasks/d3ae5bc8-de7a-11ea-81c5-065a187ca958
						$contact->sasa_extension_c=$bean->sasa_extension_c;
						$contact->sasa_extension2_c=$bean->sasa_extension2_c;
						$contact->sasa_extension3_c=$bean->sasa_extension3_c;
						
						
						
						//ciudad ppal
						if ($bean->load_relationship('sasa_municipios_accounts_1')){
							$sasa_municipios_accounts_1 = array_shift($bean->sasa_municipios_accounts_1->getBeans());
							$contact->sasa_municipio_principal_2_c = $sasa_municipios_accounts_1->sasa_codigomunicipio_c;
						}		
								
						//ciudad alt
						if ($bean->load_relationship('sasa_municipios_accounts_2')){
							$sasa_municipios_accounts_2 = array_shift($bean->sasa_municipios_accounts_2->getBeans());
							$contact->sasa_municipio_secundario_2_c = $sasa_municipios_accounts_2->sasa_codigomunicipio_c;
						}
						
						//Ciudad del teléfono principal
						if ($bean->load_relationship('sasa_municipios_accounts_3')){
							$sasa_municipios_accounts_3 = array_shift($bean->sasa_municipios_accounts_3->getBeans());
							$contact->sasa_ciudad_tel_principal_2_c = $sasa_municipios_accounts_3->sasa_codigomunicipio_c;
						}
						
						//Ciudad del teléfono alternativa
						if ($bean->load_relationship('sasa_municipios_accounts_4')){
							$sasa_municipios_accounts_4 = array_shift($bean->sasa_municipios_accounts_4->getBeans());
							$contact->sasa_ciudad_tel_alternativ_2_c = $sasa_municipios_accounts_4->sasa_codigomunicipio_c;
						}	

						//Ciudad del teléfono 3
						if ($bean->load_relationship('sasa_municipios_accounts_5')){
							$sasa_municipios_accounts_5 = array_shift($bean->sasa_municipios_accounts_5->getBeans());
							$contact->sasa_ciudad_tel_oficina_2_c = $sasa_municipios_accounts_5->sasa_codigomunicipio_c;
						}	

						//Ciudad del dirección 3
						if ($bean->load_relationship('sasa_municipios_accounts_6')){
							$sasa_municipios_accounts_6 = array_shift($bean->sasa_municipios_accounts_6->getBeans());
							$contact->sasa_municipio_3_c = $sasa_municipios_accounts_6->sasa_codigomunicipio_c;
						}	
						
						
						
						
						$contact->save(false);
						
						//email
						$result = $GLOBALS['db']->query("
						delete 
						FROM email_addr_bean_rel 
						WHERE 
							bean_id = '{$contact->id}' 
							and bean_module = '{$contact->module_dir}'
						");
						$result = $GLOBALS['db']->query("
						replace into email_addr_bean_rel 
						(
							SELECT 
								uuid() as id, 
								email_address_id, 
								'{$contact->id}' as bean_id, 
								'{$contact->module_dir}' as bean_module, 
								primary_address,
								reply_to_Address,
								date_created, 
								date_modified,
								deleted 
							FROM 
								email_addr_bean_rel 
							where 
								bean_module = '{$bean->module_dir}' 
								and bean_id = '{$bean->id}' 
								and deleted = 0
						)
						");
					}
				}
			}			
			
			//https://sasaconsultoria.sugarondemand.com/#Tasks/caa7f1d8-6e13-11ea-b0d2-065a187ca958	
			
			if(!empty($bean->sasa_numero_documento_c)){ 
				$lead = BeanFactory::newBean("Leads");
				$lead->retrieve_by_string_fields( 
					array( 
						'sasa_numero_documento_c' => $bean->sasa_numero_documento_c, 
						'sasa_tipo_documento_c' => $bean->sasa_tipo_documento_c
					)
				); 
				$queryleadbycel = $GLOBALS['db']->query("SELECT leads.id, leads.first_name, leads.last_name from leads_cstm INNER JOIN leads ON leads_cstm.id_c=leads.id WHERE (sasa_phone_mobile_c='{$bean->sasa_cel_principal_c}' OR sasa_phone_mobile_c='{$bean->sasa_cel_alternativo_c}' OR sasa_phone_mobile_c='{$bean->sasa_celular_otro_c}')AND CONCAT(leads.first_name,' ',leads.last_name)='{$bean->name}' AND leads.deleted=0");
				$leadcel = mysqli_fetch_array($queryleadbycel);

				$querynamelead = $GLOBALS['db']->query("SELECT id, first_name, last_name from leads WHERE CONCAT(leads.first_name,' ',leads.last_name)='{$bean->name}' AND leads.deleted=0");

				$getnamelead = mysqli_fetch_array($querynamelead);
				if(!empty($lead->id)){	
					$Lead = BeanFactory::getBean("Leads", $lead->id, array('disable_row_level_security' => true));		
					$Lead->status = 'I';
					$Lead->converted = true;
					$Lead->account_id = $bean->id;
					$Lead->account_name = $bean->name;
					$Lead->save();
				}
				if ($leadcel['id'] != null || $leadcel['id']!= "") {
					$namelead = $leadcel['first_name']." ".$leadcel['last_name'];
					if (strtolower($namelead) == strtolower($bean->name)) {
						$Lead = BeanFactory::getBean("Leads", $leadcel['id'], array('disable_row_level_security' => true));
						$Lead->status = 'I';
						$Lead->converted = true;
						$Lead->account_id = $bean->id;
						$Lead->account_name = $bean->name;
						$Lead->save();
						
					}
				}elseif ($getnamelead['id'] != null || $getnamelead['id'] != "") {
					$Lead = BeanFactory::getBean("Leads", $getnamelead['id'], array('disable_row_level_security' => true));

					$bandemail = false;
					foreach ($bean->email as $key => $value) {
						$QueryLeadEmail = $GLOBALS['db']->query("SELECT * from email_addresses INNER JOIN email_addr_bean_rel ON email_addresses.id=email_addr_bean_rel.email_address_id where email_addresses.email_address='{$value['email_address']}' AND email_addr_bean_rel.bean_id='{$Lead->id}' AND email_addresses.deleted=0");
						if (mysqli_num_rows($QueryLeadEmail)>=1) {
							$bandemail=true;
						}
					}
					if ($bandemail) {
						
						$Lead->status = 'I';
						$Lead->converted = true;
						$Lead->account_id = $bean->id;
						$Lead->account_name = $bean->name;
						$Lead->save();
					}else{
						$queryleadbytel = $GLOBALS['db']->query("SELECT leads.id, leads.first_name, leads.last_name from leads_cstm INNER JOIN leads ON leads_cstm.id_c=leads.id where (sasa_phone_home_c='{$bean->sasa_phone_office_c}' OR sasa_phone_home_c='{$bean->sasa_phone_alternate_c}' OR sasa_phone_home_c='{$bean->sasa_phone_other_c}') AND CONCAT(leads.first_name,' ',leads.last_name)='{$bean->name}' AND leads.deleted=0");

						$leadtel = mysqli_fetch_array($queryleadbytel);
						if ($leadtel['id'] != null || $leadtel['id']!= "") {
							$Lead = BeanFactory::getBean("Leads", $leadtel['id'], array('disable_row_level_security' => true));	
							
							$Lead->status = 'I';
							$Lead->converted = true;
							$Lead->account_id = $bean->id;
							$Lead->account_name = $bean->name;
							$Lead->save();
						}
					}
				}
			}

			
			
			
		}	
	}
}
?>
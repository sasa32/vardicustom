<?php
// created: 2022-10-31 10:59:53
$viewdefs['Accounts']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        1 => 
        array (
          'name' => 'account_type',
          'comment' => 'The Company is of this type',
          'label' => 'LBL_TYPE',
        ),
        2 => 
        array (
          'name' => 'sasa_categoria_c',
          'label' => 'LBL_SASA_CATEGORIA_C',
        ),
        3 => 
        array (
          'name' => 'sasa_tipo_persona_c',
          'label' => 'LBL_SASA_TIPO_PERSONA_C',
        ),
        4 => 
        array (
          'name' => 'sasa_tipo_documento_c',
          'label' => 'LBL_SASA_TIPO_DOCUMENTO_C',
        ),
        5 => 
        array (
          'name' => 'sasa_numero_documento_c',
          'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
        ),
        6 => 
        array (
          'name' => 'sasa_celular_principal_c',
          'label' => 'LBL_SASA_CELULAR_PRINCIPAL_C',
        ),
        7 => 
        array (
          'name' => 'sasa_celular_alternativo_c',
          'label' => 'LBL_SASA_CELULAR_ALTERNATIVO_C',
        ),
        8 => 'phone_office',
        9 => 
        array (
          'name' => 'sasa_municipios_accounts_3_name',
          'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        10 => 
        array (
          'name' => 'phone_alternate',
          'comment' => 'An alternate phone number',
          'label' => 'LBL_PHONE_ALT',
        ),
        11 => 
        array (
          'name' => 'sasa_extension_c',
          'label' => 'LBL_SASA_EXTENSION_C',
        ),
        12 => 
        array (
          'name' => 'sasa_municipios_accounts_4_name',
          'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        13 => 'email',
        14 => 
        array (
          'name' => 'sasa_municipios_accounts_1_name',
          'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        15 => 'billing_address_street',
        16 => 'billing_address_state',
        17 => 'billing_address_city',
        18 => 'billing_address_postalcode',
        19 => 'billing_address_country',
        20 => 
        array (
          'name' => 'sasa_municipios_accounts_2_name',
          'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        21 => 'shipping_address_street',
        22 => 'shipping_address_state',
        23 => 'shipping_address_city',
        24 => 'shipping_address_postalcode',
        25 => 'shipping_address_country',
        26 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        27 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        28 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
        29 => 
        array (
          'name' => 'website',
          'displayParams' => 
          array (
            'type' => 'link',
          ),
        ),
        30 => 
        array (
          'name' => 'facebook',
          'comment' => 'The facebook name of the company',
          'label' => 'LBL_FACEBOOK',
        ),
        31 => 
        array (
          'name' => 'twitter',
          'comment' => 'The twitter name of the company',
          'label' => 'LBL_TWITTER',
        ),
        32 => 
        array (
          'name' => 'sasa_ingreso_mensual_c',
          'label' => 'LBL_SASA_INGRESO_MENSUAL_C',
        ),
        33 => 
        array (
          'name' => 'sasa_regimen_tributario_c',
          'label' => 'LBL_SASA_REGIMEN_TRIBUTARIO_C',
        ),
        34 => 
        array (
          'name' => 'sasa_regimen_fiscal_c',
          'label' => 'LBL_SASA_REGIMEN_FISCAL_C',
        ),
        35 => 
        array (
          'name' => 'industry',
          'comment' => 'The company belongs in this industry',
          'label' => 'LBL_INDUSTRY',
        ),
        36 => 
        array (
          'name' => 'sasa_sector_c',
          'label' => 'LBL_SASA_SECTOR_C',
        ),
        37 => 
        array (
          'name' => 'sasa_cantidad_empleados_c',
          'label' => 'LBL_SASA_CANTIDAD_EMPLEADOS_C',
        ),
        38 => 
        array (
          'name' => 'sasa_tipo_sigru_c',
          'label' => 'LBL_SASA_TIPO_SIGRU_C',
        ),
      ),
    ),
  ),
);
<?php
// created: 2022-10-31 10:59:53
$viewdefs['Accounts']['mobile']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'default' => true,
          'enabled' => true,
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'account_type',
          'label' => 'LBL_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_tipo_persona_c',
          'label' => 'LBL_SASA_TIPO_PERSONA_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_tipo_documento_c',
          'label' => 'LBL_SASA_TIPO_DOCUMENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_numero_documento_c',
          'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'phone_office',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_celular_principal_c',
          'label' => 'LBL_SASA_CELULAR_PRINCIPAL_C',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'email',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'date_modified',
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
);
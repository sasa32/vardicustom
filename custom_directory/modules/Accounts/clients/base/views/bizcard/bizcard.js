({
    plugins: [],

    initialize: function(options) {
        this._super('initialize', [options]);
    },

    _renderHtml: function() {
            this._super('_renderHtml');
    },
    fields_list:null,
    fields_count:null,
    fields_empty:null,
    fields_porcentarje:null,
    loadData: function(options) {
        if (_.isUndefined(this.model)) {
            return;
        }
        
        /*
        * Context
        */
        esto = _;
        este = this;
        var self = this;
	self.data = {};
        self.data.cl = {};
	
	var module = 'Accounts';
	self.data.account_id = this.model.attributes.id;
	self.data.account_id = this.model.attributes.id;

	app.api.call('GET', app.api.buildURL(module+'/' + self.data.account_id), null, {
	    success: function(data) {	       
	    
	        /*
	        * Campos
	        * https://sasaconsultoria.sugarondemand.com/index.php#Tasks/b2c23380-5428-11ea-a958-02dfd714a754
	        */
	        self.fields_list = {};			
		self.fields_list.name = data.name;
		self.fields_list.facebook = data.facebook;
		self.fields_list.twitter = data.twitter;
		self.fields_list.billing_address_street = data.billing_address_street;
		self.fields_list.billing_address_city = data.billing_address_city;
		self.fields_list.billing_address_state = data.billing_address_state;
		self.fields_list.billing_address_postalcode = data.billing_address_postalcode;
		self.fields_list.billing_address_country = data.billing_address_country;
		self.fields_list.website = data.website;
		//self.fields_list.shipping_address_street = data.shipping_address_street;
		//self.fields_list.shipping_address_city = data.shipping_address_city;
		//self.fields_list.shipping_address_state = data.shipping_address_state;
		//self.fields_list.shipping_address_country = data.shipping_address_country;
		self.fields_list.sasa_cel_principal_c = data.sasa_cel_principal_c;
		self.fields_list.sasa_ingreso_mensual_c = data.sasa_ingreso_mensual_c;
		//self.fields_list.sasa_extension_c = data.sasa_extension_c;
		self.fields_list.sasa_regimen_fiscal_c = data.sasa_regimen_fiscal_c;
		self.fields_list.sasa_phone_office_c = data.sasa_phone_office_c;
		//self.fields_list.sasa_phone_alternate_c = data.sasa_phone_alternate_c;
		//self.fields_list.sasa_cel_alternativo_c = data.sasa_cel_alternativo_c;
		self.fields_list.email = data.email;
		
		/*
		* https://sasaconsultoria.sugarondemand.com/index.php#Notes/85e58562-ab1f-11ea-a209-065a187ca958
		* Solo tipoe persona juridica 
		*/
		if(data.sasa_tipo_persona_c == 'J'){
			self.fields_list.industry = data.industry;
			self.fields_list.sasa_cantidad_empleados_c = data.sasa_cantidad_empleados_c;
			self.fields_list.sasa_regimen_tributario_c = data.sasa_regimen_tributario_c;
			self.fields_list.sasa_sector_c = data.sasa_sector_c;
		}
		
		self.fields_count =Object.keys(self.fields_list).length;
		self.fields_empty=0;
		$.each(self.fields_list, function(key, value) {
			if(!(typeof value === 'undefined' || value == null || value.length === 0)) {
				self.fields_empty++;
			}
		});
		
		if(self.fields_empty == 0 || self.fields_count == 0){
			self.fields_porcentarje = 0;
		}
		else{
			self.fields_porcentarje = (self.fields_empty/self.fields_count)*100;
		}

		console.log(" self.fields_list:", self.fields_list);
		// console.log(" self.fields_count:", self.fields_count);
		// console.log(" self.fields_empty:", self.fields_empty);
		// console.log(" self.fields_porcentarje:", self.fields_porcentarje);
	    	                
	        /*
	        * Dashlet Code
	        */                
	        self.data.cl.cursor1_margin = 10;
	        self.data.cl.cursor1 = 0; 
	        self.data.cl.cursor2 = Number(self.fields_porcentarje).toFixed(0);
	        self.data.cl.cursor3 = 100; 
	        self.data.cl.cursor2_progressbar =  100-self.data.cl.cursor2 ;
	        
	        /*
	        * ToDisplay
	        */
	        _.extend(self, self.data);
	        self.render();
	    },
	    error: function(data) {
	        console.log('Error: loadData bizcard');
	    }
	});
    },
})

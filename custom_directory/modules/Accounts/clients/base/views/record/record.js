/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Views.Base.Accounts.RecordView
 * @alias SUGAR.App.view.views.BaseAccountsRecordView
 * @extends View.Views.Base.RecordView
 */
({
    extendsFrom: 'RecordView',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this.plugins = _.union(this.plugins || [], ['HistoricalSummary']);
        var self=this;
    
        self._super('initialize', [options]);  
  		
  		self.ValidaRelContact(self.model.id);
    },


 
    ValidaRelContact: function(id){
        //Estableciendo los campos y filtros de la peticion para encontrar la SxC de AGCO y segmentación en SI
        //var CamposURL = "?fields[0]=sasa_ant_segmentacion_c&fields[1]=sasa_abreviatura_soc_c";
        //var FiltrosURL = "&filter[0][sasa_abreviatura_soc_c]=AGCO&filter[1][sasa_ant_segmentacion_c]=SI";
        //llamada
        console.log("datos:" + id);
       // var request =  App.api.call('read',app.api.buildURL('Accounts/')+id+'/link/accounts_nutsc_sociedad_por_cuenta_1'+CamposURL+FiltrosURL);
                
                /*request.xhr.done(function(data){
                    //Verificar si hay valores
                    if (data.records.length != 0) {
                        //Mensajede información 
                        App.alert.show('message-id', {
                            level: 'info',
                            title:' ',
                            messages: 'Si Socio',
                            autoClose: false
                        });
                    }
                });*/
                //Campos a mostrar del api
                var CamposURL = "?fields=sasa_genero_c, name";
                //Filtros para el api
                var FiltrosURL = '&filter=[{"sasa_genero_c":{"$is_null":""}}]';
                app.api.call("read", app.api.buildURL('Accounts/'+id+'/link/contacts'+CamposURL+FiltrosURL), null,{
                    success: function(response) {
                        // body...
                        console.log("SUCCESS: ", response);
                        console.log("CANTIDAD: ", response.records.length);
                       //Validación información del api
                       if (response.records.length >= 1) {
                        //Mensajede información 
                            App.alert.show('message-id', {
                                level: 'info',
                                title:'Información:',
                                messages: 'El campo género esta vacío, cantidad contactos: '+response.records.length+'. Favor diligenciarlo.',
                                autoClose: false
                            });
                        }else{
                            console.log("Contactos campo genero ok");
                        }


                    },
                    error: function(error){
                      console.log("ERROR: ", error);  
                    }
                });
    }
})
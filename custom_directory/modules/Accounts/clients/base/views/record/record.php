<?php
// created: 2022-10-31 10:59:53
$viewdefs['Accounts']['base']['view']['record'] = array (
  'buttons' => 
  array (
    0 => 
    array (
      'type' => 'button',
      'name' => 'cancel_button',
      'label' => 'LBL_CANCEL_BUTTON_LABEL',
      'css_class' => 'btn-invisible btn-link',
      'showOn' => 'edit',
      'events' => 
      array (
        'click' => 'button:cancel_button:click',
      ),
    ),
    1 => 
    array (
      'type' => 'rowaction',
      'event' => 'button:save_button:click',
      'name' => 'save_button',
      'label' => 'LBL_SAVE_BUTTON_LABEL',
      'css_class' => 'btn btn-primary',
      'showOn' => 'edit',
      'acl_action' => 'edit',
    ),
    2 => 
    array (
      'type' => 'actiondropdown',
      'name' => 'main_dropdown',
      'primary' => true,
      'showOn' => 'view',
      'buttons' => 
      array (
        0 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:edit_button:click',
          'name' => 'edit_button',
          'label' => 'LBL_EDIT_BUTTON_LABEL',
          'acl_action' => 'edit',
        ),
        1 => 
        array (
          'type' => 'escalate-action',
          'event' => 'button:escalate_button:click',
          'name' => 'escalate_button',
          'label' => 'LBL_ESCALATE_BUTTON_LABEL',
          'acl_action' => 'create',
        ),
        2 => 
        array (
          'type' => 'shareaction',
          'name' => 'share',
          'label' => 'LBL_RECORD_SHARE_BUTTON',
          'acl_action' => 'view',
        ),
        3 => 
        array (
          'type' => 'pdfaction',
          'name' => 'download-pdf',
          'label' => 'LBL_PDF_VIEW',
          'action' => 'download',
          'acl_action' => 'view',
        ),
        4 => 
        array (
          'type' => 'pdfaction',
          'name' => 'email-pdf',
          'label' => 'LBL_PDF_EMAIL',
          'action' => 'email',
          'acl_action' => 'view',
        ),
        5 => 
        array (
          'type' => 'divider',
        ),
        6 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:find_duplicates_button:click',
          'name' => 'find_duplicates_button',
          'label' => 'LBL_DUP_MERGE',
          'acl_action' => 'edit',
        ),
        7 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:duplicate_button:click',
          'name' => 'duplicate_button',
          'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
          'acl_module' => 'Accounts',
          'acl_action' => 'create',
        ),
        8 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:historical_summary_button:click',
          'name' => 'historical_summary_button',
          'label' => 'LBL_HISTORICAL_SUMMARY',
          'acl_action' => 'view',
        ),
        9 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:audit_button:click',
          'name' => 'audit_button',
          'label' => 'LNK_VIEW_CHANGE_LOG',
          'acl_action' => 'view',
        ),
        10 => 
        array (
          'type' => 'divider',
        ),
        11 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:delete_button:click',
          'name' => 'delete_button',
          'label' => 'LBL_DELETE_BUTTON_LABEL',
          'acl_action' => 'delete',
        ),
      ),
    ),
    3 => 
    array (
      'name' => 'sidebar_toggle',
      'type' => 'sidebartoggle',
    ),
  ),
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_HEADER',
      'header' => true,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'picture',
          'type' => 'avatar',
          'size' => 'large',
          'dismiss_label' => true,
          'readonly' => true,
          'white_list' => true,
          'licenseDependency' => 
          array (
            'HINT' => 
            array (
              'name' => 'hint_account_pic',
              'type' => 'hint-accounts-logo',
            ),
          ),
        ),
        1 => 
        array (
          'name' => 'name',
          'type' => 'name',
          'licenseDependency' => 
          array (
            'HINT' => 
            array (
              'type' => 'hint-accounts-search-dropdown',
            ),
          ),
        ),
        2 => 
        array (
          'name' => 'favorite',
          'label' => 'LBL_FAVORITE',
          'type' => 'favorite',
          'dismiss_label' => true,
        ),
        3 => 
        array (
          'name' => 'follow',
          'label' => 'LBL_FOLLOW',
          'type' => 'follow',
          'readonly' => true,
          'dismiss_label' => true,
        ),
        4 => 
        array (
          'name' => 'is_escalated',
          'type' => 'badge',
          'badge_label' => 'LBL_ESCALATED',
          'warning_level' => 'important',
          'dismiss_label' => true,
        ),
      ),
    ),
    1 => 
    array (
      'name' => 'panel_body',
      'label' => 'LBL_RECORD_BODY',
      'columns' => 2,
      'placeholders' => true,
      'newTab' => true,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'sasa_nombres_c',
          'label' => 'LBL_SASA_NOMBRES_C',
          'span' => 12,
        ),
        1 => 
        array (
          'name' => 'sasa_primerapellido_c',
          'label' => 'LBL_SASA_PRIMERAPELLIDO_C',
        ),
        2 => 
        array (
          'name' => 'sasa_last_name_2_c',
          'label' => 'LBL_SASA_LAST_NAME_2_C',
        ),
        3 => 
        array (
          'name' => 'sasa_tipo_persona_c',
          'label' => 'LBL_SASA_TIPO_PERSONA_C',
        ),
        4 => 
        array (
          'name' => 'sasa_categoria_c',
          'label' => 'LBL_SASA_CATEGORIA_C',
        ),
        5 => 
        array (
          'name' => 'sasa_tipo_documento_c',
          'label' => 'LBL_SASA_TIPO_DOCUMENTO_C',
        ),
        6 => 
        array (
          'name' => 'sasa_numero_documento_c',
          'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
        ),
        7 => 
        array (
          'name' => 'sasa_cel_principal_c',
          'label' => 'LBL_SASA_CEL_PRINCIPAL_C',
        ),
        8 => 'email',
        9 => 
        array (
          'name' => 'sasa_cel_alternativo_c',
          'label' => 'LBL_SASA_CEL_ALTERNATIVO_C',
        ),
        10 => 
        array (
          'name' => 'sasa_celular_otro_c',
          'label' => 'LBL_SASA_CELULAR_OTRO_C',
        ),
        11 => 
        array (
          'name' => 'sasa_tipotel1_c',
          'label' => 'LBL_SASA_TIPOTEL1_C',
        ),
        12 => 
        array (
          'name' => 'sasa_phone_office_c',
          'label' => 'LBL_SASA_PHONE_OFFICE_C',
        ),
        13 => 
        array (
          'name' => 'sasa_extension_c',
          'label' => 'LBL_SASA_EXTENSION_C',
        ),
        14 => 
        array (
          'name' => 'sasa_municipios_accounts_3_name',
        ),
        15 => 
        array (
          'name' => 'sasa_tipotel2_c',
          'label' => 'LBL_SASA_TIPOTEL2_C',
        ),
        16 => 
        array (
          'name' => 'sasa_phone_alternate_c',
          'label' => 'LBL_SASA_PHONE_ALTERNATE_C',
        ),
        17 => 
        array (
          'name' => 'sasa_extension2_c',
          'label' => 'LBL_SASA_EXTENSION2_C',
        ),
        18 => 
        array (
          'name' => 'sasa_municipios_accounts_4_name',
        ),
        19 => 
        array (
          'name' => 'sasa_tipotel3_c',
          'label' => 'LBL_SASA_TIPOTEL3_C',
        ),
        20 => 
        array (
          'name' => 'sasa_phone_other_c',
          'label' => 'LBL_SASA_PHONE_OTHER_C',
        ),
        21 => 
        array (
          'name' => 'sasa_extension3_c',
          'label' => 'LBL_SASA_EXTENSION3_C',
        ),
        22 => 
        array (
          'name' => 'sasa_municipios_accounts_5_name',
          'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        23 => 
        array (
          'name' => 'sasa_tipodirec1_c',
          'label' => 'LBL_SASA_TIPODIREC1_C',
        ),
        24 => 
        array (
          'name' => 'billing_address',
          'type' => 'fieldset',
          'css_class' => 'address',
          'label' => 'LBL_BILLING_ADDRESS',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'billing_address_street',
              'css_class' => 'address_street',
              'placeholder' => 'LBL_BILLING_ADDRESS_STREET',
            ),
            1 => 
            array (
              'name' => 'billing_address_city',
              'css_class' => 'address_city',
              'placeholder' => 'LBL_BILLING_ADDRESS_CITY',
            ),
            2 => 
            array (
              'name' => 'billing_address_state',
              'css_class' => 'address_state',
              'placeholder' => 'LBL_BILLING_ADDRESS_STATE',
            ),
            3 => 
            array (
              'name' => 'billing_address_postalcode',
              'css_class' => 'address_zip',
              'placeholder' => 'LBL_BILLING_ADDRESS_POSTALCODE',
            ),
            4 => 
            array (
              'name' => 'billing_address_country',
              'css_class' => 'address_country',
              'placeholder' => 'LBL_BILLING_ADDRESS_COUNTRY',
            ),
          ),
        ),
        25 => 
        array (
          'name' => 'sasa_municipios_accounts_1_name',
          'span' => 12,
        ),
        26 => 
        array (
          'name' => 'sasa_tipodirec2_c',
          'label' => 'LBL_SASA_TIPODIREC2_C',
        ),
        27 => 
        array (
          'name' => 'shipping_address',
          'type' => 'fieldset',
          'css_class' => 'address',
          'label' => 'LBL_SHIPPING_ADDRESS',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'shipping_address_street',
              'css_class' => 'address_street',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_STREET',
            ),
            1 => 
            array (
              'name' => 'shipping_address_city',
              'css_class' => 'address_city',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_CITY',
            ),
            2 => 
            array (
              'name' => 'shipping_address_state',
              'css_class' => 'address_state',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_STATE',
            ),
            3 => 
            array (
              'name' => 'shipping_address_postalcode',
              'css_class' => 'address_zip',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
            ),
            4 => 
            array (
              'name' => 'shipping_address_country',
              'css_class' => 'address_country',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
            ),
          ),
        ),
        28 => 
        array (
          'name' => 'sasa_municipios_accounts_2_name',
          'span' => 12,
        ),
        29 => 
        array (
          'name' => 'sasa_tipodirec3_c',
          'label' => 'LBL_SASA_TIPODIREC3_C',
        ),
        30 => 
        array (
          'name' => 'sasa_caja_direccion3_c',
          'type' => 'fieldset',
          'css_class' => 'address',
          'label' => 'LBL_SASA_DIRECCION3_C',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'sasa_direccion3_c',
              'css_class' => 'address_street',
              'placeholder' => 'LBL_SASA_DIRECCION3_C',
            ),
            1 => 
            array (
              'name' => 'sasa_ciudad3_c',
              'css_class' => 'address_city',
              'placeholder' => 'LBL_SASA_CIUDAD3_C',
            ),
            2 => 
            array (
              'name' => 'sasa_departamento3_c',
              'css_class' => 'address_state',
              'placeholder' => 'LBL_SASA_DEPARTAMENTO3_C',
            ),
            3 => 
            array (
              'name' => 'sasa_codigopostal3_c',
              'css_class' => 'address_zip',
              'placeholder' => 'LBL_SASA_CODIGOPOSTAL3_C',
            ),
            4 => 
            array (
              'name' => 'sasa_pais3_c',
              'css_class' => 'address_country',
              'placeholder' => 'LBL_SASA_PAIS3_C',
            ),
          ),
        ),
        31 => 
        array (
          'name' => 'sasa_municipios_accounts_6_name',
          'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE',
          'span' => 12,
        ),
        32 => 
        array (
          'name' => 'sasa_empleadodelgrupo_c',
          'label' => 'LBL_SASA_EMPLEADODELGRUPO_C',
        ),
        33 => 
        array (
          'name' => 'sasa_compania_c',
          'label' => 'LBL_SASA_COMPANIA_C',
        ),
        34 => 
        array (
          'name' => 'description',
          'span' => 6,
        ),
        35 => 
        array (
          'name' => 'tag',
          'span' => 6,
        ),
      ),
    ),
    2 => 
    array (
      'name' => 'panel_hidden',
      'label' => 'LBL_RECORD_SHOWMORE',
      'hide' => true,
      'columns' => 2,
      'placeholders' => true,
      'newTab' => true,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 'website',
        1 => 
        array (
          'name' => 'facebook',
          'comment' => 'The facebook name of the company',
          'label' => 'LBL_FACEBOOK',
        ),
        2 => 
        array (
          'name' => 'twitter',
        ),
        3 => 
        array (
          'name' => 'sasa_ingreso_mensual_c',
          'label' => 'LBL_SASA_INGRESO_MENSUAL_C',
        ),
        4 => 
        array (
          'name' => 'sasa_regimen_fiscal_c',
          'label' => 'LBL_SASA_REGIMEN_FISCAL_C',
        ),
        5 => 
        array (
          'name' => 'sasa_tipo_sigru_c',
          'label' => 'LBL_SASA_TIPO_SIGRU_C',
        ),
        6 => 
        array (
          'name' => 'sasa_regimen_tributario_c',
          'label' => 'LBL_SASA_REGIMEN_TRIBUTARIO_C',
        ),
        7 => 
        array (
          'name' => 'sasa_cantidad_empleados_c',
          'label' => 'LBL_SASA_CANTIDAD_EMPLEADOS_C',
        ),
        8 => 
        array (
          'name' => 'industry',
        ),
        9 => 
        array (
          'name' => 'sasa_sector_c',
          'label' => 'LBL_SASA_SECTOR_C',
        ),
        10 => 
        array (
          'name' => 'date_entered_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_ENTERED',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_entered',
            ),
            1 => 
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 => 
            array (
              'name' => 'created_by_name',
            ),
          ),
        ),
        11 => 
        array (
          'name' => 'date_modified_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_MODIFIED',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_modified',
            ),
            1 => 
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 => 
            array (
              'name' => 'modified_by_name',
            ),
          ),
        ),
        12 => 'hint_account_size',
        13 => 'hint_account_industry',
        14 => 'hint_account_location',
        15 => 'hint_account_founded_year',
        16 => 'hint_account_industry_tags',
        17 => 'hint_account_naics_code_lbl',
        18 => 'hint_account_fiscal_year_end',
        19 => 
        array (
          'name' => 'hint_account_facebook_handle',
          'type' => 'stage2_url',
        ),
        20 => 
        array (
          'name' => 'hint_account_logo',
          'type' => 'stage2_image',
          'readonly' => true,
          'dismiss_label' => true,
          'white_list' => true,
          'fields' => 
          array (
            0 => 'hint_account_pic',
          ),
        ),
        21 => 
        array (
          'name' => 'geocode_status',
          'licenseFilter' => 
          array (
            0 => 'MAPS',
          ),
        ),
      ),
    ),
    3 => 
    array (
      'name' => 'panel_hint',
      'label' => 'LBL_HINT_PANEL',
      'columns' => 2,
      'labels' => true,
      'labelsOnTop' => true,
      'placeholders' => true,
      'fields' => 
      array (
        0 => 'hint_account_size',
        1 => 'hint_account_industry',
        2 => 'hint_account_location',
        3 => 'hint_account_founded_year',
        4 => 'hint_account_industry_tags',
        5 => 'hint_account_naics_code_lbl',
        6 => 'hint_account_fiscal_year_end',
        7 => 
        array (
          'name' => 'hint_account_facebook_handle',
          'type' => 'stage2_url',
        ),
        8 => 
        array (
          'name' => 'hint_account_logo',
          'type' => 'stage2_image',
          'readonly' => true,
          'dismiss_label' => true,
          'white_list' => true,
          'fields' => 
          array (
            0 => 'hint_account_pic',
          ),
        ),
      ),
      'licenseFilter' => 
      array (
        0 => 'HINT',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'useTabs' => true,
  ),
);
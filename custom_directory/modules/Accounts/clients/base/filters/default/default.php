<?php
// created: 2021-06-10 16:02:16
$viewdefs['Accounts']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'sasa_numero_documento_c' => 
    array (
    ),
    'email' => 
    array (
    ),
    'industry' => 
    array (
    ),
    'annual_revenue' => 
    array (
    ),
    'address_street' => 
    array (
      'dbFields' => 
      array (
        0 => 'billing_address_street',
        1 => 'shipping_address_street',
      ),
      'vname' => 'LBL_STREET',
      'type' => 'text',
    ),
    'address_city' => 
    array (
      'dbFields' => 
      array (
        0 => 'billing_address_city',
        1 => 'shipping_address_city',
      ),
      'vname' => 'LBL_CITY',
      'type' => 'text',
    ),
    'address_state' => 
    array (
      'dbFields' => 
      array (
        0 => 'billing_address_state',
        1 => 'shipping_address_state',
      ),
      'vname' => 'LBL_STATE',
      'type' => 'text',
    ),
    'address_postalcode' => 
    array (
      'dbFields' => 
      array (
        0 => 'billing_address_postalcode',
        1 => 'shipping_address_postalcode',
      ),
      'vname' => 'LBL_POSTAL_CODE',
      'type' => 'text',
    ),
    'address_country' => 
    array (
      'dbFields' => 
      array (
        0 => 'billing_address_country',
        1 => 'shipping_address_country',
      ),
      'vname' => 'LBL_COUNTRY',
      'type' => 'text',
    ),
    'sasa_municipios_accounts_1_name' => 
    array (
    ),
    'sasa_phone_office_c' => 
    array (
    ),
    'rating' => 
    array (
    ),
    'sasa_departamentos_accounts_1_name' => 
    array (
    ),
    'sasa_tipo_persona_c' => 
    array (
    ),
    'sasa_tipo_documento_c' => 
    array (
    ),
    'sasa_cel_principal_c' => 
    array (
    ),
    'ownership' => 
    array (
    ),
    'employees' => 
    array (
    ),
    'sic_code' => 
    array (
    ),
    'ticker_symbol' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'tag' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'assigned_user_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'created_by_name' => 
    array (
    ),
  ),
);
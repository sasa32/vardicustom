<?php  
	
	/**
	 * 
	 */
	require_once('include/SugarQuery/SugarQuery.php');
	class Loader_Cuentas{
		var $return;
		var $db;
		
		function __construct($data_array){
			$this->return = array();
			$this->db = DBManagerFactory::getInstance();
			$this->main_cuentas($data_array);
		}

		public function main_cuentas($data_array){
			try{
				foreach ($data_array as $key => $value) {
					$validate = $this->validate_record($value);
					if($validate['band']){
						$status = $this->create_cuenta($value);
						$this->return[] = $status;
					}else{
						$this->return[] =array('Error' => '120', 'Description' => implode(' - ', $validate['error']));
					}
				}
			}catch(Exception $e){
				return array('Error' => '100', 'Description' => $e->getMessage());
			}
		}

		public function create_cuenta($record){
			$sql_get_account = "SELECT id FROM accounts INNER JOIN accounts_cstm ON accounts.id=accounts_cstm.id_c WHERE accounts_cstm.sasa_numero_documento_c='{$record['sasa_numero_documento_c']}' AND deleted=0";
			
			$account =  $this->db->query($sql_get_account);
			while($row = $this->db->fetchByAssoc($account)){
				$account_state_id = $row['id'];
			}

			if(!empty($account_state_id)){
				$status = "Update";
				$cuenta = BeanFactory::getBean('Accounts', $account_state_id, array('disable_row_level_security' => true));
			}else{
				$status = "Created";
				$cuenta = BeanFactory::newBean('Accounts');
			}

			$dir_address2 = array();
			foreach ($record as $key => $value) {
				if ($value == "(null)") {
					$cuenta->{$key} = null;
				}else{
					$cuenta->{$key} = $value;
					if ($key == "emailsecundarios") {
						//Separando las direcciones de correo
						if ($value != "" || $value != null || $value!="(null)") {
							$dir_address = explode(";", $value);
							array_push($dir_address2, $dir_address);
						}
					}
					if ($key == "date_entered") {
						//$date = new DateTime($value);
						$date = date("d-m-Y", strtotime($value));
						$date = new DateTime($date);
						$dateformat = $date->format('Y-m-d');
						$cuenta->creacionsigru_c = $dateformat;
					}
					if ($key == "date_modified") {
						//$date = new DateTime($value);
						$date = date("d-m-Y", strtotime($value));
						$date = new DateTime($date);
						$dateformat = $date->format('Y-m-d');
						$cuenta->modificacionsigru_c = $dateformat;
					}
				}	
				
			}

			$cuenta->save();

			//Relacionar Direcciones de correo secundrias con la Cuenta.
			$dir_email = new SugarEmailAddress;
			if (!empty($cuenta->email1)) {
				//Asociar el email principal
				$dir_email->addAddress($cuenta->email1,true);
			}
			foreach ($dir_address2[0] as $llave => $valor) {
				$dir_address = substr($valor, 0,-4);
				
				if (!empty($dir_address)) {
					//Asociar los emails secundarios
					if ($cuenta->email1 != $dir_address) {
						$dir_email->addAddress($dir_address,false);
					}
				}
			}
			$dir_email->save($cuenta->id, "Accounts");

			return array('status' => $status, 'id' => $cuenta->id); 
		}


		public function validate_record($record){
			$validate_array = array('band' => true);

			return $validate_array;
		}

		public function get_return(){
			return $this->return;
		}
	}


?>
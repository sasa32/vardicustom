<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Layoutdefs/accounts_contacts_1_Accounts.php

 // created: 2018-12-04 16:24:59
$layout_defs["Accounts"]["subpanel_setup"]['accounts_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'accounts_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Layoutdefs/accounts_sasa_unidadnegclienteprospect_1_Accounts.php

 // created: 2020-05-21 16:21:34
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_unidadnegclienteprospect_1'] = array (
  'order' => 100,
  'module' => 'SASA_UnidadNegClienteProspect',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'get_subpanel_data' => 'accounts_sasa_unidadnegclienteprospect_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Layoutdefs/_overrideAccount_subpanel_accounts_sasa_unidadnegclienteprospect_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_sasa_unidadnegclienteprospect_1']['override_subpanel_name'] = 'Account_subpanel_accounts_sasa_unidadnegclienteprospect_1';

?>

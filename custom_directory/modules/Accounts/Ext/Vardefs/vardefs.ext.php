<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_paises_accounts_1_Accounts.php

// created: 2020-05-15 15:40:50
$dictionary["Account"]["fields"]["sasa_paises_accounts_1"] = array (
  'name' => 'sasa_paises_accounts_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_accounts_1',
  'source' => 'non-db',
  'module' => 'sasa_Paises',
  'bean_name' => 'sasa_Paises',
  'side' => 'right',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_paises_accounts_1sasa_paises_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_paises_accounts_1_name"] = array (
  'name' => 'sasa_paises_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_1_FROM_SASA_PAISES_TITLE',
  'save' => true,
  'id_name' => 'sasa_paises_accounts_1sasa_paises_ida',
  'link' => 'sasa_paises_accounts_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_paises_accounts_1sasa_paises_ida"] = array (
  'name' => 'sasa_paises_accounts_1sasa_paises_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_paises_accounts_1sasa_paises_ida',
  'link' => 'sasa_paises_accounts_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_departamentos_accounts_1_Accounts.php

// created: 2020-05-15 15:42:47
$dictionary["Account"]["fields"]["sasa_departamentos_accounts_1"] = array (
  'name' => 'sasa_departamentos_accounts_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_accounts_1',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'side' => 'right',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_departamentos_accounts_1sasa_departamentos_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_departamentos_accounts_1_name"] = array (
  'name' => 'sasa_departamentos_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_departamentos_accounts_1sasa_departamentos_ida',
  'link' => 'sasa_departamentos_accounts_1',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_departamentos_accounts_1sasa_departamentos_ida"] = array (
  'name' => 'sasa_departamentos_accounts_1sasa_departamentos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_departamentos_accounts_1sasa_departamentos_ida',
  'link' => 'sasa_departamentos_accounts_1',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_municipios_accounts_1_Accounts.php

// created: 2020-05-15 15:44:33
$dictionary["Account"]["fields"]["sasa_municipios_accounts_1"] = array (
  'name' => 'sasa_municipios_accounts_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_1',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_municipios_accounts_1sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_1_name"] = array (
  'name' => 'sasa_municipios_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_accounts_1sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_1',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_1sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_accounts_1sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_municipios_accounts_1sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_1',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_paises_accounts_2_Accounts.php

// created: 2020-05-15 16:04:27
$dictionary["Account"]["fields"]["sasa_paises_accounts_2"] = array (
  'name' => 'sasa_paises_accounts_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_accounts_2',
  'source' => 'non-db',
  'module' => 'sasa_Paises',
  'bean_name' => 'sasa_Paises',
  'side' => 'right',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_paises_accounts_2sasa_paises_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_paises_accounts_2_name"] = array (
  'name' => 'sasa_paises_accounts_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_2_FROM_SASA_PAISES_TITLE',
  'save' => true,
  'id_name' => 'sasa_paises_accounts_2sasa_paises_ida',
  'link' => 'sasa_paises_accounts_2',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_paises_accounts_2sasa_paises_ida"] = array (
  'name' => 'sasa_paises_accounts_2sasa_paises_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_2_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_paises_accounts_2sasa_paises_ida',
  'link' => 'sasa_paises_accounts_2',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_departamentos_accounts_2_Accounts.php

// created: 2020-05-15 16:05:56
$dictionary["Account"]["fields"]["sasa_departamentos_accounts_2"] = array (
  'name' => 'sasa_departamentos_accounts_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_accounts_2',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'side' => 'right',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_departamentos_accounts_2sasa_departamentos_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_departamentos_accounts_2_name"] = array (
  'name' => 'sasa_departamentos_accounts_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_departamentos_accounts_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_accounts_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_departamentos_accounts_2sasa_departamentos_ida"] = array (
  'name' => 'sasa_departamentos_accounts_2sasa_departamentos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_departamentos_accounts_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_accounts_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_municipios_accounts_2_Accounts.php

// created: 2020-05-15 16:08:40
$dictionary["Account"]["fields"]["sasa_municipios_accounts_2"] = array (
  'name' => 'sasa_municipios_accounts_2',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_2',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_municipios_accounts_2sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_2_name"] = array (
  'name' => 'sasa_municipios_accounts_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_accounts_2sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_2',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_2sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_accounts_2sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_municipios_accounts_2sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_2',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_municipios_accounts_3_Accounts.php

// created: 2020-05-15 16:11:28
$dictionary["Account"]["fields"]["sasa_municipios_accounts_3"] = array (
  'name' => 'sasa_municipios_accounts_3',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_3',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_municipios_accounts_3sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_3_name"] = array (
  'name' => 'sasa_municipios_accounts_3_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_accounts_3sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_3',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_3sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_accounts_3sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_municipios_accounts_3sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_3',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_municipios_accounts_4_Accounts.php

// created: 2020-05-15 16:11:54
$dictionary["Account"]["fields"]["sasa_municipios_accounts_4"] = array (
  'name' => 'sasa_municipios_accounts_4',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_4',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_municipios_accounts_4sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_4_name"] = array (
  'name' => 'sasa_municipios_accounts_4_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_accounts_4sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_4',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_4sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_accounts_4sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_municipios_accounts_4sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_4',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_contacts_1_Accounts.php

// created: 2018-12-04 16:24:59
$dictionary["Account"]["fields"]["accounts_contacts_1"] = array (
  'name' => 'accounts_contacts_1',
  'type' => 'link',
  'relationship' => 'accounts_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contact_id',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_sasa_unidadnegclienteprospect_1_Accounts.php

// created: 2020-05-21 16:21:34
$dictionary["Account"]["fields"]["accounts_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'accounts_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'SASA_UnidadNegClienteProspect',
  'bean_name' => 'SASA_UnidadNegClienteProspect',
  'vname' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/rli_link_workflow.php

$dictionary['Account']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/readonly.php

/*
* Requerimiento en https://sasaconsultoria.sugarondemand.com/index.php#Tasks/5ea23328-7e7a-11ea-b3b6-02fb8f607ac4
*/
$dictionary["Account"]["fields"]["billing_address_city"]['readonly'] = true;
$dictionary["Account"]["fields"]["billing_address_state"]['readonly'] = true;
$dictionary["Account"]["fields"]["billing_address_country"]['readonly'] = true;

$dictionary["Account"]["fields"]["shipping_address_city"]['readonly'] = true;
$dictionary["Account"]["fields"]["shipping_address_state"]['readonly'] = true;
$dictionary["Account"]["fields"]["shipping_address_country"]['readonly'] = true;

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_account_type.php

 // created: 2020-05-16 09:21:23
$dictionary['Account']['fields']['account_type']['len']=100;
$dictionary['Account']['fields']['account_type']['required']=true;
$dictionary['Account']['fields']['account_type']['audited']=true;
$dictionary['Account']['fields']['account_type']['massupdate']=false;
$dictionary['Account']['fields']['account_type']['comments']='The Company is of this type';
$dictionary['Account']['fields']['account_type']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['account_type']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['account_type']['merge_filter']='disabled';
$dictionary['Account']['fields']['account_type']['calculated']=false;
$dictionary['Account']['fields']['account_type']['dependency']=false;
$dictionary['Account']['fields']['account_type']['options']='account_type_list';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_municipios_accounts_6_Accounts.php

// created: 2020-07-31 15:03:32
$dictionary["Account"]["fields"]["sasa_municipios_accounts_6"] = array (
  'name' => 'sasa_municipios_accounts_6',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_6',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_municipios_accounts_6sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_6_name"] = array (
  'name' => 'sasa_municipios_accounts_6_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_accounts_6sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_6',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_6sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_accounts_6sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_municipios_accounts_6sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_6',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_annual_revenue.php

 // created: 2020-05-16 23:43:11
$dictionary['Account']['fields']['annual_revenue']['len']='100';
$dictionary['Account']['fields']['annual_revenue']['audited']=false;
$dictionary['Account']['fields']['annual_revenue']['massupdate']=false;
$dictionary['Account']['fields']['annual_revenue']['comments']='Annual revenue for this company';
$dictionary['Account']['fields']['annual_revenue']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['annual_revenue']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['annual_revenue']['merge_filter']='disabled';
$dictionary['Account']['fields']['annual_revenue']['reportable']=false;
$dictionary['Account']['fields']['annual_revenue']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['annual_revenue']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sasa_municipios_accounts_5_Accounts.php

// created: 2020-07-31 09:41:58
$dictionary["Account"]["fields"]["sasa_municipios_accounts_5"] = array (
  'name' => 'sasa_municipios_accounts_5',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_5',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_5_name"] = array (
  'name' => 'sasa_municipios_accounts_5_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_5',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_5sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_5',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_country.php

 // created: 2020-07-31 10:54:23
$dictionary['Account']['fields']['billing_address_country']['len']='255';
$dictionary['Account']['fields']['billing_address_country']['audited']=true;
$dictionary['Account']['fields']['billing_address_country']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['billing_address_country']['importable']='false';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['billing_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_country']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_state.php

 // created: 2020-07-31 10:55:18
$dictionary['Account']['fields']['shipping_address_state']['len']='100';
$dictionary['Account']['fields']['shipping_address_state']['audited']=true;
$dictionary['Account']['fields']['shipping_address_state']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_state']['comments']='The state used for the shipping address';
$dictionary['Account']['fields']['shipping_address_state']['importable']='false';
$dictionary['Account']['fields']['shipping_address_state']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['shipping_address_state']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['shipping_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_state']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_name.php

 // created: 2020-07-07 08:46:06
$dictionary['Account']['fields']['name']['len']='150';
$dictionary['Account']['fields']['name']['massupdate']=false;
$dictionary['Account']['fields']['name']['comments']='Name of the Company';
$dictionary['Account']['fields']['name']['importable']='false';
$dictionary['Account']['fields']['name']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['name']['merge_filter']='disabled';
$dictionary['Account']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['Account']['fields']['name']['calculated']='1';
$dictionary['Account']['fields']['name']['formula']='concat($sasa_nombres_c," ",$sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['Account']['fields']['name']['enforced']=true;
$dictionary['Account']['fields']['name']['audited']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_phone_alternate.php

 // created: 2020-07-31 13:28:16
$dictionary['Account']['fields']['phone_alternate']['len']='100';
$dictionary['Account']['fields']['phone_alternate']['audited']=false;
$dictionary['Account']['fields']['phone_alternate']['massupdate']=false;
$dictionary['Account']['fields']['phone_alternate']['comments']='';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['phone_alternate']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_alternate']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.03',
  'searchable' => false,
);
$dictionary['Account']['fields']['phone_alternate']['calculated']='1';
$dictionary['Account']['fields']['phone_alternate']['importable']='false';
$dictionary['Account']['fields']['phone_alternate']['formula']='concat(related($sasa_municipios_accounts_4,"sasa_indicativo_c"),$sasa_phone_alternate_c)';
$dictionary['Account']['fields']['phone_alternate']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_getemails_c.php

 // created: 2020-05-20 15:06:03
$dictionary['Account']['fields']['sasa_getemails_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_getemails_c']['labelValue']='sasa_getemails_c';
$dictionary['Account']['fields']['sasa_getemails_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_getemails_c']['calculated']='true';
$dictionary['Account']['fields']['sasa_getemails_c']['formula']='getEmails("olaKace")';
$dictionary['Account']['fields']['sasa_getemails_c']['enforced']='true';
$dictionary['Account']['fields']['sasa_getemails_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_city.php

 // created: 2020-07-31 10:53:24
$dictionary['Account']['fields']['billing_address_city']['audited']=true;
$dictionary['Account']['fields']['billing_address_city']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_city']['comments']='The city used for billing address';
$dictionary['Account']['fields']['billing_address_city']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['billing_address_city']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['billing_address_city']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_city']['calculated']=false;
$dictionary['Account']['fields']['billing_address_city']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_state.php

 // created: 2020-07-31 10:53:43
$dictionary['Account']['fields']['billing_address_state']['audited']=true;
$dictionary['Account']['fields']['billing_address_state']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_state']['comments']='The state used for billing address';
$dictionary['Account']['fields']['billing_address_state']['importable']='false';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['billing_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_state']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_industry.php

 // created: 2021-03-12 08:39:43
$dictionary['Account']['fields']['industry']['len']=100;
$dictionary['Account']['fields']['industry']['audited']=false;
$dictionary['Account']['fields']['industry']['massupdate']=true;
$dictionary['Account']['fields']['industry']['options']='industry_list';
$dictionary['Account']['fields']['industry']['comments']='The company belongs in this industry';
$dictionary['Account']['fields']['industry']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['industry']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['industry']['merge_filter']='disabled';
$dictionary['Account']['fields']['industry']['calculated']=false;
$dictionary['Account']['fields']['industry']['dependency']=false;
$dictionary['Account']['fields']['industry']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_email.php

 // created: 2020-05-09 13:36:53
$dictionary['Account']['fields']['email']['len']='100';
$dictionary['Account']['fields']['email']['massupdate']=true;
$dictionary['Account']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['email']['merge_filter']='disabled';
$dictionary['Account']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.89',
  'searchable' => true,
);
$dictionary['Account']['fields']['email']['calculated']=false;
$dictionary['Account']['fields']['email']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_postalcode.php

 // created: 2020-07-31 10:54:10
$dictionary['Account']['fields']['billing_address_postalcode']['audited']=true;
$dictionary['Account']['fields']['billing_address_postalcode']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_postalcode']['comments']='The postal code used for billing address';
$dictionary['Account']['fields']['billing_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_postalcode']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_postalcode']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_service_level.php

 // created: 2020-07-15 16:41:06
$dictionary['Account']['fields']['service_level']['audited']=false;
$dictionary['Account']['fields']['service_level']['massupdate']=true;
$dictionary['Account']['fields']['service_level']['comments']='An indication of the service level of a company';
$dictionary['Account']['fields']['service_level']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['service_level']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['service_level']['merge_filter']='disabled';
$dictionary['Account']['fields']['service_level']['calculated']=false;
$dictionary['Account']['fields']['service_level']['dependency']=false;
$dictionary['Account']['fields']['service_level']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_twitter.php

 // created: 2020-05-09 09:08:36
$dictionary['Account']['fields']['twitter']['audited']=false;
$dictionary['Account']['fields']['twitter']['massupdate']=false;
$dictionary['Account']['fields']['twitter']['comments']='The twitter name of the company';
$dictionary['Account']['fields']['twitter']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['twitter']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['twitter']['merge_filter']='disabled';
$dictionary['Account']['fields']['twitter']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['twitter']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_postalcode.php

 // created: 2020-07-31 10:55:28
$dictionary['Account']['fields']['shipping_address_postalcode']['len']='20';
$dictionary['Account']['fields']['shipping_address_postalcode']['audited']=true;
$dictionary['Account']['fields']['shipping_address_postalcode']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_postalcode']['comments']='The zip code used for the shipping address';
$dictionary['Account']['fields']['shipping_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_postalcode']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_postalcode']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_city.php

 // created: 2020-07-31 10:55:08
$dictionary['Account']['fields']['shipping_address_city']['len']='100';
$dictionary['Account']['fields']['shipping_address_city']['audited']=true;
$dictionary['Account']['fields']['shipping_address_city']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_city']['comments']='The city used for the shipping address';
$dictionary['Account']['fields']['shipping_address_city']['importable']='false';
$dictionary['Account']['fields']['shipping_address_city']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['shipping_address_city']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['shipping_address_city']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_city']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_phone_office.php

 // created: 2020-07-31 13:28:03
$dictionary['Account']['fields']['phone_office']['len']='100';
$dictionary['Account']['fields']['phone_office']['massupdate']=false;
$dictionary['Account']['fields']['phone_office']['comments']='';
$dictionary['Account']['fields']['phone_office']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['phone_office']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_office']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.05',
  'searchable' => false,
);
$dictionary['Account']['fields']['phone_office']['calculated']='1';
$dictionary['Account']['fields']['phone_office']['importable']='false';
$dictionary['Account']['fields']['phone_office']['formula']='concat(related($sasa_municipios_accounts_3,"sasa_indicativo_c"),$sasa_phone_office_c)';
$dictionary['Account']['fields']['phone_office']['enforced']=true;
$dictionary['Account']['fields']['phone_office']['reportable']=true;
$dictionary['Account']['fields']['phone_office']['audited']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_country.php

 // created: 2020-07-31 10:55:48
$dictionary['Account']['fields']['shipping_address_country']['len']='255';
$dictionary['Account']['fields']['shipping_address_country']['audited']=true;
$dictionary['Account']['fields']['shipping_address_country']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_country']['comments']='The country used for the shipping address';
$dictionary['Account']['fields']['shipping_address_country']['importable']='false';
$dictionary['Account']['fields']['shipping_address_country']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['shipping_address_country']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['shipping_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_country']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_street.php

 // created: 2021-02-04 11:58:06
$dictionary['Account']['fields']['billing_address_street']['audited']=true;
$dictionary['Account']['fields']['billing_address_street']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_street']['comments']='The street address used for billing address';
$dictionary['Account']['fields']['billing_address_street']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_street']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.35',
  'searchable' => true,
);
$dictionary['Account']['fields']['billing_address_street']['calculated']=false;
$dictionary['Account']['fields']['billing_address_street']['rows']='4';
$dictionary['Account']['fields']['billing_address_street']['cols']='20';
$dictionary['Account']['fields']['billing_address_street']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_facebook.php

 // created: 2020-05-20 15:17:11
$dictionary['Account']['fields']['facebook']['audited']=false;
$dictionary['Account']['fields']['facebook']['massupdate']=false;
$dictionary['Account']['fields']['facebook']['comments']='The facebook name of the company';
$dictionary['Account']['fields']['facebook']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['facebook']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['facebook']['merge_filter']='disabled';
$dictionary['Account']['fields']['facebook']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['facebook']['calculated']=0;
$dictionary['Account']['fields']['facebook']['importable']=0;
$dictionary['Account']['fields']['facebook']['formula']=0;
 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_street.php

 // created: 2020-07-31 10:54:58
$dictionary['Account']['fields']['shipping_address_street']['audited']=true;
$dictionary['Account']['fields']['shipping_address_street']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_street']['comments']='The street address used for for shipping purposes';
$dictionary['Account']['fields']['shipping_address_street']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_street']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_street']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '0.34',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_street']['calculated']=false;
$dictionary['Account']['fields']['shipping_address_street']['rows']='4';
$dictionary['Account']['fields']['shipping_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipo_documento_c-visibility_grid.php

$dictionary['Account']['fields']['sasa_tipo_documento_c']['visibility_grid']=array (
	'trigger' => 'sasa_tipo_persona_c',
	'values' => 
	array (
		'' => 
		array (
		),
		'N' => 
		array (
			0 => '',
			1 => 'C',
			2 => 'E',
			3 => 'T',
			4 => 'P',
			5 => 'D',
			6 => 'U',
		),
		'J' => 
		array (
			0 => 'N',
			1 => 'D',
			2 => 'P',
			3 => 'Prueba',
		),
	),
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_cantidad_empleados_c.php

 // created: 2021-06-15 16:31:54
$dictionary['Account']['fields']['sasa_cantidad_empleados_c']['labelValue']='Cantidad de Empleados';
$dictionary['Account']['fields']['sasa_cantidad_empleados_c']['dependency']='equal($sasa_tipo_persona_c,"J")';
$dictionary['Account']['fields']['sasa_cantidad_empleados_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_categoria_c.php

 // created: 2021-06-15 16:31:54
$dictionary['Account']['fields']['sasa_categoria_c']['labelValue']='Categoría';
$dictionary['Account']['fields']['sasa_categoria_c']['dependency']='';
$dictionary['Account']['fields']['sasa_categoria_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_celular_alternativo_c.php

 // created: 2021-06-15 16:31:55
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['labelValue']='Cel 2';
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['calculated']='1';
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['formula']='concat("+57",$sasa_cel_alternativo_c)';
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['enforced']='1';
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_celular_otro_c.php

 // created: 2021-06-15 16:31:55
$dictionary['Account']['fields']['sasa_celular_otro_c']['labelValue']='Celular 3';
$dictionary['Account']['fields']['sasa_celular_otro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_celular_otro_c']['enforced']='';
$dictionary['Account']['fields']['sasa_celular_otro_c']['dependency']='not(equal($sasa_cel_alternativo_c,""))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_celular_principal_c.php

 // created: 2021-06-15 16:31:55
$dictionary['Account']['fields']['sasa_celular_principal_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_celular_principal_c']['labelValue']='Cel 1';
$dictionary['Account']['fields']['sasa_celular_principal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_celular_principal_c']['calculated']='1';
$dictionary['Account']['fields']['sasa_celular_principal_c']['formula']='concat("+57",$sasa_cel_principal_c)';
$dictionary['Account']['fields']['sasa_celular_principal_c']['enforced']='1';
$dictionary['Account']['fields']['sasa_celular_principal_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_cel_alternativo_c.php

 // created: 2021-06-15 16:31:56
$dictionary['Account']['fields']['sasa_cel_alternativo_c']['labelValue']='Celular 2';
$dictionary['Account']['fields']['sasa_cel_alternativo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_cel_alternativo_c']['enforced']='';
$dictionary['Account']['fields']['sasa_cel_alternativo_c']['dependency']='not(equal($sasa_cel_principal_c,""))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_cel_otro_c.php

 // created: 2021-06-15 16:31:56
$dictionary['Account']['fields']['sasa_cel_otro_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_cel_otro_c']['labelValue']='Cel 3';
$dictionary['Account']['fields']['sasa_cel_otro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_cel_otro_c']['calculated']='true';
$dictionary['Account']['fields']['sasa_cel_otro_c']['formula']='concat("+57",$sasa_celular_otro_c)';
$dictionary['Account']['fields']['sasa_cel_otro_c']['enforced']='true';
$dictionary['Account']['fields']['sasa_cel_otro_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_ciudad3_c.php

 // created: 2021-06-15 16:31:57
$dictionary['Account']['fields']['sasa_ciudad3_c']['labelValue']='Ciudad 3 ';
$dictionary['Account']['fields']['sasa_ciudad3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_ciudad3_c']['enforced']='';
$dictionary['Account']['fields']['sasa_ciudad3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_cel_principal_c.php

 // created: 2021-06-15 16:31:57
$dictionary['Account']['fields']['sasa_cel_principal_c']['labelValue']='Celular 1';
$dictionary['Account']['fields']['sasa_cel_principal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_cel_principal_c']['enforced']='';
$dictionary['Account']['fields']['sasa_cel_principal_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_ciudad_tel_3_c.php

 // created: 2021-06-15 16:31:57

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_ciudad_tel_alternativ_2_c.php

 // created: 2021-06-15 16:31:57

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_ciudad_tel_principal_2_c.php

 // created: 2021-06-15 16:31:58

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_codigopostal3_c.php

 // created: 2021-06-15 16:31:58
$dictionary['Account']['fields']['sasa_codigopostal3_c']['labelValue']='Código Postal 3';
$dictionary['Account']['fields']['sasa_codigopostal3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_codigopostal3_c']['enforced']='';
$dictionary['Account']['fields']['sasa_codigopostal3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_compania_c.php

 // created: 2021-06-15 16:32:00
$dictionary['Account']['fields']['sasa_compania_c']['labelValue']='Compañía';
$dictionary['Account']['fields']['sasa_compania_c']['dependency']='equal($sasa_empleadodelgrupo_c,"true")';
$dictionary['Account']['fields']['sasa_compania_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_direccion3_c.php

 // created: 2021-06-15 16:32:01
$dictionary['Account']['fields']['sasa_direccion3_c']['labelValue']='Dirección 3';
$dictionary['Account']['fields']['sasa_direccion3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_direccion3_c']['enforced']='';
$dictionary['Account']['fields']['sasa_direccion3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_departamento3_c.php

 // created: 2021-06-15 16:32:01
$dictionary['Account']['fields']['sasa_departamento3_c']['labelValue']='Departamento 3';
$dictionary['Account']['fields']['sasa_departamento3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_departamento3_c']['enforced']='';
$dictionary['Account']['fields']['sasa_departamento3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_empleadodelgrupo_c.php

 // created: 2021-06-15 16:32:02
$dictionary['Account']['fields']['sasa_empleadodelgrupo_c']['labelValue']='¿Empleado del Grupo?';
$dictionary['Account']['fields']['sasa_empleadodelgrupo_c']['enforced']='';
$dictionary['Account']['fields']['sasa_empleadodelgrupo_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_extension2_c.php

 // created: 2021-06-15 16:32:03
$dictionary['Account']['fields']['sasa_extension2_c']['labelValue']='Extensión Teléfono 2';
$dictionary['Account']['fields']['sasa_extension2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_extension2_c']['enforced']='';
$dictionary['Account']['fields']['sasa_extension2_c']['dependency']='equal($sasa_tipotel2_c,"OF")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_extension3_c.php

 // created: 2021-06-15 16:32:04
$dictionary['Account']['fields']['sasa_extension3_c']['labelValue']='Extensión Teléfono 3';
$dictionary['Account']['fields']['sasa_extension3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_extension3_c']['enforced']='';
$dictionary['Account']['fields']['sasa_extension3_c']['dependency']='equal($sasa_tipotel3_c,"OF")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_extension_c.php

 // created: 2021-06-15 16:32:06
$dictionary['Account']['fields']['sasa_extension_c']['labelValue']='Extensión Teléfono 1';
$dictionary['Account']['fields']['sasa_extension_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_extension_c']['enforced']='';
$dictionary['Account']['fields']['sasa_extension_c']['dependency']='equal($sasa_tipotel1_c,"OF")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2021-06-15 16:32:08
$dictionary['Account']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['Account']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['Account']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_ingreso_mensual_c.php

 // created: 2021-06-15 16:32:09
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['options']='numeric_range_search_dom';
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['labelValue']='Ingreso Mensual (COP)';
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['enforced']='';
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['dependency']='';
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_last_name_2_c.php

 // created: 2021-06-15 16:32:09
$dictionary['Account']['fields']['sasa_last_name_2_c']['labelValue']='Segundo Apellido';
$dictionary['Account']['fields']['sasa_last_name_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_last_name_2_c']['enforced']='';
$dictionary['Account']['fields']['sasa_last_name_2_c']['dependency']='equal($sasa_tipo_persona_c,"N")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_municipio_3_c.php

 // created: 2021-06-15 16:32:10

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_municipio_principal_2_c.php

 // created: 2021-06-15 16:32:10
$dictionary['Account']['fields']['sasa_municipio_principal_2_c']['labelValue']='Ciudad Dirección 1 (Integración)';
$dictionary['Account']['fields']['sasa_municipio_principal_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_municipio_principal_2_c']['enforced']='';
$dictionary['Account']['fields']['sasa_municipio_principal_2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_municipio_secundario_2_c.php

 // created: 2021-06-15 16:32:10

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_numero_documento_c.php

 // created: 2021-06-15 16:32:11
$dictionary['Account']['fields']['sasa_numero_documento_c']['labelValue']='Número Documento';
$dictionary['Account']['fields']['sasa_numero_documento_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Account']['fields']['sasa_numero_documento_c']['enforced']='';
$dictionary['Account']['fields']['sasa_numero_documento_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_nombres_c.php

 // created: 2021-06-15 16:32:11
$dictionary['Account']['fields']['sasa_nombres_c']['labelValue']='Nombres';
$dictionary['Account']['fields']['sasa_nombres_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_nombres_c']['enforced']='';
$dictionary['Account']['fields']['sasa_nombres_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_pais3_c.php

 // created: 2021-06-15 16:32:12
$dictionary['Account']['fields']['sasa_pais3_c']['labelValue']='País 3';
$dictionary['Account']['fields']['sasa_pais3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_pais3_c']['enforced']='';
$dictionary['Account']['fields']['sasa_pais3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_phone_alternate_c.php

 // created: 2021-06-15 16:32:12
$dictionary['Account']['fields']['sasa_phone_alternate_c']['labelValue']='Teléfono 2';
$dictionary['Account']['fields']['sasa_phone_alternate_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Account']['fields']['sasa_phone_alternate_c']['enforced']='';
$dictionary['Account']['fields']['sasa_phone_alternate_c']['dependency']='not(equal($sasa_phone_office_c,""))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_phone_office_c.php

 // created: 2021-06-15 16:32:12
$dictionary['Account']['fields']['sasa_phone_office_c']['labelValue']='Teléfono 1';
$dictionary['Account']['fields']['sasa_phone_office_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Account']['fields']['sasa_phone_office_c']['enforced']='';
$dictionary['Account']['fields']['sasa_phone_office_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_phone_other_c.php

 // created: 2021-06-15 16:32:13
$dictionary['Account']['fields']['sasa_phone_other_c']['labelValue']='Teléfono 3';
$dictionary['Account']['fields']['sasa_phone_other_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_phone_other_c']['enforced']='';
$dictionary['Account']['fields']['sasa_phone_other_c']['dependency']='not(equal($sasa_phone_alternate_c,""))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_primerapellido_c.php

 // created: 2021-06-15 16:32:13
$dictionary['Account']['fields']['sasa_primerapellido_c']['labelValue']='Primer Apellido';
$dictionary['Account']['fields']['sasa_primerapellido_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_primerapellido_c']['enforced']='';
$dictionary['Account']['fields']['sasa_primerapellido_c']['dependency']='equal($sasa_tipo_persona_c,"N")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_regimen_fiscal_c.php

 // created: 2021-06-15 16:32:14
$dictionary['Account']['fields']['sasa_regimen_fiscal_c']['labelValue']='Régimen Fiscal';
$dictionary['Account']['fields']['sasa_regimen_fiscal_c']['dependency']='';
$dictionary['Account']['fields']['sasa_regimen_fiscal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_regimen_tributario_c.php

 // created: 2021-06-15 16:32:15
$dictionary['Account']['fields']['sasa_regimen_tributario_c']['labelValue']='Régimen Tributario';
$dictionary['Account']['fields']['sasa_regimen_tributario_c']['dependency']='equal($sasa_tipo_persona_c,"J")';
$dictionary['Account']['fields']['sasa_regimen_tributario_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_sector_c.php

 // created: 2021-06-15 16:32:15
$dictionary['Account']['fields']['sasa_sector_c']['labelValue']='Sector';
$dictionary['Account']['fields']['sasa_sector_c']['dependency']='';
$dictionary['Account']['fields']['sasa_sector_c']['visibility_grid']=array (
  'trigger' => 'industry',
  'values' => 
  array (
    1 => 
    array (
      0 => '0_0',
      1 => '1_1',
      2 => '1_2',
      3 => '1_10',
      4 => '1_11',
      5 => '3_3',
      6 => '1_13',
      7 => '1_14',
      8 => '1_15',
      9 => '1_3',
    ),
    2 => 
    array (
      0 => '0_0',
      1 => '2_1',
      2 => '2_2',
      3 => '2_3',
      4 => '2_4',
      5 => '2_5',
      6 => '2_6',
      7 => '2_7',
    ),
    3 => 
    array (
      0 => '0_0',
      1 => '3_1',
      2 => '3_2',
      3 => '1_12',
      4 => '3_4',
      5 => '3_5',
      6 => '3_6',
      7 => '3_7',
      8 => '3_8',
      9 => '3_9',
    ),
    4 => 
    array (
      0 => '0_0',
      1 => '4_18',
      2 => '4_19',
      3 => '4_20',
      4 => '4_21',
      5 => '4_1',
      6 => '4_2',
      7 => '4_3',
      8 => '4_4',
      9 => '4_5',
      10 => '4_6',
      11 => '4_7',
      12 => '4_8',
      13 => '4_9',
      14 => '4_10',
      15 => '4_11',
      16 => '4_12',
      17 => '4_13',
      18 => '4_14',
      19 => '4_15',
      20 => '4_16',
      21 => '4_17',
    ),
    5 => 
    array (
      0 => '0_0',
      1 => '5_1',
      2 => '5_2',
      3 => '5_3',
      4 => '5_4',
      5 => '5_5',
      6 => '5_6',
      7 => '5_7',
      8 => '5_8',
      9 => '5_9',
      10 => '5_10',
      11 => '5_11',
      12 => '5_12',
    ),
    6 => 
    array (
      0 => '0_0',
      1 => '6_1',
      2 => '6_2',
      3 => '6_3',
      4 => '6_4',
    ),
    7 => 
    array (
      0 => '0_0',
      1 => '7_1',
      2 => '7_2',
      3 => '7_3',
      4 => '7_4',
      5 => '7_5',
    ),
    8 => 
    array (
      0 => '0_0',
      1 => '8_1',
      2 => '8_2',
      3 => '8_3',
      4 => '8_4',
      5 => '8_5',
      6 => '8_6',
      7 => '8_7',
      8 => '8_8',
      9 => '8_9',
      10 => '8_10',
    ),
    9 => 
    array (
      0 => '0_0',
      1 => '9_1',
      2 => '9_2',
      3 => '9_3',
      4 => '9_4',
      5 => '9_5',
    ),
    10 => 
    array (
      0 => '0_0',
      1 => '10_1',
    ),
    11 => 
    array (
      0 => '0_0',
      1 => '11_1',
      2 => '11_2',
      3 => '11_3',
    ),
    12 => 
    array (
      0 => '0_0',
      1 => '12_1',
      2 => '12_2',
    ),
    13 => 
    array (
      0 => '0_0',
      1 => '13_1',
      2 => '13_2',
      3 => '13_3',
      4 => '13_4',
    ),
    14 => 
    array (
      0 => '0_0',
      1 => '14_1',
      2 => '14_2',
      3 => '14_3',
      4 => '14_4',
      5 => '14_5',
      6 => '15_2',
      7 => '14_7',
    ),
    15 => 
    array (
      0 => '0_0',
      1 => '15_1',
      2 => '28_3',
      3 => '15_3',
      4 => '15_4',
    ),
    16 => 
    array (
      0 => '0_0',
      1 => '16_1',
      2 => '16_2',
      3 => '16_3',
      4 => '16_4',
      5 => '16_5',
      6 => '16_6',
      7 => '16_7',
      8 => '16_8',
      9 => '16_9',
      10 => '16_10',
    ),
    17 => 
    array (
      0 => '0_0',
      1 => '17_1',
      2 => '17_2',
      3 => '17_3',
      4 => '17_4',
      5 => '17_5',
      6 => '17_6',
      7 => '17_7',
    ),
    18 => 
    array (
      0 => '0_0',
      1 => '18_1',
      2 => '18_2',
      3 => '18_3',
      4 => '18_4',
      5 => '18_5',
    ),
    19 => 
    array (
      0 => '0_0',
      1 => '19_1',
      2 => '19_2',
      3 => '19_3',
      4 => '19_4',
    ),
    20 => 
    array (
      0 => '0_0',
      1 => '20_1',
      2 => '20_2',
      3 => '20_3',
    ),
    21 => 
    array (
      0 => '0_0',
      1 => '21_1',
      2 => '21_2',
      3 => '21_3',
      4 => '21_4',
    ),
    22 => 
    array (
      0 => '0_0',
      1 => '22_1',
      2 => '22_2',
      3 => '22_3',
      4 => '22_4',
      5 => '14_6',
      6 => '22_6',
    ),
    23 => 
    array (
      0 => '0_0',
      1 => '23_1',
      2 => '23_2',
    ),
    24 => 
    array (
      0 => '0_0',
      1 => '24_1',
      2 => '24_2',
      3 => '24_3',
      4 => '24_4',
      5 => '24_5',
      6 => '24_6',
      7 => '24_7',
      8 => '24_8',
    ),
    25 => 
    array (
      0 => '0_0',
      1 => '25_1',
      2 => '25_2',
      3 => '25_3',
      4 => '25_4',
      5 => '25_5',
      6 => '25_6',
    ),
    26 => 
    array (
      0 => '0_0',
      1 => '26_1',
      2 => '26_4',
      3 => '26_5',
      4 => '26_6',
      5 => '26_7',
      6 => '26_8',
      7 => '26_9',
      8 => '26_10',
      9 => '26_11',
      10 => '26_12',
      11 => '26_13',
      12 => '26_14',
    ),
    27 => 
    array (
      0 => '0_0',
      1 => '27_1',
      2 => '27_2',
      3 => '27_3',
    ),
    28 => 
    array (
      0 => '0_0',
      1 => '28_1',
      2 => '28_2',
      3 => '22_5',
      4 => '28_4',
    ),
    29 => 
    array (
      0 => '0_0',
      1 => '29_1',
      2 => '29_2',
      3 => '29_3',
      4 => '29_4',
      5 => '29_5',
    ),
    30 => 
    array (
      0 => '0_0',
      1 => '30_1',
      2 => '30_2',
      3 => '30_3',
      4 => '30_4',
      5 => '30_5',
    ),
    '' => 
    array (
      0 => '0_0',
    ),
    '0_' => 
    array (
      0 => '0_0',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tel_phone_other_c.php

 // created: 2021-06-15 16:32:16
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['labelValue']='Tel 3';
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['calculated']='1';
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['formula']='concat(related($sasa_municipios_accounts_5,"sasa_indicativo_c"),$sasa_phone_other_c)';
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['enforced']='1';
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipodirec1_c.php

 // created: 2021-06-15 16:32:16
$dictionary['Account']['fields']['sasa_tipodirec1_c']['labelValue']='Tipo Dirección 1 ';
$dictionary['Account']['fields']['sasa_tipodirec1_c']['dependency']='';
$dictionary['Account']['fields']['sasa_tipodirec1_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipodirec2_c.php

 // created: 2021-06-15 16:32:17
$dictionary['Account']['fields']['sasa_tipodirec2_c']['labelValue']='Tipo Dirección 2';
$dictionary['Account']['fields']['sasa_tipodirec2_c']['dependency']='';
$dictionary['Account']['fields']['sasa_tipodirec2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipodirec3_c.php

 // created: 2021-06-15 16:32:18
$dictionary['Account']['fields']['sasa_tipodirec3_c']['labelValue']='Tipo Dirección 3 ';
$dictionary['Account']['fields']['sasa_tipodirec3_c']['dependency']='';
$dictionary['Account']['fields']['sasa_tipodirec3_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipotel1_c.php

 // created: 2021-06-15 16:32:19
$dictionary['Account']['fields']['sasa_tipotel1_c']['labelValue']='Tipo Teléfono 1';
$dictionary['Account']['fields']['sasa_tipotel1_c']['dependency']='';
$dictionary['Account']['fields']['sasa_tipotel1_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipotel2_c.php

 // created: 2021-06-15 16:32:19
$dictionary['Account']['fields']['sasa_tipotel2_c']['labelValue']='Tipo Teléfono 2 ';
$dictionary['Account']['fields']['sasa_tipotel2_c']['dependency']='not(equal($sasa_phone_office_c,""))';
$dictionary['Account']['fields']['sasa_tipotel2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipotel3_c.php

 // created: 2021-06-15 16:32:20
$dictionary['Account']['fields']['sasa_tipotel3_c']['labelValue']='Tipo Teléfono 3';
$dictionary['Account']['fields']['sasa_tipotel3_c']['dependency']='not(equal($sasa_phone_alternate_c,""))';
$dictionary['Account']['fields']['sasa_tipotel3_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipo_documento_c.php

 // created: 2021-06-15 16:32:21
$dictionary['Account']['fields']['sasa_tipo_documento_c']['labelValue']='Tipo Documento';
$dictionary['Account']['fields']['sasa_tipo_documento_c']['dependency']='';
$dictionary['Account']['fields']['sasa_tipo_documento_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipo_persona_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'N' => 
    array (
      0 => '',
      1 => 'C',
      2 => 'E',
      3 => 'T',
      4 => 'P',
      5 => 'D',
      6 => 'U',
    ),
    'J' => 
    array (
      0 => 'N',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipo_persona_c.php

 // created: 2021-06-15 16:32:23
$dictionary['Account']['fields']['sasa_tipo_persona_c']['labelValue']='Tipo Persona';
$dictionary['Account']['fields']['sasa_tipo_persona_c']['dependency']='';
$dictionary['Account']['fields']['sasa_tipo_persona_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_tipo_sigru_c.php

 // created: 2021-06-15 16:32:23
$dictionary['Account']['fields']['sasa_tipo_sigru_c']['labelValue']='Tipo Sigru';
$dictionary['Account']['fields']['sasa_tipo_sigru_c']['dependency']='';
$dictionary['Account']['fields']['sasa_tipo_sigru_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_ciudad_tel_oficina_2_c.php

 // created: 2021-06-15 16:32:24

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sasa_agendar_cita_c.php

 // created: 2021-06-21 16:30:12

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/customer_journey_parent.php

// created: 2023-02-08 11:09:23
VardefManager::createVardef('Accounts', 'Account', [
                                'customer_journey_parent',
                        ]);
?>

<?php
// WARNING: The contents of this file are auto-generated.


// created: 2018-12-04 16:24:59
/*
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_contacts_1',
  ),
);*/


// created: 2020-05-21 16:21:34
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_unidadnegclienteprospect_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'accounts_sasa_unidadnegclienteprospect_1',
  'view' => 'subpanel-for-accounts-accounts_sasa_unidadnegclienteprospect_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'contacts',
  'view' => 'subpanel-for-accounts-contacts',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'opportunities',
  'view' => 'subpanel-for-accounts-opportunities',
);

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_paises_accounts_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_paises_accounts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_1_FROM_SASA_PAISES_TITLE'] = 'Países';
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Países';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_1_FROM_SASA_PAISES_TITLE'] = 'Países';
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Países';


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0012020051605.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0012020051605.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0012020051605.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0022020051907.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel principal 3 (Integración)';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0022020052101.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel principal 3 (Integración)';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0022020052711.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad alternativo 2(Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad principal 2 (Integración)';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0032020060210.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad alternativo 2(Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad principal 2 (Integración)';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono Principal';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0032020060305.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad alternativo 2(Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad principal 2 (Integración)';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono Principal';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0032020063009.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad alternativo 2(Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad principal 2 (Integración)';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono Principal';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0032020070112.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad alternativo 2(Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad principal 2 (Integración)';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono Principal';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0042020070111.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Cliente y Prospecto Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Clientes y Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Save to continue creating this new Cliente y Prospecto, or click Cancel to return to the module without creating the Cliente y Prospecto.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Cliente y Prospecto List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cliente y Prospecto Search';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Clientes y Prospectos';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Cliente y Prospecto from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Cliente y Prospecto.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Cliente y Prospecto';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Home';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Cliente y Prospecto ID';
$mod_strings['MSG_DUPLICATE'] = 'The Cliente y Prospecto record you are about to create might be a duplicate of an Cliente y Prospecto record that already exists. Cliente y Prospecto records containing similar names are listed below.Click Create Cliente y Prospecto to continue creating this new Cliente y Prospecto, or select an existing Cliente y Prospecto listed below.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Cliente y Prospecto Type';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Clientes y Prospectos List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Clientes y Prospectos Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Cliente y Prospecto Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Cliente y Prospecto Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Clientes y Prospectos&#039; reports';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad alternativo 2(Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad principal 2 (Integración)';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono Principal';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0052020080302.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contact';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Meetings';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tasks';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campaigns';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad alternativo 2(Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad principal 2 (Integración)';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_EMPLADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3 ';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3 ';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0052020080609.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contact';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Meetings';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tasks';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campaigns';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad Dirección 2 (Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad Dirección 1 (Integración)';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_EMPLADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3 ';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3 ';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0052020081004.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contact';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Meetings';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tasks';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campaigns';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad Dirección 2 (Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad Dirección 1 (Integración)';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_EMPLADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3 ';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3 ';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0052020081804.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contact';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Meetings';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tasks';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campaigns';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad Dirección 2 (Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad Dirección 1 (Integración)';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_EMPLADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3 ';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3 ';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contactos';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tareas';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaña';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campañas';
$mod_strings['LBL_CAMPAIGNS'] = 'Campañas';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contacts';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3 ';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3 ';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0062020093004.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0062020093004.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contactos';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tareas';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaña';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campañas';
$mod_strings['LBL_CAMPAIGNS'] = 'Campañas';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contactos';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tareas';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaña';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campañas';
$mod_strings['LBL_CAMPAIGNS'] = 'Campañas';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0072020120703.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contactos';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tareas';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaña';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campañas';
$mod_strings['LBL_CAMPAIGNS'] = 'Campañas';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad de tel 3 (integración)';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0082021061504.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0082021061504.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0082021061504.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.gvaccountscustomfieldsv0082021061504.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contactos';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tareas';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaña';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campañas';
$mod_strings['LBL_CAMPAIGNS'] = 'Campañas';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad de tel 3 (integración)';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contactos';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tareas';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaña';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campañas';
$mod_strings['LBL_CAMPAIGNS'] = 'Campañas';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad de tel 3 (integración)';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contactos';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tareas';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaña';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campañas';
$mod_strings['LBL_CAMPAIGNS'] = 'Campañas';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad de tel 3 (integración)';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente y Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente y Prospecto';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Ver Clientes y Prospectos';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Ver Informes de Cliente y Prospectos';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes y Prospectos';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente y Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente y Prospectos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente y Prospectos';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes y Prospectos: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes y Prospectos';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID Cliente y Prospecto Principal';
$mod_strings['MSG_DUPLICATE'] = 'El registro de cuenta que va a crear podría ser un duplicado de otro registro de cuenta existente. Los registros de cuenta con nombres similares se enumeran a continuación.Haga clic en Crear Cliente y Prospecto para continuar con la creación de esta cuenta nueva o seleccione una cuenta existente a continuación.';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Informes de Clientes y Prospectos';
$mod_strings['LBL_INVITEE'] = 'Contactos';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tareas';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaña';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Registro de Campañas';
$mod_strings['LBL_CAMPAIGNS'] = 'Campañas';
$mod_strings['LBL_PROSPECT_LIST'] = 'Lista de Target';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_TYPE'] = 'Tipo de Cuenta';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Direccion 2';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_TIPO_SIGRU_C'] = 'Tipo Sigru';
$mod_strings['LBL_SASA_INGRESO_MENSUAL_C'] = 'Ingreso Mensual (COP)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_CANTIDAD_EMPLEADOS_C'] = 'Cantidad de Empleados';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número Documento';
$mod_strings['LBL_SASA_REGIMEN_FISCAL_C'] = 'Régimen Fiscal';
$mod_strings['LBL_SASA_ORIGEN_C'] = 'Origen';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle Origen';
$mod_strings['LBL_SASA_REGIMEN_TRIBUTARIO_C'] = 'Régimen Tributario';
$mod_strings['LBL_SASA_CATEGORIA_C'] = 'Categoría';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_TIPO_PERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OFFICE_C'] = 'Teléfono 1';
$mod_strings['LBL_SASA_PHONE_ALTERNATE_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_CEL_PRINCIPAL_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_'] = 'Ciudad tel principal 2 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad tel principal 3 (Integración)';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_EMPLEADODELGRUPO_C'] = '¿Empleado del Grupo?';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Presupuestos';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_DATAPRIVACY_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_DATAPRIVACY_FROM_ACCOUNTS_TITLE'] = 'DataPrivacy';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Contactos';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_MODIFIED_USER_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_CREATED_BY_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNT_ACTIVITIES_FROM_ACCOUNTS_TITLE'] = 'Flujo de actividad';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FOLLOWING_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_USERS_TITLE'] = 'Users';
$mod_strings['LBL_ACCOUNTS_FAVORITE_FROM_ACCOUNTS_TITLE'] = 'Users';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirrección 2';
$mod_strings['LBL_SASA_ASESOR_C'] = 'Asesor';
$mod_strings['LBL_SASA_VITRINA_C'] = 'Vitrina';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TEL_PHONE_OTHER_C'] = 'Tel 3';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2'] = 'Ciudad Teléfono 2  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2'] = 'Ciudad Teléfono 1  (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_3_C'] = 'Ciudad Teléfono 3  (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad de tel 3 (integración)';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_municipios_accounts_4.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_municipios_accounts_5.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_municipios_accounts_6.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_departamentos_accounts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Departamentos';
$mod_strings['LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Departamentos';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_municipios_accounts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_sasa_unidadnegclienteprospect_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE'] = 'U Negocios por Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_ACCOUNTS_TITLE'] = 'U Negocios por Clientes y Prospectos';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_paises_accounts_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_2_FROM_SASA_PAISES_TITLE'] = 'Países';
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Países';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_departamentos_accounts_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Departamentos';
$mod_strings['LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Departamentos';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_municipios_accounts_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_sasa_unidad_de_negocio_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE'] = 'Unidad de Negocio';
$mod_strings['LBL_ACCOUNTS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_ACCOUNTS_TITLE'] = 'Unidad de Negocio';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customsasa_municipios_accounts_3.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudades';

?>

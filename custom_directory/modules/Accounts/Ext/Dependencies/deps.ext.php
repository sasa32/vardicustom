<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependencies1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependencies1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_office_c','sasa_municipios_accounts_3_name'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_3_name',
				'value' => '
					not(equal($sasa_phone_office_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_3_name',
				'value' => '
					not(equal($sasa_phone_office_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_accounts_3_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_3_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipDir1.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipDir1.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec1_c'),
	'trigger' => 'equal($sasa_tipodirec1_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'billing_address_street',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipDir2.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipDir2.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec2_c'),
	'trigger' => 'equal($sasa_tipodirec2_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'shipping_address_street',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_2sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipDir2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipDir2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('shipping_address_street'),
	'trigger' => 'equal($shipping_address_street,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipDir3.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipDir3.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec3_c'),
	'trigger' => 'equal($sasa_tipodirec3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_direccion3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipDir3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipDir3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_direccion3_c'),
	'trigger' => 'equal($sasa_direccion3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_6sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel1.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel1.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipotel1_c'),
	'trigger' => 'equal($sasa_tipotel1_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_office_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_3sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_office_c'),
	'trigger' => 'equal($sasa_phone_office_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_3sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipDir1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipDir1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('billing_address_street'),
	'trigger' => 'equal($billing_address_street,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec1_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel2.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel2.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipotel2_c'),
	'trigger' => 'equal($sasa_tipotel2_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_alternate_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_4sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel3.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel3.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipotel3_c'),
	'trigger' => 'equal($sasa_tipotel3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_5sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipDir3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_other_c'),
	'trigger' => 'equal($sasa_phone_other_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_5sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel1.3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel1.3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_municipios_accounts_3_name'),
	'trigger' => 'equal($sasa_municipios_accounts_3_name,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_office_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel2.3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel2.3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_municipios_accounts_4_name'),
	'trigger' => 'equal($sasa_municipios_accounts_4_name,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_alternate_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel3.3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel3.3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_municipios_accounts_5_name'),
	'trigger' => 'equal($sasa_municipios_accounts_5_name,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel1.4.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
/*$dependencies['Accounts']['dependenciessetvalueTipTel1.4'] = array(
	'hooks' => array("edit"),
	'triggerFields' => array('sasa_extension_c'),
	'trigger' => 'and(equal($sasa_extension_c,""),equal($sasa_tipotel1_c,"OF"))',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_office_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_3sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);*/

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel2.4.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
/*$dependencies['Accounts']['dependenciessetvalueTipTel2.4'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_extension2_c'),
	'trigger' => 'equal($sasa_extension2_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_alternate_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_4sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);*/

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_alternate_c'),
	'trigger' => 'equal($sasa_phone_alternate_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_4sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciesTipDirec3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciesTipDirec3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec3_c','sasa_direccion3_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_direccion3_c',
				'value' => '
					not(equal($sasa_tipodirec3_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => '
					not(equal($sasa_direccion3_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_direccion3_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciestiptel3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciestiptel3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_other_c','sasa_tipotel3_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '
					not(equal($sasa_tipotel3_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '
					not(equal($sasa_phone_other_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciesTipDirec2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciesTipDirec2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec2_c','shipping_address_street'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'shipping_address_street',
				'value' => '
					not(equal($sasa_tipodirec2_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => '
					not(equal($shipping_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'shipping_address_street',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependencies3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependencies3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_other_c','sasa_municipios_accounts_5_name'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_5_name',
				'value' => '
					not(equal($sasa_phone_other_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_5_name',
				'value' => '
					not(equal($sasa_phone_other_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_accounts_5_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_5_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependencies4.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependencies4'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('shipping_address_street'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_caja_direccion3_c',
				'value' => '
					or(not(equal($sasa_direccion3_c,0)),not(equal($shipping_address_street,0)))
				',
			),
		),
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => '
					not(equal($shipping_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_caja_direccion3_c',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => 'false'
			)
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/setVisibilitydireccion2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['setVisibilitydireccion2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('billing_address_street'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'shipping_address',
				'value' => '
					or(not(equal($billing_address_street,0)),not(equal($shipping_address_street,0)))
				',
			),
		),
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => '
					not(equal($billing_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'shipping_address',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => 'false'
			)
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalue1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalue1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_office_c','sasa_tipotel1_c','sasa_phone_alternate_c'),
	'trigger' => 'equal($sasa_phone_office_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_alternate_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalue2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalue2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_alternate_c','sasa_tipotel2_c','sasa_phone_other_c'),
	'trigger' => 'equal($sasa_phone_alternate_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciestiptel1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciestiptel1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_office_c','sasa_tipotel1_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_office_c',
				'value' => '
					not(equal($sasa_tipotel1_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => '
					not(equal($sasa_phone_office_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_office_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependencies2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependencies2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_alternate_c','sasa_municipios_accounts_4_name'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_4_name',
				'value' => '
					not(equal($sasa_phone_alternate_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_4_name',
				'value' => '
					not(equal($sasa_phone_alternate_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_accounts_4_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_4_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciestiptel2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciestiptel2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_alternate_c','sasa_tipotel2_c','sasa_phone_office_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_alternate_c',
				'value' => '
					not(equal($sasa_tipotel2_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '
					not(equal($sasa_phone_alternate_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_alternate_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/DependencyCiudadDire1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['DependencyCiudadDire1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('billing_address_street'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_1_name',
				'value' => '
					not(equal($billing_address_street,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_1_name',
				'value' => '
					not(equal($billing_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_accounts_1_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_1_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/DependencyCiudadDire2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['DependencyCiudadDire2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('shipping_address_street'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_2_name',
				'value' => '
					not(equal($shipping_address_street,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_2_name',
				'value' => '
					not(equal($shipping_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_accounts_2_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_2_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/DependencyCiudadDire3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['DependencyCiudadDire3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_direccion3_c'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_6_name',
				'value' => '
					not(equal($sasa_direccion3_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_6_name',
				'value' => '
					not(equal($sasa_direccion3_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_accounts_6_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_6_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueDireccion1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueDireccion1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('billing_address_street'),
	'trigger' => 'equal($billing_address_street,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'billing_address_city',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'billing_address_state',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'billing_address_postalcode',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'billing_address_country',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_1sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueDireccion2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueDireccion2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('shipping_address_street'),
	'trigger' => 'equal($shipping_address_street,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'shipping_address_city',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'shipping_address_state',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'shipping_address_postalcode',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'shipping_address_country',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_2sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueDireccion3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueDireccion3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_direccion3_c'),
	'trigger' => 'equal($sasa_direccion3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_ciudad3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_departamento3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_codigopostal3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_pais3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_6sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciesTipDirec1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciesTipDirec1.php'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec1_c','billing_address_street'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'billing_address_street',
				'value' => '
					not(equal($sasa_tipodirec1_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec1_c',
				'value' => '
					not(equal($billing_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'billing_address_street',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec1_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/dependenciessetvalueTipTel3.4.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
/*$dependencies['Accounts']['dependenciessetvalueTipTel3.4'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_extension3_c'),
	'trigger' => 'equal($sasa_extension3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_5sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);*/

?>

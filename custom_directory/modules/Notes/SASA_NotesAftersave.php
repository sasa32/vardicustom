<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_NotesAftersave
{
	function after_save($bean, $event, $arguments)
	{	
		/*
		Logic Hook para poblar automaticamente la relacion entre caso y reunión a traves del campo sasa_casorelacion_c
		*/
		try{

			if (!isset($bean->logic_hooks_notes_after_save_ignore_update) || $bean->logic_hooks_notes_after_save_ignore_update === false){//antiloop
				$bean->logic_hooks_notes_after_save_ignore_update = true;//antiloop

				global $db;
				//Si es enviar notificacion función para procesar y enviar correo
				if ($bean->sasa_tipo_de_nota_c == "NOTIFICACION AL ASESOR") {
					$this->SendEmailNotas($bean);
				}

				//If para quitar los saltos de lineas en las notas cuando el tipo de nota sea diferente de NOTIFICACION AL ASESOR
				if ($bean->sasa_tipo_de_nota_c != "NOTIFICACION AL ASESOR") {
					//FUncion para quitar los saltos de linea
					$string = preg_replace("[\n|\r|\n\r]", " ", $bean->description);
					$bean->description = $string;
					//Actualizar los registros antiguos por base de datos.
					$db->query("UPDATE notes INNER JOIN notes_cstm ON notes.id=notes_cstm.id_c SET notes.description = REPLACE(notes.description,'\n',' ') WHERE sasa_tipo_de_nota_c !='NOTIFICACION AL ASESOR' OR sasa_tipo_de_nota_c IS NULL");

				}
				
				//Varible para identificar si la nota ya tiene una URL
				$urlvisualizador= "http://templatephp";
				$pos = strpos($bean->sasa_soporte_adjunto_c, $urlvisualizador);

				if ($pos === false) {
					//sasa_soporte_adjunto_c no tiene una URL de visualizador establecida (http://templatephp)
					//Si no tiene se concatena el id de la nota enviado por SIGRU con la URL habilitada para el visualizador de adjuntos SIGRU
					$bean->sasa_soporte_adjunto_c = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$bean->sasa_soporte_adjunto_c;
					//$bean->save(false);
					$db->query("UPDATE notes INNER JOIN notes_cstm ON notes.id=notes_cstm.id_c SET notes_cstm.sasa_soporte_adjunto_c = '{$bean->sasa_soporte_adjunto_c}' WHERE notes.id ='{$bean->id}'");
				}

				//Enviar notas a SIGRU
				if ($bean->modified_by_name != "Integracion1") {
					if ($bean->parent_type=="Cases" && !empty($bean->parent_id)) {
						//Query para identificar si la nota tiene adjuntos (Nueva estrcutrua de adjuntos en Sugar en version 10.3)
						$query = "SELECT id, file_ext, filename FROM notes WHERE note_parent_id='{$bean->id}' AND deleted=0";
						$query_get_notes_attachment = $db->query($query);
						$NotesAttachment = $db->fetchByAssoc($query_get_notes_attachment);

						if (!empty($NotesAttachment)) {
							$fecthattachment = $db->query($query);
							while ($row = $db->fetchByAssoc($fecthattachment)) {
								$GLOBALS['log']->security("La nota tiene adjuntos y son: ".$row['id']);
								require_once("custom/modules/integration_sigru/Send_Data_Sigru_Notes.php");
								$sendsigruNotes = new Send_Data_Sigru_Notes();
								$repuesta = $sendsigruNotes->data($bean->id,$bean->module_name,$row['id'],$row['file_ext'],$row['filename']);
							}
						}else{
							$GLOBALS['log']->security("Nota sin adjunto: ");
							require_once("custom/modules/integration_sigru/Send_Data_Sigru_Notes.php");
							$sendsigruNotes = new Send_Data_Sigru_Notes();
							$repuesta = $sendsigruNotes->data($bean->id,$bean->module_name);
						}
						
						//Varible para identificar si la nota ya tiene una URL
						$urlvisualizador= "http://templatephp";
						$pos = strpos($bean->sasa_soporte_adjunto_c, $urlvisualizador);
						if ($repuesta) {
							$bean->sasa_soporte_adjunto_c = "";
							//if ($pos === false) {
								$repuesta = json_decode($repuesta['Response'],true);
								//sasa_url_formt_hd_c no tiene una URL de visualizador establecida (http://templatephp)
								//Si no tiene se concatena el id de la nota enviado por SIGRU con la URL habilitada para el visualizador de adjuntos SIGRU
								if ($repuesta['deRutaArch']!= null) {
									$bean->sasa_soporte_adjunto_c = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$repuesta['deRutaArch'];
									//$bean->save();
									$db->query("UPDATE notes INNER JOIN notes_cstm ON notes.id=notes_cstm.id_c SET notes_cstm.sasa_soporte_adjunto_c = '{$bean->sasa_soporte_adjunto_c}' WHERE notes.id ='{$bean->id}'");
								}
								if ($repuesta['idEsta'] != null) {
									$Case = BeanFactory::retrieveBean('Cases', $bean->parent_id, array('disable_row_level_security' => true));
					        		$Case->id_esta_c = $repuesta['idEsta'];
					        		$Case->save();
					        	}
								
							//}
						}
					}
				}
			}
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Notas: ".$e->getMessage()); 
		}
	}

	public function SendEmailNotas($bean){

		if ($bean->parent_type=="Cases" || $bean->parent_type=='Leads') {
			//Cargar dependencias
			require_once 'modules/Emails/Email.php';
        	require_once 'include/SugarPHPMailer.php';
        	global $app_list_strings;
			//Obtener registro relacionado
			$RelatedRecord = BeanFactory::retrieveBean($bean->parent_type,$bean->parent_id,array('disable_row_level_security' => true));

			//obtener correo del usuario asignado
			$User = BeanFactory::retrieveBean('Users',$bean->assigned_user_id,array('disable_row_level_security' => true));
			$GLOBALS['log']->security("USUARIO {$User->id} con email {$User->email1}");

			//Mapeo de listas desplegables
			//Traer toda la lista de motivo
			$campos = array('sasa_tipo_c' => 'sasa_tipo_c_list', 'sasa_motivo_c'=>'sasa_motivo_c_list', 'sasa_tipo_solicitud_c'=>'sasa_tipo_solicitud_c_list','lead_source'=>'lead_source_list','sasa_detalle_origen_c'=>'sasa_detalle_origen_c_list','source'=>'source_cases_list_c','sasa_estadocontacto_c'=>'sasa_estadocontacto_c_list');
			//Los valores que se asignaran luego para los campos list
			$valores = array('sasa_tipo_c'=>'', 'sasa_motivo_c'=>'','sasa_tipo_solicitud_c'=>'','lead_source'=>'','sasa_detalle_origen_c'=>'','source'=>'','sasa_estadocontacto_c'=>'');

			$list = array();
			foreach ($campos as $campo => $lista) {
				if (isset($app_list_strings[$lista]))
				{
				    $list = $app_list_strings[$lista];
				}
				//Asignar el valor segun el valor interno
				foreach ($list as $key => $value) {
					if ($key==$RelatedRecord->{$campo}) {
						$valores[$campo] = $value;
					}
				}
			}

			//Body a enviar en correo
			$body_html = "<p>Hola,</p><p>Se te ha asignado el siguiente caso {$valores['sasa_tipo_c']} {$valores['sasa_motivo_c']} {$valores['sasa_tipo_solicitud_c']}</p><p>Origen: {$valores['lead_source']}{$valores['sasa_detalle_origen_c']}{$valores['source']}</p><p>{$bean->sasa_plannotificarasesor_c}</p><p>Placa: {$RelatedRecord->nu_plac_vehi_c} {$RelatedRecord->sasa_placaleads_c}</p><p>Línea del Vehículo: {$RelatedRecord->sasa_lineanotificacionase_c}{$RelatedRecord->sasa_espejolineavehi_c}</p><p>Observación del caso: {$RelatedRecord->description}</p><p>Estado:{$valores['sasa_estadocontacto_c']}</p><p>Para mayor información del caso puedes consultar al equipo del Call Center con el número de caso {$RelatedRecord->case_number} o numero documento {$RelatedRecord->sasa_numero_documento_c}</p><p>Si va a remitir el LEAD a otra área por favor ingrese al siguiente enlace: <a href='https://bit.ly/3bsagQx'>https://bit.ly/3bsagQx</a></p>";

			//Enviar el correo
			$email_obj = new Email();
            $defaults = $email_obj->getSystemDefaultEmail();
            $mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults["email"];
            $mail->FromName = $defaults["name"];
            $mail->Subject = "Asignación de caso {$valores['sasa_tipo_c']} {$valores['sasa_motivo_c']} {$valores['sasa_tipo_solicitud_c']}";
            $mail->Body = from_html($body_html);
            $mail->AltBody =from_html($body_html);
            $mail->prepForOutbound();
            //Copia
            $mail->AddCC('canaldigitalnssw@dinissan.com.co');
            //Copia oculta
            $mail->AddBCC('jeilopez@grupovardi.com.co');
            $mail->AddAddress($User->email1);
            if (@$mail->Send()) {
	        }
	         
		}
	}
}
?>
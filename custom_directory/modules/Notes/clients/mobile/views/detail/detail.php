<?php
// created: 2023-02-08 11:06:10
$viewdefs['Notes']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 'name',
        1 => 'parent_name',
        2 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        3 => 
        array (
          'name' => 'sasa_tipo_de_nota_c',
          'label' => 'LBL_SASA_TIPO_DE_NOTA_C',
        ),
        4 => 
        array (
          'name' => 'sasa_fuente_c',
          'label' => 'LBL_SASA_FUENTE_C',
        ),
        5 => 
        array (
          'name' => 'sasa_usuario_avance_c',
          'label' => 'LBL_SASA_USUARIO_AVANCE_C',
        ),
        6 => 'attachment_list',
      ),
    ),
  ),
);
<?php
// created: 2022-10-31 10:59:53
$viewdefs['Notes']['mobile']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_LIST_SUBJECT',
          'default' => true,
          'enabled' => true,
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'date_modified',
          'label' => 'LBL_LIST_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
        ),
        2 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_LIST_DATE_ENTERED',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
          'target_module' => 'Employees',
          'target_record_key' => 'assigned_user_id',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
);
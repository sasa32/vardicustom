<?php
$viewdefs['Notes'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labels' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'parent_name',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'description',
                'rows' => 5,
                'span' => 12,
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'sasa_plannotificarasesor_c',
                'studio' => 'visible',
                'label' => 'LBL_SASA_PLANNOTIFICARASESOR_C',
                'span' => 12,
              ),
              3 => 
              array (
                'name' => 'sasa_tipo_de_nota_c',
                'label' => 'LBL_SASA_TIPO_DE_NOTA_C',
              ),
              4 => 
              array (
                'name' => 'sasa_fuente_c',
                'label' => 'LBL_SASA_FUENTE_C',
              ),
              5 => 
              array (
                'name' => 'assigned_user_name',
              ),
              6 => 
              array (
              ),
              7 => 
              array (
                'name' => 'sasa_usuario_avance_c',
                'label' => 'LBL_SASA_USUARIO_AVANCE_C',
              ),
              8 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              9 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              10 => 
              array (
                'name' => 'attachment_list',
                'label' => 'LBL_ATTACHMENTS',
                'type' => 'multi-attachments',
                'link' => 'attachments',
                'module' => 'Notes',
                'modulefield' => 'filename',
                'bLabel' => 'LBL_ADD_ATTACHMENT',
                'related_fields' => 
                array (
                  0 => 'filename',
                  1 => 'file_mime_type',
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);

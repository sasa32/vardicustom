({
    extendsFrom: 'CreateView',
    initialize: function(options) {
    var self=this;
    este = self;
    self._super('initialize', [options]);
        self.model.on('change:sasa_tipo_de_nota_c', self.functionnotifiasesor, self);
    },
    


    functionnotifiasesor: function() {
        console.log("OEEE")
        var self=this;
        var tipo = self.model.get('parent_type');
        var idRecordRealted = self.model.get('parent_id');
        var sasa_tipo_de_nota_c = self.model.get('sasa_tipo_de_nota_c');

        var RecordRelated = app.data.createBean(tipo, {id: idRecordRealted});
        if (sasa_tipo_de_nota_c=="NOTIFICACION AL ASESOR") {
            if (tipo=="Leads" || tipo=="Accounts") {
                RecordRelated.fetch({ success: function(data){
                    var name = RecordRelated.get('name');
                    var numid = RecordRelated.get('sasa_numero_documento_c');
                    var email1 = RecordRelated.get('email1');
                    var sasa_cel_principal_c = RecordRelated.get('sasa_cel_principal_c');
                    var sasa_phone_mobile_c = RecordRelated.get('sasa_phone_mobile_c');
                    var sasa_phone_office_c = RecordRelated.get('sasa_phone_office_c');
                    var sasa_phone_home_c = RecordRelated.get('sasa_phone_home_c');
                    var sasa_municipios_accounts_1_name = RecordRelated.get('sasa_municipios_accounts_1_name');
                    var sasa_municipios_leads_1_name = RecordRelated.get('sasa_municipios_leads_1_name');

                    if (sasa_cel_principal_c!=undefined) {
                        var cel = sasa_cel_principal_c
                    }else if (sasa_phone_mobile_c!=undefined) {
                        var cel = sasa_phone_mobile_c
                    }else{
                        var cel = ""
                    }
                    if (sasa_phone_office_c!=undefined) {
                        var tel = sasa_phone_office_c
                    }else if (sasa_phone_home_c!=undefined) {
                        var tel = sasa_phone_home_c
                    }else{
                        var tel = ""
                    }
                    if (sasa_municipios_accounts_1_name!=undefined) {
                        var ciudad = sasa_municipios_accounts_1_name
                    }else if (sasa_municipios_leads_1_name!=undefined) {
                        var ciudad = sasa_municipios_leads_1_name
                    }else{
                        var ciudad = ""
                    }

                    var descriptionNoti = "Nombre del cliente o lead: "+name+"\n"
                    descriptionNoti += "No. de documento: "+numid+"\n"
                    descriptionNoti += "Email: "+email1+"\n"
                    descriptionNoti += "Celular: "+cel+"\n" 
                    descriptionNoti += "Teléfono: "+tel+"\n"
                    descriptionNoti += "Ciudad: "+ciudad+"\n"

                    self.model.set('sasa_plannotificarasesor_c',descriptionNoti);          
                } });
            }else if (tipo=="Cases") {
                RecordRelated.fetch({ success: function(data){
                    //self.model.set('sasa_plannotificarasesor_c',RecordRelated.get('id')); 
                    var AccountCase = app.data.createBean('Accounts', {id: RecordRelated.get('account_id')});
                    AccountCase.fetch({ success: function(data){
                        var name = AccountCase.get('name');
                        var numid = AccountCase.get('sasa_numero_documento_c');
                        var email1 = AccountCase.get('email1');
                        var sasa_cel_principal_c = AccountCase.get('sasa_cel_principal_c');
                        var sasa_phone_office_c = AccountCase.get('sasa_phone_office_c');
                        var sasa_municipios_accounts_1_name = AccountCase.get('sasa_municipios_accounts_1_name');

                        var descriptionNoti = "Nombre del cliente o lead: "+name+"\n"
                        descriptionNoti += "No. de documento: "+numid+"\n"
                        descriptionNoti += "Email: "+email1+"\n"
                        descriptionNoti += "Celular: "+sasa_cel_principal_c+"\n" 
                        descriptionNoti += "Teléfono: "+sasa_phone_office_c+"\n"
                        descriptionNoti += "Ciudad: "+sasa_municipios_accounts_1_name+"\n"
                        self.model.set('sasa_plannotificarasesor_c',descriptionNoti);
                    } });
                } });
            }
        }
    },    
    
})

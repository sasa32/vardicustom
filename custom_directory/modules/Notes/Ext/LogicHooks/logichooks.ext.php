<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/LogicHooks/SASA_NotesAftersave.php

$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'SASA_NotesAftersave',

	//The PHP file where your class is located.
	'custom/modules/Notes/SASA_NotesAftersave.php',

	//The class the method is in.
	'SASA_NotesAftersave',

	//The method to call.
	'after_save'
);


?>

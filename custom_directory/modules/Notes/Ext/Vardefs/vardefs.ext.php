<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/rli_link_workflow.php

$dictionary['Note']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_description.php

 // created: 2020-09-10 15:42:59
$dictionary['Note']['fields']['description']['audited']=false;
$dictionary['Note']['fields']['description']['massupdate']=false;
$dictionary['Note']['fields']['description']['comments']='Full text of the note';
$dictionary['Note']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['description']['merge_filter']='disabled';
$dictionary['Note']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.44',
  'searchable' => true,
);
$dictionary['Note']['fields']['description']['calculated']=false;
$dictionary['Note']['fields']['description']['rows']='30';
$dictionary['Note']['fields']['description']['cols']='90';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/full_text_search_admin.php

 // created: 2020-08-28 14:29:24
$dictionary['Note']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2020-10-19 13:46:26
$dictionary['Note']['fields']['date_entered']['audited']=false;
$dictionary['Note']['fields']['date_entered']['comments']='Date record created';
$dictionary['Note']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Note']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Note']['fields']['date_entered']['calculated']=false;
$dictionary['Note']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_parent_id.php

 // created: 2020-10-19 13:46:54
$dictionary['Note']['fields']['parent_id']['audited']=false;
$dictionary['Note']['fields']['parent_id']['massupdate']=false;
$dictionary['Note']['fields']['parent_id']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['parent_id']['duplicate_merge_dom_value']=1;
$dictionary['Note']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['Note']['fields']['parent_id']['calculated']=false;
$dictionary['Note']['fields']['parent_id']['len']=255;
$dictionary['Note']['fields']['parent_id']['unified_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_parent_name.php

 // created: 2020-10-19 13:46:54
$dictionary['Note']['fields']['parent_name']['audited']=false;
$dictionary['Note']['fields']['parent_name']['massupdate']=false;
$dictionary['Note']['fields']['parent_name']['options']='parent_type_display';
$dictionary['Note']['fields']['parent_name']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['parent_name']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['Note']['fields']['parent_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_parent_type.php

 // created: 2020-10-19 13:46:54
$dictionary['Note']['fields']['parent_type']['audited']=false;
$dictionary['Note']['fields']['parent_type']['massupdate']=false;
$dictionary['Note']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['parent_type']['duplicate_merge_dom_value']=1;
$dictionary['Note']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Note']['fields']['parent_type']['calculated']=false;
$dictionary['Note']['fields']['parent_type']['len']=255;
$dictionary['Note']['fields']['parent_type']['options']='';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2022-05-31 01:59:51
$dictionary['Note']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['Note']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['Note']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_soporte_adjunto_c.php

 // created: 2022-05-31 01:59:51
$dictionary['Note']['fields']['sasa_soporte_adjunto_c']['labelValue']='Soporte Adjunto (URL)';
$dictionary['Note']['fields']['sasa_soporte_adjunto_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Note']['fields']['sasa_soporte_adjunto_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_tipo_de_nota_c.php

 // created: 2022-05-31 01:59:51
$dictionary['Note']['fields']['sasa_tipo_de_nota_c']['labelValue']='Tipo de Nota';
$dictionary['Note']['fields']['sasa_tipo_de_nota_c']['dependency']='';
$dictionary['Note']['fields']['sasa_tipo_de_nota_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_usuario_avance_c.php

 // created: 2022-05-31 01:59:51
$dictionary['Note']['fields']['sasa_usuario_avance_c']['labelValue']='Usuario Avance';
$dictionary['Note']['fields']['sasa_usuario_avance_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Note']['fields']['sasa_usuario_avance_c']['enforced']='';
$dictionary['Note']['fields']['sasa_usuario_avance_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_id_tipo_c.php

 // created: 2022-05-31 01:59:51

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_plannotificarasesor_c.php

 // created: 2022-05-31 01:59:51

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_gestionado_c.php

 // created: 2022-05-31 01:59:51

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_id_avan_c.php

 // created: 2022-05-31 01:59:51

 
?>

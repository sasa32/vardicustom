({
    extendsFrom: 'CasesRecordView',
    initialize: function(options) {
	var self=this;
	este = self;
	self._super('initialize', [options]);
    
	self.model.addValidationTask('sasa_motivo_c', _.bind(self._ValidarMotivo, self));

    self.model.once("sync",
        function() {
            self.disabledFunction();
        },self
    );
	
    },

    disabledFunction: function(){
        var self = this;
        var sasa_motivo_c = self.model.get('sasa_motivo_c');
        //Motivos habilitados 
        var motivosreq = [
            '54',// Cita campaña de seguridad
            '55',//Cita campaña de servicio
            '56',//Cita carro taller
            '57',//Cita en vitrina
            '58',//Cita garantía
            '59'//Cita taller RMP
        ];
        if (motivosreq.indexOf(sasa_motivo_c) != -1) {
            console.log('visible cita');
            //add listener for custom button
            console.log("add listener for button");
            self.context.on('button:agendarcita:click', self.agendarcita, self);
        }else{
            console.log("disabled the agendar_cita_button");
            $('[name="agendar_cita_button"]').attr('disabled',true);
        }
    },
    
    agendarcita: function(){
        window.open("https://sigruweb.dinissan.com.co/SIGRU/Citas_Web/v2/validaDatos.xhtml", "Diseño Web", "width=900, height=650")
    },


    _ValidarMotivo: function(fields, errors, callback) {
        var self=this;
        var sasa_pqrf_c = self.model.get('sasa_pqrf_c');

        if (sasa_pqrf_c=="S") {
            if (window.location.pathname != null) {
                var your_url = window.location.origin+window.location.pathname;
            }else{
                var your_url = window.location.origin;
            }
            var responseSugar=Array();
            //Compañia
            var cd_cia_c = self.model.get('cd_cia_c');
            //Unidad
            var cd_uneg_cont_c = self.model.get('cd_uneg_cont_c');
            cd_uneg_cont_c = cd_uneg_cont_c.substr(-2);
            //SubUnidad
            var cd_area_c = self.model.get('cd_area_c');
            cd_area_c = cd_area_c.substr(-2);
            //motivo
            var sasa_motivo_c = self.model.get('sasa_motivo_c');
            //tipo
            var sasa_tipo_c = self.model.get('sasa_tipo_c');
            console.log("com "+cd_cia_c+" Unidad "+cd_uneg_cont_c+" SubUnidad "+cd_area_c+" Motivo "+sasa_motivo_c+" tipo "+sasa_tipo_c)
            $.ajax({
                type: 'POST',
                url : your_url+"rest/v11/ws_motivos_cases",
                data:{
                    "compania":cd_cia_c,
                    "unidadneg":cd_uneg_cont_c,
                    "subunidadneg":cd_area_c,
                    "codigo_motivo_sugar":sasa_motivo_c,
                    "tipo_sugar":sasa_tipo_c
                },
                async:false,
                success: function (response) {
                    responseSugar=response;
                },
                error: function (response){
                    app.alert.show('message-id', {
                        level: 'error',
                        messages: 'Falló el consumo',
                        autoClose: true
                    });
                }
            })
            console.log(responseSugar)
            if (responseSugar.length != 0) {
                if (responseSugar[0].codigo == "No hay") {
                    var message_id = "M_ValidarMotivo";
                    app.alert.show(message_id, {
                        level: 'info',
                        title: 'Nota',
                        messages: ["La combinación de Compañia, Unidad, Subunidad, tipo y motivo no es la correcta"],
                        autoClose: false,
                    });
                
                    errors['sasa_motivo_c'] = errors['sasa_motivo_c'] || {};
                    errors['sasa_motivo_c'].required = true;
                }
            }
        }else{
            console.log("No es PQRF");
        }

		
        
        
        callback(null, fields, errors);
    },    
    
})

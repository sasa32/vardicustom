<?php
$viewdefs['Cases'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'case_number',
                'label' => 'LBL_LIST_NUMBER',
                'default' => true,
                'enabled' => true,
                'readonly' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'label' => 'LBL_LIST_SUBJECT',
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              2 => 
              array (
                'name' => 'priority',
                'label' => 'LBL_LIST_PRIORITY',
                'default' => true,
                'enabled' => true,
              ),
              3 => 
              array (
                'name' => 'account_name',
                'label' => 'LBL_LIST_ACCOUNT_NAME',
                'module' => 'Accounts',
                'id' => 'ACCOUNT_ID',
                'ACLTag' => 'ACCOUNT',
                'related_fields' => 
                array (
                  0 => 'account_id',
                ),
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              4 => 
              array (
                'name' => 'cd_cia_c',
                'label' => 'LBL_CD_CIA',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'default' => true,
                'enabled' => true,
              ),
              6 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'id' => 'ASSIGNED_USER_ID',
                'default' => true,
                'enabled' => true,
              ),
              7 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'default' => true,
                'enabled' => true,
                'readonly' => true,
              ),
              8 => 
              array (
                'name' => 'date_modified',
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'sasa_tipificacion_de_casos_cases_1_name',
                'label' => 'LBL_SASA_TIPIFICACION_DE_CASOS_CASES_1_FROM_SASA_TIPIFICACION_DE_CASOS_TITLE',
                'enabled' => true,
                'id' => 'SASA_TIPIFICACION_DE_CASOS_CASES_1SASA_TIPIFICACION_DE_CASOS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_LIST_TEAM',
                'default' => false,
                'enabled' => true,
              ),
              11 => 
              array (
                'name' => 'sasa_document_solicitado_c',
                'label' => 'LBL_SASA_DOCUMENT_SOLICITADO_C',
                'enabled' => true,
                'default' => false,
              ),
              12 => 
              array (
                'name' => 'id_dere_peti_c',
                'label' => 'LBL_ID_DERE_PETI',
                'enabled' => true,
                'default' => false,
              ),
              13 => 
              array (
                'name' => 'cases_sasa_puntos_de_ventas_1_name',
                'label' => 'LBL_CASES_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
                'enabled' => true,
                'id' => 'CASES_SASA_PUNTOS_DE_VENTAS_1SASA_PUNTOS_DE_VENTAS_IDB',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              14 => 
              array (
                'name' => 'sasa_centrosdecostos_cases_1_name',
                'label' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_NAME_FIELD_TITLE',
                'enabled' => true,
                'id' => 'SASA_CENTROSDECOSTOS_CASES_1SASA_CENTROSDECOSTOS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              15 => 
              array (
                'name' => 'sasa_fecha_cierre_pqrf_c',
                'label' => 'LBL_SASA_FECHA_CIERRE_PQRF_C',
                'enabled' => true,
                'default' => false,
              ),
              16 => 
              array (
                'name' => 'nu_plac_vehi_c',
                'label' => 'LBL_NU_PLAC_VEHI',
                'enabled' => true,
                'default' => false,
              ),
              17 => 
              array (
                'name' => 'nu_kms_c',
                'label' => 'LBL_NU_KMS',
                'enabled' => true,
                'default' => false,
              ),
              18 => 
              array (
                'name' => 'nu_fact_repu_c',
                'label' => 'LBL_NU_FACT_REPU',
                'enabled' => true,
                'default' => false,
              ),
              19 => 
              array (
                'name' => 'nu_chas_seri_c',
                'label' => 'LBL_NU_CHAS_SERI',
                'enabled' => true,
                'default' => false,
              ),
              20 => 
              array (
                'name' => 'cd_area_c',
                'label' => 'LBL_CD_AREA',
                'enabled' => true,
                'default' => false,
              ),
              21 => 
              array (
                'name' => 'id_esta_c',
                'label' => 'LBL_ID_ESTA',
                'enabled' => true,
                'default' => false,
              ),
              22 => 
              array (
                'name' => 'sasa_pqrf_c',
                'label' => 'LBL_SASA_PQRFS_C',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);

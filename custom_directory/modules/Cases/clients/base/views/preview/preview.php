<?php
$viewdefs['Cases'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'case_number',
                'readonly' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_tipificacion_de_casos_cases_1_name',
              ),
              2 => 'priority',
              3 => 'account_name',
              4 => 
              array (
                'name' => 'cd_cia_c',
                'label' => 'LBL_CD_CIA',
              ),
              5 => 
              array (
                'name' => 'cd_uneg_cont_c',
                'label' => 'LBL_CD_UNEG_CONT',
              ),
              6 => 
              array (
                'name' => 'cd_area_c',
                'label' => 'LBL_CD_AREA',
              ),
              7 => 
              array (
                'name' => 'sasa_instructivo_c',
                'studio' => 'visible',
                'label' => 'LBL_SASA_INSTRUCTIVO_C',
              ),
              8 => 'source',
              9 => 'status',
              10 => 
              array (
                'name' => 'cases_sasa_puntos_de_ventas_1_name',
              ),
              11 => 
              array (
                'name' => 'sasa_centrosdecostos_cases_1_name',
                'label' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_NAME_FIELD_TITLE',
              ),
              12 => 'resolved_datetime',
              13 => 
              array (
                'name' => 'sasa_document_solicitado_c',
                'label' => 'LBL_SASA_DOCUMENT_SOLICITADO_C',
              ),
              14 => 
              array (
                'name' => 'sasa_area_solicitante_c',
                'label' => 'LBL_SASA_AREA_SOLICITANTE_C',
              ),
              15 => 
              array (
                'name' => 'description',
                'nl2br' => true,
              ),
              16 => 
              array (
                'name' => 'commentlog',
                'label' => 'LBL_COMMENTLOG',
              ),
              17 => 
              array (
                'name' => 'resolution',
                'nl2br' => true,
              ),
              18 => 
              array (
                'name' => 'nu_pqrf_c',
                'label' => 'LBL_NU_PQRF',
              ),
              19 => 
              array (
                'name' => 'id_esta_c',
                'label' => 'LBL_ID_ESTA',
              ),
              20 => 
              array (
                'name' => 'sasa_fecha_cierre_pqrf_c',
                'label' => 'LBL_SASA_FECHA_CIERRE_PQRF_C',
              ),
              21 => 'primary_contact_name',
              22 => 
              array (
                'readonly' => false,
                'name' => 'sasa_nombrerepresencs_c',
                'label' => 'LBL_SASA_NOMBREREPRESENCS_C',
              ),
              23 => 
              array (
                'name' => 'nu_nit_empl_c',
                'label' => 'LBL_NU_NIT_EMPL',
              ),
              24 => 
              array (
                'name' => 'id_contacto_c',
                'label' => 'LBL_ID_CONTACTO',
              ),
              25 => 
              array (
                'name' => 'nu_chas_seri_c',
                'label' => 'LBL_NU_CHAS_SERI',
              ),
              26 => 
              array (
                'name' => 'nu_plac_vehi_c',
                'label' => 'LBL_NU_PLAC_VEHI',
              ),
              27 => 
              array (
                'name' => 'nu_kms_c',
                'label' => 'LBL_NU_KMS',
              ),
              28 => 
              array (
                'name' => 'nu_fact_repu_c',
                'label' => 'LBL_NU_FACT_REPU',
              ),
              29 => 
              array (
                'name' => 'nu_orde_trab_c',
                'label' => 'LBL_NU_ORDE_TRAB',
              ),
              30 => 
              array (
                'name' => 'id_dere_peti_c',
                'label' => 'LBL_ID_DERE_PETI',
              ),
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_RECORD_SHOWMORE',
            'hide' => true,
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 'assigned_user_name',
              1 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              2 => 'team_name',
              3 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
          'useTabs' => false,
        ),
      ),
    ),
  ),
);

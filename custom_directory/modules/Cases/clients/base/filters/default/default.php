<?php
// created: 2023-03-06 03:36:06
$viewdefs['Cases']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'quicksearch_field' => 
  array (
    0 => 'name',
    1 => 'case_number',
  ),
  'quicksearch_priority' => 2,
  'fields' => 
  array (
    'case_number' => 
    array (
    ),
    'nu_pqrf_c' => 
    array (
    ),
    'sasa_pqrf_c' => 
    array (
    ),
    'name' => 
    array (
    ),
    'priority' => 
    array (
    ),
    'account_name' => 
    array (
    ),
    'cd_cia_c' => 
    array (
    ),
    'cd_uneg_cont_c' => 
    array (
    ),
    'cd_area_c' => 
    array (
    ),
    'status' => 
    array (
    ),
    'sasa_tipo_c' => 
    array (
    ),
    'sasa_motivo_c' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
  ),
);
<?php
// created: 2023-02-08 11:06:09
$viewdefs['Cases']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        1 => 'priority',
        2 => 'account_name',
        3 => 
        array (
          'name' => 'cd_cia_c',
          'label' => 'LBL_CD_CIA',
        ),
        4 => 
        array (
          'name' => 'cd_uneg_cont_c',
          'label' => 'LBL_CD_UNEG_CONT',
        ),
        5 => 
        array (
          'name' => 'cd_area_c',
          'label' => 'LBL_CD_AREA',
        ),
        6 => 
        array (
          'name' => 'sasa_tipo_c',
          'label' => 'LBL_SASA_TIPO_C',
        ),
        7 => 
        array (
          'name' => 'sasa_motivo_c',
          'label' => 'LBL_SASA_MOTIVO_C',
        ),
        8 => 'status',
        9 => 
        array (
          'name' => 'resolved_datetime',
          'comment' => 'Date when an issue is resolved',
          'label' => 'LBL_RESOLVED_DATETIME',
        ),
        10 => 
        array (
          'name' => 'source',
          'comment' => 'An indicator of how the bug was entered (ex: via web, email, etc.)',
          'label' => 'LBL_SOURCE',
        ),
        11 => 
        array (
          'name' => 'sasa_document_solicitado_c',
          'label' => 'LBL_SASA_DOCUMENT_SOLICITADO_C',
        ),
        12 => 
        array (
          'name' => 'sasa_area_solicitante_c',
          'label' => 'LBL_SASA_AREA_SOLICITANTE_C',
        ),
        13 => 
        array (
          'name' => 'cases_sasa_puntos_de_ventas_1_name',
          'label' => 'LBL_CASES_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
        ),
        14 => 'description',
        15 => 
        array (
          'name' => 'work_log',
          'comment' => 'Free-form text used to denote activities of interest',
          'label' => 'LBL_WORK_LOG',
        ),
        16 => 'resolution',
        17 => 
        array (
          'name' => 'sasa_pqrf_c',
          'label' => 'LBL_SASA_PQRFS_C',
        ),
        18 => 
        array (
          'name' => 'id_esta_c',
          'label' => 'LBL_ID_ESTA',
        ),
        19 => 
        array (
          'name' => 'nu_pqrf_c',
          'label' => 'LBL_NU_PQRF',
        ),
        20 => 
        array (
          'name' => 'sasa_fecha_cierre_pqrf_c',
          'label' => 'LBL_SASA_FECHA_CIERRE_PQRF_C',
        ),
        21 => 
        array (
          'name' => 'nu_nit_empl_c',
          'label' => 'LBL_NU_NIT_EMPL',
        ),
        22 => 
        array (
          'name' => 'id_contacto_c',
          'label' => 'LBL_ID_CONTACTO',
        ),
        23 => 
        array (
          'name' => 'nu_plac_vehi_c',
          'label' => 'LBL_NU_PLAC_VEHI',
        ),
        24 => 
        array (
          'name' => 'nu_chas_seri_c',
          'label' => 'LBL_NU_CHAS_SERI',
        ),
        25 => 
        array (
          'name' => 'nu_kms_c',
          'label' => 'LBL_NU_KMS',
        ),
        26 => 
        array (
          'name' => 'nu_fact_repu_c',
          'label' => 'LBL_NU_FACT_REPU',
        ),
        27 => 
        array (
          'name' => 'nu_orde_trab_c',
          'label' => 'LBL_NU_ORDE_TRAB',
        ),
        28 => 
        array (
          'name' => 'id_dere_peti_c',
          'label' => 'LBL_ID_DERE_PETI',
        ),
        29 => 'assigned_user_name',
        30 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        31 => 
        array (
          'name' => 'created_by_name',
          'readonly' => true,
          'label' => 'LBL_CREATED',
        ),
        32 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
        33 => 
        array (
          'name' => 'modified_by_name',
          'readonly' => true,
          'label' => 'LBL_MODIFIED',
        ),
      ),
    ),
  ),
);
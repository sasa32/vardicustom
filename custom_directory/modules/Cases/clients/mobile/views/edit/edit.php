<?php
// created: 2023-02-08 11:06:09
$viewdefs['Cases']['mobile']['view']['edit'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        1 => 
        array (
          'name' => 'case_number',
          'displayParams' => 
          array (
            'required' => false,
            'wireless_detail_only' => true,
          ),
        ),
        2 => 'priority',
        3 => 'account_name',
        4 => 
        array (
          'name' => 'cd_cia_c',
          'label' => 'LBL_CD_CIA',
        ),
        5 => 
        array (
          'name' => 'cd_uneg_cont_c',
          'label' => 'LBL_CD_UNEG_CONT',
        ),
        6 => 
        array (
          'name' => 'cd_area_c',
          'label' => 'LBL_CD_AREA',
        ),
        7 => 
        array (
          'name' => 'sasa_tipo_c',
          'label' => 'LBL_SASA_TIPO_C',
        ),
        8 => 
        array (
          'name' => 'sasa_motivo_c',
          'label' => 'LBL_SASA_MOTIVO_C',
        ),
        9 => 'status',
        10 => 
        array (
          'name' => 'resolved_datetime',
          'comment' => 'Date when an issue is resolved',
          'label' => 'LBL_RESOLVED_DATETIME',
        ),
        11 => 
        array (
          'name' => 'source',
          'comment' => 'An indicator of how the bug was entered (ex: via web, email, etc.)',
          'label' => 'LBL_SOURCE',
        ),
        12 => 
        array (
          'name' => 'sasa_document_solicitado_c',
          'label' => 'LBL_SASA_DOCUMENT_SOLICITADO_C',
        ),
        13 => 
        array (
          'name' => 'sasa_area_solicitante_c',
          'label' => 'LBL_SASA_AREA_SOLICITANTE_C',
        ),
        14 => 
        array (
          'name' => 'cases_sasa_puntos_de_ventas_1_name',
          'label' => 'LBL_CASES_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
        ),
        15 => 'description',
        16 => 
        array (
          'name' => 'work_log',
          'comment' => 'Free-form text used to denote activities of interest',
          'label' => 'LBL_WORK_LOG',
        ),
        17 => 'resolution',
        18 => 
        array (
          'name' => 'sasa_pqrf_c',
          'label' => 'LBL_SASA_PQRFS_C',
        ),
        19 => 
        array (
          'name' => 'id_esta_c',
          'label' => 'LBL_ID_ESTA',
        ),
        20 => 
        array (
          'name' => 'nu_pqrf_c',
          'label' => 'LBL_NU_PQRF',
        ),
        21 => 
        array (
          'name' => 'sasa_fecha_cierre_pqrf_c',
          'label' => 'LBL_SASA_FECHA_CIERRE_PQRF_C',
        ),
        22 => 
        array (
          'name' => 'nu_nit_empl_c',
          'label' => 'LBL_NU_NIT_EMPL',
        ),
        23 => 
        array (
          'name' => 'id_contacto_c',
          'label' => 'LBL_ID_CONTACTO',
        ),
        24 => 
        array (
          'name' => 'nu_plac_vehi_c',
          'label' => 'LBL_NU_PLAC_VEHI',
        ),
        25 => 
        array (
          'name' => 'nu_chas_seri_c',
          'label' => 'LBL_NU_CHAS_SERI',
        ),
        26 => 
        array (
          'name' => 'nu_kms_c',
          'label' => 'LBL_NU_KMS',
        ),
        27 => 
        array (
          'name' => 'nu_fact_repu_c',
          'label' => 'LBL_NU_FACT_REPU',
        ),
        28 => 
        array (
          'name' => 'nu_orde_trab_c',
          'label' => 'LBL_NU_ORDE_TRAB',
        ),
        29 => 
        array (
          'name' => 'id_dere_peti_c',
          'label' => 'LBL_ID_DERE_PETI',
        ),
        30 => 'assigned_user_name',
        31 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        32 => 
        array (
          'name' => 'created_by_name',
          'readonly' => true,
          'label' => 'LBL_CREATED',
        ),
        33 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
        34 => 
        array (
          'name' => 'modified_by_name',
          'readonly' => true,
          'label' => 'LBL_MODIFIED',
        ),
      ),
    ),
  ),
);
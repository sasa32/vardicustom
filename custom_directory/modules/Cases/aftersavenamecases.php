<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class aftersavenamecases
{
	function after_save($bean, $event, $arguments)
	{	
		/*
		Logic Hook para poblar automaticamente la esctutura geografica de relacion
		*/
		try{
			if (!isset($bean->aftersavenamecases) || $bean->aftersavenamecases === false){//antiloop
				$bean->aftersavenamecases = true;//antiloop	
				global $db;
				global $app_list_strings;

				//Traer toda la lista de motivo
				$list = array();
				if (isset($app_list_strings['sasa_motivo_c_list']))
				{
				    $list = $app_list_strings['sasa_motivo_c_list'];
				}
				//Asignar el motivo segun el valor interno
				foreach ($list as $key => $value) {
					if ($key==$bean->sasa_motivo_c) {
						$motivo = $value;
					}
				}

				$bean->name = $bean->case_number." - ".$motivo;

				if (!empty($bean->sasa_centrocostos_c)){
					$resultpunto = $GLOBALS['db']->query("SELECT id FROM sasa_centrosdecostos INNER JOIN sasa_centrosdecostos_cstm ON sasa_centrosdecostos.id=sasa_centrosdecostos_cstm.id_c WHERE sasa_centrosdecostos_cstm.sasa_codcentrodecostos_c='{$bean->sasa_centrocostos_c}' AND deleted=0");
					$sasa_cod_puntoAtencion =  $GLOBALS['db']->fetchByAssoc($resultpunto);	
					
					if ($bean->load_relationship('sasa_centrosdecostos_cases_1')){
						$relatedBeans = $bean->sasa_centrosdecostos_cases_1->add($sasa_cod_puntoAtencion["id"]);
					}
				}

				
				$querymotivo = $GLOBALS['db']->query("SELECT * FROM diccionario_motivos WHERE codigo = '{$bean->sasa_motivo_c}' AND tipo = '{$bean->sasa_tipo_c}' AND estado='A' LIMIT 1;");
				$MotivoDiccionario =  $GLOBALS['db']->fetchByAssoc($querymotivo);
				if ($MotivoDiccionario['codigo'] != null) {
					$bean->sasa_motivo_c = $MotivoDiccionario['codigo_motivo_sugar'];
					$bean->sasa_tipo_c = $MotivoDiccionario['tipo_sugar'];
				}


				//Mapear a través del numero de cedula del empleado el nombre del empleado y llevarlo al nu_nit_empl_c
				if (!empty($bean->nu_nit_empl_c)) {
					$Contact = BeanFactory::newBean("Contacts");
					$Contact->retrieve_by_string_fields( 
						array( 
							'sasa_numero_documento_c' => $bean->nu_nit_empl_c
						) 
					);

					if ($Contact->id) {
						$bean->sasa_nombrerepresencs_c = $Contact->first_name." ".$Contact->last_name;
					}
				}

				//Mapear cuentas genericas en caso de que el caso esté relacionado a un contacto o lead.
				if ($bean->contacts_cases_1contacts_ida != "" || $bean->leads_cases_1leads_ida) {
					if ($bean->contacts_cases_1contacts_ida != "") {
						//Mapear la cuenta generica "CUENTA GENéRICA CONTACTOS"
						$bean->account_id = "c5f0a53e-8cc3-11eb-b3f2-0286beac7abe";
					}elseif ($bean->leads_cases_1leads_ida != "") {
						//Mapear la cuenta generica "CUENTA LEADS"
						$bean->account_id = "a2590d86-50f6-11eb-9d24-0286beac7abe";
					}
				}

				$bean->save();
				if ($bean->sasa_pqrf_c == "S" && $bean->modified_by_name != "Integracion1") {
					if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) {
			            require_once("custom/modules/integration_sigru/Send_Data_Sigru_Cases.php");
						$sendsigru = new Send_Data_Sigru_Cases();
						$repuesta = $sendsigru->data($bean->id,$bean->module_name,"POST");

						//Mapeo de campos de la respuesta
						if ($repuesta) {
				        	$repuesta = json_decode($repuesta['Response'],true);
				        	if ($repuesta['nuPqrf'] != null) {
				        		$bean->nu_pqrf_c = $repuesta['nuPqrf'];
				        	}
				        	if ($repuesta['nitResp'] != null) {
				        		$bean->nu_nit_empl_c = $repuesta['nitResp'];
				        	}
				        	if ($repuesta['idEsta'] != null) {
				        		$bean->id_esta_c = $repuesta['idEsta'];
				        	}
				        	
				        	$bean->save();
				        }

						//Crear nota o avance al momento de la creación del caso
						//Create bean
						$Note = BeanFactory::newBean('Notes');
						$Note->name = 'Avance Inicial';
						$Note->description = $bean->description;
						$Note->parent_id = $bean->id;
						$Note->parent_type = $bean->module_name;
						$User = BeanFactory::retrieveBean('Users', $bean->created_by, array('disable_row_level_security' => true));
						$Note->sasa_usuario_avance_c = $User->user_name;
						$Note->sasa_id_avan_c = "A";
						$Note->sasa_id_tipo_c = "P";
						//Save
						$Note->save();

			        } else {
			            require_once("custom/modules/integration_sigru/Send_Data_Sigru_Cases.php");
						$sendsigru = new Send_Data_Sigru_Cases();
						$repuesta = $sendsigru->data($bean->id,$bean->module_name,"PUT");
						//Mapeo de campos de la respuesta
						if ($repuesta) {
				        	$repuesta = json_decode($repuesta['Response'],true);
				        	if ($repuesta['nuPqrf'] != null) {
				        		$bean->nu_pqrf_c = $repuesta['nuPqrf'];
				        	}
				        	if ($repuesta['nitResp'] != null) {
				        		$bean->nu_nit_empl_c = $repuesta['nitResp'];
				        	}
				        	if ($repuesta['idEsta'] != null) {
				        		$bean->id_esta_c = $repuesta['idEsta'];
				        	}
				        	
				        	$bean->save();
				        }
			        }
				}
				
			}
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Cases Name: ".$e->getMessage()); 
		}
	}
}
?>

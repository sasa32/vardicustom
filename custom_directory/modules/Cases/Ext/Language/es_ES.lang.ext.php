<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customsasa_tipificacion_de_casos_cases_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customsasa_tipificacion_de_casos_cases_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_TIPIFICACION_DE_CASOS_CASES_1_FROM_SASA_TIPIFICACION_DE_CASOS_TITLE'] = 'Tipificación de Casos';
$mod_strings['LBL_SASA_TIPIFICACION_DE_CASOS_CASES_1_FROM_CASES_TITLE'] = 'Tipificación de Casos';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_TIPIFICACION_DE_CASOS_CASES_1_FROM_SASA_TIPIFICACION_DE_CASOS_TITLE'] = 'Tipificación de Casos';
$mod_strings['LBL_SASA_TIPIFICACION_DE_CASOS_CASES_1_FROM_CASES_TITLE'] = 'Tipificación de Casos';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0012020-11-11.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0022020-11-19.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0042020-12-23.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0042020-12-28.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0062021-05-14.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0062021-05-14.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0082021-06-17.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0082021-06-17.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0082021-06-17.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0082021-06-17.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0092021-06-21.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0092021-06-21.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita URL';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITA_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita URL';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0122021-07-15.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0122021-07-15.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0122021-07-15.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0122021-07-15.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0122021-07-15.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0122021-07-15.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0122021-07-15.php

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';

?>
<?php
//L_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_AGENDAR_CITAU_C'] = 'Agendar Cita';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_ZAPIER_C'] = 'Zapier';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customcalls_cases_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CALLS_CASES_1_FROM_CALLS_TITLE'] = 'Llamadas';
$mod_strings['LBL_CALLS_CASES_1_FROM_CASES_TITLE'] = 'Llamadas';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customsasa_puntos_de_ventas_cases_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Puntos de Atención';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE'] = 'Puntos de Atención';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customcases_sasa_puntos_de_ventas_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CASES_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Puntos de Atención';
$mod_strings['LBL_CASES_SASA_PUNTOS_DE_VENTAS_1_FROM_CASES_TITLE'] = 'Puntos de Atención';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customsasa_vehiculos_cases_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_CASES_1_FROM_SASA_VEHICULOS_TITLE'] = 'Líneas de vehículos';
$mod_strings['LBL_SASA_VEHICULOS_CASES_1_FROM_CASES_TITLE'] = 'Líneas de vehículos';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customsasa_vehiculos_cases_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_CASES_2_FROM_SASA_VEHICULOS_TITLE'] = 'Líneas de vehículos';
$mod_strings['LBL_SASA_VEHICULOS_CASES_2_FROM_CASES_TITLE'] = 'Líneas de vehículos';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customsasa_vehiculos_cases_3.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_CASES_3_FROM_SASA_VEHICULOS_TITLE'] = 'Líneas de vehículos';
$mod_strings['LBL_SASA_VEHICULOS_CASES_3_FROM_CASES_TITLE'] = 'Líneas de vehículos';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customsasa_vehiculos_cases_4.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_CASES_4_FROM_SASA_VEHICULOS_TITLE'] = 'Líneas de vehículos';
$mod_strings['LBL_SASA_VEHICULOS_CASES_4_FROM_CASES_TITLE'] = 'Líneas de vehículos';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldV0152021-10-06.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldV0152021-10-06.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/temp.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0162021-11-16.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0162021-11-18.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0172021-12-04.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha de contacto';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha inicio de gestión ';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha final de gestión';
$mod_strings['LBL_SASA_ESTADOCOMERCIAL_C'] = 'Estado Comercial';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0172021-12-05.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha de contacto';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha inicio de gestión ';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha final de gestión';
$mod_strings['LBL_SASA_ESTADOCOMERCIAL_C'] = 'Estado Comercial';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0192021-12-07.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha de contacto';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha inicio de gestión ';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha final de gestión';
$mod_strings['LBL_SASA_ESTADOCOMERCIAL_C'] = 'Estado Comercial';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0192022-05-31.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha de contacto';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha inicio de gestión ';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha final de gestión';
$mod_strings['LBL_SASA_ESTADOCOMERCIAL_C'] = 'Estado Comercial';
$mod_strings['LBL_SASA_CENTROCOSTOS_C'] = 'Centro de costos';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0192022-07-27.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha de contacto';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha inicio de gestión ';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha final de gestión';
$mod_strings['LBL_SASA_ESTADOCOMERCIAL_C'] = 'Estado Comercial';
$mod_strings['LBL_SASA_CENTROCOSTOS_C'] = 'Centro de costos';
$mod_strings['LBL_COUNT_CALL_STATUS'] = 'Contador Estado Llamada';
$mod_strings['LBL_SASA_EVAL_SI_C'] = 'Eval Si';
$mod_strings['LBL_SASA_EVAL_NO_C'] = 'Eval No';
$mod_strings['LBL_SASA_EVAL_NO_CONTACTO_C'] = 'Eval No Contactado';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customsasa_centrosdecostos_cases_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_SASA_CENTROSDECOSTOS_TITLE'] = 'Centros De Costos';
$mod_strings['LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE'] = 'Centros De Costos';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.gvcasescustomfieldv0192022-10-21.php


$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS_C'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_COD_PUNTOATEN_C'] = 'Código Punto de Atención';
$mod_strings['LBL_SASA_MOTIVOLLORENTE_C'] = 'Motivo de Llorente (Zapier)';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha de contacto';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha inicio de gestión ';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha final de gestión';
$mod_strings['LBL_SASA_ESTADOCOMERCIAL_C'] = 'Estado Comercial';
$mod_strings['LBL_SASA_CENTROCOSTOS_C'] = 'Centro de costos';
$mod_strings['LBL_COUNT_CALL_STATUS'] = 'Contador Estado Llamada';
$mod_strings['LBL_SASA_EVAL_SI_C'] = 'Eval Si';
$mod_strings['LBL_SASA_EVAL_NO_C'] = 'Eval No';
$mod_strings['LBL_SASA_EVAL_NO_CONTACTO_C'] = 'Eval No Contactado';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customcontacts_cases_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_CONTACTS_CASES_1_FROM_CASES_TITLE'] = 'Contactos';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.customleads_cases_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LEADS_CASES_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_LEADS_CASES_1_FROM_CASES_TITLE'] = 'Leads';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID Cliente y Prospecto';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_MEMBER_OF'] = 'Cliente y Prospecto';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_INTRO'] = 'El módulo de Casos es para la gestión de problemas de compatibilidad que afecta a las Cliente y Prospectos. Utilice las flechas más abajo para una visita rápida.';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_PAGE'] = 'Esta página muestra el listado de Casos existentes asociados a su Cliente y Prospecto.';
$mod_strings['LBL_CONTACT_CASE_TITLE'] = 'Contacto-Caso:';
$mod_strings['LBL_INVITEE'] = 'Contactos';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Caso';
$mod_strings['LNK_CREATE'] = 'Nuevo Caso';
$mod_strings['LBL_MODULE_NAME'] = 'Casos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Caso';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Caso';
$mod_strings['LNK_CASE_LIST'] = 'Ver Casos';
$mod_strings['LNK_CASE_REPORTS'] = 'Ver Informes de Casos';
$mod_strings['LNK_IMPORT_CASES'] = 'Importar Casos';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Casos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Casos';
$mod_strings['LBL_CASE'] = 'Caso:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Casos';
$mod_strings['LBL_MODULE_TITLE'] = 'Casos: Inicio';
$mod_strings['LBL_SHOW_MORE'] = 'Mostrar más Casos';
$mod_strings['LNK_CREATE_WHEN_EMPTY'] = 'Crear un Caso ahora';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_FILTER'] = 'Puede filtrar el listado de Casos indicando un término de búsqueda.';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_CREATE'] = 'Si tiene un nuevo Caso de asistencia que le gustaría enviar, puede hacer clic aquí para enviarlo.';
$mod_strings['LBL_ATTACH_NOTE'] = 'Adjuntar Nota';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Incidencias';
$mod_strings['LBL_DESCRIPTION'] = 'Observación';
$mod_strings['LBL_SOURCE'] = 'Canal de Ingreso';
$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Cierre';
$mod_strings['LBL_PRIMARY_CONTACT_NAME_CONTACT_ID'] = 'Contacto principal (relacionado Contacto ID)';
$mod_strings['LBL_PRIMARY_CONTACT_NAME'] = 'Contacto principal';
$mod_strings['LBL_SASA_ID_ESTA_C'] = 'Estado PQRF';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'No. documento Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_ID_E'] = 'id E';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'PQRFS';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA_C'] = 'id esta c';
$mod_strings['LBL_NU_PLAC_VEHI_C'] = 'nu plac vehi c';
$mod_strings['LBL_CALLS_CASES_1_FROM_CALLS_TITLE'] = 'Llamadas';
$mod_strings['LBL_AAAAAAAAA'] = 'aaaaaaaaa';
$mod_strings['LBL_SASA_PQRFS_C_A'] = 'zzzzzzzzzzzzzz';
$mod_strings['LBL_SASA_PQRF_C'] = 'sasa pqrfs c';
$mod_strings['LBL_TYPE'] = 'Tipo (N/A)';
$mod_strings['LBL_SASA_AGENDARCITAS_C'] = 'Agendar Citar';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Medición comercial';
$mod_strings['LBL_SASA_LINEANOTIFICACIONASE_C'] = 'Campo espejo Linea (Notificacion Asesor)';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha de contacto';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONCS_C'] = 'Fecha inicio de gestión ';
$mod_strings['LBL_SASA_FECHAFINGESTIONCS_C'] = 'Fecha final de gestión';
$mod_strings['LBL_SASA_ESTADOCOMERCIAL_C'] = 'Estado Comercial';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Nuevo Panel 3';
$mod_strings['LBL_SASA_NOMBREREPRESENCS_C'] = 'Nombre del Representante';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Puntos de Atencin';
$mod_strings['LBL_SASA_VEHICULOS_CASES_1_FROM_SASA_VEHICULOS_TITLE'] = 'Líneas de vehículos';
$mod_strings['LBL_SASA_CENTRONEGOCIOS_C'] = 'Centro de Negocios';
$mod_strings['LBL_SASA_CENTROCOSTOS_C'] = 'Centro de Costos';
$mod_strings['LBL_SASA_ESPERA_C'] = 'Espera';
$mod_strings['LBL_COUNT_CALL_STATUS'] = 'Contador Estado Llamada';
$mod_strings['LBL_SASA_EVAL_SI_C'] = 'Eval Si';
$mod_strings['LBL_SASA_EVAL_NO_C'] = 'Eval No';
$mod_strings['LBL_SASA_EVAL_NO_CONTACTO_C'] = 'Eval No Contactado';
$mod_strings['LBL_COUNT_TASK_STATUS_C'] = 'Contador Estado Tarea';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_NAME_FIELD_TITLE'] = 'Punto de Atención';
$mod_strings['LBL_SASA_CENTROSDECOSTOS_CASES_1_NAME_FIELD_TITLE'] = 'Centro de Costos';
$mod_strings['LBL_SASA_VEHICULOS_CASES_1_NAME_FIELD_TITLE'] = 'Líneas de vehículos';
$mod_strings['LBL_RECORDDASHLETVIEW_PANEL1'] = 'PQRFS';
$mod_strings['LBL_CONTACTS_CASES_1_NAME_FIELD_TITLE'] = 'Contacto';
$mod_strings['LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE'] = 'Contacto';
$mod_strings['LBL_LEADS_CASES_1_NAME_FIELD_TITLE'] = 'Lead';

?>

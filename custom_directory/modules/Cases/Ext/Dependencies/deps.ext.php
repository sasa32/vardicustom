<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption1.php

$dependencies['Cases']['SetOption1'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","212","191","190","94","137","156","189","209","192","17","195")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","TRASLADO DE SALDO","SOLICITUD COPIA MANUAL DEL CONDUCTOR","SOLICITUD COPIA MANUAL DE GARANTIAS","ELIMINACION DE LAS BASES DE DATOS","INFORMACION DESEMPENO Y FUNCIONAMIENTO","MERCADEO","SOLICITUD COPIA DE DOCUMENTOS","TRAMITES","SOLICITUD DE CERTIFICACION","AUTORIZACION DE USO DE DATOS PERSONALES","SOLICITUD DE FICHA TECNICA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption15.php

$dependencies['Cases']['SetOption15'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"11012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","5","146","34","113","112","65","214","209","107","204","152","156","2","131","111","208","116","84","3","122","125","82","138","140","79","170","168","35","10","183")',
           'labels' => 'createList("","ACTITUD DEL PERSONAL DE SERVICIO","INSTALACIONES","CALIDAD DE PRODUCTO","FALTA DE CONOCIMIENTO DEL PRODUCTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DE LA SALA DE VENTAS","TRAMITES","EXPLICACION DE GARANTIA","TEST DRIVE","LIMPIEZA DEL VEHICULO","MERCADEO","ACCESORIOS","INCUMPLIMIENTO FECHA DE ENTREGA","FALTA DE ASESORIA","TRAMITE FINANCIERO","FALTANTES EN LA ENTREGA","DEVOLUCION DE DINERO","ACCESORIOS INSTALADOS","INCONSISTENCIAS DURANTE LA ENTREGA","INCUMPLIMIENTO DE COMPROMISOS","DEMORA EN PROCESOS DE RETOMA","INFORMACION ERRADA DEL PRODUCTO","INFORMACION SOBRE TRAMITES","DEMORA EN LA DEVOLUCION DEL DINERO","PRECIO DE RETOMA","POLITICAS FORMAS DE PAGO","CALIDAD DE PRODUCTO EN LA ENTREGA","AGILIDAD EN LA ATENCION AL CLIENTE","SEGUIMIENTO DESPUES DE LA ENTREGA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption7.php

$dependencies['Cases']['SetOption7'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($cd_area_c,"21012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","197","193","191","190","199","17","107","108")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","SOLICITUD DIAGNOSTICO DE PIEZAS","SOLICITUD DE COTIZACION","SOLICITUD COPIA MANUAL DEL CONDUCTOR","SOLICITUD COPIA MANUAL DE GARANTIAS","SOLICITUD INFORMACION DESEMPEÑO Y FUNCIO","AUTORIZACION DE USO DE DATOS PERSONALES","EXPLICACION DE GARANTIA","EXPLICACION TRAMITES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption12.php

$dependencies['Cases']['SetOption12'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","5","79","15","146","34","113","112","65","214","209","107","204","152","156","2","75","140","170","111","208","116","84","4","122","125","82","138","80","157","25","27","26","95","96","165","131","10","87","168")',
           'labels' => 'createList("","ACTITUD DEL PERSONAL DE SERVICIO","DEMORA EN LA DEVOLUCION DEL DINERO","ATENCION AL CLIENTE","INSTALACIONES","CALIDAD DE PRODUCTO","FALTA DE CONOCIMIENTO DEL PRODUCTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DE LA SALA DE VENTAS","TRAMITES","EXPLICACION DE GARANTIA","TEST DRIVE","LIMPIEZA DEL VEHICULO","MERCADEO","ACCESORIOS","CUMPLIMIENTO EN LA ENTREGA DEL VEHICULO","INFORMACION SOBRE TRAMITES","PRECIO DE RETOMA","FALTA DE ASESORIA","TRAMITE FINANCIERO","FALTANTES EN LA ENTREGA","DEVOLUCION DE DINERO","ACCESORIOS NO INSTALADOS","INCONSISTENCIAS DURANTE LA ENTREGA","INCUMPLIMIENTO DE COMPROMISOS","DEMORA EN PROCESOS DE RETOMA","INFORMACION ERRADA DEL PRODUCTO","DEMORA EN LA ENTREGA DEL VEHICULO","MOTOR","CAJA DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","INCUMPLIMIENTO FECHA DE ENTREGA","AGILIDAD EN LA ATENCION AL CLIENTE","DIFICULTAD EN RESPUESTA TELEFONICA","POLITICAS FORMAS DE PAGO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption13.php

$dependencies['Cases']['SetOption13'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","15","146","37","43","113","112","65","216","209","203","156","131","14","130","84","79","77","109","111","170","116","10","183","5","34","151","208","125","140","87")',
           'labels' => 'createList("","ATENCIÓN AL CLIENTE","INSTALACIONES","CALIDAD DE PRODUCTO POSTERIOR ENTREGA","CALIDAD DEL SERVICIO","FALTA DE CONOCIMIENTO DEL PRODUCTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DEL PUNTO DE VENTA","TRAMITES","TERMINOS DE GARANTIA","MERCADEO","INCUMPLIMIENTO FECHA DE ENTREGA"."ASESORIA E INFORMACION","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","DEVOLUCION DE DINERO","DEMORA EN LA DEVOLUCION DEL DINERO","DEMORA EN EL ENVIO DE INFORMACION","FALTA DE ACOMPANAMIENTO EN PROCESO COMPR","FALTA DE ASESORIA","PRECIO DE RETOMA","FALTANTES EN LA ENTREGA","AGILIDAD EN LA ATENCION AL CLIENTE","SEGUIMIENTO DESPUES DE LA ENTREGA","ACTITUD DEL PERSONAL DE SERVICIO","CALIDAD DE PRODUCTO","LIMPIEZA DE LA MAQUINA ","TRAMITE FINANCIERO","INCUMPLIMIENTO DE COMPROMISOS","INFORMACION SOBRE TRAMITES","DIFICULTAD EN RESPUESTA TELEFONICA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption14.php

$dependencies['Cases']['SetOption14'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","5","146","34","113","112","65","214","209","204","152","156","2","131","111","208","116","84","3","122","125","82","140","79","120","10","183","87","84","168","35","4","196","134","174","44","15","140")',
           'labels' => 'createList("","ACTITUD DEL PERSONAL DE SERVICIO","INSTALACIONES","CALIDAD DE PRODUCTO","FALTA DE CONOCIMIENTO DEL PRODUCTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DE LA SALA DE VENTAS","TRAMITES","TEST DRIVE","LIMPIEZA DEL VEHICULO","MERCADEO","ACCESORIOS","INCUMPLIMIENTO FECHA DE ENTREGA","FALTA DE ASESORIA","TRAMITE FINANCIERO","FALTANTES EN LA ENTREGA","DEVOLUCION DE DINERO","ACCESORIOS INSTALADOS","INCONSISTENCIAS DURANTE LA ENTREGA","INCUMPLIMIENTO DE COMPROMISOS","DEMORA EN PROCESOS DE RETOMA","INFORMACION SOBRE TRAMITES","DEMORA EN LA DEVOLUCION DEL DINERO","INCONFORMIDAD POR EL PRECIO DE RETOMA","AGILIDAD EN LA ATENCION AL CLIENTE","SEGUIMIENTO DESPUES DE LA ENTREGA","DIFICULTAD EN RESPUESTA TELEFONICA","DEVOLUCION DE DINERO","POLITICAS FORMAS DE PAGO","CALIDAD DE PRODUCTO EN LA ENTREGA","ACCESORIOS NO INSTALADOS","SOLICITUD DE INFORMACION","INFORMACION BRINDADA SOBRE PRODUCTO","PROGRAMACIÓN DE CITAS CALL CENTER","CALIDAD EN EL ALISTAMIENTO","ATENCION AL CLIENTE","INFORMACION SOBRE TRAMITES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption16.php

$dependencies['Cases']['SetOption16'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","15","146","42","112","65","215","107","205","2","92","29","84","79","78","5","10","111","183","113","168","140","66","87","185","186","209","156","208","3","125","196")',
           'labels' => 'createList("","ATENCION AL CLIENTE","INSTALACIONES","CALIDAD DEL REPUESTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DEL MOSTRADOR","EXPLICACION DE GARANTIA","TIEMPO DE SUMINISTRO DE RESPUESTOS","ACCESORIOS","DISPONIBILIDAD DE REPUESTOS","CALIDAD DE ACCESORIOS","DEVOLUCION DE DINERO","DEMORA EN LA DEVOLUCION DEL DINERO","DEMORA EN EL SUMINISTRO DE REPUESTOS","ACTITUD DEL PERSONAL DE SERVICIO","AGILIDAD EN LA ATENCION AL CLIENTE","FALTA DE ASESORIA","SEGUIMIENTO DESPUES DE LA ENTREGA","FALTA DE CONOCIMIENTO DEL PRODUCTO","POLITICAS FORMAS DE PAGO","INFORMACION SOBRE TRAMITES","COSTOS DE REPUESTOS","DIFICULTAD EN RESPUESTA TELEFONICA ","SERVICIO DE CAJA","SERVICIO DE CAJA","TRAMITES","MERCADEO","TRAMITE FINANCIERO","ACCESORIOS INSTALADOS","INCUMPLIMIENTO DE COMPROMISOS","SOLICITUD DE INFORMACION")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption17.php

$dependencies['Cases']['SetOption17'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","81","147","97","115","16","87","129","152","110","167","12","31","65","175","146","217","5","206","30","48","40","112","179","84","79","89","207","107","91","181","83","34","139","92","78","46","43","36","47","42","157","24","27","26","95","96","165","185","44","62","163","121","125")',
           'labels' => 'createList("","DEMORA EN LA RECEPCION DEL VEHICULO","INVENTARIO DEL VEHICULO/PERTENENCIAS DE","ERROR EN EL COSTO ESTIMADO DE REPARACION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","AUTORIZACION DE TRABAJOS ADICIONALES","DIFICULTAD EN RESPUESTA TELEFONICA","INCUMPLIMIENTO EN LA ENTREGA DEL VEHICUL","LIMPIEZA DEL VEHICULO","FALTA DE AGILIDAD PARA OTORGAR CITAS","PERMANENCIA DEL VEHICULO EN EL TALLER","AGILIDAD EN LA ENTREGA DEL VEHICULO","CALIDAD DE LA REPARACION DE LATONERIA Y","COSTOS DE LA REPARACION O SERVICIO","PROGRAMACION EMISORAS DEL RADIO","INSTALACIONES","UBICACION DEL TALLER","ACTITUD DEL PERSONAL DE SERVICIO","TIEMPO ESTIMADO DEL SERVICIO","CALIDAD DE LA REPARACION","CALIDAD EN TRABAJOS DE ELECTRICIDAD","CALIDAD DEL DIAGNOSTICO","FALTA DE COMUNICACION CON EL CLIENTE","RAYONES O GOLPES","DEVOLUCION DE DINERO","DEMORA EN LA DEVOLUCION DEl DINERO","DIFICULTAD PARA SOLICITAR CITA","TRABAJO INCOMPLETO","EXPLICACION DE GARANTIA","DISPONIBILIDAD DE CITAS PARA EL TALLER","RUIDOS","DEMORA EN TRAMITES INTERNOS","CALIDAD DE PRODUCTO","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD EN LA REPARACION DE COLISION","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","CALIDAD EN LA REV. MANTENIMIENTO PERIODI","CALIDAD DEL REPUESTO","MOTOR","CAJA DE DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","SERVICIO DE CAJA","CALIDAD EN EL ALISTAMIENTO","CONSUMO DE COMBUSTIBLE","PAGOS ELECTRONICOS","INCONSISTENCIA FACTURA","INCUMPLIMIENTO DE COMPROMISOS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption18.php

$dependencies['Cases']['SetOption18'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"21012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","81","148","97","115","16","87","129","152","110","167","80","31","65","175","146","217","5","206","30","48","40","112","178","84","79","89","207","107","91","181","83","34","139","92","78","46","43","36","47","42","157","24","27","26","95","96","165","185","44","62","163","121","125")',
           'labels' => 'createList("","DEMORA EN LA RECEPCION DEL VEHICULO","INVENTARIO,PERTENENCIAS DEL VEHICULO","ERROR EN EL COSTO ESTIMADO DE REPARACION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","AUTORIZACION DE TRABAJOS ADICIONALES","DIFICULTAD EN RESPUESTA TELEFONICA","INCUMPLIMIENTO EN LA ENTREGA DEL VEHICUL","LIMPIEZA DEL VEHICULO","FALTA DE AGILIDAD PARA OTORGAR CITAS","PERMANENCIA DEL VEHICULO EN EL TALLER","DEMORA EN LA ENTREGA DEL VEHICULO","CALIDAD DE LA REPARACION DE LATONERIA Y","COSTOS DE LA REPARACION O SERVICIO","PROGRAMACION EMISORAS DEL RADIO","INSTALACIONES","UBICACION DEL TALLER","ACTITUD DEL PERSONAL DE SERVICIO","TIEMPO ESTIMADO DEL SERVICIO","CALIDAD DE LA REPARACION","CALIDAD EN TRABAJOS DE ELECTRICIDAD","CALIDAD DEL DIAGNOSTICO","FALTA DE COMUNICACION CON EL CLIENTE","RAYONES","DEVOLUCION DE DINERO","DEMORA EN LA DEVOLUCION DEl DINERO","DIFICULTAD PARA SOLICITAR CITA","TRABAJO INCOMPLETO","EXPLICACION DE GARANTIA","DISPONIBILIDAD DE CITAS PARA EL TALLER","RUIDOS","DEMORA EN TRAMITES INTERNOS","CALIDAD DE PRODUCTO","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD EN LA REPARACION DE COLISION","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","CALIDAD EN LA REV. MANTENIMIENTO PERIODI","CALIDAD DEL REPUESTO","MOTOR","CAJA DE DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","SERVICIO DE CAJA","CALIDAD EN EL ALISTAMIENTO","CONSUMO DE COMBUSTIBLE","PAGOS ELECTRONICOS","INCONSISTENCIA FACTURA","INCUMPLIMIENTO DE COMPROMISOS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption2.php

$dependencies['Cases']['SetOption2'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($cd_area_c,"11012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","191","190","94","137","189","209")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","SOLICITUD COPIA MANUAL DEL CONDUCTOR","SOLICITUD COPIA MANUAL DE GARANTIAS","ELIMINACION DE LAS BASES DE DATOS","INFORMACION DESEMPENO Y FUNCIONAMIENTO","SOLICITUD COPIA DE DOCUMENTOS","TRAMITES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption19.php

$dependencies['Cases']['SetOption19'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"220"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","127","15","97","115","16","87","128","151","167","11","31","61","146","217","5","206",."30","48","40","114","178","126","88","84","181","79","218","107","92","43","36","220","207","78","47","42","159","185","163","121","125")',
           'labels' => 'createList("","INCUMPLIMIENTO EN LA CITA DE SERVICIO","ATENCION AL CLIENTE","ERROR EN EL COSTO ESTIMADO DE REPARACION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","AUTORIZACION DE TRABAJOS ADICIONALES","DIFICULTAD EN RESPUESTA TELEFONICA","INCUMPLIMIENTO EN LA ENTREGA","LIMPIEZA DE LA MAQUINA","PERMANENCIA DEL VEHICULO EN EL TALLER","AGILIDAD EN LA ENTREGA DE LA MAQUINA","CALIDAD DE LA REPARACION DE LATONERIA Y","COSTOS DE LA REPARACION O SERVICIO","INSTALACIONES","UBICACION DEL TALLER","ACTITUD DEL PERSONAL DE SERVICIO","TIEMPO ESTIMADO DEL SERVICIO","CALIDAD DE LA REPARACION","CALIDAD EN TRABAJOS DE ELECTRICIDAD","CALIDAD DEL DIAGNOSTICO","FALTA DE SEGUIMIENTO","RAYONES","INCUMPLIMIENTO EN ENVIO DE COTIZACION","DIFICULTAD PARA REALIZAR PAGOS","DEVOLUCION DE DINERO","RUIDOS","DEMORA EN LA DEVOLUCION DEL DINERO","VISITA DE SERVICIO","EXPLICACION DE GARANTIA","DISPONIBILIDAD DE REPUESTOS","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","DEMORA TRAMITES INTERNOS","TRABAJO INCOMPLETO","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD EN LA REV. MANTENIMIENTO PERIODICO","CALIDAD DEL REPUESTO","MOTOR, TREN DE POTENCIA","SERVICIO DE CAJA","PAGOS ELECTRONICOS","INCONSISTENCIA FACTURA","INCUMPLIMIENTO DE COMPROMISOS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption21.php

$dependencies['Cases']['SetOption21'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","134","128","4","116","35","123","130","210","34","29","80","45","84","85","209","212","113","124","208","38","53","17","125","187","44","79")',
           'labels' => 'createList("","INFORMACION BRINDADA SOBRE PRODUCTO","INCUMPLIMIENTO EN LA ENTREGA","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","CALIDAD DE PRODUCTO","CALIDAD DE ACCESORIOS","DEMORA EN LA ENTREGA DEL VEHICULO","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","DEVOLUCION SALDO A FAVOR","TRAMITES","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIAS EN PAGOS","TRAMITE FINANCIERO","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","AUTORIZACION DE USO DE DATOS PERSONALES","INCUMPLIMIENTO DE COMPROMISOS","SISTEMA DE NAVEGACION","CALIDAD EN EL ALISTAMIENTO","DEMORA EN LA DEVOLUCION DEL DINERO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption23.php

$dependencies['Cases']['SetOption23'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","157","25","26","95","96","165","162","28","202","150","117","1","201","4","116","35","123","130","210","34","84","80","85","209","107","67","134","196","125","27","83","128","29","45","212","113","208","38","17","44")',
           'labels' => 'createList("","MOTOR","CAJA DIRECCION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","NO REALIZACION TRASPASO RETOMA","CAJA, TRANSMISION Y EMBRAGUE","SUSPENSION","INYECCION,ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","CALIDAD DE PRODUCTO","DEVOLUCION DE DINERO","DEMORA EN LA ENTREGA DEL VEHICULO","DEVOLUCION SALDO A FAVOR","TRAMITES","EXPLICACION DE GARANTIA","COSTOS TRAMITES","INFORMACION BRINDADA SOBRE PRODUCTO","SOLICITUD DE INFORMACION","INCUMPLIMIENTO DE COMPROMISOS","CAJA TRANSMISION","DEMORA EN TRAMITES INTERNOS","INCUMPLIMIENTO EN LA ENTREGA","CALIDAD DE ACCESORIOS ","CALIDAD EN LA INSTALACION DE ACCESORIOS","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","TRAMITE FINANCIERO","CALIDAD DE VIDRIOS","AUTORIZACION DE USO DE DATOS PERSONALES","CALIDAD EN EL ALISTAMIENTO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption3.php

$dependencies['Cases']['SetOption3'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","139","189")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","SOLICITUD COPIA DE DOCUMENTOS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption4.php

$dependencies['Cases']['SetOption4'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","92","17","189","191","212","94","209")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","DISPONIBILIDAD DE REPUESTOS","AUTORIZACION DE USO DE DATOS PERSONALES","SOLICITUD COPIA DE DOCUMENTOS","SOLICITUD COPIA MANUAL DEL CONDUCTOR","TRASLADO DE SALDO ","ELIMINACION DE LAS BASES DE DATOS","TRAMITES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption6.php

$dependencies['Cases']['SetOption6'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","197","199","193","191","190","17","107","108")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","SOLICITUD DIAGNOSTICO DE PIEZAS","SOLICITUD INFORMACION DESEMPEÑO Y FUNCIO","SOLICITUD DE COTIZACION","SOLICITUD COPIA MANUAL DEL CONDUCTOR","SOLICITUD COPIA MANUAL DE GARANTIAS","AUTORIZACION DE USO DE DATOS PERSONALES","EXPLICACION DE GARANTIA","EXPLICACION TRAMITES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption20.php

$dependencies['Cases']['SetOption20'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"2144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","81","147","97","115","16","87","129","152","167","12","31","65","175","146","217","5","206","30","40","112","179","84","79","89","207","107","91","181","83","34","139","43","36","157","24","27","26","95","96","165","44","62","110","92","78","46","47","42","163","121","125")',
           'labels' => 'createList("","DEMORA EN LA RECEPCION DEL VEHICULO","INVENTARIO DEL VEHICULO/PERTENENCIAS DE","ERROR EN EL COSTO ESTIMADO DE REPARACION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","AUTORIZACION DE TRABAJOS ADICIONALES","DIFICULTAD EN RESPUESTA TELEFONICA","INCUMPLIMIENTO EN LA ENTREGA DEL VEHICUL","LIMPIEZA DEL VEHICULO","PERMANENCIA DEL VEHICULO EN EL TALLER","AGILIDAD EN LA ENTREGA DEL VEHICULO","CALIDAD DE LA REPARACION DE LATONERIA Y","COSTOS DE LA REPARACION O SERVICIO","PROGRAMACION EMISORAS DEL RADIO","INSTALACIONES","UBICACION DEL TALLER","ACTITUD DEL PERSONAL DE SERVICIO","TIEMPO ESTIMADO DEL SERVICIO","CALIDAD DE LA REPARACION","CALIDAD DEL DIAGNOSTICO","FALTA DE COMUNICACION CON EL CLIENTE","RAYONES O GOLPES","DEVOLUCION DE DINERO","DEMORA EN LA DEVOLUCION DEL DINERO","DIFICULTAD PARA SOLICITAR CITA","TRABAJO INCOMPLETO","EXPLICACION DE GARANTIA","DISPONIBILIDAD DE CITAS PARA EL TALLER","RUIDOS","DEMORA EN TRAMITES INTERNOS","CALIDAD DE PRODUCTO","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","MOTOR","CAJA DE DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","CALIDAD EN EL ALISTAMIENTO","CONSUMO DE COMBUSTIBLE","FALTA DE AGILIDAD PARA OTORGAR CITAS","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD EN LA REPARACION DE COLISION","CALIDAD EN LA REV. MANTENIMIENTO PERIODI","CALIDAD DEL REPUESTO","PAGOS ELECTRONICOS","INCONSISTENCIA FACTURA","INCUMPLIMIENTO DE COMPROMISOS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption5.php

$dependencies['Cases']['SetOption5'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","132","189"."212","195","198","191","190","94","137","156","209","193","192","17")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","INF. HISTORIAL SERV. FACTURAS VENTA ETC","SOLICITUD COPIA DE DOCUMENTOS","TRASLADO DE SALDO","SOLICITUD DE FICHA TECNICA","SOLICITUD INFO TRASPASO VEHICULO RETOMA","SOLICITUD COPIA MANUAL DEL CONDUCTOR"."SOLICITUD COPIA MANUAL DE GARANTIAS","ELIMINACION DE LAS BASES DE DATOS","INFORMACION DESEMPENO Y FUNCIONAMIENTO","MERCADEO","TRAMITES","SOLICITUD DE COTIZACION","SOLICITUD DE CERTIFICACION","AUTORIZACION DE USO DE DATOS PERSONALES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_TOS_NA_SOL_6.php

$dependencies['Cases']['COM_TOS_NA_SOL_6'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"130"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","8")',
				'labels' => 'createList("","ACTUALIZACION DE DATOS")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption11.php

$dependencies['Cases']['SetOption11'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","15","146","41","113","112","65","214","209","107","204","152","156","2","129","111","208","116","86","4","122","125","82","138","140","5","10","183","170","79")',
           'labels' => 'createList("","ATENCION AL CLIENTE","INSTALACIONES","CALIDAD DEL PRODUCTO POSTERIOR ENTREGA","FALTA DE CONOCIMIENTO DEL PRODUCTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DE LA SALA DE VENTAS","TRAMITES","EXPLICACION DE GARANTIA","TEST DRIVE","LIMPIEZA DEL VEHICULO","MERCADEO","ACCESORIOS","INCUMPLIMIENTO EN LA ENTREGA DEL VEHICUL","FALTA DE ASESORIA","TRAMITE FINANCIERO","FALTANTES EN LA ENTREGA","DEVOLUCION DE DINERO","ACCESORIOS NO INSTALADOS","INCONSISTENCIAS DURANTE LA ENTREGA","INCUMPLIMIENTO DE COMPROMISOS","DEMORA EN PROCESOS DE RETOMA","INFORMACION ERRADA DEL PRODUCTO","INFORMACION SOBRE TRAMITES","ACTITUD DEL PERSONAL DE SERVICIO","AGILIDAD EN LA ATENCION AL CLIENTE","SEGUIMIENTO DESPUES DE LA ENTREGA","PRECIO DE RETOMA","DEMORA EN LA DEVOLUCION DEL DINERO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption22.php

$dependencies['Cases']['SetOption22'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"11012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","28","202","150","117","1","201","4","116","35","123","130","210","34","29","80","106","46","84","85","209","134","128")',
           'labels' => 'createList("","CAJA, TRANSMISION Y EMBRAGUE","SUSPENSION","INYECCION,ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","CALIDAD DE PRODUCTO","CALIDAD DE ACCESORIOS","DEMORA EN LA ENTREGA DEL VEHICULO","ESTATICA","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","DEVOLUCION SALDO A FAVOR","TRAMITES","INFORMACION BRINDADA SOBRE PRODUCTO","INCUMPLIMIENTO EN LA ENTREGA","EXPLICACION DE GARANTIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption8.php

$dependencies['Cases']['SetOption8'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"220"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","199","17","107","108")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","SOLICITUD INFORMACION DESEMPEÑO Y FUNCIO","AUTORIZACION DE USO DE DATOS PERSONALES","EXPLICACION DE GARANTIA","EXPLICACION TRAMITES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption9.php

$dependencies['Cases']['SetOption9'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","139","132","84","197","199","193","191","190","17","107","108")',
           'labels' => 'createList("","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","INF. HISTORIAL SERV. FACTURAS VENTA ETC","DEVOLUCION DE DINERO","SOLICITUD DIAGNOSTICO DE PIEZAS","SOLICITUD INFORMACION DESEMPEÑO Y FUNCIO","SOLICITUD DE COTIZACION","SOLICITUD COPIA MANUAL DEL CONDUCTOR","SOLICITUD COPIA MANUAL DE GARANTIAS","AUTORIZACION DE USO DE DATOS PERSONALES","EXPLICACION DE GARANTIA","EXPLICACION TRAMITES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_DOS_NA_SOL_11.php

$dependencies['Cases']['COM_DOS_NA_SOL_11'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","57","73","204","8","194","213","60","149")',
				'labels' => 'createList("","PAGOS PSE","CITA EN VITRINA","COTIZACIÓN VEHÍCULO USADO","TEST DRIVE","ACTUALIZACION DE DATOS","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","COMUNICADOS INTERNOS","INVESTIGACIONES DE MERCADO")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/DIN_RIA_NA_SOL_4.php

$dependencies['Cases']['DIN_RIA_NA_SOL_4'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","70","57","8","194","213","60","6","149")',
				'labels' => 'createList("","PAGOS PSE","COTIZACIÓN MAQUINARIA","CITA EN VITRINA","ACTUALIZACION DE DATOS","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)","INVESTIGACIONES DE MERCADO")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_NA_VEH_INF_3.php

$dependencies['Cases']['COM_NA_VEH_INF_3'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','cd_area_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","52","141","99","101","144","184","90","103","9","136","177","164","154","172")',
				'labels' => 'createList("","CHANGAN","INFORMACIÓN VEHICULO NUEVO","ESPECIFICACIONES VEHÍCULO NUEVO","ESTADO DEL NEGOCIO","INFORMACIÓN VITRINAS, CAMPAÑAS, ASESORES, LANZAMIENTOS Y RETOMAS","SEGUROS MILENIO","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","PROPUESTA COMERCIAL","PAGOS PSE","MANTENIMIENTO PREVENTIVO","PRECIOS RMP")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_NA_VEH_OTR_4.php

$dependencies['Cases']['COM_NA_VEH_OTR_4'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"),equal($sasa_tipo_c,"G"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','cd_area_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","23","211")',
				'labels' => 'createList("","CAIDA LLAMADA","TRANSFERENCIA DE LLAMADA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_NA_VEH_SOL_5.php

$dependencies['Cases']['COM_NA_VEH_SOL_5'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','cd_area_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","57","74","204","8","49","194","213","59","60","6","149")',
				'labels' => 'createList("","PAGOS PSE","CITA EN VITRINA","COTIZACIÓN VEHÍCULOS NUEVOS","TEST DRIVE","ACTUALIZACION DE DATOS","CAMBIO DE PROPIETARIO","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","CITA REVISIÓN TALLER","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)","INVESTIGACIONES DE MERCADO")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_RIZ_NA_INF_7.php

$dependencies['Cases']['COM_RIZ_NA_INF_7'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"210"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","154")',
				'labels' => 'createList("","MANTENIMIENTO PREVENTIVO")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_SAN_NA_SOL_8.php

$dependencies['Cases']['COM_SAN_NA_SOL_8'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"110"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","204","8")',
				'labels' => 'createList("","TEST DRIVE","ACTUALIZACION DE DATOS")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/DIN_RIA_NA_INF_2.php

$dependencies['Cases']['DIN_RIA_NA_INF_2'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","98","101","144","","103","9","136","155")',
				'labels' => 'createList("","ESPECIFICACIONES MAQUINARIA","ESTADO DEL NEGOCIO","INFORMACIÓN VITRINAS, CAMPAÑAS, ASESORES, LANZAMIENTOS Y RETOMAS","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","MAQUINARIA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_RIA_NA_SOL_2.php

$dependencies['Cases']['COM_RIA_NA_SOL_2'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"120"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","8")',
				'labels' => 'createList("","ACTUALIZACION DE DATOS")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/DIN_RIA_NA_OTR_3.php

$dependencies['Cases']['DIN_RIA_NA_OTR_3'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($sasa_tipo_c,"G"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","23","211")',
				'labels' => 'createList("","CAIDA LLAMADA","TRANSFERENCIA DE LLAMADA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/DIN_SAN_NA_OTR_9.php

$dependencies['Cases']['DIN_SAN_NA_OTR_9'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($sasa_tipo_c,"G"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","23","211")',
				'labels' => 'createList("","CAIDA LLAMADA","TRANSFERENCIA DE LLAMADA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/DIN_SAN_NA_SOL_10.php

$dependencies['Cases']['DIN_SAN_NA_SOL_10'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","57","74","204","8","49","194","213","60","6","149")',
				'labels' => 'createList("","PAGOS PSE","CITA EN VITRINA","COTIZACIÓN VEHÍCULOS NUEVOS","TEST DRIVE","ACTUALIZACION DE DATOS","CAMBIO DE PROPIETARIO","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)","INVESTIGACIONES DE MERCADO")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/DIN_TOS_NA_INF_5.php

$dependencies['Cases']['DIN_TOS_NA_INF_5'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","102","176","182","90","103","9","136")',
				'labels' => 'createList("","ESTADO DEL PEDIDO DEL REPUESTO","PROMOCIÓN MES","SEGUIMIENTO A GUIAS DESPACHOS","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/DIN_TOS_NA_OTR_7.php

$dependencies['Cases']['DIN_TOS_NA_OTR_7'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($sasa_tipo_c,"G"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","23","211")',
				'labels' => 'createList("","CAIDA LLAMADA","TRANSFERENCIA DE LLAMADA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/DIN_TOS_NA_SOL_6.php

$dependencies['Cases']['DIN_TOS_NA_SOL_6'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","71","69","86","8","194","213","60","6","149")',
				'labels' => 'createList("","PAGOS PSE","COTIZACIÓN REPUESTOS","COTIZACIÓN ACCESORIOS","DEVOLUCIONES DE REPUESTOS","ACTUALIZACION DE DATOS","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)","INVESTIGACIONES DE MERCADO")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_DOS_NA_OTR_10.php

$dependencies['Cases']['COM_DOS_NA_OTR_10'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($sasa_tipo_c,"G"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","23","211")',
				'labels' => 'createList("","CAIDA LLAMADA","TRANSFERENCIA DE LLAMADA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/DIN_SAN_NA_INF_8.php

$dependencies['Cases']['DIN_SAN_NA_INF_8'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","141","99","101","184","90","103","9","177","164","164","164","136","144)',
				'labels' => 'createList("","INFORMACIÓN VEHICULO NUEVO","ESPECIFICACIONES VEHÍCULO NUEVO","ESTADO DEL NEGOCIO","SEGUROS MILENIO","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","PROPUESTA COMERCIAL","PAGOS PSE","PAGOS PSE","PAGOS PSE","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","INFORMACIÓN VITRINAS, CAMPAÑAS, ASESORES, LANZAMIENTOS Y RETOMAS")'
			),
		),
	),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption10.php

$dependencies['Cases']['SetOption10'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","193","195","189","198","5","192","17")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","SOLICITUD DE COTIZACION","SOLICITUD DE FICHA TECNICA","SOLICITUD COPIA DE DOCUMENTOS","SOLICITUD INFO TRASPASO VEHICULO RETOMA","ACTITUD DEL PERSONAL DE SERVICIO","SOLICITUD DE CERTIFICACION","AUTORIZACION DE USO DE DATOS PERSONALES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption39.php

$dependencies['Cases']['SetOption39'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_tipo_NA.php

$dependencies['Cases']['SetOption_tipo_NA'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_tipo_c,"NA")',
   'triggerFields' => array('sasa_tipo_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","221","222","226")',
           'labels' => 'createList("","PROTOCOLO VITRINA","PROTOCOLO TALLER","SEGUIMIENTO PQRFS A REPRESENTANTES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia05.php

$dependencies['Cases']['SetOption_Sugerencia05'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Felicitaciones02.php

$dependencies['Cases']['SetOption_Felicitaciones02'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"21012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Felicitaciones01.php

$dependencies['Cases']['SetOption_Felicitaciones01'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"11012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption29.php

$dependencies['Cases']['SetOption29'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"21012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","158","202","93","117","1","201","4","181","34","30","40","65","179","16","45","84","29","106","167","35","91","107","116","152","147","85","92","78","32","42","38","53","47","17","46","115","43","36","207","39","83","118","18","125","157","24","27","26","95","96","165","62","163","209","128","123","80","212","113","121","112")',
           'labels' => 'createList("","MOTOR, CAJA, TRANSMISION Y EMBRAGUE","SUSPENSION","ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","RUIDOS","CALIDAD DE PRODUCTO","CALIDAD DE LA REPARACION","CALIDAD DEL DIAGNOSTICO","COSTOS DE LA REPARACION O SERVICIO","RAYONES O GOLPES","AUTORIZACION DE TRABAJOS ADICIONALES","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","CALIDAD DE ACCESORIOS","ESTATICA","PERMANENCIA DEL VEHICULO EN EL TALLER","CALIDAD DE PRODUCTO EN LA ENTREGA","DISPONIBILIDAD DE CITAS PARA EL TALLER","EXPLICACION DE GARANTIA","FALTANTES EN LA ENTREGA","LIMPIEZA DEL VEHICULO","INVENTARIO DEL VEHICULO/PERTENENCIAS DE","DEVOLUCION SALDO A FAVOR","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD DE LLANTAS","CALIDAD DEL REPUESTO","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","CALIDAD EN LA REV. MANTENIMIENTO PERIODI","AUTORIZACION DE USO DE DATOS PERSONALES","CALIDAD EN LA REPARACION DE COLISION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","TRABAJO INCOMPLETO","CALIDAD DEL AIRE ACONDICIONADO","DEMORA EN TRAMITES FINTERNOS","FUNCIONAMIENTO RADIO/SISTEMA DE AUDIO","BOLSA DE AIRE","INCUMPLIMIENTO DE COMPROMISOS","MOTOR","CAJA DE DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","CONSUMO DE COMBUSTIBLE","PAGOS ELECTRONICOS","TRAMITES","INCUMPLIMIENTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","DEMORA EN LA ENTREGA DEL VEHICULO","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIA FACTURA","FALTA DE COMUNICACIÓN CON EL CLIENTE")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption30.php

$dependencies['Cases']['SetOption30'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"220"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","160","202","188","1","201","153","19","4","181","34","30","40","65","121","64","207","166","107","43","36","179","16","84","116","151","85","92","78","42","17","115","83","125","62","163","209","112")',
           'labels' => 'createList("","MOTOR,TREN DE POTENCIA","SUSPENSION","SISTEMA HIDRAULICO","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","MANDOS","BOMBAS, CILINDROS","ACCESORIOS NO INSTALADOS","RUIDOS","CALIDAD DE PRODUCTO","CALIDAD DE LA REPARACION","CALIDAD DEL DIAGNOSTICO","COSTOS DE LA REPARACION O SERVICIO","INCONSISTENCIA FACTURA","COSTOS CARGADOS NO AUTORIZADOS","TRABAJO INCOMPLETO","PERMANENCIA DE LA MAQUINA EN EL TALLER","EXPLICACION DE GARANTIA","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","RAYONES O GOLPES","AUTORIZACION DE TRABAJOS ADICIONALES","DEVOLUCION DE DINERO","FALTANTES EN LA ENTREGA","LIMPIEZA DE LA MAQUINA","DEVOLUCION SALDO A FAVOR","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD DEL REPUESTO","AUTORIZACION DE USO DE DATOS PERSONALES","FALTA EXPLICACION DEL SERVICIO EFECTUADO","DEMORA EN TRAMITES INTERNOS","INCUMPLIMIENTO DE COMPROMISOS","CONSUMO DE COMBUSTIBLE","PAGOS ELECTRONICOS","TRAMITES","FALTA DE COMUNICACIÓN CON EL CLIENTE")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOptionPQRFS.php

$dependencies['Cases']['SetOptionPQRFS'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_pqrf_c,"N"))',
   'triggerFields' => array('sasa_pqrf_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'source',
           'keys' => 'createList("","C","E","V","T","W","P")',
           'labels' => 'createList("","CARTA","EMAIL","FORMATO VERBAL","LLAMADA","SITIO WEB","WHATSAPP")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia03.php

$dependencies['Cases']['SetOption_Sugerencia03'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"340"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Felicitaciones03.php

$dependencies['Cases']['SetOption_Felicitaciones03'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"340"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia10.php

$dependencies['Cases']['SetOption_Sugerencia10'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia09.php

$dependencies['Cases']['SetOption_Sugerencia09'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_tipo_NA_y_compania_DNisan.php

$dependencies['Cases']['SetOption_tipo_NA_y_compania_DNisan'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"NA"),equal($cd_cia_c,"1"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","227","233")',
           'labels' => 'createList("","INVITACIÓN DE VITRINAS A NIVEL NACIONAL","ENCUESTAS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_NoVigente01.php

$dependencies['Cases']['SetOption_NoVigente01'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"340"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","189","17")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","SOLICITUD COPIA DE DOCUMENTOS","AUTORIZACION DE USO DE DATOS PERSONALES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_NoVigente02.php

$dependencies['Cases']['SetOption_NoVigente02'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"340"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","15","146","41","113","112","65","214","209","107","204","152","156","2","129","111","208","116","84","4","122","125","82","138","140","84","5","10","183")',
           'labels' => 'createList("","ATENCION AL CLIENTE","INSTALACIONES","CALIDAD DEL PRODUCTO POSTERIOR ENTREGA","FALTA DE CONOCIMIENTO DEL PRODUCTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DE LA SALA DE VENTAS","TRAMITES","EXPLICACION DE GARANTIA","TEST DRIVE","LIMPIEZA DEL VEHICULO","MERCADEO","ACCESORIOS","INCUMPLIMIENTO EN LA ENTREGA DEL VEHICUL","FALTA DE ASESORIA","TRAMITE FINANCIERO","FALTANTES EN LA ENTREGA","DEVOLUCION DE DINERO","ACCESORIOS NO INSTALADOS","INCONSISTENCIAS DURANTE LA ENTREGA","INCUMPLIMIENTO DE COMPROMISOS","DEMORA EN PROCESOS DE RETOMA","INFORMACION ERRADA DEL PRODUCTO","INFORMACION SOBRE TRAMITES","DEVOLUCION DE DINERO","ACTITUD DEL PERSONAL DE SERVICIO","AGILIDAD EN LA ATENCION AL CLIENTE","SEGUIMIENTO DESPUES DE LA ENTREGA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_NoVigente03.php

$dependencies['Cases']['SetOption_NoVigente03'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"340"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","28","202","150","117","1","201","4","116","34","123","130","210","84","209","80","35","38","53","17","134")',
           'labels' => 'createList("","CAJA, TRANSMISION Y EMBRAGUE","SUSPENSION","INYECCION,ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","DEVOLUCION DE DINERO","TRAMITES","DEMORA EN LA ENTREGA DEL VEHICULO","CALIDAD DE PRODUCTO EN LA ENTREGA","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","AUTORIZACION DE USO DE DATOS PERSONALES","INFORMACION BRINDADA SOBRE PRODUCTO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia02.php

$dependencies['Cases']['SetOption_Sugerencia02'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"11012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia04.php

$dependencies['Cases']['SetOption_Sugerencia04'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption24.php

$dependencies['Cases']['SetOption24'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"315"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","209")',
           'labels' => 'createList("","TRAMITES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption31.php

$dependencies['Cases']['SetOption31'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"240"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","34","38","53","201","167","43","36","83","202","93","117","1","4","181","40","65","179","16","45","84","29","106","35","91","107","116","152","147","85","92","78","32","42","47","17","46","115","207","39","118","18","125","157","24","27","26","95","96","165","62","163","209","128","123","80","212","113","121","112")',
           'labels' => 'createList("","CALIDAD DE PRODUCTO","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","SUMINISTRO DE REPUESTOS","PERMANENCIA DEL VEHICULO EN EL TALLER","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","DEMORA EN TRAMITES INTERNOS","SUSPENSION","ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","ACCESORIOS NO INSTALADOS","RUIDOS","CALIDAD DEL DIAGNOSTICO","COSTOS DE LA REPARACION O SERVICIO","RAYONES O GOLPES","AUTORIZACION DE TRABAJOS ADICIONALES","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","CALIDAD DE ACCESORIOS","ESTATICA","CALIDAD DE PRODUCTO EN LA ENTREGA","DISPONIBILIDAD DE CITAS PARA EL TALLER","EXPLICACION DE GARANTIA","FALTANTES EN LA ENTREGA","LIMPIEZA DEL VEHICULO","INVENTARIO DEL VEHICULO/PERTENENCIAS DE","DEVOLUCION SALDO A FAVOR","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD DE LLANTAS","CALIDAD DEL REPUESTO","CALIDAD EN LA REV. MANTENIMIENTO PERIODI","AUTORIZACION DE USO DE DATOS PERSONALES","CALIDAD EN LA REPARACION DE COLISION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","TRABAJO INCOMPLETO","CALIDAD DEL AIRE ACONDICIONADO","FUNCIONAMIENTO RADIO/SISTEMA DE AUDIO","BOLSA DE AIRE","INCUMPLIMIENTO DE COMPROMISOS","MOTOR","CAJA DE DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","CONSUMO DE COMBUSTIBLE","PAGOS ELECTRONICOS","TRAMITES","INCUMPLIMIENTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","DEMORA EN LA ENTREGA DEL VEHICULO","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIA FACTURA","FALTA DE COMUNICACIÓN CON EL CLIENTE")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption28.php

$dependencies['Cases']['SetOption28'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","158","202","93","117","1","201","4","181","34","30","40","65","179","16","45","84","29","106","167","35","91","107","116","152","147","85","92","78","32","42","38","53","47","17","46","115","43","36","207","39","83","118","18","187","125","157","24","27","26","95","96","165","44","62","163","209","128","123","80","212","113","121","112")',
           'labels' => 'createList("","MOTOR, CAJA, TRANSMISION Y EMBRAGUE","SUSPENSION","ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","RUIDOS","CALIDAD DE PRODUCTO","CALIDAD DE LA REPARACION","CALIDAD DEL DIAGNOSTICO","COSTOS DE LA REPARACION O SERVICIO","RAYONES O GOLPES","AUTORIZACION DE TRABAJOS ADICIONALES","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","CALIDAD DE ACCESORIOS","ESTATICA","PERMANENCIA DEL VEHICULO EN EL TALLER","CALIDAD DE PRODUCTO EN LA ENTREGA","DISPONIBILIDAD DE CITAS PARA EL TALLER","EXPLICACION DE GARANTIA","FALTANTES EN LA ENTREGA","LIMPIEZA DEL VEHICULO","INVENTARIO DEL VEHICULO/PERTENENCIAS DE","DEVOLUCION SALDO A FAVOR","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD DE LLANTAS","CALIDAD DEL REPUESTO","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","CALIDAD EN LA REV. MANTENIMIENTO PERIODI","AUTORIZACION DE USO DE DATOS PERSONALES","CALIDAD EN LA REPARACION DE COLISION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","TRABAJO INCOMPLETO","CALIDAD DEL AIRE ACONDICIONADO","DEMORA EN TRAMITES FINTERNOS","FUNCIONAMIENTO RADIO/SISTEMA DE AUDIO","BOLSA DE AIRE","SISTEMA DE NAVEGACION","INCUMPLIMIENTO DE COMPROMISOS","MOTOR","CAJA DE DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","CALIDAD EN EL ALISTAMIENTO","CONSUMO DE COMBUSTIBLE","PAGOS ELECTRONICOS","TRAMITES","INCUMPLIMIENTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","DEMORA EN LA ENTREGA DEL VEHICULO","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIA FACTURA","FALTA DE COMUNICACIÓN CON EL CLIENTE")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption27.php

$dependencies['Cases']['SetOption27'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","160","202","188","1","201","153","19","4","116","35","123","130","210","34","84","85","134","128","209","113","124","208","17","125","44")',
           'labels' => 'createList("","MOTOR,TREN DE POTENCIA","SUSPENSION","SISTEMA HIDRAULICO","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","MANDOS","BOMBAS, CILINDROS","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","CALIDAD DE PRODUCTO","DEVOLUCION DE DINERO","DEVOLUCION SALDO A FAVOR","INFORMACION BRINDADA SOBRE PRODUCTO","INCUMPLIMIENTO EN LA ENTREGA","TRAMITES","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIAS EN PAGOS","TRAMITE FINANCIERO","AUTORIZACION DE USO DE DATOS PERSONALES","INCUMPLIMIENTO DE COMPROMISOS","CALIDAD EN EL ALISTAMIENTO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption33.php

$dependencies['Cases']['SetOption33'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_tipo_NA_y_compania_DNissan_o_TAutorizados.php

$dependencies['Cases']['SetOption_tipo_NA_y_compania_DNissan_o_TAutorizados'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"NA"),or(equal($cd_cia_c,"1"),equal($cd_cia_c,"2")))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","233")',
           'labels' => 'createList("","ENCUESTAS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption37.php

$dependencies['Cases']['SetOption37'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"220"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/TAL_RIZ_NA_SOL_6.php

$dependencies['Cases']['TAL_RIZ_NA_SOL_6'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","8","49","194","213","51","54","55","56","58","59","61","145","180","60","6","7","149","76")',
				'labels' => 'createList("","PAGOS PSE","ACTUALIZACION DE DATOS","CAMBIO DE PROPIETARIO","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","CANCELACIÓN CITA","CITA CAMPAÑA DE SEGURIDAD","CITA CAMPAÑA DE SERVICIO","CITA CARRO TALLER","CITA GARANTÍA","CITA REVISIÓN TALLER","CONFIRMACIÓN CITA TALLER","INGRESO POR COLISIÓN","REPROGRAMACIÓN CITA","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)","ACTIVACIÓN TASA","INVESTIGACIONES DE MERCADO","CURSO MECÁNICA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption38.php

$dependencies['Cases']['SetOption38'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"310"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/TAL_TOS_NA_SOL_3.php

$dependencies['Cases']['TAL_TOS_NA_SOL_3'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"130"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","8")',
				'labels' => 'createList("","ACTUALIZACION DE DATOS")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia06.php

$dependencies['Cases']['SetOption_Sugerencia06'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia01.php

$dependencies['Cases']['SetOption_Sugerencia01'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_tipo_SOLICITUD_y_compania_DNisan.php

$dependencies['Cases']['SetOption_tipo_SOLICITUD_y_compania_DNisan'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"F"),equal($cd_cia_c,"1"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","229","230","228")',
           'labels' => 'createList("","EVENTO VITRINAS O TALLERES","LANZAMIENTO PRODUCTOS","INVITACIÓN A PERIODISTAS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption26.php

$dependencies['Cases']['SetOption26'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","201","13","123","130","210","42","85","78","107","84","33","209","92","134","17","32","191")',
           'labels' => 'createList("","SUMINISTRO DE REPUESTOS","ALTO COSTO EN LOS REPUESTOS","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","CALIDAD DEL REPUESTO","DEVOLUCION SALDO A FAVOR","DEMORA EN EL SUMINISTRO DE REPUESTOS","EXPLICACION DE GARANTIA","DEVOLUCION DE DINERO","CALIDAD DE LOS ACCESORIOS","TRAMITES","DISPONIBILIDAD DE REPUESTOS","INFORMACION BRINDADA SOBRE PRODUCTO","AUTORIZACION DE USO DE DATOS PERSONALES","CALIDAD DE LLANTAS","SOLICITUD COPIA MANUAL DEL CONDUCTOR")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia07.php

$dependencies['Cases']['SetOption_Sugerencia07'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"21012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/TAL_RIA_NA_OTR_9.php

$dependencies['Cases']['TAL_RIA_NA_OTR_9'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"220"),equal($sasa_tipo_c,"G"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","23","211")',
				'labels' => 'createList("","CAIDA LLAMADA","TRANSFERENCIA DE LLAMADA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/TAL_RIA_NA_INF_7.php

$dependencies['Cases']['TAL_RIA_NA_INF_7'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"220"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","103","136","164","155")',
				'labels' => 'createList("","ESTADO PQRFS","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","PAGOS PSE","MAQUINARIA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/Subunidad02.php

$dependencies['Cases']['Subunidad02'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"))',
   'triggerFields' => array('cd_cia_c','cd_uneg_cont_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'cd_area_c',
           'keys' => 'createList("","3144")',
           'labels' => 'createList("","VEHICULOS NUEVOS CHANGAN")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/Subunidad03.php

$dependencies['Cases']['Subunidad03'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"))',
   'triggerFields' => array('cd_cia_c','cd_uneg_cont_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'cd_area_c',
           'keys' => 'createList("","11012")',
           'labels' => 'createList("","BUSES Y CAMIONES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_tipo_SOLICITUD_y_compania_DNisan_o_CAutomotriz.php

$dependencies['Cases']['SetOption_tipo_SOLICITUD_y_compania_DNisan_o_CAutomotriz'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"F"),equal($cd_cia_c,"3"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","228")',
           'labels' => 'createList("","INVITACIÓN A PERIODISTAS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/TAL_RIA_NA_SOL_2.php

$dependencies['Cases']['TAL_RIA_NA_SOL_2'] = array(
    'hooks' => array("all"),
    'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"120"),equal($sasa_tipo_c,"F"))',
    'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'sasa_motivo_c',
                'keys' => 'createList("","8")',
                'labels' => 'createList("","ACTUALIZACION DE DATOS")'
            ),
        ),
    ),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption32.php

$dependencies['Cases']['SetOption32'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"2144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","158","202","93","117","1","78","4","181","34","30","40","65","179","16","45","84","29","167","107","116","152","147","85","92","78","32","42","38","53","47","17","43","36","39","157","24","27","26","95","96","165","44","62","18","118","106","35","91","46","115","207","83","187","125","163","209","128","123","80","212","113","121","112")',
           'labels' => 'createList("","MOTOR, CAJA, TRANSMISION Y EMBRAGUE","SUSPENSION","ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","RUIDOS","CALIDAD DE PRODUCTO","CALIDAD DE LA REPARACION","CALIDAD DEL DIAGNOSTICO","COSTOS DE LA REPARACION O SERVICIO","RAYONES O GOLPES","AUTORIZACION DE TRABAJOS ADICIONALES","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","CALIDAD DE ACCESORIOS","PERMANENCIA DEL VEHICULO EN EL TALLER","EXPLICACION DE GARANTIA","FALTANTES EN LA ENTREGA","LIMPIEZA DEL VEHICULO","INVENTARIO DEL VEHICULO/PERTENENCIAS DE","DEVOLUCION SALDO A FAVOR","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD DE LLANTAS","CALIDAD DEL REPUESTO","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","CALIDAD EN LA REV. MANTENIMIENTO PERIODI","AUTORIZACION DE USO DE DATOS PERSONALES","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","CALIDAD DEL AIRE ACONDICIONADO","MOTOR","CAJA DE DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","CALIDAD EN EL ALISTAMIENTO","CONSUMO DE COMBUSTIBLE","BOLSA DE AIRE","FUNCIONAMIENTO RADIO/SISTEMA DE AUDIO","ESTATICA","CALIDAD DE PRODUCTO EN LA ENTREGA","DISPONIBILIDAD DE CITAS PARA EL TALLER","CALIDAD EN LA REPARACION DE COLISION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","TRABAJO INCOMPLETO","DEMORA EN TRAMITES INTERNOS","SISTEMA DE NAVEGACION","INCUMPLIMIENTO DE COMPROMISOS","PAGOS ELECTRONICOS","TRAMITES","INCUMPLIMIENTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","DEMORA EN LA ENTREGA DEL VEHICULO","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIA FACTURA","FALTA DE COMUNICACIÓN CON EL CLIENTE")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption34.php

$dependencies['Cases']['SetOption34'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption25.php

$dependencies['Cases']['SetOption25'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","201","4","116","34","123","130","210","84","209","80","35","85","134","128","29","212","113","124","208","17","125","44","187")',
           'labels' => 'createList("","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","DEVOLUCION DE DINERO","TRAMITES","DEMORA EN LA ENTREGA DEL VEHICULO","CALIDAD DE PRODUCTO EN LA ENTREGA","DEVOLUCION SALDO A FAVOR","INFORMACION BRINDADA SOBRE PRODUCTO","INCUMPLIMIENTO EN LA ENTREGA","CALIDAD DE ACCESORIOS","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIAS EN PAGOS","TRAMITE FINANCIERO","AUTORIZACION DE USO DE DATOS PERSONALES","INCUMPLIMIENTO DE COMPROMISOS","CALIDAD EN EL ALISTAMIENTO","SISTEMA DE NAVEGACION")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/TAL_RIA_NA_SOL_8.php

$dependencies['Cases']['TAL_RIA_NA_SOL_8'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"220"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","8","49","194","213","60","6")',
				'labels' => 'createList("","ACTUALIZACION DE DATOS","CAMBIO DE PROPIETARIO","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/TAL_RIZ_NA_INF_4.php

$dependencies['Cases']['TAL_RIZ_NA_INF_4'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","101","90","103","9","136","177","135","104","105","50","119","154","169","172","173","200")',
				'labels' => 'createList("","ESTADO DEL NEGOCIO","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","PROPUESTA COMERCIAL","INFORMACIÓN CAMPAÑA DE SEGURIDAD","ESTADO VEHÍCULO EN COLISIÓN","ESTADO VEHÍCULO EN TALLER","CAMPAÑA DE SERVICIO","HISTORIAL DE SERVICIO","MANTENIMIENTO PREVENTIVO","POLIZA","PRECIOS RMP","PREPAGADOS","SOPORTE TECNICO")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/TAL_RIZ_NA_OTR_5.php

$dependencies['Cases']['TAL_RIZ_NA_OTR_5'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($sasa_tipo_c,"G"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","23","211")',
				'labels' => 'createList("","CAIDA LLAMADA","TRANSFERENCIA DE LLAMADA")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/TAL_SAN_NA_SOL_10.php

$dependencies['Cases']['TAL_SAN_NA_SOL_10'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"110"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","8")',
				'labels' => 'createList("","ACTUALIZACION DE DATOS")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/COM_DOS_NA_INF_9.php

$dependencies['Cases']['COM_DOS_NA_INF_9'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","100","142","101","144","90","103","9","136","164")',
				'labels' => 'createList("","ESPECIFICACIONES VEHÍCULO USADO","INFORMACIÓN VEHÍCULO USADO","ESTADO DEL NEGOCIO","INFORMACIÓN VITRINAS, CAMPAÑAS, ASESORES, LANZAMIENTOS Y RETOMAS","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","PAGOS PSE")'
			),
		),
	),
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_Sugerencia08.php

$dependencies['Cases']['SetOption_Sugerencia08'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"220"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/Subunidad04.php

$dependencies['Cases']['Subunidad04'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"))',
   'triggerFields' => array('cd_cia_c','cd_uneg_cont_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'cd_area_c',
           'keys' => 'createList("","21012")',
           'labels' => 'createList("","TALLER BUSES Y CAMIONES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/Subunidad01.php

$dependencies['Cases']['Subunidad01'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"410"))',
   'triggerFields' => array('cd_cia_c','cd_uneg_cont_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'cd_area_c',
           'keys' => 'createList("","11012")',
           'labels' => 'createList("","BUSES Y CAMIONES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetRequiredCases.php

$dependencies['Cases']['SetRequiredCases'] = array(
	'hooks' => array("all"),
	'trigger' => 'not(equal($name,""))',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_tipificacion_de_casos_cases_1_name',
                'value' => 'true',
            ),
        ),
	),
    'notActions' => array(
        
    )
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/Subunidad05.php

$dependencies['Cases']['Subunidad05'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"410"))',
   'triggerFields' => array('cd_cia_c','cd_uneg_cont_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'cd_area_c',
           'keys' => 'createList("","3144","2144","21012")',
           'labels' => 'createList("","VEHÍCULOS NUEVOS CHANGAN","TALLER CHANGAN","TALLER BUSES Y CAMIONES")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption35.php

$dependencies['Cases']['SetOption35'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_tipo_NA_y_compania_TAutorizados.php

$dependencies['Cases']['SetOption_tipo_NA_y_compania_TAutorizados'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"NA"),equal($cd_cia_c,"2"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","232","234","235","236","233")',
           'labels' => 'createList("","RECORDATORIO CITA TALLER PROGRAMADA","CONVOCATORIA CLIENTES DE RMP","CONVOCATORIA CLIENTES PRIMER SERVICIO","REPROGRAMACIÓN DE CITAS NO CUMPLIDAS","ENCUESTAS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption36.php

$dependencies['Cases']['SetOption36'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetOption_tipo_SOLICITUD.php

$dependencies['Cases']['SetOption_tipo_SOLICITUD'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_tipo_c,"F")',
   'triggerFields' => array('sasa_tipo_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","223","224","225","231")',
           'labels' => 'createList("","ESTUDIO DE PRECIOS LUBRICANTES","ESTUDIO DE PRECIOS AUTOPARTES","ESTUDIO DE PRECIOS EN MANO DE OBRA","INVITACIÓN A EVENTOS CORPORATIVOS")'
        ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetRequiredVisiContactLead.php

$dependencies['Cases']['SetRequiredVisiContactLead'] = array(
    'hooks' => array("edit", "view", "save"),
    'trigger' => 'equal(related($accounts,"id"),"a2590d86-50f6-11eb-9d24-0286beac7abe")',
    'onload' => false,
    //'triggerFields' => array('account_name'),
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'leads_cases_1_name',
                'value' => 'equal(related($accounts,"id"),"a2590d86-50f6-11eb-9d24-0286beac7abe")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'leads_cases_1_name',
                'value' => 'equal(related($accounts,"id"),"a2590d86-50f6-11eb-9d24-0286beac7abe")',
            ),
        ),
    ),
    'notActions' => array(
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'leads_cases_1_name',
                'value' => false,
            ),
        ),
    )
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Dependencies/SetRequiredVisiContactLead2.php

$dependencies['Cases']['SetRequiredVisiContactLead2'] = array(
    'hooks' => array("edit", "view", "save"),
    'trigger' => 'equal(related($accounts,"id"),"c5f0a53e-8cc3-11eb-b3f2-0286beac7abe")',
    'onload' => false,
    //'triggerFields' => array('account_name'),
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'contacts_cases_1_name',
                'value' => 'equal(related($accounts,"id"),"c5f0a53e-8cc3-11eb-b3f2-0286beac7abe")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'contacts_cases_1_name',
                'value' => 'equal(related($accounts,"id"),"c5f0a53e-8cc3-11eb-b3f2-0286beac7abe")',
            ),
        ),
    ),
    'notActions' => array(
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'contacts_cases_1_name',
                'value' => false,
            ),
        ),
    )
);

?>

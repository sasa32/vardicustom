<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/full_text_search_admin.php

 // created: 2020-08-28 14:29:24
$dictionary['Case']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_case_number.php

 // created: 2020-09-21 16:36:51
$dictionary['Case']['fields']['case_number']['len']='11';
$dictionary['Case']['fields']['case_number']['audited']=false;
$dictionary['Case']['fields']['case_number']['massupdate']=false;
$dictionary['Case']['fields']['case_number']['comments']='Visual unique identifier';
$dictionary['Case']['fields']['case_number']['merge_filter']='disabled';
$dictionary['Case']['fields']['case_number']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.29',
  'searchable' => true,
);
$dictionary['Case']['fields']['case_number']['calculated']=false;
$dictionary['Case']['fields']['case_number']['enable_range_search']=false;
$dictionary['Case']['fields']['case_number']['autoinc_next']='2';
$dictionary['Case']['fields']['case_number']['min']=false;
$dictionary['Case']['fields']['case_number']['max']=false;
$dictionary['Case']['fields']['case_number']['disable_num_format']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_business_hours_to_resolution.php

 // created: 2020-09-22 15:09:11
$dictionary['Case']['fields']['business_hours_to_resolution']['audited']=false;
$dictionary['Case']['fields']['business_hours_to_resolution']['massupdate']=false;
$dictionary['Case']['fields']['business_hours_to_resolution']['comments']='How long it took to resolve this issue, in decimal business hours';
$dictionary['Case']['fields']['business_hours_to_resolution']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['business_hours_to_resolution']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['business_hours_to_resolution']['merge_filter']='disabled';
$dictionary['Case']['fields']['business_hours_to_resolution']['reportable']=false;
$dictionary['Case']['fields']['business_hours_to_resolution']['calculated']=false;
$dictionary['Case']['fields']['business_hours_to_resolution']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_target_datetime.php

 // created: 2020-09-22 15:08:25
$dictionary['Case']['fields']['first_response_target_datetime']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['first_response_target_datetime']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['first_response_target_datetime']['merge_filter']='disabled';
$dictionary['Case']['fields']['first_response_target_datetime']['reportable']=false;
$dictionary['Case']['fields']['first_response_target_datetime']['calculated']=false;
$dictionary['Case']['fields']['first_response_target_datetime']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_actual_datetime.php

 // created: 2020-09-22 15:08:37
$dictionary['Case']['fields']['first_response_actual_datetime']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['first_response_actual_datetime']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['first_response_actual_datetime']['merge_filter']='disabled';
$dictionary['Case']['fields']['first_response_actual_datetime']['reportable']=false;
$dictionary['Case']['fields']['first_response_actual_datetime']['calculated']=false;
$dictionary['Case']['fields']['first_response_actual_datetime']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2020-10-08 09:03:35
$dictionary['Case']['fields']['date_entered']['audited']=true;
$dictionary['Case']['fields']['date_entered']['comments']='Date record created';
$dictionary['Case']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Case']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Case']['fields']['date_entered']['calculated']=false;
$dictionary['Case']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_hours_to_first_response.php

 // created: 2020-09-22 15:08:53
$dictionary['Case']['fields']['hours_to_first_response']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['hours_to_first_response']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['hours_to_first_response']['merge_filter']='disabled';
$dictionary['Case']['fields']['hours_to_first_response']['reportable']=false;
$dictionary['Case']['fields']['hours_to_first_response']['calculated']=false;
$dictionary['Case']['fields']['hours_to_first_response']['enable_range_search']=false;
$dictionary['Case']['fields']['hours_to_first_response']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_work_log.php

 // created: 2020-09-22 14:39:59
$dictionary['Case']['fields']['work_log']['audited']=true;
$dictionary['Case']['fields']['work_log']['massupdate']=false;
$dictionary['Case']['fields']['work_log']['comments']='Free-form text used to denote activities of interest';
$dictionary['Case']['fields']['work_log']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['work_log']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['work_log']['merge_filter']='disabled';
$dictionary['Case']['fields']['work_log']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.64',
  'searchable' => true,
);
$dictionary['Case']['fields']['work_log']['calculated']=false;
$dictionary['Case']['fields']['work_log']['rows']='4';
$dictionary['Case']['fields']['work_log']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_sent.php

 // created: 2020-09-22 15:09:52
$dictionary['Case']['fields']['first_response_sent']['default']=false;
$dictionary['Case']['fields']['first_response_sent']['audited']=false;
$dictionary['Case']['fields']['first_response_sent']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['first_response_sent']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['first_response_sent']['merge_filter']='disabled';
$dictionary['Case']['fields']['first_response_sent']['reportable']=false;
$dictionary['Case']['fields']['first_response_sent']['unified_search']=false;
$dictionary['Case']['fields']['first_response_sent']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_hours_to_resolution.php

 // created: 2020-09-22 15:07:42
$dictionary['Case']['fields']['hours_to_resolution']['audited']=false;
$dictionary['Case']['fields']['hours_to_resolution']['massupdate']=false;
$dictionary['Case']['fields']['hours_to_resolution']['comments']='How long it took to resolve this issue, in decimal calendar hours';
$dictionary['Case']['fields']['hours_to_resolution']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['hours_to_resolution']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['hours_to_resolution']['merge_filter']='disabled';
$dictionary['Case']['fields']['hours_to_resolution']['reportable']=false;
$dictionary['Case']['fields']['hours_to_resolution']['calculated']=false;
$dictionary['Case']['fields']['hours_to_resolution']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_portal_viewable.php

 // created: 2020-09-22 15:08:03
$dictionary['Case']['fields']['portal_viewable']['default']=true;
$dictionary['Case']['fields']['portal_viewable']['audited']=false;
$dictionary['Case']['fields']['portal_viewable']['massupdate']=false;
$dictionary['Case']['fields']['portal_viewable']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['portal_viewable']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['portal_viewable']['merge_filter']='disabled';
$dictionary['Case']['fields']['portal_viewable']['unified_search']=false;
$dictionary['Case']['fields']['portal_viewable']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_name.php

 // created: 2020-12-22 15:38:09
$dictionary['Case']['fields']['name']['len']='255';
$dictionary['Case']['fields']['name']['massupdate']=false;
$dictionary['Case']['fields']['name']['comments']='The short description of the bug';
$dictionary['Case']['fields']['name']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['name']['merge_filter']='disabled';
$dictionary['Case']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.53',
  'searchable' => true,
);
$dictionary['Case']['fields']['name']['calculated']='1';
$dictionary['Case']['fields']['name']['importable']='false';
$dictionary['Case']['fields']['name']['formula']='concat(toString($case_number)," - ",getDropdownValue("sasa_tipo_cases_c_list",$sasa_tipo_c)," - ",getDropdownValue("sasa_motivo_c_list",$sasa_motivo_c))';
$dictionary['Case']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_var_from_target.php

 // created: 2020-09-22 15:09:22
$dictionary['Case']['fields']['first_response_var_from_target']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['first_response_var_from_target']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['first_response_var_from_target']['merge_filter']='disabled';
$dictionary['Case']['fields']['first_response_var_from_target']['reportable']=false;
$dictionary['Case']['fields']['first_response_var_from_target']['calculated']=false;
$dictionary['Case']['fields']['first_response_var_from_target']['enable_range_search']=false;
$dictionary['Case']['fields']['first_response_var_from_target']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_primary_contact_name.php

 // created: 2020-09-22 15:08:13
$dictionary['Case']['fields']['primary_contact_name']['len']=255;
$dictionary['Case']['fields']['primary_contact_name']['audited']=false;
$dictionary['Case']['fields']['primary_contact_name']['massupdate']=false;
$dictionary['Case']['fields']['primary_contact_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['primary_contact_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['primary_contact_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['primary_contact_name']['reportable']=false;
$dictionary['Case']['fields']['primary_contact_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_resolution.php

 // created: 2020-09-22 14:35:17
$dictionary['Case']['fields']['resolution']['required']=true;
$dictionary['Case']['fields']['resolution']['audited']=true;
$dictionary['Case']['fields']['resolution']['massupdate']=false;
$dictionary['Case']['fields']['resolution']['comments']='The resolution of the case';
$dictionary['Case']['fields']['resolution']['importable']='false';
$dictionary['Case']['fields']['resolution']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['resolution']['duplicate_merge_dom_value']='0';
$dictionary['Case']['fields']['resolution']['merge_filter']='disabled';
$dictionary['Case']['fields']['resolution']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.65',
  'searchable' => true,
);
$dictionary['Case']['fields']['resolution']['calculated']=false;
$dictionary['Case']['fields']['resolution']['rows']='4';
$dictionary['Case']['fields']['resolution']['cols']='20';
$dictionary['Case']['fields']['resolution']['dependency']='equal($status,"Closed")';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_follow_up_datetime.php

 // created: 2020-09-22 15:07:29
$dictionary['Case']['fields']['follow_up_datetime']['audited']=false;
$dictionary['Case']['fields']['follow_up_datetime']['massupdate']=false;
$dictionary['Case']['fields']['follow_up_datetime']['comments']='Deadline for following up on an issue';
$dictionary['Case']['fields']['follow_up_datetime']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['follow_up_datetime']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['follow_up_datetime']['merge_filter']='disabled';
$dictionary['Case']['fields']['follow_up_datetime']['reportable']=false;
$dictionary['Case']['fields']['follow_up_datetime']['calculated']=false;
$dictionary['Case']['fields']['follow_up_datetime']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_sla_met.php

 // created: 2020-09-22 15:09:40
$dictionary['Case']['fields']['first_response_sla_met']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['first_response_sla_met']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['first_response_sla_met']['merge_filter']='disabled';
$dictionary['Case']['fields']['first_response_sla_met']['reportable']=false;
$dictionary['Case']['fields']['first_response_sla_met']['calculated']=false;
$dictionary['Case']['fields']['first_response_sla_met']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2020-10-08 09:03:43
$dictionary['Case']['fields']['date_modified']['audited']=true;
$dictionary['Case']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Case']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Case']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Case']['fields']['date_modified']['calculated']=false;
$dictionary['Case']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_status.php

 // created: 2020-09-22 14:32:01
$dictionary['Case']['fields']['status']['default']='New';
$dictionary['Case']['fields']['status']['massupdate']=true;
$dictionary['Case']['fields']['status']['comments']='The status of the case';
$dictionary['Case']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['status']['merge_filter']='disabled';
$dictionary['Case']['fields']['status']['calculated']=false;
$dictionary['Case']['fields']['status']['dependency']=false;
$dictionary['Case']['fields']['status']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_description.php

 // created: 2020-09-21 16:32:54
$dictionary['Case']['fields']['description']['required']=true;
$dictionary['Case']['fields']['description']['audited']=true;
$dictionary['Case']['fields']['description']['massupdate']=false;
$dictionary['Case']['fields']['description']['comments']='Full text of the note';
$dictionary['Case']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['description']['merge_filter']='disabled';
$dictionary['Case']['fields']['description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '0.66',
  'searchable' => false,
);
$dictionary['Case']['fields']['description']['calculated']=false;
$dictionary['Case']['fields']['description']['rows']='6';
$dictionary['Case']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sasa_puntos_de_ventas_cases_1_Cases.php

// created: 2021-03-09 21:41:14
$dictionary["Case"]["fields"]["sasa_puntos_de_ventas_cases_1"] = array (
  'name' => 'sasa_puntos_de_ventas_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_cases_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'side' => 'right',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_puntos_de_ventas_cases_1_name"] = array (
  'name' => 'sasa_puntos_de_ventas_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_cases_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida"] = array (
  'name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_cases_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_cd_uneg_cont_c-visibility_grid.php

$dictionary['Case']['fields']['cd_uneg_cont_c']['visibility_grid']=array (
	'trigger' => 'cd_cia_c',
	'values' => 
	array (
		1 => 
		array (
			0 => '',
			1 => 110,
			2 => 120,
			3 => 130,
			4 => 410,
			5 => 310,
		),
		2 => 
		array (
			0 => '',
			1 => 210,
			2 => 220,
			3 => 240,
			4 => 410,
		),
		3 => 
		array (
			0 => '',
			1 => 310,
			2 => 315,
			3 => 340,
			4 => 410,
		),
		'' => 
		array (
		),
	),
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_type.php

 // created: 2020-09-22 15:07:18
$dictionary['Case']['fields']['type']['default']='Administration';
$dictionary['Case']['fields']['type']['len']=100;
$dictionary['Case']['fields']['type']['audited']=false;
$dictionary['Case']['fields']['type']['massupdate']=false;
$dictionary['Case']['fields']['type']['comments']='The type of issue (ex: issue, feature)';
$dictionary['Case']['fields']['type']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['type']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['type']['merge_filter']='disabled';
$dictionary['Case']['fields']['type']['reportable']=false;
$dictionary['Case']['fields']['type']['calculated']=false;
$dictionary['Case']['fields']['type']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_agendar_citau_c.php

 // created: 2021-07-15 20:37:00
$dictionary['Case']['fields']['sasa_agendar_citau_c']['labelValue']='Agendar Cita URL';
$dictionary['Case']['fields']['sasa_agendar_citau_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_agendar_citau_c']['dependency']='';
$dictionary['Case']['fields']['sasa_agendar_citau_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_agendar_citau_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/denorm_account_name.php


// 'account_name'
$dictionary['Case']['fields']['account_name']['is_denormalized'] = true;
$dictionary['Case']['fields']['account_name']['denormalized_field_name'] = 'denorm_account_name';

// 'denorm_account_name'
$dictionary['Case']['fields']['denorm_account_name']['name'] = 'denorm_account_name';
$dictionary['Case']['fields']['denorm_account_name']['type'] = 'varchar';
$dictionary['Case']['fields']['denorm_account_name']['dbType'] = 'varchar';
$dictionary['Case']['fields']['denorm_account_name']['vname'] = 'LBL_ACCOUNT_NAME';
$dictionary['Case']['fields']['denorm_account_name']['len'] = '150';
$dictionary['Case']['fields']['denorm_account_name']['comment'] = 'Name of the Company';
$dictionary['Case']['fields']['denorm_account_name']['unified_search'] = true;
$dictionary['Case']['fields']['denorm_account_name']['full_text_search'] = array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['Case']['fields']['denorm_account_name']['audited'] = false;
$dictionary['Case']['fields']['denorm_account_name']['required'] = false;
$dictionary['Case']['fields']['denorm_account_name']['importable'] = 'false';
$dictionary['Case']['fields']['denorm_account_name']['duplicate_on_record_copy'] = 'always';
$dictionary['Case']['fields']['denorm_account_name']['merge_filter'] = 'disabled';
$dictionary['Case']['fields']['denorm_account_name']['massupdate'] = false;
$dictionary['Case']['fields']['denorm_account_name']['comments'] = 'Name of the Company';
$dictionary['Case']['fields']['denorm_account_name']['duplicate_merge'] = 'disabled';
$dictionary['Case']['fields']['denorm_account_name']['duplicate_merge_dom_value'] = 0;
$dictionary['Case']['fields']['denorm_account_name']['calculated'] = '1';
$dictionary['Case']['fields']['denorm_account_name']['formula'] = 'concat($sasa_nombres_c," ",$sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['Case']['fields']['denorm_account_name']['enforced'] = true;
$dictionary['Case']['fields']['denorm_account_name']['denorm_from_module'] = 'Accounts';
$dictionary['Case']['fields']['denorm_account_name']['studio'] = false;

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sasa_vehiculos_cases_1_Cases.php

// created: 2021-08-04 21:19:24
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_1"] = array (
  'name' => 'sasa_vehiculos_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_1',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'side' => 'right',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_1_FROM_CASES_TITLE',
  'id_name' => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_1_name"] = array (
  'name' => 'sasa_vehiculos_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_1_FROM_SASA_VEHICULOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_1sasa_vehiculos_ida"] = array (
  'name' => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_1_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sasa_vehiculos_cases_2_Cases.php

// created: 2021-08-04 21:22:45
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_2"] = array (
  'name' => 'sasa_vehiculos_cases_2',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_2',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'side' => 'right',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_CASES_TITLE',
  'id_name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_2_name"] = array (
  'name' => 'sasa_vehiculos_cases_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_SASA_VEHICULOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_2',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_2sasa_vehiculos_ida"] = array (
  'name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_2',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sasa_vehiculos_cases_3_Cases.php

// created: 2021-08-04 21:24:18
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_3"] = array (
  'name' => 'sasa_vehiculos_cases_3',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_3',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'side' => 'right',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_3_FROM_CASES_TITLE',
  'id_name' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_3_name"] = array (
  'name' => 'sasa_vehiculos_cases_3_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_3_FROM_SASA_VEHICULOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_3',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_3sasa_vehiculos_ida"] = array (
  'name' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_3_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_3',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sasa_vehiculos_cases_4_Cases.php

// created: 2021-08-04 21:25:11
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_4"] = array (
  'name' => 'sasa_vehiculos_cases_4',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_4',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'side' => 'right',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_4_FROM_CASES_TITLE',
  'id_name' => 'sasa_vehiculos_cases_4sasa_vehiculos_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_4_name"] = array (
  'name' => 'sasa_vehiculos_cases_4_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_4_FROM_SASA_VEHICULOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_vehiculos_cases_4sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_4',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_4sasa_vehiculos_ida"] = array (
  'name' => 'sasa_vehiculos_cases_4sasa_vehiculos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_4_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_vehiculos_cases_4sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_4',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_resolved_datetime.php

 // created: 2021-09-28 14:50:28
$dictionary['Case']['fields']['resolved_datetime']['massupdate']=false;
$dictionary['Case']['fields']['resolved_datetime']['comments']='Date when an issue is resolved';
$dictionary['Case']['fields']['resolved_datetime']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['resolved_datetime']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['resolved_datetime']['merge_filter']='disabled';
$dictionary['Case']['fields']['resolved_datetime']['calculated']=false;
$dictionary['Case']['fields']['resolved_datetime']['enable_range_search']=false;
$dictionary['Case']['fields']['resolved_datetime']['hidemassupdate']=false;
$dictionary['Case']['fields']['resolved_datetime']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_nit_empl.php

 /* created: 2020-10-30 15:19:35
$dictionary['Case']['fields']['nu_nit_empl']['labelValue']='Nombre Representante';
$dictionary['Case']['fields']['nu_nit_empl']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_nit_empl']['enforced']='';
$dictionary['Case']['fields']['nu_nit_empl']['dependency']='equal($sasa_pqrfs_c,"SI")';
*/
 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_pqrf.php

 /* created: 2020-10-30 14:27:00
$dictionary['Case']['fields']['nu_pqrf']['labelValue']='Número PQRF';
$dictionary['Case']['fields']['nu_pqrf']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_pqrf']['formula']='equal($sasa_pqrfs_c,"SI")';
$dictionary['Case']['fields']['nu_pqrf']['enforced']='false';
$dictionary['Case']['fields']['nu_pqrf']['dependency']='equal($sasa_pqrfs_c,"SI")';
*/
 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_contacto.php

 /* created: 2020-10-19 15:25:56
$dictionary['Case']['fields']['id_contacto']['labelValue']='Cómo Prefiere que le Contactemos?';
$dictionary['Case']['fields']['id_contacto']['dependency']='equal($sasa_pqrfs_c,"SI")';
$dictionary['Case']['fields']['id_contacto']['visibility_grid']='';
*/
 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_dere_peti.php

 /* created: 2020-10-19 15:23:51
$dictionary['Case']['fields']['id_dere_peti']['labelValue']='Derecho de Petición';
$dictionary['Case']['fields']['id_dere_peti']['dependency']='equal($sasa_pqrfs_c,"SI")';
$dictionary['Case']['fields']['id_dere_peti']['visibility_grid']='';
*/
 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_esta.php

 /* created: 2020-10-19 15:17:54
$dictionary['Case']['fields']['id_esta']['labelValue']='Estado PQRF';
$dictionary['Case']['fields']['id_esta']['dependency']='equal($sasa_pqrfs_c,"SI")';
$dictionary['Case']['fields']['id_esta']['visibility_grid']='';
*/
 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida.php

 // created: 2022-09-30 15:21:00
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['name']='sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['type']='id';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['source']='non-db';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['vname']='LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['id_name']='sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['link']='sasa_puntos_de_ventas_cases_1';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['table']='sasa_puntos_de_ventas';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['module']='sasa_Puntos_de_Ventas';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['rname']='id';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['reportable']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['side']='right';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['massupdate']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['hideacl']=true;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['audited']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_puntos_de_ventas_cases_1_name.php

 // created: 2022-09-30 15:21:00
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['audited']=true;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['massupdate']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['dependency']='equal($sasa_pqrf_c,"N")';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['vname']='LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sasa_centrosdecostos_cases_1_Cases.php

// created: 2022-10-05 15:14:01
$dictionary["Case"]["fields"]["sasa_centrosdecostos_cases_1"] = array (
  'name' => 'sasa_centrosdecostos_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_centrosdecostos_cases_1',
  'source' => 'non-db',
  'module' => 'sasa_CentrosDeCostos',
  'bean_name' => 'sasa_CentrosDeCostos',
  'side' => 'right',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE',
  'id_name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_centrosdecostos_cases_1_name"] = array (
  'name' => 'sasa_centrosdecostos_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_SASA_CENTROSDECOSTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'link' => 'sasa_centrosdecostos_cases_1',
  'table' => 'sasa_centrosdecostos',
  'module' => 'sasa_CentrosDeCostos',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida"] = array (
  'name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'link' => 'sasa_centrosdecostos_cases_1',
  'table' => 'sasa_centrosdecostos',
  'module' => 'sasa_CentrosDeCostos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_cd_cia_c.php

 // created: 2022-10-21 22:07:26
$dictionary['Case']['fields']['cd_cia_c']['labelValue']='Compañía';
$dictionary['Case']['fields']['cd_cia_c']['dependency']='';
$dictionary['Case']['fields']['cd_cia_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_contacto_c.php

 // created: 2022-10-21 22:07:28
$dictionary['Case']['fields']['id_contacto_c']['labelValue']='Cómo Prefiere que le Contactemos?';
$dictionary['Case']['fields']['id_contacto_c']['dependency']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['id_contacto_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_dere_peti_c.php

 // created: 2022-10-21 22:07:29
$dictionary['Case']['fields']['id_dere_peti_c']['labelValue']='Derecho de Petición';
$dictionary['Case']['fields']['id_dere_peti_c']['dependency']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['id_dere_peti_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_esta_c.php

 // created: 2022-10-21 22:07:29
$dictionary['Case']['fields']['id_esta_c']['labelValue']='Estado PQRF';
$dictionary['Case']['fields']['id_esta_c']['dependency']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['id_esta_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_chas_seri_c.php

 // created: 2022-10-21 22:07:30
$dictionary['Case']['fields']['nu_chas_seri_c']['labelValue']='Chasis Vehículo/Serie Máquina';
$dictionary['Case']['fields']['nu_chas_seri_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_chas_seri_c']['enforced']='';
$dictionary['Case']['fields']['nu_chas_seri_c']['dependency']='or(
	and(
		equal($sasa_pqrf_c,"N"),
		or(
			equal($sasa_motivo_c,"8"),
			equal($sasa_motivo_c,"49"),
			equal($sasa_motivo_c,"51"),
			equal($sasa_motivo_c,"56"),
			equal($sasa_motivo_c,"58"),
			equal($sasa_motivo_c,"54"),
			equal($sasa_motivo_c,"55"),
			equal($sasa_motivo_c,"59"),
			equal($sasa_motivo_c,"61"),
			equal($sasa_motivo_c,"71"),
			equal($sasa_motivo_c,"69"),
			equal($sasa_motivo_c,"76"),
			equal($sasa_motivo_c,"86"),
			equal($sasa_motivo_c,"145"),
			equal($sasa_motivo_c,"180"),
			equal($sasa_motivo_c,"194"),
			equal($sasa_motivo_c,"213"),
			equal($sasa_motivo_c,"50"),
			equal($sasa_motivo_c,"52"),
			equal($sasa_motivo_c,"68"),
			equal($sasa_motivo_c,"90"),
			equal($sasa_motivo_c,"102"),
			equal($sasa_motivo_c,"103"),
			equal($sasa_motivo_c,"104"),
			equal($sasa_motivo_c,"105"),
			equal($sasa_motivo_c,"119"),
			equal($sasa_motivo_c,"135"),
			equal($sasa_motivo_c,"154"),
			equal($sasa_motivo_c,"169"),
			equal($sasa_motivo_c,"172"),
			equal($sasa_motivo_c,"173"),
			equal($sasa_motivo_c,"176"),
			equal($sasa_motivo_c,"200")
		)
	),equal($sasa_pqrf_c,"S")
)';
$dictionary['Case']['fields']['nu_chas_seri_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_fact_repu_c.php

 // created: 2022-10-21 22:07:31
$dictionary['Case']['fields']['nu_fact_repu_c']['labelValue']='Número Factura Repuestos';
$dictionary['Case']['fields']['nu_fact_repu_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_fact_repu_c']['enforced']='';
$dictionary['Case']['fields']['nu_fact_repu_c']['dependency']='or(equal($sasa_motivo_c,"102"),equal($sasa_motivo_c,"71"))';
$dictionary['Case']['fields']['nu_fact_repu_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_kms_c.php

 // created: 2022-10-21 22:07:33
$dictionary['Case']['fields']['nu_kms_c']['labelValue']='Kilometraje Vehículo/Horas Máquina';
$dictionary['Case']['fields']['nu_kms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_kms_c']['enforced']='';
$dictionary['Case']['fields']['nu_kms_c']['dependency']='or(
	and(
		equal($sasa_pqrf_c,"N"),
		or(
			equal($sasa_motivo_c,"8"),
			equal($sasa_motivo_c,"49"),
			equal($sasa_motivo_c,"51"),
			equal($sasa_motivo_c,"56"),
			equal($sasa_motivo_c,"58"),
			equal($sasa_motivo_c,"54"),
			equal($sasa_motivo_c,"55"),
			equal($sasa_motivo_c,"59"),
			equal($sasa_motivo_c,"61"),
			equal($sasa_motivo_c,"71"),
			equal($sasa_motivo_c,"69"),
			equal($sasa_motivo_c,"76"),
			equal($sasa_motivo_c,"86"),
			equal($sasa_motivo_c,"145"),
			equal($sasa_motivo_c,"180"),
			equal($sasa_motivo_c,"194"),
			equal($sasa_motivo_c,"213"),
			equal($sasa_motivo_c,"50"),
			equal($sasa_motivo_c,"52"),
			equal($sasa_motivo_c,"68"),
			equal($sasa_motivo_c,"90"),
			equal($sasa_motivo_c,"102"),
			equal($sasa_motivo_c,"103"),
			equal($sasa_motivo_c,"104"),
			equal($sasa_motivo_c,"105"),
			equal($sasa_motivo_c,"119"),
			equal($sasa_motivo_c,"135"),
			equal($sasa_motivo_c,"154"),
			equal($sasa_motivo_c,"169"),
			equal($sasa_motivo_c,"172"),
			equal($sasa_motivo_c,"173"),
			equal($sasa_motivo_c,"176"),
			equal($sasa_motivo_c,"200")
		)
	),equal($sasa_pqrf_c,"S")
)';
$dictionary['Case']['fields']['nu_kms_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_nit_empl_c.php

 // created: 2022-10-21 22:07:35
$dictionary['Case']['fields']['nu_nit_empl_c']['labelValue']='No. documento Representante';
$dictionary['Case']['fields']['nu_nit_empl_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_nit_empl_c']['enforced']='';
$dictionary['Case']['fields']['nu_nit_empl_c']['dependency']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['nu_nit_empl_c']['required_formula']='';
$dictionary['Case']['fields']['nu_nit_empl_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_plac_vehi_c.php

 // created: 2022-10-21 22:07:36
$dictionary['Case']['fields']['nu_plac_vehi_c']['labelValue']='Placa';
$dictionary['Case']['fields']['nu_plac_vehi_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_plac_vehi_c']['enforced']='';
$dictionary['Case']['fields']['nu_plac_vehi_c']['dependency']='or(
	and(
		equal($sasa_pqrf_c,"N"),
		or(
			equal($sasa_motivo_c,"8"),
			equal($sasa_motivo_c,"49"),
			equal($sasa_motivo_c,"51"),
			equal($sasa_motivo_c,"56"),
			equal($sasa_motivo_c,"58"),
			equal($sasa_motivo_c,"54"),
			equal($sasa_motivo_c,"55"),
			equal($sasa_motivo_c,"59"),
			equal($sasa_motivo_c,"61"),
			equal($sasa_motivo_c,"71"),
			equal($sasa_motivo_c,"69"),
			equal($sasa_motivo_c,"76"),
			equal($sasa_motivo_c,"86"),
			equal($sasa_motivo_c,"145"),
			equal($sasa_motivo_c,"180"),
			equal($sasa_motivo_c,"194"),
			equal($sasa_motivo_c,"213"),
			equal($sasa_motivo_c,"50"),
			equal($sasa_motivo_c,"52"),
			equal($sasa_motivo_c,"68"),
			equal($sasa_motivo_c,"90"),
			equal($sasa_motivo_c,"102"),
			equal($sasa_motivo_c,"103"),
			equal($sasa_motivo_c,"104"),
			equal($sasa_motivo_c,"105"),
			equal($sasa_motivo_c,"119"),
			equal($sasa_motivo_c,"135"),
			equal($sasa_motivo_c,"154"),
			equal($sasa_motivo_c,"169"),
			equal($sasa_motivo_c,"172"),
			equal($sasa_motivo_c,"173"),
			equal($sasa_motivo_c,"176"),
			equal($sasa_motivo_c,"200")
		)
	),equal($sasa_pqrf_c,"S"),or(
			equal($cd_uneg_cont_c,"130"),
			equal($cd_uneg_cont_c,"210"),
			equal($cd_uneg_cont_c,"310")
			)
)';
$dictionary['Case']['fields']['nu_plac_vehi_c']['required_formula']='';
$dictionary['Case']['fields']['nu_plac_vehi_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_orde_trab_c.php

 // created: 2022-10-21 22:07:36
$dictionary['Case']['fields']['nu_orde_trab_c']['labelValue']='Número Orden Trabajo Servicio';
$dictionary['Case']['fields']['nu_orde_trab_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_orde_trab_c']['enforced']='';
$dictionary['Case']['fields']['nu_orde_trab_c']['dependency']='or(
	and(
		equal($sasa_pqrf_c,"N"),
		or(
			equal($sasa_motivo_c,"8"),
			equal($sasa_motivo_c,"49"),
			equal($sasa_motivo_c,"51"),
			equal($sasa_motivo_c,"56"),
			equal($sasa_motivo_c,"58"),
			equal($sasa_motivo_c,"54"),
			equal($sasa_motivo_c,"55"),
			equal($sasa_motivo_c,"59"),
			equal($sasa_motivo_c,"61"),
			equal($sasa_motivo_c,"71"),
			equal($sasa_motivo_c,"69"),
			equal($sasa_motivo_c,"76"),
			equal($sasa_motivo_c,"86"),
			equal($sasa_motivo_c,"145"),
			equal($sasa_motivo_c,"180"),
			equal($sasa_motivo_c,"194"),
			equal($sasa_motivo_c,"213"),
			equal($sasa_motivo_c,"50"),
			equal($sasa_motivo_c,"52"),
			equal($sasa_motivo_c,"68"),
			equal($sasa_motivo_c,"90"),
			equal($sasa_motivo_c,"102"),
			equal($sasa_motivo_c,"103"),
			equal($sasa_motivo_c,"104"),
			equal($sasa_motivo_c,"105"),
			equal($sasa_motivo_c,"119"),
			equal($sasa_motivo_c,"135"),
			equal($sasa_motivo_c,"154"),
			equal($sasa_motivo_c,"169"),
			equal($sasa_motivo_c,"172"),
			equal($sasa_motivo_c,"173"),
			equal($sasa_motivo_c,"176"),
			equal($sasa_motivo_c,"200")
		)
	),equal($sasa_pqrf_c,"S")
)';
$dictionary['Case']['fields']['nu_orde_trab_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_pqrf_c.php

 // created: 2022-10-21 22:07:37
$dictionary['Case']['fields']['nu_pqrf_c']['labelValue']='Número PQRF';
$dictionary['Case']['fields']['nu_pqrf_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_pqrf_c']['enforced']='false';
$dictionary['Case']['fields']['nu_pqrf_c']['dependency']='equal($sasa_pqrf_c,"S")';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_area_solicitante_c.php

 // created: 2022-10-21 22:07:38
$dictionary['Case']['fields']['sasa_area_solicitante_c']['labelValue']='Área Solicitante';
$dictionary['Case']['fields']['sasa_area_solicitante_c']['dependency']='equal($sasa_motivo_c,"60")';
$dictionary['Case']['fields']['sasa_area_solicitante_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_cod_puntoaten_c.php

 // created: 2022-10-21 22:07:39

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_document_solicitado_c.php

 // created: 2022-10-21 22:07:40
$dictionary['Case']['fields']['sasa_document_solicitado_c']['labelValue']='Documento Solicitado';
$dictionary['Case']['fields']['sasa_document_solicitado_c']['dependency']='equal($sasa_motivo_c,"194")';
$dictionary['Case']['fields']['sasa_document_solicitado_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fecha_cierre_pqrf_c.php

 // created: 2022-10-21 22:07:41
$dictionary['Case']['fields']['sasa_fecha_cierre_pqrf_c']['labelValue']='Fecha cierre PQRF';
$dictionary['Case']['fields']['sasa_fecha_cierre_pqrf_c']['enforced']='';
$dictionary['Case']['fields']['sasa_fecha_cierre_pqrf_c']['dependency']='equal($sasa_pqrf_c,"S")';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2022-10-21 22:07:43
$dictionary['Case']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['Case']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['Case']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_instructivo_c.php

 // created: 2022-10-21 22:07:44
$dictionary['Case']['fields']['sasa_instructivo_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_instructivo_c']['labelValue']='Instructivo';
$dictionary['Case']['fields']['sasa_instructivo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_instructivo_c']['calculated']='false';
$dictionary['Case']['fields']['sasa_instructivo_c']['enforced']='true';
$dictionary['Case']['fields']['sasa_instructivo_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_pqrf_c.php

 // created: 2022-10-21 22:07:45
$dictionary['Case']['fields']['sasa_pqrf_c']['labelValue']='PQRFS';
$dictionary['Case']['fields']['sasa_pqrf_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipo_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'A' => 
    array (
      0 => 'S',
    ),
    'B' => 
    array (
      0 => 'S',
    ),
    'C' => 
    array (
      0 => 'S',
    ),
    'H' => 
    array (
      0 => 'S',
    ),
    'D' => 
    array (
      0 => 'S',
    ),
    'E' => 
    array (
      0 => 'N',
    ),
    'F' => 
    array (
      0 => 'N',
    ),
    'G' => 
    array (
      0 => 'S',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_tipo_c.php

 // created: 2022-10-21 22:07:46
$dictionary['Case']['fields']['sasa_tipo_c']['labelValue']='Tipo';
$dictionary['Case']['fields']['sasa_tipo_c']['dependency']='';
$dictionary['Case']['fields']['sasa_tipo_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_motivollorente_c.php

 // created: 2022-10-21 22:07:47

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_lineanotificacionase_c.php

 // created: 2022-10-21 22:07:50
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['labelValue']='Campo espejo Linea (Notificacion Asesor)';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['calculated']='true';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['formula']='related($sasa_vehiculos_cases_1,"name")';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['enforced']='true';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['dependency']='';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fechafingestioncs_c.php

 // created: 2022-10-21 22:07:50
$dictionary['Case']['fields']['sasa_fechafingestioncs_c']['labelValue']='Fecha final de gestión';
$dictionary['Case']['fields']['sasa_fechafingestioncs_c']['enforced']='';
$dictionary['Case']['fields']['sasa_fechafingestioncs_c']['dependency']='or(
equal($sasa_motivo_c,74),
equal($sasa_motivo_c,73),
equal($sasa_motivo_c,71),
equal($sasa_motivo_c,68),
equal($sasa_motivo_c,141),
equal($sasa_motivo_c,142),
equal($sasa_motivo_c,143),
equal($sasa_motivo_c,204))';
$dictionary['Case']['fields']['sasa_fechafingestioncs_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_fechafingestioncs_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fechainiciogestioncs_c.php

 // created: 2022-10-21 22:07:52
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['labelValue']='Fecha inicio de gestión ';
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['enforced']='';
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['dependency']='or(
equal($sasa_motivo_c,74),
equal($sasa_motivo_c,73),
equal($sasa_motivo_c,71),
equal($sasa_motivo_c,68),
equal($sasa_motivo_c,141),
equal($sasa_motivo_c,142),
equal($sasa_motivo_c,143),
equal($sasa_motivo_c,204))';
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_estadocontacto_c.php

 // created: 2022-10-21 22:07:53
$dictionary['Case']['fields']['sasa_estadocontacto_c']['labelValue']='Estado del Contacto';
$dictionary['Case']['fields']['sasa_estadocontacto_c']['dependency']='or(
equal($sasa_motivo_c,74),
equal($sasa_motivo_c,73),
equal($sasa_motivo_c,71),
equal($sasa_motivo_c,68),
equal($sasa_motivo_c,141),
equal($sasa_motivo_c,142),
equal($sasa_motivo_c,143),
equal($sasa_motivo_c,204))';
$dictionary['Case']['fields']['sasa_estadocontacto_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_estadocontacto_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_estadocontacto_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_numerogestiones_c.php

 // created: 2022-10-21 22:07:54
$dictionary['Case']['fields']['sasa_numerogestiones_c']['labelValue']='Número de Gestiones';
$dictionary['Case']['fields']['sasa_numerogestiones_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_numerogestiones_c']['enforced']='';
$dictionary['Case']['fields']['sasa_numerogestiones_c']['dependency']='or(
equal($sasa_motivo_c,74),
equal($sasa_motivo_c,73),
equal($sasa_motivo_c,71),
equal($sasa_motivo_c,68),
equal($sasa_motivo_c,141),
equal($sasa_motivo_c,142),
equal($sasa_motivo_c,143),
equal($sasa_motivo_c,204))';
$dictionary['Case']['fields']['sasa_numerogestiones_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_numerogestiones_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_estadocomercial_c.php

 // created: 2022-10-21 22:07:55
$dictionary['Case']['fields']['sasa_estadocomercial_c']['labelValue']='Estado Comercial';
$dictionary['Case']['fields']['sasa_estadocomercial_c']['dependency']='or(
equal($sasa_motivo_c,74),
equal($sasa_motivo_c,73),
equal($sasa_motivo_c,71),
equal($sasa_motivo_c,68),
equal($sasa_motivo_c,141),
equal($sasa_motivo_c,142),
equal($sasa_motivo_c,143),
equal($sasa_motivo_c,204))';
$dictionary['Case']['fields']['sasa_estadocomercial_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_estadocomercial_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_estadocomercial_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fechacontacto_c.php

 // created: 2022-10-21 22:07:55
$dictionary['Case']['fields']['sasa_fechacontacto_c']['labelValue']='Fecha de contacto';
$dictionary['Case']['fields']['sasa_fechacontacto_c']['enforced']='';
$dictionary['Case']['fields']['sasa_fechacontacto_c']['dependency']='or(
equal($sasa_motivo_c,74),
equal($sasa_motivo_c,73),
equal($sasa_motivo_c,71),
equal($sasa_motivo_c,68),
equal($sasa_motivo_c,141),
equal($sasa_motivo_c,142),
equal($sasa_motivo_c,143),
equal($sasa_motivo_c,204))';
$dictionary['Case']['fields']['sasa_fechacontacto_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_fechacontacto_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_centrocostos_c.php

 // created: 2022-10-21 22:07:56

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_count_call_status_c.php

 // created: 2022-10-21 22:07:57

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_eval_si_c.php

 // created: 2022-10-21 22:07:58

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_eval_no_c.php

 // created: 2022-10-21 22:07:59

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_eval_no_contacto_c.php

 // created: 2022-10-21 22:08:00

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida.php

 // created: 2022-12-05 19:56:06
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['name']='sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['type']='id';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['source']='non-db';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['vname']='LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['id_name']='sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['link']='sasa_centrosdecostos_cases_1';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['table']='sasa_centrosdecostos';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['module']='sasa_CentrosDeCostos';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['rname']='id';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['reportable']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['side']='right';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['massupdate']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['hideacl']=true;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['audited']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_centrosdecostos_cases_1_name.php

 // created: 2022-12-05 19:56:06
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['audited']=true;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['massupdate']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['dependency']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['vname']='LBL_SASA_CENTROSDECOSTOS_CASES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_priority.php

 // created: 2022-12-27 20:00:15
$dictionary['Case']['fields']['priority']['default']='';
$dictionary['Case']['fields']['priority']['required']=false;
$dictionary['Case']['fields']['priority']['massupdate']=false;
$dictionary['Case']['fields']['priority']['comments']='The priority of the case';
$dictionary['Case']['fields']['priority']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['priority']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['priority']['merge_filter']='disabled';
$dictionary['Case']['fields']['priority']['calculated']=false;
$dictionary['Case']['fields']['priority']['dependency']=false;
$dictionary['Case']['fields']['priority']['hidemassupdate']=false;
$dictionary['Case']['fields']['priority']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_cd_area_c.php

 // created: 2022-12-26 16:59:29
$dictionary['Case']['fields']['cd_area_c']['labelValue']='Subunidad de Negocio';
$dictionary['Case']['fields']['cd_area_c']['dependency']='';
$dictionary['Case']['fields']['cd_area_c']['required_formula']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['cd_area_c']['readonly_formula']='';
$dictionary['Case']['fields']['cd_area_c']['visibility_grid']=array (
  'trigger' => 'cd_uneg_cont_c',
  'values' => 
  array (
    110 => 
    array (
      0 => '',
      1 => '11011',
    ),
    120 => 
    array (
      0 => '',
      1 => '12020',
    ),
    130 => 
    array (
      0 => '',
      1 => '13030',
    ),
    153 => 
    array (
      0 => '',
      1 => '15353',
    ),
    210 => 
    array (
      0 => '',
      1 => '21010',
      2 => '21012',
    ),
    220 => 
    array (
      0 => '',
      1 => '22020',
    ),
    310 => 
    array (
      0 => '',
      1 => '31011',
    ),
    315 => 
    array (
      0 => '',
      1 => '31515',
    ),
    340 => 
    array (
      0 => '',
      1 => '34044',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_cd_uneg_cont_c.php

 // created: 2022-12-26 16:48:06
$dictionary['Case']['fields']['cd_uneg_cont_c']['labelValue']='Unidad de Negocio';
$dictionary['Case']['fields']['cd_uneg_cont_c']['dependency']='';
$dictionary['Case']['fields']['cd_uneg_cont_c']['required_formula']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['cd_uneg_cont_c']['readonly_formula']='';
$dictionary['Case']['fields']['cd_uneg_cont_c']['visibility_grid']=array (
  'trigger' => 'cd_cia_c',
  'values' => 
  array (
    1 => 
    array (
      0 => '',
      1 => '153',
      2 => '120',
      3 => '130',
      4 => '110',
    ),
    2 => 
    array (
      0 => '',
      1 => '210',
      2 => '220',
    ),
    3 => 
    array (
      0 => '',
      1 => '310',
      2 => '315',
      3 => '340',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_motivo_c.php

 // created: 2022-12-12 16:55:47
$dictionary['Case']['fields']['sasa_motivo_c']['labelValue']='Motivo';
$dictionary['Case']['fields']['sasa_motivo_c']['dependency']='';
$dictionary['Case']['fields']['sasa_motivo_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_motivo_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_motivo_c']['visibility_grid']=array (
  'trigger' => 'cd_area_c',
  'values' => 
  array (
    11010 => 
    array (
    ),
    11011 => 
    array (
      0 => '',
      1 => '2',
      2 => '3',
      3 => '4',
      4 => '5',
      5 => '6',
      6 => '8',
      7 => '241',
      8 => '9',
      9 => '10',
      10 => '15',
      11 => '17',
      12 => '20',
      13 => '21',
      14 => '22',
      15 => '23',
      16 => '29',
      17 => '34',
      18 => '35',
      19 => '44',
      20 => '49',
      21 => '53',
      22 => '57',
      23 => '60',
      24 => '65',
      25 => '74',
      26 => '68',
      27 => '79',
      28 => '80',
      29 => '82',
      30 => '84',
      31 => '85',
      32 => '87',
      33 => '90',
      34 => '94',
      35 => '233',
      36 => '99',
      37 => '101',
      38 => '103',
      39 => '107',
      40 => '111',
      41 => '112',
      42 => '113',
      43 => '116',
      44 => '243',
      45 => '120',
      46 => '122',
      47 => '123',
      48 => '124',
      49 => '125',
      50 => '130',
      51 => '132',
      52 => '134',
      53 => '137',
      54 => '246',
      55 => '139',
      56 => '140',
      57 => '141',
      58 => '146',
      59 => '152',
      60 => '156',
      61 => '164',
      62 => '245',
      63 => '168',
      64 => '174',
      65 => '176',
      66 => '183',
      67 => '242',
      68 => '187',
      69 => '189',
      70 => '190',
      71 => '191',
      72 => '192',
      73 => '194',
      74 => '195',
      75 => '196',
      76 => '129',
      77 => '219',
      78 => '204',
      79 => '209',
      80 => '210',
      81 => '211',
      82 => '212',
      83 => '213',
      84 => '214',
    ),
    11012 => 
    array (
    ),
    12020 => 
    array (
      0 => '',
      1 => '4',
      2 => '5',
      3 => '6',
      4 => '8',
      5 => '241',
      6 => '9',
      7 => '10',
      8 => '14',
      9 => '15',
      10 => '17',
      11 => '21',
      12 => '22',
      13 => '23',
      14 => '35',
      15 => '37',
      16 => '34',
      17 => '43',
      18 => '44',
      19 => '57',
      20 => '60',
      21 => '65',
      22 => '70',
      23 => '68',
      24 => '77',
      25 => '79',
      26 => '84',
      27 => '85',
      28 => '87',
      29 => '90',
      30 => '233',
      31 => '98',
      32 => '103',
      33 => '109',
      34 => '111',
      35 => '112',
      36 => '113',
      37 => '116',
      38 => '123',
      39 => '124',
      40 => '125',
      41 => '130',
      42 => '132',
      43 => '134',
      44 => '246',
      45 => '139',
      46 => '140',
      47 => '146',
      48 => '151',
      49 => '155',
      50 => '156',
      51 => '164',
      52 => '245',
      53 => '170',
      54 => '183',
      55 => '242',
      56 => '189',
      57 => '219',
      58 => '203',
      59 => '208',
      60 => '209',
      61 => '210',
      62 => '211',
      63 => '216',
    ),
    12021 => 
    array (
    ),
    12022 => 
    array (
    ),
    12023 => 
    array (
    ),
    13030 => 
    array (
      0 => '',
      1 => '2',
      2 => '5',
      3 => '6',
      4 => '8',
      5 => '241',
      6 => '9',
      7 => '10',
      8 => '13',
      9 => '15',
      10 => '17',
      11 => '20',
      12 => '21',
      13 => '22',
      14 => '23',
      15 => '29',
      16 => '32',
      17 => '33',
      18 => '42',
      19 => '60',
      20 => '65',
      21 => '66',
      22 => '69',
      23 => '68',
      24 => '71',
      25 => '78',
      26 => '79',
      27 => '84',
      28 => '85',
      29 => '86',
      30 => '240',
      31 => '87',
      32 => '90',
      33 => '92',
      34 => '94',
      35 => '233',
      36 => '102',
      37 => '103',
      38 => '107',
      39 => '111',
      40 => '112',
      41 => '113',
      42 => '123',
      43 => '130',
      44 => '132',
      45 => '134',
      46 => '246',
      47 => '139',
      48 => '140',
      49 => '146',
      50 => '156',
      51 => '164',
      52 => '245',
      53 => '168',
      54 => '176',
      55 => '182',
      56 => '183',
      57 => '185',
      58 => '242',
      59 => '189',
      60 => '191',
      61 => '194',
      62 => '219',
      63 => '201',
      64 => '205',
      65 => '208',
      66 => '209',
      67 => '210',
      68 => '211',
      69 => '212',
      70 => '215',
    ),
    15353 => 
    array (
      0 => '',
      1 => '241',
      2 => '246',
      3 => '164',
      4 => '194',
      5 => '245',
      6 => '208',
      7 => '211',
    ),
    21010 => 
    array (
      0 => '',
      1 => '1',
      2 => '4',
      3 => '5',
      4 => '6',
      5 => '7',
      6 => '8',
      7 => '241',
      8 => '9',
      9 => '12',
      10 => '17',
      11 => '16',
      12 => '237',
      13 => '219',
      14 => '18',
      15 => '20',
      16 => '21',
      17 => '22',
      18 => '23',
      19 => '24',
      20 => '26',
      21 => '27',
      22 => '49',
      23 => '29',
      24 => '30',
      25 => '31',
      26 => '32',
      27 => '34',
      28 => '35',
      29 => '36',
      30 => '38',
      31 => '39',
      32 => '40',
      33 => '42',
      34 => '43',
      35 => '44',
      36 => '45',
      37 => '46',
      38 => '47',
      39 => '48',
      40 => '50',
      41 => '51',
      42 => '53',
      43 => '54',
      44 => '55',
      45 => '56',
      46 => '58',
      47 => '59',
      48 => '60',
      49 => '61',
      50 => '62',
      51 => '234',
      52 => '235',
      53 => '65',
      54 => '68',
      55 => '76',
      56 => '78',
      57 => '79',
      58 => '80',
      59 => '81',
      60 => '83',
      61 => '84',
      62 => '85',
      63 => '87',
      64 => '89',
      65 => '90',
      66 => '91',
      67 => '92',
      68 => '93',
      69 => '95',
      70 => '96',
      71 => '233',
      72 => '97',
      73 => '101',
      74 => '103',
      75 => '104',
      76 => '105',
      77 => '106',
      78 => '225',
      79 => '229',
      80 => '107',
      81 => '108',
      82 => '110',
      83 => '112',
      84 => '113',
      85 => '115',
      86 => '116',
      87 => '117',
      88 => '118',
      89 => '119',
      90 => '121',
      91 => '123',
      92 => '125',
      93 => '132',
      94 => '133',
      95 => '135',
      96 => '136',
      97 => '246',
      98 => '139',
      99 => '145',
      100 => '146',
      101 => '147',
      102 => '149',
      103 => '152',
      104 => '154',
      105 => '157',
      106 => '161',
      107 => '163',
      108 => '164',
      109 => '245',
      110 => '165',
      111 => '167',
      112 => '169',
      113 => '172',
      114 => '173',
      115 => '175',
      116 => '177',
      117 => '222',
      118 => '179',
      119 => '232',
      120 => '180',
      121 => '236',
      122 => '181',
      123 => '185',
      124 => '242',
      125 => '187',
      126 => '193',
      127 => '194',
      128 => '197',
      129 => '199',
      130 => '190',
      131 => '191',
      132 => '200',
      133 => '201',
      134 => '202',
      135 => '206',
      136 => '207',
      137 => '209',
      138 => '211',
      139 => '212',
      140 => '213',
      141 => '217',
    ),
    21011 => 
    array (
    ),
    21012 => 
    array (
      0 => '',
      1 => '1',
      2 => '4',
      3 => '5',
      4 => '6',
      5 => '7',
      6 => '8',
      7 => '241',
      8 => '9',
      9 => '17',
      10 => '16',
      11 => '18',
      12 => '20',
      13 => '21',
      14 => '22',
      15 => '23',
      16 => '24',
      17 => '26',
      18 => '27',
      19 => '29',
      20 => '30',
      21 => '31',
      22 => '32',
      23 => '34',
      24 => '35',
      25 => '36',
      26 => '38',
      27 => '39',
      28 => '40',
      29 => '42',
      30 => '43',
      31 => '44',
      32 => '45',
      33 => '46',
      34 => '47',
      35 => '48',
      36 => '50',
      37 => '51',
      38 => '53',
      39 => '54',
      40 => '55',
      41 => '56',
      42 => '58',
      43 => '59',
      44 => '60',
      45 => '61',
      46 => '62',
      47 => '234',
      48 => '235',
      49 => '63',
      50 => '65',
      51 => '68',
      52 => '76',
      53 => '78',
      54 => '79',
      55 => '80',
      56 => '81',
      57 => '83',
      58 => '84',
      59 => '85',
      60 => '87',
      61 => '89',
      62 => '90',
      63 => '91',
      64 => '92',
      65 => '93',
      66 => '95',
      67 => '96',
      68 => '233',
      69 => '97',
      70 => '101',
      71 => '103',
      72 => '104',
      73 => '105',
      74 => '106',
      75 => '225',
      76 => '229',
      77 => '107',
      78 => '108',
      79 => '246',
      80 => '110',
      81 => '112',
      82 => '113',
      83 => '115',
      84 => '116',
      85 => '117',
      86 => '118',
      87 => '119',
      88 => '121',
      89 => '123',
      90 => '125',
      91 => '132',
      92 => '133',
      93 => '135',
      94 => '136',
      95 => '139',
      96 => '145',
      97 => '146',
      98 => '147',
      99 => '148',
      100 => '149',
      101 => '152',
      102 => '154',
      103 => '157',
      104 => '161',
      105 => '163',
      106 => '164',
      107 => '245',
      108 => '165',
      109 => '167',
      110 => '169',
      111 => '172',
      112 => '173',
      113 => '175',
      114 => '177',
      115 => '222',
      116 => '178',
      117 => '179',
      118 => '232',
      119 => '180',
      120 => '236',
      121 => '181',
      122 => '185',
      123 => '242',
      124 => '190',
      125 => '191',
      126 => '193',
      127 => '194',
      128 => '197',
      129 => '199',
      130 => '219',
      131 => '201',
      132 => '202',
      133 => '206',
      134 => '207',
      135 => '209',
      136 => '211',
      137 => '212',
      138 => '217',
    ),
    22018 => 
    array (
    ),
    22020 => 
    array (
      0 => '',
      1 => '4',
      2 => '5',
      3 => '6',
      4 => '8',
      5 => '241',
      6 => '11',
      7 => '15',
      8 => '17',
      9 => '16',
      10 => '19',
      11 => '20',
      12 => '21',
      13 => '22',
      14 => '23',
      15 => '30',
      16 => '31',
      17 => '34',
      18 => '36',
      19 => '40',
      20 => '42',
      21 => '43',
      22 => '47',
      23 => '48',
      24 => '60',
      25 => '62',
      26 => '64',
      27 => '65',
      28 => '68',
      29 => '78',
      30 => '79',
      31 => '83',
      32 => '84',
      33 => '85',
      34 => '87',
      35 => '88',
      36 => '92',
      37 => '233',
      38 => '97',
      39 => '107',
      40 => '108',
      41 => '112',
      42 => '114',
      43 => '115',
      44 => '116',
      45 => '121',
      46 => '125',
      47 => '126',
      48 => '127',
      49 => '132',
      50 => '246',
      51 => '139',
      52 => '146',
      53 => '151',
      54 => '153',
      55 => '159',
      56 => '163',
      57 => '164',
      58 => '245',
      59 => '166',
      60 => '167',
      61 => '178',
      62 => '179',
      63 => '181',
      64 => '185',
      65 => '242',
      66 => '188',
      67 => '194',
      68 => '199',
      69 => '200',
      70 => '219',
      71 => '201',
      72 => '202',
      73 => '206',
      74 => '207',
      75 => '209',
      76 => '211',
      77 => '217',
      78 => '218',
    ),
    22021 => 
    array (
    ),
    22022 => 
    array (
    ),
    22023 => 
    array (
    ),
    31011 => 
    array (
      0 => '',
      1 => '1',
      2 => '2',
      3 => '4',
      4 => '5',
      5 => '8',
      6 => '241',
      7 => '9',
      8 => '10',
      9 => '15',
      10 => '17',
      11 => '219',
      12 => '237',
      13 => '20',
      14 => '21',
      15 => '22',
      16 => '23',
      17 => '25',
      18 => '26',
      19 => '27',
      20 => '28',
      21 => '29',
      22 => '34',
      23 => '35',
      24 => '38',
      25 => '44',
      26 => '45',
      27 => '49',
      28 => '57',
      29 => '60',
      30 => '65',
      31 => '67',
      32 => '72',
      33 => '73',
      34 => '68',
      35 => '75',
      36 => '79',
      37 => '80',
      38 => '82',
      39 => '83',
      40 => '84',
      41 => '85',
      42 => '87',
      43 => '90',
      44 => '95',
      45 => '96',
      46 => '100',
      47 => '101',
      48 => '103',
      49 => '107',
      50 => '111',
      51 => '112',
      52 => '113',
      53 => '116',
      54 => '117',
      55 => '122',
      56 => '123',
      57 => '125',
      58 => '130',
      59 => '132',
      60 => '133',
      61 => '134',
      62 => '136',
      63 => '138',
      64 => '246',
      65 => '139',
      66 => '140',
      67 => '142',
      68 => '144',
      69 => '146',
      70 => '149',
      71 => '150',
      72 => '152',
      73 => '156',
      74 => '157',
      75 => '204',
      76 => '162',
      77 => '164',
      78 => '245',
      79 => '165',
      80 => '168',
      81 => '170',
      82 => '176',
      83 => '226',
      84 => '242',
      85 => '189',
      86 => '192',
      87 => '193',
      88 => '195',
      89 => '194',
      90 => '196',
      91 => '200',
      92 => '198',
      93 => '201',
      94 => '202',
      95 => '208',
      96 => '209',
      97 => '210',
      98 => '211',
      99 => '212',
      100 => '214',
    ),
    31515 => 
    array (
      0 => '',
      1 => '241',
      2 => '246',
      3 => '164',
      4 => '245',
      5 => '194',
      6 => '209',
      7 => '211',
    ),
    34040 => 
    array (
    ),
    34041 => 
    array (
    ),
    34044 => 
    array (
      0 => '',
      1 => '2',
      2 => '4',
      3 => '5',
      4 => '6',
      5 => '8',
      6 => '241',
      7 => '9',
      8 => '10',
      9 => '15',
      10 => '17',
      11 => '20',
      12 => '21',
      13 => '22',
      14 => '23',
      15 => '29',
      16 => '34',
      17 => '35',
      18 => '41',
      19 => '44',
      20 => '49',
      21 => '52',
      22 => '54',
      23 => '57',
      24 => '59',
      25 => '60',
      26 => '65',
      27 => '68',
      28 => '74',
      29 => '79',
      30 => '80',
      31 => '82',
      32 => '84',
      33 => '85',
      34 => '90',
      35 => '94',
      36 => '99',
      37 => '101',
      38 => '103',
      39 => '107',
      40 => '111',
      41 => '112',
      42 => '113',
      43 => '116',
      44 => '122',
      45 => '123',
      46 => '124',
      47 => '125',
      48 => '130',
      49 => '132',
      50 => '133',
      51 => '134',
      52 => '136',
      53 => '137',
      54 => '138',
      55 => '246',
      56 => '139',
      57 => '140',
      58 => '143',
      59 => '144',
      60 => '146',
      61 => '149',
      62 => '231',
      63 => '152',
      64 => '156',
      65 => '164',
      66 => '245',
      67 => '170',
      68 => '176',
      69 => '183',
      70 => '242',
      71 => '187',
      72 => '189',
      73 => '190',
      74 => '191',
      75 => '192',
      76 => '193',
      77 => '194',
      78 => '195',
      79 => '198',
      80 => '219',
      81 => '201',
      82 => '204',
      83 => '208',
      84 => '209',
      85 => '210',
      86 => '211',
      87 => '212',
      88 => '214',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_nombrerepresencs_c.php

 // created: 2023-01-06 08:12:06

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/customer_journey_parent.php

// created: 2023-02-08 11:09:23
VardefManager::createVardef('Cases', 'Case', [
                                'customer_journey_parent',
                        ]);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/contacts_cases_1_Cases.php

// created: 2023-03-06 03:26:09
$dictionary["Case"]["fields"]["contacts_cases_1"] = array (
  'name' => 'contacts_cases_1',
  'type' => 'link',
  'relationship' => 'contacts_cases_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_CASES_1_FROM_CASES_TITLE',
  'id_name' => 'contacts_cases_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["contacts_cases_1_name"] = array (
  'name' => 'contacts_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_cases_1contacts_ida',
  'link' => 'contacts_cases_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["Case"]["fields"]["contacts_cases_1contacts_ida"] = array (
  'name' => 'contacts_cases_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_CASES_1_FROM_CASES_TITLE_ID',
  'id_name' => 'contacts_cases_1contacts_ida',
  'link' => 'contacts_cases_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/leads_cases_1_Cases.php

// created: 2023-03-06 03:28:30
$dictionary["Case"]["fields"]["leads_cases_1"] = array (
  'name' => 'leads_cases_1',
  'type' => 'link',
  'relationship' => 'leads_cases_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'side' => 'right',
  'vname' => 'LBL_LEADS_CASES_1_FROM_CASES_TITLE',
  'id_name' => 'leads_cases_1leads_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["leads_cases_1_name"] = array (
  'name' => 'leads_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_CASES_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_cases_1leads_ida',
  'link' => 'leads_cases_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["Case"]["fields"]["leads_cases_1leads_ida"] = array (
  'name' => 'leads_cases_1leads_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_CASES_1_FROM_CASES_TITLE_ID',
  'id_name' => 'leads_cases_1leads_ida',
  'link' => 'leads_cases_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_contacts_cases_1_name.php

 // created: 2023-03-06 03:33:06
$dictionary['Case']['fields']['contacts_cases_1_name']['audited']=true;
$dictionary['Case']['fields']['contacts_cases_1_name']['massupdate']=false;
$dictionary['Case']['fields']['contacts_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['contacts_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['contacts_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['contacts_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['contacts_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['contacts_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['contacts_cases_1_name']['vname']='LBL_CONTACTS_CASES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_contacts_cases_1contacts_ida.php

 // created: 2023-03-06 03:33:06
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['name']='contacts_cases_1contacts_ida';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['type']='id';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['source']='non-db';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['vname']='LBL_CONTACTS_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['id_name']='contacts_cases_1contacts_ida';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['link']='contacts_cases_1';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['table']='contacts';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['module']='Contacts';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['rname']='id';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['reportable']=false;
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['side']='right';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['massupdate']=false;
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['hideacl']=true;
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['audited']=false;
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_leads_cases_1leads_ida.php

 // created: 2023-03-06 03:34:53
$dictionary['Case']['fields']['leads_cases_1leads_ida']['name']='leads_cases_1leads_ida';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['type']='id';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['source']='non-db';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['vname']='LBL_LEADS_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['id_name']='leads_cases_1leads_ida';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['link']='leads_cases_1';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['table']='leads';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['module']='Leads';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['rname']='id';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['reportable']=false;
$dictionary['Case']['fields']['leads_cases_1leads_ida']['side']='right';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['massupdate']=false;
$dictionary['Case']['fields']['leads_cases_1leads_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['hideacl']=true;
$dictionary['Case']['fields']['leads_cases_1leads_ida']['audited']=false;
$dictionary['Case']['fields']['leads_cases_1leads_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_leads_cases_1_name.php

 // created: 2023-03-06 03:34:53
$dictionary['Case']['fields']['leads_cases_1_name']['audited']=true;
$dictionary['Case']['fields']['leads_cases_1_name']['massupdate']=false;
$dictionary['Case']['fields']['leads_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['leads_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['leads_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['leads_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['leads_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['leads_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['leads_cases_1_name']['vname']='LBL_LEADS_CASES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_source.php

 // created: 2023-05-10 16:24:00
$dictionary['Case']['fields']['source']['len']=100;
$dictionary['Case']['fields']['source']['required']=true;
$dictionary['Case']['fields']['source']['audited']=true;
$dictionary['Case']['fields']['source']['massupdate']=true;
$dictionary['Case']['fields']['source']['options']='source_cases_list_c';
$dictionary['Case']['fields']['source']['comments']='An indicator of how the bug was entered (ex: via web, email, etc.)';
$dictionary['Case']['fields']['source']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['source']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['source']['merge_filter']='disabled';
$dictionary['Case']['fields']['source']['calculated']=false;
$dictionary['Case']['fields']['source']['dependency']=false;
$dictionary['Case']['fields']['source']['default']='';
$dictionary['Case']['fields']['source']['hidemassupdate']=false;
$dictionary['Case']['fields']['source']['visibility_grid']=array (
);

 
?>

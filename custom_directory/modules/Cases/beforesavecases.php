<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class beforesavecases
{
	function before_save($bean, $event, $arguments)
	{	
		//Capturar el cierre de gestión del Leads (Gestionado Call, En Proceso Comercial, Sin Interés). Casos (Cancelado, Cerrado).
		try{

			//Validacion gestion coercial solucion directa, requerimiento: https://sasaconsultoria.sugarondemand.com/index.php#Tasks/e4ccb6fc-85ef-11ec-a51d-06f8a63ac887
			if ($bean->sasa_motivo_c == "141" || $bean->sasa_motivo_c == "142" || $bean->sasa_motivo_c == "143") {
				//Si el motivo es informacion vehiculos nuevos o informacion vehiculos usados, poblar los siguientes 4 campos de la gestion comercial
				$bean->sasa_fechainiciogestioncs_c = $bean->date_entered;
				$bean->sasa_fechafingestioncs_c = $bean->date_entered;
				$bean->sasa_estadocontacto_c = "CONTACTADO";
				$bean->sasa_fechacontacto_c = $bean->date_entered;
				$bean->sasa_numerogestiones_c = "1";
			}

			$statusRecord = array("Rejected","Closed");
			$GLOBALS['log']->security("LOGICbefore ".$bean->sasa_estadocomercial_c);
			if ($bean->sasa_estadocomercial_c != "" && ($bean->sasa_fechafingestioncs_c == null || $bean->sasa_fechafingestioncs_c=="")) {
				$mifecha = new DateTime(); 
                
				$bean->sasa_fechafingestioncs_c = $mifecha->format('Y-m-d H:i:s');
				//$bean->sasa_fechafinciogestioncs_c = date("Y-m-d");
			}
			//Trasladar el comentario de la tarea de enviar notificación al campo Comentario/leads, Observación/casos. Según control proceso 2
			if (($bean->fetched_row['sasa_estadocomercial_c'] == "" || $bean->fetched_row['sasa_estadocomercial_c']==null) && ($bean->sasa_estadocomercial_c != "" && $bean->sasa_estadocomercial_c != null)) {
				if (!empty($bean->id)) {
					//query para obtener la documentación de la última tarea
					$query_get_doc_lastcall = "SELECT id, description, name FROM calls INNER JOIN calls_cstm ON calls.id=calls_cstm.id_c WHERE (calls_cstm.sasa_controlproceso_c IS NOT NULL OR calls_cstm.sasa_controlproceso_c != '') AND (sasa_tipo_de_llamada_c='INFORMACION_COMERCIAL' OR sasa_tipo_de_llamada_c='SE_GENERO_COTIZACION') AND calls.parent_id='{$bean->id}' AND deleted=0 ORDER BY calls.date_entered DESC LIMIT 1";
					$execute_query_get_doc_lastcall = $GLOBALS['db']->query($query_get_doc_lastcall);
					$resultquery_lastCall = $GLOBALS['db']->fetchByAssoc($execute_query_get_doc_lastcall);
					if (!empty($resultquery_lastCall['description'])) {
						$bean->description .= "\nGestión: ".$resultquery_lastCall['description'];
					}
				}
			}
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Cases beforesavecases: ".$e->getMessage()); 
		}
	}
}
?>
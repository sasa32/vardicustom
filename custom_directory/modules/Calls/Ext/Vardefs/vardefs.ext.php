<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/rli_link_workflow.php

$dictionary['Call']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/audit.php

$dictionary['Call']['audited'] = true;
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sasa_puntos_de_ventas_calls_1_Calls.php

// created: 2020-06-19 14:28:13
$dictionary["Call"]["fields"]["sasa_puntos_de_ventas_calls_1"] = array (
  'name' => 'sasa_puntos_de_ventas_calls_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_calls_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'side' => 'right',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_CALLS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
  'link-type' => 'one',
);
$dictionary["Call"]["fields"]["sasa_puntos_de_ventas_calls_1_name"] = array (
  'name' => 'sasa_puntos_de_ventas_calls_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_calls_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'name',
);
$dictionary["Call"]["fields"]["sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida"] = array (
  'name' => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_CALLS_TITLE_ID',
  'id_name' => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_calls_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_name.php

 // created: 2020-06-05 13:44:42
$dictionary['Call']['fields']['name']['audited']=true;
$dictionary['Call']['fields']['name']['massupdate']=false;
$dictionary['Call']['fields']['name']['comments']='Brief description of the call';
$dictionary['Call']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['name']['merge_filter']='disabled';
$dictionary['Call']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.41',
  'searchable' => true,
);
$dictionary['Call']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_status.php

 // created: 2020-09-14 15:55:52
$dictionary['Call']['fields']['status']['audited']=true;
$dictionary['Call']['fields']['status']['massupdate']=true;
$dictionary['Call']['fields']['status']['hidemassupdate']=false;
$dictionary['Call']['fields']['status']['comments']='The status of the call (Held, Not Held, etc.)';
$dictionary['Call']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['status']['merge_filter']='disabled';
$dictionary['Call']['fields']['status']['full_text_search']=array (
);
$dictionary['Call']['fields']['status']['calculated']=false;
$dictionary['Call']['fields']['status']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_direction.php

 // created: 2020-09-14 19:46:06
$dictionary['Call']['fields']['direction']['default']='Outbound';
$dictionary['Call']['fields']['direction']['audited']=true;
$dictionary['Call']['fields']['direction']['massupdate']=true;
$dictionary['Call']['fields']['direction']['hidemassupdate']=false;
$dictionary['Call']['fields']['direction']['comments']='Indicates whether call is inbound or outbound';
$dictionary['Call']['fields']['direction']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['direction']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['direction']['merge_filter']='disabled';
$dictionary['Call']['fields']['direction']['calculated']=false;
$dictionary['Call']['fields']['direction']['dependency']=false;
$dictionary['Call']['fields']['direction']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_date_start.php

 // created: 2020-09-14 15:54:07
$dictionary['Call']['fields']['date_start']['audited']=true;
$dictionary['Call']['fields']['date_start']['hidemassupdate']=false;
$dictionary['Call']['fields']['date_start']['comments']='Date in which call is schedule to (or did) start';
$dictionary['Call']['fields']['date_start']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_start']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['date_start']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_start']['full_text_search']=array (
);
$dictionary['Call']['fields']['date_start']['calculated']=false;
$dictionary['Call']['fields']['date_start']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_date_end.php

 // created: 2020-10-14 19:32:12
$dictionary['Call']['fields']['date_end']['audited']=false;
$dictionary['Call']['fields']['date_end']['hidemassupdate']=false;
$dictionary['Call']['fields']['date_end']['comments']='Date is which call is scheduled to (or did) end';
$dictionary['Call']['fields']['date_end']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_end']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['date_end']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_end']['calculated']=false;
$dictionary['Call']['fields']['date_end']['enable_range_search']='1';
$dictionary['Call']['fields']['date_end']['required']=false;
$dictionary['Call']['fields']['date_end']['full_text_search']=array (
);
$dictionary['Call']['fields']['date_end']['group_label']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2020-09-14 15:52:55
$dictionary['Call']['fields']['date_entered']['audited']=false;
$dictionary['Call']['fields']['date_entered']['comments']='Date record created';
$dictionary['Call']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_entered']['calculated']=false;
$dictionary['Call']['fields']['date_entered']['enable_range_search']='1';
$dictionary['Call']['fields']['date_entered']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2020-06-05 13:45:10
$dictionary['Call']['fields']['date_modified']['audited']=true;
$dictionary['Call']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Call']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_modified']['calculated']=false;
$dictionary['Call']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_estadocontacto_c.php

 // created: 2021-12-03 10:30:01
$dictionary['Call']['fields']['sasa_estadocontacto_c']['labelValue']='Estado del Contacto borrar';
$dictionary['Call']['fields']['sasa_estadocontacto_c']['dependency']='';
$dictionary['Call']['fields']['sasa_estadocontacto_c']['required_formula']='';
$dictionary['Call']['fields']['sasa_estadocontacto_c']['readonly_formula']='';
$dictionary['Call']['fields']['sasa_estadocontacto_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_fechainiciogestion_c.php

 // created: 2021-12-03 10:31:01
$dictionary['Call']['fields']['sasa_fechainiciogestion_c']['labelValue']='Fecha Inicio de Gestión borrar';
$dictionary['Call']['fields']['sasa_fechainiciogestion_c']['enforced']='';
$dictionary['Call']['fields']['sasa_fechainiciogestion_c']['dependency']='';
$dictionary['Call']['fields']['sasa_fechainiciogestion_c']['required_formula']='';
$dictionary['Call']['fields']['sasa_fechainiciogestion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_description.php

 // created: 2021-12-03 10:34:52
$dictionary['Call']['fields']['description']['required']=true;
$dictionary['Call']['fields']['description']['audited']=false;
$dictionary['Call']['fields']['description']['massupdate']=false;
$dictionary['Call']['fields']['description']['hidemassupdate']=false;
$dictionary['Call']['fields']['description']['comments']='Full text of the note';
$dictionary['Call']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['description']['merge_filter']='disabled';
$dictionary['Call']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.54',
  'searchable' => true,
);
$dictionary['Call']['fields']['description']['calculated']=false;
$dictionary['Call']['fields']['description']['rows']='6';
$dictionary['Call']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_controlproceso_c.php

 // created: 2021-11-30 14:04:02

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_tipo_de_llamada_c.php

 // created: 2021-11-30 14:04:01
$dictionary['Call']['fields']['sasa_tipo_de_llamada_c']['labelValue']='Tipo de llamada';
$dictionary['Call']['fields']['sasa_tipo_de_llamada_c']['dependency']='equal($direction,"Outbound")';
$dictionary['Call']['fields']['sasa_tipo_de_llamada_c']['required_formula']='';
$dictionary['Call']['fields']['sasa_tipo_de_llamada_c']['readonly_formula']='';
$dictionary['Call']['fields']['sasa_tipo_de_llamada_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_resultado_c.php

 // created: 2022-01-26 13:31:48
$dictionary['Call']['fields']['sasa_resultado_c']['labelValue']='Resultado';
$dictionary['Call']['fields']['sasa_resultado_c']['dependency']='';
$dictionary['Call']['fields']['sasa_resultado_c']['required_formula']='';
$dictionary['Call']['fields']['sasa_resultado_c']['readonly_formula']='';
$dictionary['Call']['fields']['sasa_resultado_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipo_de_llamada_c',
  'values' => 
  array (
    '' => 
    array (
      0 => '',
    ),
    'CONFIRMACION_CIERRE_PQRFS' => 
    array (
      0 => '',
      1 => 'NO CONTESTA',
      2 => 'LLAMAR DESPUES',
      3 => 'CASO CERRADO',
      4 => 'CASO REABIERTO',
      5 => 'AVANCE PARCIAL',
      6 => 'SEGUIMIENTO A CLIENTE',
      7 => 'SEGUIMIENTO A REPRESENTANTE',
      8 => 'INGRESO DE CASO',
      9 => 'CASO REPETIDO',
      10 => 'CASO ALTO IMPACTO',
      11 => 'CASO REDES',
      12 => 'CASO LATAM',
      13 => 'APRUEBA EL CIERRE',
      14 => 'FUERA DE SERVICIO',
      15 => 'NO VOLVER A LLAMAR',
      16 => 'NUMERO EQUIVOCADO',
    ),
    'CURSO_MECANICA' => 
    array (
      0 => '',
      1 => 'CONFIRMA',
      2 => 'FUERA DE SERVICIO',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'INTERESADO',
      5 => 'LLAMAR DESPUES',
      6 => 'NO CONFIRMA',
      7 => 'NO VOLVER A LLAMAR',
      8 => 'NUMERO EQUIVOCADO',
      9 => 'PERDIDA DE VEHICULO',
      10 => 'VENTA DE VEHICULO',
    ),
    'CONVOCATORIA_DE_MANTENIMIENTO_RMP' => 
    array (
      0 => '',
      1 => 'AGENDADO',
      2 => 'CIUDAD SIN COBERTURA',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'DATO ERRADO',
      5 => 'INTERESADO',
      6 => 'LLAMAR DESPUES',
      7 => 'NO APLICA VEH OTRA MARCA',
      8 => 'NO REALIZA SERVICIOS CON NISSAN',
      9 => 'PERDIDA TOTAL',
      10 => 'SE COMUNICARA PARA AGENDAR LA CITA',
      11 => 'SIN GESTION',
      12 => 'VENTA DE VEHICULO',
      13 => 'YA REALIZO SERVICIO',
      14 => 'YA TIENE CITA PROGRAMADA',
      15 => 'NO CUMPLE KM',
      16 => 'REPUESTO DE IMPORTACION',
      17 => 'NO INTERESADO',
      18 => 'NO PERMITE VER COTIZACION',
      19 => 'VENTA TRASLADADA',
      20 => 'NO DAN INFORMACION',
      21 => 'NO VOLVER A LLAMAR',
      22 => 'PERDIDA DE VEHICULO',
      23 => 'CANCELADA',
      24 => 'NO TIENE CITA PROGRAMADA',
      25 => 'REPROGRAMACION',
      26 => 'DESISTE',
    ),
    'CONVOCATORIA_PRIMER_SERVICIO' => 
    array (
      0 => '',
      1 => 'AGENDADO',
      2 => 'CIUDAD SIN COBERTURA',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'DATO ERRADO',
      5 => 'INTERESADO',
      6 => 'LLAMAR DESPUES',
      7 => 'NO APLICA VEH OTRA MARCA',
      8 => 'NO REALIZA SERVICIOS CON NISSAN',
      9 => 'PERDIDA TOTAL',
      10 => 'SE COMUNICARA PARA AGENDAR LA CITA',
      11 => 'SIN GESTION',
      12 => 'VENTA DE VEHICULO',
      13 => 'YA REALIZO SERVICIO',
      14 => 'YA TIENE CITA PROGRAMADA',
      15 => 'NO CUMPLE KM',
      16 => 'REPUESTO DE IMPORTACION',
      17 => 'NO INTERESADO',
      18 => 'NO PERMITE VER COTIZACION',
      19 => 'VENTA TRASLADADA',
      20 => 'NO DAN INFORMACION',
      21 => 'NO VOLVER A LLAMAR',
      22 => 'PERDIDA DE VEHICULO',
      23 => 'CANCELADA',
      24 => 'NO TIENE CITA PROGRAMADA',
      25 => 'REPROGRAMACION',
      26 => 'DESISTE',
    ),
    'ENCUESTA' => 
    array (
      0 => '',
      1 => 'ENCUESTADO',
      2 => 'NUMERO EQUIVOCADO',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'TELEFONO FUERA DE SERVICIO',
      5 => 'LLAMAR DESPUES',
      6 => 'DESISTE DE LA ENCUESTA',
      7 => 'YA REALIZO LA ENCUESTA',
      8 => 'AUN NO HAN TERMINADO LA REPARACION',
      9 => 'NO APARECE EN SUGAR',
      10 => 'NO INGRESO EL VEHICULO',
      11 => 'SIN DATOS',
      12 => 'VENTA DE VEHICULO',
      13 => 'NO REALIZO LA COMPRA DEL REPUESTO',
      14 => 'NO VOLVER A LLAMAR',
      15 => 'PERDIDA DE VEHICULO',
    ),
    'ESTADO_DEL_VEHICULO' => 
    array (
      0 => '',
      1 => 'GESTIONADO',
    ),
    'ESTUDIO_DE_PRECIOS_AUTOPARTES_Y_LUBRICANTES' => 
    array (
      0 => '',
      1 => 'FUERA DE SERVICIO',
      2 => 'GESTIONADO',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'LLAMAR DESPUES',
      5 => 'NO VOLVER A LLAMAR',
      6 => 'NUMERO EQUIVOCADO',
    ),
    'ESTUDIO_ DE_ PRECIOS_EN_MANO_DE_OBRA' => 
    array (
      0 => '',
      1 => 'FUERA DE SERVICIO',
      2 => 'GESTIONADO',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'NUMERO EQUIVOCADO',
    ),
    'EVENTOS_VITRINAS_O_TALLERES' => 
    array (
      0 => '',
      1 => 'CONFIRMA ASISTENCIA',
      2 => 'NO ASISTE',
      3 => 'LLAMAR DESPUES',
      4 => 'CONTACTO NO EFECTIVO',
      5 => 'DATO ERRADO',
      6 => 'FALLECIDO',
      7 => 'YA COMPRÓ NISSAN',
      8 => 'YA COMPRÓ OTRA MARCA',
      9 => 'INTERESADO',
      10 => 'NO VOLVER A LLAMAR',
      11 => 'POSTERGA COMPRA',
      12 => 'USADO OTRA MARCA',
    ),
    'GESTION WEB' => 
    array (
      0 => '',
      1 => 'GESTIONADO',
      2 => 'NO GESTIONADO',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'LLAMAR DESPUES',
      5 => 'NO REALIZA SERVICIOS CON NISSAN',
      6 => 'NO VOLVER A LLAMAR',
      7 => 'NUMERO EQUIVOCADO',
    ),
    'INFORMACION_COMERCIAL' => 
    array (
      0 => '',
      1 => 'SI',
      2 => 'NO',
      3 => 'NO CONTACTADO',
    ),
    'INVITACION_A_VITRINAS_A_NIVEL_NACIONAL_PREVENTA' => 
    array (
      0 => '',
      1 => 'CONFIRMA ASISTENCIA',
      2 => 'NO ASISTE',
      3 => 'LLAMAR DESPUES',
      4 => 'CONTACTO NO EFECTIVO',
      5 => 'DATO ERRADO',
      6 => 'FALLECIDO',
      7 => 'YA COMPRÓ NISSAN',
      8 => 'YA COMPRÓ OTRA MARCA',
      9 => 'INTERESADO',
      10 => 'NO VOLVER A LLAMAR',
      11 => 'POSTERGA COMPRA',
      12 => 'USADO OTRA MARCA',
    ),
    'INVITACION_PERIODISTAS' => 
    array (
      0 => '',
      1 => 'CONFIRMA DATOS Y ASISTENCIA ',
      2 => 'NO ASISTE',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'DATO ERRADO',
      5 => 'LLAMAR DESPUES',
      6 => 'NO VOLVER A LLAMAR',
    ),
    'INVITACION_EVENTOS_CORPORATIVOS' => 
    array (
      0 => '',
      1 => 'CONFIRMA DATOS Y ASISTENCIA ',
      2 => 'NO ASISTE',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'DATO ERRADO',
      5 => 'LLAMAR DESPUES',
      6 => 'NO VOLVER A LLAMAR',
    ),
    'LANZAMIENTO_PRODUCTOS' => 
    array (
      0 => '',
      1 => 'CONFIRMA ASISTENCIA',
      2 => 'NO ASISTE',
      3 => 'LLAMAR DESPUES',
      4 => 'CONTACTO NO EFECTIVO',
      5 => 'DATO ERRADO',
      6 => 'FALLECIDO',
      7 => 'YA COMPRÓ NISSAN',
      8 => 'YA COMPRÓ OTRA MARCA',
      9 => 'INTERESADO',
      10 => 'NO VOLVER A LLAMAR',
      11 => 'POSTERGA COMPRA',
      12 => 'USADO OTRA MARCA',
    ),
    'PROTOCOLO_VITRINAS' => 
    array (
      0 => 'NO CUMPLE PROTOCOLO',
      1 => 'CUMPLE PROTOCOLO',
      2 => 'CONTACTO NO EFECTIVO',
      3 => '',
    ),
    'PROTOCOLO_ TALLERES' => 
    array (
      0 => 'NO CUMPLE PROTOCOLO',
      1 => 'CUMPLE PROTOCOLO',
      2 => 'CONTACTO NO EFECTIVO',
      3 => '',
    ),
    'RECORDATORIO_CITA_TALLER_PROGRAMADA' => 
    array (
      0 => '',
      1 => 'AGENDADO',
      2 => 'CIUDAD SIN COBERTURA',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'DATO ERRADO',
      5 => 'INTERESADO',
      6 => 'LLAMAR DESPUES',
      7 => 'NO APLICA VEH OTRA MARCA',
      8 => 'NO REALIZA SERVICIOS CON NISSAN',
      9 => 'PERDIDA TOTAL',
      10 => 'SE COMUNICARA PARA AGENDAR LA CITA',
      11 => 'SIN GESTION',
      12 => 'VENTA DE VEHICULO',
      13 => 'YA REALIZO SERVICIO',
      14 => 'YA TIENE CITA PROGRAMADA',
      15 => 'NO CUMPLE KM',
      16 => 'REPUESTO DE IMPORTACION',
      17 => 'NO INTERESADO',
      18 => 'NO PERMITE VER COTIZACION',
      19 => 'VENTA TRASLADADA',
      20 => 'NO DAN INFORMACION',
      21 => 'NO VOLVER A LLAMAR',
      22 => 'PERDIDA DE  VEHICULO',
      23 => 'CANCELADA',
      24 => 'NO TIENE CITA PROGRAMADA',
      25 => 'REPROGRAMACION',
      26 => 'DESISTE',
    ),
    'REPROGRAMACION_CITAS_NO_CUMPLIDAS' => 
    array (
      0 => '',
      1 => 'AGENDADO',
      2 => 'CIUDAD SIN COBERTURA',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'DATO ERRADO',
      5 => 'INTERESADO',
      6 => 'LLAMAR DESPUES',
      7 => 'NO APLICA VEH OTRA MARCA',
      8 => 'NO REALIZA SERVICIOS CON NISSAN',
      9 => 'PERDIDA TOTAL',
      10 => 'SE COMUNICARA PARA AGENDAR LA CITA',
      11 => 'SIN GESTION',
      12 => 'VENTA DE VEHICULO',
      13 => 'YA REALIZO SERVICIO',
      14 => 'YA TIENE CITA PROGRAMADA',
      15 => 'NO CUMPLE KM',
      16 => 'REPUESTO DE IMPORTACION',
      17 => 'NO INTERESADO',
      18 => 'NO PERMITE VER COTIZACION',
      19 => 'VENTA TRASLADADA',
      20 => 'NO DAN INFORMACION',
      21 => 'NO VOLVER A LLAMAR',
      22 => 'PERDIDA DE VEHICULO',
      23 => 'CANCELADA',
      24 => 'NO TIENE CITA PROGRAMADA',
      25 => 'REPROGRAMACION',
      26 => 'DESISTE',
    ),
    'SEGUIMIENTO_PQRFS_ A_ CLIENTE' => 
    array (
      0 => '',
      1 => 'NO CONTESTA',
      2 => 'LLAMAR DESPUES',
      3 => 'CASO CERRADO',
      4 => 'CASO REABIERTO',
      5 => 'AVANCE PARCIAL',
      6 => 'SEGUIMIENTO A CLIENTE',
      7 => 'SEGUIMIENTO A REPRESENTANTE',
      8 => 'INGRESO DE CASO',
      9 => 'CASO REPETIDO',
      10 => 'CASO ALTO IMPACTO',
      11 => 'CASO REDES',
      12 => 'CASO LATAM',
      13 => 'APRUEBA EL CIERRE',
      14 => 'FUERA DE SERVICIO',
      15 => 'NO VOLVER A LLAMAR',
      16 => 'NUMERO EQUIVOCADO',
    ),
    'SEGUIMIENTO_PQRFS_A REPRESENTANTES' => 
    array (
      0 => '',
      1 => 'NO CONTESTA',
      2 => 'LLAMAR DESPUES',
      3 => 'CASO CERRADO',
      4 => 'CASO REABIERTO',
      5 => 'AVANCE PARCIAL',
      6 => 'SEGUIMIENTO A CLIENTE',
      7 => 'SEGUIMIENTO A REPRESENTANTE',
      8 => 'INGRESO DE CASO',
      9 => 'CASO REPETIDO',
      10 => 'CASO ALTO IMPACTO',
      11 => 'CASO REDES',
      12 => 'CASO LATAM',
      13 => 'APRUEBA EL CIERRE',
      14 => 'FUERA DE SERVICIO',
      15 => 'NO VOLVER A LLAMAR',
      16 => 'NUMERO EQUIVOCADO',
    ),
    'SE_GENERO_COTIZACION' => 
    array (
      0 => '',
      1 => 'SI',
      2 => 'NO',
      3 => 'NO CONTACTADO',
    ),
    'SE_GENERO_CITA_TALLER' => 
    array (
      0 => '',
      1 => 'SI',
      2 => 'NO',
      3 => 'NO CONTACTADO',
    ),
    'CONVOCATORIA_LOCAL' => 
    array (
      0 => '',
      1 => 'AGENDADO',
      2 => 'CIUDAD SIN COBERTURA',
      3 => 'CONTACTO NO EFECTIVO',
      4 => 'DATO ERRADO',
      5 => 'INTERESADO',
      6 => 'LLAMAR DESPUES',
      7 => 'NO APLICA VEH OTRA MARCA',
      8 => 'NO REALIZA SERVICIOS CON NISSAN',
      9 => 'PERDIDA TOTAL',
      10 => 'SE COMUNICARA PARA AGENDAR LA CITA',
      11 => 'SIN GESTION',
      12 => 'VENTA DE VEHICULO',
      13 => 'YA REALIZO SERVICIO',
      14 => 'YA TIENE CITA PROGRAMADA',
      15 => 'NO CUMPLE KM',
      16 => 'REPUESTO DE IMPORTACION',
      17 => 'NO INTERESADO',
      18 => 'NO PERMITE VER COTIZACION',
      19 => 'VENTA TRASLADADA',
      20 => 'NO DAN INFORMACION',
      21 => 'NO VOLVER A LLAMAR',
      22 => 'PERDIDA DE  VEHICULO',
      23 => 'CANCELADA',
      24 => 'NO TIENE CITA PROGRAMADA',
      25 => 'REPROGRAMACION',
      26 => 'DESISTE',
    ),
    'CONVOCATORIA_REPUESTOS' => 
    array (
      0 => '',
      1 => 'REPUESTO DE IMPORTACION',
      2 => 'NO GESTIONADO',
      3 => 'NO PERMITE VER COTIZACION',
      4 => 'VENTA TRASLADADA',
      5 => 'NO DAN INFORMACION',
      6 => 'NO VOLVER A LLAMAR',
      7 => 'CONTACTO NO EFECTIVO',
      8 => 'DATO ERRADO',
      9 => 'AGENDADO',
    ),
    'DESTRATES' => 
    array (
      0 => '',
      1 => 'PRESTAMO',
      2 => 'VENTA DE SU CARRO ACTUAL',
      3 => 'INVERSION DE VIVIENDA',
      4 => 'NO LE MANTUVIERON LA RETOMA',
      5 => 'VALOR RESTANTE',
      6 => 'MENOS COSTO',
      7 => 'RETOMA',
      8 => 'CREDITO CON REQUISITO D (CODEUDOR)',
      9 => 'DECIDIO NO CAMBIAR DE MODELO',
      10 => 'NO INFORMA',
    ),
    'INVESTIGACION_MERCADOS' => 
    array (
      0 => '',
      1 => 'NUMERO EQUIVOCADO',
      2 => 'CONTACTO NO EFECTIVO',
      3 => 'ENCUESTADO',
      4 => 'TELEFONO FUERA DE SERVICIO',
      5 => 'LLAMAR DESPUES',
      6 => 'DESISTE DE LA ENCUESTA',
      7 => 'YA REALIZO LA ENCUESTA',
      8 => 'SIN DATOS',
      9 => 'VENTA DE VEHICULO',
    ),
  ),
);

 
?>

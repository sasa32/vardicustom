<?php
// created: 2022-10-31 10:59:53
$viewdefs['Calls']['mobile']['view']['edit'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 'name',
        1 => 
        array (
          'name' => 'date',
          'type' => 'fieldset',
          'related_fields' => 
          array (
            0 => 'date_start',
            1 => 'date_end',
          ),
          'label' => 'LBL_START_AND_END_DATE_DETAIL_VIEW',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_start',
            ),
            1 => 
            array (
              'name' => 'date_end',
              'required' => true,
              'readonly' => false,
            ),
          ),
        ),
        2 => 'status',
        3 => 'direction',
        4 => 
        array (
          'name' => 'sasa_tipo_de_llamada_c',
          'label' => 'LBL_SASA_TIPO_DE_LLAMADA_C',
        ),
        5 => 
        array (
          'name' => 'sasa_resultado_c',
          'label' => 'LBL_SASA_RESULTADO_C',
        ),
        6 => 'description',
        7 => 'parent_name',
        8 => 
        array (
          'name' => 'sasa_puntos_de_ventas_calls_1_name',
          'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
        ),
        9 => 
        array (
          'name' => 'reminder',
          'type' => 'fieldset',
          'orientation' => 'horizontal',
          'related_fields' => 
          array (
            0 => 'reminder_checked',
            1 => 'reminder_time',
          ),
          'label' => 'LBL_REMINDER',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'reminder_checked',
            ),
            1 => 
            array (
              'name' => 'reminder_time',
              'type' => 'enum',
              'options' => 'reminder_time_options',
            ),
          ),
        ),
        10 => 
        array (
          'name' => 'email_reminder',
          'type' => 'fieldset',
          'orientation' => 'horizontal',
          'related_fields' => 
          array (
            0 => 'email_reminder_checked',
            1 => 'email_reminder_time',
          ),
          'label' => 'LBL_EMAIL_REMINDER',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'email_reminder_checked',
            ),
            1 => 
            array (
              'name' => 'email_reminder_time',
              'type' => 'enum',
              'options' => 'reminder_time_options',
            ),
          ),
        ),
        11 => 'assigned_user_name',
        12 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        13 => 
        array (
          'name' => 'created_by_name',
          'readonly' => true,
          'label' => 'LBL_CREATED',
        ),
        14 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
        15 => 
        array (
          'name' => 'modified_by_name',
          'readonly' => true,
          'label' => 'LBL_MODIFIED',
        ),
        16 => 'team_name',
      ),
    ),
  ),
);
<?php
// created: 2022-10-31 10:59:53
$viewdefs['Calls']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        1 => 'date_start',
        2 => 
        array (
          'name' => 'date_end',
          'comment' => 'Date is which call is scheduled to (or did) end',
          'studio' => 
          array (
            'recordview' => false,
            'wirelesseditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_CALENDAR_END_DATE',
        ),
        3 => 'status',
        4 => 'direction',
        5 => 
        array (
          'name' => 'sasa_tipo_de_llamada_c',
          'label' => 'LBL_SASA_TIPO_DE_LLAMADA_C',
        ),
        6 => 
        array (
          'name' => 'sasa_resultado_c',
          'label' => 'LBL_SASA_RESULTADO_C',
        ),
        7 => 'description',
        8 => 'parent_name',
        9 => 
        array (
          'name' => 'sasa_puntos_de_ventas_calls_1_name',
          'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
        ),
        10 => 'assigned_user_name',
        11 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        12 => 
        array (
          'name' => 'created_by_name',
          'readonly' => true,
          'label' => 'LBL_CREATED',
        ),
        13 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
        14 => 
        array (
          'name' => 'modified_by_name',
          'readonly' => true,
          'label' => 'LBL_MODIFIED',
        ),
      ),
    ),
  ),
);
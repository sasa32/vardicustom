<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_CallsAftersave
{
	function after_save($bean, $event, $arguments)
	{	
		/*
		Tarea 17435 https://sasaconsultoria.sugarondemand.com/index.php#Tasks/34a87d98-46fb-11ec-b137-02dfd714a754
		*/
		try{

			if (!isset($bean->logic_hooks_calls_after_save_ignore_update) || $bean->logic_hooks_calls_after_save_ignore_update === false){//antiloop
				$bean->logic_hooks_calls_after_save_ignore_update = true;//antiloop

				global $db;
				
				//Desarrollo para actualizar el campo contactado en el lead y/o Caso. dependiendo del campo en tareas.
				if (($bean->sasa_resultado_c=="SI" || $bean->sasa_resultado_c=="NO") && ($bean->parent_type=="Leads" || $bean->parent_type=="Cases")) {
					$RecordRelated = BeanFactory::retrieveBean($bean->parent_type, $bean->parent_id, array('disable_row_level_security' => true));
					if ($RecordRelated->sasa_fechacontacto_c=="" || $RecordRelated->sasa_fechacontacto_c==null) {
						$mifecha = new DateTime(); 
	                    //$mifecha->modify('+5 hours');
						$RecordRelated->sasa_fechacontacto_c = $mifecha->format('Y-m-d H:i:s');
						$RecordRelated->sasa_estadocontacto_c = "CONTACTADO";
						$RecordRelated->save();
					}
					
				}

				//Desarrollo para contar el número de gestiones que se le hicieron al lead/Caso.
				if ($bean->parent_type=="Leads" || $bean->parent_type=="Cases") {
					$Modulo = strtolower($bean->parent_type);
					$querycountTasks = "SELECT COUNT(*) AS conteo FROM(SELECT calls.id AS conteo FROM calls INNER JOIN $Modulo ON calls.parent_id={$Modulo}.id INNER JOIN calls_cstm ON calls.id=calls_cstm.id_c WHERE (calls_cstm.sasa_controlproceso_c IS NOT NULL OR calls_cstm.sasa_controlproceso_c!='') AND calls.deleted=0 AND {$Modulo}.deleted=0 AND calls.parent_id='{$bean->parent_id}' AND (calls.status='Held' || calls.status='Not Held') )tconteo";
					$GLOBALS['log']->security("QUERY ".$querycountTasks);
					
					$resultquery = $db->query($querycountTasks);
					$conteoquery = $GLOBALS['db']->fetchByAssoc($resultquery);
					$RecordRelated = BeanFactory::retrieveBean($bean->parent_type, $bean->parent_id, array('disable_row_level_security' => true));
					$RecordRelated->sasa_numerogestiones_c = $conteoquery['conteo'];
					$RecordRelated->save();
				}

				//Trasladar el comentario de la tarea de enviar notificación al campo Comentario/leads, Observación/casos. Según control proceso 2
				/*$querydesccall = "SELECT * FROM calls INNER JOIN calls_cstm ON calls.id=calls_cstm.id_c WHERE (calls_cstm.sasa_controlproceso_c IS NOT NULL OR calls_cstm.sasa_controlproceso_c != '') AND (sasa_tipo_de_llamada_c='INFORMACION_COMERCIAL' OR sasa_tipo_de_llamada_c='SE_GENERO_COTIZACION') AND parent_id='{$bean->parent_id}' ORDER BY `calls`.`date_entered` DESC LIMIT 1";
				$resultquerydescall = $db->query($querydesccall);
				$desccall = $GLOBALS['db']->fetchByAssoc($resultquerydescall);
				if ($desccall!=null) {
					if (!empty($bean->description) && $bean->description == $desccall['description']) {
						$GLOBALS['log']->security("WFPOR AQUI ". $bean->description);
						$RecordRelated = BeanFactory::retrieveBean($bean->parent_type, $bean->parent_id, array('disable_row_level_security' => true));
						$RecordRelated->description .= "\n".$desccall['description'];
						$RecordRelated->save();
					}
				}*/

			}

			
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Notas: ".$e->getMessage()); 
		}
	}
}
?>
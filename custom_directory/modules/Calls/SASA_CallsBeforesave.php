<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_CallsBeforesave
{
	function before_save($bean, $event, $arguments)
	{	
		/*
		Tarea 17435 https://sasaconsultoria.sugarondemand.com/index.php#Tasks/34a87d98-46fb-11ec-b137-02dfd714a754
		*/
		try{
			//Capturar en el campo Fecha de leads/casos la fecha del cambio de estado de la primera tarea. control proceso=1
			$listctrlproceso = array("1","4","7");
			if (in_array($bean->sasa_controlproceso_c, $listctrlproceso) && ($bean->parent_type=="Leads" || $bean->parent_type=="Cases")) {
				if ($bean->status != "Planned") {
					$RecordRelated = BeanFactory::retrieveBean($bean->parent_type, $bean->parent_id, array('disable_row_level_security' => true));
					if ($bean->parent_type=="Leads" && ($RecordRelated->sasa_fechainiciogestionld_c == "" || $RecordRelated->sasa_fechainiciogestionld_c == null)) {
						$mifecha = new DateTime(); 
                        //$mifecha->modify('+5 hours');
						$RecordRelated->sasa_fechainiciogestionld_c = $mifecha->format('Y-m-d H:i:s');
					}else{
						if (($RecordRelated->sasa_fechainiciogestioncs_c == "" || $RecordRelated->sasa_fechainiciogestioncs_c == null)) {
							$mifecha = new DateTime(); 
	                        //$mifecha->modify('+5 hours');
							$RecordRelated->sasa_fechainiciogestioncs_c = $mifecha->format('Y-m-d H:i:s');
						}
					}
					$RecordRelated->save();
				}
			}

			
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Notas: ".$e->getMessage()); 
		}
	}
}
?>
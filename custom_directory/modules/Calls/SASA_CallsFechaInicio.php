<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_CallsFechaInicio
{
	function before_save($bean, $event, $arguments)
	{	
		/*
		Tarea en SASA 17556 https://sasaconsultoria.sugarondemand.com/#Tasks/3672f140-6e96-11ec-8b7a-02fb8f607ac4
		*/
		try{
			if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) {
	            //new record
	            global $db;
				//Identificar si aplica para las segundas y terceras llamadas, se hace por el control proceso establecido en el WF "Proceso Leads 2"
				$listctrlproceso = array("2","5","8","3","6","9");
				if (in_array($bean->sasa_controlproceso_c, $listctrlproceso)) {

					//Buscar centro de negocio
					$BusinessCenters = BeanFactory::newBean("BusinessCenters");
					$BusinessCenters->retrieve_by_string_fields(
						array( 
							'name' => "Operación Vardí Leads"
						)
					);

					if (!empty($BusinessCenters->id)) {
						//Establecer la fecha de inicio, debe ser el día siguiente, por ahora no se hace asignación de hora.
						$FechaInicio = new DateTime();
						//$FechaInicio->setTimezone(new DateTimeZone($BusinessCenters->timezone));
						$FechaInicio->modify('+1 day');
						
						//Query para contar el total de festivos
						$query_count_holiday = $db->query("SELECT count(*) countHolidays FROM holidays WHERE holidays.related_module_id='{$BusinessCenters->id}' AND holidays.holiday_date LIKE '%{$FechaInicio->format('Y')}%'");

						$resul_count_holiday = $db->fetchByAssoc($query_count_holiday);

						//Cilo para ver si el dia es un festivo o día habil
						while ($i <= $resul_count_holiday['countHolidays']) {
							
							$Holiday = BeanFactory::newBean("Holidays");
							$Holiday->retrieve_by_string_fields(
								array( 
									'holiday_date' => $FechaInicio->format("Y-m-d"),
									'related_module_id' => $BusinessCenters->id
								)
							);
							//SI encuentra un festivo que coincida con el dia que se plantea insertar.
							if (!empty($Holiday->id)) {
								$i++;
								$FechaInicio->modify('+1 day');
							}else{
								//validar si es un dia habil de la semana
								$DiaInicio = $FechaInicio->format("l");
								//Variable para hacer la busqueda de si es un dia habil o no.
								$DiaInicio = "is_open_".strtolower($DiaInicio);
								if ($BusinessCenters->{$DiaInicio}=="0") {
									$i++;
									$FechaInicio->modify('+1 day');
								}else{
									break;
								}
							}
						}

						//Almacenar el nombre del campo a hacer la busqueda, ver la estuctura de la tabla business_centers
						$HoraInicio = strtolower($FechaInicio->format("l"))."_open_hour";

						//En la BD se alacena solo un caracter si la hora es en la mañana, por eso se hace esta validación
						$Hora = strlen($BusinessCenters->{$HoraInicio})=="1" ? "0".$BusinessCenters->{$HoraInicio}:$BusinessCenters->{$HoraInicio};

						//Almacenar el nombre del campo a hacer la busqueda, ver la estuctura de la tabla business_centers
						$MinutosInicio = strtolower($FechaInicio->format("l"))."_open_minutes";

						//En la BD se alacena solo un caracter si la hora es en la mañana, por eso se hace esta validación
						$Minutos = strlen($BusinessCenters->{$MinutosInicio})=="1" ? "0".$BusinessCenters->{$MinutosInicio}:$BusinessCenters->{$MinutosInicio};
						$str_FechaInicio = $FechaInicio->format('Y-m-d '.$Hora.':'.$Minutos.':00');
						//Se agregan 5 horas de más ya que queda con una zona horaria diferente.
						$ModFechaInicio = new DateTime($str_FechaInicio);
						$ModFechaInicio->modify('+5 hours');
						$bean->date_start = $ModFechaInicio->format('Y-m-d H:i:s');

						//establecer la fecha fin, son 15+ que la fecha de inicio.
						$FechaFin = new DateTime($str_FechaInicio);
						$FechaFin->modify('+15 minutes');
						//Se agregan 5 horas de más ya que queda con una zona horaria diferente.
						$FechaFin->modify('+5 hours');
						$bean->date_end = $FechaFin->format('Y-m-d H:i:s');
					}else{
						$GLOBALS['log']->security("No se pudo encontrar el centro de negocio");
					}
				}
	        } else {
	            //existing record
	        }
			

			
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Notas: ".$e->getMessage()); 
		}
	}
}
?>
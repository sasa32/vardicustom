<?php
$module_name = 'sasa_Unidad_de_Negocio';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_codunidnegocio_c',
                'label' => 'LBL_SASA_CODUNIDNEGOCIO_C',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_companias_sasa_unidad_de_negocio_1_name',
                'label' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE',
                'enabled' => true,
                'id' => 'SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1SASA_COMPANIAS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);

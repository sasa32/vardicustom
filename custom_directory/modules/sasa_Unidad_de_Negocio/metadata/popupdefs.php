<?php
$popupMeta = array (
    'moduleMain' => 'sasa_Unidad_de_Negocio',
    'varName' => 'sasa_Unidad_de_Negocio',
    'orderBy' => 'sasa_unidad_de_negocio.name',
    'whereClauses' => array (
  'name' => 'sasa_unidad_de_negocio.name',
  'sasa_codunidnegocio_c' => 'sasa_unidad_de_negocio_cstm.sasa_codunidnegocio_c',
  'sasa_companias_sasa_unidad_de_negocio_1_name' => 'sasa_unidad_de_negocio.sasa_companias_sasa_unidad_de_negocio_1_name',
  'date_entered' => 'sasa_unidad_de_negocio.date_entered',
  'date_modified' => 'sasa_unidad_de_negocio.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sasa_codunidnegocio_c',
  5 => 'sasa_companias_sasa_unidad_de_negocio_1_name',
  6 => 'date_entered',
  7 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'sasa_codunidnegocio_c' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_CODUNIDNEGOCIO_C',
    'width' => 10,
    'name' => 'sasa_codunidnegocio_c',
  ),
  'sasa_companias_sasa_unidad_de_negocio_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE',
    'id' => 'SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1SASA_COMPANIAS_IDA',
    'width' => 10,
    'name' => 'sasa_companias_sasa_unidad_de_negocio_1_name',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SASA_CODUNIDNEGOCIO_C' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_CODUNIDNEGOCIO_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE',
    'id' => 'SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1SASA_COMPANIAS_IDA',
    'width' => 10,
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
),
);

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Vardefs/sasa_companias_sasa_unidad_de_negocio_1_sasa_Unidad_de_Negocio.php

// created: 2020-05-15 23:20:17
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_companias_sasa_unidad_de_negocio_1"] = array (
  'name' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'type' => 'link',
  'relationship' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'source' => 'non-db',
  'module' => 'sasa_Companias',
  'bean_name' => 'sasa_Companias',
  'side' => 'right',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'link-type' => 'one',
);
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_companias_sasa_unidad_de_negocio_1_name"] = array (
  'name' => 'sasa_companias_sasa_unidad_de_negocio_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'link' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'table' => 'sasa_companias',
  'module' => 'sasa_Companias',
  'rname' => 'name',
);
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida"] = array (
  'name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE_ID',
  'id_name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'link' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'table' => 'sasa_companias',
  'module' => 'sasa_Companias',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Vardefs/sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_sasa_Unidad_de_Negocio.php

// created: 2020-05-15 18:54:37
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_puntos_de_ventas_sasa_unidad_de_negocio_1"] = array (
  'name' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntoeab5_ventas_ida',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Vardefs/sugarfield_name.php

 // created: 2020-05-16 20:57:29
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['len']='255';
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['audited']=false;
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['massupdate']=false;
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['unified_search']=false;
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Vardefs/sugarfield_sasa_codunidnegocio_c.php

 // created: 2020-05-16 20:59:05
$dictionary['sasa_Unidad_de_Negocio']['fields']['sasa_codunidnegocio_c']['labelValue']='Código Unidad de negocio';
$dictionary['sasa_Unidad_de_Negocio']['fields']['sasa_codunidnegocio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_Unidad_de_Negocio']['fields']['sasa_codunidnegocio_c']['enforced']='1';
$dictionary['sasa_Unidad_de_Negocio']['fields']['sasa_codunidnegocio_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2020-05-16 21:00:16
$dictionary['sasa_Unidad_de_Negocio']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['sasa_Unidad_de_Negocio']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['sasa_Unidad_de_Negocio']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Vardefs/sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_sasa_Unidad_de_Negocio.php

// created: 2020-05-21 16:23:40
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'SASA_UnidadNegClienteProspect',
  'bean_name' => 'SASA_UnidadNegClienteProspect',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa_unida3a6anegocio_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Vardefs/sasa_unidad_de_negocio_opportunities_1_sasa_Unidad_de_Negocio.php

// created: 2022-11-23 15:22:18
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_unidad_de_negocio_opportunities_1"] = array (
  'name' => 'sasa_unidad_de_negocio_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_unidad_de_negocio_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Layoutdefs/sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_sasa_Unidad_de_Negocio.php

 // created: 2020-05-15 23:48:57
$layout_defs["sasa_Unidad_de_Negocio"]["subpanel_setup"]['sasa_puntos_de_ventas_sasa_unidad_de_negocio_1'] = array (
  'order' => 100,
  'module' => 'sasa_Puntos_de_Ventas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Layoutdefs/sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_sasa_Unidad_de_Negocio.php

 // created: 2020-05-21 16:23:40
$layout_defs["sasa_Unidad_de_Negocio"]["subpanel_setup"]['sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1'] = array (
  'order' => 100,
  'module' => 'SASA_UnidadNegClienteProspect',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'get_subpanel_data' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Unidad_de_Negocio/Ext/Layoutdefs/sasa_unidad_de_negocio_opportunities_1_sasa_Unidad_de_Negocio.php

 // created: 2022-11-23 15:22:18
$layout_defs["sasa_Unidad_de_Negocio"]["subpanel_setup"]['sasa_unidad_de_negocio_opportunities_1'] = array (
  'order' => 100,
  'module' => 'Opportunities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'get_subpanel_data' => 'sasa_unidad_de_negocio_opportunities_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>

<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-05-15 23:48:57
$viewdefs['sasa_Unidad_de_Negocio']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
  ),
);

// created: 2022-11-23 15:22:18
$viewdefs['sasa_Unidad_de_Negocio']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_unidad_de_negocio_opportunities_1',
  ),
);

// created: 2020-05-21 16:23:40
$viewdefs['sasa_Unidad_de_Negocio']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'context' => 
  array (
    'link' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  ),
);
<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv001202005151216.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_DESCRIPTION'] = 'Descripción de la Cliente y Prospecto';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID Cliente y Prospecto';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LBL_CONVERTED_ACCOUNT'] = 'Cliente y Prospecto Convertida:';
$mod_strings['LNK_SELECT_ACCOUNTS'] = 'O Seleccione una Cliente y Prospecto';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_CONTACT_ID'] = 'ID Contact';
$mod_strings['LBL_CONVERTED_CONTACT'] = 'Contact Convertido:';
$mod_strings['LBL_OPPORTUNITY_AMOUNT'] = 'Cantidad de la Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'ID Opportunity';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Nombre de la Opportunity:';
$mod_strings['LBL_CONVERTED_OPP'] = 'Opportunity Convertida:';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_LEAD'] = 'Nuevo Lead';
$mod_strings['LBL_MODULE_NAME'] = 'Leads';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Lead';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Lead';
$mod_strings['LNK_LEAD_LIST'] = 'Ver Leads';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Lead desde vCard';
$mod_strings['LNK_IMPORT_LEADS'] = 'Importar Leads';
$mod_strings['LBL_CAMPAIGN_ID'] = 'Id de Campaign';
$mod_strings['LBL_CAMPAIGN_LEAD'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_DESCRIPTION'] = 'Comentario';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_'] = 'Municipio Alternativo 2';
$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono fijo';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
$mod_strings['LBL_LAST_NAME'] = 'Apellidos';
$mod_strings['LBL_DEPARTMENT'] = 'Departamento';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_paises_leads_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_paises_leads_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PAISES_LEADS_1_FROM_SASA_PAISES_TITLE'] = 'Países';
$mod_strings['LBL_SASA_PAISES_LEADS_1_FROM_LEADS_TITLE'] = 'Países';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PAISES_LEADS_1_FROM_SASA_PAISES_TITLE'] = 'Países';
$mod_strings['LBL_SASA_PAISES_LEADS_1_FROM_LEADS_TITLE'] = 'Países';


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv002202005190736.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv001202005210142.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv002202005271146.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv003202006021053.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv003202007010106.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv003202007010151.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv003202007020345.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv003202007020358.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv004202007240322.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv004202007240358.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv004202007240847.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv004202007270838.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv005202101220933.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv006202103050115.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv006202103050116.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv007202104060907.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv007202104060908.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv007202104060908.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv008202105190140.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv008202105190141.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv008202105190141.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv008202105190141.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202105190505.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202105190505.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202105190505.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202105190507.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202105190508.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202105190508.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202105190508.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202106080827.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202106080828.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202106080829.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202106080829.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv010202106111037.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv010202106111037.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv010202106111038.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_vehiculos_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE'] = 'Líneas de vehículos';
$mod_strings['LBL_SASA_VEHICULOS_LEADS_1_FROM_LEADS_TITLE'] = 'Líneas de vehículos';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_puntos_de_ventas_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Puntos de Atención';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_LEADS_TITLE'] = 'Puntos de Atención';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customleads_sasa_puntos_de_ventas_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LEADS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Puntos de Atención';
$mod_strings['LBL_LEADS_SASA_PUNTOS_DE_VENTAS_1_FROM_LEADS_TITLE'] = 'Puntos de Atención';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customleads_sasa_habeas_data_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LEADS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_LEADS_SASA_HABEAS_DATA_1_FROM_LEADS_TITLE'] = 'Habeas Data';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_habeas_data_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_HABEAS_DATA_LEADS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_SASA_HABEAS_DATA_LEADS_1_FROM_LEADS_TITLE'] = 'Habeas Data';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_departamentos_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Departamentos';
$mod_strings['LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_LEADS_TITLE'] = 'Departamentos';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_municipios_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_paises_leads_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PAISES_LEADS_2_FROM_SASA_PAISES_TITLE'] = 'Países';
$mod_strings['LBL_SASA_PAISES_LEADS_2_FROM_LEADS_TITLE'] = 'Países';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_departamentos_leads_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Departamentos';
$mod_strings['LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_LEADS_TITLE'] = 'Departamentos';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_municipios_leads_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_2_FROM_LEADS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customsasa_marcas_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MARCAS_LEADS_1_FROM_SASA_MARCAS_TITLE'] = 'Marcas';
$mod_strings['LBL_SASA_MARCAS_LEADS_1_FROM_LEADS_TITLE'] = 'Marcas';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsV012202109230500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsV012202109230500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsV012202109230500.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv013202111110951.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv014202111160932.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHAFINCIOGESTIONLD_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONLD_C'] = 'Fecha Inicio de Gestión';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv015202112041201.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHAFINGESTIONLD_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONLD_C'] = 'Fecha Inicio de Gestión';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv015202112050244.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHAFINGESTIONLD_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONLD_C'] = 'Fecha Inicio de Gestión';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv017202112140901.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHAFINGESTIONLD_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONLD_C'] = 'Fecha Inicio de Gestión';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv017202112220824.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHAFINGESTIONLD_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONLD_C'] = 'Fecha Inicio de Gestión';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv018202205181256.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
	$mod_strings['LBL_SASA_ESPEJOLINEAVEHI_C'] = 'Espejo Linea de Vehículo';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
$mod_strings['LBL_SASA_FECHACONTACTO_C'] = 'Fecha del Contacto';
$mod_strings['LBL_SASA_NUMEROGESTIONES_C'] = 'Número de Gestiones';
$mod_strings['LBL_SASA_FECHAFINGESTIONLD_C'] = 'Fecha Final de la Gestión';
$mod_strings['LBL_SASA_FECHAINICIOGESTIONLD_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_CAMPANAUSADOS_C'] = 'Campaña Autos Usados';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_DESCRIPTION'] = 'Descripción de la Cliente y Prospecto';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID Cliente y Prospecto';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LBL_CONVERTED_ACCOUNT'] = 'Cliente y Prospecto Convertida:';
$mod_strings['LNK_SELECT_ACCOUNTS'] = 'O Seleccione una Cliente y Prospecto';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_CONTACT_ID'] = 'ID Contacto';
$mod_strings['LBL_CONVERTED_CONTACT'] = 'Contacto Convertido:';
$mod_strings['LBL_OPPORTUNITY_AMOUNT'] = 'Cantidad de la Cotización:';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'ID Cotización';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Nombre de la Cotización:';
$mod_strings['LBL_CONVERTED_OPP'] = 'Cotización Convertida:';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LNK_NEW_LEAD'] = 'Nuevo Lead';
$mod_strings['LBL_MODULE_NAME'] = 'Leads';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Lead';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Lead';
$mod_strings['LNK_LEAD_LIST'] = 'Ver Leads';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Lead desde vCard';
$mod_strings['LNK_IMPORT_LEADS'] = 'Importar Leads';
$mod_strings['LBL_CAMPAIGN_ID'] = 'Id de Campaña';
$mod_strings['LBL_CAMPAIGN_LEAD'] = 'Campañas';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campañas';
$mod_strings['LBL_DESCRIPTION'] = 'Comentario';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_'] = 'Municipio Alternativo 2';
$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono fijo';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
$mod_strings['LBL_LAST_NAME'] = 'Apellidos';
$mod_strings['LBL_DEPARTMENT'] = 'Departamento';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
$mod_strings['LBL_CALLS_LEADS_FROM_CALLS_TITLE'] = 'Llamadas';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_MEETINGS_LEADS_FROM_MEETINGS_TITLE'] = 'Reuniones';
$mod_strings['LBL_CAMPAIGN_LEAD_SUBPANEL_TITLE'] = 'Campañas';
$mod_strings['LBL_OPPORTUNITY_LEADS_FROM_OPPORTUNITIES_TITLE'] = 'Oportunidades';
$mod_strings['LBL_ACCOUNT_LEADS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
$mod_strings['LBL_CALLS_LEADS_1_FROM_CALLS_TITLE'] = 'Llamadas';
$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE'] = 'Ciudad Dirección';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_2_FROM_LEADS_TITLE'] = 'Ciudad Dirección';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección';
$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
$mod_strings['LBL_SASA_PLACALEADS_C'] = 'Placa';
$mod_strings['LBL_SASA_FECHAFINGESTIONLD_C'] = 'Fecha Final de la Gestión';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.customleads_cases_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LEADS_CASES_1_FROM_CASES_TITLE'] = 'Casos';
$mod_strings['LBL_LEADS_CASES_1_FROM_LEADS_TITLE'] = 'Casos';

?>

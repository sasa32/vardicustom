<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/WirelessLayoutdefs/leads_sasa_habeas_data_1_Leads.php

 // created: 2020-07-30 15:35:23
$layout_defs["Leads"]["subpanel_setup"]['leads_sasa_habeas_data_1'] = array (
  'order' => 100,
  'module' => 'SASA_Habeas_Data',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'get_subpanel_data' => 'leads_sasa_habeas_data_1',
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/WirelessLayoutdefs/leads_cases_1_Leads.php

 // created: 2023-03-06 03:28:30
$layout_defs["Leads"]["subpanel_setup"]['leads_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LEADS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'leads_cases_1',
);

?>

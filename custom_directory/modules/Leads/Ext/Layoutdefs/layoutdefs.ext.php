<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Layoutdefs/leads_sasa_habeas_data_1_Leads.php

 // created: 2020-07-30 15:35:23
$layout_defs["Leads"]["subpanel_setup"]['leads_sasa_habeas_data_1'] = array (
  'order' => 100,
  'module' => 'SASA_Habeas_Data',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'get_subpanel_data' => 'leads_sasa_habeas_data_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Layoutdefs/leads_cases_1_Leads.php

 // created: 2023-03-06 03:28:30
$layout_defs["Leads"]["subpanel_setup"]['leads_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'leads_cases_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Layoutdefs/_overrideLead_subpanel_leads_sasa_habeas_data_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_sasa_habeas_data_1']['override_subpanel_name'] = 'Lead_subpanel_leads_sasa_habeas_data_1';

?>

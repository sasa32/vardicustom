<?php
// WARNING: The contents of this file are auto-generated.


// created: 2023-03-06 03:28:30
$viewdefs['Leads']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_LEADS_CASES_1_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'leads_cases_1',
  ),
);

// created: 2020-07-30 15:35:23
$viewdefs['Leads']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'context' => 
  array (
    'link' => 'leads_sasa_habeas_data_1',
  ),
);
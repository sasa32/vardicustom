<?php
// WARNING: The contents of this file are auto-generated.


// created: 2023-03-06 03:28:30
$viewdefs['Leads']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_LEADS_CASES_1_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'leads_cases_1',
  ),
);

// created: 2020-07-30 15:35:23
$viewdefs['Leads']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'context' => 
  array (
    'link' => 'leads_sasa_habeas_data_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['Leads']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'leads_sasa_habeas_data_1',
  'view' => 'subpanel-for-leads-leads_sasa_habeas_data_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Leads']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'opportunity',
  'view' => 'subpanel-for-leads-opportunity',
);

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sasa_paises_leads_1_Leads.php

// created: 2020-05-15 15:41:37
$dictionary["Lead"]["fields"]["sasa_paises_leads_1"] = array (
  'name' => 'sasa_paises_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_leads_1',
  'source' => 'non-db',
  'module' => 'sasa_Paises',
  'bean_name' => 'sasa_Paises',
  'side' => 'right',
  'vname' => 'LBL_SASA_PAISES_LEADS_1_FROM_LEADS_TITLE',
  'id_name' => 'sasa_paises_leads_1sasa_paises_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_paises_leads_1_name"] = array (
  'name' => 'sasa_paises_leads_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_LEADS_1_FROM_SASA_PAISES_TITLE',
  'save' => true,
  'id_name' => 'sasa_paises_leads_1sasa_paises_ida',
  'link' => 'sasa_paises_leads_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_paises_leads_1sasa_paises_ida"] = array (
  'name' => 'sasa_paises_leads_1sasa_paises_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_LEADS_1_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_paises_leads_1sasa_paises_ida',
  'link' => 'sasa_paises_leads_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sasa_departamentos_leads_1_Leads.php

// created: 2020-05-15 15:43:53
$dictionary["Lead"]["fields"]["sasa_departamentos_leads_1"] = array (
  'name' => 'sasa_departamentos_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_leads_1',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'side' => 'right',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_LEADS_TITLE',
  'id_name' => 'sasa_departamentos_leads_1sasa_departamentos_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_departamentos_leads_1_name"] = array (
  'name' => 'sasa_departamentos_leads_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_departamentos_leads_1sasa_departamentos_ida',
  'link' => 'sasa_departamentos_leads_1',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_departamentos_leads_1sasa_departamentos_ida"] = array (
  'name' => 'sasa_departamentos_leads_1sasa_departamentos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_departamentos_leads_1sasa_departamentos_ida',
  'link' => 'sasa_departamentos_leads_1',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sasa_municipios_leads_1_Leads.php

// created: 2020-05-15 15:45:19
$dictionary["Lead"]["fields"]["sasa_municipios_leads_1"] = array (
  'name' => 'sasa_municipios_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_leads_1',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE',
  'id_name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_municipios_leads_1_name"] = array (
  'name' => 'sasa_municipios_leads_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'link' => 'sasa_municipios_leads_1',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_municipios_leads_1sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'link' => 'sasa_municipios_leads_1',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sasa_paises_leads_2_Leads.php

// created: 2020-05-15 16:05:13
$dictionary["Lead"]["fields"]["sasa_paises_leads_2"] = array (
  'name' => 'sasa_paises_leads_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_leads_2',
  'source' => 'non-db',
  'module' => 'sasa_Paises',
  'bean_name' => 'sasa_Paises',
  'side' => 'right',
  'vname' => 'LBL_SASA_PAISES_LEADS_2_FROM_LEADS_TITLE',
  'id_name' => 'sasa_paises_leads_2sasa_paises_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_paises_leads_2_name"] = array (
  'name' => 'sasa_paises_leads_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_LEADS_2_FROM_SASA_PAISES_TITLE',
  'save' => true,
  'id_name' => 'sasa_paises_leads_2sasa_paises_ida',
  'link' => 'sasa_paises_leads_2',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_paises_leads_2sasa_paises_ida"] = array (
  'name' => 'sasa_paises_leads_2sasa_paises_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_LEADS_2_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_paises_leads_2sasa_paises_ida',
  'link' => 'sasa_paises_leads_2',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sasa_departamentos_leads_2_Leads.php

// created: 2020-05-15 16:07:49
$dictionary["Lead"]["fields"]["sasa_departamentos_leads_2"] = array (
  'name' => 'sasa_departamentos_leads_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_leads_2',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'side' => 'right',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_LEADS_TITLE',
  'id_name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_departamentos_leads_2_name"] = array (
  'name' => 'sasa_departamentos_leads_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_leads_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_departamentos_leads_2sasa_departamentos_ida"] = array (
  'name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_leads_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sasa_vehiculos_leads_1_Leads.php

// created: 2020-05-15 23:44:20
$dictionary["Lead"]["fields"]["sasa_vehiculos_leads_1"] = array (
  'name' => 'sasa_vehiculos_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_leads_1',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'side' => 'right',
  'vname' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_LEADS_TITLE',
  'id_name' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_vehiculos_leads_1_name"] = array (
  'name' => 'sasa_vehiculos_leads_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_leads_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_vehiculos_leads_1sasa_vehiculos_ida"] = array (
  'name' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_leads_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/leads_sasa_habeas_data_1_Leads.php

// created: 2020-07-30 15:35:23
$dictionary["Lead"]["fields"]["leads_sasa_habeas_data_1"] = array (
  'name' => 'leads_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'leads_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'vname' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_sasa_habeas_data_1leads_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sasa_puntos_de_ventas_leads_1_Leads.php

// created: 2021-01-27 16:50:54
$dictionary["Lead"]["fields"]["sasa_puntos_de_ventas_leads_1"] = array (
  'name' => 'sasa_puntos_de_ventas_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_leads_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'side' => 'right',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_LEADS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_puntos_de_ventas_leads_1_name"] = array (
  'name' => 'sasa_puntos_de_ventas_leads_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_leads_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida"] = array (
  'name' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_leads_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_detalle_origen_c-visibility_grid.php

$dictionary['Lead']['fields']['sasa_detalle_origen_c']['visibility_grid']=array (
	'trigger' => 'lead_source',
	'values' => 
	array (
		1 => 
		array (
			0 => 71,
			1 => 1,
		),
		2 => 
		array (
		),
		3 => 
		array (
			0 => '',
			1 => 71,
		),
		4 => 
		array (
			0 => '',
			1 => 1,
			2 => 3,
			3 => 2,
			4 => 4,
			5 => 5,
			6 => 73,
			7 => 74,
		),
		5 => 
		array (
		),
		6 => 
		array (
		),
		8 => 
		array (
		),
		9 => 
		array (
		),
		10 => 
		array (
		),
		11 => 
		array (
		),
		13 => 
		array (
		),
		14 => 
		array (
		),
		15 => 
		array (
			0 => '',
			1 => 70,
		),
		16 => 
		array (
		),
		17 => 
		array (
		),
		18 => 
		array (
			0 => '',
			1 => 18,
			2 => 57,
		),
		19 => 
		array (
			0 => '',
			1 => 20,
			2 => 21,
		),
		20 => 
		array (
			0 => '',
			1 => 23,
			2 => 22,
			3 => 79,
			4 => 69,
			5 => 67,
			6 => 68,
			7 => 25,
			8 => 24,
			9 => 76,
			10 => 77,
			11 => 78,
		),
		21 => 
		array (
		),
		22 => 
		array (
			0 => '',
			1 => 27,
			2 => 75,
			3 => 80,
		),
		23 => 
		array (
			0 => '',
			1 => 28,
		),
		24 => 
		array (
		),
		25 => 
		array (
			0 => '',
			1 => 42,
			2 => 43,
			3 => 34,
			4 => 44,
		),
		26 => 
		array (
		),
		28 => 
		array (
		),
		29 => 
		array (
			0 => '',
			1 => 31,
			2 => 32,
			3 => 52,
		),
		30 => 
		array (
		),
		31 => 
		array (
		),
		32 => 
		array (
		),
		33 => 
		array (
		),
		34 => 
		array (
			0 => '',
			1 => 39,
		),
		35 => 
		array (
		),
		36 => 
		array (
			0 => '',
			1 => 40,
			2 => 51,
		),
		37 => 
		array (
			0 => '',
			1 => 58,
			2 => 64,
		),
		38 => 
		array (
			0 => '',
			1 => 45,
			2 => 50,
			3 => 46,
			4 => 72,
		),
		39 => 
		array (
			0 => '',
			1 => 54,
			2 => 56,
			3 => 59,
			4 => 60,
			5 => 55,
		),
		40 => 
		array (
			0 => 81,
		),
		41 => 
		array (
			0 => '',
			1 => 61,
			2 => 62,
		),
		42 => 
		array (
			0 => '',
			1 => 63,
			2 => 64,
		),
		43 => 
		array (
			0 => '',
			1 => 65,
			2 => 66,
		),
		'' => 
		array (
		),
	),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_alt_address_street.php

 // created: 2020-07-07 08:09:18
$dictionary['Lead']['fields']['alt_address_street']['audited']=false;
$dictionary['Lead']['fields']['alt_address_street']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Lead']['fields']['alt_address_street']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_street']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.3',
  'searchable' => true,
);
$dictionary['Lead']['fields']['alt_address_street']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_street']['pii']=false;
$dictionary['Lead']['fields']['alt_address_street']['rows']='4';
$dictionary['Lead']['fields']['alt_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_birthdate.php

 // created: 2020-05-11 18:42:01
$dictionary['Lead']['fields']['birthdate']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['birthdate']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['birthdate']['merge_filter']='disabled';
$dictionary['Lead']['fields']['birthdate']['calculated']=false;
$dictionary['Lead']['fields']['birthdate']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_city.php

 // created: 2020-05-11 19:40:45
$dictionary['Lead']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_city']['importable']='false';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_city']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_first_name.php

 // created: 2020-07-07 08:35:42
$dictionary['Lead']['fields']['first_name']['massupdate']=false;
$dictionary['Lead']['fields']['first_name']['comments']='';
$dictionary['Lead']['fields']['first_name']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['first_name']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['first_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['first_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.87',
  'searchable' => true,
);
$dictionary['Lead']['fields']['first_name']['calculated']='1';
$dictionary['Lead']['fields']['first_name']['required']=true;
$dictionary['Lead']['fields']['first_name']['importable']='false';
$dictionary['Lead']['fields']['first_name']['formula']='$sasa_nombres_c';
$dictionary['Lead']['fields']['first_name']['enforced']=true;
$dictionary['Lead']['fields']['first_name']['pii']=false;
$dictionary['Lead']['fields']['first_name']['audited']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_dp_consent_last_updated.php

 // created: 2020-07-07 08:20:55
$dictionary['Lead']['fields']['dp_consent_last_updated']['audited']=false;
$dictionary['Lead']['fields']['dp_consent_last_updated']['massupdate']=true;
$dictionary['Lead']['fields']['dp_consent_last_updated']['comments']='Date consent last updated';
$dictionary['Lead']['fields']['dp_consent_last_updated']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['dp_consent_last_updated']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['dp_consent_last_updated']['merge_filter']='disabled';
$dictionary['Lead']['fields']['dp_consent_last_updated']['calculated']=false;
$dictionary['Lead']['fields']['dp_consent_last_updated']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_dnb_principal_id.php

 // created: 2020-05-16 21:03:19
$dictionary['Lead']['fields']['dnb_principal_id']['len']='30';
$dictionary['Lead']['fields']['dnb_principal_id']['audited']=false;
$dictionary['Lead']['fields']['dnb_principal_id']['massupdate']=false;
$dictionary['Lead']['fields']['dnb_principal_id']['comments']='Unique Id For D&B Contact';
$dictionary['Lead']['fields']['dnb_principal_id']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['dnb_principal_id']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['dnb_principal_id']['merge_filter']='disabled';
$dictionary['Lead']['fields']['dnb_principal_id']['reportable']=false;
$dictionary['Lead']['fields']['dnb_principal_id']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['dnb_principal_id']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_twitter.php

 // created: 2020-05-16 20:25:20
$dictionary['Lead']['fields']['twitter']['audited']=false;
$dictionary['Lead']['fields']['twitter']['massupdate']=false;
$dictionary['Lead']['fields']['twitter']['comments']='The twitter name of the user';
$dictionary['Lead']['fields']['twitter']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['twitter']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['twitter']['merge_filter']='disabled';
$dictionary['Lead']['fields']['twitter']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['twitter']['calculated']=false;
$dictionary['Lead']['fields']['twitter']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_alt_address_city.php

 // created: 2020-07-07 08:09:29
$dictionary['Lead']['fields']['alt_address_city']['audited']=false;
$dictionary['Lead']['fields']['alt_address_city']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_city']['comments']='City for alternate address';
$dictionary['Lead']['fields']['alt_address_city']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_city']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_city']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_home.php

 // created: 2020-06-17 15:02:40
$dictionary['Lead']['fields']['phone_home']['len']='255';
$dictionary['Lead']['fields']['phone_home']['massupdate']=false;
$dictionary['Lead']['fields']['phone_home']['comments']='';
$dictionary['Lead']['fields']['phone_home']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['phone_home']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_home']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.02',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_home']['calculated']='1';
$dictionary['Lead']['fields']['phone_home']['pii']=false;
$dictionary['Lead']['fields']['phone_home']['audited']=false;
$dictionary['Lead']['fields']['phone_home']['importable']='false';
$dictionary['Lead']['fields']['phone_home']['formula']='concat(related($sasa_municipios_leads_1,"sasa_indicativo_c"),$sasa_phone_home_c)';
$dictionary['Lead']['fields']['phone_home']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_mkto_id.php

 // created: 2020-05-16 21:16:00
$dictionary['Lead']['fields']['mkto_id']['len']='11';
$dictionary['Lead']['fields']['mkto_id']['audited']=false;
$dictionary['Lead']['fields']['mkto_id']['massupdate']=false;
$dictionary['Lead']['fields']['mkto_id']['comments']='Associated Marketo Lead ID';
$dictionary['Lead']['fields']['mkto_id']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['mkto_id']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['mkto_id']['merge_filter']='disabled';
$dictionary['Lead']['fields']['mkto_id']['reportable']=false;
$dictionary['Lead']['fields']['mkto_id']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['mkto_id']['calculated']=false;
$dictionary['Lead']['fields']['mkto_id']['enable_range_search']=false;
$dictionary['Lead']['fields']['mkto_id']['min']=false;
$dictionary['Lead']['fields']['mkto_id']['max']=false;
$dictionary['Lead']['fields']['mkto_id']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_opportunity_amount.php

 // created: 2020-05-16 21:12:04
$dictionary['Lead']['fields']['opportunity_amount']['audited']=false;
$dictionary['Lead']['fields']['opportunity_amount']['massupdate']=false;
$dictionary['Lead']['fields']['opportunity_amount']['comments']='Amount of the opportunity';
$dictionary['Lead']['fields']['opportunity_amount']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['opportunity_amount']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['opportunity_amount']['merge_filter']='disabled';
$dictionary['Lead']['fields']['opportunity_amount']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['opportunity_amount']['calculated']=false;
$dictionary['Lead']['fields']['opportunity_amount']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_do_not_call.php

 // created: 2020-05-11 18:00:07
$dictionary['Lead']['fields']['do_not_call']['default']=false;
$dictionary['Lead']['fields']['do_not_call']['massupdate']=false;
$dictionary['Lead']['fields']['do_not_call']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['do_not_call']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['do_not_call']['merge_filter']='disabled';
$dictionary['Lead']['fields']['do_not_call']['unified_search']=false;
$dictionary['Lead']['fields']['do_not_call']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_mkto_lead_score.php

 // created: 2020-05-16 21:16:48
$dictionary['Lead']['fields']['mkto_lead_score']['len']='11';
$dictionary['Lead']['fields']['mkto_lead_score']['audited']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['massupdate']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['mkto_lead_score']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['mkto_lead_score']['merge_filter']='disabled';
$dictionary['Lead']['fields']['mkto_lead_score']['reportable']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['mkto_lead_score']['calculated']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['enable_range_search']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['min']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['max']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_opportunity_name.php

 // created: 2020-05-16 21:13:08
$dictionary['Lead']['fields']['opportunity_name']['audited']=false;
$dictionary['Lead']['fields']['opportunity_name']['massupdate']=false;
$dictionary['Lead']['fields']['opportunity_name']['comments']='Opportunity name associated with lead';
$dictionary['Lead']['fields']['opportunity_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['opportunity_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['opportunity_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['opportunity_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['opportunity_name']['calculated']=false;
$dictionary['Lead']['fields']['opportunity_name']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_mobile.php

 // created: 2020-06-17 15:01:58
$dictionary['Lead']['fields']['phone_mobile']['len']='255';
$dictionary['Lead']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Lead']['fields']['phone_mobile']['comments']='';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.01',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_mobile']['calculated']='1';
$dictionary['Lead']['fields']['phone_mobile']['audited']=false;
$dictionary['Lead']['fields']['phone_mobile']['pii']=false;
$dictionary['Lead']['fields']['phone_mobile']['importable']='false';
$dictionary['Lead']['fields']['phone_mobile']['formula']='concat("+57",$sasa_phone_mobile_c)';
$dictionary['Lead']['fields']['phone_mobile']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_fax.php

 // created: 2020-05-16 20:31:54
$dictionary['Lead']['fields']['phone_fax']['len']='100';
$dictionary['Lead']['fields']['phone_fax']['audited']=false;
$dictionary['Lead']['fields']['phone_fax']['massupdate']=false;
$dictionary['Lead']['fields']['phone_fax']['comments']='Contact fax number';
$dictionary['Lead']['fields']['phone_fax']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_fax']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_fax']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_fax']['reportable']=false;
$dictionary['Lead']['fields']['phone_fax']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.98',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_fax']['calculated']=false;
$dictionary['Lead']['fields']['phone_fax']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_googleplus.php

 // created: 2020-05-16 20:25:40
$dictionary['Lead']['fields']['googleplus']['audited']=false;
$dictionary['Lead']['fields']['googleplus']['massupdate']=false;
$dictionary['Lead']['fields']['googleplus']['comments']='The google plus id of the user';
$dictionary['Lead']['fields']['googleplus']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['googleplus']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['googleplus']['merge_filter']='disabled';
$dictionary['Lead']['fields']['googleplus']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['googleplus']['calculated']=false;
$dictionary['Lead']['fields']['googleplus']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_other.php

 // created: 2020-05-16 20:31:06
$dictionary['Lead']['fields']['phone_other']['len']='100';
$dictionary['Lead']['fields']['phone_other']['audited']=false;
$dictionary['Lead']['fields']['phone_other']['massupdate']=false;
$dictionary['Lead']['fields']['phone_other']['comments']='Other phone number for the contact';
$dictionary['Lead']['fields']['phone_other']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_other']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_other']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_other']['reportable']=false;
$dictionary['Lead']['fields']['phone_other']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.99',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_other']['calculated']=false;
$dictionary['Lead']['fields']['phone_other']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_salutation.php

 // created: 2020-05-09 13:19:55
$dictionary['Lead']['fields']['salutation']['len']=100;
$dictionary['Lead']['fields']['salutation']['audited']=false;
$dictionary['Lead']['fields']['salutation']['massupdate']=true;
$dictionary['Lead']['fields']['salutation']['comments']='';
$dictionary['Lead']['fields']['salutation']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['salutation']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['salutation']['merge_filter']='disabled';
$dictionary['Lead']['fields']['salutation']['calculated']=false;
$dictionary['Lead']['fields']['salutation']['dependency']=false;
$dictionary['Lead']['fields']['salutation']['pii']=false;
$dictionary['Lead']['fields']['salutation']['options']='salutation_list';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_state.php

 // created: 2020-05-11 19:41:32
$dictionary['Lead']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_state']['importable']='false';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_state']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_portal_app.php

 // created: 2020-05-16 21:13:32
$dictionary['Lead']['fields']['portal_app']['audited']=false;
$dictionary['Lead']['fields']['portal_app']['massupdate']=false;
$dictionary['Lead']['fields']['portal_app']['comments']='Portal application that resulted in created of lead';
$dictionary['Lead']['fields']['portal_app']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['portal_app']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['portal_app']['merge_filter']='disabled';
$dictionary['Lead']['fields']['portal_app']['reportable']=false;
$dictionary['Lead']['fields']['portal_app']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['portal_app']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_facebook.php

 // created: 2020-05-16 20:24:58
$dictionary['Lead']['fields']['facebook']['massupdate']=false;
$dictionary['Lead']['fields']['facebook']['comments']='The facebook name of the user';
$dictionary['Lead']['fields']['facebook']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['facebook']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['facebook']['merge_filter']='disabled';
$dictionary['Lead']['fields']['facebook']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['facebook']['calculated']=false;
$dictionary['Lead']['fields']['facebook']['audited']=false;
$dictionary['Lead']['fields']['facebook']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_dp_business_purpose.php

 // created: 2020-07-07 08:20:40
$dictionary['Lead']['fields']['dp_business_purpose']['len']=NULL;
$dictionary['Lead']['fields']['dp_business_purpose']['audited']=false;
$dictionary['Lead']['fields']['dp_business_purpose']['massupdate']=true;
$dictionary['Lead']['fields']['dp_business_purpose']['comments']='Business purposes consented for';
$dictionary['Lead']['fields']['dp_business_purpose']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['dp_business_purpose']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['dp_business_purpose']['merge_filter']='disabled';
$dictionary['Lead']['fields']['dp_business_purpose']['calculated']=false;
$dictionary['Lead']['fields']['dp_business_purpose']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_postalcode.php

 // created: 2020-07-07 08:07:23
$dictionary['Lead']['fields']['primary_address_postalcode']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_postalcode']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['audited']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Lead']['fields']['primary_address_postalcode']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_department.php

 // created: 2020-05-11 17:58:24
$dictionary['Lead']['fields']['department']['audited']=false;
$dictionary['Lead']['fields']['department']['massupdate']=false;
$dictionary['Lead']['fields']['department']['importable']='false';
$dictionary['Lead']['fields']['department']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['department']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['department']['merge_filter']='disabled';
$dictionary['Lead']['fields']['department']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['department']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_country.php

 // created: 2020-05-16 20:58:53
$dictionary['Lead']['fields']['primary_address_country']['len']='255';
$dictionary['Lead']['fields']['primary_address_country']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_country']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_country']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_country']['audited']=false;
$dictionary['Lead']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Lead']['fields']['primary_address_country']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_alt_address_state.php

 // created: 2020-07-07 08:09:41
$dictionary['Lead']['fields']['alt_address_state']['audited']=false;
$dictionary['Lead']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Lead']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_state']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_state']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_mkto_sync.php

 // created: 2020-05-16 21:15:14
$dictionary['Lead']['fields']['mkto_sync']['default']=false;
$dictionary['Lead']['fields']['mkto_sync']['audited']=false;
$dictionary['Lead']['fields']['mkto_sync']['massupdate']=false;
$dictionary['Lead']['fields']['mkto_sync']['comments']='Should the Lead be synced to Marketo';
$dictionary['Lead']['fields']['mkto_sync']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['mkto_sync']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['mkto_sync']['merge_filter']='disabled';
$dictionary['Lead']['fields']['mkto_sync']['reportable']=false;
$dictionary['Lead']['fields']['mkto_sync']['unified_search']=false;
$dictionary['Lead']['fields']['mkto_sync']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_street.php

 // created: 2020-05-11 19:40:09
$dictionary['Lead']['fields']['primary_address_street']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_street']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.31',
  'searchable' => true,
);
$dictionary['Lead']['fields']['primary_address_street']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_street']['rows']='4';
$dictionary['Lead']['fields']['primary_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_email.php

 // created: 2020-05-11 18:43:08
$dictionary['Lead']['fields']['email']['len']='100';
$dictionary['Lead']['fields']['email']['audited']=false;
$dictionary['Lead']['fields']['email']['massupdate']=true;
$dictionary['Lead']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['email']['merge_filter']='disabled';
$dictionary['Lead']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.83',
  'searchable' => true,
);
$dictionary['Lead']['fields']['email']['calculated']=false;
$dictionary['Lead']['fields']['email']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_municipio_secundario_2__c.php

 // created: 2020-05-09 12:06:05
$dictionary['Lead']['fields']['sasa_municipio_secundario_2__c']['labelValue']='Municipio Alternativo 2';
$dictionary['Lead']['fields']['sasa_municipio_secundario_2__c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_municipio_secundario_2__c']['enforced']='';
$dictionary['Lead']['fields']['sasa_municipio_secundario_2__c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_assistant_phone.php

 // created: 2020-05-16 21:01:59
$dictionary['Lead']['fields']['assistant_phone']['len']='100';
$dictionary['Lead']['fields']['assistant_phone']['audited']=false;
$dictionary['Lead']['fields']['assistant_phone']['massupdate']=false;
$dictionary['Lead']['fields']['assistant_phone']['comments']='Phone number of the assistant of the contact';
$dictionary['Lead']['fields']['assistant_phone']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['assistant_phone']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['assistant_phone']['merge_filter']='disabled';
$dictionary['Lead']['fields']['assistant_phone']['reportable']=false;
$dictionary['Lead']['fields']['assistant_phone']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['assistant_phone']['calculated']=false;
$dictionary['Lead']['fields']['assistant_phone']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_alt_address_country.php

 // created: 2020-07-07 08:10:01
$dictionary['Lead']['fields']['alt_address_country']['len']='255';
$dictionary['Lead']['fields']['alt_address_country']['audited']=false;
$dictionary['Lead']['fields']['alt_address_country']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_country']['comments']='Country for alternate address';
$dictionary['Lead']['fields']['alt_address_country']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_country']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_country']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_preferred_language.php

 // created: 2020-05-16 21:14:29
$dictionary['Lead']['fields']['preferred_language']['default']='en_us';
$dictionary['Lead']['fields']['preferred_language']['audited']=false;
$dictionary['Lead']['fields']['preferred_language']['massupdate']=true;
$dictionary['Lead']['fields']['preferred_language']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['preferred_language']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['preferred_language']['merge_filter']='disabled';
$dictionary['Lead']['fields']['preferred_language']['reportable']=false;
$dictionary['Lead']['fields']['preferred_language']['calculated']=false;
$dictionary['Lead']['fields']['preferred_language']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_alt_address_postalcode.php

 // created: 2020-07-07 08:09:52
$dictionary['Lead']['fields']['alt_address_postalcode']['audited']=false;
$dictionary['Lead']['fields']['alt_address_postalcode']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Lead']['fields']['alt_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_postalcode']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_postalcode']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_postalcode']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_work.php

 // created: 2020-05-16 20:30:48
$dictionary['Lead']['fields']['phone_work']['len']='100';
$dictionary['Lead']['fields']['phone_work']['audited']=false;
$dictionary['Lead']['fields']['phone_work']['massupdate']=false;
$dictionary['Lead']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_work']['reportable']=false;
$dictionary['Lead']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_work']['calculated']=false;
$dictionary['Lead']['fields']['phone_work']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/readonly.php

/*
* Requerimiento en https://sasaconsultoria.sugarondemand.com/index.php#Tasks/5ea23328-7e7a-11ea-b3b6-02fb8f607ac4
*/
$dictionary["Lead"]["fields"]["primary_address_city"]['readonly'] = true;
$dictionary["Lead"]["fields"]["primary_address_state"]['readonly'] = true;
$dictionary["Lead"]["fields"]["primary_address_country"]['readonly'] = true;

$dictionary["Lead"]["fields"]["alt_address_city"]['readonly'] = true;
$dictionary["Lead"]["fields"]["alt_address_state"]['readonly'] = true;
$dictionary["Lead"]["fields"]["alt_address_country"]['readonly'] = true;

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_last_name.php

 // created: 2020-07-07 08:35:55
$dictionary['Lead']['fields']['last_name']['massupdate']=false;
$dictionary['Lead']['fields']['last_name']['importable']='false';
$dictionary['Lead']['fields']['last_name']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['last_name']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.85',
  'searchable' => true,
);
$dictionary['Lead']['fields']['last_name']['calculated']='1';
$dictionary['Lead']['fields']['last_name']['formula']='concat($sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['Lead']['fields']['last_name']['enforced']=true;
$dictionary['Lead']['fields']['last_name']['pii']=false;
$dictionary['Lead']['fields']['last_name']['audited']=false;
$dictionary['Lead']['fields']['last_name']['comments']='Last name of the contact';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_assistant.php

 // created: 2020-05-16 21:01:31
$dictionary['Lead']['fields']['assistant']['audited']=false;
$dictionary['Lead']['fields']['assistant']['massupdate']=false;
$dictionary['Lead']['fields']['assistant']['comments']='Name of the assistant of the contact';
$dictionary['Lead']['fields']['assistant']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['assistant']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['assistant']['merge_filter']='disabled';
$dictionary['Lead']['fields']['assistant']['reportable']=false;
$dictionary['Lead']['fields']['assistant']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['assistant']['calculated']=false;
$dictionary['Lead']['fields']['assistant']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_refered_by.php

 // created: 2020-05-16 21:02:33
$dictionary['Lead']['fields']['refered_by']['audited']=false;
$dictionary['Lead']['fields']['refered_by']['massupdate']=false;
$dictionary['Lead']['fields']['refered_by']['comments']='Identifies who refered the lead';
$dictionary['Lead']['fields']['refered_by']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['refered_by']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['refered_by']['merge_filter']='disabled';
$dictionary['Lead']['fields']['refered_by']['reportable']=false;
$dictionary['Lead']['fields']['refered_by']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['refered_by']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_title.php

 // created: 2020-05-11 17:59:24
$dictionary['Lead']['fields']['title']['audited']=false;
$dictionary['Lead']['fields']['title']['massupdate']=false;
$dictionary['Lead']['fields']['title']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['title']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['title']['merge_filter']='disabled';
$dictionary['Lead']['fields']['title']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['title']['calculated']=false;
$dictionary['Lead']['fields']['title']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_account_name.php

 // created: 2020-05-20 19:01:32
$dictionary['Lead']['fields']['account_name']['audited']=true;
$dictionary['Lead']['fields']['account_name']['massupdate']=false;
$dictionary['Lead']['fields']['account_name']['importable']='false';
$dictionary['Lead']['fields']['account_name']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['account_name']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['account_name']['calculated']=false;
$dictionary['Lead']['fields']['account_name']['comments']='Account name for lead';
$dictionary['Lead']['fields']['account_name']['dependency']='equal($sasa_tipo_documento_c,"N")';
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_description.php

 // created: 2021-10-08 21:42:14
$dictionary['Lead']['fields']['description']['audited']=false;
$dictionary['Lead']['fields']['description']['massupdate']=false;
$dictionary['Lead']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.7',
  'searchable' => true,
);
$dictionary['Lead']['fields']['description']['calculated']=false;
$dictionary['Lead']['fields']['description']['rows']='6';
$dictionary['Lead']['fields']['description']['cols']='80';
$dictionary['Lead']['fields']['description']['comments']='Full text of the note';
$dictionary['Lead']['fields']['description']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/full_text_search_admin.php

 // created: 2020-05-22 14:06:00
$dictionary['Lead']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_status.php

 // created: 2021-12-06 02:59:58
$dictionary['Lead']['fields']['status']['default']='A';
$dictionary['Lead']['fields']['status']['len']=100;
$dictionary['Lead']['fields']['status']['massupdate']=true;
$dictionary['Lead']['fields']['status']['hidemassupdate']=false;
$dictionary['Lead']['fields']['status']['options']='status_list';
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status']['calculated']=false;
$dictionary['Lead']['fields']['status']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_lead_source.php

 // created: 2021-05-13 14:25:12
$dictionary['Lead']['fields']['lead_source']['len']=100;
$dictionary['Lead']['fields']['lead_source']['massupdate']=true;
$dictionary['Lead']['fields']['lead_source']['options']='lead_source_list';
$dictionary['Lead']['fields']['lead_source']['comments']='';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Lead']['fields']['lead_source']['calculated']=false;
$dictionary['Lead']['fields']['lead_source']['dependency']=false;
$dictionary['Lead']['fields']['lead_source']['required']=true;
$dictionary['Lead']['fields']['lead_source']['default']='0';
$dictionary['Lead']['fields']['lead_source']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_compania_c.php

 // created: 2022-05-18 12:56:55
$dictionary['Lead']['fields']['sasa_compania_c']['labelValue']='Compañia';
$dictionary['Lead']['fields']['sasa_compania_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_compania_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2022-05-18 12:56:56
$dictionary['Lead']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['Lead']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_genero_c.php

 // created: 2022-05-18 12:56:56
$dictionary['Lead']['fields']['sasa_genero_c']['labelValue']='Género';
$dictionary['Lead']['fields']['sasa_genero_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_genero_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_last_name_2_c.php

 // created: 2022-05-18 12:56:56
$dictionary['Lead']['fields']['sasa_last_name_2_c']['labelValue']='Segundo Apellido';
$dictionary['Lead']['fields']['sasa_last_name_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_last_name_2_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_last_name_2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_municipio_principal_2_c.php

 // created: 2022-05-18 12:56:56
$dictionary['Lead']['fields']['sasa_municipio_principal_2_c']['labelValue']='Municipio Principal 2';
$dictionary['Lead']['fields']['sasa_municipio_principal_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_municipio_principal_2_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_municipio_principal_2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_municipio_secundario_2_c.php

 // created: 2022-05-18 12:56:57

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_nombres_c.php

 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_nombres_c']['labelValue']='Nombres';
$dictionary['Lead']['fields']['sasa_nombres_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_nombres_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_nombres_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_phone_mobile_c.php

 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_phone_mobile_c']['labelValue']='Teléfono Celular';
$dictionary['Lead']['fields']['sasa_phone_mobile_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['sasa_phone_mobile_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_phone_mobile_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_phone_home_c.php

 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_phone_home_c']['labelValue']='Teléfono fijo';
$dictionary['Lead']['fields']['sasa_phone_home_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['sasa_phone_home_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_phone_home_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_numero_documento_c.php

 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_numero_documento_c']['labelValue']='Número de Documento';
$dictionary['Lead']['fields']['sasa_numero_documento_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['sasa_numero_documento_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_numero_documento_c']['dependency']='equal($sasa_tipo_de_persona_c,"N")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_primerapellido_c.php

 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_primerapellido_c']['labelValue']='Primer Apellido';
$dictionary['Lead']['fields']['sasa_primerapellido_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_primerapellido_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_primerapellido_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_tipo_documento_c.php

 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_tipo_documento_c']['labelValue']='Tipo de Documento';
$dictionary['Lead']['fields']['sasa_tipo_documento_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_tipo_documento_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipo_de_persona_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'N' => 
    array (
      0 => '',
      1 => 'D',
      2 => 'C',
      3 => 'E',
      4 => 'U',
      5 => 'P',
      6 => 'T',
    ),
    'J' => 
    array (
      0 => 'N',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_tipo_de_persona_c.php

 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_tipo_de_persona_c']['labelValue']='Tipo de Persona';
$dictionary['Lead']['fields']['sasa_tipo_de_persona_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_tipo_de_persona_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_tipo_de_persona_c']['readonly_formula']='';
$dictionary['Lead']['fields']['sasa_tipo_de_persona_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_unidad_de_negocio_c.php

 // created: 2022-05-18 12:56:58
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['labelValue']='Unidad de Negocio';
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['readonly_formula']='';
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['visibility_grid']=array (
  'trigger' => 'sasa_compania_c',
  'values' => 
  array (
    1 => 
    array (
      0 => '',
      1 => '110',
      2 => '120',
      3 => '130',
      4 => '153',
    ),
    2 => 
    array (
      0 => '',
      1 => '210',
      2 => '220',
    ),
    3 => 
    array (
      0 => '',
      1 => '310',
      2 => '315',
      3 => '340',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_num_docu_juridica_c.php

 // created: 2022-05-18 12:56:58
$dictionary['Lead']['fields']['sasa_num_docu_juridica_c']['labelValue']='Número de Documento';
$dictionary['Lead']['fields']['sasa_num_docu_juridica_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_num_docu_juridica_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_num_docu_juridica_c']['dependency']='equal($sasa_tipo_de_persona_c,"J")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_tipo_solicitud_c.php

 // created: 2022-05-18 12:56:58
$dictionary['Lead']['fields']['sasa_tipo_solicitud_c']['labelValue']='Tipo de solicitud';
$dictionary['Lead']['fields']['sasa_tipo_solicitud_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_tipo_solicitud_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_tipo_solicitud_c']['readonly_formula']='';
$dictionary['Lead']['fields']['sasa_tipo_solicitud_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_cod_puntoatencion_c.php

 // created: 2022-05-18 12:56:58

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_cod_linea_c.php

 // created: 2022-05-18 12:56:58

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_heliosleadid_c.php

 // created: 2022-05-18 12:56:59

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_cd_area_c.php

 // created: 2022-05-18 12:56:59
$dictionary['Lead']['fields']['cd_area_c']['labelValue']='Subunidad de Negocio';
$dictionary['Lead']['fields']['cd_area_c']['dependency']='';
$dictionary['Lead']['fields']['cd_area_c']['required_formula']='';
$dictionary['Lead']['fields']['cd_area_c']['readonly_formula']='';
$dictionary['Lead']['fields']['cd_area_c']['visibility_grid']=array (
  'trigger' => 'sasa_unidad_de_negocio_c',
  'values' => 
  array (
    110 => 
    array (
      0 => '',
      1 => '11011',
    ),
    120 => 
    array (
      0 => '',
      1 => '12020',
    ),
    130 => 
    array (
      0 => '',
      1 => '13030',
    ),
    153 => 
    array (
      0 => '',
      1 => '15353',
    ),
    210 => 
    array (
      0 => '',
      1 => '21010',
      2 => '21012',
    ),
    220 => 
    array (
      0 => '',
      1 => '22020',
    ),
    310 => 
    array (
      0 => '',
      1 => '31011',
    ),
    315 => 
    array (
      0 => '',
      1 => '31515',
    ),
    340 => 
    array (
      0 => '',
      1 => '34044',
      2 => '34040',
      3 => '34041',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_tipodeformulario_c.php

 // created: 2022-05-18 12:56:59
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['labelValue']='Nombre de Formulario';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['readonly']='1';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_acepta_terminos_c.php

 // created: 2022-05-18 12:56:59
$dictionary['Lead']['fields']['sasa_acepta_terminos_c']['labelValue']='Acepta Términos';
$dictionary['Lead']['fields']['sasa_acepta_terminos_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_acepta_terminos_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_acepta_terminos_c']['readonly']='1';
$dictionary['Lead']['fields']['sasa_acepta_terminos_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_ip_habeasdata_c.php

 // created: 2022-05-18 12:56:59
$dictionary['Lead']['fields']['sasa_ip_habeasdata_c']['labelValue']='IP Aceptación de Habeas Data';
$dictionary['Lead']['fields']['sasa_ip_habeasdata_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_ip_habeasdata_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_ip_habeasdata_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_ip_habeasdata_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_ip_habeasdata_c']['readonly']='1';
$dictionary['Lead']['fields']['sasa_ip_habeasdata_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_espejolineavehi_c.php

 // created: 2022-05-18 12:56:59
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['labelValue']='Espejo Linea de Vehículo';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['calculated']='1';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['formula']='related($sasa_vehiculos_leads_1,"name")';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['enforced']='1';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_fechacontacto_c.php

 // created: 2022-05-18 12:56:59

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_numerogestiones_c.php

 // created: 2022-05-18 12:56:59

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_estadocontacto_c.php

 // created: 2022-05-18 12:56:59
$dictionary['Lead']['fields']['sasa_estadocontacto_c']['labelValue']='Estado del Contacto';
$dictionary['Lead']['fields']['sasa_estadocontacto_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_estadocontacto_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_estadocontacto_c']['readonly_formula']='';
$dictionary['Lead']['fields']['sasa_estadocontacto_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_fechafingestionld_c.php

 // created: 2022-05-18 12:57:00
$dictionary['Lead']['fields']['sasa_fechafingestionld_c']['labelValue']='Fecha Final de la Gestión';
$dictionary['Lead']['fields']['sasa_fechafingestionld_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_fechafingestionld_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_fechafingestionld_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_fechafingestionld_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_fechainiciogestionld_c.php

 // created: 2022-05-18 12:57:00

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_campanausados_c.php

 // created: 2022-05-18 12:57:00

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/customer_journey_parent.php

// created: 2023-02-08 11:09:23
VardefManager::createVardef('Leads', 'Lead', [
                                'customer_journey_parent',
                        ]);
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/leads_cases_1_Leads.php

// created: 2023-03-06 03:28:30
$dictionary["Lead"]["fields"]["leads_cases_1"] = array (
  'name' => 'leads_cases_1',
  'type' => 'link',
  'relationship' => 'leads_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_LEADS_CASES_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_cases_1leads_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_placaleads_c.php

 // created: 2023-05-16 13:43:09
$dictionary['Lead']['fields']['sasa_placaleads_c']['labelValue']='Placa';
$dictionary['Lead']['fields']['sasa_placaleads_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_placaleads_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_placaleads_c']['dependency']='or(equal($sasa_tipo_solicitud_c,"5"),equal($sasa_unidad_de_negocio_c,"130"),equal($sasa_unidad_de_negocio_c,"210"),equal($sasa_tipo_solicitud_c,"1"),equal($sasa_unidad_de_negocio_c,"310"))';
$dictionary['Lead']['fields']['sasa_placaleads_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_placaleads_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_detalle_origen_c.php

 // created: 2023-05-19 14:01:39
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['labelValue']='Detalle de Origen';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['readonly_formula']='';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['visibility_grid']=array (
  'trigger' => 'lead_source',
  'values' => 
  array (
    1 => 
    array (
    ),
    2 => 
    array (
    ),
    3 => 
    array (
      0 => '',
      1 => '71',
    ),
    4 => 
    array (
      0 => '',
      1 => '1',
      2 => '3',
      3 => '2',
      4 => '4',
      5 => '5',
      6 => '73',
      7 => '74',
    ),
    5 => 
    array (
    ),
    6 => 
    array (
    ),
    8 => 
    array (
    ),
    9 => 
    array (
    ),
    10 => 
    array (
    ),
    11 => 
    array (
    ),
    13 => 
    array (
    ),
    14 => 
    array (
    ),
    15 => 
    array (
      0 => '',
      1 => '70',
    ),
    16 => 
    array (
    ),
    17 => 
    array (
    ),
    18 => 
    array (
      0 => '',
      1 => '18',
      2 => '57',
    ),
    19 => 
    array (
      0 => '',
      1 => '20',
      2 => '21',
    ),
    20 => 
    array (
      0 => '',
      1 => '89',
      2 => '23',
      3 => '22',
      4 => '79',
      5 => '69',
      6 => '67',
      7 => '68',
      8 => '25',
      9 => '24',
      10 => '76',
      11 => '77',
      12 => '78',
      13 => '87',
    ),
    21 => 
    array (
    ),
    22 => 
    array (
      0 => '',
      1 => '27',
      2 => '75',
      3 => '80',
    ),
    23 => 
    array (
      0 => '',
      1 => '28',
    ),
    24 => 
    array (
    ),
    25 => 
    array (
      0 => '',
      1 => '42',
      2 => '43',
      3 => '34',
      4 => '44',
    ),
    26 => 
    array (
    ),
    28 => 
    array (
    ),
    29 => 
    array (
      0 => '',
      1 => '31',
      2 => '32',
      3 => '52',
      4 => '46',
      5 => '14',
    ),
    30 => 
    array (
    ),
    31 => 
    array (
    ),
    32 => 
    array (
    ),
    33 => 
    array (
    ),
    34 => 
    array (
      0 => '',
      1 => '39',
    ),
    35 => 
    array (
    ),
    36 => 
    array (
      0 => '',
      1 => '40',
      2 => '51',
    ),
    37 => 
    array (
      0 => '',
      1 => '58',
      2 => '64',
      3 => '46',
    ),
    38 => 
    array (
      0 => '',
      1 => '45',
      2 => '72',
      3 => '88',
      4 => '46',
      5 => '50',
    ),
    39 => 
    array (
      0 => '',
      1 => '54',
      2 => '56',
      3 => '59',
      4 => '60',
      5 => '55',
    ),
    40 => 
    array (
    ),
    41 => 
    array (
      0 => '',
      1 => '61',
      2 => '62',
    ),
    42 => 
    array (
      0 => '',
      1 => '63',
      2 => '64',
    ),
    43 => 
    array (
      0 => '',
      1 => '65',
      2 => '66',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>

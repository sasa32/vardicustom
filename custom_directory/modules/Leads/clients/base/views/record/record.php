<?php
// created: 2023-02-08 11:06:10
$viewdefs['Leads']['base']['view']['record'] = array (
  'buttons' => 
  array (
    0 => 
    array (
      'type' => 'button',
      'name' => 'cancel_button',
      'label' => 'LBL_CANCEL_BUTTON_LABEL',
      'css_class' => 'btn-invisible btn-link',
      'showOn' => 'edit',
      'events' => 
      array (
        'click' => 'button:cancel_button:click',
      ),
    ),
    1 => 
    array (
      'type' => 'rowaction',
      'event' => 'button:save_button:click',
      'name' => 'save_button',
      'label' => 'LBL_SAVE_BUTTON_LABEL',
      'css_class' => 'btn btn-primary',
      'showOn' => 'edit',
      'acl_action' => 'edit',
    ),
    2 => 
    array (
      'type' => 'actiondropdown',
      'name' => 'main_dropdown',
      'primary' => true,
      'showOn' => 'view',
      'buttons' => 
      array (
        0 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:edit_button:click',
          'name' => 'edit_button',
          'label' => 'LBL_EDIT_BUTTON_LABEL',
          'acl_action' => 'edit',
        ),
        1 => 
        array (
          'type' => 'shareaction',
          'name' => 'share',
          'label' => 'LBL_RECORD_SHARE_BUTTON',
          'acl_action' => 'view',
        ),
        2 => 
        array (
          'type' => 'pdfaction',
          'name' => 'download-pdf',
          'label' => 'LBL_PDF_VIEW',
          'action' => 'download',
          'acl_action' => 'view',
        ),
        3 => 
        array (
          'type' => 'pdfaction',
          'name' => 'email-pdf',
          'label' => 'LBL_PDF_EMAIL',
          'action' => 'email',
          'acl_action' => 'view',
        ),
        4 => 
        array (
          'type' => 'divider',
        ),
        5 => 
        array (
          'type' => 'convertbutton',
          'name' => 'lead_convert_button',
          'label' => 'LBL_CONVERT_BUTTON_LABEL',
          'acl_action' => 'edit',
        ),
        6 => 
        array (
          'type' => 'manage-subscription',
          'name' => 'manage_subscription_button',
          'label' => 'LBL_MANAGE_SUBSCRIPTIONS',
          'acl_action' => 'view',
        ),
        7 => 
        array (
          'type' => 'vcard',
          'name' => 'vcard_button',
          'label' => 'LBL_VCARD_DOWNLOAD',
          'acl_action' => 'view',
        ),
        8 => 
        array (
          'type' => 'divider',
        ),
        9 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:find_duplicates_button:click',
          'name' => 'find_duplicates_button',
          'label' => 'LBL_DUP_MERGE',
          'acl_action' => 'edit',
        ),
        10 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:duplicate_button:click',
          'name' => 'duplicate_button',
          'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
          'acl_module' => 'Leads',
          'acl_action' => 'create',
        ),
        11 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:historical_summary_button:click',
          'name' => 'historical_summary_button',
          'label' => 'LBL_HISTORICAL_SUMMARY',
          'acl_action' => 'view',
        ),
        12 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:audit_button:click',
          'name' => 'audit_button',
          'label' => 'LNK_VIEW_CHANGE_LOG',
          'acl_action' => 'view',
        ),
        13 => 
        array (
          'type' => 'divider',
        ),
        14 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:delete_button:click',
          'name' => 'delete_button',
          'label' => 'LBL_DELETE_BUTTON_LABEL',
          'acl_action' => 'delete',
        ),
      ),
    ),
    3 => 
    array (
      'name' => 'sidebar_toggle',
      'type' => 'sidebartoggle',
    ),
  ),
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'header' => true,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'picture',
          'type' => 'hint-contacts-photo',
          'size' => 'large',
          'dismiss_label' => true,
          'white_list' => true,
          'related_fields' => 
          array (
            0 => 'hint_contact_pic',
          ),
        ),
        1 => 
        array (
          'name' => 'name',
          'type' => 'fullname',
          'label' => 'LBL_NAME',
          'dismiss_label' => true,
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'salutation',
              'type' => 'enum',
              'enum_width' => 'auto',
              'searchBarThreshold' => 7,
            ),
            1 => 'first_name',
            2 => 'last_name',
          ),
        ),
        2 => 
        array (
          'type' => 'favorite',
        ),
        3 => 
        array (
          'type' => 'follow',
          'readonly' => true,
        ),
        4 => 
        array (
          'name' => 'converted',
          'type' => 'badge',
          'dismiss_label' => true,
          'readonly' => true,
          'related_fields' => 
          array (
            0 => 'account_id',
            1 => 'account_name',
            2 => 'contact_id',
            3 => 'contact_name',
            4 => 'opportunity_id',
            5 => 'opportunity_name',
            6 => 'converted_opp_name',
          ),
        ),
      ),
    ),
    1 => 
    array (
      'name' => 'panel_body',
      'label' => 'LBL_RECORD_BODY',
      'columns' => 2,
      'labels' => true,
      'placeholders' => true,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'sasa_nombres_c',
          'label' => 'LBL_SASA_NOMBRES_C',
          'span' => 12,
        ),
        1 => 
        array (
          'name' => 'sasa_primerapellido_c',
          'label' => 'LBL_SASA_PRIMERAPELLIDO_C',
        ),
        2 => 
        array (
          'name' => 'sasa_last_name_2_c',
          'label' => 'LBL_SASA_LAST_NAME_2_C',
        ),
        3 => 
        array (
          'name' => 'sasa_compania_c',
          'label' => 'LBL_SASA_COMPANIA_C',
        ),
        4 => 
        array (
          'name' => 'sasa_unidad_de_negocio_c',
          'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_C',
        ),
        5 => 
        array (
          'readonly' => false,
          'name' => 'cd_area_c',
          'label' => 'LBL_CD_AREA_C',
        ),
        6 => 
        array (
          'name' => 'sasa_tipo_de_persona_c',
          'label' => 'LBL_SASA_TIPO_DE_PERSONA_C',
        ),
        7 => 
        array (
          'name' => 'sasa_tipo_solicitud_c',
          'label' => 'LBL_SASA_TIPO_SOLICITUD_C',
          'span' => 12,
        ),
        8 => 
        array (
          'name' => 'sasa_num_docu_juridica_c',
          'label' => 'LBL_SASA_NUM_DOCU_JURIDICA_C',
        ),
        9 => 
        array (
          'readonly' => false,
          'name' => 'sasa_placaleads_c',
          'label' => 'LBL_SASA_PLACALEADS_C',
        ),
        10 => 
        array (
          'name' => 'account_name',
          'span' => 12,
        ),
        11 => 
        array (
          'name' => 'sasa_tipo_documento_c',
          'label' => 'LBL_SASA_TIPO_DOCUMENTO_C',
        ),
        12 => 
        array (
          'name' => 'sasa_numero_documento_c',
          'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
        ),
        13 => 
        array (
          'name' => 'email',
        ),
        14 => 
        array (
          'name' => 'birthdate',
          'comment' => 'The birthdate of the contact',
          'label' => 'LBL_BIRTHDATE',
        ),
        15 => 
        array (
          'name' => 'sasa_phone_home_c',
          'label' => 'LBL_SASA_PHONE_HOME_C',
        ),
        16 => 
        array (
          'name' => 'sasa_phone_mobile_c',
          'label' => 'LBL_SASA_PHONE_MOBILE_C',
        ),
        17 => 
        array (
          'name' => 'sasa_municipios_leads_1_name',
          'label' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE',
          'span' => 12,
        ),
        18 => 
        array (
          'name' => 'primary_address',
          'type' => 'fieldset',
          'css_class' => 'address',
          'label' => 'LBL_PRIMARY_ADDRESS',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'primary_address_street',
              'css_class' => 'address_street',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_STREET',
            ),
            1 => 
            array (
              'name' => 'primary_address_city',
              'css_class' => 'address_city',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_CITY',
            ),
            2 => 
            array (
              'name' => 'primary_address_state',
              'css_class' => 'address_state',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_STATE',
            ),
            3 => 
            array (
              'name' => 'primary_address_postalcode',
              'css_class' => 'address_zip',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
            ),
            4 => 
            array (
              'name' => 'primary_address_country',
              'css_class' => 'address_country',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
            ),
          ),
          'span' => 12,
        ),
        19 => 
        array (
          'name' => 'sasa_genero_c',
          'label' => 'LBL_SASA_GENERO_C',
        ),
        20 => 
        array (
          'name' => 'status',
          'type' => 'status',
        ),
        21 => 'lead_source',
        22 => 
        array (
          'name' => 'sasa_detalle_origen_c',
          'label' => 'LBL_SASA_DETALLE_ORIGEN_C',
        ),
        23 => 
        array (
          'name' => 'sasa_vehiculos_leads_1_name',
          'label' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE',
        ),
        24 => 
        array (
          'name' => 'sasa_puntos_de_ventas_leads_1_name',
        ),
        25 => 
        array (
          'name' => 'description',
          'span' => 12,
        ),
        26 => 
        array (
          'readonly' => false,
          'name' => 'sasa_acepta_terminos_c',
          'label' => 'LBL_SASA_ACEPTA_TERMINOS_C',
        ),
        27 => 
        array (
          'readonly' => false,
          'name' => 'sasa_campanausados_c',
          'label' => 'LBL_SASA_CAMPANAUSADOS_C',
        ),
        28 => 
        array (
          'readonly' => false,
          'name' => 'sasa_ip_habeasdata_c',
          'label' => 'LBL_SASA_IP_HABEASDATA_C',
        ),
        29 => 
        array (
          'readonly' => false,
          'name' => 'sasa_tipodeformulario_c',
          'label' => 'LBL_SASA_TIPODEFORMULARIO_C',
        ),
        30 => 
        array (
          'name' => 'commentlog',
          'displayParams' => 
          array (
            'type' => 'commentlog',
            'fields' => 
            array (
              0 => 'entry',
              1 => 'date_entered',
              2 => 'created_by_name',
            ),
            'max_num' => 100,
          ),
          'studio' => 
          array (
            'listview' => false,
            'recordview' => true,
            'wirelesseditview' => false,
            'wirelessdetailview' => true,
            'wirelesslistview' => false,
            'wireless_basic_search' => false,
            'wireless_advanced_search' => false,
          ),
          'label' => 'LBL_COMMENTLOG',
          'span' => 12,
        ),
        31 => 
        array (
          'name' => 'facebook',
          'comment' => 'The facebook name of the user',
          'label' => 'LBL_FACEBOOK',
        ),
        32 => 'twitter',
        33 => 
        array (
          'name' => 'assigned_user_name',
        ),
        34 => 
        array (
          'name' => 'team_name',
        ),
        35 => 
        array (
          'name' => 'date_entered_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_ENTERED',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_entered',
            ),
            1 => 
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 => 
            array (
              'name' => 'created_by_name',
            ),
          ),
        ),
        36 => 
        array (
          'name' => 'date_modified_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_MODIFIED',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_modified',
            ),
            1 => 
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 => 
            array (
              'name' => 'modified_by_name',
            ),
          ),
        ),
        37 => 
        array (
          'name' => 'tag',
          'span' => 6,
        ),
        38 => 
        array (
          'name' => 'do_not_call',
          'span' => 6,
        ),
        39 => 
        array (
          'name' => 'market_score',
        ),
        40 => 
        array (
        ),
      ),
    ),
    2 => 
    array (
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_RECORDVIEW_PANEL1',
      'label' => 'LBL_RECORDVIEW_PANEL1',
      'columns' => 2,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'readonly' => false,
          'name' => 'sasa_fechainiciogestionld_c',
          'label' => 'LBL_SASA_FECHAINICIOGESTIONLD_C',
        ),
        1 => 
        array (
          'readonly' => false,
          'name' => 'sasa_fechafingestionld_c',
          'label' => 'LBL_SASA_FECHAFINGESTIONLD_C',
        ),
        2 => 
        array (
          'readonly' => false,
          'name' => 'sasa_estadocontacto_c',
          'label' => 'LBL_SASA_ESTADOCONTACTO_C',
        ),
        3 => 
        array (
          'readonly' => false,
          'name' => 'sasa_fechacontacto_c',
          'label' => 'LBL_SASA_FECHACONTACTO_C',
        ),
        4 => 
        array (
          'readonly' => false,
          'name' => 'sasa_numerogestiones_c',
          'label' => 'LBL_SASA_NUMEROGESTIONES_C',
        ),
        5 => 
        array (
        ),
        6 => 
        array (
          'name' => 'geocode_status',
          'licenseFilter' => 
          array (
            0 => 'MAPS',
          ),
        ),
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'useTabs' => false,
  ),
);
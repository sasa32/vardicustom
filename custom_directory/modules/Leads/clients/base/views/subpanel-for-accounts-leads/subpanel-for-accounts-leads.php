<?php
// created: 2020-07-09 16:15:27
$viewdefs['Leads']['base']['view']['subpanel-for-accounts-leads'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'salutation',
            1 => 'first_name',
            2 => 'last_name',
          ),
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_compania_c',
          'label' => 'LBL_SASA_COMPANIA_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_unidad_de_negocio_c',
          'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_phone_home_c',
          'label' => 'LBL_SASA_PHONE_HOME_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_phone_mobile_c',
          'label' => 'LBL_SASA_PHONE_MOBILE_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_vehiculos_leads_1_name',
          'label' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE',
          'enabled' => true,
          'id' => 'SASA_VEHICULOS_LEADS_1SASA_VEHICULOS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'rowactions' => 
  array (
    'actions' => 
    array (
      0 => 
      array (
        'type' => 'rowaction',
        'css_class' => 'btn',
        'tooltip' => 'LBL_PREVIEW',
        'event' => 'list:preview:fire',
        'icon' => 'fa-eye',
        'acl_action' => 'view',
      ),
      1 => 
      array (
        'type' => 'rowaction',
        'name' => 'edit_button',
        'icon' => 'fa-pencil',
        'label' => 'LBL_EDIT_BUTTON',
        'event' => 'list:editrow:fire',
        'acl_action' => 'edit',
      ),
      2 => 
      array (
        'type' => 'convertbutton',
        'name' => 'lead_convert_button',
        'label' => 'LBL_CONVERT_BUTTON_LABEL',
        'acl_action' => 'edit',
      ),
      3 => 
      array (
        'type' => 'unlink-action',
        'name' => 'unlink_button',
        'icon' => 'fa-chain-broken',
        'label' => 'LBL_UNLINK_BUTTON',
      ),
    ),
  ),
  'type' => 'subpanel-list',
);
({
    extendsFrom: 'CreateView',
    initialize: function(options) {
	var self=this;
	este = self;
	self._super('initialize', [options]);

	self.model.once("sync",
		function() {
			//console.log("olaKace");
		},
		self
	);
    
	self.model.addValidationTask('sasa_phone_mobile_c', _.bind(self._ValidarNumeric_sasa_phone_mobile_c, self));
	self.model.addValidationTask('sasa_phone_home_c', _.bind(self._ValidarNumeric_sasa_phone_home_c, self));
	self.model.addValidationTask('email', _.bind(self._ValidarDatosDeContactacion, self));
	self.model.addValidationTask('sasa_num_docu_juridica_c', _.bind(self._ValidarNumeric_sasa_num_docu_juridica_c, self));
    },
    
    _ValidarNumeric_sasa_phone_mobile_c: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarNumeric_sasa_phone_mobile_c';
		if (!_.isEmpty(self.model.get('sasa_phone_mobile_c'))){
			if(/^\d+$/.exec(self.model.get('sasa_phone_mobile_c')) === null){//https://stackoverflow.com/questions/3291289/preg-match-in-javascript					
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Debe ingresar sólo números en los teléfonos de contacto"],
					autoClose: false,
				});
			
				errors['sasa_phone_mobile_c'] = errors['sasa_phone_mobile_c'] || {};
				errors['sasa_phone_mobile_c'].required = true;
			}
			if (self.model.get('sasa_phone_mobile_c').length<10) {
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar diez dígitos en Teléfono Celular"],
					autoClose: false,
				});
			
				errors['sasa_phone_mobile_c'] = errors['sasa_phone_mobile_c'] || {};
				errors['sasa_phone_mobile_c'].required = true;
			}
		}		
		callback(null, fields, errors);
    },    
    
    _ValidarNumeric_sasa_phone_home_c: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarNumeric_sasa_phone_home_c';
		if (!_.isEmpty(self.model.get('sasa_phone_home_c'))){
			if(/^\d+$/.exec(self.model.get('sasa_phone_home_c')) === null){//https://stackoverflow.com/questions/3291289/preg-match-in-javascript				
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Debe ingresar sólo números en los teléfonos de contacto"],
					autoClose: false,
				});
			
				errors['sasa_phone_home_c'] = errors['sasa_phone_home_c'] || {};
				errors['sasa_phone_home_c'].required = true;
			}
			if (self.model.get('sasa_phone_home_c').length<7) {
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar siete dígitos en Teléfono Fijo"],
					autoClose: false,
				});
			
				errors['sasa_phone_home_c'] = errors['sasa_phone_home_c'] || {};
				errors['sasa_phone_home_c'].required = true;
			}
		}
		callback(null, fields, errors);
    },
    
    _ValidarDatosDeContactacion: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarDatosDeContactacion';
		if (_.isEmpty(self.model.get('sasa_phone_mobile_c')) && _.isEmpty(self.model.get('sasa_phone_home_c')) && _.isEmpty(self.model.get('email'))){			
			app.alert.show(message_id, {
				level: 'info',
				title: 'Nota',
				messages: ["Debe ingresar al menos un dato de contacto"],
				autoClose: false,
			});
			
			errors['sasa_phone_mobile_c'] = errors['sasa_phone_mobile_c'] || {};
			errors['sasa_phone_mobile_c'].required = true;
			
			errors['sasa_phone_home_c'] = errors['sasa_phone_home_c'] || {};
			errors['sasa_phone_home_c'].required = true;
			
			errors['email'] = errors['email'] || {};
			errors['email'].required = true;
		}
		callback(null, fields, errors);
    },


    _ValidarNumeric_sasa_num_docu_juridica_c: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarNumeric_sasa_num_docu_juridica_c';
		if (!_.isEmpty(self.model.get('sasa_num_docu_juridica_c'))){
			if(/^\d+$/.exec(self.model.get('sasa_num_docu_juridica_c')) === null){//https://stackoverflow.com/questions/3291289/preg-match-in-javascript					
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar sólo números en el Número de Documento Persona Jurídica"],
					autoClose: false,
				});
			
				errors['sasa_num_docu_juridica_c'] = errors['sasa_num_docu_juridica_c'] || {};
				errors['sasa_num_docu_juridica_c'].required = true;
			}
			if (self.model.get('sasa_num_docu_juridica_c').length<9 && self.model.get('sasa_tipo_de_persona_c')=='J') {
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar nueve dígitos en Número de Documento Persona Jurídica"],
					autoClose: false,
				});
			
				errors['sasa_num_docu_juridica_c'] = errors['sasa_num_docu_juridica_c'] || {};
				errors['sasa_num_docu_juridica_c'].required = true;
			}
		}		
		callback(null, fields, errors);
    },
})

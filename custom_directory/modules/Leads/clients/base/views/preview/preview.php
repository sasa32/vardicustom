<?php
$viewdefs['Leads'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'type' => 'fullname',
                'label' => 'LBL_NAME',
                'dismiss_label' => true,
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'salutation',
                    'type' => 'enum',
                    'enum_width' => 'auto',
                    'searchBarThreshold' => 7,
                  ),
                  1 => 'first_name',
                  2 => 'last_name',
                ),
              ),
              4 => 
              array (
                'name' => 'converted',
                'type' => 'badge',
                'dismiss_label' => true,
                'readonly' => true,
                'related_fields' => 
                array (
                  0 => 'account_id',
                  1 => 'account_name',
                  2 => 'contact_id',
                  3 => 'contact_name',
                  4 => 'opportunity_id',
                  5 => 'opportunity_name',
                  6 => 'converted_opp_name',
                ),
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labels' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'sasa_nombres_c',
                'label' => 'LBL_SASA_NOMBRES_C',
              ),
              1 => 
              array (
                'name' => 'sasa_primerapellido_c',
                'label' => 'LBL_SASA_PRIMERAPELLIDO_C',
              ),
              2 => 
              array (
                'name' => 'sasa_last_name_2_c',
                'label' => 'LBL_SASA_LAST_NAME_2_C',
              ),
              3 => 
              array (
                'name' => 'sasa_compania_c',
                'label' => 'LBL_SASA_COMPANIA_C',
              ),
              4 => 
              array (
                'name' => 'sasa_unidad_de_negocio_c',
                'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_C',
              ),
              5 => 
              array (
                'name' => 'sasa_tipo_de_persona_c',
                'label' => 'LBL_SASA_TIPO_DE_PERSONA_C',
              ),
              6 => 
              array (
                'span' => 12,
              ),
              7 => 
              array (
                'name' => 'account_name',
              ),
              8 => 
              array (
                'span' => 12,
              ),
              9 => 
              array (
                'name' => 'sasa_tipo_documento_c',
                'label' => 'LBL_SASA_TIPO_DOCUMENTO_C',
              ),
              10 => 
              array (
                'name' => 'sasa_numero_documento_c',
                'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
              ),
              11 => 
              array (
                'name' => 'sasa_phone_mobile_c',
                'label' => 'LBL_SASA_PHONE_MOBILE_C',
              ),
              12 => 
              array (
                'name' => 'email',
              ),
              13 => 
              array (
                'name' => 'sasa_municipios_leads_1_name',
                'label' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE',
              ),
              14 => 
              array (
                'name' => 'sasa_municipios_leads_2_name',
                'label' => 'LBL_SASA_MUNICIPIOS_LEADS_2_FROM_SASA_MUNICIPIOS_TITLE',
              ),
              15 => 
              array (
                'name' => 'primary_address',
                'type' => 'fieldset',
                'css_class' => 'address',
                'label' => 'LBL_PRIMARY_ADDRESS',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'primary_address_street',
                    'css_class' => 'address_street',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_STREET',
                  ),
                  1 => 
                  array (
                    'name' => 'primary_address_city',
                    'css_class' => 'address_city',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_CITY',
                  ),
                  2 => 
                  array (
                    'name' => 'primary_address_state',
                    'css_class' => 'address_state',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_STATE',
                  ),
                  3 => 
                  array (
                    'name' => 'primary_address_postalcode',
                    'css_class' => 'address_zip',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
                  ),
                  4 => 
                  array (
                    'name' => 'primary_address_country',
                    'css_class' => 'address_country',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
                  ),
                ),
              ),
              16 => 
              array (
                'name' => 'alt_address',
                'type' => 'fieldset',
                'css_class' => 'address',
                'label' => 'LBL_ALT_ADDRESS',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'alt_address_street',
                    'css_class' => 'address_street',
                    'placeholder' => 'LBL_ALT_ADDRESS_STREET',
                  ),
                  1 => 
                  array (
                    'name' => 'alt_address_city',
                    'css_class' => 'address_city',
                    'placeholder' => 'LBL_ALT_ADDRESS_CITY',
                  ),
                  2 => 
                  array (
                    'name' => 'alt_address_state',
                    'css_class' => 'address_state',
                    'placeholder' => 'LBL_ALT_ADDRESS_STATE',
                  ),
                  3 => 
                  array (
                    'name' => 'alt_address_postalcode',
                    'css_class' => 'address_zip',
                    'placeholder' => 'LBL_ALT_ADDRESS_POSTALCODE',
                  ),
                  4 => 
                  array (
                    'name' => 'alt_address_country',
                    'css_class' => 'address_country',
                    'placeholder' => 'LBL_ALT_ADDRESS_COUNTRY',
                  ),
                  5 => 
                  array (
                    'name' => 'copy',
                    'label' => 'NTC_COPY_PRIMARY_ADDRESS',
                    'type' => 'copy',
                    'mapping' => 
                    array (
                      'primary_address_street' => 'alt_address_street',
                      'primary_address_city' => 'alt_address_city',
                      'primary_address_state' => 'alt_address_state',
                      'primary_address_postalcode' => 'alt_address_postalcode',
                      'primary_address_country' => 'alt_address_country',
                    ),
                  ),
                ),
              ),
              17 => 
              array (
                'name' => 'sasa_phone_home_c',
                'label' => 'LBL_SASA_PHONE_HOME_C',
              ),
              18 => 
              array (
                'name' => 'birthdate',
                'comment' => 'The birthdate of the contact',
                'label' => 'LBL_BIRTHDATE',
              ),
              19 => 
              array (
                'name' => 'sasa_genero_c',
                'label' => 'LBL_SASA_GENERO_C',
              ),
              20 => 
              array (
                'name' => 'status',
                'type' => 'status',
              ),
              21 => 'lead_source',
              22 => 
              array (
                'name' => 'sasa_detalle_origen_c',
                'label' => 'LBL_SASA_DETALLE_ORIGEN_C',
              ),
              23 => 
              array (
                'name' => 'sasa_marcas_leads_1_name',
                'label' => 'LBL_SASA_MARCAS_LEADS_1_FROM_SASA_MARCAS_TITLE',
              ),
              24 => 
              array (
                'name' => 'sasa_vehiculos_leads_1_name',
                'label' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE',
              ),
              25 => 
              array (
                'name' => 'assigned_user_name',
                'related_fields' => 
                array (
                  0 => 'assigned_user_id',
                ),
                'label' => 'LBL_ASSIGNED_TO',
              ),
              26 => 
              array (
                'name' => 'team_name',
                'studio' => 
                array (
                  'portallistview' => false,
                  'portalrecordview' => false,
                ),
                'label' => 'LBL_TEAMS',
              ),
              27 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              28 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              29 => 
              array (
                'name' => 'description',
              ),
              30 => 
              array (
                'readonly' => false,
                'name' => 'sasa_campanausados_c',
                'label' => 'LBL_SASA_CAMPANAUSADOS_C',
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
          'useTabs' => false,
        ),
      ),
    ),
  ),
);

<?php
// created: 2021-06-04 20:14:39
$viewdefs['Leads']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'sasa_numero_documento_c' => 
    array (
    ),
    'sasa_tipo_documento_c' => 
    array (
    ),
    'sasa_tipo_de_persona_c' => 
    array (
    ),
    'first_name' => 
    array (
    ),
    'sasa_tipodeformulario_c' => 
    array (
    ),
    'last_name' => 
    array (
    ),
    'sasa_phone_home_c' => 
    array (
    ),
    'email' => 
    array (
    ),
    'sasa_primerapellido_c' => 
    array (
    ),
    'sasa_phone_mobile_c' => 
    array (
    ),
    'status' => 
    array (
    ),
    'sasa_tipo_solicitud_c' => 
    array (
    ),
    'sasa_vehiculos_leads_1_name' => 
    array (
    ),
    'cd_area_c' => 
    array (
    ),
    'primary_address_city' => 
    array (
    ),
    'sasa_compania_c' => 
    array (
    ),
    'account_name' => 
    array (
    ),
    'sasa_unidad_de_negocio_c' => 
    array (
    ),
    'lead_source' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
  ),
);
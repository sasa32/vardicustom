<?php
// created: 2023-02-08 11:06:10
$viewdefs['Leads']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'sasa_nombres_c',
          'label' => 'LBL_SASA_NOMBRES_C',
        ),
        1 => 
        array (
          'name' => 'sasa_primerapellido_c',
          'label' => 'LBL_SASA_PRIMERAPELLIDO_C',
        ),
        2 => 
        array (
          'name' => 'sasa_last_name_2_c',
          'label' => 'LBL_SASA_LAST_NAME_2_C',
        ),
        3 => 
        array (
          'name' => 'sasa_compania_c',
          'label' => 'LBL_SASA_COMPANIA_C',
        ),
        4 => 
        array (
          'name' => 'sasa_unidad_de_negocio_c',
          'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_C',
        ),
        5 => 
        array (
          'name' => 'sasa_tipo_solicitud_c',
          'label' => 'LBL_SASA_TIPO_SOLICITUD_C',
        ),
        6 => 
        array (
          'name' => 'sasa_tipo_de_persona_c',
          'label' => 'LBL_SASA_TIPO_DE_PERSONA_C',
        ),
        7 => 'account_name',
        8 => 
        array (
          'name' => 'sasa_tipo_documento_c',
          'label' => 'LBL_SASA_TIPO_DOCUMENTO_C',
        ),
        9 => 
        array (
          'name' => 'sasa_numero_documento_c',
          'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
        ),
        10 => 'email',
        11 => 
        array (
          'name' => 'birthdate',
          'comment' => 'The birthdate of the contact',
          'label' => 'LBL_BIRTHDATE',
        ),
        12 => 'phone_home',
        13 => 'phone_mobile',
        14 => 
        array (
          'name' => 'sasa_municipios_leads_1_name',
          'label' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        15 => 'primary_address_street',
        16 => 'primary_address_postalcode',
        17 => 
        array (
          'name' => 'sasa_genero_c',
          'label' => 'LBL_SASA_GENERO_C',
        ),
        18 => 'status',
        19 => 
        array (
          'name' => 'lead_source',
          'comment' => 'Lead source (ex: Web, print)',
          'label' => 'LBL_LEAD_SOURCE',
        ),
        20 => 
        array (
          'name' => 'sasa_detalle_origen_c',
          'label' => 'LBL_SASA_DETALLE_ORIGEN_C',
        ),
        21 => 
        array (
          'name' => 'sasa_vehiculos_leads_1_name',
          'label' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE',
        ),
        22 => 
        array (
          'name' => 'sasa_puntos_de_ventas_leads_1_name',
          'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
        ),
        23 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        24 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
        25 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        26 => 
        array (
          'name' => 'do_not_call',
          'comment' => 'An indicator of whether contact can be called',
          'label' => 'LBL_DO_NOT_CALL',
        ),
      ),
    ),
  ),
);
<?php  
	
	/**
	 * 
	 */
	require_once('include/SugarQuery/SugarQuery.php');
	class Process_Lead{
		var $return;
		var $db;
		var $depar;
		
		function __construct($data_array){
			$this->return = array();
			$this->db = DBManagerFactory::getInstance();
			$this->main_leads($data_array);
		}

		public function main_leads($data_array){
			try{
				$validatelead = $this->validate_lead($data_array);
				$GLOBALS['log']->security("LeadAccount ".print_r($validatelead,true));

				if($validatelead['band']){
					$status = $this->related_note($validatelead,$data_array);
					$this->return = $status;
				}else{
					$status = $this->create_lead($data_array);
					$this->return = $status;
				}

			}catch(Exception $e){
				return array('Error' => '100', 'Description' => $e->getMessage());
			}
		}

		//Validar si ya existe un lead o una cuenta con el mismo Numero de Identificación, email o celular
		public function validate_lead($record){
			
			if (!empty($record['sasa_numero_documento_c'])) {
				//Consultar si ya hay un lead o cuenta con el mismo numero de identificacion
				$query_getlead= $this->db->query("SELECT leads_cstm.id_c IdLead FROM leads INNER JOIN leads_cstm ON leads_cstm.id_c=leads.id WHERE (leads_cstm.sasa_numero_documento_c='{$record['sasa_numero_documento_c']}' OR leads_cstm.sasa_num_docu_juridica_c='{$record['sasa_numero_documento_c']}') AND leads.deleted=0 LIMIT 1");

				$Lead_query = $this->db->fetchByAssoc($query_getlead);
				if ($Lead_query['IdLead'] != NULL || !empty($Lead_query['IdLead'])) {
					//Si existe se debe crear una nota a la cuenta o lead
					$validate_array = array('band' => true,"Id_Account"=>$Lead_query['IdAccount'],'Id_Lead'=>$Lead_query['IdLead']);
					return $validate_array;
				}else{
					$GLOBALS['log']->security("Cuenta Hay ");
					$query_getaccount= $this->db->query("SELECT accounts.id IdAccount FROM accounts INNER JOIN accounts_cstm ON accounts.id=accounts_cstm.id_c WHERE accounts_cstm.sasa_numero_documento_c='{$record['sasa_numero_documento_c']}' AND accounts.deleted=0 LIMIT 1");

					$Account_query = $this->db->fetchByAssoc($query_getaccount);
					if ($Account_query['IdAccount'] != NULL || !empty($Account_query['IdAccount'])) {
						//Si existe se debe crear una nota a la cuenta o lead
						$validate_array = array('band' => true,"Id_Account"=>$Account_query['IdAccount'],'Id_Lead'=>$Account_query['IdLead']);
						return $validate_array;
					}
				}
			}elseif (!empty($record['email'])) {
				//Consultar si ya hay un lead o cuenta con el mismo numero de identificacion
				$query_getlead_account = $this->db->query("SELECT email_addr_bean_rel.bean_id IdRecord, email_addr_bean_rel.bean_module Module FROM email_addresses INNER JOIN email_addr_bean_rel ON email_addresses.id=email_addr_bean_rel.email_address_id WHERE email_addresses.email_address='{$record['email']}' AND email_addresses.deleted=0 AND email_addr_bean_rel.deleted=0 AND (email_addr_bean_rel.bean_module='Leads' OR email_addr_bean_rel.bean_module='Accounts') LIMIT 1");

				$Lead_Account = $this->db->fetchByAssoc($query_getlead_account);
				if ($Lead_Account['IdRecord'] != NULL) {
					//Si existe se debe crear una nota a la cuenta o lead
					$validate_array = array('band' => true,"Id_Record"=>$Lead_Account['IdRecord'],'Module'=>$Lead_Account['Module']);
					return $validate_array;
				}
			}elseif(!empty($record['sasa_phone_mobile_c'])){
				$query_getlead_account = $this->db->query("SELECT leads_cstm.id_c IdLead, accounts_cstm.id_c IdAccount FROM leads INNER JOIN leads_cstm ON leads_cstm.id_c=leads.id LEFT JOIN accounts_cstm ON accounts_cstm.sasa_cel_principal_c=leads_cstm.sasa_phone_mobile_c WHERE leads_cstm.sasa_phone_mobile_c='{$record['sasa_phone_mobile_c']}' AND leads.deleted=0 LIMIT 1");

				$Lead_Account = $this->db->fetchByAssoc($query_getlead_account);
				if ($Lead_Account['IdAccount'] != NULL || $Lead_Account['IdLead'] != NULL) {
					//Si existe se debe crear una nota a la cuenta o lead
					$validate_array = array('band' => true,"Id_Account"=>$Lead_Account['IdAccount'],'Id_Lead'=>$Lead_Account['IdLead']);
					return $validate_array;
				}
			}

			return $validate_array;
		}

		//Crear nota relacionada a la cuenta o lead
		public function related_note($validatelead,$data_array){
			 
			//Identificar el id del registro al que quedara relacionada la nota
			if (array_key_exists('Id_Account', $validatelead)) {
				if ($validatelead['Id_Account'] != null || $validatelead['Id_Account'] != "") {
					$GLOBALS['log']->security("Valida Cuenta");
					$idRecord  = $validatelead['Id_Account'];
					$modulo = "Accounts";
				}elseif ($validatelead['Id_Lead'] != null || $validatelead['$Id_Lead'] != "") {
					$GLOBALS['log']->security("Valida Lead");
					$idRecord  = $validatelead['Id_Lead'];
					$modulo = "Leads";
				}
			}elseif (array_key_exists('Id_Record', $validatelead)) {
				if ($validatelead['Id_Record'] != null || $validatelead['Id_Record'] != "") {
					$GLOBALS['log']->security("Valida Email");
					$idRecord  = $validatelead['Id_Record'];
					$modulo = $validatelead['Module'];
				}
			}

			$RecordRelated = BeanFactory::getBean($modulo, $idRecord, array('disable_row_level_security' => true));

			//Crear nota y vincular la Cuenta o Lead a ella, adicional de vincular en la descripción la información del Lead que se intentó crear pero ya existia
			$Note = BeanFactory::newBean('Notes');
			$Note->parent_type = $modulo;
			$Note->parent_id = $idRecord;
			$Note->name = "Lead ".$data_array['sasa_nombres_c']." ".$data_array['sasa_primerapellido_c'];
			$textdescription = "El señor/a ".$data_array['sasa_nombres_c']." ".$data_array['sasa_primerapellido_c']." se ha vuelto a registrar en un formulario del sitio web.\n";
			$textdescription .= "Sus datos de contacto son los siguientes, por favor verificar si se deben actualizar en el sistema: \n";
			$textdescription .= "Número de identificación: {$data_array['sasa_numero_documento_c']}\n";
			$textdescription .= "Correo: {$data_array['email']}\n";
			$textdescription .= "Teléfono Celular: {$data_array['sasa_phone_mobile_c']}\n";
			$textdescription .= "Teléfono fijo: {$data_array['sasa_phone_home_c']}\n";
			$textdescription .= "Dirección: {$data_array['primary_address_street']} {$data_array['primary_address_city']}\n";
			$textdescription .= "Formulario: {$data_array['sasa_tipodeformulario_c']}\n";
			$textdescription .= "Línea de vehículo: {$data_array['sasa_cod_linea_c']}\n";
			$Note->description = $textdescription;
			//$Note->assigned_user_id = $RecordRelated->assigned_user_id;
			$Note->save();

			$json_response = $this->return_fields($RecordRelated->id,$modulo);

			return $json_response;
			//return array('status' => "Nota creada", "Nota" => $Note->id,"RecordRelated"=>$RecordRelated->id); 
		}

		public function create_lead($record){
			
			$Lead = BeanFactory::newBean('Leads');
			foreach ($record as $key => $value) {
				if ($key != "sasa_unidad_de_negocio_c") {
					$Lead->{$key} = $value;
					switch ($key) {
						case 'sasa_tipo_documento_c':
							if ($value == "N" || $value=="NIT") {
								$Lead->sasa_tipo_de_persona_c = "J";
							}else{
								$Lead->sasa_tipo_de_persona_c = "N";
							}
							break;
						case 'sasa_tipodeformulario_c':
							$TipoYDetalle = $this->get_tipoydetalle($value);
							$Lead->sasa_tipo_solicitud_c = $TipoYDetalle['tiposolicitudSugar'];
							$Lead->sasa_detalle_origen_c = $TipoYDetalle['Detalledeorigen'];
							break;
						case 'email':
							$Lead->email1 = $value;
							break;
						case 'primary_address_state':
							$departamento = trim($value);
							$this->depar = $departamento;

							break;
						case 'primary_address_city':
							$municipio = trim($value);
							$depar = trim($record['primary_address_state']);
							$query_get_municipio = "SELECT sasa_municipios.id, sasa_departamentos.name FROM sasa_municipios LEFT JOIN sasa_departamentos_sasa_municipios_1_c ON sasa_departamentos_sasa_municipios_1_c.sasa_departamentos_sasa_municipios_1sasa_municipios_idb=sasa_municipios.id LEFT JOIN sasa_departamentos ON sasa_departamentos_sasa_municipios_1_c.sasa_departamentos_sasa_municipios_1sasa_departamentos_ida=sasa_departamentos.id WHERE sasa_municipios.name like '{$municipio}%' AND sasa_municipios.deleted=0 ";
							if ($depar != "" || $depar != null) {
								$query_get_municipio .= "AND sasa_departamentos.name LIKE '%{$depar}%'";
							}
							
							$GLOBALS['log']->security("query: ".$query_get_municipio);
							$exceutequery = $this->db->query($query_get_municipio);
							$Ciudad = $this->db->fetchByAssoc($exceutequery);
							$Lead->sasa_municipios_leads_1sasa_municipios_ida = $Ciudad['id'];
							break;
						case 'sasa_municipio_principal_2_c':
							$municipio = trim($value);
							$depar = trim($record['primary_address_state']);
							$query_get_municipio = "SELECT sasa_municipios.id, sasa_departamentos.name FROM sasa_municipios LEFT JOIN sasa_departamentos_sasa_municipios_1_c ON sasa_departamentos_sasa_municipios_1_c.sasa_departamentos_sasa_municipios_1sasa_municipios_idb=sasa_municipios.id LEFT JOIN sasa_departamentos ON sasa_departamentos_sasa_municipios_1_c.sasa_departamentos_sasa_municipios_1sasa_departamentos_ida=sasa_departamentos.id WHERE sasa_municipios.name like '{$municipio}%' AND sasa_municipios.deleted=0 ";
							if ($depar != "" || $depar != null) {
								$query_get_municipio .= "AND sasa_departamentos.name LIKE '%{$depar}%'";
							}
							$exceutequery = $this->db->query($query_get_municipio);
							$Ciudad = $this->db->fetchByAssoc($exceutequery);

							$Lead->sasa_municipios_leads_1sasa_municipios_ida = $Ciudad['id'];
							$Lead->twitter = $value;
							$Lead->assistant = $depar;
							break;
						case 'lead_source':
							if ($value == "15" || $value == "web") {
								$Lead->lead_source = "20";
							}
							break;
						case 'sasa_genero_c':
							if ($value == "FEMENINO") {
								$Lead->sasa_genero_c = "F";
							}elseif ($value == "MASCULINO") {
								$Lead->sasa_genero_c = "M";
							}
							break;
						case 'sasa_unidad_de_negocio_c':
							$Lead->sasa_unidad_de_negocio_c = "";
							break;
						case 'sasa_compania_c':
							if ($value == "3") {
								$Lead->sasa_unidad_de_negocio_c = "340";
								$Lead->cd_area_c = "34044";
							}
							if ($value == "1") {
								$Lead->sasa_unidad_de_negocio_c = "110";
								$Lead->cd_area_c = "11011";
							}
							if ($value == "2") {
								$Lead->sasa_unidad_de_negocio_c = "210";
								$Lead->cd_area_c = "21010";
							}
							
							break;
						case 'sasa_cod_linea_c':
							if (strpos($value, 'FRONTIER') !== false) {
							    $query_vehiculos = "SELECT sasa_vehiculos_cstm.id_c, sasa_vehiculos_cstm.sas_cd_line_vehi_c FROM sasa_vehiculos_cstm INNER JOIN sasa_vehiculos ON sasa_vehiculos_cstm.id_c=sasa_vehiculos.id WHERE sasa_vehiculos.deleted=0 AND sasa_vehiculos_cstm.sas_cd_line_vehi_c='D23'";
							}else{
								$query_vehiculos = "SELECT sasa_vehiculos_cstm.id_c, sasa_vehiculos_cstm.sas_cd_line_vehi_c FROM `DicccionarioLineas` inner join sasa_vehiculos_cstm ON DicccionarioLineas.Codigo_Linea_Vehiculo=sasa_vehiculos_cstm.sas_cd_line_vehi_c WHERE DicccionarioLineas.FORM_SOLICITE_UNA_COTIZACION='{$value}' OR DicccionarioLineas.FORM_AUTORENUEVATE='{$value}' OR DicccionarioLineas.PLAN_RETOMA='{$value}'";
							}
							
							$exceutequeryvehiculos = $this->db->query($query_vehiculos);
							$Vehiculo = $this->db->fetchByAssoc($exceutequeryvehiculos);
							$Lead->sasa_vehiculos_leads_1sasa_vehiculos_ida = $Vehiculo['id_c'];
							$Lead->sasa_cod_linea_c = $Vehiculo['sas_cd_line_vehi_c'];
							$Lead->facebook = $value;
							break;
						default:
							# code...
							break;
					}
					if (!array_key_exists('sasa_tipo_documento_c',$key)) {
						$Lead->sasa_tipo_de_persona_c = "N";
					}
				}
				
			}
			$Lead->save();

			$json_response = $this->return_fields($Lead->id,"Leads");
			$this->Create_habeas($Lead);

			return $json_response;
		}

		public function get_tipoydetalle($requestType){
			$query_get_tipoydetalle = $this->db->query("SELECT * FROM diccionariofromleads WHERE diccionariofromleads.requestType='{$requestType}'");
			$TipoYDetalle = $this->db->fetchByAssoc($query_get_tipoydetalle);
			return $TipoYDetalle;
		}

		public function return_fields($id_record,$module){
			$RecordRelated = BeanFactory::getBean($module, $id_record, array('disable_row_level_security' => true));

			$fields_return = array(
				'id'=> $RecordRelated->id,
				'name' => $RecordRelated->name,
				'date_entered' => $RecordRelated->date_entered,
				'date_modified' => $RecordRelated->date_modified,
				'modified_user_id' => $RecordRelated->modified_user_id,
				'modified_by_name' => $RecordRelated->modified_by_name,
				'created_by' => $RecordRelated->created_by,
				'created_by_name' => $RecordRelated->created_by_name
			);

			return $fields_return;
		}


		public function get_return(){
			return $this->return;
		}

		public function Create_habeas($Lead){

			$SASA_Habeas_Data = BeanFactory::newBean('SASA_Habeas_Data');
			//Mapeo 
			$SASA_Habeas_Data->documents_sasa_habeas_data_1documents_ida = "060e244e-7930-11ec-97e6-06be4ff698d4";
			$SASA_Habeas_Data->leads_sasa_habeas_data_1leads_ida = $Lead->id;
			$SASA_Habeas_Data->sasa_estado_autorizacion_c = "1";
			$SASA_Habeas_Data->sasa_auto_contactacion_c = "1";
			$SASA_Habeas_Data->sasa_fuente_autorizacion_c = "W";
			//Mapeo compañia
			//Buscar la compañia
			$Compania = BeanFactory::newBean("sasa_Companias");
			$Compania->retrieve_by_string_fields( 
				array( 
					'sasa_cod_c' => $Lead->sasa_compania_c
				)
			); 
			$SASA_Habeas_Data->sasa_companias_sasa_habeas_data_1sasa_companias_ida = $Compania->id;
			$SASA_Habeas_Data->save();
			
		}
		
	}


?>
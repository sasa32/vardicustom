<?php
$popupMeta = array (
    'moduleMain' => 'Lead',
    'varName' => 'LEAD',
    'orderBy' => 'last_name, first_name',
    'whereClauses' => array (
  'status' => 'leads.status',
  'account_name' => 'leads.account_name',
  'name' => 'leads.name',
  'phone_home' => 'leads.phone_home',
  'phone_mobile' => 'leads.phone_mobile',
  'email' => 'leads.email',
  'date_entered' => 'leads.date_entered',
  'date_modified' => 'leads.date_modified',
  'sasa_tipo_solicitud_c' => 'leads_cstm.sasa_tipo_solicitud_c',
  'sasa_vehiculos_leads_1_name' => 'leads.sasa_vehiculos_leads_1_name',
  'sasa_puntos_de_ventas_leads_1_name' => 'leads.sasa_puntos_de_ventas_leads_1_name',
),
    'searchInputs' => array (
  3 => 'status',
  4 => 'account_name',
  6 => 'name',
  7 => 'phone_home',
  8 => 'phone_mobile',
  9 => 'email',
  10 => 'date_entered',
  11 => 'date_modified',
  12 => 'sasa_tipo_solicitud_c',
  13 => 'sasa_vehiculos_leads_1_name',
  14 => 'sasa_puntos_de_ventas_leads_1_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'fullname',
    'label' => 'LBL_NAME',
    'width' => '10',
    'name' => 'name',
  ),
  'status' => 
  array (
    'name' => 'status',
    'width' => '10',
  ),
  'sasa_tipo_solicitud_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_TIPO_SOLICITUD_C',
    'width' => 10,
    'name' => 'sasa_tipo_solicitud_c',
  ),
  'sasa_vehiculos_leads_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE',
    'id' => 'SASA_VEHICULOS_LEADS_1SASA_VEHICULOS_IDA',
    'width' => 10,
    'name' => 'sasa_vehiculos_leads_1_name',
  ),
  'sasa_puntos_de_ventas_leads_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
    'id' => 'SASA_PUNTOS_DE_VENTAS_LEADS_1SASA_PUNTOS_DE_VENTAS_IDA',
    'width' => 10,
    'name' => 'sasa_puntos_de_ventas_leads_1_name',
  ),
  'account_name' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_NAME',
    'width' => '10',
    'name' => 'account_name',
  ),
  'phone_home' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_HOME_PHONE',
    'width' => '10',
    'name' => 'phone_home',
  ),
  'phone_mobile' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => '10',
    'name' => 'phone_mobile',
  ),
  'email' => 
  array (
    'name' => 'email',
    'width' => '10',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10',
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10',
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
      2 => 'salutation',
    ),
    'name' => 'name',
  ),
  'STATUS' => 
  array (
    'width' => 10,
    'label' => 'LBL_LIST_STATUS',
    'default' => true,
    'name' => 'status',
  ),
  'PHONE_HOME' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_HOME_PHONE',
    'width' => 10,
    'default' => true,
    'name' => 'phone_home',
  ),
  'PHONE_MOBILE' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => 10,
    'default' => true,
    'name' => 'phone_mobile',
  ),
  'EMAIL' => 
  array (
    'type' => 'email',
    'studio' => 
    array (
      'visible' => true,
      'searchview' => true,
      'editview' => true,
      'editField' => true,
    ),
    'link' => 'email_addresses_primary',
    'label' => 'LBL_EMAIL_ADDRESS',
    'sortable' => false,
    'width' => 10,
    'default' => true,
    'name' => 'email',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
    'name' => 'date_entered',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
    'name' => 'date_modified',
  ),
),
);

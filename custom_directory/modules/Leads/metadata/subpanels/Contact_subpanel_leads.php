<?php
// created: 2020-07-09 16:22:11
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_LIST_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'sort_order' => 'asc',
    'sort_by' => 'last_name',
    'module' => 'Leads',
    'width' => 10,
    'default' => true,
  ),
  'sasa_tipo_documento_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SASA_TIPO_DOCUMENTO_C',
    'width' => 10,
  ),
  'sasa_numero_documento_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
    'width' => 10,
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'sasa_compania_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SASA_COMPANIA_C',
    'width' => 10,
  ),
  'sasa_unidad_de_negocio_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_C',
    'width' => 10,
  ),
  'sasa_phone_home_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_SASA_PHONE_HOME_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_phone_mobile_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_SASA_PHONE_MOBILE_C',
    'width' => 10,
    'default' => true,
  ),
  'first_name' => 
  array (
    'usage' => 'query_only',
  ),
  'last_name' => 
  array (
    'usage' => 'query_only',
  ),
  'salutation' => 
  array (
    'name' => 'salutation',
    'usage' => 'query_only',
  ),
);
<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class logic_hooks_leads
{
	/*
	logic_hooks_leads before_save 
	*/
	function before_save($bean, $event, $arguments)
	{	
		global $db;
		//Validacion segun requerimiento https://sasaconsultoria.sugarondemand.com/index.php#Tasks/e4ccb6fc-85ef-11ec-a51d-06f8a63ac887
		if ($bean->sasa_tipo_solicitud_c == "3" || $bean->sasa_tipo_solicitud_c == "7") {
			//Si el tipo de solicitd es Información de precios de Vehículos ó solicitud información Vehículos poblar la gestion comercial el conjunto de los 4 siguientes campos
			$bean->sasa_fechainiciogestionld_c = $bean->date_entered;
			$bean->sasa_fechafingestionld_c = $bean->date_entered;
			$bean->sasa_estadocontacto_c = "CONTACTADO";
			$bean->sasa_fechacontacto_c = $bean->date_entered;
			$bean->sasa_numerogestiones_c = "1";
		}

		if (!empty($bean->first_name)){
			setlocale(LC_ALL,"es_ES");
			$primernombre = iconv('UTF-8', 'ASCII//TRANSLIT', $bean->first_name);
			$segundonombre = iconv('UTF-8', 'ASCII//TRANSLIT', $bean->sasa_primerapellido_c);
			$segundoape = iconv('UTF-8', 'ASCII//TRANSLIT', $bean->sasa_last_name_2_c);

			$bean->sasa_nombres_c = $primernombre;
			$bean->sasa_primerapellido_c = $segundonombre;
			$bean->first_name = strtoupper($primernombre);
			$bean->last_name = strtoupper($segundonombre)." ".strtoupper($segundoape);

		}
		//Capturar el cierre de gestión del Leads (Gestionado Call, En Proceso Comercial, Sin Interés). Casos (Cancelado, Cerrado).
		$statusRecord = array("G","C","F","J");
		//$GLOBALS['log']->security("lobeforelead ".$bean->status);
		if (in_array($bean->status, $statusRecord) && ($bean->sasa_fechafingestionld_c==null || $bean->sasa_fechafingestionld_c=="")) {
			$mifecha = new DateTime(); 
            //$mifecha->modify('+5 hours');
			$bean->sasa_fechafingestionld_c = $mifecha->format('Y-m-d H:i:s');
		}

		//validar el cambio del lead, si ya finalizó el proceso recolectar la documentación de la última tarea
		//Trasladar el comentario de la tarea de enviar notificación al campo Comentario/leads, Observación/casos. Según control proceso 2
		if (in_array($bean->status, $statusRecord) && $bean->fetched_row['status']!=$bean->status) {
			if (!empty($bean->id)) {
				//query para obtener la documentación de la última tarea
				$query_get_doc_lastcall = "SELECT id, description, name FROM calls INNER JOIN calls_cstm ON calls.id=calls_cstm.id_c WHERE (calls_cstm.sasa_controlproceso_c IS NOT NULL OR calls_cstm.sasa_controlproceso_c != '') AND (sasa_tipo_de_llamada_c='INFORMACION_COMERCIAL' OR sasa_tipo_de_llamada_c='SE_GENERO_COTIZACION') AND calls.parent_id='{$bean->id}' AND deleted=0 ORDER BY calls.date_entered DESC LIMIT 1";
				$execute_query_get_doc_lastcall = $db->query($query_get_doc_lastcall);
				$resultquery_lastCall = $GLOBALS['db']->fetchByAssoc($execute_query_get_doc_lastcall);
				if (!empty($resultquery_lastCall['description'])) {
					$bean->description .= "\nGestión: ".$resultquery_lastCall['description'];
				}
			}
		}
		
		if (!isset($bean->logic_hooks_leads_before_save_ignore_update) || $bean->logic_hooks_leads_before_save_ignore_update === false){//antiloop
			$bean->logic_hooks_leads_before_save_ignore_update = true;//antiloop


			//ciudad ppal
			if (!empty($bean->sasa_municipio_principal_2_c)){
				$result = $GLOBALS['db']->query("SELECT id FROM sasa_municipios WHERE sasa_codigomunicipio_c LIKE '{$bean->sasa_municipio_principal_2_c}' AND deleted=0 limit 1");
				$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);	
				
				if ($bean->load_relationship('sasa_municipios_leads_1')){
					$relatedBeans = $bean->sasa_municipios_leads_1->add($sasa_municipios["id"]);
					$bean->sasa_municipio_principal_2_c ="";
				}
			}
			//ciudad Alter
			if (!empty($bean->sasa_municipio_secundario_2_c)){
				$result = $GLOBALS['db']->query("SELECT id FROM sasa_municipios WHERE sasa_codigomunicipio_c LIKE '{$bean->sasa_municipio_secundario_2_c}' AND deleted=0 limit 1");
				$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);	
				
				if ($bean->load_relationship('sasa_municipios_leads_2')){
					$relatedBeans = $bean->sasa_municipios_leads_2->add($sasa_municipios["id"]);
					$bean->sasa_municipio_secundario_2_c ="";
				}
			}
			

			//Mapeo de codigo de linea de vehiculo
			if (!empty($bean->sasa_cod_linea_c)) {
				$resultlinea = $GLOBALS['db']->query("SELECT id_c FROM sasa_vehiculos_cstm INNER JOIN sasa_vehiculos ON sasa_vehiculos_cstm.id_c=sasa_vehiculos.id WHERE sas_cd_line_vehi_c='{$bean->sasa_cod_linea_c}' AND deleted=0 limit 1");
				$sasa_lineavehiculo =  $GLOBALS['db']->fetchByAssoc($resultlinea);

				if ($bean->load_relationship('sasa_vehiculos_leads_1')){
					$relatedBeans = $bean->sasa_vehiculos_leads_1->add($sasa_lineavehiculo["id_c"]);
				}
			}
			
			/*
			* https://sasaconsultoria.sugarondemand.com/#Tasks/b3b62e86-5428-11ea-b5fd-02dfd714a754
			* Funcionalidad para validar si al guardar un registro este tiene diligenciado un dato de contactación tales como un email, celular principal o teléfono principal,
			*/
			if( empty(trim($bean->sasa_phone_mobile_c)) and empty(trim($bean->sasa_phone_home_c)) and empty($bean->emailAddress->addresses) ){
				//throw new SugarApiExceptionInvalidParameter("Debe ingresar al menos un dato de contacto");//usar http_code 404 en lugar de 422 para que no salga error de conexion			
			}
			
			/*
			* Telefonos solo numeros (entero)
			*/
			$int_fields_error = false;
			$int_fields = array(
				trim($bean->sasa_phone_home_c),
				trim($bean->sasa_phone_mobile_c),
			);	
			foreach( $int_fields  as $key => $int_field){  
				if(!empty($int_field)){
					if(!preg_match('/^\d+$/',$int_field)){ //https://www.codexpedia.com/php/php-checking-if-a-variable-contains-only-digits/
						$int_fields_error = true;	
					}
				}
			}	
			if($int_fields_error){
				throw new SugarApiExceptionInvalidParameter("Debe ingresar sólo números en los teléfonos de contactación");//usar http_code 404 en lugar de 422 para que no salga error de conexion
			}


			/*
			* Telefonos solo numeros (entero)
			*/
			$int_fields_error_numid = false;
			$int_fields_numid = array(
				trim($bean->sasa_num_docu_juridica_c),
			);	
			foreach( $int_fields_numid  as $key => $int_field){  
				if(!empty($int_field)){
					if(!preg_match('/^\d+$/',$int_field)){ //https://www.codexpedia.com/php/php-checking-if-a-variable-contains-only-digits/
						$int_fields_error_numid = true;	
					}
				}
			}	
			if ($bean->sasa_num_docu_juridica_c == "J") {
				if($int_fields_error_numid){
					throw new SugarApiExceptionInvalidParameter("Ingresar sólo números en el Número de Documento Persona Jurídica");//usar http_code 404 en lugar de 422 para que no salga error de conexion
				}
				if (strlen($bean->sasa_num_docu_juridica_c)<9) {
					throw new SugarApiExceptionInvalidParameter("Ingresar nueve dígitos en Número de Documento Persona Jurídica");
				}
			}
		
			/*
			* https://sasaconsultoria.sugarondemand.com/index.php#Tasks/d0030608-74ea-11ea-86e4-02fb8f607ac4
			*/
			if (empty($bean->team_id)){
				$bean->team_id = 1;
			}	
			
			if (empty($bean->team_set_id)){
				$bean->team_set_id = 1;
			}	
			
			if (empty($bean->acl_team_set_id)){
				$bean->acl_team_set_id = 1;
			}	

			if (empty($bean->created_by)){
				$bean->created_by = 1;
			}
			
			if (empty($bean->assigned_user_id)){
				$bean->assigned_user_id = $bean->created_by;
			}

			//Asiganr el mismo numero para el campo numero de documento en caso de que sea persona natural y juridica
			if ($bean->sasa_tipo_de_persona_c == "J" && !empty($bean->sasa_numero_documento_c)) {
				$bean->sasa_num_docu_juridica_c=$bean->sasa_numero_documento_c;
			}elseif ($bean->sasa_tipo_de_persona_c == "N" && !empty($bean->sasa_numero_documento_c)) {
				$bean->sasa_num_docu_juridica_c=$bean->sasa_numero_documento_c;
			}



			//Conversion de lead
			if ($bean->status != "D" && $bean->status != "I") {
				if(!empty($bean->sasa_numero_documento_c)){
					$GLOBALS['log']->security("LEADCONVERSION ");
					//Buscar si hay cuenta con mismo tipo de doc y mismo numero de doc
					$account = BeanFactory::newBean("Accounts");
					$account->retrieve_by_string_fields( 
						array( 
							'sasa_numero_documento_c' => $bean->sasa_numero_documento_c, 
							'sasa_tipo_documento_c' => $bean->sasa_tipo_documento_c
						)
					); 

					$NombreLead = $bean->first_name." ".$bean->last_name;
					$queryleadbycel = $GLOBALS['db']->query("SELECT * FROM accounts INNER JOIN accounts_cstm ON accounts.id=accounts_cstm.id_c WHERE (sasa_cel_principal_c='{$bean->sasa_phone_mobile_c}' OR sasa_cel_alternativo_c='{$bean->sasa_phone_mobile_c}' OR sasa_celular_otro_c='{$bean->sasa_phone_mobile_c}') AND accounts.name='{$NombreLead}' AND accounts.deleted=0");
					$leadcel = mysqli_fetch_array($queryleadbycel);

					$querynamelead = $GLOBALS['db']->query("SELECT id, name from accounts WHERE name='{$NombreLead}' AND accounts.deleted=0");

					$getnamelead = mysqli_fetch_array($querynamelead);
					//Validación por numero y tipo de documento iguales
					if(!empty($account->id)){	
						$fecha_lead = strtotime($bean->date_entered);
	            		$fecha_account = strtotime($account->date_entered);
	            		if ($fecha_lead > $fecha_account) {
			                $bean->status = 'D';
							$bean->converted = true;
							$bean->account_id = $account->id;
							$bean->account_name = $account->name;
			            }else{
			            	$bean->status = 'I';
							$bean->converted = true;
							$bean->account_id = $account->id;
							$bean->account_name = $account->name;
			            }
					}
					//Validación por celulares iguales
					if ($leadcel['id'] != null || $leadcel['id']!= "") {

						if (strtolower($NombreLead) == strtolower($bean->name)) {
							$fecha_lead = strtotime($bean->date_entered);
		            		$fecha_account = strtotime($leadcel['date_entered']);
		            		if ($fecha_lead > $fecha_account) {
				                $bean->status = 'D';
								$bean->converted = true;
								$bean->account_id = $account->id;
								$bean->account_name = $account->name;
				            }else{
				            	$bean->status = 'I';
								$bean->converted = true;
								$bean->account_id = $account->id;
								$bean->account_name = $account->name;
				            }
						}
					}elseif ($getnamelead['id'] != null || $getnamelead['id'] != "") {
						$Account = BeanFactory::getBean("Accounts", $getnamelead['id'], array('disable_row_level_security' => true));

						$bandemail = false;
						foreach ($bean->email as $key => $value) {
							$QueryLeadEmail = $GLOBALS['db']->query("SELECT accounts.date_entered, accounts.id IDCuenta from email_addresses INNER JOIN email_addr_bean_rel ON email_addresses.id=email_addr_bean_rel.email_address_id INNER JOIN accounts ON email_addr_bean_rel.bean_id=accounts.id WHERE email_addresses.email_address='{$value['email_address']}' AND email_addr_bean_rel.bean_id='{$Account->id}' AND email_addresses.deleted=0");
							$queryemail = mysqli_fetch_array($QueryLeadEmail);
							if (mysqli_num_rows($QueryLeadEmail)>=1) {
								$bandemail=true;
							}
						}
						if ($bandemail) {
							
							$fecha_lead = strtotime($bean->date_entered);
		            		$fecha_account = strtotime($queryemail['date_entered']);
		            		if ($fecha_lead > $fecha_account) {
		            			$bean->status = 'D';
								$bean->converted = true;
								$bean->account_id = $account->id;
								$bean->account_name = $account->name;
				            }else{
				            	$bean->status = 'I';
								$bean->converted = true;
								$bean->account_id = $account->id;
								$bean->account_name = $account->name;
				            }
							
						}else{
							$queryleadbytel = $GLOBALS['db']->query("SELECT * FROM accounts INNER JOIN accounts_cstm ON accounts.id=accounts_cstm.id_c WHERE (sasa_phone_office_c='{$bean->sasa_phone_home_c}' OR sasa_phone_alternate_c='{$bean->sasa_phone_home_c}' OR sasa_phone_other_c='{$bean->sasa_phone_home_c}') AND accounts.name='{$NombreLead}' AND accounts.deleted=0");

							$leadtel = mysqli_fetch_array($queryleadbytel);
							if ($leadtel['id'] != null || $leadtel['id']!= "") {
								$fecha_lead = strtotime($bean->date_entered);
			            		$fecha_account = strtotime($queryemail['date_entered']);
			            		if ($fecha_lead > $fecha_account) {
			            			$bean->status = 'D';
									$bean->converted = true;
									$bean->account_id = $account->id;
									$bean->account_name = $account->name;
					            }else{
					            	$bean->status = 'I';
									$bean->converted = true;
									$bean->account_id = $account->id;
									$bean->account_name = $account->name;
					            }
							}
						}
					}
				}	
			}			
		}

		
		//Consumir el serivio de SIGRU de LEADS
		if ($bean->modified_by_name != "Integracion1" AND strpos($bean->sasa_phone_home_c, '0000000') === false AND $bean->status != 'A' AND $bean->status != 'B') {
			if (!isset($bean->ignore_updatelead_c) || $bean->ignore_updatelead_c === false)
	        {
	        	$bean->ignore_updatelead_c = true;
	        	

	        	$queryleadcall = $GLOBALS['db']->query("SELECT * FROM calls WHERE calls.status='Held' AND calls.deleted=0 AND calls.parent_type='Leads' AND calls.parent_id='{$bean->id}'");
				$getleadcall = mysqli_fetch_array($queryleadcall);
				if ($getleadcall['id'] != null) {
					$GLOBALS['log']->security("Consumir SIgru Leads");
					//Consumir SIgru Leads
					require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
					$sendsigru = new Send_Data_Sigru();
					$sendsigru->data($bean->id,$bean->module_name);

					//Traer Habeas Data más reciente
					$query_get_hd = $GLOBALS['db']->query("SELECT sasa_habeas_data.* FROM sasa_habeas_data INNER JOIN leads_sasa_habeas_data_1_c ON sasa_habeas_data.id=leads_sasa_habeas_data_1_c.leads_sasa_habeas_data_1sasa_habeas_data_idb INNER JOIN leads ON leads.id=leads_sasa_habeas_data_1_c.leads_sasa_habeas_data_1leads_ida WHERE leads.id='{$bean->id}' AND leads.deleted=0 AND sasa_habeas_data.deleted=0 ORDER BY `sasa_habeas_data`.`date_modified` DESC");
					$get_hd = mysqli_fetch_array($query_get_hd);
					if ($get_hd != null) {
						//Instanciar bean para que ejecute los logic hooks necesario, incluyendo el consumo de HD a SIGRU /custom/modules/SASA_Habeas_Data/SASA_HabeasAftersave.php
						$Habeas = BeanFactory::getBean('SASA_Habeas_Data',$get_hd['id'],array('disable_row_level_security' => true));
						$Habeas->save();
					}
				}
	        }
			
		}
	}
}
?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sasa_vehiculos_leads_1_sasa_vehiculos.php

// created: 2020-05-15 18:35:34
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_leads_1"] = array (
  'name' => 'sasa_vehiculos_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sasa_marcas_sasa_vehiculos_1_sasa_vehiculos.php

// created: 2020-05-15 18:26:27
$dictionary["sasa_vehiculos"]["fields"]["sasa_marcas_sasa_vehiculos_1"] = array (
  'name' => 'sasa_marcas_sasa_vehiculos_1',
  'type' => 'link',
  'relationship' => 'sasa_marcas_sasa_vehiculos_1',
  'source' => 'non-db',
  'module' => 'sasa_Marcas',
  'bean_name' => 'sasa_Marcas',
  'side' => 'right',
  'vname' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'link-type' => 'one',
);
$dictionary["sasa_vehiculos"]["fields"]["sasa_marcas_sasa_vehiculos_1_name"] = array (
  'name' => 'sasa_marcas_sasa_vehiculos_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_MARCAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'link' => 'sasa_marcas_sasa_vehiculos_1',
  'table' => 'sasa_marcas',
  'module' => 'sasa_Marcas',
  'rname' => 'name',
);
$dictionary["sasa_vehiculos"]["fields"]["sasa_marcas_sasa_vehiculos_1sasa_marcas_ida"] = array (
  'name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_VEHICULOS_TITLE_ID',
  'id_name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'link' => 'sasa_marcas_sasa_vehiculos_1',
  'table' => 'sasa_marcas',
  'module' => 'sasa_Marcas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sugarfield_description.php

 // created: 2020-05-16 16:05:43
$dictionary['sasa_vehiculos']['fields']['description']['audited']=false;
$dictionary['sasa_vehiculos']['fields']['description']['massupdate']=false;
$dictionary['sasa_vehiculos']['fields']['description']['comments']='Full text of the note';
$dictionary['sasa_vehiculos']['fields']['description']['duplicate_merge']='enabled';
$dictionary['sasa_vehiculos']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['sasa_vehiculos']['fields']['description']['merge_filter']='disabled';
$dictionary['sasa_vehiculos']['fields']['description']['unified_search']=false;
$dictionary['sasa_vehiculos']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['sasa_vehiculos']['fields']['description']['calculated']=false;
$dictionary['sasa_vehiculos']['fields']['description']['rows']='6';
$dictionary['sasa_vehiculos']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sasa_vehiculos_sasa_unidadnegclienteprospect_1_sasa_vehiculos.php

// created: 2020-06-11 20:27:26
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'SASA_UnidadNegClienteProspect',
  'bean_name' => 'SASA_UnidadNegClienteProspect',
  'vname' => 'LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sasa_vehiculos_cases_1_sasa_vehiculos.php

// created: 2021-08-04 21:19:24
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_cases_1"] = array (
  'name' => 'sasa_vehiculos_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sasa_vehiculos_cases_2_sasa_vehiculos.php

// created: 2021-08-04 21:22:45
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_cases_2"] = array (
  'name' => 'sasa_vehiculos_cases_2',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_2',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sasa_vehiculos_cases_3_sasa_vehiculos.php

// created: 2021-08-04 21:24:18
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_cases_3"] = array (
  'name' => 'sasa_vehiculos_cases_3',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_3',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_3_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sasa_vehiculos_cases_4_sasa_vehiculos.php

// created: 2021-08-04 21:25:11
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_cases_4"] = array (
  'name' => 'sasa_vehiculos_cases_4',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_4',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_4_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_cases_4sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2021-12-27 19:04:48
$dictionary['sasa_vehiculos']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['sasa_vehiculos']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['sasa_vehiculos']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sugarfield_sas_cd_line_vehi_c.php

 // created: 2021-12-27 19:04:48
$dictionary['sasa_vehiculos']['fields']['sas_cd_line_vehi_c']['labelValue']='Código Línea Vehículo';
$dictionary['sasa_vehiculos']['fields']['sas_cd_line_vehi_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_vehiculos']['fields']['sas_cd_line_vehi_c']['enforced']='';
$dictionary['sasa_vehiculos']['fields']['sas_cd_line_vehi_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sugarfield_sasa_urlbrochure_c.php

 // created: 2021-12-27 19:04:48

 
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sugarfield_sasa_estadolinea_c.php

 // created: 2021-12-27 19:04:48

 
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sugarfield_sasa_lineabrochure_c.php

 // created: 2021-12-27 19:04:48

 
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Vardefs/sasa_vehiculos_opportunities_1_sasa_vehiculos.php

// created: 2022-11-23 16:02:16
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_opportunities_1"] = array (
  'name' => 'sasa_vehiculos_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>

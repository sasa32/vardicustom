<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/WirelessLayoutdefs/sasa_vehiculos_leads_1_sasa_vehiculos.php

 // created: 2020-05-15 23:44:20
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_leads_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/WirelessLayoutdefs/sasa_vehiculos_sasa_unidadnegclienteprospect_1_sasa_vehiculos.php

 // created: 2020-06-11 20:27:26
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_sasa_unidadnegclienteprospect_1'] = array (
  'order' => 100,
  'module' => 'SASA_UnidadNegClienteProspect',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/WirelessLayoutdefs/sasa_vehiculos_cases_1_sasa_vehiculos.php

 // created: 2021-08-04 21:19:24
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_VEHICULOS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_cases_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/WirelessLayoutdefs/sasa_vehiculos_cases_2_sasa_vehiculos.php

 // created: 2021-08-04 21:22:45
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_cases_2'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_cases_2',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/WirelessLayoutdefs/sasa_vehiculos_cases_3_sasa_vehiculos.php

 // created: 2021-08-04 21:24:18
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_cases_3'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_VEHICULOS_CASES_3_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_cases_3',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/WirelessLayoutdefs/sasa_vehiculos_cases_4_sasa_vehiculos.php

 // created: 2021-08-04 21:25:11
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_cases_4'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_VEHICULOS_CASES_4_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_cases_4',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/WirelessLayoutdefs/sasa_vehiculos_opportunities_1_sasa_vehiculos.php

 // created: 2022-11-23 16:02:16
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_opportunities_1'] = array (
  'order' => 100,
  'module' => 'Opportunities',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_opportunities_1',
);

?>

<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-08-04 21:19:24
$viewdefs['sasa_vehiculos']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_VEHICULOS_CASES_1_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_vehiculos_cases_1',
  ),
);

// created: 2021-08-04 21:22:45
$viewdefs['sasa_vehiculos']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_vehiculos_cases_2',
  ),
);

// created: 2021-08-04 21:24:18
$viewdefs['sasa_vehiculos']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_VEHICULOS_CASES_3_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_vehiculos_cases_3',
  ),
);

// created: 2021-08-04 21:25:11
$viewdefs['sasa_vehiculos']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_VEHICULOS_CASES_4_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_vehiculos_cases_4',
  ),
);

// created: 2020-05-15 23:44:20
$viewdefs['sasa_vehiculos']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_LEADS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_vehiculos_leads_1',
  ),
);

// created: 2022-11-23 16:02:16
$viewdefs['sasa_vehiculos']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_vehiculos_opportunities_1',
  ),
);

// created: 2020-06-11 20:27:26
$viewdefs['sasa_vehiculos']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'context' => 
  array (
    'link' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
  ),
);
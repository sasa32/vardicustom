<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.gvvehiculosCustomFieldsv001-2020-05-15.php

$mod_strings['LBL_SAS_CD_LINE_VEHI_C'] = 'Código Linea Vehículo';
 $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
 $mod_strings['LBL_NAME'] = 'Nombre de Línea';
 $mod_strings['LBL_DESCRIPTION'] = 'Observaciones';
 $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
 $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_vehiculos_leads_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_vehiculos_leads_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_VEHICULOS_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE'] = 'Leads';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_VEHICULOS_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE'] = 'Leads';


?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.gvvehiculosCustomFieldsV002-2021-08-04.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.gvvehiculosCustomFieldsV002-2021-08-04.php

$mod_strings['LBL_SAS_CD_LINE_VEHI_C'] = 'Código Linea Vehículo';
 $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
 $mod_strings['LBL_NAME'] = 'Nombre de Línea';
 $mod_strings['LBL_DESCRIPTION'] = 'Descripción';
 $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
 $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URLBROCHURE_C'] = 'Url Brochure';
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/temp.php

$mod_strings['LBL_SAS_CD_LINE_VEHI_C'] = 'Código Linea Vehículo';
 $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
 $mod_strings['LBL_NAME'] = 'Nombre de Línea';
 $mod_strings['LBL_DESCRIPTION'] = 'Descripción';
 $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
 $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URLBROCHURE_C'] = 'Url Brochure';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_marcas_sasa_vehiculos_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_MARCAS_TITLE'] = 'Marcas';
$mod_strings['LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_VEHICULOS_TITLE'] = 'Marcas';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_vehiculos_sasa_unidadnegclienteprospect_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE'] = 'Unidades de Neg. por Clientes y Prospectos';
$mod_strings['LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_VEHICULOS_TITLE'] = 'Unidades de Neg. por Clientes y Prospectos';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_vehiculos_cases_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_CASES_1_FROM_CASES_TITLE'] = 'Casos';
$mod_strings['LBL_SASA_VEHICULOS_CASES_1_FROM_SASA_VEHICULOS_TITLE'] = 'Casos';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_vehiculos_cases_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_CASES_2_FROM_CASES_TITLE'] = 'Casos';
$mod_strings['LBL_SASA_VEHICULOS_CASES_2_FROM_SASA_VEHICULOS_TITLE'] = 'Casos';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_vehiculos_cases_3.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_CASES_3_FROM_CASES_TITLE'] = 'Casos';
$mod_strings['LBL_SASA_VEHICULOS_CASES_3_FROM_SASA_VEHICULOS_TITLE'] = 'Casos';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_vehiculos_cases_4.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_VEHICULOS_CASES_4_FROM_CASES_TITLE'] = 'Casos';
$mod_strings['LBL_SASA_VEHICULOS_CASES_4_FROM_SASA_VEHICULOS_TITLE'] = 'Casos';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.gvvehiculosCustomFieldsV003-2021-09-30.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.gvvehiculosCustomFieldsV003-2021-09-30.php

$mod_strings['LBL_SAS_CD_LINE_VEHI_C'] = 'Código Linea Vehículo';
 $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
 $mod_strings['LBL_NAME'] = 'Nombre de Línea';
 $mod_strings['LBL_DESCRIPTION'] = 'Descripción';
 $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
 $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URLBROCHURE_C'] = 'Url Brochure';
$mod_strings['LBL_SASA_ESTADOLINEA_C'] = 'Estado de línea';
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/temp.php

$mod_strings['LBL_SAS_CD_LINE_VEHI_C'] = 'Código Linea Vehículo';
 $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
 $mod_strings['LBL_NAME'] = 'Nombre de Línea';
 $mod_strings['LBL_DESCRIPTION'] = 'Descripción';
 $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
 $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URLBROCHURE_C'] = 'Url Brochure';
$mod_strings['LBL_SASA_ESTADOLINEA_C'] = 'Estado de línea';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.gvvehiculosCustomFieldsv004-2021-12-27.php

$mod_strings['LBL_SAS_CD_LINE_VEHI_C'] = 'Código Linea Vehículo';
 $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
 $mod_strings['LBL_NAME'] = 'Nombre de Línea';
 $mod_strings['LBL_DESCRIPTION'] = 'Descripción';
 $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
 $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URLBROCHURE_C'] = 'Url Brochure';
$mod_strings['LBL_SASA_ESTADOLINEA_C'] = 'Estado de línea';
$mod_strings['LBL_SASA_LINEABROCHURE_C'] = 'Línea Brochure';
?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_vehiculos_opportunities_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE'] = 'Cotizaciones';
$mod_strings['LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_SASA_VEHICULOS_TITLE'] = 'Cotizaciones';

?>
<?php
// Merged from custom/Extension/modules/sasa_vehiculos/Ext/Language/en_us.customsasa_vehiculos_opportunities_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_VEHICULOS_OPPORTUNITIES_2_FROM_OPPORTUNITIES_TITLE'] = 'Cotizaciones';
$mod_strings['LBL_SASA_VEHICULOS_OPPORTUNITIES_2_FROM_SASA_VEHICULOS_TITLE'] = 'Cotizaciones';

?>

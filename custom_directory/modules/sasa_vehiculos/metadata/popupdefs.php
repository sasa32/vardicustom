<?php
$popupMeta = array (
    'moduleMain' => 'sasa_vehiculos',
    'varName' => 'sasa_vehiculos',
    'orderBy' => 'sasa_vehiculos.name',
    'whereClauses' => array (
  'name' => 'sasa_vehiculos.name',
  'sas_cd_line_vehi_c' => 'sasa_vehiculos_cstm.sas_cd_line_vehi_c',
  'sasa_marcas_sasa_vehiculos_1_name' => 'sasa_vehiculos.sasa_marcas_sasa_vehiculos_1_name',
  'date_entered' => 'sasa_vehiculos.date_entered',
  'date_modified' => 'sasa_vehiculos.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sas_cd_line_vehi_c',
  5 => 'sasa_marcas_sasa_vehiculos_1_name',
  6 => 'date_entered',
  7 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'sas_cd_line_vehi_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SAS_CD_LINE_VEHI_C',
    'width' => 10,
    'name' => 'sas_cd_line_vehi_c',
  ),
  'sasa_marcas_sasa_vehiculos_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_MARCAS_TITLE',
    'id' => 'SASA_MARCAS_SASA_VEHICULOS_1SASA_MARCAS_IDA',
    'width' => 10,
    'name' => 'sasa_marcas_sasa_vehiculos_1_name',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SAS_CD_LINE_VEHI_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SAS_CD_LINE_VEHI_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_MARCAS_SASA_VEHICULOS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_MARCAS_TITLE',
    'id' => 'SASA_MARCAS_SASA_VEHICULOS_1SASA_MARCAS_IDA',
    'width' => 10,
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
),
);

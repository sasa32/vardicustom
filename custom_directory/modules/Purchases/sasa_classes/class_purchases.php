<?php
class class_purchases
{

	private $argumentos;
	private $Bean = null;

	function __construct($args)
	{	
		$this->argumentos = $args;
		//$GLOBALS['log']->security("Argumento #: " . print_r($this->argumentos, true));
	
	}

	public function miFuncion(){

		return "COMPRAS mi funcion";
	}

	public function createPurchases(){

		$response;

		$Bean = BeanFactory::newBean("Purchases");

		if(!empty($this->argumentos['sasa_placa_c']))
			$placa = "-" . $this->argumentos['sasa_placa_c'];

		$Bean->name = $this->argumentos['sasa_numero_pedido_c'] . $placa;
		$Bean->account_id = $this->argumentos['account_id'];;
		
		$response = $this->saveModules($Bean);

		return $response;
	}

	public function updatePurchases($id_pedido){

		$Bean = BeanFactory::getBean("PurchasedLineItems", $id_pedido);
		
		$id_compra = $Bean->purchase_id;
		
		$GLOBALS['log']->security("Update COMPRA::" . $id_compra);

		$Bean = BeanFactory::retrieveBean("Purchases", $id_compra);
		
		if(!empty($this->argumentos['sasa_placa_c']))
		$placa = "-" . $this->argumentos['sasa_placa_c'];

		$Bean->name = $this->argumentos['sasa_numero_pedido_c'] . $placa;
		//$Bean->save();
		$response = $this->saveModules($Bean);

		return $response;
		//$GLOBALS['log']->security("Update Cotización::" . $response);
	}
	
	private function saveModules($Bean){
		
		//Save
		$Bean->save();

		//Retrieve the bean id
		$record_id = $Bean->id;

		return $record_id;
	}

}

?>
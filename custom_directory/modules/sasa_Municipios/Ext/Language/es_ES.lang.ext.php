<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_departamentos_sasa_municipios_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_departamentos_sasa_municipios_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Departamentos';
$mod_strings['LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Departamentos';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Departamentos';
$mod_strings['LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Departamentos';


?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.gvciudadcustomfields--v001202006.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.gvciudadcustomfields--v001202006.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.gvciudadcustomfields--v001202006.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Crear Ciudad';
$mod_strings['LNK_LIST'] = 'Vista Ciudades';
$mod_strings['LBL_MODULE_NAME'] = 'Ciudades';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Ciudad';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Ciudad';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Ciudad vCard';
$mod_strings['LNK_IMPORT_SASA_MUNICIPIOS'] = 'Import Ciudades';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Ciudads Lista';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Buscar Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_SUBPANEL_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_INDICATIVO_C'] = 'Indicativo';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Crear Ciudad';
$mod_strings['LNK_LIST'] = 'Vista Ciudades';
$mod_strings['LBL_MODULE_NAME'] = 'Ciudades';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Ciudad';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Ciudad';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Ciudad vCard';
$mod_strings['LNK_IMPORT_SASA_MUNICIPIOS'] = 'Import Ciudades';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Ciudads Lista';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Buscar Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_SUBPANEL_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_INDICATIVO_C'] = 'Indicativo';


?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Crear Ciudad';
$mod_strings['LNK_LIST'] = 'Vista Ciudades';
$mod_strings['LBL_MODULE_NAME'] = 'Ciudades';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Ciudad';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Ciudad';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Ciudad vCard';
$mod_strings['LNK_IMPORT_SASA_MUNICIPIOS'] = 'Import Ciudades';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Ciudads Lista';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Buscar Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_SUBPANEL_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_INDICATIVO_C'] = 'Indicativo';


?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Crear Ciudad';
$mod_strings['LNK_LIST'] = 'Vista Ciudades';
$mod_strings['LBL_MODULE_NAME'] = 'Ciudades';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Ciudad';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Ciudad';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Ciudad vCard';
$mod_strings['LNK_IMPORT_SASA_MUNICIPIOS'] = 'Import Ciudades';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Ciudads Lista';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Buscar Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_SUBPANEL_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'CyP Ciudad Tel Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'CyP Ciudad Tel Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'CyP Ciudad Tel Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'CyP Ciudad Tel Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'CO Ciudad Tel Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'CO Ciudad Tel Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'CO Ciudad Tel Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'CO Ciudad Tel Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'CO Ciudad Tel Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'CO Ciudad Tel Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Contactos Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Clientes y Prospectos Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Clientes y Prospectos Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Contactos Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Contactos Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_2_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_MUNICIPIOS_FOCUS_DRAWER_DASHBOARD'] = 'Ciudades Panel de enfoque';
$mod_strings['LBL_SASA_MUNICIPIOS_RECORD_DASHBOARD'] = 'Ciudades Cuadro de mando del registro';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Leads';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_accounts_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Clientes y Prospectos';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_contacts_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_leads_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_2_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_MUNICIPIOS_LEADS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Leads';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_accounts_3.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Clientes y Prospectos';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_accounts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Clientes y Prospectos';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_contacts_3.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_contacts_4.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_contacts_5.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_contacts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_accounts_5.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_contacts_6.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_accounts_6.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Language/es_ES.customsasa_municipios_accounts_4.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Clientes y Prospectos';

?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_accounts_1_sasa_Municipios.php

 // created: 2020-05-15 15:44:33
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_accounts_1'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_accounts_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_contacts_1_sasa_Municipios.php

 // created: 2020-05-15 15:44:56
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_contacts_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_leads_1_sasa_Municipios.php

 // created: 2020-05-15 15:45:19
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_leads_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_accounts_2_sasa_Municipios.php

 // created: 2020-05-15 16:08:40
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_accounts_2'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_accounts_2',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_contacts_2_sasa_Municipios.php

 // created: 2020-05-15 16:09:12
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_contacts_2'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_contacts_2',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_accounts_3_sasa_Municipios.php

 // created: 2020-05-15 16:11:28
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_accounts_3'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_accounts_3',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_accounts_4_sasa_Municipios.php

 // created: 2020-05-15 16:11:54
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_accounts_4'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_accounts_4',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_contacts_3_sasa_Municipios.php

 // created: 2020-05-15 16:12:58
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_contacts_3'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_contacts_3',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_contacts_4_sasa_Municipios.php

 // created: 2020-05-15 16:13:32
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_contacts_4'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_contacts_4',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_contacts_5_sasa_Municipios.php

 // created: 2020-05-15 16:14:43
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_contacts_5'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_contacts_5',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_accounts_5_sasa_Municipios.php

 // created: 2020-08-03 14:03:29
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_accounts_5'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_accounts_5',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_contacts_6_sasa_Municipios.php

 // created: 2020-08-03 14:04:12
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_contacts_6'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_contacts_6',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/WirelessLayoutdefs/sasa_municipios_accounts_6_sasa_Municipios.php

 // created: 2020-08-03 14:04:52
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_accounts_6'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_accounts_6',
);

?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_departamentos_sasa_municipios_1_sasa_Municipios.php

// created: 2020-05-15 15:42:24
$dictionary["sasa_Municipios"]["fields"]["sasa_departamentos_sasa_municipios_1"] = array (
  'name' => 'sasa_departamentos_sasa_municipios_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_sasa_municipios_1',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'side' => 'right',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida',
  'link-type' => 'one',
);
$dictionary["sasa_Municipios"]["fields"]["sasa_departamentos_sasa_municipios_1_name"] = array (
  'name' => 'sasa_departamentos_sasa_municipios_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida',
  'link' => 'sasa_departamentos_sasa_municipios_1',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'name',
);
$dictionary["sasa_Municipios"]["fields"]["sasa_departamentos_sasa_municipios_1sasa_departamentos_ida"] = array (
  'name' => 'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_MUNICIPIOS_TITLE_ID',
  'id_name' => 'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida',
  'link' => 'sasa_departamentos_sasa_municipios_1',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_accounts_1_sasa_Municipios.php

// created: 2020-05-15 15:44:33
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_1"] = array (
  'name' => 'sasa_municipios_accounts_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_1sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_contacts_1_sasa_Municipios.php

// created: 2020-05-15 15:44:56
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_1"] = array (
  'name' => 'sasa_municipios_contacts_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_1sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_leads_1_sasa_Municipios.php

// created: 2020-05-15 15:45:19
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_leads_1"] = array (
  'name' => 'sasa_municipios_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_accounts_2_sasa_Municipios.php

// created: 2020-05-15 16:08:40
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_2"] = array (
  'name' => 'sasa_municipios_accounts_2',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_2',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_2sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_contacts_2_sasa_Municipios.php

// created: 2020-05-15 16:09:12
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_2"] = array (
  'name' => 'sasa_municipios_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_accounts_3_sasa_Municipios.php

// created: 2020-05-15 16:11:28
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_3"] = array (
  'name' => 'sasa_municipios_accounts_3',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_3',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_3sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_accounts_4_sasa_Municipios.php

// created: 2020-05-15 16:11:54
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_4"] = array (
  'name' => 'sasa_municipios_accounts_4',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_4',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_4sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_contacts_3_sasa_Municipios.php

// created: 2020-05-15 16:12:58
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_3"] = array (
  'name' => 'sasa_municipios_contacts_3',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_3',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_3sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_contacts_4_sasa_Municipios.php

// created: 2020-05-15 16:13:32
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_4"] = array (
  'name' => 'sasa_municipios_contacts_4',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_4',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_4sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_contacts_5_sasa_Municipios.php

// created: 2020-05-15 16:14:43
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_5"] = array (
  'name' => 'sasa_municipios_contacts_5',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_5',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_5sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sugarfield_sasa_indicativo_c.php

 // created: 2020-06-17 23:44:01

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_accounts_5_sasa_Municipios.php

// created: 2020-08-03 14:03:29
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_5"] = array (
  'name' => 'sasa_municipios_accounts_5',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_5',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_contacts_6_sasa_Municipios.php

// created: 2020-08-03 14:04:12
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_6"] = array (
  'name' => 'sasa_municipios_contacts_6',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_6',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Municipios/Ext/Vardefs/sasa_municipios_accounts_6_sasa_Municipios.php

// created: 2020-08-03 14:04:52
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_6"] = array (
  'name' => 'sasa_municipios_accounts_6',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_6',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_6sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>

<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-05-15 15:44:33
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_accounts_1',
  ),
);

// created: 2020-05-15 16:08:40
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_accounts_2',
  ),
);

// created: 2020-05-15 16:11:28
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_accounts_3',
  ),
);

// created: 2020-05-15 16:11:54
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_accounts_4',
  ),
);

// created: 2020-08-03 14:03:29
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_accounts_5',
  ),
);

// created: 2020-08-03 14:04:52
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_accounts_6',
  ),
);

// created: 2020-05-15 15:44:56
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_contacts_1',
  ),
);

// created: 2020-05-15 16:09:12
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_contacts_2',
  ),
);

// created: 2020-05-15 16:12:58
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_contacts_3',
  ),
);

// created: 2020-05-15 16:13:32
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_contacts_4',
  ),
);

// created: 2020-05-15 16:14:43
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_contacts_5',
  ),
);

// created: 2020-08-03 14:04:12
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_contacts_6',
  ),
);

// created: 2020-05-15 15:45:19
$viewdefs['sasa_Municipios']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_municipios_leads_1',
  ),
);
<?php
$popupMeta = array (
    'moduleMain' => 'sasa_Municipios',
    'varName' => 'sasa_Municipios',
    'orderBy' => 'sasa_municipios.name',
    'whereClauses' => array (
  'name' => 'sasa_municipios.name',
  'sasa_codigomunicipio_c' => 'sasa_municipios.sasa_codigomunicipio_c',
  'sasa_estado_c' => 'sasa_municipios.sasa_estado_c',
  'date_entered' => 'sasa_municipios.date_entered',
  'sasa_departamentos_sasa_municipios_1_name' => 'sasa_municipios.sasa_departamentos_sasa_municipios_1_name',
  'sasa_indicativo_c' => 'sasa_municipios_cstm.sasa_indicativo_c',
  'description' => 'sasa_municipios.description',
  'date_modified' => 'sasa_municipios.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sasa_codigomunicipio_c',
  6 => 'sasa_estado_c',
  7 => 'date_entered',
  8 => 'sasa_departamentos_sasa_municipios_1_name',
  9 => 'sasa_indicativo_c',
  10 => 'description',
  11 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'name',
    'label' => 'LBL_NAME',
    'width' => '10',
    'name' => 'name',
  ),
  'sasa_codigomunicipio_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_CODIGOMUNICIPIO_C',
    'width' => '10',
    'name' => 'sasa_codigomunicipio_c',
  ),
  'sasa_estado_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_ESTADO_C',
    'width' => '10',
    'name' => 'sasa_estado_c',
  ),
  'sasa_departamentos_sasa_municipios_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
    'id' => 'SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1SASA_DEPARTAMENTOS_IDA',
    'width' => 10,
    'name' => 'sasa_departamentos_sasa_municipios_1_name',
  ),
  'sasa_indicativo_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_INDICATIVO_C',
    'width' => 10,
    'name' => 'sasa_indicativo_c',
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'name' => 'description',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10',
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'name',
    'label' => 'LBL_NAME',
    'width' => 10,
    'default' => true,
    'name' => 'name',
  ),
  'SASA_CODIGOMUNICIPIO_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_SASA_CODIGOMUNICIPIO_C',
    'width' => 10,
    'name' => 'sasa_codigomunicipio_c',
  ),
  'SASA_ESTADO_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_ESTADO_C',
    'width' => 10,
    'name' => 'sasa_estado_c',
  ),
  'SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
    'id' => 'SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1SASA_DEPARTAMENTOS_IDA',
    'width' => 10,
    'default' => true,
  ),
  'SASA_INDICATIVO_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_INDICATIVO_C',
    'width' => 10,
    'default' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
    'name' => 'date_modified',
  ),
),
);

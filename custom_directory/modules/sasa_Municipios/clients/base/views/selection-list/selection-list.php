<?php
$module_name = 'sasa_Municipios';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_codigomunicipio_c',
                'label' => 'LBL_SASA_CODIGOMUNICIPIO_C',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_estado_c',
                'label' => 'LBL_SASA_ESTADO_C',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sasa_departamentos_sasa_municipios_1_name',
                'label' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
                'enabled' => true,
                'id' => 'SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1SASA_DEPARTAMENTOS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_indicativo_c',
                'label' => 'LBL_SASA_INDICATIVO_C',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              7 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => true,
                'name' => 'date_modified',
                'readonly' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);

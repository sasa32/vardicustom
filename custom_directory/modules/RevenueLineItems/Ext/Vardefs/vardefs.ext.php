<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/rli_vardef.ext.php

$dictionary['RevenueLineItem']['importable'] = true;
$dictionary['RevenueLineItem']['unified_search'] = true;
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/denorm_account_name.php


// 'account_name'
$dictionary['RevenueLineItem']['fields']['account_name']['is_denormalized'] = true;
$dictionary['RevenueLineItem']['fields']['account_name']['denormalized_field_name'] = 'denorm_account_name';

// 'denorm_account_name'
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['name'] = 'denorm_account_name';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['type'] = 'varchar';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['dbType'] = 'varchar';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['vname'] = 'LBL_ACCOUNT_NAME';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['len'] = '150';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['comment'] = 'Name of the Company';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['unified_search'] = true;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['full_text_search'] = array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['audited'] = false;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['required'] = false;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['importable'] = 'false';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['duplicate_on_record_copy'] = 'always';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['merge_filter'] = 'disabled';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['massupdate'] = false;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['comments'] = 'Name of the Company';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['duplicate_merge'] = 'disabled';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['duplicate_merge_dom_value'] = 0;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['calculated'] = '1';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['formula'] = 'concat($sasa_nombres_c," ",$sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['enforced'] = true;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['denorm_from_module'] = 'Accounts';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['studio'] = false;

?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_deleted.php

 // created: 2023-02-07 21:46:52
$dictionary['RevenueLineItem']['fields']['deleted']['default']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['audited']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['comments']='Record deletion indicator';
$dictionary['RevenueLineItem']['fields']['deleted']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['deleted']['duplicate_merge_dom_value']=1;
$dictionary['RevenueLineItem']['fields']['deleted']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['deleted']['unified_search']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_product_template_id.php

 // created: 2023-02-07 21:47:37
$dictionary['RevenueLineItem']['fields']['product_template_id']['name']='product_template_id';
$dictionary['RevenueLineItem']['fields']['product_template_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['product_template_id']['vname']='LBL_PRODUCT_TEMPLATE_ID';
$dictionary['RevenueLineItem']['fields']['product_template_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['product_template_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['product_template_id']['comment']='Product (in Admin Products) from which this product is derived (in user Products)';
$dictionary['RevenueLineItem']['fields']['product_template_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['product_template_id']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_product_template_name.php

 // created: 2023-02-07 21:47:37
$dictionary['RevenueLineItem']['fields']['product_template_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['product_template_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['product_template_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['product_template_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['product_template_name']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['product_template_name']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['product_template_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['product_template_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['product_template_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_subtotal.php

 // created: 2023-02-07 21:48:36
$dictionary['RevenueLineItem']['fields']['subtotal']['len']=26;
$dictionary['RevenueLineItem']['fields']['subtotal']['audited']=false;
$dictionary['RevenueLineItem']['fields']['subtotal']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['subtotal']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['subtotal']['comments']='Stores the total of the line item before any discounts are applied, taking proration into consideration';
$dictionary['RevenueLineItem']['fields']['subtotal']['importable']='false';
$dictionary['RevenueLineItem']['fields']['subtotal']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['subtotal']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['subtotal']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['subtotal']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['subtotal']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['subtotal']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['subtotal']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_total_amount.php

 // created: 2023-02-07 21:49:10
$dictionary['RevenueLineItem']['fields']['total_amount']['audited']=false;
$dictionary['RevenueLineItem']['fields']['total_amount']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['total_amount']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['total_amount']['importable']='false';
$dictionary['RevenueLineItem']['fields']['total_amount']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['total_amount']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['total_amount']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['total_amount']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['total_amount']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['total_amount']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_cost_price.php

 // created: 2023-02-15 16:26:33
$dictionary['RevenueLineItem']['fields']['cost_price']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['cost_price']['len']=26;
$dictionary['RevenueLineItem']['fields']['cost_price']['audited']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['comments']='Product cost ("Cost" in Quote)';
$dictionary['RevenueLineItem']['fields']['cost_price']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['cost_price']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['cost_price']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['cost_price']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['cost_price']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_likely_case.php

 // created: 2023-02-15 16:29:52
$dictionary['RevenueLineItem']['fields']['likely_case']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['likely_case']['len']=26;
$dictionary['RevenueLineItem']['fields']['likely_case']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['likely_case']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['likely_case']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['likely_case']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['likely_case']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['required']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['audited']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['formula']='';
$dictionary['RevenueLineItem']['fields']['likely_case']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_sales_stage.php

 // created: 2023-02-15 16:32:28
$dictionary['RevenueLineItem']['fields']['sales_stage']['len']=100;
$dictionary['RevenueLineItem']['fields']['sales_stage']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['RevenueLineItem']['fields']['sales_stage']['importable']='true';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['sales_stage']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['visibility_grid']=array (
);
$dictionary['RevenueLineItem']['fields']['sales_stage']['required']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['audited']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['reportable']=true;
$dictionary['RevenueLineItem']['fields']['sales_stage']['default']='';
$dictionary['RevenueLineItem']['fields']['sales_stage']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_book_value.php

 // created: 2023-02-15 16:35:51
$dictionary['RevenueLineItem']['fields']['book_value']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['book_value']['len']=26;
$dictionary['RevenueLineItem']['fields']['book_value']['audited']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['comments']='Book value of sales item in use';
$dictionary['RevenueLineItem']['fields']['book_value']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['book_value']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['book_value']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['book_value']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_list_price.php

 // created: 2023-02-15 16:36:38
$dictionary['RevenueLineItem']['fields']['list_price']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['list_price']['len']=26;
$dictionary['RevenueLineItem']['fields']['list_price']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['comments']='List price of sales item';
$dictionary['RevenueLineItem']['fields']['list_price']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['list_price']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['list_price']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['list_price']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['list_price']['audited']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_quantity.php

 // created: 2023-02-15 16:38:07
$dictionary['RevenueLineItem']['fields']['quantity']['default']='';
$dictionary['RevenueLineItem']['fields']['quantity']['len']='12';
$dictionary['RevenueLineItem']['fields']['quantity']['audited']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['comments']='Quantity in use';
$dictionary['RevenueLineItem']['fields']['quantity']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['quantity']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['quantity']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['quantity']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['readonly']=true;
$dictionary['RevenueLineItem']['fields']['quantity']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_renewable.php

 // created: 2023-02-15 16:39:27
$dictionary['RevenueLineItem']['fields']['renewable']['default']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['audited']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['comments']='Indicates whether the sales item is renewable (e.g. a service)';
$dictionary['RevenueLineItem']['fields']['renewable']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['renewable']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['renewable']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['renewable']['unified_search']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_service.php

 // created: 2023-02-15 16:40:25
$dictionary['RevenueLineItem']['fields']['service']['default']=false;
$dictionary['RevenueLineItem']['fields']['service']['audited']=false;
$dictionary['RevenueLineItem']['fields']['service']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['service']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['service']['comments']='Indicates whether the sales item is a service or a product';
$dictionary['RevenueLineItem']['fields']['service']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['service']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['service']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['service']['unified_search']=false;
$dictionary['RevenueLineItem']['fields']['service']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_service_start_date.php

 // created: 2023-02-15 16:43:01
$dictionary['RevenueLineItem']['fields']['service_start_date']['audited']=false;
$dictionary['RevenueLineItem']['fields']['service_start_date']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['service_start_date']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['service_start_date']['comments']='Start date of the service';
$dictionary['RevenueLineItem']['fields']['service_start_date']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['service_start_date']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['service_start_date']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['service_start_date']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['service_start_date']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_manufacturer_id.php

 // created: 2023-02-15 16:57:31
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['name']='manufacturer_id';
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['vname']='LBL_MANUFACTURER';
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['comment']='Manufacturer of product';
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_manufacturer_name.php

 // created: 2023-02-15 16:57:31
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['related_fields']=array (
  0 => 'manufacturer_id',
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_category_id.php

 // created: 2023-02-15 16:58:38
$dictionary['RevenueLineItem']['fields']['category_id']['name']='category_id';
$dictionary['RevenueLineItem']['fields']['category_id']['vname']='LBL_CATEGORY_ID';
$dictionary['RevenueLineItem']['fields']['category_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['category_id']['group']='category_name';
$dictionary['RevenueLineItem']['fields']['category_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['category_id']['reportable']=true;
$dictionary['RevenueLineItem']['fields']['category_id']['comment']='Product category';
$dictionary['RevenueLineItem']['fields']['category_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['category_id']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_category_name.php

 // created: 2023-02-15 16:58:38
$dictionary['RevenueLineItem']['fields']['category_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['category_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['category_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['category_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['category_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['category_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['category_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['category_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['category_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['category_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_mft_part_num.php

 // created: 2023-02-15 16:59:23
$dictionary['RevenueLineItem']['fields']['mft_part_num']['audited']=false;
$dictionary['RevenueLineItem']['fields']['mft_part_num']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['mft_part_num']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['mft_part_num']['comments']='Manufacturer part number';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['importable']='false';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['mft_part_num']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['mft_part_num']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_discount_price.php

 // created: 2023-02-15 17:01:30
$dictionary['RevenueLineItem']['fields']['discount_price']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['discount_price']['len']=26;
$dictionary['RevenueLineItem']['fields']['discount_price']['audited']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['comments']='Discounted price ("Unit Price" in Quote)';
$dictionary['RevenueLineItem']['fields']['discount_price']['importable']='false';
$dictionary['RevenueLineItem']['fields']['discount_price']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_price']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['discount_price']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_price']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_price']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_discount_rate_percent.php

 // created: 2023-02-15 17:02:29
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['audited']=false;
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['importable']='false';
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_discount_amount_usdollar.php

 // created: 2023-02-15 17:03:43
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['precision']=8;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_deal_calc.php

 // created: 2023-02-15 17:04:29
$dictionary['RevenueLineItem']['fields']['deal_calc']['len']=26;
$dictionary['RevenueLineItem']['fields']['deal_calc']['audited']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc']['comments']='deal_calc';
$dictionary['RevenueLineItem']['fields']['deal_calc']['importable']='false';
$dictionary['RevenueLineItem']['fields']['deal_calc']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['deal_calc']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['deal_calc']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['deal_calc']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['deal_calc']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['deal_calc']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_deal_calc_usdollar.php

 // created: 2023-02-15 17:05:10
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['comments']='deal_calc_usdollar';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_cost_usdollar.php

 // created: 2023-02-15 17:06:10
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['comments']='Cost expressed in USD';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_discount_usdollar.php

 // created: 2023-02-15 17:07:00
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['comments']='Discount price expressed in USD';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_list_usdollar.php

 // created: 2023-02-15 17:07:32
$dictionary['RevenueLineItem']['fields']['list_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['comments']='List price expressed in USD';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_status.php

 // created: 2023-02-15 17:08:57
$dictionary['RevenueLineItem']['fields']['status']['audited']=false;
$dictionary['RevenueLineItem']['fields']['status']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['status']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['status']['comments']='Product status (ex: Quoted, Ordered, Shipped)';
$dictionary['RevenueLineItem']['fields']['status']['importable']='false';
$dictionary['RevenueLineItem']['fields']['status']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['status']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['status']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['status']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['status']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['status']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['status']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_book_value_usdollar.php

 // created: 2023-02-15 17:09:22
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['comments']='Book value expressed in USD';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_generate_purchase.php

 // created: 2023-02-15 17:10:08
$dictionary['RevenueLineItem']['fields']['generate_purchase']['len']=100;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['audited']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['comments']='Generate Purchase Options (ex: Yes, No, Completed)';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['importable']='false';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_purchasedlineitem_id.php

 // created: 2023-02-15 17:11:35
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['name']='purchasedlineitem_id';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['vname']='LBL_PLI_ID';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['comment']='If PLI was created from this RLI, this is that PLIs ID';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_purchasedlineitem_name.php

 // created: 2023-02-15 17:11:35
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['comments']='PLI Name';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_best_case.php

 // created: 2023-02-15 17:12:51
$dictionary['RevenueLineItem']['fields']['best_case']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['best_case']['len']=26;
$dictionary['RevenueLineItem']['fields']['best_case']['audited']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['importable']='false';
$dictionary['RevenueLineItem']['fields']['best_case']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['best_case']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['best_case']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['best_case']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['best_case']['enforced']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['best_case']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_worst_case.php

 // created: 2023-02-15 17:13:56
$dictionary['RevenueLineItem']['fields']['worst_case']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['worst_case']['len']=26;
$dictionary['RevenueLineItem']['fields']['worst_case']['audited']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['importable']='false';
$dictionary['RevenueLineItem']['fields']['worst_case']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['worst_case']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['worst_case']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['worst_case']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['worst_case']['enforced']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['worst_case']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_closed.php

 // created: 2023-02-15 17:15:11
$dictionary['RevenueLineItem']['fields']['date_closed']['required']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['audited']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['options']='';
$dictionary['RevenueLineItem']['fields']['date_closed']['comments']='Expected or actual date the product (for opportunity) will close';
$dictionary['RevenueLineItem']['fields']['date_closed']['importable']='false';
$dictionary['RevenueLineItem']['fields']['date_closed']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['date_closed']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['date_closed']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_closed']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['full_text_search']=array (
);
$dictionary['RevenueLineItem']['fields']['date_closed']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['related_fields']=array (
);
$dictionary['RevenueLineItem']['fields']['date_closed']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_next_step.php

 // created: 2023-02-15 17:16:06
$dictionary['RevenueLineItem']['fields']['next_step']['audited']=false;
$dictionary['RevenueLineItem']['fields']['next_step']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['next_step']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['next_step']['comments']='The next step in the sales process';
$dictionary['RevenueLineItem']['fields']['next_step']['importable']='false';
$dictionary['RevenueLineItem']['fields']['next_step']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['next_step']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['next_step']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['next_step']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['next_step']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.49',
  'searchable' => true,
);
$dictionary['RevenueLineItem']['fields']['next_step']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_commit_stage.php

 // created: 2023-02-15 17:16:54
$dictionary['RevenueLineItem']['fields']['commit_stage']['len']=100;
$dictionary['RevenueLineItem']['fields']['commit_stage']['audited']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['options']='';
$dictionary['RevenueLineItem']['fields']['commit_stage']['comments']='Forecast commit category: Include, Likely, Omit etc.';
$dictionary['RevenueLineItem']['fields']['commit_stage']['importable']='false';
$dictionary['RevenueLineItem']['fields']['commit_stage']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['commit_stage']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['commit_stage']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['commit_stage']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['formula']='';
$dictionary['RevenueLineItem']['fields']['commit_stage']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['related_fields']=array (
);
$dictionary['RevenueLineItem']['fields']['commit_stage']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_probability.php

 // created: 2023-02-15 17:17:30
$dictionary['RevenueLineItem']['fields']['probability']['len']='11';
$dictionary['RevenueLineItem']['fields']['probability']['audited']=false;
$dictionary['RevenueLineItem']['fields']['probability']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['probability']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['probability']['comments']='The probability of closure';
$dictionary['RevenueLineItem']['fields']['probability']['importable']='false';
$dictionary['RevenueLineItem']['fields']['probability']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['probability']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['probability']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['probability']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['probability']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['probability']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['probability']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['probability']['min']=0;
$dictionary['RevenueLineItem']['fields']['probability']['max']=100;
$dictionary['RevenueLineItem']['fields']['probability']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_lead_source.php

 // created: 2023-02-15 17:18:30
$dictionary['RevenueLineItem']['fields']['lead_source']['len']=100;
$dictionary['RevenueLineItem']['fields']['lead_source']['audited']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['comments']='Source of the product';
$dictionary['RevenueLineItem']['fields']['lead_source']['importable']='false';
$dictionary['RevenueLineItem']['fields']['lead_source']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['lead_source']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_campaign_id.php

 // created: 2023-02-15 17:19:30
$dictionary['RevenueLineItem']['fields']['campaign_id']['name']='campaign_id';
$dictionary['RevenueLineItem']['fields']['campaign_id']['comment']='Campaign that generated lead';
$dictionary['RevenueLineItem']['fields']['campaign_id']['vname']='LBL_CAMPAIGN_ID';
$dictionary['RevenueLineItem']['fields']['campaign_id']['rname']='id';
$dictionary['RevenueLineItem']['fields']['campaign_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['campaign_id']['dbType']='id';
$dictionary['RevenueLineItem']['fields']['campaign_id']['table']='campaigns';
$dictionary['RevenueLineItem']['fields']['campaign_id']['isnull']='true';
$dictionary['RevenueLineItem']['fields']['campaign_id']['module']='Campaigns';
$dictionary['RevenueLineItem']['fields']['campaign_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['campaign_id']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['campaign_id']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['campaign_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['campaign_id']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_campaign_name.php

 // created: 2023-02-15 17:19:30
$dictionary['RevenueLineItem']['fields']['campaign_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['campaign_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['campaign_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['campaign_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['campaign_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['campaign_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['campaign_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['campaign_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['campaign_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_forecasted_likely.php

 // created: 2023-02-15 17:20:15
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['options']='';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['comments']='Rollup of included RLIs on the Opportunity';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['importable']='false';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_quote_id.php

 // created: 2023-02-15 17:21:25
$dictionary['RevenueLineItem']['fields']['quote_id']['name']='quote_id';
$dictionary['RevenueLineItem']['fields']['quote_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['quote_id']['vname']='LBL_QUOTE_ID';
$dictionary['RevenueLineItem']['fields']['quote_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['quote_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['quote_id']['comment']='If product created via Quote, this is quote ID';
$dictionary['RevenueLineItem']['fields']['quote_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['quote_id']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_quote_name.php

 // created: 2023-02-15 17:21:25
$dictionary['RevenueLineItem']['fields']['quote_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['quote_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['quote_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['quote_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['quote_name']['comments']='Quote Name';
$dictionary['RevenueLineItem']['fields']['quote_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['quote_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['quote_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['quote_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['quote_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['quote_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_product_type.php

 // created: 2023-02-15 17:22:24
$dictionary['RevenueLineItem']['fields']['product_type']['len']=100;
$dictionary['RevenueLineItem']['fields']['product_type']['audited']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['comments']='Type of product ( from opportunities opportunity_type ex: Existing, New)';
$dictionary['RevenueLineItem']['fields']['product_type']['importable']='false';
$dictionary['RevenueLineItem']['fields']['product_type']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['product_type']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['product_type']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['product_type']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_type_id.php

 // created: 2023-02-15 17:22:54
$dictionary['RevenueLineItem']['fields']['type_id']['name']='type_id';
$dictionary['RevenueLineItem']['fields']['type_id']['vname']='LBL_TYPE';
$dictionary['RevenueLineItem']['fields']['type_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['type_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['type_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['type_id']['comment']='Product type (ex: hardware, software)';
$dictionary['RevenueLineItem']['fields']['type_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['type_id']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_type_name.php

 // created: 2023-02-15 17:22:54
$dictionary['RevenueLineItem']['fields']['type_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['type_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['type_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['type_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['type_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['type_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['type_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['type_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['type_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_account_id.php

 // created: 2023-02-15 17:24:18
$dictionary['RevenueLineItem']['fields']['account_id']['name']='account_id';
$dictionary['RevenueLineItem']['fields']['account_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['account_id']['vname']='LBL_ACCOUNT_ID';
$dictionary['RevenueLineItem']['fields']['account_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['account_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['account_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['account_id']['comment']='Account this product is associated with';
$dictionary['RevenueLineItem']['fields']['account_id']['formula']='ifElse(related($opportunities, "account_id"), related($opportunities, "account_id"), $account_id)';
$dictionary['RevenueLineItem']['fields']['account_id']['enforced']=true;
$dictionary['RevenueLineItem']['fields']['account_id']['calculated']=true;
$dictionary['RevenueLineItem']['fields']['account_id']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_account_name.php

 // created: 2023-02-15 17:24:18
$dictionary['RevenueLineItem']['fields']['account_name']['audited']=true;
$dictionary['RevenueLineItem']['fields']['account_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['account_name']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['account_name']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['account_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['account_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['account_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_add_on_to_id.php

 // created: 2023-02-15 17:25:23
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['name']='add_on_to_id';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['comment']='Purchased line item that this is an add-on to';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['vname']='LBL_ADD_ON_TO_ID';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['rname']='id';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['dbType']='id';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['table']='purchased_line_items';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['isnull']='true';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['module']='PurchasedLineItems';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_add_on_to_name.php

 // created: 2023-02-15 17:25:23
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_renewal_rli_id.php

 // created: 2023-02-15 17:26:12
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['name']='renewal_rli_id';
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['vname']='LBL_RENEWAL_RLI_ID';
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['comment']='Renewal RLI of this RLI, set during renewal generation';
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_renewal_rli_name.php

 // created: 2023-02-15 17:26:12
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_sync_key.php

 // created: 2023-02-15 17:27:11
$dictionary['RevenueLineItem']['fields']['sync_key']['audited']=false;
$dictionary['RevenueLineItem']['fields']['sync_key']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['sync_key']['importable']='false';
$dictionary['RevenueLineItem']['fields']['sync_key']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['sync_key']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_asset_number.php

 // created: 2023-02-15 17:27:57
$dictionary['RevenueLineItem']['fields']['asset_number']['len']='50';
$dictionary['RevenueLineItem']['fields']['asset_number']['audited']=false;
$dictionary['RevenueLineItem']['fields']['asset_number']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['asset_number']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['asset_number']['comments']='Asset tag number of sales item in use';
$dictionary['RevenueLineItem']['fields']['asset_number']['importable']='false';
$dictionary['RevenueLineItem']['fields']['asset_number']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['asset_number']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['asset_number']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['asset_number']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['asset_number']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['asset_number']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_book_value_date.php

 // created: 2023-02-15 17:28:30
$dictionary['RevenueLineItem']['fields']['book_value_date']['audited']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['comments']='Date of book value for sales item in use';
$dictionary['RevenueLineItem']['fields']['book_value_date']['importable']='false';
$dictionary['RevenueLineItem']['fields']['book_value_date']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value_date']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['book_value_date']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value_date']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_purchased.php

 // created: 2023-02-15 17:29:12
$dictionary['RevenueLineItem']['fields']['date_purchased']['audited']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['comments']='Date sales item purchased';
$dictionary['RevenueLineItem']['fields']['date_purchased']['importable']='false';
$dictionary['RevenueLineItem']['fields']['date_purchased']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['date_purchased']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['date_purchased']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_purchased']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_support_expires.php

 // created: 2023-02-15 17:29:57
$dictionary['RevenueLineItem']['fields']['date_support_expires']['audited']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['comments']='Support expiration date';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['importable']='false';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_support_starts.php

 // created: 2023-02-15 17:30:38
$dictionary['RevenueLineItem']['fields']['date_support_starts']['audited']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['comments']='Support start date';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['importable']='false';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_pricing_factor.php

 // created: 2023-02-15 17:31:25
$dictionary['RevenueLineItem']['fields']['pricing_factor']['len']='4';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['audited']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['comments']='Variable pricing factor depending on pricing_formula';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['importable']='false';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['pricing_factor']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['min']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['max']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_pricing_formula.php

 // created: 2023-02-15 17:31:58
$dictionary['RevenueLineItem']['fields']['pricing_formula']['len']='100';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['audited']=false;
$dictionary['RevenueLineItem']['fields']['pricing_formula']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['pricing_formula']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['pricing_formula']['comments']='Pricing formula (ex: Fixed, Markup over Cost)';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['importable']='false';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['pricing_formula']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['pricing_formula']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_serial_number.php

 // created: 2023-02-15 17:32:30
$dictionary['RevenueLineItem']['fields']['serial_number']['len']='50';
$dictionary['RevenueLineItem']['fields']['serial_number']['audited']=false;
$dictionary['RevenueLineItem']['fields']['serial_number']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['serial_number']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['serial_number']['comments']='Serial number of sales item in use';
$dictionary['RevenueLineItem']['fields']['serial_number']['importable']='false';
$dictionary['RevenueLineItem']['fields']['serial_number']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['serial_number']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['serial_number']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['serial_number']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['serial_number']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['serial_number']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_support_contact.php

 // created: 2023-02-15 17:33:19
$dictionary['RevenueLineItem']['fields']['support_contact']['len']='50';
$dictionary['RevenueLineItem']['fields']['support_contact']['audited']=false;
$dictionary['RevenueLineItem']['fields']['support_contact']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_contact']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_contact']['comments']='Contact for support purposes';
$dictionary['RevenueLineItem']['fields']['support_contact']['importable']='false';
$dictionary['RevenueLineItem']['fields']['support_contact']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['support_contact']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['support_contact']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['support_contact']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['support_contact']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['support_contact']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_support_description.php

 // created: 2023-02-15 17:33:53
$dictionary['RevenueLineItem']['fields']['support_description']['len']='255';
$dictionary['RevenueLineItem']['fields']['support_description']['audited']=false;
$dictionary['RevenueLineItem']['fields']['support_description']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_description']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_description']['comments']='Description of sales item for support purposes';
$dictionary['RevenueLineItem']['fields']['support_description']['importable']='false';
$dictionary['RevenueLineItem']['fields']['support_description']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['support_description']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['support_description']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['support_description']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['support_description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['support_description']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_support_name.php

 // created: 2023-02-15 17:34:23
$dictionary['RevenueLineItem']['fields']['support_name']['len']='50';
$dictionary['RevenueLineItem']['fields']['support_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['support_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_name']['comments']='Name of sales item for support purposes';
$dictionary['RevenueLineItem']['fields']['support_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['support_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['support_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['support_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['support_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['support_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['support_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_support_term.php

 // created: 2023-02-15 17:35:02
$dictionary['RevenueLineItem']['fields']['support_term']['len']='100';
$dictionary['RevenueLineItem']['fields']['support_term']['audited']=false;
$dictionary['RevenueLineItem']['fields']['support_term']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_term']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_term']['comments']='Term (length) of support contract';
$dictionary['RevenueLineItem']['fields']['support_term']['importable']='false';
$dictionary['RevenueLineItem']['fields']['support_term']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['support_term']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['support_term']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['support_term']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['support_term']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['support_term']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_tax_class.php

 // created: 2023-02-15 17:35:49
$dictionary['RevenueLineItem']['fields']['tax_class']['audited']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['comments']='Tax classification (ex: Taxable, Non-taxable)';
$dictionary['RevenueLineItem']['fields']['tax_class']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['tax_class']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['tax_class']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['tax_class']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_vendor_part_num.php

 // created: 2023-02-15 17:36:17
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['len']='50';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['audited']=false;
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['comments']='Vendor part number';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['importable']='false';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_website.php

 // created: 2023-02-15 17:37:07
$dictionary['RevenueLineItem']['fields']['website']['len']='255';
$dictionary['RevenueLineItem']['fields']['website']['audited']=false;
$dictionary['RevenueLineItem']['fields']['website']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['website']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['website']['comments']='Sales item URL';
$dictionary['RevenueLineItem']['fields']['website']['importable']='false';
$dictionary['RevenueLineItem']['fields']['website']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['website']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['website']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['website']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['website']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['website']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_weight.php

 // created: 2023-02-15 17:37:38
$dictionary['RevenueLineItem']['fields']['weight']['audited']=false;
$dictionary['RevenueLineItem']['fields']['weight']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['weight']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['weight']['comments']='Weight of the sales item';
$dictionary['RevenueLineItem']['fields']['weight']['importable']='false';
$dictionary['RevenueLineItem']['fields']['weight']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['weight']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['weight']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['weight']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['weight']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['weight']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2023-02-15 17:38:15
$dictionary['RevenueLineItem']['fields']['base_rate']['audited']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['importable']='false';
$dictionary['RevenueLineItem']['fields']['base_rate']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['base_rate']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['base_rate']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['base_rate']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['base_rate']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['readonly']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['rows']='4';
$dictionary['RevenueLineItem']['fields']['base_rate']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sasa1_sasa_seguradoras_revenuelineitems_1_RevenueLineItems.php

// created: 2023-05-25 19:46:01
$dictionary["RevenueLineItem"]["fields"]["sasa1_sasa_seguradoras_revenuelineitems_1"] = array (
  'name' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'type' => 'link',
  'relationship' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'source' => 'non-db',
  'module' => 'SASA1_sasa_seguradoras',
  'bean_name' => 'SASA1_sasa_seguradoras',
  'side' => 'right',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_REVENUELINEITEMS_TITLE',
  'id_name' => 'sasa1_sasabb9eradoras_ida',
  'link-type' => 'one',
);
$dictionary["RevenueLineItem"]["fields"]["sasa1_sasa_seguradoras_revenuelineitems_1_name"] = array (
  'name' => 'sasa1_sasa_seguradoras_revenuelineitems_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_SASA1_SASA_SEGURADORAS_TITLE',
  'save' => true,
  'id_name' => 'sasa1_sasabb9eradoras_ida',
  'link' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'table' => 'sasa1_sasa_seguradoras',
  'module' => 'SASA1_sasa_seguradoras',
  'rname' => 'name',
);
$dictionary["RevenueLineItem"]["fields"]["sasa1_sasabb9eradoras_ida"] = array (
  'name' => 'sasa1_sasabb9eradoras_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_REVENUELINEITEMS_TITLE_ID',
  'id_name' => 'sasa1_sasabb9eradoras_ida',
  'link' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'table' => 'sasa1_sasa_seguradoras',
  'module' => 'SASA1_sasa_seguradoras',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>

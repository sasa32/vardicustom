<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$searchFields['RevenueLineItems'] = array (
  'name' => 
  array (
    'query_type' => 'default',
    'force_unifiedsearch' => true,
  ),
  'account_name' => 
  array (
    'query_type' => 'default',
    'db_field' => 
    array (
      0 => 'accounts.name',
    ),
  ),
  'opportunity_name' => 
  array (
    'query_type' => 'default',
    'db_field' => 
    array (
      0 => 'opportunities.name',
    ),
  ),
  'best_case' => 
  array (
    'query_type' => 'default',
  ),
  'likely_case' => 
  array (
    'query_type' => 'default',
  ),
  'worst_case' => 
  array (
    'query_type' => 'default',
  ),
  'probability' => 
  array (
    'query_type' => 'default',
  ),
  'sales_stage' => 
  array (
    'query_type' => 'default',
    'options' => 'sales_stage_dom',
    'template_var' => 'SALES_STAGE_OPTIONS',
    'options_add_blank' => true,
  ),
  'type_id' => 
  array (
    'query_type' => 'default',
    'options' => 'product_type_dom',
    'template_var' => 'TYPE_OPTIONS',
  ),
  'category_id' => 
  array (
    'query_type' => 'default',
    'options' => 'products_cat_dom',
    'template_var' => 'CATEGORY_OPTIONS',
  ),
  'manufacturer_id' => 
  array (
    'query_type' => 'default',
    'options' => 'manufacturer_dom',
    'template_var' => 'MANUFACTURER_OPTIONS',
  ),
  'favorites_only' => 
  array (
    'query_type' => 'format',
    'operator' => 'subquery',
    'subquery' => 'SELECT sugarfavorites.record_id FROM sugarfavorites 
                                        WHERE sugarfavorites.deleted=0 
                                            and sugarfavorites.module = \'RevenueLineItems\'
                                            and sugarfavorites.assigned_user_id = \'{0}\'',
    'db_field' => 
    array (
      0 => 'id',
    ),
  ),
  'range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'cost_price' => 
  array (
    'query_type' => 'default',
  ),
  'book_value' => 
  array (
    'query_type' => 'default',
  ),
  'list_price' => 
  array (
    'query_type' => 'default',
  ),
  'product_template_name' => 
  array (
    'query_type' => 'default',
  ),
  'subtotal' => 
  array (
    'query_type' => 'default',
  ),
  'total_amount' => 
  array (
    'query_type' => 'default',
  ),
  'quantity' => 
  array (
    'query_type' => 'default',
  ),
  'service_start_date' => 
  array (
    'query_type' => 'default',
  ),
  'manufacturer_name' => 
  array (
    'query_type' => 'default',
  ),
  'category_name' => 
  array (
    'query_type' => 'default',
  ),
  'mft_part_num' => 
  array (
    'query_type' => 'default',
  ),
  'discount_price' => 
  array (
    'query_type' => 'default',
  ),
  'discount_rate_percent' => 
  array (
    'query_type' => 'default',
  ),
  'discount_amount_usdollar' => 
  array (
    'query_type' => 'default',
  ),
  'deal_calc' => 
  array (
    'query_type' => 'default',
  ),
  'deal_calc_usdollar' => 
  array (
    'query_type' => 'default',
  ),
  'cost_usdollar' => 
  array (
    'query_type' => 'default',
  ),
  'discount_usdollar' => 
  array (
    'query_type' => 'default',
  ),
  'list_usdollar' => 
  array (
    'query_type' => 'default',
  ),
  'status' => 
  array (
    'query_type' => 'default',
  ),
  'book_value_usdollar' => 
  array (
    'query_type' => 'default',
  ),
  'generate_purchase' => 
  array (
    'query_type' => 'default',
  ),
  'purchasedlineitem_name' => 
  array (
    'query_type' => 'default',
  ),
  'date_closed' => 
  array (
    'query_type' => 'default',
  ),
  'next_step' => 
  array (
    'query_type' => 'default',
  ),
  'commit_stage' => 
  array (
    'query_type' => 'default',
  ),
  'lead_source' => 
  array (
    'query_type' => 'default',
  ),
  'campaign_name' => 
  array (
    'query_type' => 'default',
  ),
  'forecasted_likely' => 
  array (
    'query_type' => 'default',
  ),
  'quote_name' => 
  array (
    'query_type' => 'default',
  ),
  'product_type' => 
  array (
    'query_type' => 'default',
  ),
  'type_name' => 
  array (
    'query_type' => 'default',
  ),
  'add_on_to_name' => 
  array (
    'query_type' => 'default',
  ),
  'renewal_rli_name' => 
  array (
    'query_type' => 'default',
  ),
  'asset_number' => 
  array (
    'query_type' => 'default',
  ),
  'book_value_date' => 
  array (
    'query_type' => 'default',
  ),
  'date_purchased' => 
  array (
    'query_type' => 'default',
  ),
  'date_support_expires' => 
  array (
    'query_type' => 'default',
  ),
  'date_support_starts' => 
  array (
    'query_type' => 'default',
  ),
  'pricing_factor' => 
  array (
    'query_type' => 'default',
  ),
  'pricing_formula' => 
  array (
    'query_type' => 'default',
  ),
  'serial_number' => 
  array (
    'query_type' => 'default',
  ),
  'support_contact' => 
  array (
    'query_type' => 'default',
  ),
  'support_description' => 
  array (
    'query_type' => 'default',
  ),
  'support_name' => 
  array (
    'query_type' => 'default',
  ),
  'support_term' => 
  array (
    'query_type' => 'default',
  ),
  'tax_class' => 
  array (
    'query_type' => 'default',
  ),
  'vendor_part_num' => 
  array (
    'query_type' => 'default',
  ),
  'website' => 
  array (
    'query_type' => 'default',
  ),
  'weight' => 
  array (
    'query_type' => 'default',
  ),
  'base_rate' => 
  array (
    'query_type' => 'default',
  ),
);
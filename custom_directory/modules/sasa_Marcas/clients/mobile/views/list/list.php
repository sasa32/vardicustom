<?php
$module_name = 'sasa_Marcas';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_cd_marca_c',
                'label' => 'LBL_SASA_CD_MARCA_C',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_vendidopornosotros_c',
                'label' => 'LBL_SASA_MA_ANO_INGRE_MERCADO_C',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'date_modified',
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);

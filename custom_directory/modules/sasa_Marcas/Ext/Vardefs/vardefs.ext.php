<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Marcas/Ext/Vardefs/sugarfield_sasa_cd_marca_c.php

 // created: 2020-05-12 17:02:41
$dictionary['sasa_Marcas']['fields']['sasa_cd_marca_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_Marcas']['fields']['sasa_cd_marca_c']['labelValue']='Código marca';
$dictionary['sasa_Marcas']['fields']['sasa_cd_marca_c']['calculated']='';
$dictionary['sasa_Marcas']['fields']['sasa_cd_marca_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Marcas/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2020-05-13 17:22:04
$dictionary['sasa_Marcas']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['sasa_Marcas']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['sasa_Marcas']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Marcas/Ext/Vardefs/sasa_marcas_sasa_vehiculos_1_sasa_Marcas.php

// created: 2020-05-15 18:26:27
$dictionary["sasa_Marcas"]["fields"]["sasa_marcas_sasa_vehiculos_1"] = array (
  'name' => 'sasa_marcas_sasa_vehiculos_1',
  'type' => 'link',
  'relationship' => 'sasa_marcas_sasa_vehiculos_1',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'vname' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_MARCAS_TITLE',
  'id_name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Marcas/Ext/Vardefs/sugarfield_sasa_vendidopornosotros_c.php

 // created: 2020-05-16 20:09:45
$dictionary['sasa_Marcas']['fields']['sasa_vendidopornosotros_c']['labelValue']='Vendido por Nosotros';
$dictionary['sasa_Marcas']['fields']['sasa_vendidopornosotros_c']['dependency']='';
$dictionary['sasa_Marcas']['fields']['sasa_vendidopornosotros_c']['visibility_grid']='';

 
?>

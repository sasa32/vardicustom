<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Marcas/Ext/Language/en_us.gvmarcascustomFieldsv001--2020-05-15.php


  $mod_strings['LBL_SASA_MA_ANO_INGRE_MERCADO_C'] = 'Vendido por Nosotros';
  $mod_strings['LBL_SASA_CD_MARCA_C'] = 'Código Marca';
  $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
  $mod_strings['LBL_NAME'] = 'Nombre de la Marca';
  $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de creación';
  $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de modificación';
 
  
?>
<?php
// Merged from custom/Extension/modules/sasa_Marcas/Ext/Language/en_us.customsasa_marcas_sasa_vehiculos_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Marcas/Ext/Language/en_us.customsasa_marcas_sasa_vehiculos_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_VEHICULOS_TITLE'] = 'Líneas de vehículos';
$mod_strings['LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_MARCAS_TITLE'] = 'Líneas de vehículos';

?>
<?php
// Merged from custom/Extension/modules/sasa_Marcas/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_VEHICULOS_TITLE'] = 'Líneas de vehículos';
$mod_strings['LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_MARCAS_TITLE'] = 'Líneas de vehículos';


?>
<?php
// Merged from custom/Extension/modules/sasa_Marcas/Ext/Language/en_us.customsasa_marcas_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MARCAS_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_MARCAS_LEADS_1_FROM_SASA_MARCAS_TITLE'] = 'Leads';

?>

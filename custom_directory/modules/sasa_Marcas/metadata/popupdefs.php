<?php
$popupMeta = array (
    'moduleMain' => 'sasa_Marcas',
    'varName' => 'sasa_Marcas',
    'orderBy' => 'sasa_marcas.name',
    'whereClauses' => array (
  'name' => 'sasa_marcas.name',
  'sasa_cd_marca_c' => 'sasa_marcas_cstm.sasa_cd_marca_c',
  'sasa_vendidopornosotros_c' => 'sasa_marcas_cstm.sasa_vendidopornosotros_c',
  'date_entered' => 'sasa_marcas.date_entered',
  'date_modified' => 'sasa_marcas.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sasa_cd_marca_c',
  5 => 'sasa_vendidopornosotros_c',
  6 => 'date_entered',
  7 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'sasa_cd_marca_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_CD_MARCA_C',
    'width' => 10,
    'name' => 'sasa_cd_marca_c',
  ),
  'sasa_vendidopornosotros_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_MA_ANO_INGRE_MERCADO_C',
    'width' => 10,
    'name' => 'sasa_vendidopornosotros_c',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SASA_CD_MARCA_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_CD_MARCA_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_VENDIDOPORNOSOTROS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_MA_ANO_INGRE_MERCADO_C',
    'width' => 10,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
),
);

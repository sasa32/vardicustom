<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SASA1_sasa_seguradoras/Ext/Layoutdefs/sasa1_sasa_seguradoras_revenuelineitems_1_SASA1_sasa_seguradoras.php

 // created: 2023-05-25 19:46:01
$layout_defs["SASA1_sasa_seguradoras"]["subpanel_setup"]['sasa1_sasa_seguradoras_revenuelineitems_1'] = array (
  'order' => 100,
  'module' => 'RevenueLineItems',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_REVENUELINEITEMS_TITLE',
  'get_subpanel_data' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/SASA1_sasa_seguradoras/Ext/Layoutdefs/sasa1_sasa_seguradoras_purchasedlineitems_1_SASA1_sasa_seguradoras.php

 // created: 2023-05-25 19:59:20
$layout_defs["SASA1_sasa_seguradoras"]["subpanel_setup"]['sasa1_sasa_seguradoras_purchasedlineitems_1'] = array (
  'order' => 100,
  'module' => 'PurchasedLineItems',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_PURCHASEDLINEITEMS_TITLE',
  'get_subpanel_data' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>

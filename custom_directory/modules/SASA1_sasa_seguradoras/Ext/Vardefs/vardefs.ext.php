<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SASA1_sasa_seguradoras/Ext/Vardefs/sasa1_sasa_seguradoras_revenuelineitems_1_SASA1_sasa_seguradoras.php

// created: 2023-05-25 19:46:01
$dictionary["SASA1_sasa_seguradoras"]["fields"]["sasa1_sasa_seguradoras_revenuelineitems_1"] = array (
  'name' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'type' => 'link',
  'relationship' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'source' => 'non-db',
  'module' => 'RevenueLineItems',
  'bean_name' => 'RevenueLineItem',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_SASA1_SASA_SEGURADORAS_TITLE',
  'id_name' => 'sasa1_sasabb9eradoras_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/SASA1_sasa_seguradoras/Ext/Vardefs/sasa1_sasa_seguradoras_purchasedlineitems_1_SASA1_sasa_seguradoras.php

// created: 2023-05-25 19:59:20
$dictionary["SASA1_sasa_seguradoras"]["fields"]["sasa1_sasa_seguradoras_purchasedlineitems_1"] = array (
  'name' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'type' => 'link',
  'relationship' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'source' => 'non-db',
  'module' => 'PurchasedLineItems',
  'bean_name' => 'PurchasedLineItem',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_SASA1_SASA_SEGURADORAS_TITLE',
  'id_name' => 'sasa1_sasa7258radoras_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SASA1_sasa_seguradoras/Ext/WirelessLayoutdefs/sasa1_sasa_seguradoras_revenuelineitems_1_SASA1_sasa_seguradoras.php

 // created: 2023-05-25 19:46:01
$layout_defs["SASA1_sasa_seguradoras"]["subpanel_setup"]['sasa1_sasa_seguradoras_revenuelineitems_1'] = array (
  'order' => 100,
  'module' => 'RevenueLineItems',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_REVENUELINEITEMS_TITLE',
  'get_subpanel_data' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
);

?>
<?php
// Merged from custom/Extension/modules/SASA1_sasa_seguradoras/Ext/WirelessLayoutdefs/sasa1_sasa_seguradoras_purchasedlineitems_1_SASA1_sasa_seguradoras.php

 // created: 2023-05-25 19:59:20
$layout_defs["SASA1_sasa_seguradoras"]["subpanel_setup"]['sasa1_sasa_seguradoras_purchasedlineitems_1'] = array (
  'order' => 100,
  'module' => 'PurchasedLineItems',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_PURCHASEDLINEITEMS_TITLE',
  'get_subpanel_data' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
);

?>

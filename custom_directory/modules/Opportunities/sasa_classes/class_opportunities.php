<?php
class class_opportunities
{

	private $argumentos;
	private $Bean = null;

	function __construct($args)
	{	
		$this->argumentos = $args;
		//$GLOBALS['log']->security("Argumento #: " . print_r($this->argumentos, true));
	
	}

	public function miFuncion(){

		return "opportunida mi funcion";
	}

	public function createOpportinitie(){

		//$Bean = null;		
		$response = array();
		$lineas = array();
		//Create bean
		$Bean = BeanFactory::newBean("Opportunities");

		foreach ($this->argumentos['cotizacion'] as $key => $value) 
		{
		//$GLOBALS['log']->security("Argumento #: " . $key . " Valor: " . $value);
			//Populate bean fields
			$Bean->{$key} = $value;
			if(!empty($this->argumentos['cotizacion']['sasa_cotsigru_c'])){

				$url = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota={$this->argumentos['cotizacion']['sasa_cotsigru_c']}";
				//$GLOBALS['log']->security("CAMPO DE LA URL: " . $url);
				$Bean->sasa_cotsigru_c = $url;
			}
		}

		$this->relationshipOfOpportunities($Bean);
		
		$id_opportunitie = $this->saveModules($Bean);
		$id_account = $this->argumentos['cotizacion']['account_id'];
		
		$lineas = $this->createLineasIngreso(
								$id_opportunitie,
								$id_account);

		$response["id_cotizacion"] = $id_opportunitie;
		$response["linea"]= $lineas;
	

		$GLOBALS['log']->security("Respuesta FInal: " . print_r($response, true));

		return $response;
	}

	private function createLineasIngreso($id_opportunitie, $id_account){

		$lineas_response = array();

		$GLOBALS['log']->security("Argumento #: " . print_r($this->argumentos['linea'][0]['name'], true));

		$cantidad = count($this->argumentos['linea']);
		$GLOBALS['log']->security("Cantidad array: " . $cantidad);
		
		for($i = 0; $i < $cantidad ; $i++){
			//Create bean
			  $Bean = BeanFactory::newBean("RevenueLineItems");
			  $Bean->opportunity_id = $id_opportunitie;
			  $Bean->account_name = $id_account;
			  $Bean->sales_stage = "Proposal/Price Quote";

			foreach($this->argumentos['linea'][$i] as $key => $value){
				//$GLOBALS['log']->security("Argumento # {$i}: " . $value);
				$Bean->{$key} = $value;
			}

			$lineas_response[] = $this->saveModules($Bean);
		}

		$GLOBALS['log']->security("Lineas Response #: " . print_r($lineas_response, true));

		return $lineas_response;

	}
	public function updateOpportinitie($id_cotizacion){

		//$Bean = null;
		$response = array();

		$GLOBALS['log']->security("Update de opportunities");

		//Retrieve bean
		$Bean = BeanFactory::retrieveBean("Opportunities", $id_cotizacion);
		
		//Returna No existe si la opportunidad no existe
		if($Bean == null) return "No existe";

		foreach ($this->argumentos['cotizacion'] as $key => $value) 
		{
		//$GLOBALS['log']->security("Argumento #: " . $key . " Valor: " . $value);
			//Fields to update
			$Bean->{$key} = $value;
			if(!empty($this->argumentos['cotizacion']['sasa_cotsigru_c'])){

				$url = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota={$this->argumentos['cotizacion']['sasa_cotsigru_c']}";
				//$GLOBALS['log']->security("CAMPO DE LA URL: " . $url);
				$Bean->sasa_cotsigru_c = $url;
			}
		}
		
		$this->relationshipOfOpportunities($Bean);
		$this->updateLineas();

		$response["id_cotizacion"] = $this->saveModules($Bean);
		$response["linea"]= $this->updateLineas();

		return $response;

	}

	public function queryLine($id_cotizacion, $name_linea = NULL){

		$id_lineas = array();
		//$name_linea = "";
		
		//$GLOBALS['log']->security("Name de la linea por el json: " . $name_linea);

		//$name_linea = is_null($name_linea) ? "" : "AND rli.name = '{$name_linea}'";
		if(!empty($name_linea)){
			$name_linea = "AND rli.name = '{$name_linea}'";
		}
		
		$sql = "SELECT o.id, o.name, rli.id as id_linea, rli.name as name_linea FROM opportunities o 
			INNER JOIN revenue_line_items rli ON rli.opportunity_id = o.id
			WHERE o.id = '{$id_cotizacion}' AND o.deleted = 0 AND rli.deleted = 0 {$name_linea}";

		$query = $GLOBALS['db']->query($sql);

		//$GLOBALS['log']->security("query lineas" . $sql);

			while ($row =  $GLOBALS['db']->fetchByAssoc($query)) {
				$id_lineas[] = $row['id_linea'];
			}

			//$GLOBALS['log']->security("Respuesta Lineas:: " . print_r($id_lineas, true));	
			return $id_lineas;
	}

	public function updateLineas(){

		$cantidad = count($this->argumentos['linea']);

		//$id_lineas = $this->queryLine($this->argumentos['cotizacion']['id_cotizacion'], "");
		$GLOBALS['log']->security("Cantidad Lineas update:" . $cantidad);
		$id_lineas = array();
		if(!empty($this->argumentos['linea'])){

			for($i = 0; $i < $cantidad ; $i++){
				
				$name_linea = $this->argumentos['linea'][$i]['name'];
				
				$id_lineas = $this->queryLine($this->argumentos['cotizacion']['id_cotizacion'], $name_linea);
				//$GLOBALS['log']->security("ID DE LA LINEA CON NAME: *{$i}* " . $id_lineas[0]);
				//Create bean
				$Bean = BeanFactory::retrieveBean("RevenueLineItems", $id_lineas[0]);

				foreach($this->argumentos['linea'][$i] as $key => $value){
					//$GLOBALS['log']->security("Argumento # {$i}: " . $value);
					//if($key !== "name") 
					if(empty($id_lineas[0])){
					  $Bean->opportunity_id = $this->argumentos['cotizacion']['id_cotizacion'];	
					  $Bean->sales_stage = "Proposal/Price Quote";
					}
					$Bean->{$key} = $value;
				}

				$lineas_response[] = $this->saveModules($Bean) ;
			}
			
			return $lineas_response;
		}
	}

	private function relationshipOfOpportunities($Bean){
		
		$punto_atencion = $this->argumentos['cotizacion']['sasa_codpatencion_c'];

		$linea_vehiculo = $this->argumentos['cotizacion']['sasa_linea_c'];

		$unidad_negocio = $this->argumentos['cotizacion']['sasa_codnegocio_c'];

		//Validar relación de punto de atención
		if(!empty($punto_atencion)){

			$query = $GLOBALS['db']->query("SELECT spv.id AS id_punto_antencion FROM sasa_puntos_de_ventas spv
				INNER JOIN sasa_puntos_de_ventas_cstm spvc ON spvc.id_c = spv.id
				WHERE spvc.sasa_codpuntoatencion_c = '{$punto_atencion}' AND spv.deleted = 0 limit 1");

			$id_punto =  $GLOBALS['db']->fetchByAssoc($query);

			//$GLOBALS['log']->security("Punto de Atención:" . $id_punto['id_punto_antencion']);

			$punto_atencion = $id_punto['id_punto_antencion'];

			$Bean->sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida = $punto_atencion;
			$Bean->load_relationship('sasa_puntos_de_ventas_opportunities_1');
			$Bean->sasa_puntos_de_ventas_opportunities_1->add($punto_atencion);
		}

		//Validar relación de linea de vehículo
		if(!empty($linea_vehiculo)){

			$query = $GLOBALS['db']->query("SELECT sv.id AS id_linea_vehiculo FROM sasa_vehiculos sv 
			INNER JOIN sasa_vehiculos_cstm svc ON svc.id_c = sv.id
			WHERE svc.sas_cd_line_vehi_c = '{$linea_vehiculo}' AND sv.deleted = 0 limit 1");

			$id_linea =  $GLOBALS['db']->fetchByAssoc($query);

			$GLOBALS['log']->security("Linea Vehículo:" . $id_linea['id_linea_vehiculo']);

			$linea_vehiculo = $id_linea['id_linea_vehiculo'];

			$Bean->sasa_vehiculos_opportunities_1sasa_vehiculos_ida = $linea_vehiculo;
			$Bean->load_relationship('sasa_vehiculos_opportunities_1');
			$Bean->sasa_vehiculos_opportunities_1->add($linea_vehiculo);
		}

		//Validar relación de unidad de negocio
		if(!empty($unidad_negocio)){

			$query = $GLOBALS['db']->query("SELECT sun.id AS id_unidad_negocio 
				FROM sasa_unidad_de_negocio sun
				INNER JOIN sasa_unidad_de_negocio_cstm sunc ON sunc.id_c = sun.id
				WHERE sunc.sasa_codunidnegocio_c = '{$unidad_negocio}'  AND sun.deleted = 0 limit 1");

			$id_unidad =  $GLOBALS['db']->fetchByAssoc($query);

			$GLOBALS['log']->security("Unidad de negocio:" . $id_unidad['id_unidad_negocio']);

			$unidad_negocio = $id_unidad['id_unidad_negocio'];

			$Bean->sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida = $unidad_negocio;
			$Bean->load_relationship('sasa_unidad_de_negocio_opportunities_1');
			$Bean->sasa_unidad_de_negocio_opportunities_1->add($unidad_negocio);
		}
	}

	private function saveModules($Bean){
		
		//Save
		$Bean->save();

		//Retrieve the bean id
		$record_id = $Bean->id;

		return $record_id;
	}

}

?>
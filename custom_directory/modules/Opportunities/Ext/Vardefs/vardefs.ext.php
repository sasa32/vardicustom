<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_date_closed_timestamp.php

 // created: 2020-01-31 00:19:08
$dictionary['Opportunity']['fields']['date_closed_timestamp']['audited']=false;
$dictionary['Opportunity']['fields']['date_closed_timestamp']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['date_closed_timestamp']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['date_closed_timestamp']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['date_closed_timestamp']['formula']='rollupMax($revenuelineitems, "date_closed_timestamp")';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_probability.php

 // created: 2020-01-31 00:19:08
$dictionary['Opportunity']['fields']['probability']['audited']=false;
$dictionary['Opportunity']['fields']['probability']['massupdate']=false;
$dictionary['Opportunity']['fields']['probability']['comments']='The probability of closure';
$dictionary['Opportunity']['fields']['probability']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['probability']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['probability']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['probability']['reportable']=false;
$dictionary['Opportunity']['fields']['probability']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['probability']['min']=false;
$dictionary['Opportunity']['fields']['probability']['max']=false;
$dictionary['Opportunity']['fields']['probability']['disable_num_format']='';
$dictionary['Opportunity']['fields']['probability']['studio']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/rli_link_workflow.php

$dictionary['Opportunity']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/dupe_check.ext.php

$dictionary['Opportunity']['fields']['revenuelineitems']['workflow'] = true;
$dictionary['Opportunity']['duplicate_check']['FilterDuplicateCheck']['filter_template'][0]['$and'][1] = array('sales_status' => array('$not_equals' => 'Closed Lost'));
$dictionary['Opportunity']['duplicate_check']['FilterDuplicateCheck']['filter_template'][0]['$and'][2] = array('sales_status' => array('$not_equals' => 'Closed Won'));
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/denorm_account_name.php


// 'account_name'
$dictionary['Opportunity']['fields']['account_name']['is_denormalized'] = true;
$dictionary['Opportunity']['fields']['account_name']['denormalized_field_name'] = 'denorm_account_name';

// 'denorm_account_name'
$dictionary['Opportunity']['fields']['denorm_account_name']['name'] = 'denorm_account_name';
$dictionary['Opportunity']['fields']['denorm_account_name']['type'] = 'varchar';
$dictionary['Opportunity']['fields']['denorm_account_name']['dbType'] = 'varchar';
$dictionary['Opportunity']['fields']['denorm_account_name']['vname'] = 'LBL_ACCOUNT_NAME';
$dictionary['Opportunity']['fields']['denorm_account_name']['len'] = '150';
$dictionary['Opportunity']['fields']['denorm_account_name']['comment'] = 'Name of the Company';
$dictionary['Opportunity']['fields']['denorm_account_name']['unified_search'] = true;
$dictionary['Opportunity']['fields']['denorm_account_name']['full_text_search'] = array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['Opportunity']['fields']['denorm_account_name']['audited'] = false;
$dictionary['Opportunity']['fields']['denorm_account_name']['required'] = false;
$dictionary['Opportunity']['fields']['denorm_account_name']['importable'] = 'false';
$dictionary['Opportunity']['fields']['denorm_account_name']['duplicate_on_record_copy'] = 'always';
$dictionary['Opportunity']['fields']['denorm_account_name']['merge_filter'] = 'disabled';
$dictionary['Opportunity']['fields']['denorm_account_name']['massupdate'] = false;
$dictionary['Opportunity']['fields']['denorm_account_name']['comments'] = 'Name of the Company';
$dictionary['Opportunity']['fields']['denorm_account_name']['duplicate_merge'] = 'disabled';
$dictionary['Opportunity']['fields']['denorm_account_name']['duplicate_merge_dom_value'] = 0;
$dictionary['Opportunity']['fields']['denorm_account_name']['calculated'] = '1';
$dictionary['Opportunity']['fields']['denorm_account_name']['formula'] = 'concat($sasa_nombres_c," ",$sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['Opportunity']['fields']['denorm_account_name']['enforced'] = true;
$dictionary['Opportunity']['fields']['denorm_account_name']['denorm_from_module'] = 'Accounts';
$dictionary['Opportunity']['fields']['denorm_account_name']['studio'] = false;

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sales_status.php

 // created: 2022-07-20 09:37:04
$dictionary['Opportunity']['fields']['sales_status']['audited']=false;
$dictionary['Opportunity']['fields']['sales_status']['massupdate']=true;
$dictionary['Opportunity']['fields']['sales_status']['importable']=true;
$dictionary['Opportunity']['fields']['sales_status']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sales_status']['reportable']=true;
$dictionary['Opportunity']['fields']['sales_status']['calculated']=false;
$dictionary['Opportunity']['fields']['sales_status']['dependency']=false;
$dictionary['Opportunity']['fields']['sales_status']['studio']=true;
$dictionary['Opportunity']['fields']['sales_status']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_closed_revenue_line_items.php

 // created: 2022-07-20 09:37:04
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['audited']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['massupdate']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['reportable']=true;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['min']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['max']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['disable_num_format']='';
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_total_revenue_line_items.php

 // created: 2022-07-20 09:37:04
$dictionary['Opportunity']['fields']['total_revenue_line_items']['audited']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['massupdate']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['total_revenue_line_items']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['total_revenue_line_items']['reportable']=true;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['min']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['max']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['disable_num_format']='';
$dictionary['Opportunity']['fields']['total_revenue_line_items']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_closed_won_revenue_line_items.php

 // created: 2022-07-20 09:37:04
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['audited']=false;
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['massupdate']=false;
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['reportable']=true;
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['min']=false;
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['max']=false;
$dictionary['Opportunity']['fields']['closed_won_revenue_line_items']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sasa_unidad_de_negocio_opportunities_1_Opportunities.php

// created: 2022-11-23 15:22:18
$dictionary["Opportunity"]["fields"]["sasa_unidad_de_negocio_opportunities_1"] = array (
  'name' => 'sasa_unidad_de_negocio_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_unidad_de_negocio_opportunities_1',
  'source' => 'non-db',
  'module' => 'sasa_Unidad_de_Negocio',
  'bean_name' => 'sasa_Unidad_de_Negocio',
  'side' => 'right',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'link-type' => 'one',
);
$dictionary["Opportunity"]["fields"]["sasa_unidad_de_negocio_opportunities_1_name"] = array (
  'name' => 'sasa_unidad_de_negocio_opportunities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'save' => true,
  'id_name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'link' => 'sasa_unidad_de_negocio_opportunities_1',
  'table' => 'sasa_unidad_de_negocio',
  'module' => 'sasa_Unidad_de_Negocio',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida"] = array (
  'name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID',
  'id_name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'link' => 'sasa_unidad_de_negocio_opportunities_1',
  'table' => 'sasa_unidad_de_negocio',
  'module' => 'sasa_Unidad_de_Negocio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sasa_puntos_de_ventas_opportunities_1_Opportunities.php

// created: 2022-11-23 15:55:57
$dictionary["Opportunity"]["fields"]["sasa_puntos_de_ventas_opportunities_1"] = array (
  'name' => 'sasa_puntos_de_ventas_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_opportunities_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'side' => 'right',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida',
  'link-type' => 'one',
);
$dictionary["Opportunity"]["fields"]["sasa_puntos_de_ventas_opportunities_1_name"] = array (
  'name' => 'sasa_puntos_de_ventas_opportunities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_opportunities_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida"] = array (
  'name' => 'sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID',
  'id_name' => 'sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_opportunities_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sasa_vehiculos_opportunities_1_Opportunities.php

// created: 2022-11-23 16:02:16
$dictionary["Opportunity"]["fields"]["sasa_vehiculos_opportunities_1"] = array (
  'name' => 'sasa_vehiculos_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_opportunities_1',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'side' => 'right',
  'vname' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'link-type' => 'one',
);
$dictionary["Opportunity"]["fields"]["sasa_vehiculos_opportunities_1_name"] = array (
  'name' => 'sasa_vehiculos_opportunities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_SASA_VEHICULOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_opportunities_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["sasa_vehiculos_opportunities_1sasa_vehiculos_ida"] = array (
  'name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID',
  'id_name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_opportunities_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_vehiculos_opportunities_1sasa_vehiculos_ida.php

 // created: 2022-11-23 16:08:27
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['name']='sasa_vehiculos_opportunities_1sasa_vehiculos_ida';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['type']='id';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['source']='non-db';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['vname']='LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['id_name']='sasa_vehiculos_opportunities_1sasa_vehiculos_ida';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['link']='sasa_vehiculos_opportunities_1';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['table']='sasa_vehiculos';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['module']='sasa_vehiculos';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['rname']='id';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['side']='right';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['hideacl']=true;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_vehiculos_opportunities_1_name.php

 // created: 2022-11-23 16:08:28
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['calculated']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['vname']='LBL_SASA_VEHICULOS_OPPORTUNITIES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida.php

 // created: 2022-11-23 16:17:14
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['name']='sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['type']='id';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['source']='non-db';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['vname']='LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['id_name']='sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['link']='sasa_puntos_de_ventas_opportunities_1';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['table']='sasa_puntos_de_ventas';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['module']='sasa_Puntos_de_Ventas';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['rname']='id';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['side']='right';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['hideacl']=true;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_puntos_de_ventas_opportunities_1_name.php

 // created: 2022-11-23 16:17:14
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1_name']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1_name']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1_name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1_name']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1_name']['calculated']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1_name']['vname']='LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida.php

 // created: 2022-11-23 16:18:26
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['name']='sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['type']='id';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['source']='non-db';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['vname']='LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['id_name']='sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['link']='sasa_unidad_de_negocio_opportunities_1';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['table']='sasa_unidad_de_negocio';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['module']='sasa_Unidad_de_Negocio';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['rname']='id';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['side']='right';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['hideacl']=true;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_unidad_de_negocio_opportunities_1_name.php

 // created: 2022-11-23 16:18:27
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1_name']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1_name']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1_name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1_name']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1_name']['calculated']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1_name']['vname']='LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_account_id.php

 // created: 2022-11-25 16:42:25
$dictionary['Opportunity']['fields']['account_id']['name']='account_id';
$dictionary['Opportunity']['fields']['account_id']['vname']='LBL_ACCOUNT_ID';
$dictionary['Opportunity']['fields']['account_id']['id_name']='account_id';
$dictionary['Opportunity']['fields']['account_id']['type']='relate';
$dictionary['Opportunity']['fields']['account_id']['link']='accounts';
$dictionary['Opportunity']['fields']['account_id']['rname']='id';
$dictionary['Opportunity']['fields']['account_id']['source']='non-db';
$dictionary['Opportunity']['fields']['account_id']['audited']=false;
$dictionary['Opportunity']['fields']['account_id']['dbType']='id';
$dictionary['Opportunity']['fields']['account_id']['module']='Accounts';
$dictionary['Opportunity']['fields']['account_id']['massupdate']=false;
$dictionary['Opportunity']['fields']['account_id']['importable']='required';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_account_name.php

 // created: 2022-11-25 16:42:25
$dictionary['Opportunity']['fields']['account_name']['len']=255;
$dictionary['Opportunity']['fields']['account_name']['required']=false;
$dictionary['Opportunity']['fields']['account_name']['audited']=true;
$dictionary['Opportunity']['fields']['account_name']['massupdate']=false;
$dictionary['Opportunity']['fields']['account_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['account_name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['account_name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['account_name']['reportable']=false;
$dictionary['Opportunity']['fields']['account_name']['calculated']=false;
$dictionary['Opportunity']['fields']['account_name']['related_fields']=array (
  0 => 'account_id',
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2022-11-25 17:14:29
$dictionary['Opportunity']['fields']['date_modified']['audited']=false;
$dictionary['Opportunity']['fields']['date_modified']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Opportunity']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['date_modified']['calculated']=false;
$dictionary['Opportunity']['fields']['date_modified']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_opportunity_type.php

 // created: 2022-11-25 17:15:43
$dictionary['Opportunity']['fields']['opportunity_type']['len']=100;
$dictionary['Opportunity']['fields']['opportunity_type']['audited']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['massupdate']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['comments']='Type of opportunity (ex: Existing, New)';
$dictionary['Opportunity']['fields']['opportunity_type']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['opportunity_type']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['opportunity_type']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['opportunity_type']['reportable']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['calculated']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['dependency']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['visibility_grid']=array (
);
$dictionary['Opportunity']['fields']['opportunity_type']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_deleted.php

 // created: 2022-11-25 17:16:02
$dictionary['Opportunity']['fields']['deleted']['default']=false;
$dictionary['Opportunity']['fields']['deleted']['audited']=false;
$dictionary['Opportunity']['fields']['deleted']['massupdate']=false;
$dictionary['Opportunity']['fields']['deleted']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['deleted']['comments']='Record deletion indicator';
$dictionary['Opportunity']['fields']['deleted']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['deleted']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['deleted']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['deleted']['unified_search']=false;
$dictionary['Opportunity']['fields']['deleted']['calculated']=false;
$dictionary['Opportunity']['fields']['deleted']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_campaign_id.php

 // created: 2022-11-25 17:16:33
$dictionary['Opportunity']['fields']['campaign_id']['name']='campaign_id';
$dictionary['Opportunity']['fields']['campaign_id']['comment']='Campaign that generated lead';
$dictionary['Opportunity']['fields']['campaign_id']['vname']='LBL_CAMPAIGN_ID';
$dictionary['Opportunity']['fields']['campaign_id']['rname']='id';
$dictionary['Opportunity']['fields']['campaign_id']['type']='id';
$dictionary['Opportunity']['fields']['campaign_id']['dbType']='id';
$dictionary['Opportunity']['fields']['campaign_id']['table']='campaigns';
$dictionary['Opportunity']['fields']['campaign_id']['isnull']=true;
$dictionary['Opportunity']['fields']['campaign_id']['module']='Campaigns';
$dictionary['Opportunity']['fields']['campaign_id']['reportable']=false;
$dictionary['Opportunity']['fields']['campaign_id']['massupdate']=false;
$dictionary['Opportunity']['fields']['campaign_id']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['campaign_id']['audited']=false;
$dictionary['Opportunity']['fields']['campaign_id']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_campaign_name.php

 // created: 2022-11-25 17:16:33
$dictionary['Opportunity']['fields']['campaign_name']['audited']=false;
$dictionary['Opportunity']['fields']['campaign_name']['massupdate']=false;
$dictionary['Opportunity']['fields']['campaign_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['campaign_name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['campaign_name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['campaign_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['campaign_name']['reportable']=false;
$dictionary['Opportunity']['fields']['campaign_name']['calculated']=false;
$dictionary['Opportunity']['fields']['campaign_name']['related_fields']=array (
  0 => 'campaign_id',
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_lead_source.php

 // created: 2022-11-25 17:17:09
$dictionary['Opportunity']['fields']['lead_source']['len']=100;
$dictionary['Opportunity']['fields']['lead_source']['audited']=false;
$dictionary['Opportunity']['fields']['lead_source']['massupdate']=false;
$dictionary['Opportunity']['fields']['lead_source']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['lead_source']['comments']='Source of the opportunity';
$dictionary['Opportunity']['fields']['lead_source']['importable']='false';
$dictionary['Opportunity']['fields']['lead_source']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['lead_source']['reportable']=false;
$dictionary['Opportunity']['fields']['lead_source']['calculated']=false;
$dictionary['Opportunity']['fields']['lead_source']['dependency']=false;
$dictionary['Opportunity']['fields']['lead_source']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_amount_usdollar.php

 // created: 2022-11-25 17:17:41
$dictionary['Opportunity']['fields']['amount_usdollar']['audited']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['massupdate']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['comments']='Formatted amount of the opportunity';
$dictionary['Opportunity']['fields']['amount_usdollar']['importable']='false';
$dictionary['Opportunity']['fields']['amount_usdollar']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['amount_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['amount_usdollar']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['amount_usdollar']['reportable']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['calculated']='1';
$dictionary['Opportunity']['fields']['amount_usdollar']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sales_stage.php

 // created: 2022-11-25 17:19:05
$dictionary['Opportunity']['fields']['sales_stage']['required']=false;
$dictionary['Opportunity']['fields']['sales_stage']['audited']=false;
$dictionary['Opportunity']['fields']['sales_stage']['massupdate']=false;
$dictionary['Opportunity']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['Opportunity']['fields']['sales_stage']['importable']='true';
$dictionary['Opportunity']['fields']['sales_stage']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['sales_stage']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['sales_stage']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sales_stage']['reportable']=false;
$dictionary['Opportunity']['fields']['sales_stage']['calculated']=false;
$dictionary['Opportunity']['fields']['sales_stage']['dependency']=false;
$dictionary['Opportunity']['fields']['sales_stage']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['sales_stage']['len']=100;
$dictionary['Opportunity']['fields']['sales_stage']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_best_case.php

 // created: 2022-11-25 17:19:54
$dictionary['Opportunity']['fields']['best_case']['audited']=false;
$dictionary['Opportunity']['fields']['best_case']['massupdate']=false;
$dictionary['Opportunity']['fields']['best_case']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['best_case']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['best_case']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['best_case']['calculated']='1';
$dictionary['Opportunity']['fields']['best_case']['formula']='rollupConditionalSum($revenuelineitems, "best_case", "sales_stage", forecastSalesStages(true, false))';
$dictionary['Opportunity']['fields']['best_case']['enforced']=true;
$dictionary['Opportunity']['fields']['best_case']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['best_case']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['best_case']['readonly']=true;
$dictionary['Opportunity']['fields']['best_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Opportunity']['fields']['best_case']['len']=26;
$dictionary['Opportunity']['fields']['best_case']['importable']='false';
$dictionary['Opportunity']['fields']['best_case']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_worst_case.php

 // created: 2022-11-25 17:20:13
$dictionary['Opportunity']['fields']['worst_case']['audited']=false;
$dictionary['Opportunity']['fields']['worst_case']['massupdate']=false;
$dictionary['Opportunity']['fields']['worst_case']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['worst_case']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['worst_case']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['worst_case']['calculated']='1';
$dictionary['Opportunity']['fields']['worst_case']['formula']='rollupConditionalSum($revenuelineitems, "worst_case", "sales_stage", forecastSalesStages(true, false))';
$dictionary['Opportunity']['fields']['worst_case']['enforced']=true;
$dictionary['Opportunity']['fields']['worst_case']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['worst_case']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['worst_case']['readonly']=true;
$dictionary['Opportunity']['fields']['worst_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Opportunity']['fields']['worst_case']['len']=26;
$dictionary['Opportunity']['fields']['worst_case']['importable']='false';
$dictionary['Opportunity']['fields']['worst_case']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_commit_stage.php

 // created: 2022-11-25 17:20:57
$dictionary['Opportunity']['fields']['commit_stage']['audited']=false;
$dictionary['Opportunity']['fields']['commit_stage']['massupdate']=false;
$dictionary['Opportunity']['fields']['commit_stage']['options']='';
$dictionary['Opportunity']['fields']['commit_stage']['comments']='Forecast commit ranges: Include, Likely, Omit etc.';
$dictionary['Opportunity']['fields']['commit_stage']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['commit_stage']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['commit_stage']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['commit_stage']['reportable']=false;
$dictionary['Opportunity']['fields']['commit_stage']['dependency']=false;
$dictionary['Opportunity']['fields']['commit_stage']['studio']=true;
$dictionary['Opportunity']['fields']['commit_stage']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['commit_stage']['calculated']=false;
$dictionary['Opportunity']['fields']['commit_stage']['len']=100;
$dictionary['Opportunity']['fields']['commit_stage']['visibility_grid']=array (
);
$dictionary['Opportunity']['fields']['commit_stage']['formula']='';
$dictionary['Opportunity']['fields']['commit_stage']['related_fields']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_forecasted_likely.php

 // created: 2022-11-25 17:21:22
$dictionary['Opportunity']['fields']['forecasted_likely']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['forecasted_likely']['options']='';
$dictionary['Opportunity']['fields']['forecasted_likely']['comments']='Rollup of included RLIs on the Opportunity';
$dictionary['Opportunity']['fields']['forecasted_likely']['importable']='false';
$dictionary['Opportunity']['fields']['forecasted_likely']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['forecasted_likely']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['forecasted_likely']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['forecasted_likely']['reportable']=false;
$dictionary['Opportunity']['fields']['forecasted_likely']['calculated']='1';
$dictionary['Opportunity']['fields']['forecasted_likely']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['forecasted_likely']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_renewal_parent_id.php

 // created: 2022-11-25 17:22:58
$dictionary['Opportunity']['fields']['renewal_parent_id']['name']='renewal_parent_id';
$dictionary['Opportunity']['fields']['renewal_parent_id']['vname']='LBL_PARENT_RENEWAL_OPPORTUNITY_ID';
$dictionary['Opportunity']['fields']['renewal_parent_id']['type']='id';
$dictionary['Opportunity']['fields']['renewal_parent_id']['required']=false;
$dictionary['Opportunity']['fields']['renewal_parent_id']['reportable']=false;
$dictionary['Opportunity']['fields']['renewal_parent_id']['audited']=false;
$dictionary['Opportunity']['fields']['renewal_parent_id']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_renewal_parent_name.php

 // created: 2022-11-25 17:22:58
$dictionary['Opportunity']['fields']['renewal_parent_name']['audited']=false;
$dictionary['Opportunity']['fields']['renewal_parent_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['renewal_parent_name']['importable']='false';
$dictionary['Opportunity']['fields']['renewal_parent_name']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['renewal_parent_name']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['renewal_parent_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['renewal_parent_name']['reportable']=false;
$dictionary['Opportunity']['fields']['renewal_parent_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_mkto_sync.php

 // created: 2022-11-25 17:23:34
$dictionary['Opportunity']['fields']['mkto_sync']['default']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['audited']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['massupdate']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['comments']='Should the Lead be synced to Marketo';
$dictionary['Opportunity']['fields']['mkto_sync']['importable']='false';
$dictionary['Opportunity']['fields']['mkto_sync']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['mkto_sync']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['mkto_sync']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['mkto_sync']['reportable']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['unified_search']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_mkto_id.php

 // created: 2022-11-25 17:24:10
$dictionary['Opportunity']['fields']['mkto_id']['len']='11';
$dictionary['Opportunity']['fields']['mkto_id']['audited']=false;
$dictionary['Opportunity']['fields']['mkto_id']['massupdate']=false;
$dictionary['Opportunity']['fields']['mkto_id']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['mkto_id']['comments']='Associated Marketo Lead ID';
$dictionary['Opportunity']['fields']['mkto_id']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['mkto_id']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['mkto_id']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['mkto_id']['reportable']=false;
$dictionary['Opportunity']['fields']['mkto_id']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['mkto_id']['calculated']=false;
$dictionary['Opportunity']['fields']['mkto_id']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['mkto_id']['min']=false;
$dictionary['Opportunity']['fields']['mkto_id']['max']=false;
$dictionary['Opportunity']['fields']['mkto_id']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_renewal.php

 // created: 2022-11-25 17:24:40
$dictionary['Opportunity']['fields']['renewal']['default']=false;
$dictionary['Opportunity']['fields']['renewal']['audited']=false;
$dictionary['Opportunity']['fields']['renewal']['massupdate']=false;
$dictionary['Opportunity']['fields']['renewal']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['renewal']['comments']='Indicates whether the opportunity is a renewal';
$dictionary['Opportunity']['fields']['renewal']['importable']='false';
$dictionary['Opportunity']['fields']['renewal']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['renewal']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['renewal']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['renewal']['reportable']=false;
$dictionary['Opportunity']['fields']['renewal']['unified_search']=false;
$dictionary['Opportunity']['fields']['renewal']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sync_key.php

 // created: 2022-11-25 17:25:13
$dictionary['Opportunity']['fields']['sync_key']['audited']=false;
$dictionary['Opportunity']['fields']['sync_key']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['sync_key']['importable']='false';
$dictionary['Opportunity']['fields']['sync_key']['reportable']=false;
$dictionary['Opportunity']['fields']['sync_key']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2022-11-25 17:26:23
$dictionary['Opportunity']['fields']['base_rate']['audited']=false;
$dictionary['Opportunity']['fields']['base_rate']['massupdate']=false;
$dictionary['Opportunity']['fields']['base_rate']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['base_rate']['importable']='false';
$dictionary['Opportunity']['fields']['base_rate']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['base_rate']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['base_rate']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['base_rate']['reportable']=false;
$dictionary['Opportunity']['fields']['base_rate']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['base_rate']['calculated']=false;
$dictionary['Opportunity']['fields']['base_rate']['readonly']=false;
$dictionary['Opportunity']['fields']['base_rate']['rows']='4';
$dictionary['Opportunity']['fields']['base_rate']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_is_escalated.php

 // created: 2022-11-25 17:26:56
$dictionary['Opportunity']['fields']['is_escalated']['default']=false;
$dictionary['Opportunity']['fields']['is_escalated']['audited']=false;
$dictionary['Opportunity']['fields']['is_escalated']['massupdate']=true;
$dictionary['Opportunity']['fields']['is_escalated']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['is_escalated']['comments']='Is this escalated?';
$dictionary['Opportunity']['fields']['is_escalated']['importable']='false';
$dictionary['Opportunity']['fields']['is_escalated']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['is_escalated']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['is_escalated']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['is_escalated']['reportable']=false;
$dictionary['Opportunity']['fields']['is_escalated']['unified_search']=false;
$dictionary['Opportunity']['fields']['is_escalated']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_next_step.php

 // created: 2022-11-25 17:29:48
$dictionary['Opportunity']['fields']['next_step']['audited']=false;
$dictionary['Opportunity']['fields']['next_step']['massupdate']=false;
$dictionary['Opportunity']['fields']['next_step']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['next_step']['comments']='The next step in the sales process';
$dictionary['Opportunity']['fields']['next_step']['importable']='false';
$dictionary['Opportunity']['fields']['next_step']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['next_step']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['next_step']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['next_step']['reportable']=false;
$dictionary['Opportunity']['fields']['next_step']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '0.74',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['next_step']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_service_start_date.php

 // created: 2022-11-25 17:32:00
$dictionary['Opportunity']['fields']['service_start_date']['audited']=false;
$dictionary['Opportunity']['fields']['service_start_date']['massupdate']=false;
$dictionary['Opportunity']['fields']['service_start_date']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['service_start_date']['comments']='Service start date field.';
$dictionary['Opportunity']['fields']['service_start_date']['importable']='true';
$dictionary['Opportunity']['fields']['service_start_date']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['service_start_date']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['service_start_date']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['service_start_date']['calculated']=false;
$dictionary['Opportunity']['fields']['service_start_date']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['service_start_date']['reportable']=false;
$dictionary['Opportunity']['fields']['service_start_date']['related_fields']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_ai_opp_conv_score_enum.php

 // created: 2022-11-25 17:33:48
$dictionary['Opportunity']['fields']['ai_opp_conv_score_enum']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['ai_opp_conv_score_enum']['importable']='false';
$dictionary['Opportunity']['fields']['ai_opp_conv_score_enum']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['ai_opp_conv_score_enum']['reportable']=false;
$dictionary['Opportunity']['fields']['ai_opp_conv_score_enum']['calculated']=false;
$dictionary['Opportunity']['fields']['ai_opp_conv_score_enum']['dependency']=false;
$dictionary['Opportunity']['fields']['ai_opp_conv_score_enum']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_name.php

 // created: 2022-12-29 20:29:56
$dictionary['Opportunity']['fields']['name']['audited']=false;
$dictionary['Opportunity']['fields']['name']['massupdate']=false;
$dictionary['Opportunity']['fields']['name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['name']['comments']='Name of the opportunity';
$dictionary['Opportunity']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.65',
  'searchable' => true,
);
$dictionary['Opportunity']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_description.php

 // created: 2022-12-29 20:31:24
$dictionary['Opportunity']['fields']['description']['audited']=false;
$dictionary['Opportunity']['fields']['description']['massupdate']=false;
$dictionary['Opportunity']['fields']['description']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['description']['comments']='Full text of the note';
$dictionary['Opportunity']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['description']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.59',
  'searchable' => true,
);
$dictionary['Opportunity']['fields']['description']['calculated']=false;
$dictionary['Opportunity']['fields']['description']['readonly']=true;
$dictionary['Opportunity']['fields']['description']['rows']='6';
$dictionary['Opportunity']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_estadocotizacion_c.php

 // created: 2022-12-29 20:35:04
$dictionary['Opportunity']['fields']['sasa_estadocotizacion_c']['labelValue']='Estado de la Cotización';
$dictionary['Opportunity']['fields']['sasa_estadocotizacion_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_estadocotizacion_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_estadocotizacion_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_estadocotizacion_c']['readonly_formula']='';
$dictionary['Opportunity']['fields']['sasa_estadocotizacion_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_asesor_c.php

 // created: 2022-12-29 20:35:31
$dictionary['Opportunity']['fields']['sasa_asesor_c']['labelValue']='Asesor';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_asesor_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_numasesor_c.php

 // created: 2022-12-29 20:35:55
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['labelValue']='No. documento del asesor';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_numcotizacion_c.php

 // created: 2022-12-29 20:36:38
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['labelValue']='Número de la cotización';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_terminocotizacion_c.php

 // created: 2022-12-29 20:39:54
$dictionary['Opportunity']['fields']['sasa_terminocotizacion_c']['labelValue']='Término de cotización';
$dictionary['Opportunity']['fields']['sasa_terminocotizacion_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_terminocotizacion_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_terminocotizacion_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_terminocotizacion_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_terminocotizacion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_anomodelo_c.php

 // created: 2022-12-29 20:40:19
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['labelValue']='Año Modelo';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_color_c.php

 // created: 2022-12-29 20:40:46
$dictionary['Opportunity']['fields']['sasa_color_c']['labelValue']='Color de Vehículo';
$dictionary['Opportunity']['fields']['sasa_color_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_color_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_color_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_color_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_color_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_color_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_tapiceria_c.php

 // created: 2022-12-29 20:41:11
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['labelValue']='Tapicería';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_tipodecaja_c.php

 // created: 2022-12-29 20:41:39
$dictionary['Opportunity']['fields']['sasa_tipodecaja_c']['labelValue']='Tipo de caja';
$dictionary['Opportunity']['fields']['sasa_tipodecaja_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_tipodecaja_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipodecaja_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_tipodecaja_c']['readonly_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipodecaja_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_tipodeservicio_c.php

 // created: 2022-12-29 20:42:08
$dictionary['Opportunity']['fields']['sasa_tipodeservicio_c']['labelValue']='Tipo de servicio';
$dictionary['Opportunity']['fields']['sasa_tipodeservicio_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_tipodeservicio_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipodeservicio_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_tipodeservicio_c']['readonly_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipodeservicio_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_modelo_c.php

 // created: 2022-12-29 20:42:31
$dictionary['Opportunity']['fields']['sasa_modelo_c']['labelValue']='Modelo comercial';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_modelo_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_tipocotizacion_c.php

 // created: 2022-12-29 20:43:15
$dictionary['Opportunity']['fields']['sasa_tipocotizacion_c']['labelValue']='Tipo de cotización';
$dictionary['Opportunity']['fields']['sasa_tipocotizacion_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_tipocotizacion_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipocotizacion_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_tipocotizacion_c']['readonly_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipocotizacion_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_chasis_c.php

 // created: 2022-12-29 20:45:02
$dictionary['Opportunity']['fields']['sasa_chasis_c']['labelValue']='Chasis del Vehículo';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_chasis_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_estadopedidorep_c.php

 // created: 2022-12-29 20:46:40
$dictionary['Opportunity']['fields']['sasa_estadopedidorep_c']['labelValue']='Estado del pedido repuestos';
$dictionary['Opportunity']['fields']['sasa_estadopedidorep_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_estadopedidorep_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_estadopedidorep_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_estadopedidorep_c']['readonly_formula']='';
$dictionary['Opportunity']['fields']['sasa_estadopedidorep_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_estadopedido_c.php

 // created: 2022-12-29 20:47:02
$dictionary['Opportunity']['fields']['sasa_estadopedido_c']['labelValue']='Estado del pedido (V. Nuevos - Maq)';
$dictionary['Opportunity']['fields']['sasa_estadopedido_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_estadopedido_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_estadopedido_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_estadopedido_c']['readonly_formula']='';
$dictionary['Opportunity']['fields']['sasa_estadopedido_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_numeropedido_c.php

 // created: 2022-12-29 20:47:32
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['labelValue']='Número del pedido';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_cotizacionprepagado_c.php

 // created: 2023-01-21 18:34:13
$dictionary['Opportunity']['fields']['sasa_cotizacionprepagado_c']['labelValue']='Cotización prepagado';
$dictionary['Opportunity']['fields']['sasa_cotizacionprepagado_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_cotizacionprepagado_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_cotizacionprepagado_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_cotizacionprepagado_c']['readonly_formula']='';
$dictionary['Opportunity']['fields']['sasa_cotizacionprepagado_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_tipocombustible_c.php

 // created: 2023-01-23 21:04:40
$dictionary['Opportunity']['fields']['sasa_tipocombustible_c']['labelValue']='Tipo de combustible';
$dictionary['Opportunity']['fields']['sasa_tipocombustible_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_tipocombustible_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipocombustible_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_tipocombustible_c']['readonly_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipocombustible_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_sucursal_c.php

 // created: 2023-01-25 23:27:44
$dictionary['Opportunity']['fields']['sasa_sucursal_c']['labelValue']='Sucursal';
$dictionary['Opportunity']['fields']['sasa_sucursal_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_sucursal_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_sucursal_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_sucursal_c']['readonly_formula']='';
$dictionary['Opportunity']['fields']['sasa_sucursal_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_iva_c.php

 // created: 2023-01-25 23:31:31
$dictionary['Opportunity']['fields']['sasa_iva_c']['labelValue']='IVA';
$dictionary['Opportunity']['fields']['sasa_iva_c']['formula']='related($revenuelineitems,"sasa_iva_c")';
$dictionary['Opportunity']['fields']['sasa_iva_c']['enforced']='false';
$dictionary['Opportunity']['fields']['sasa_iva_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_iva_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Opportunity']['fields']['sasa_iva_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_iva_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_subtotal_c.php

 // created: 2023-01-25 23:33:36
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['labelValue']='Subtotal';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['formula']='related($revenuelineitems,"subtotal")';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['enforced']='false';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_amount.php

 // created: 2023-01-25 23:37:16
$dictionary['Opportunity']['fields']['amount']['required']=false;
$dictionary['Opportunity']['fields']['amount']['audited']=false;
$dictionary['Opportunity']['fields']['amount']['massupdate']=false;
$dictionary['Opportunity']['fields']['amount']['comments']='Unconverted amount of the opportunity';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['amount']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['amount']['calculated']='1';
$dictionary['Opportunity']['fields']['amount']['formula']='rollupSum($revenuelineitems,"list_price")';
$dictionary['Opportunity']['fields']['amount']['enforced']=true;
$dictionary['Opportunity']['fields']['amount']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['amount']['readonly']=true;
$dictionary['Opportunity']['fields']['amount']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Opportunity']['fields']['amount']['importable']='false';
$dictionary['Opportunity']['fields']['amount']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['amount']['options']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_date_closed.php

 // created: 2023-02-01 22:41:23
$dictionary['Opportunity']['fields']['date_closed']['required']=false;
$dictionary['Opportunity']['fields']['date_closed']['audited']=false;
$dictionary['Opportunity']['fields']['date_closed']['massupdate']=false;
$dictionary['Opportunity']['fields']['date_closed']['comments']='Expected or actual date the oppportunity will close';
$dictionary['Opportunity']['fields']['date_closed']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['date_closed']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['date_closed']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['date_closed']['calculated']='1';
$dictionary['Opportunity']['fields']['date_closed']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['date_closed']['importable']='false';
$dictionary['Opportunity']['fields']['date_closed']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['date_closed']['formula']='addDays($date_entered,$sasa_vigencia_c)';
$dictionary['Opportunity']['fields']['date_closed']['enforced']=true;
$dictionary['Opportunity']['fields']['date_closed']['options']='';
$dictionary['Opportunity']['fields']['date_closed']['full_text_search']=array (
);
$dictionary['Opportunity']['fields']['date_closed']['related_fields']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_modelotasa_c.php

 // created: 2023-02-06 22:01:17
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['labelValue']='Modelo TASA';
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_cotsigru_c.php

 // created: 2023-02-07 20:45:04
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['labelValue']='URL Cotización Sigru';
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/customer_journey_parent.php

// created: 2023-02-08 11:09:23
VardefManager::createVardef('Opportunities', 'Opportunity', [
                                'customer_journey_parent',
                        ]);
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_tipocaja_c.php

 // created: 2023-03-08 20:47:02
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['labelValue']='Tipo de caja';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_unidades_c.php

 // created: 2023-03-08 22:09:59
$dictionary['Opportunity']['fields']['sasa_unidades_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['sasa_unidades_c']['labelValue']='Cantidad de unidades Cotizadas';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_unidades_c']['calculated']='1';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['formula']='rollupSum($revenuelineitems,"quantity")';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['enforced']='1';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_termino2_c.php

 // created: 2023-03-15 14:04:45
$dictionary['Opportunity']['fields']['sasa_termino2_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['sasa_termino2_c']['labelValue']='sasa termino2 c';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_termino2_c']['calculated']='true';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['formula']='daysUntil(addDays($date_entered,$sasa_vigencia_c))';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['enforced']='true';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_vigencia_c.php

 // created: 2023-03-15 14:07:18
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['labelValue']='Días de vigencia';
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['required_formula']='';

 
?>

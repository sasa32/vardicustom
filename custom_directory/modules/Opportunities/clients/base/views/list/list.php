<?php
$viewdefs['Opportunities'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_LIST_OPPORTUNITY_NAME',
                'enabled' => true,
                'default' => true,
                'related_fields' => 
                array (
                  0 => 'total_revenue_line_items',
                  1 => 'closed_revenue_line_items',
                  2 => 'included_revenue_line_items',
                ),
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_numcotizacion_c',
                'label' => 'LBL_SASA_NUMCOTIZACION_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'account_name',
                'label' => 'LBL_LIST_ACCOUNT_NAME',
                'enabled' => true,
                'default' => true,
                'related_fields' => 
                array (
                  0 => 'account_id',
                ),
                'id' => 'ACCOUNT_ID',
                'link' => true,
                'sortable' => true,
              ),
              3 => 
              array (
                'name' => 'sasa_tipocotizacion_c',
                'label' => 'LBL_SASA_TIPOCOTIZACION_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'amount',
                'label' => 'LBL_LIKELY',
                'enabled' => true,
                'default' => true,
                'related_fields' => 
                array (
                  0 => 'amount',
                  1 => 'currency_id',
                  2 => 'base_rate',
                ),
                'currency_format' => true,
                'type' => 'currency',
                'currency_field' => 'currency_id',
                'base_rate_field' => 'base_rate',
              ),
              5 => 
              array (
                'name' => 'date_closed',
                'label' => 'LBL_DATE_CLOSED',
                'enabled' => true,
                'default' => true,
                'type' => 'date-cascade',
                'disable_field' => 
                array (
                  0 => 'total_revenue_line_items',
                  1 => 'closed_revenue_line_items',
                ),
              ),
              6 => 
              array (
                'name' => 'sasa_unidad_de_negocio_opportunities_1_name',
                'label' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_NAME_FIELD_TITLE',
                'enabled' => true,
                'id' => 'SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1SASA_UNIDAD_DE_NEGOCIO_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_LIST_ASSIGNED_USER',
                'id' => 'ASSIGNED_USER_ID',
                'enabled' => true,
                'default' => true,
                'sortable' => true,
              ),
              8 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              9 => 
              array (
                'name' => 'date_modified',
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'sales_stage',
                'type' => 'enum-cascade',
                'label' => 'LBL_LIST_SALES_STAGE',
                'enabled' => true,
                'default' => true,
                'disable_field' => 
                array (
                  0 => 'total_revenue_line_items',
                  1 => 'closed_revenue_line_items',
                ),
              ),
              11 => 
              array (
                'name' => 'sasa_unidades_c',
                'label' => 'LBL_SASA_UNIDADES_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'sasa_estadocotizacion_c',
                'label' => 'LBL_SASA_ESTADOCOTIZACION_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'sasa_puntos_de_ventas_opportunities_1_name',
                'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_NAME_FIELD_TITLE',
                'enabled' => true,
                'id' => 'SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1SASA_PUNTOS_DE_VENTAS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              14 => 
              array (
                'name' => 'sasa_vehiculos_opportunities_1_name',
                'label' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_NAME_FIELD_TITLE',
                'enabled' => true,
                'id' => 'SASA_VEHICULOS_OPPORTUNITIES_1SASA_VEHICULOS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              15 => 
              array (
                'name' => 'sasa_modelotasa_c',
                'label' => 'LBL_SASA_MODELOTASA_C',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              16 => 
              array (
                'name' => 'sasa_asesor_c',
                'label' => 'LBL_SASA_ASESOR_C',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              17 => 
              array (
                'name' => 'sasa_vigencia_c',
                'label' => 'LBL_SASA_VIGENCIA_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              18 => 
              array (
                'name' => 'sasa_cotizacionprepagado_c',
                'label' => 'LBL_SASA_COTIZACIONPREPAGADO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              19 => 
              array (
                'name' => 'sasa_cotsigru_c',
                'label' => 'LBL_SASA_COTSIGRU_C',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              20 => 
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'default' => false,
                'readonly' => true,
                'sortable' => true,
              ),
              21 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'default' => false,
                'readonly' => true,
                'sortable' => true,
              ),
              22 => 
              array (
                'name' => 'team_name',
                'type' => 'teamset',
                'label' => 'LBL_LIST_TEAM',
                'enabled' => true,
                'default' => false,
              ),
              23 => 
              array (
                'name' => 'service_start_date',
                'type' => 'date-cascade',
                'label' => 'LBL_SERVICE_START_DATE',
                'disable_field' => 'service_open_revenue_line_items',
                'related_fields' => 
                array (
                  0 => 'service_open_revenue_line_items',
                ),
                'default' => false,
                'enabled' => true,
              ),
              24 => 
              array (
                'name' => 'service_duration',
                'type' => 'fieldset-cascade',
                'label' => 'LBL_SERVICE_DURATION',
                'inline' => true,
                'show_child_labels' => false,
                'css_class' => 'service-duration-field',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'service_duration_value',
                    'label' => 'LBL_SERVICE_DURATION_VALUE',
                  ),
                  1 => 
                  array (
                    'name' => 'service_duration_unit',
                    'label' => 'LBL_SERVICE_DURATION_UNIT',
                  ),
                ),
                'orderBy' => 'service_duration_unit',
                'related_fields' => 
                array (
                  0 => 'service_duration_value',
                  1 => 'service_duration_unit',
                  2 => 'service_open_flex_duration_rlis',
                ),
                'disable_field' => 'service_open_flex_duration_rlis',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);

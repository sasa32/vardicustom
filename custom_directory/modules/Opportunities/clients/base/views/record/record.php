<?php
$viewdefs['Opportunities'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'escalate-action',
                'event' => 'button:escalate_button:click',
                'name' => 'escalate_button',
                'label' => 'LBL_ESCALATE_BUTTON_LABEL',
                'acl_action' => 'create',
              ),
              2 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              5 => 
              array (
                'type' => 'divider',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'Opportunities',
                'acl_action' => 'create',
              ),
              8 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:historical_summary_button:click',
                'name' => 'historical_summary_button',
                'label' => 'LBL_HISTORICAL_SUMMARY',
                'acl_action' => 'view',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              10 => 
              array (
                'type' => 'divider',
              ),
              11 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'related_fields' => 
                array (
                  0 => 'total_revenue_line_items',
                  1 => 'closed_revenue_line_items',
                  2 => 'included_revenue_line_items',
                ),
              ),
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              4 => 
              array (
                'name' => 'renewal',
                'type' => 'renewal',
                'dismiss_label' => true,
              ),
              5 => 
              array (
                'name' => 'is_escalated',
                'type' => 'badge',
                'badge_label' => 'LBL_ESCALATED',
                'warning_level' => 'important',
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labels' => true,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'sasa_numcotizacion_c',
                'label' => 'LBL_SASA_NUMCOTIZACION_C',
              ),
              1 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_cotizacionprepagado_c',
                'label' => 'LBL_SASA_COTIZACIONPREPAGADO_C',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tipocotizacion_c',
                'label' => 'LBL_SASA_TIPOCOTIZACION_C',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'sasa_estadocotizacion_c',
                'label' => 'LBL_SASA_ESTADOCOTIZACION_C',
              ),
              4 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_vigencia_c',
                'label' => 'LBL_SASA_VIGENCIA_C',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'sasa_termino2_c',
                'label' => 'LBL_SASA_TERMINO2_C',
              ),
              6 => 
              array (
                'name' => 'date_closed',
                'type' => 'date-cascade',
                'label' => 'LBL_LIST_DATE_CLOSED',
                'disable_field' => 
                array (
                  0 => 'total_revenue_line_items',
                  1 => 'closed_revenue_line_items',
                ),
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'sasa_terminocotizacion_c',
                'label' => 'LBL_SASA_TERMINOCOTIZACION_C',
              ),
              8 => 
              array (
                'name' => 'account_name',
                'related_fields' => 
                array (
                  0 => 'account_id',
                ),
                'span' => 12,
              ),
              9 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              10 => 
              array (
                'name' => 'sasa_unidad_de_negocio_opportunities_1_name',
              ),
              11 => 
              array (
                'name' => 'sasa_vehiculos_opportunities_1_name',
              ),
              12 => 
              array (
                'readonly' => false,
                'name' => 'sasa_anomodelo_c',
                'label' => 'LBL_SASA_ANOMODELO_C',
              ),
              13 => 
              array (
                'readonly' => false,
                'name' => 'sasa_modelo_c',
                'label' => 'LBL_SASA_MODELO_C',
              ),
              14 => 
              array (
                'readonly' => false,
                'name' => 'sasa_unidades_c',
                'label' => 'LBL_SASA_UNIDADES_C',
              ),
              15 => 
              array (
                'readonly' => false,
                'name' => 'sasa_modelotasa_c',
                'label' => 'LBL_SASA_MODELOTASA_C',
              ),
              16 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipocaja_c',
                'label' => 'LBL_SASA_TIPOCAJA_C',
              ),
              17 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tipodeservicio_c',
                'label' => 'LBL_SASA_TIPODESERVICIO_C',
              ),
              18 => 
              array (
                'readonly' => false,
                'name' => 'sasa_color_c',
                'label' => 'LBL_SASA_COLOR_C',
              ),
              19 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tapiceria_c',
                'label' => 'LBL_SASA_TAPICERIA_C',
              ),
              20 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipocombustible_c',
                'label' => 'LBL_SASA_TIPOCOMBUSTIBLE_C',
              ),
              21 => 
              array (
                'readonly' => false,
                'name' => 'sasa_chasis_c',
                'label' => 'LBL_SASA_CHASIS_C',
              ),
              22 => 
              array (
                'name' => 'amount',
                'type' => 'currency',
                'label' => 'LBL_LIKELY',
                'related_fields' => 
                array (
                  0 => 'amount',
                  1 => 'currency_id',
                  2 => 'base_rate',
                ),
                'currency_field' => 'currency_id',
                'base_rate_field' => 'base_rate',
                'span' => 12,
              ),
              23 => 
              array (
                'readonly' => false,
                'name' => 'sasa_asesor_c',
                'label' => 'LBL_SASA_ASESOR_C',
                'span' => 12,
              ),
              24 => 
              array (
                'name' => 'sasa_puntos_de_ventas_opportunities_1_name',
              ),
              25 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_sucursal_c',
                'label' => 'LBL_SASA_SUCURSAL_C',
              ),
              26 => 
              array (
                'readonly' => false,
                'name' => 'sasa_estadopedido_c',
                'label' => 'LBL_SASA_ESTADOPEDIDO_C',
              ),
              27 => 
              array (
                'readonly' => false,
                'name' => 'sasa_estadopedidorep_c',
                'label' => 'LBL_SASA_ESTADOPEDIDOREP_C',
              ),
              28 => 
              array (
                'readonly' => false,
                'name' => 'sasa_numeropedido_c',
                'label' => 'LBL_SASA_NUMEROPEDIDO_C',
              ),
              29 => 
              array (
                'readonly' => false,
                'name' => 'sasa_cotsigru_c',
                'label' => 'LBL_SASA_COTSIGRU_C',
              ),
              30 => 
              array (
                'name' => 'commentlog',
                'label' => 'LBL_COMMENTLOG',
                'displayParams' => 
                array (
                  'type' => 'commentlog',
                  'fields' => 
                  array (
                    0 => 'entry',
                    1 => 'date_entered',
                    2 => 'created_by_name',
                  ),
                  'max_num' => 100,
                ),
                'span' => 12,
              ),
              31 => 
              array (
                'name' => 'forecasted_likely',
                'comment' => 'Rollup of included RLIs on the Opportunity',
                'readonly' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_FORECASTED_LIKELY',
                'span' => 6,
              ),
              32 => 
              array (
                'name' => 'lost',
                'comment' => 'Rollup of lost RLIs on the Opportunity',
                'readonly' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_LOST',
                'span' => 6,
              ),
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_RECORD_SHOWMORE',
            'hide' => true,
            'labelsOnTop' => true,
            'placeholders' => true,
            'columns' => 2,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              1 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              2 => 
              array (
                'name' => 'tag',
                'span' => 6,
              ),
              3 => 
              array (
                'name' => 'team_name',
                'span' => 6,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);

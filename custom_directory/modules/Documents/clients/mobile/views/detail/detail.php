<?php
// created: 2023-02-08 11:06:10
$viewdefs['Documents']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
      1 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_DOC_NAME',
        ),
        1 => 
        array (
          'name' => 'template_type',
          'label' => 'LBL_TEMPLATE_TYPE',
        ),
        2 => 
        array (
          'name' => 'sasa_codigo_c',
          'label' => 'LBL_SASA_CODIGO_C',
        ),
        3 => 'status_id',
        4 => 
        array (
          'name' => 'filename',
          'comment' => 'The filename of the document attachment',
          'studio' => true,
          'label' => 'LBL_FILENAME',
        ),
        5 => 
        array (
          'name' => 'sasa_url_formt_hd_c',
          'label' => 'LBL_SASA_URL_FORMT_HD_C',
        ),
        6 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        7 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        8 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
      ),
    ),
  ),
);
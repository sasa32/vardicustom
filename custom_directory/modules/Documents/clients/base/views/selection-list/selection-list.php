<?php
// created: 2023-02-08 11:06:10
$viewdefs['Documents']['base']['view']['selection-list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'default' => true,
          'enabled' => true,
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_nombrecompania_c',
          'label' => 'LBL_SASA_NOMBRECOMPANIA_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'template_type',
          'label' => 'LBL_TEMPLATE_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_codigo_c',
          'label' => 'LBL_SASA_CODIGO_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'status_id',
          'label' => 'LBL_DOC_STATUS',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
);
<?php
// created: 2023-02-08 11:06:10
$viewdefs['Documents']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'document_name',
          'label' => 'LBL_LIST_DOCUMENT_NAME',
          'enabled' => true,
          'default' => true,
          'link' => true,
          'type' => 'name',
          'width' => '20',
        ),
        1 => 
        array (
          'name' => 'template_type',
          'label' => 'LBL_TEMPLATE_TYPE',
          'enabled' => true,
          'default' => true,
          'width' => '10',
        ),
        2 => 
        array (
          'name' => 'sasa_nombrecompania_c',
          'label' => 'LBL_SASA_NOMBRECOMPANIA_C',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
          'width' => '10',
        ),
        3 => 
        array (
          'name' => 'sasa_codigo_c',
          'label' => 'LBL_SASA_CODIGO_C',
          'enabled' => true,
          'default' => true,
          'width' => '10',
        ),
        4 => 
        array (
          'name' => 'sasa_url_formt_hd_c',
          'label' => 'LBL_SASA_URL_FORMT_HD_C',
          'enabled' => true,
          'default' => true,
          'width' => '10',
        ),
        5 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
          'width' => '10',
        ),
        6 => 
        array (
          'name' => 'date_modified',
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
          'width' => '10',
        ),
        7 => 
        array (
          'name' => 'subcategory_id',
          'label' => 'LBL_LIST_SUBCATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'last_rev_create_date',
          'label' => 'LBL_LIST_LAST_REV_DATE',
          'enabled' => true,
          'default' => true,
          'list_display_type' => 'datetime',
          'readonly' => true,
        ),
        9 => 
        array (
          'name' => 'exp_date',
          'label' => 'LBL_LIST_EXP_DATE',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_USER',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'is_shared',
          'label' => 'LBL_IS_SHARED',
          'default' => false,
          'enabled' => true,
          'selected' => false,
        ),
      ),
    ),
  ),
);
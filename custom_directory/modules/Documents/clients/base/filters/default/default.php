<?php
// created: 2021-04-20 16:34:59
$viewdefs['Documents']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'document_name' => 
    array (
    ),
    'template_type' => 
    array (
    ),
    'sasa_nombrecompania_c' => 
    array (
      'readonly_formula' => '',
      'readonly' => false,
      'type' => 'varchar',
      'width' => '10',
      'default' => true,
      'name' => 'sasa_nombrecompania_c',
      'vname' => 'LBL_SASA_NOMBRECOMPANIA_C',
    ),
    'sasa_codigo_c' => 
    array (
      'type' => 'varchar',
      'width' => '10',
      'default' => true,
      'name' => 'sasa_codigo_c',
      'vname' => 'LBL_SASA_CODIGO_C',
    ),
    'status_id' => 
    array (
    ),
    'sasa_url_formt_hd_c' => 
    array (
      'type' => 'url',
      'default' => true,
      'width' => '10',
      'name' => 'sasa_url_formt_hd_c',
      'vname' => 'LBL_SASA_URL_FORMT_HD_C',
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);
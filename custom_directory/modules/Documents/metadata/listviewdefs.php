<?php
// created: 2023-04-25 06:21:32
$listViewDefs['Documents'] = array (
  'document_name' => 
  array (
    'width' => '20',
    'label' => 'LBL_DOCUMENT_NAME',
    'link' => true,
    'default' => true,
    'bold' => true,
  ),
  'template_type' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TEMPLATE_TYPE',
    'width' => '10',
    'default' => true,
  ),
  'sasa_codigo_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_CODIGO_C',
    'width' => '10',
    'default' => true,
  ),
  'status_id' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_DOC_STATUS',
    'width' => '10',
  ),
  'filename' => 
  array (
    'width' => '20',
    'label' => 'LBL_FILENAME',
    'link' => true,
    'default' => true,
    'bold' => false,
    'displayParams' => 
    array (
      'module' => 'Documents',
    ),
    'sortable' => false,
    'related_fields' => 
    array (
      0 => 'document_revision_id',
      1 => 'doc_id',
      2 => 'doc_type',
      3 => 'doc_url',
    ),
  ),
  'sasa_url_formt_hd_c' => 
  array (
    'type' => 'url',
    'default' => true,
    'label' => 'LBL_SASA_URL_FORMT_HD_C',
    'width' => '10',
  ),
  'date_entered' => 
  array (
    'width' => '10',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10',
    'default' => true,
  ),
);
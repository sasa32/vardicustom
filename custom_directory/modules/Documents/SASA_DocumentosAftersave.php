<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_DocumentosAftersave
{
	function after_save($bean, $event, $arguments)
	{	
		/*
		Logic Hook para poblar automaticamente la esctutura geografica de relacion
		*/
		try{

			if (!isset($bean->logic_hooks_documentos_after_save_ignore_update) || $bean->logic_hooks_documentos_after_save_ignore_update === false){//antiloop
				$bean->logic_hooks_documentos_after_save_ignore_update = true;//antiloop

				if (!empty($bean->sasa_codcompania_c)){
					$resultcompania = $GLOBALS['db']->query("SELECT id FROM sasa_companias INNER JOIN sasa_companias_cstm ON sasa_companias.id=sasa_companias_cstm.id_c WHERE sasa_companias_cstm.sasa_cod_c='{$bean->sasa_codcompania_c}'");
					$sasa_cod_compania =  $GLOBALS['db']->fetchByAssoc($resultcompania);	
					
					if ($bean->load_relationship('documents_sasa_companias_1')){
						$relatedBeans = $bean->documents_sasa_companias_1->add($sasa_cod_compania["id"]);
						$relatedBeans = $bean->documents_sasa_companias_1->get();
						$GLOBALS['log']->security("Compañias ".print_r($relatedBeans,true));
						$bean->documents_sasa_companias_1 ="";

						foreach ($relatedBeans as $key => $value) {
							$Compania = BeanFactory::getBean('sasa_Companias', $value, array('disable_row_level_security' => true));
							$nombrecompanias .= $Compania->name."|";
							$bean->sasa_nombrecompania_c = $nombrecompanias;
						}
					}
				}

				//Varible para identificar si la nota ya tiene una URL
				$urlvisualizador= "http://templatephp";
				$pos = strpos($bean->sasa_url_formt_hd_c, $urlvisualizador);

				if ($pos === false) {
					//sasa_url_formt_hd_c no tiene una URL de visualizador establecida (http://templatephp)
					//Si no tiene se concatena el id de la nota enviado por SIGRU con la URL habilitada para el visualizador de adjuntos SIGRU
					$bean->sasa_url_formt_hd_c = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$bean->sasa_url_formt_hd_c;
				}

				$bean->save();

			}
							
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Lineas de Vehiculos: ".$e->getMessage()); 
		}
	}
}
?>
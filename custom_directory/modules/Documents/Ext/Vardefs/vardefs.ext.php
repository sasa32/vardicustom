<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/documents_sasa_habeas_data_1_Documents.php

// created: 2020-07-29 16:43:27
$dictionary["Document"]["fields"]["documents_sasa_habeas_data_1"] = array (
  'name' => 'documents_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'documents_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'vname' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'documents_sasa_habeas_data_1documents_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/rli_link_workflow.php

$dictionary['Document']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_template_type.php

 // created: 2020-07-29 11:11:56
$dictionary['Document']['fields']['template_type']['audited']=true;
$dictionary['Document']['fields']['template_type']['massupdate']=false;
$dictionary['Document']['fields']['template_type']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['template_type']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['template_type']['merge_filter']='disabled';
$dictionary['Document']['fields']['template_type']['reportable']=true;
$dictionary['Document']['fields']['template_type']['calculated']=false;
$dictionary['Document']['fields']['template_type']['dependency']=false;
$dictionary['Document']['fields']['template_type']['options']='template_type_list';

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_subcategory_id.php

 // created: 2020-07-29 09:29:39
$dictionary['Document']['fields']['subcategory_id']['audited']=false;
$dictionary['Document']['fields']['subcategory_id']['massupdate']=false;
$dictionary['Document']['fields']['subcategory_id']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['subcategory_id']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['subcategory_id']['merge_filter']='disabled';
$dictionary['Document']['fields']['subcategory_id']['reportable']=false;
$dictionary['Document']['fields']['subcategory_id']['calculated']=false;
$dictionary['Document']['fields']['subcategory_id']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_status_id.php

 // created: 2020-07-29 09:32:38
$dictionary['Document']['fields']['status_id']['default']='';
$dictionary['Document']['fields']['status_id']['audited']=true;
$dictionary['Document']['fields']['status_id']['massupdate']=true;
$dictionary['Document']['fields']['status_id']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['status_id']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['status_id']['merge_filter']='disabled';
$dictionary['Document']['fields']['status_id']['reportable']=true;
$dictionary['Document']['fields']['status_id']['calculated']=false;
$dictionary['Document']['fields']['status_id']['dependency']=false;
$dictionary['Document']['fields']['status_id']['options']='sasa_estado_c_list';

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_filename.php

 // created: 2020-07-29 11:04:42
$dictionary['Document']['fields']['filename']['audited']=true;
$dictionary['Document']['fields']['filename']['importable']=true;
$dictionary['Document']['fields']['filename']['reportable']=true;
$dictionary['Document']['fields']['filename']['studio']=true;
 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_exp_date.php

 // created: 2020-07-29 08:53:10
$dictionary['Document']['fields']['exp_date']['audited']=false;
$dictionary['Document']['fields']['exp_date']['massupdate']=false;
$dictionary['Document']['fields']['exp_date']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['exp_date']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['exp_date']['merge_filter']='disabled';
$dictionary['Document']['fields']['exp_date']['calculated']=false;
$dictionary['Document']['fields']['exp_date']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_doc_type.php

 // created: 2020-07-29 09:29:09
$dictionary['Document']['fields']['doc_type']['len']=100;
$dictionary['Document']['fields']['doc_type']['audited']=false;
$dictionary['Document']['fields']['doc_type']['comments']='Document type (ex: Google, box.net, IBM SmartCloud)';
$dictionary['Document']['fields']['doc_type']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['doc_type']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['doc_type']['merge_filter']='disabled';
$dictionary['Document']['fields']['doc_type']['calculated']=false;
$dictionary['Document']['fields']['doc_type']['dependency']=false;
$dictionary['Document']['fields']['doc_type']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_document_name.php

 // created: 2020-07-29 11:04:42
$dictionary['Document']['fields']['document_name']['audited']=true;
$dictionary['Document']['fields']['document_name']['massupdate']=false;
$dictionary['Document']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['document_name']['merge_filter']='disabled';
$dictionary['Document']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['Document']['fields']['document_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_description.php

 // created: 2020-07-29 11:11:37
$dictionary['Document']['fields']['description']['audited']=true;
$dictionary['Document']['fields']['description']['massupdate']=false;
$dictionary['Document']['fields']['description']['comments']='Full text of the note';
$dictionary['Document']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['description']['merge_filter']='disabled';
$dictionary['Document']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.61',
  'searchable' => true,
);
$dictionary['Document']['fields']['description']['calculated']=false;
$dictionary['Document']['fields']['description']['rows']='6';
$dictionary['Document']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2020-07-29 11:11:27
$dictionary['Document']['fields']['date_modified']['audited']=true;
$dictionary['Document']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Document']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Document']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Document']['fields']['date_modified']['calculated']=false;
$dictionary['Document']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_category_id.php

 // created: 2020-07-29 09:28:49
$dictionary['Document']['fields']['category_id']['audited']=false;
$dictionary['Document']['fields']['category_id']['massupdate']=false;
$dictionary['Document']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['category_id']['merge_filter']='disabled';
$dictionary['Document']['fields']['category_id']['reportable']=false;
$dictionary['Document']['fields']['category_id']['calculated']=false;
$dictionary['Document']['fields']['category_id']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_active_date.php

 // created: 2020-07-29 08:51:50
$dictionary['Document']['fields']['active_date']['audited']=false;
$dictionary['Document']['fields']['active_date']['massupdate']=true;
$dictionary['Document']['fields']['active_date']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['active_date']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['active_date']['merge_filter']='disabled';
$dictionary['Document']['fields']['active_date']['calculated']=false;
$dictionary['Document']['fields']['active_date']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/documents_sasa_companias_1_Documents.php

// created: 2020-07-29 11:19:24
$dictionary["Document"]["fields"]["documents_sasa_companias_1"] = array (
  'name' => 'documents_sasa_companias_1',
  'type' => 'link',
  'relationship' => 'documents_sasa_companias_1',
  'source' => 'non-db',
  'module' => 'sasa_Companias',
  'bean_name' => 'sasa_Companias',
  'vname' => 'LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE',
  'id_name' => 'documents_sasa_companias_1sasa_companias_idb',
);

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/audit.php

$dictionary['Document']['audited'] = true;

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_sasa_codigo_c.php

 // created: 2021-04-20 19:33:14
$dictionary['Document']['fields']['sasa_codigo_c']['labelValue']='Código';
$dictionary['Document']['fields']['sasa_codigo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Document']['fields']['sasa_codigo_c']['enforced']='';
$dictionary['Document']['fields']['sasa_codigo_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_sasa_url_formt_hd_c.php

 // created: 2021-04-20 19:33:14
$dictionary['Document']['fields']['sasa_url_formt_hd_c']['labelValue']='URL Formato Habeas Data';
$dictionary['Document']['fields']['sasa_url_formt_hd_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Document']['fields']['sasa_url_formt_hd_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_sasa_codcompania_c.php

 // created: 2021-04-20 19:33:15

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_sasa_nombrecompania_c.php

 // created: 2021-04-20 19:33:15

 
?>

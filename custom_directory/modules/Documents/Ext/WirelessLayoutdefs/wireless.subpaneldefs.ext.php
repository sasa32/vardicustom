<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/WirelessLayoutdefs/documents_sasa_companias_1_Documents.php

 // created: 2020-07-29 16:42:01
$layout_defs["Documents"]["subpanel_setup"]['documents_sasa_companias_1'] = array (
  'order' => 100,
  'module' => 'sasa_Companias',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE',
  'get_subpanel_data' => 'documents_sasa_companias_1',
);

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/WirelessLayoutdefs/documents_sasa_habeas_data_1_Documents.php

 // created: 2020-07-29 16:43:27
$layout_defs["Documents"]["subpanel_setup"]['documents_sasa_habeas_data_1'] = array (
  'order' => 100,
  'module' => 'SASA_Habeas_Data',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'get_subpanel_data' => 'documents_sasa_habeas_data_1',
);

?>

<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-07-29 16:42:01
$viewdefs['Documents']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE',
  'context' => 
  array (
    'link' => 'documents_sasa_companias_1',
  ),
);

// created: 2020-07-29 16:43:27
$viewdefs['Documents']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'context' => 
  array (
    'link' => 'documents_sasa_habeas_data_1',
  ),
);
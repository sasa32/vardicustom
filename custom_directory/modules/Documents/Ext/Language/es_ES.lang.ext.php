<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/es_ES.customsasa_habeas_data_documents_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/es_ES.customsasa_habeas_data_documents_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_HABEAS_DATA_DOCUMENTS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_SASA_HABEAS_DATA_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Habeas Data';

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_HABEAS_DATA_DOCUMENTS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_SASA_HABEAS_DATA_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Habeas Data';


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/es_ES.gvdocumentscustomfieldsv00120200729.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_CONTRACT_NAME'] = 'Nombre de Contract:';
$mod_strings['LBL_CONTRACTS'] = 'Contracts';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LNK_NEW_Documento'] = 'Crear Documento';
$mod_strings['LBL_MODULE_NAME'] = 'Documentos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Documento';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Documento';
$mod_strings['LNK_Documento_LIST'] = 'Ver Documentos';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Documentos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Documentos';
$mod_strings['LBL_MODULE_TITLE'] = 'Documentos: Inicio';
$mod_strings['LBL_Documento_ID'] = 'ID de Documento';
$mod_strings['LBL_Documento_NAME'] = 'Nombre del Documento';
$mod_strings['LBL_RELATED_Documento_ID'] = 'ID de Documento Relacionado';
$mod_strings['LBL_RELATED_Documento_REVISION_ID'] = 'ID de Revisión de Documentos Relacionado';
$mod_strings['LBL_TEMPLATE_TYPE'] = 'Tipo de Documento';
$mod_strings['LBL_Documento'] = 'Documento Relacionado';
$mod_strings['LBL_Documento_REVISION_ID'] = 'Id de la Revisión del Documento';
$mod_strings['LBL_DOC_NAME'] = 'Nombre de Documento:';
$mod_strings['LBL_DET_RELATED_Documento_VERSION'] = 'Revisión de Documento Relacionado:';
$mod_strings['LBL_DET_TEMPLATE_TYPE'] = 'Tipo de Documento:';
$mod_strings['LBL_LIST_Documento'] = 'Documento';
$mod_strings['LBL_SF_Documento'] = 'Nombre de Documento:';
$mod_strings['DEF_CREATE_LOG'] = 'Documento Creado';
$mod_strings['ERR_DOC_NAME'] = 'Nombre del Documento';
$mod_strings['ERR_DOC_VERSION'] = 'Versión de Documento';
$mod_strings['LBL_TREE_TITLE'] = 'Documentos';
$mod_strings['LBL_LIST_TEMPLATE_TYPE'] = 'Tipo de Documento';
$mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URL_FORMT_HD_C'] = 'URL Formato Habeas Data';
$mod_strings['LBL_SASA_CODIGO_C'] = 'Código';

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/es_ES.gvdocumentscustomfieldsv00220200923.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_CONTRACT_NAME'] = 'Nombre de Contract:';
$mod_strings['LBL_CONTRACTS'] = 'Contracts';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LNK_NEW_Documento'] = 'Crear Documento';
$mod_strings['LBL_MODULE_NAME'] = 'Documentos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Documento';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Documento';
$mod_strings['LNK_Documento_LIST'] = 'Ver Documentos';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Documentos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Documentos';
$mod_strings['LBL_MODULE_TITLE'] = 'Documentos: Inicio';
$mod_strings['LBL_Documento_ID'] = 'ID de Documento';
$mod_strings['LBL_Documento_NAME'] = 'Nombre del Documento';
$mod_strings['LBL_RELATED_Documento_ID'] = 'ID de Documento Relacionado';
$mod_strings['LBL_RELATED_Documento_REVISION_ID'] = 'ID de Revisión de Documentos Relacionado';
$mod_strings['LBL_TEMPLATE_TYPE'] = 'Tipo de Documento';
$mod_strings['LBL_Documento'] = 'Documento Relacionado';
$mod_strings['LBL_Documento_REVISION_ID'] = 'Id de la Revisión del Documento';
$mod_strings['LBL_DOC_NAME'] = 'Nombre de Documento:';
$mod_strings['LBL_DET_RELATED_Documento_VERSION'] = 'Revisión de Documento Relacionado:';
$mod_strings['LBL_DET_TEMPLATE_TYPE'] = 'Tipo de Documento:';
$mod_strings['LBL_LIST_Documento'] = 'Documento';
$mod_strings['LBL_SF_Documento'] = 'Nombre de Documento:';
$mod_strings['DEF_CREATE_LOG'] = 'Documento Creado';
$mod_strings['ERR_DOC_NAME'] = 'Nombre del Documento';
$mod_strings['ERR_DOC_VERSION'] = 'Versión de Documento';
$mod_strings['LBL_TREE_TITLE'] = 'Documentos';
$mod_strings['LBL_LIST_TEMPLATE_TYPE'] = 'Tipo de Documento';
$mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URL_FORMT_HD_C'] = 'URL Formato Habeas Data';
$mod_strings['LBL_SASA_CODIGO_C'] = 'Código';
$mod_strings['LBL_SASA_CODCOMPANIA_C'] = 'Código Compañia';

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/es_ES.gvdocumentscustomfieldsv00320210420.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/es_ES.gvdocumentscustomfieldsv00320210420.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_CONTRACT_NAME'] = 'Nombre de Contract:';
$mod_strings['LBL_CONTRACTS'] = 'Contracts';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LNK_NEW_Documento'] = 'Crear Documento';
$mod_strings['LBL_MODULE_NAME'] = 'Documentos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Documento';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Documento';
$mod_strings['LNK_Documento_LIST'] = 'Ver Documentos';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Documentos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Documentos';
$mod_strings['LBL_MODULE_TITLE'] = 'Documentos: Inicio';
$mod_strings['LBL_Documento_ID'] = 'ID de Documento';
$mod_strings['LBL_Documento_NAME'] = 'Nombre del Documento';
$mod_strings['LBL_RELATED_Documento_ID'] = 'ID de Documento Relacionado';
$mod_strings['LBL_RELATED_Documento_REVISION_ID'] = 'ID de Revisión de Documentos Relacionado';
$mod_strings['LBL_TEMPLATE_TYPE'] = 'Tipo de Documento';
$mod_strings['LBL_Documento'] = 'Documento Relacionado';
$mod_strings['LBL_Documento_REVISION_ID'] = 'Id de la Revisión del Documento';
$mod_strings['LBL_DOC_NAME'] = 'Nombre de Documento:';
$mod_strings['LBL_DET_RELATED_Documento_VERSION'] = 'Revisión de Documento Relacionado:';
$mod_strings['LBL_DET_TEMPLATE_TYPE'] = 'Tipo de Documento:';
$mod_strings['LBL_LIST_Documento'] = 'Documento';
$mod_strings['LBL_SF_Documento'] = 'Nombre de Documento:';
$mod_strings['DEF_CREATE_LOG'] = 'Documento Creado';
$mod_strings['ERR_DOC_NAME'] = 'Nombre del Documento';
$mod_strings['ERR_DOC_VERSION'] = 'Versión de Documento';
$mod_strings['LBL_TREE_TITLE'] = 'Documentos';
$mod_strings['LBL_LIST_TEMPLATE_TYPE'] = 'Tipo de Documento';
$mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URL_FORMT_HD_C'] = 'URL Formato Habeas Data';
$mod_strings['LBL_SASA_CODIGO_C'] = 'Código';
$mod_strings['LBL_SASA_CODCOMPANIA_C'] = 'Código Compañia';
$mod_strings['LBL_SASA_NOMBRECOMPANIA_C'] = 'Compañía';

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_CONTRACT_NAME'] = 'Nombre de Contract:';
$mod_strings['LBL_CONTRACTS'] = 'Contracts';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LNK_NEW_Documento'] = 'Crear Documento';
$mod_strings['LBL_MODULE_NAME'] = 'Documentos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Documento';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Documento';
$mod_strings['LNK_Documento_LIST'] = 'Ver Documentos';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Documentos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Documentos';
$mod_strings['LBL_MODULE_TITLE'] = 'Documentos: Inicio';
$mod_strings['LBL_Documento_ID'] = 'ID de Documento';
$mod_strings['LBL_Documento_NAME'] = 'Nombre del Documento';
$mod_strings['LBL_RELATED_Documento_ID'] = 'ID de Documento Relacionado';
$mod_strings['LBL_RELATED_Documento_REVISION_ID'] = 'ID de Revisión de Documentos Relacionado';
$mod_strings['LBL_TEMPLATE_TYPE'] = 'Tipo de Documento';
$mod_strings['LBL_Documento'] = 'Documento Relacionado';
$mod_strings['LBL_Documento_REVISION_ID'] = 'Id de la Revisión del Documento';
$mod_strings['LBL_DOC_NAME'] = 'Nombre de Documento:';
$mod_strings['LBL_DET_RELATED_Documento_VERSION'] = 'Revisión de Documento Relacionado:';
$mod_strings['LBL_DET_TEMPLATE_TYPE'] = 'Tipo de Documento:';
$mod_strings['LBL_LIST_Documento'] = 'Documento';
$mod_strings['LBL_SF_Documento'] = 'Nombre de Documento:';
$mod_strings['DEF_CREATE_LOG'] = 'Documento Creado';
$mod_strings['ERR_DOC_NAME'] = 'Nombre del Documento';
$mod_strings['ERR_DOC_VERSION'] = 'Versión de Documento';
$mod_strings['LBL_TREE_TITLE'] = 'Documentos';
$mod_strings['LBL_LIST_TEMPLATE_TYPE'] = 'Tipo de Documento';
$mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URL_FORMT_HD_C'] = 'URL Formato Habeas Data';
$mod_strings['LBL_SASA_CODIGO_C'] = 'Código';
$mod_strings['LBL_SASA_CODCOMPANIA_C'] = 'Código Compañia';
$mod_strings['LBL_SASA_NOMBRECOMPANIA_C'] = 'Compañía';


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/es_ES.customdocuments_sasa_companias_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE'] = 'Compañias';
$mod_strings['LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_DOCUMENTS_TITLE'] = 'Compañias';

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/es_ES.customdocuments_sasa_habeas_data_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_DOCUMENTS_TITLE'] = 'Habeas Data';

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DOCUMENTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE'] = 'Compañías';
$mod_strings['LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_DOCUMENTS_TITLE'] = 'Habeas Data';
$mod_strings['LBL_OPPORTUNITIES_SUBPANEL_TITLE'] = 'Cotizaciones';

?>

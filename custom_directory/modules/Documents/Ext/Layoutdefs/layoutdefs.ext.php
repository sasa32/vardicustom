<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Layoutdefs/documents_sasa_companias_1_Documents.php

 // created: 2020-07-29 16:42:01
$layout_defs["Documents"]["subpanel_setup"]['documents_sasa_companias_1'] = array (
  'order' => 100,
  'module' => 'sasa_Companias',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE',
  'get_subpanel_data' => 'documents_sasa_companias_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Layoutdefs/documents_sasa_habeas_data_1_Documents.php

 // created: 2020-07-29 16:43:27
$layout_defs["Documents"]["subpanel_setup"]['documents_sasa_habeas_data_1'] = array (
  'order' => 100,
  'module' => 'SASA_Habeas_Data',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'get_subpanel_data' => 'documents_sasa_habeas_data_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Layoutdefs/_overrideDocument_subpanel_documents_sasa_habeas_data_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Documents']['subpanel_setup']['documents_sasa_habeas_data_1']['override_subpanel_name'] = 'Document_subpanel_documents_sasa_habeas_data_1';

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Layoutdefs/_overrideDocument_subpanel_opportunities.php

//auto-generated file DO NOT EDIT
$layout_defs['Documents']['subpanel_setup']['opportunities']['override_subpanel_name'] = 'Document_subpanel_opportunities';

?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Administration/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_OPPORTUNITIES_DESC'] = 'Configurar los parámetros de administración para el módulo de Cotizaciones. La configuración de Cotizaciones incluye ver solo por Cotizaciones o por Cotizaciones y líneas de ingresos unidas a ellas.';
$mod_strings['LBL_MANAGE_OPPORTUNITIES_TITLE'] = 'Cotizaciones';
$mod_strings['LBL_MANAGE_OPPORTUNITIES_DESC'] = 'Configurar el módulo de Cotizaciones';

?>

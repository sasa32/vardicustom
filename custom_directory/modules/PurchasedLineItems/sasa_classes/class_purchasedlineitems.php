<?php
class class_purchasedLineItems
{

	private $argumentos;
	private $Bean = null;

	function __construct($args)
	{	
		$this->argumentos = $args;
		//$GLOBALS['log']->security("Argumento #: " . print_r($this->argumentos, true));
	
	}

	public function miFuncionLine(){

		return "Elemento Comprado mi funcion";
	}
	//metodo para crear un elemento comprado
	public function createPurchasedLineItems(){

		//$Bean = null;		
		$response = array();

		require_once 'custom/modules/Purchases/sasa_classes/class_purchases.php';

		$purchases = new class_purchases($this->argumentos);

		$id_purchases = $purchases->createPurchases();

		$GLOBALS['log']->security("COMPRAS:: " . $id_purchases);
		//Create bean
		$Bean = BeanFactory::newBean("PurchasedLineItems");

			foreach ($this->argumentos as $key => $value) 
			{
			//$GLOBALS['log']->security("Argumento #: " . $key . " Valor: " . $value);
				//Populate bean fields
				$Bean->{$key} = $value;
			}
		
		$Bean->purchase_id = $id_purchases;
		
		$this->relationshipOfPurchasedLineItems($Bean);
		$orderStatus = $this->udpdateOrderStatusQuote();
		$response = $this->saveModules($Bean);

		return $response;
	}
	//metodo para Actualiazar el elemento comprado
	public function updatePurchasedLineItems($id_pedido){

		$GLOBALS['log']->security("Update de Pedidos.." . $id_pedido);
		
		$response = array();
		//Retrieve bean
		$Bean = BeanFactory::retrieveBean("PurchasedLineItems", $id_pedido);
		
		//Returna No existe si la opportunidad no existe
		if($Bean == null) return "No existe";

		foreach ($this->argumentos as $key => $value) 
		{
		//$GLOBALS['log']->security("Argumento #: " . $key . " Valor: " . $value);
			//Fields to update
			$Bean->{$key} = $value;
		}
		
		$this->relationshipOfPurchasedLineItems($Bean);
		$orderStatus = $this->udpdateOrderStatusQuote();
		//Actualizar compra.

		require_once 'custom/modules/Purchases/sasa_classes/class_purchases.php';

		$purchases = new class_purchases($this->argumentos);

		$cotizacion = $purchases->updatePurchases($id_pedido);

		//$GLOBALS['log']->security("COMPRA:::" . $cotizacion);

		$response["id_pedido"] = $this->saveModules($Bean);

		return $response;

	}

	//metodo para Las relaciones de punto atencion, unidad de entrega, linea devehiculo, unidad de negocio y cotizaciones
	private function relationshipOfPurchasedLineItems($Bean){
		
		$punto_atencion = $this->argumentos['sasa_codatencion_c'];
		$unidad_entrega = $this->argumentos['sasa_cod_entrega_c'];
		$linea_vehiculo = $this->argumentos['sasa_codlinea_c'];
		$unidad_negocio = $this->argumentos['sasa_codnegocio_c'];
		$id_cotizacion = $this->argumentos['id_cotizacion'];

		//Validar relación de punto de atención
		if(!empty($punto_atencion)){

			$query = $GLOBALS['db']->query("SELECT spv.id AS id_punto_antencion FROM sasa_puntos_de_ventas spv
				INNER JOIN sasa_puntos_de_ventas_cstm spvc ON spvc.id_c = spv.id
				WHERE spvc.sasa_codpuntoatencion_c = '{$punto_atencion}' AND spv.deleted = 0 limit 1");

			$id_punto =  $GLOBALS['db']->fetchByAssoc($query);

			//$GLOBALS['log']->security("Punto de Atención:" . $id_punto['id_punto_antencion']);

			$punto_atencion = $id_punto['id_punto_antencion'];

			$Bean->sasa_punto048a_ventas_ida = $punto_atencion;
			$Bean->load_relationship('sasa_puntos_de_ventas_purchasedlineitems_1');
			$Bean->sasa_puntos_de_ventas_purchasedlineitems_1->add($punto_atencion);
		}

		if(!empty($unidad_entrega)){

			$query = $GLOBALS['db']->query("SELECT spv.id AS id_punto_antencion FROM sasa_puntos_de_ventas spv
				INNER JOIN sasa_puntos_de_ventas_cstm spvc ON spvc.id_c = spv.id
				WHERE spvc.sasa_codpuntoatencion_c = '{$unidad_entrega}' AND spv.deleted = 0 limit 1");

			$id_punto =  $GLOBALS['db']->fetchByAssoc($query);

			//$GLOBALS['log']->security("Punto de Atención:" . $id_punto['id_punto_antencion']);

			$punto_atencion = $id_punto['id_punto_antencion'];

			$Bean->sasa_punto8446_ventas_ida = $punto_atencion;
			$Bean->load_relationship('sasa_puntos_de_ventas_purchasedlineitems_2');
			$Bean->sasa_puntos_de_ventas_purchasedlineitems_2->add($punto_atencion);
		}
		//Validar relación de linea de vehículo
		if(!empty($linea_vehiculo)){

			$query = $GLOBALS['db']->query("SELECT sv.id AS id_linea_vehiculo FROM sasa_vehiculos sv 
			INNER JOIN sasa_vehiculos_cstm svc ON svc.id_c = sv.id
			WHERE svc.sas_cd_line_vehi_c = '{$linea_vehiculo}' AND sv.deleted = 0 limit 1");

			$id_linea =  $GLOBALS['db']->fetchByAssoc($query);

			$GLOBALS['log']->security("Linea Vehículo:" . $id_linea['id_linea_vehiculo']);

			$linea_vehiculo = $id_linea['id_linea_vehiculo'];

			$Bean->sasa_vehiculos_purchasedlineitems_1sasa_vehiculos_ida = $linea_vehiculo;
			$Bean->load_relationship('sasa_vehiculos_purchasedlineitems_1');
			$Bean->sasa_vehiculos_purchasedlineitems_1->add($linea_vehiculo);
		}

		//Validar relación de unidad de negocio
		if(!empty($unidad_negocio)){

			$query = $GLOBALS['db']->query("SELECT sun.id AS id_unidad_negocio 
				FROM sasa_unidad_de_negocio sun
				INNER JOIN sasa_unidad_de_negocio_cstm sunc ON sunc.id_c = sun.id
				WHERE sunc.sasa_codunidnegocio_c = '{$unidad_negocio}'  AND sun.deleted = 0 limit 1");

			$id_unidad =  $GLOBALS['db']->fetchByAssoc($query);

			$GLOBALS['log']->security("Unidad de negocio:" . $id_unidad['id_unidad_negocio']);

			$unidad_negocio = $id_unidad['id_unidad_negocio'];

			$Bean->sasa_unidad993negocio_ida = $unidad_negocio;
			$Bean->load_relationship('sasa_unidad_de_negocio_purchasedlineitems_1');
			$Bean->sasa_unidad_de_negocio_purchasedlineitems_1->add($unidad_negocio);
		}

		//Validar relación id cotización
		if(!empty($id_cotizacion)){

			$Bean->opportunities_purchasedlineitems_1opportunities_ida = $id_cotizacion;
			$Bean->load_relationship('opportunities_purchasedlineitems_1');
			$Bean->opportunities_purchasedlineitems_1->add($id_cotizacion);
		}
	}
	//Metodo para actualizar el estado del pedido en la cotización
	private function udpdateOrderStatusQuote(){

		$id_cotizacion = $this->argumentos['id_cotizacion'];
		$order_status = $this->argumentos['sasa_estadopedido_c'];

		if(!empty($id_cotizacion) && !empty($order_status))
		{
			$Bean = BeanFactory::retrieveBean("Opportunities", $id_cotizacion);
			$Bean->sasa_estadopedido_c = $order_status;
			$response = $this->saveModules($Bean);
			$GLOBALS['log']->security("ACTUALIZO ESTADO COTIZACION::" . $response);
			return $response;
		}
		
	}

	private function saveModules($Bean){
		
		//Save
		$Bean->save();

		//Retrieve the bean id
		$record_id = $Bean->id;

		return $record_id;
	}

}

?>
<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/PurchasedLineItems/Ext/Vardefs/denorm_account_name.php


// 'account_name'
$dictionary['PurchasedLineItem']['fields']['account_name']['is_denormalized'] = true;
$dictionary['PurchasedLineItem']['fields']['account_name']['denormalized_field_name'] = 'denorm_account_name';

// 'denorm_account_name'
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['name'] = 'denorm_account_name';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['type'] = 'varchar';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['dbType'] = 'varchar';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['vname'] = 'LBL_ACCOUNT';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['len'] = '150';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['comment'] = 'Name of the Company';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['unified_search'] = true;
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['full_text_search'] = array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['audited'] = false;
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['required'] = false;
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['importable'] = 'false';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['duplicate_on_record_copy'] = 'always';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['merge_filter'] = 'disabled';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['massupdate'] = false;
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['comments'] = 'Name of the Company';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['duplicate_merge'] = 'disabled';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['duplicate_merge_dom_value'] = 0;
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['calculated'] = '1';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['formula'] = 'concat($sasa_nombres_c," ",$sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['enforced'] = true;
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['denorm_from_module'] = 'Accounts';
$dictionary['PurchasedLineItem']['fields']['denorm_account_name']['studio'] = false;

?>
<?php
// Merged from custom/Extension/modules/PurchasedLineItems/Ext/Vardefs/sasa1_sasa_seguradoras_purchasedlineitems_1_PurchasedLineItems.php

// created: 2023-05-25 19:59:20
$dictionary["PurchasedLineItem"]["fields"]["sasa1_sasa_seguradoras_purchasedlineitems_1"] = array (
  'name' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'type' => 'link',
  'relationship' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'source' => 'non-db',
  'module' => 'SASA1_sasa_seguradoras',
  'bean_name' => 'SASA1_sasa_seguradoras',
  'side' => 'right',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_PURCHASEDLINEITEMS_TITLE',
  'id_name' => 'sasa1_sasa7258radoras_ida',
  'link-type' => 'one',
);
$dictionary["PurchasedLineItem"]["fields"]["sasa1_sasa_seguradoras_purchasedlineitems_1_name"] = array (
  'name' => 'sasa1_sasa_seguradoras_purchasedlineitems_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_SASA1_SASA_SEGURADORAS_TITLE',
  'save' => true,
  'id_name' => 'sasa1_sasa7258radoras_ida',
  'link' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'table' => 'sasa1_sasa_seguradoras',
  'module' => 'SASA1_sasa_seguradoras',
  'rname' => 'name',
);
$dictionary["PurchasedLineItem"]["fields"]["sasa1_sasa7258radoras_ida"] = array (
  'name' => 'sasa1_sasa7258radoras_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_PURCHASEDLINEITEMS_TITLE_ID',
  'id_name' => 'sasa1_sasa7258radoras_ida',
  'link' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'table' => 'sasa1_sasa_seguradoras',
  'module' => 'SASA1_sasa_seguradoras',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>

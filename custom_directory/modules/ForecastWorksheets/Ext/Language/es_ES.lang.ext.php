<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ForecastWorksheets/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nombre de Cliente y Prospecto:';
$mod_strings['LNK_ACCOUNTS'] = 'Cliente y Prospecto';
$mod_strings['LBL_OW_ACCOUNTNAME'] = 'Cliente y Prospecto';
$mod_strings['LBL_REVENUELINEITEM_NAME'] = 'Nombre de la Revenue Line Item';
$mod_strings['LBL_FDR_OPPORTUNITIES'] = 'Cotizaciones en Previsión';
$mod_strings['LBL_DV_FORECAST_OPPORTUNITY'] = 'Cotizaciones de la Previsión';
$mod_strings['LBL_LV_OPPORTUNITIES'] = 'Cotizaciones';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Crear Cotización';
$mod_strings['LNK_OPPORTUNITY'] = 'Cotización';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Nombre de la Cotización';
$mod_strings['LBL_OW_OPPORTUNITIES'] = 'Cotización';
$mod_strings['LBL_OW_MODULE_TITLE'] = 'Hoja de Cotización';

?>

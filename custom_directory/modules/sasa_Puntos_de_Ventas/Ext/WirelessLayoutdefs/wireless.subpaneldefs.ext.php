<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/WirelessLayoutdefs/sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_sasa_Puntos_de_Ventas.php

 // created: 2020-05-15 23:48:57
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_sasa_unidad_de_negocio_1'] = array (
  'order' => 100,
  'module' => 'sasa_Unidad_de_Negocio',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/WirelessLayoutdefs/sasa_puntos_de_ventas_meetings_1_sasa_Puntos_de_Ventas.php

 // created: 2020-05-15 23:49:35
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_meetings_1'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_meetings_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/WirelessLayoutdefs/sasa_puntos_de_ventas_tasks_1_sasa_Puntos_de_Ventas.php

 // created: 2020-06-19 14:27:46
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_tasks_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/WirelessLayoutdefs/sasa_puntos_de_ventas_calls_1_sasa_Puntos_de_Ventas.php

 // created: 2020-06-19 14:28:13
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_calls_1'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_calls_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/WirelessLayoutdefs/sasa_puntos_de_ventas_leads_1_sasa_Puntos_de_Ventas.php

 // created: 2021-01-27 16:50:54
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_leads_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/WirelessLayoutdefs/sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_sasa_Puntos_de_Ventas.php

 // created: 2021-03-05 13:31:31
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1'] = array (
  'order' => 100,
  'module' => 'SASA_UnidadNegClienteProspect',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/WirelessLayoutdefs/sasa_puntos_de_ventas_cases_1_sasa_Puntos_de_Ventas.php

 // created: 2021-03-09 21:41:15
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_cases_1',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/WirelessLayoutdefs/sasa_puntos_de_ventas_opportunities_1_sasa_Puntos_de_Ventas.php

 // created: 2022-11-23 15:55:57
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_opportunities_1'] = array (
  'order' => 100,
  'module' => 'Opportunities',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_opportunities_1',
);

?>

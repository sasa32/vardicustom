<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_sasa_Puntos_de_Ventas.php

// created: 2020-05-15 18:54:37
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_sasa_unidad_de_negocio_1"] = array (
  'name' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
  'source' => 'non-db',
  'module' => 'sasa_Unidad_de_Negocio',
  'bean_name' => 'sasa_Unidad_de_Negocio',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa_puntoeacanegocio_idb',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sasa_puntos_de_ventas_meetings_1_sasa_Puntos_de_Ventas.php

// created: 2020-05-15 18:55:38
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_meetings_1"] = array (
  'name' => 'sasa_puntos_de_ventas_meetings_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_meetings_1',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_description.php

 // created: 2020-05-16 20:27:40
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['audited']=false;
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['massupdate']=false;
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['comments']='Full text of the note';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['duplicate_merge']='enabled';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['merge_filter']='disabled';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['unified_search']=false;
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '0.5',
  'searchable' => false,
);
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['calculated']=false;
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['rows']='6';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sasa_puntos_de_ventas_tasks_1_sasa_Puntos_de_Ventas.php

// created: 2020-06-19 14:27:46
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_tasks_1"] = array (
  'name' => 'sasa_puntos_de_ventas_tasks_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sasa_puntos_de_ventas_calls_1_sasa_Puntos_de_Ventas.php

// created: 2020-06-19 14:28:13
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_calls_1"] = array (
  'name' => 'sasa_puntos_de_ventas_calls_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_calls_1',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sasa_puntos_de_ventas_leads_1_sasa_Puntos_de_Ventas.php

// created: 2021-01-27 16:50:54
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_leads_1"] = array (
  'name' => 'sasa_puntos_de_ventas_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_sasa_Puntos_de_Ventas.php

// created: 2021-03-05 13:31:31
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'SASA_UnidadNegClienteProspect',
  'bean_name' => 'SASA_UnidadNegClienteProspect',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_punto200b_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sasa_puntos_de_ventas_cases_1_sasa_Puntos_de_Ventas.php

// created: 2021-03-09 21:41:14
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_cases_1"] = array (
  'name' => 'sasa_puntos_de_ventas_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_codpuntoatencion_c.php

 // created: 2022-05-31 02:02:53
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_codpuntoatencion_c']['labelValue']='Código Punto Atención';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_codpuntoatencion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_codpuntoatencion_c']['enforced']='';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_codpuntoatencion_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_codsucursal_c.php

 // created: 2022-05-31 02:02:53
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_codsucursal_c']['labelValue']='Código Sucursal';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_codsucursal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_codsucursal_c']['enforced']='';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_codsucursal_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_direccion_c.php

 // created: 2022-05-31 02:02:53
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_direccion_c']['labelValue']='Dirección';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_direccion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_direccion_c']['enforced']='';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_direccion_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_estado_c.php

 // created: 2022-05-31 02:02:53
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_estado_c']['labelValue']='Estado';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_estado_c']['dependency']='';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_estado_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_programar_llamada_c.php

 // created: 2022-05-31 02:02:54

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2022-05-31 02:02:54
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_gerentedirector_c.php

 // created: 2022-05-31 02:02:54
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_gerentedirector_c']['duplicate_merge_dom_value']=1;
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_gerentedirector_c']['labelValue']='Gerente/Director';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_gerentedirector_c']['calculated']='false';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_gerentedirector_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_sucursal_c.php

 // created: 2022-05-31 02:02:56
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_sucursal_c']['duplicate_merge_dom_value']=1;
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_sucursal_c']['labelValue']='Sucursal';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_sucursal_c']['calculated']='false';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_sucursal_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_telefono_c.php

 // created: 2022-05-31 02:02:57
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_telefono_c']['labelValue']='Número de Teléfono';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_telefono_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_telefono_c']['enforced']='';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_telefono_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_tipo_c.php

 // created: 2022-05-31 02:02:57
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_tipo_c']['labelValue']='Tipo';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_tipo_c']['dependency']='';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_tipo_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sugarfield_sasa_centrocostos_c.php

 // created: 2022-08-29 10:47:19
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_centrocostos_c']['labelValue']='Centro de costos';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_centrocostos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_centrocostos_c']['enforced']='';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_centrocostos_c']['dependency']='';
$dictionary['sasa_Puntos_de_Ventas']['fields']['sasa_centrocostos_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sasa_centrosdecostos_sasa_puntos_de_ventas_1_sasa_Puntos_de_Ventas.php

// created: 2022-10-04 17:32:58
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_centrosdecostos_sasa_puntos_de_ventas_1"] = array (
  'name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'type' => 'link',
  'relationship' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'source' => 'non-db',
  'module' => 'sasa_CentrosDeCostos',
  'bean_name' => 'sasa_CentrosDeCostos',
  'side' => 'right',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_centr37aaecostos_ida',
  'link-type' => 'one',
);
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_centrosdecostos_sasa_puntos_de_ventas_1_name"] = array (
  'name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_CENTROSDECOSTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_centr37aaecostos_ida',
  'link' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'table' => 'sasa_centrosdecostos',
  'module' => 'sasa_CentrosDeCostos',
  'rname' => 'name',
);
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_centr37aaecostos_ida"] = array (
  'name' => 'sasa_centr37aaecostos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE_ID',
  'id_name' => 'sasa_centr37aaecostos_ida',
  'link' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'table' => 'sasa_centrosdecostos',
  'module' => 'sasa_CentrosDeCostos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Vardefs/sasa_puntos_de_ventas_opportunities_1_sasa_Puntos_de_Ventas.php

// created: 2022-11-23 15:55:57
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_opportunities_1"] = array (
  'name' => 'sasa_puntos_de_ventas_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>

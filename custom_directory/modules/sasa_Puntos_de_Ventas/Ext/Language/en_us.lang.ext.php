<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.gvpuntosatencioncustomfieldsv001-20200515.php


  $mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
  $mod_strings['LBL_SASA_CODPUNTOATENCION_C'] = 'Código Punto Atención';
  $mod_strings['LBL_SASA_ESTADO_C'] = 'Estado';
  $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
  $mod_strings['LBL_SASA_GERENTEDIRECTOR_C'] = 'Gerente/Director';
  $mod_strings['LBL_SASA_CODSUCURSAL_C'] = 'Código Sucursal';
  $mod_strings['LBL_SASA_SUCURSAL_C'] = 'Sucursal';
  $mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
  $mod_strings['LBL_SASA_DIRECCION_C'] = 'Dirección';
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_puntos_de_ventas_sasa_unidad_de_negocio_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_puntos_de_ventas_sasa_unidad_de_negocio_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE'] = 'Unidad de Negocio';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Unidad de Negocio';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE'] = 'Unidad de Negocio';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Unidad de Negocio';


?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.gvpuntosatencioncustomfieldsv002-20200909.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.gvpuntosatencioncustomfieldsv002-20200909.php


  $mod_strings['LBL_SASA_PROGRAMAR_LLAMADA'] = 'Programar llamada';
  $mod_strings['LBL_SASA_TELEFONO_C'] = 'Número de Teléfono';
  
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/temp.php


  $mod_strings['LBL_SASA_PROGRAMAR_LLAMADA'] = 'Programar llamada';
  $mod_strings['LBL_SASA_TELEFONO_C'] = 'Número de Teléfono';
  

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.gvpuntosatencioncustomfieldsv003-20201015.php

$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_CODPUNTOATENCION_C'] = 'Código Punto Atención';
$mod_strings['LBL_SASA_ESTADO_C'] = 'Estado';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GERENTEDIRECTOR_C'] = 'Gerente/Director';
$mod_strings['LBL_SASA_CODSUCURSAL_C'] = 'Código Sucursal';
$mod_strings['LBL_SASA_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Número de Teléfono';
$mod_strings['LBL_SASA_DIRECCION_C'] = 'Dirección';
$mod_strings['LBL_SASA_PROGRAMAR_LLAMADA_C'] = 'Programar llamada';
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_puntos_de_ventas_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Reuniones';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Reuniones';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_puntos_de_ventas_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_TASKS_TITLE'] = 'Tareas';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Tareas';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_puntos_de_ventas_calls_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_CALLS_TITLE'] = 'Llamadas';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Llamadas';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customcases_sasa_puntos_de_ventas_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CASES_SASA_PUNTOS_DE_VENTAS_1_FROM_CASES_TITLE'] = 'Cases';
$mod_strings['LBL_CASES_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Cases';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customleads_sasa_puntos_de_ventas_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LEADS_SASA_PUNTOS_DE_VENTAS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_LEADS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Leads';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_puntos_de_ventas_leads_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Leads';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE'] = 'Unidades de Neg. por Clientes y Prospectos';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Unidades de Neg. por Clientes y Prospectos';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_puntos_de_ventas_cases_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE'] = 'Casos';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Casos';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.gvpuntosatencioncustomfieldsv005-20220531.php

$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_CODPUNTOATENCION_C'] = 'Código Punto Atención';
$mod_strings['LBL_SASA_ESTADO_C'] = 'Estado';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GERENTEDIRECTOR_C'] = 'Gerente/Director';
$mod_strings['LBL_SASA_CODSUCURSAL_C'] = 'Código Sucursal';
$mod_strings['LBL_SASA_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Número de Teléfono';
$mod_strings['LBL_SASA_DIRECCION_C'] = 'Dirección';
$mod_strings['LBL_SASA_PROGRAMAR_LLAMADA_C'] = 'Programar Llamada';
$mod_strings['LBL_SASA_PROGRAMAR_LLAMADA'] = 'Programar Llamada';
$mod_strings['LBL_SASA_CENTROCOSTOS_C'] = 'Centro de costos';
?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_centrosdecostos_sasa_puntos_de_ventas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_CENTROSDECOSTOS_TITLE'] = 'Centros De Costos';
$mod_strings['LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Centros De Costos';

?>
<?php
// Merged from custom/Extension/modules/sasa_Puntos_de_Ventas/Ext/Language/en_us.customsasa_puntos_de_ventas_opportunities_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE'] = 'Cotizaciones';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Cotizaciones';

?>

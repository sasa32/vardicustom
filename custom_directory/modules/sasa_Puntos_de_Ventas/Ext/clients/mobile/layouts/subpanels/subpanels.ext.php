<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-06-19 14:28:14
$viewdefs['sasa_Puntos_de_Ventas']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_CALLS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_puntos_de_ventas_calls_1',
  ),
);

// created: 2021-03-09 21:41:15
$viewdefs['sasa_Puntos_de_Ventas']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_puntos_de_ventas_cases_1',
  ),
);

// created: 2021-01-27 16:50:54
$viewdefs['sasa_Puntos_de_Ventas']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_LEADS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_puntos_de_ventas_leads_1',
  ),
);

// created: 2020-05-15 23:49:35
$viewdefs['sasa_Puntos_de_Ventas']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_MEETINGS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_puntos_de_ventas_meetings_1',
  ),
);

// created: 2022-11-23 15:55:57
$viewdefs['sasa_Puntos_de_Ventas']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_puntos_de_ventas_opportunities_1',
  ),
);

// created: 2020-05-15 23:48:57
$viewdefs['sasa_Puntos_de_Ventas']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'context' => 
  array (
    'link' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
  ),
);

// created: 2021-03-05 13:31:31
$viewdefs['sasa_Puntos_de_Ventas']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'context' => 
  array (
    'link' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
  ),
);

// created: 2020-06-19 14:27:46
$viewdefs['sasa_Puntos_de_Ventas']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_TASKS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_puntos_de_ventas_tasks_1',
  ),
);
<?php
// created: 2022-08-29 11:19:11
$viewdefs['sasa_Puntos_de_Ventas']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'sasa_centrocostos_c' => 
    array (
    ),
    'sasa_codpuntoatencion_c' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
  ),
);
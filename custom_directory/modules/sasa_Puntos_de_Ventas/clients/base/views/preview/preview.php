<?php
$module_name = 'sasa_Puntos_de_Ventas';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'sasa_codpuntoatencion_c',
                'label' => 'LBL_SASA_CODPUNTOATENCION_C',
              ),
              1 => 
              array (
                'name' => 'sasa_gerentedirector_c',
                'label' => 'LBL_SASA_GERENTEDIRECTOR_C',
              ),
              2 => 
              array (
                'name' => 'sasa_sucursal_c',
                'label' => 'LBL_SASA_SUCURSAL_C',
              ),
              3 => 
              array (
                'name' => 'sasa_codsucursal_c',
                'label' => 'LBL_SASA_CODSUCURSAL_C',
              ),
              4 => 
              array (
                'name' => 'sasa_direccion_c',
                'label' => 'LBL_SASA_DIRECCION_C',
              ),
              5 => 
              array (
                'name' => 'sasa_telefono_c',
                'label' => 'LBL_SASA_TELEFONO_C',
              ),
              6 => 
              array (
                'name' => 'sasa_estado_c',
                'label' => 'LBL_SASA_ESTADO_C',
              ),
              7 => 
              array (
                'name' => 'sasa_tipo_c',
                'label' => 'LBL_SASA_TIPO_C',
              ),
              8 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              9 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);

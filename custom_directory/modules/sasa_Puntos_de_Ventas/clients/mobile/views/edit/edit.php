<?php
$module_name = 'sasa_Puntos_de_Ventas';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'edit' => 
      array (
        'templateMeta' => 
        array (
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 
              array (
                'name' => 'sasa_codpuntoatencion_c',
                'label' => 'LBL_SASA_CODPUNTOATENCION_C',
              ),
              2 => 
              array (
                'name' => 'sasa_gerentedirector_c',
                'label' => 'LBL_SASA_GERENTEDIRECTOR_C',
              ),
              3 => 
              array (
                'name' => 'sasa_sucursal_c',
                'label' => 'LBL_SASA_SUCURSAL_C',
              ),
              4 => 
              array (
                'name' => 'sasa_codsucursal_c',
                'label' => 'LBL_SASA_CODSUCURSAL_C',
              ),
              5 => 
              array (
                'name' => 'sasa_direccion_c',
                'label' => 'LBL_SASA_DIRECCION_C',
              ),
              6 => 
              array (
                'name' => 'sasa_telefono_c',
                'label' => 'LBL_SASA_TELEFONO_C',
              ),
              7 => 
              array (
                'name' => 'sasa_estado_c',
                'label' => 'LBL_SASA_ESTADO_C',
              ),
              8 => 
              array (
                'name' => 'sasa_tipo_c',
                'label' => 'LBL_SASA_TIPO_C',
              ),
              9 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              10 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);

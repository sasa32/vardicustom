<?php
$popupMeta = array (
    'moduleMain' => 'sasa_Puntos_de_Ventas',
    'varName' => 'sasa_Puntos_de_Ventas',
    'orderBy' => 'sasa_puntos_de_ventas.name',
    'whereClauses' => array (
  'name' => 'sasa_puntos_de_ventas.name',
  'sasa_codpuntoatencion_c' => 'sasa_puntos_de_ventas_cstm.sasa_codpuntoatencion_c',
  'sasa_gerentedirector_c' => 'sasa_puntos_de_ventas_cstm.sasa_gerentedirector_c',
  'sasa_sucursal_c' => 'sasa_puntos_de_ventas_cstm.sasa_sucursal_c',
  'sasa_codsucursal_c' => 'sasa_puntos_de_ventas_cstm.sasa_codsucursal_c',
  'sasa_direccion_c' => 'sasa_puntos_de_ventas_cstm.sasa_direccion_c',
  'sasa_telefono_c' => 'sasa_puntos_de_ventas_cstm.sasa_telefono_c',
  'sasa_estado_c' => 'sasa_puntos_de_ventas_cstm.sasa_estado_c',
  'sasa_tipo_c' => 'sasa_puntos_de_ventas_cstm.sasa_tipo_c',
  'date_entered' => 'sasa_puntos_de_ventas.date_entered',
  'date_modified' => 'sasa_puntos_de_ventas.date_modified',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'sasa_codpuntoatencion_c',
  5 => 'sasa_gerentedirector_c',
  6 => 'sasa_sucursal_c',
  7 => 'sasa_codsucursal_c',
  8 => 'sasa_direccion_c',
  9 => 'sasa_telefono_c',
  10 => 'sasa_estado_c',
  11 => 'sasa_tipo_c',
  12 => 'date_entered',
  13 => 'date_modified',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'sasa_codpuntoatencion_c' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_CODPUNTOATENCION_C',
    'width' => '10',
    'name' => 'sasa_codpuntoatencion_c',
  ),
  'sasa_gerentedirector_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_GERENTEDIRECTOR_C',
    'width' => '10',
    'name' => 'sasa_gerentedirector_c',
  ),
  'sasa_sucursal_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_SUCURSAL_C',
    'width' => '10',
    'name' => 'sasa_sucursal_c',
  ),
  'sasa_codsucursal_c' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_CODSUCURSAL_C',
    'width' => '10',
    'name' => 'sasa_codsucursal_c',
  ),
  'sasa_direccion_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_DIRECCION_C',
    'width' => '10',
    'name' => 'sasa_direccion_c',
  ),
  'sasa_telefono_c' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_TELEFONO_C',
    'width' => '10',
    'name' => 'sasa_telefono_c',
  ),
  'sasa_estado_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_ESTADO_C',
    'width' => '10',
    'name' => 'sasa_estado_c',
  ),
  'sasa_tipo_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SASA_TIPO_C',
    'width' => '10',
    'name' => 'sasa_tipo_c',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10',
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10',
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SASA_CODPUNTOATENCION_C' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_CODPUNTOATENCION_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_GERENTEDIRECTOR_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_GERENTEDIRECTOR_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_SUCURSAL_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_SUCURSAL_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_CODSUCURSAL_C' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_CODSUCURSAL_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_DIRECCION_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_DIRECCION_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_TELEFONO_C' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SASA_TELEFONO_C',
    'width' => 10,
    'default' => true,
  ),
  'SASA_ESTADO_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_ESTADO_C',
    'width' => 10,
  ),
  'SASA_TIPO_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_TIPO_C',
    'width' => 10,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
),
);

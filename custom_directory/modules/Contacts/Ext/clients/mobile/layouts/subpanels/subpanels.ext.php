<?php
// WARNING: The contents of this file are auto-generated.


// created: 2023-03-06 03:26:09
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_CASES_1_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'contacts_cases_1',
  ),
);

// created: 2020-07-30 15:35:59
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'context' => 
  array (
    'link' => 'contacts_sasa_habeas_data_1',
  ),
);
<?php
// WARNING: The contents of this file are auto-generated.


// created: 2018-12-04 16:24:59
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_contacts_1',
  ),
);

// created: 2023-03-06 03:26:09
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_CASES_1_FROM_CASES_TITLE',
  'context' => 
  array (
    'link' => 'contacts_cases_1',
  ),
);

// created: 2020-07-30 15:35:59
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'context' => 
  array (
    'link' => 'contacts_sasa_habeas_data_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'contacts_sasa_habeas_data_1',
  'view' => 'subpanel-for-contacts-contacts_sasa_habeas_data_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'opportunities',
  'view' => 'subpanel-for-contacts-opportunities',
);

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/read_only_contacts.php


$dependencies['Contacts']['read_only_contacts'] = array(
  'hooks' => array("all"),
  'trigger' => 'true',
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_city',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_state',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_country',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'alt_address_city',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'alt_address_state',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'alt_address_country',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'sasa_ciudad3_c',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'sasa_departamento3_c',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'sasa_pais3_c',
        'value' => 'true'
      )
    )
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);






?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependencies2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependencies2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_work_c','sasa_municipios_contacts_4_name'),
	//'trigger' => 'equal($sasa_phone_work_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_4_name',
				'value' => '
					not(equal($sasa_phone_work_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_4_name',
				'value' => '
					not(equal($sasa_phone_work_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_contacts_4_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_4_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciedireccion2contact.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciedireccion2contact'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('primary_address_street'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'alt_address',
				'value' => '
					or(not(equal($primary_address_street,0)),not(equal($alt_address_street,0)))
				',
			),
		),
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => '
					not(equal($primary_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'alt_address',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => 'false'
			)
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependencieCiudadDireCo1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependencieCiudadDireCo1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('primary_address_street'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_1_name',
				'value' => '
					not(equal($primary_address_street,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_1_name',
				'value' => '
					not(equal($primary_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_contacts_1_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_1_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependencieCiudadDireCo2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependencieCiudadDireCo2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('alt_address_street'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_2_name',
				'value' => '
					not(equal($alt_address_street,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_2_name',
				'value' => '
					not(equal($alt_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_contacts_2_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_2_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependencieCiudadDireCo3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependencieCiudadDireCo3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_direccion3_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_6_name',
				'value' => '
					not(equal($sasa_direccion3_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_6_name',
				'value' => '
					not(equal($sasa_direccion3_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_contacts_6_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_6_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependencies1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependencies1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_home_c','sasa_municipios_contacts_3_name'),
	//'trigger' => 'equal($sasa_phone_home_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_3_name',
				'value' => '
					not(equal($sasa_phone_home_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_3_name',
				'value' => '
					not(equal($sasa_phone_home_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_contacts_3_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_3_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciessetvalueconDire1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciessetvalueconDire1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('primary_address_street'),
	'trigger' => 'equal($primary_address_street,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_city',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_state',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_postalcode',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_country',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_1sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel2.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipTel2.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipotel2_c'),
	'trigger' => 'equal($sasa_tipotel2_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_work_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_4sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipTel2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_work_c'),
	'trigger' => 'equal($sasa_phone_work_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_4sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciessetvalueconDire3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciessetvalueconDire3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_direccion3_c'),
	'trigger' => 'equal($sasa_direccion3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_ciudad3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_departamento3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_codigopostal3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_pais3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_6sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipTel3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_other_c'),
	'trigger' => 'equal($sasa_phone_other_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_5sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciessetvaluecon2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciessetvaluecon2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_work_c','sasa_tipotel2_c','sasa_phone_other_c'),
	'trigger' => 'equal($sasa_phone_work_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOTipDirec2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOTipDirec2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec2_c','alt_address_street'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'alt_address_street',
				'value' => '
					not(equal($sasa_tipodirec2_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => '
					not(equal($alt_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'alt_address_street',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciestiptelcon2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciestiptelcon2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_work_c','sasa_tipotel2_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_work_c',
				'value' => '
					not(equal($sasa_tipotel2_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '
					not(equal($sasa_phone_work_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_work_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel1.3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipTel1.3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_municipios_contacts_3_name'),
	'trigger' => 'equal($sasa_municipios_contacts_3_name,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_home_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipDir3.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipDir3.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec3_c'),
	'trigger' => 'equal($sasa_tipodirec3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_direccion3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel3.3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipTel3.3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_municipios_contacts_5_name'),
	'trigger' => 'equal($sasa_municipios_contacts_5_name,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel1.4.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
/*$dependencies['Contacts']['dependenciesCOSetvalueTipTel1.4'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_extension_c'),
	'trigger' => 'equal($sasa_extension_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_home_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_3sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);*/

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependencies3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependencies3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_other_c','sasa_municipios_contacts_5_name'),
	//'trigger' => 'equal($sasa_phone_other_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_5_name',
				'value' => '
					not(equal($sasa_phone_other_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_5_name',
				'value' => '
					not(equal($sasa_phone_other_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_contacts_5_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_5_name',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipTel1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_home_c'),
	'trigger' => 'equal($sasa_phone_home_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_3sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel2.4.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
/*$dependencies['Contacts']['dependenciesCOSetvalueTipTel2.4'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_extension2_c'),
	'trigger' => 'equal($sasa_extension2_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_work_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_4sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);*/

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipDir3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipDir3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_direccion3_c'),
	'trigger' => 'equal($sasa_direccion3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipDir2.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipDir2.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec2_c'),
	'trigger' => 'equal($sasa_tipodirec2_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'alt_address_street',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipDir2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipDir2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('alt_address_street'),
	'trigger' => 'equal($alt_address_street,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipDir1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipDir1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('primary_address_street'),
	'trigger' => 'equal($primary_address_street,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec1_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipDir1.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipDir1.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec1_c'),
	'trigger' => 'equal($sasa_tipodirec1_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_street',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel1.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipTel1.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipotel1_c'),
	'trigger' => 'equal($sasa_tipotel1_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_home_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_3sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciessetvalueconDire2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciessetvalueconDire2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('alt_address_street'),
	'trigger' => 'equal($alt_address_street,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'alt_address_city',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'alt_address_state',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'alt_address_postalcode',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'alt_address_country',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_2sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOTipDirec3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOTipDirec3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec3_c','sasa_direccion3_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_direccion3_c',
				'value' => '
					not(equal($sasa_tipodirec3_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => '
					not(equal($sasa_direccion3_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_direccion3_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciestiptelcon3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciestiptelcon3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_other_c','sasa_tipotel3_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '
					not(equal($sasa_tipotel3_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '
					not(equal($sasa_phone_other_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel2.3.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipTel2.3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_municipios_contacts_4_name'),
	'trigger' => 'equal($sasa_municipios_contacts_4_name,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_work_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciestiptelcon1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciestiptelcon1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_home_c','sasa_tipotel1_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_home_c',
				'value' => '
					not(equal($sasa_tipotel1_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => '
					not(equal($sasa_phone_home_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_home_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel1_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel3.2.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOSetvalueTipTel3.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipotel3_c'),
	'trigger' => 'equal($sasa_tipotel3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_5sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension3_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciessetvaluecon1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciessetvaluecon1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_home_c','sasa_tipotel1_c','sasa_phone_work_c'),
	'trigger' => 'equal($sasa_phone_home_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_work_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependencies4.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependencies4'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('alt_address_street'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_caja_direccion3_c',
				'value' => '
					or(not(equal($sasa_direccion3_c,0)),not(equal($alt_address_street,0)))
				',
			),
		),
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => '
					not(equal($alt_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_caja_direccion3_c',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => 'false'
			)
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOTipDirec1.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOTipDirec1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec1_c','primary_address_street'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_street',
				'value' => '
					not(equal($sasa_tipodirec1_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec1_c',
				'value' => '
					not(equal($primary_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_street',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec1_c',
				'value' => 'false',
			),
		),
	)
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/dependenciesCOSetvalueTipTel3.4.php

//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
/*$dependencies['Contacts']['dependenciesCOSetvalueTipTel3.4'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_extension3_c'),
	'trigger' => 'equal($sasa_extension3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_other_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_5sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);*/

?>

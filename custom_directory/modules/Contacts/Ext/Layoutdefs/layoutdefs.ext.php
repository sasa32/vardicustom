<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/accounts_contacts_1_Contacts.php

 // created: 2018-12-04 16:24:59
$layout_defs["Contacts"]["subpanel_setup"]['accounts_contacts_1'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'accounts_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/contacts_sasa_habeas_data_1_Contacts.php

 // created: 2020-07-30 15:35:59
$layout_defs["Contacts"]["subpanel_setup"]['contacts_sasa_habeas_data_1'] = array (
  'order' => 100,
  'module' => 'SASA_Habeas_Data',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'get_subpanel_data' => 'contacts_sasa_habeas_data_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/contacts_cases_1_Contacts.php

 // created: 2023-03-06 03:26:09
$layout_defs["Contacts"]["subpanel_setup"]['contacts_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'contacts_cases_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/_overrideContact_subpanel_contacts_sasa_habeas_data_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_sasa_habeas_data_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_sasa_habeas_data_1';

?>

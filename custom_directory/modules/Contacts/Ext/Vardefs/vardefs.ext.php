<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_birthdate.php

 // created: 2020-05-16 16:43:23
$dictionary['Contact']['fields']['birthdate']['comments']='The birthdate of the contact';
$dictionary['Contact']['fields']['birthdate']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['birthdate']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['birthdate']['merge_filter']='disabled';
$dictionary['Contact']['fields']['birthdate']['calculated']=false;
$dictionary['Contact']['fields']['birthdate']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cookie_consent_received_on.php

 // created: 2020-05-14 18:41:00
$dictionary['Contact']['fields']['cookie_consent_received_on']['massupdate']=false;
$dictionary['Contact']['fields']['cookie_consent_received_on']['comments']='Date cookie consent received on';
$dictionary['Contact']['fields']['cookie_consent_received_on']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['cookie_consent_received_on']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['cookie_consent_received_on']['merge_filter']='disabled';
$dictionary['Contact']['fields']['cookie_consent_received_on']['calculated']=false;
$dictionary['Contact']['fields']['cookie_consent_received_on']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_do_not_call.php

 // created: 2020-05-14 17:53:40
$dictionary['Contact']['fields']['do_not_call']['default']=false;
$dictionary['Contact']['fields']['do_not_call']['audited']=false;
$dictionary['Contact']['fields']['do_not_call']['massupdate']=false;
$dictionary['Contact']['fields']['do_not_call']['comments']='An indicator of whether contact can be called';
$dictionary['Contact']['fields']['do_not_call']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['do_not_call']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['do_not_call']['merge_filter']='disabled';
$dictionary['Contact']['fields']['do_not_call']['unified_search']=false;
$dictionary['Contact']['fields']['do_not_call']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_lead_source.php

 // created: 2020-05-14 18:28:19
$dictionary['Contact']['fields']['lead_source']['len']=100;
$dictionary['Contact']['fields']['lead_source']['audited']=false;
$dictionary['Contact']['fields']['lead_source']['massupdate']=false;
$dictionary['Contact']['fields']['lead_source']['comments']='How did the contact come about';
$dictionary['Contact']['fields']['lead_source']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['lead_source']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Contact']['fields']['lead_source']['calculated']=false;
$dictionary['Contact']['fields']['lead_source']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_salutation.php

 // created: 2020-05-14 17:46:33
$dictionary['Contact']['fields']['salutation']['len']=100;
$dictionary['Contact']['fields']['salutation']['audited']=false;
$dictionary['Contact']['fields']['salutation']['massupdate']=true;
$dictionary['Contact']['fields']['salutation']['options']='salutation_list';
$dictionary['Contact']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Contact']['fields']['salutation']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['salutation']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['salutation']['merge_filter']='disabled';
$dictionary['Contact']['fields']['salutation']['calculated']=false;
$dictionary['Contact']['fields']['salutation']['dependency']=false;
$dictionary['Contact']['fields']['salutation']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_paises_contacts_1_Contacts.php

// created: 2020-05-11 19:17:42
$dictionary["Contact"]["fields"]["sasa_paises_contacts_1"] = array (
  'name' => 'sasa_paises_contacts_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_contacts_1',
  'source' => 'non-db',
  'module' => 'sasa_Paises',
  'bean_name' => 'sasa_Paises',
  'side' => 'right',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_paises_contacts_1sasa_paises_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_paises_contacts_1_name"] = array (
  'name' => 'sasa_paises_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_1_FROM_SASA_PAISES_TITLE',
  'save' => true,
  'id_name' => 'sasa_paises_contacts_1sasa_paises_ida',
  'link' => 'sasa_paises_contacts_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_paises_contacts_1sasa_paises_ida"] = array (
  'name' => 'sasa_paises_contacts_1sasa_paises_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_1_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_paises_contacts_1sasa_paises_ida',
  'link' => 'sasa_paises_contacts_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_departamentos_contacts_1_Contacts.php

// created: 2020-05-11 19:19:23
$dictionary["Contact"]["fields"]["sasa_departamentos_contacts_1"] = array (
  'name' => 'sasa_departamentos_contacts_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_contacts_1',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'side' => 'right',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_departamentos_contacts_1sasa_departamentos_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_departamentos_contacts_1_name"] = array (
  'name' => 'sasa_departamentos_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_departamentos_contacts_1sasa_departamentos_ida',
  'link' => 'sasa_departamentos_contacts_1',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_departamentos_contacts_1sasa_departamentos_ida"] = array (
  'name' => 'sasa_departamentos_contacts_1sasa_departamentos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_departamentos_contacts_1sasa_departamentos_ida',
  'link' => 'sasa_departamentos_contacts_1',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_municipios_contacts_1_Contacts.php

// created: 2020-05-11 19:31:08
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_1"] = array (
  'name' => 'sasa_municipios_contacts_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_1',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_municipios_contacts_1sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_1_name"] = array (
  'name' => 'sasa_municipios_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_contacts_1sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_1',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_1sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_contacts_1sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_municipios_contacts_1sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_1',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_paises_contacts_2_Contacts.php

// created: 2020-05-11 19:32:06
$dictionary["Contact"]["fields"]["sasa_paises_contacts_2"] = array (
  'name' => 'sasa_paises_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_contacts_2',
  'source' => 'non-db',
  'module' => 'sasa_Paises',
  'bean_name' => 'sasa_Paises',
  'side' => 'right',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_paises_contacts_2_name"] = array (
  'name' => 'sasa_paises_contacts_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_SASA_PAISES_TITLE',
  'save' => true,
  'id_name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'link' => 'sasa_paises_contacts_2',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_paises_contacts_2sasa_paises_ida"] = array (
  'name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'link' => 'sasa_paises_contacts_2',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_departamentos_contacts_2_Contacts.php

// created: 2020-05-11 19:32:57
$dictionary["Contact"]["fields"]["sasa_departamentos_contacts_2"] = array (
  'name' => 'sasa_departamentos_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_contacts_2',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'side' => 'right',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_departamentos_contacts_2_name"] = array (
  'name' => 'sasa_departamentos_contacts_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_contacts_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_departamentos_contacts_2sasa_departamentos_ida"] = array (
  'name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_contacts_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_municipios_contacts_2_Contacts.php

// created: 2020-05-11 19:34:03
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_2"] = array (
  'name' => 'sasa_municipios_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_2',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_2_name"] = array (
  'name' => 'sasa_municipios_contacts_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_2',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_2sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_2',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_municipios_contacts_3_Contacts.php

// created: 2020-05-14 17:39:25
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_3"] = array (
  'name' => 'sasa_municipios_contacts_3',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_3',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_municipios_contacts_3sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_3_name"] = array (
  'name' => 'sasa_municipios_contacts_3_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_contacts_3sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_3',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_3sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_contacts_3sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_municipios_contacts_3sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_3',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_municipios_contacts_4_Contacts.php

// created: 2020-05-14 17:40:34
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_4"] = array (
  'name' => 'sasa_municipios_contacts_4',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_4',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_municipios_contacts_4sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_4_name"] = array (
  'name' => 'sasa_municipios_contacts_4_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_contacts_4sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_4',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_4sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_contacts_4sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_municipios_contacts_4sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_4',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_municipios_contacts_5_Contacts.php

// created: 2020-05-14 17:41:15
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_5"] = array (
  'name' => 'sasa_municipios_contacts_5',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_5',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_municipios_contacts_5sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_5_name"] = array (
  'name' => 'sasa_municipios_contacts_5_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_contacts_5sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_5',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_5sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_contacts_5sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_municipios_contacts_5sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_5',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2020-05-16 10:27:53
$dictionary['Contact']['fields']['date_entered']['audited']=false;
$dictionary['Contact']['fields']['date_entered']['comments']='Date record created';
$dictionary['Contact']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Contact']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Contact']['fields']['date_entered']['calculated']=false;
$dictionary['Contact']['fields']['date_entered']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2020-05-16 10:28:22
$dictionary['Contact']['fields']['date_modified']['audited']=false;
$dictionary['Contact']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Contact']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Contact']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Contact']['fields']['date_modified']['calculated']=false;
$dictionary['Contact']['fields']['date_modified']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_mkto_sync.php

 // created: 2020-05-17 00:02:10
$dictionary['Contact']['fields']['mkto_sync']['default']=false;
$dictionary['Contact']['fields']['mkto_sync']['audited']=false;
$dictionary['Contact']['fields']['mkto_sync']['massupdate']=false;
$dictionary['Contact']['fields']['mkto_sync']['comments']='Should the Lead be synced to Marketo';
$dictionary['Contact']['fields']['mkto_sync']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['mkto_sync']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['mkto_sync']['merge_filter']='disabled';
$dictionary['Contact']['fields']['mkto_sync']['reportable']=false;
$dictionary['Contact']['fields']['mkto_sync']['unified_search']=false;
$dictionary['Contact']['fields']['mkto_sync']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_mkto_id.php

 // created: 2020-05-17 00:02:45
$dictionary['Contact']['fields']['mkto_id']['len']='11';
$dictionary['Contact']['fields']['mkto_id']['audited']=false;
$dictionary['Contact']['fields']['mkto_id']['massupdate']=false;
$dictionary['Contact']['fields']['mkto_id']['comments']='Associated Marketo Lead ID';
$dictionary['Contact']['fields']['mkto_id']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['mkto_id']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['mkto_id']['merge_filter']='disabled';
$dictionary['Contact']['fields']['mkto_id']['reportable']=false;
$dictionary['Contact']['fields']['mkto_id']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['mkto_id']['calculated']=false;
$dictionary['Contact']['fields']['mkto_id']['enable_range_search']=false;
$dictionary['Contact']['fields']['mkto_id']['min']=false;
$dictionary['Contact']['fields']['mkto_id']['max']=false;
$dictionary['Contact']['fields']['mkto_id']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_mkto_lead_score.php

 // created: 2020-05-17 00:04:41
$dictionary['Contact']['fields']['mkto_lead_score']['len']='11';
$dictionary['Contact']['fields']['mkto_lead_score']['audited']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['massupdate']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['mkto_lead_score']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['mkto_lead_score']['merge_filter']='disabled';
$dictionary['Contact']['fields']['mkto_lead_score']['reportable']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['mkto_lead_score']['calculated']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['enable_range_search']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['min']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['max']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/accounts_contacts_1_Contacts.php

// created: 2018-12-04 16:24:59
$dictionary["Contact"]["fields"]["accounts_contacts_1"] = array (
  'name' => 'accounts_contacts_1',
  'type' => 'link',
  'relationship' => 'accounts_contacts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'account_id',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/contacts_sasa_habeas_data_1_Contacts.php

// created: 2020-07-30 15:35:59
$dictionary["Contact"]["fields"]["contacts_sasa_habeas_data_1"] = array (
  'name' => 'contacts_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'contacts_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'vname' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_other.php

 // created: 2020-07-31 13:06:43
$dictionary['Contact']['fields']['phone_other']['len']='255';
$dictionary['Contact']['fields']['phone_other']['massupdate']=false;
$dictionary['Contact']['fields']['phone_other']['comments']='Other phone number for the contact';
$dictionary['Contact']['fields']['phone_other']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_other']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['phone_other']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_other']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.07',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_other']['calculated']='1';
$dictionary['Contact']['fields']['phone_other']['pii']=false;
$dictionary['Contact']['fields']['phone_other']['audited']=false;
$dictionary['Contact']['fields']['phone_other']['importable']='false';
$dictionary['Contact']['fields']['phone_other']['formula']='concat(related($sasa_municipios_contacts_4,"sasa_indicativo_c"),$sasa_phone_other_c)';
$dictionary['Contact']['fields']['phone_other']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sasa_municipios_contacts_6_Contacts.php

// created: 2020-07-31 14:28:57
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_6"] = array (
  'name' => 'sasa_municipios_contacts_6',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_6',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_6_name"] = array (
  'name' => 'sasa_municipios_contacts_6_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_6',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_6sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_6',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_googleplus.php

 // created: 2020-07-15 17:06:35
$dictionary['Contact']['fields']['googleplus']['audited']=false;
$dictionary['Contact']['fields']['googleplus']['massupdate']=false;
$dictionary['Contact']['fields']['googleplus']['comments']='The google plus id of the user';
$dictionary['Contact']['fields']['googleplus']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['googleplus']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['googleplus']['merge_filter']='disabled';
$dictionary['Contact']['fields']['googleplus']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['googleplus']['calculated']=false;
$dictionary['Contact']['fields']['googleplus']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_city.php

 // created: 2020-07-01 15:54:50
$dictionary['Contact']['fields']['primary_address_city']['audited']=true;
$dictionary['Contact']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Contact']['fields']['primary_address_city']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_city']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_city']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_assistant_phone.php

 // created: 2020-07-15 17:27:31
$dictionary['Contact']['fields']['assistant_phone']['len']='100';
$dictionary['Contact']['fields']['assistant_phone']['audited']=false;
$dictionary['Contact']['fields']['assistant_phone']['massupdate']=false;
$dictionary['Contact']['fields']['assistant_phone']['comments']='Phone number of the assistant of the contact';
$dictionary['Contact']['fields']['assistant_phone']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['assistant_phone']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['assistant_phone']['merge_filter']='disabled';
$dictionary['Contact']['fields']['assistant_phone']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['assistant_phone']['calculated']=false;
$dictionary['Contact']['fields']['assistant_phone']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_assistant.php

 // created: 2020-07-15 17:27:21
$dictionary['Contact']['fields']['assistant']['audited']=false;
$dictionary['Contact']['fields']['assistant']['massupdate']=false;
$dictionary['Contact']['fields']['assistant']['comments']='Name of the assistant of the contact';
$dictionary['Contact']['fields']['assistant']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['assistant']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['assistant']['merge_filter']='disabled';
$dictionary['Contact']['fields']['assistant']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['assistant']['calculated']=false;
$dictionary['Contact']['fields']['assistant']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_fax.php

 // created: 2020-07-15 17:11:03
$dictionary['Contact']['fields']['phone_fax']['len']='100';
$dictionary['Contact']['fields']['phone_fax']['audited']=false;
$dictionary['Contact']['fields']['phone_fax']['massupdate']=false;
$dictionary['Contact']['fields']['phone_fax']['comments']='Contact fax number';
$dictionary['Contact']['fields']['phone_fax']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_fax']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_fax']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_fax']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.06',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_fax']['calculated']=false;
$dictionary['Contact']['fields']['phone_fax']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_department.php

 // created: 2020-07-15 17:07:37
$dictionary['Contact']['fields']['department']['audited']=false;
$dictionary['Contact']['fields']['department']['massupdate']=false;
$dictionary['Contact']['fields']['department']['comments']='The department of the contact';
$dictionary['Contact']['fields']['department']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['department']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['department']['merge_filter']='disabled';
$dictionary['Contact']['fields']['department']['reportable']=false;
$dictionary['Contact']['fields']['department']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['department']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_state.php

 // created: 2020-07-31 13:17:15
$dictionary['Contact']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Contact']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_state']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_state']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_postalcode.php

 // created: 2020-07-31 13:17:05
$dictionary['Contact']['fields']['primary_address_postalcode']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Contact']['fields']['primary_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_postalcode']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_postalcode']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_state.php

 // created: 2020-07-31 13:17:56
$dictionary['Contact']['fields']['alt_address_state']['audited']=true;
$dictionary['Contact']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Contact']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_state']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_state']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_city.php

 // created: 2020-07-01 15:57:12
$dictionary['Contact']['fields']['alt_address_city']['audited']=true;
$dictionary['Contact']['fields']['alt_address_city']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_city']['comments']='City for alternate address';
$dictionary['Contact']['fields']['alt_address_city']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_city']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_city']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_city']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_country.php

 // created: 2020-07-31 13:17:24
$dictionary['Contact']['fields']['primary_address_country']['len']='255';
$dictionary['Contact']['fields']['primary_address_country']['audited']=false;
$dictionary['Contact']['fields']['primary_address_country']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Contact']['fields']['primary_address_country']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_country']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_country']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_dp_business_purpose.php

 // created: 2020-07-15 17:30:43
$dictionary['Contact']['fields']['dp_business_purpose']['len']=NULL;
$dictionary['Contact']['fields']['dp_business_purpose']['audited']=false;
$dictionary['Contact']['fields']['dp_business_purpose']['massupdate']=false;
$dictionary['Contact']['fields']['dp_business_purpose']['comments']='Business purposes consented for';
$dictionary['Contact']['fields']['dp_business_purpose']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['dp_business_purpose']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['dp_business_purpose']['merge_filter']='disabled';
$dictionary['Contact']['fields']['dp_business_purpose']['calculated']=false;
$dictionary['Contact']['fields']['dp_business_purpose']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_mobile.php

 // created: 2020-07-31 13:08:05
$dictionary['Contact']['fields']['phone_mobile']['len']='255';
$dictionary['Contact']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Contact']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.09',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_mobile']['calculated']='1';
$dictionary['Contact']['fields']['phone_mobile']['pii']=false;
$dictionary['Contact']['fields']['phone_mobile']['audited']=false;
$dictionary['Contact']['fields']['phone_mobile']['importable']='false';
$dictionary['Contact']['fields']['phone_mobile']['formula']='concat("+57",$sasa_phone_mobile_c)';
$dictionary['Contact']['fields']['phone_mobile']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_home.php

 // created: 2020-07-31 13:06:13
$dictionary['Contact']['fields']['phone_home']['len']='255';
$dictionary['Contact']['fields']['phone_home']['massupdate']=false;
$dictionary['Contact']['fields']['phone_home']['comments']='';
$dictionary['Contact']['fields']['phone_home']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_home']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_home']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_home']['calculated']='1';
$dictionary['Contact']['fields']['phone_home']['pii']=false;
$dictionary['Contact']['fields']['phone_home']['audited']=false;
$dictionary['Contact']['fields']['phone_home']['importable']='false';
$dictionary['Contact']['fields']['phone_home']['formula']='concat(related($sasa_municipios_contacts_3,"sasa_indicativo_c"),$sasa_phone_home_c)';
$dictionary['Contact']['fields']['phone_home']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_last_name.php

 // created: 2020-07-07 08:47:00
$dictionary['Contact']['fields']['last_name']['massupdate']=false;
$dictionary['Contact']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Contact']['fields']['last_name']['importable']='false';
$dictionary['Contact']['fields']['last_name']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.97',
  'searchable' => true,
);
$dictionary['Contact']['fields']['last_name']['calculated']='1';
$dictionary['Contact']['fields']['last_name']['formula']='concat($sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['Contact']['fields']['last_name']['enforced']=true;
$dictionary['Contact']['fields']['last_name']['pii']=false;
$dictionary['Contact']['fields']['last_name']['audited']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_first_name.php

 // created: 2020-07-15 17:05:51
$dictionary['Contact']['fields']['first_name']['required']=false;
$dictionary['Contact']['fields']['first_name']['massupdate']=false;
$dictionary['Contact']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Contact']['fields']['first_name']['importable']='false';
$dictionary['Contact']['fields']['first_name']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['first_name']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['first_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['first_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.99',
  'searchable' => true,
);
$dictionary['Contact']['fields']['first_name']['calculated']='1';
$dictionary['Contact']['fields']['first_name']['formula']='$sasa_nombres_c';
$dictionary['Contact']['fields']['first_name']['enforced']=true;
$dictionary['Contact']['fields']['first_name']['pii']=false;
$dictionary['Contact']['fields']['first_name']['audited']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_dp_consent_last_updated.php

 // created: 2020-07-15 17:33:02
$dictionary['Contact']['fields']['dp_consent_last_updated']['massupdate']=false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['comments']='Date consent last updated';
$dictionary['Contact']['fields']['dp_consent_last_updated']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['dp_consent_last_updated']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['dp_consent_last_updated']['merge_filter']='disabled';
$dictionary['Contact']['fields']['dp_consent_last_updated']['calculated']=false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['enable_range_search']=false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['audited']=false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['options']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_street.php

 // created: 2020-07-31 13:17:37
$dictionary['Contact']['fields']['alt_address_street']['audited']=true;
$dictionary['Contact']['fields']['alt_address_street']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Contact']['fields']['alt_address_street']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_street']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.32',
  'searchable' => true,
);
$dictionary['Contact']['fields']['alt_address_street']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_street']['pii']=false;
$dictionary['Contact']['fields']['alt_address_street']['rows']='4';
$dictionary['Contact']['fields']['alt_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_work.php

 // created: 2020-07-31 13:06:29
$dictionary['Contact']['fields']['phone_work']['len']='255';
$dictionary['Contact']['fields']['phone_work']['massupdate']=false;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_work']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.08',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_work']['calculated']='1';
$dictionary['Contact']['fields']['phone_work']['pii']=false;
$dictionary['Contact']['fields']['phone_work']['audited']=false;
$dictionary['Contact']['fields']['phone_work']['importable']='false';
$dictionary['Contact']['fields']['phone_work']['formula']='concat(related($sasa_municipios_contacts_5,"sasa_indicativo_c"),$sasa_phone_work_c)';
$dictionary['Contact']['fields']['phone_work']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_postalcode.php

 // created: 2020-07-31 13:18:07
$dictionary['Contact']['fields']['alt_address_postalcode']['audited']=true;
$dictionary['Contact']['fields']['alt_address_postalcode']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Contact']['fields']['alt_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_postalcode']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_postalcode']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_postalcode']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_country.php

 // created: 2020-07-01 16:00:36
$dictionary['Contact']['fields']['alt_address_country']['len']='255';
$dictionary['Contact']['fields']['alt_address_country']['audited']=false;
$dictionary['Contact']['fields']['alt_address_country']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_country']['comments']='Country for alternate address';
$dictionary['Contact']['fields']['alt_address_country']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_country']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_country']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/readonly.php

/*
* Requerimiento en https://sasaconsultoria.sugarondemand.com/index.php#Tasks/5ea23328-7e7a-11ea-b3b6-02fb8f607ac4
*/
$dictionary["Contact"]["fields"]["primary_address_city"]['readonly'] = true;
$dictionary["Contact"]["fields"]["primary_address_state"]['readonly'] = true;
$dictionary["Contact"]["fields"]["primary_address_country"]['readonly'] = true;

$dictionary["Contact"]["fields"]["alt_address_city"]['readonly'] = true;
$dictionary["Contact"]["fields"]["alt_address_state"]['readonly'] = true;
$dictionary["Contact"]["fields"]["alt_address_country"]['readonly'] = true;

$dictionary["Contact"]["fields"]["sasa_ciudad3_c"]['readonly'] = true;
$dictionary["Contact"]["fields"]["sasa_departamento3_c"]['readonly'] = true;
$dictionary["Contact"]["fields"]["sasa_pais3_c"]['readonly'] = true;


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_cargo_c.php

 // created: 2020-09-30 16:30:13
$dictionary['Contact']['fields']['sasa_cargo_c']['labelValue']='Cargo';
$dictionary['Contact']['fields']['sasa_cargo_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_cargo_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_cliente_fallecido_c.php

 // created: 2020-09-30 16:30:13
$dictionary['Contact']['fields']['sasa_cliente_fallecido_c']['labelValue']='sasa cliente fallecido c';
$dictionary['Contact']['fields']['sasa_cliente_fallecido_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_cliente_fallecido_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_ciudad_tel_principal_2_c.php

 // created: 2020-09-30 16:30:13

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_ciudad_tel_alternativ_2_c.php

 // created: 2020-09-30 16:30:13

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_ciudad_tel_oficina_2_c.php

 // created: 2020-09-30 16:30:13

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_estrato_c.php

 // created: 2020-09-30 16:30:14
$dictionary['Contact']['fields']['sasa_estrato_c']['labelValue']='Nivel Socioeconómico (Estrato)';
$dictionary['Contact']['fields']['sasa_estrato_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_estrato_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_mascota_c.php

 // created: 2020-09-30 16:30:14
$dictionary['Contact']['fields']['sasa_mascota_c']['labelValue']='Mascota';
$dictionary['Contact']['fields']['sasa_mascota_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_mascota_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_estado_civil_c.php

 // created: 2020-09-30 16:30:14
$dictionary['Contact']['fields']['sasa_estado_civil_c']['labelValue']='Estado Civil';
$dictionary['Contact']['fields']['sasa_estado_civil_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_estado_civil_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_edad_c.php

 // created: 2020-09-30 16:30:14
$dictionary['Contact']['fields']['sasa_edad_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['sasa_edad_c']['labelValue']='Edad';
$dictionary['Contact']['fields']['sasa_edad_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_edad_c']['calculated']='true';
$dictionary['Contact']['fields']['sasa_edad_c']['formula']='floor(divide(subtract(daysUntil(today()),daysUntil($birthdate)),365.242))';
$dictionary['Contact']['fields']['sasa_edad_c']['enforced']='true';
$dictionary['Contact']['fields']['sasa_edad_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_deportes_c.php

 // created: 2020-09-30 16:30:14
$dictionary['Contact']['fields']['sasa_deportes_c']['labelValue']='Deportes';
$dictionary['Contact']['fields']['sasa_deportes_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_deportes_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_last_name_2_c.php

 // created: 2020-09-30 16:30:14
$dictionary['Contact']['fields']['sasa_last_name_2_c']['labelValue']='Segundo Apellido';
$dictionary['Contact']['fields']['sasa_last_name_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_last_name_2_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_last_name_2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_municipio_3_c.php

 // created: 2020-09-30 16:30:15

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_municipio_secundario_2_c.php

 // created: 2020-09-30 16:30:15

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_numero_documento_c.php

 // created: 2020-09-30 16:30:15

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_ocupacion_c.php

 // created: 2020-09-30 16:30:15
$dictionary['Contact']['fields']['sasa_ocupacion_c']['labelValue']='Ocupación';
$dictionary['Contact']['fields']['sasa_ocupacion_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_ocupacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_numero_hijos_c.php

 // created: 2020-09-30 16:30:15
$dictionary['Contact']['fields']['sasa_numero_hijos_c']['labelValue']='Número de Hijos';
$dictionary['Contact']['fields']['sasa_numero_hijos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_numero_hijos_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_numero_hijos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_nombres_c.php

 // created: 2020-09-30 16:30:15
$dictionary['Contact']['fields']['sasa_nombres_c']['labelValue']='Nombres';
$dictionary['Contact']['fields']['sasa_nombres_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_nombres_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_nombres_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_nivel_educativo_c.php

 // created: 2020-09-30 16:30:15
$dictionary['Contact']['fields']['sasa_nivel_educativo_c']['labelValue']='Nivel Educativo';
$dictionary['Contact']['fields']['sasa_nivel_educativo_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_nivel_educativo_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_musica_c.php

 // created: 2020-09-30 16:30:15
$dictionary['Contact']['fields']['sasa_musica_c']['labelValue']='Música';
$dictionary['Contact']['fields']['sasa_musica_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_musica_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tiene_hijos_c.php

 // created: 2020-09-30 16:30:16
$dictionary['Contact']['fields']['sasa_tiene_hijos_c']['labelValue']='Tiene Hijos';
$dictionary['Contact']['fields']['sasa_tiene_hijos_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_tiene_hijos_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_profesion_c.php

 // created: 2020-09-30 16:30:16
$dictionary['Contact']['fields']['sasa_profesion_c']['labelValue']='Profesión';
$dictionary['Contact']['fields']['sasa_profesion_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_profesion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_primerapellido_c.php

 // created: 2020-09-30 16:30:16
$dictionary['Contact']['fields']['sasa_primerapellido_c']['labelValue']='Primer Apellido';
$dictionary['Contact']['fields']['sasa_primerapellido_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_primerapellido_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_primerapellido_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipo_de_vivienda_c.php

 // created: 2020-09-30 16:30:17
$dictionary['Contact']['fields']['sasa_tipo_de_vivienda_c']['labelValue']='Tipo de Vivienda';
$dictionary['Contact']['fields']['sasa_tipo_de_vivienda_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_tipo_de_vivienda_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_street.php

 // created: 2021-02-02 09:16:57
$dictionary['Contact']['fields']['primary_address_street']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_street']['comments']='The street address used for primary address';
$dictionary['Contact']['fields']['primary_address_street']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.33',
  'searchable' => true,
);
$dictionary['Contact']['fields']['primary_address_street']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_street']['pii']=false;
$dictionary['Contact']['fields']['primary_address_street']['rows']='4';
$dictionary['Contact']['fields']['primary_address_street']['cols']='20';
$dictionary['Contact']['fields']['primary_address_street']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_municipio_principal_2_c.php

 // created: 2021-02-02 09:17:09
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['labelValue']='Ciudad Dirección 1 (Integración)';
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/denorm_account_name.php


// 'account_name'
$dictionary['Contact']['fields']['account_name']['is_denormalized'] = true;
$dictionary['Contact']['fields']['account_name']['denormalized_field_name'] = 'denorm_account_name';

// 'denorm_account_name'
$dictionary['Contact']['fields']['denorm_account_name']['name'] = 'denorm_account_name';
$dictionary['Contact']['fields']['denorm_account_name']['type'] = 'varchar';
$dictionary['Contact']['fields']['denorm_account_name']['dbType'] = 'varchar';
$dictionary['Contact']['fields']['denorm_account_name']['vname'] = 'LBL_ACCOUNT_NAME';
$dictionary['Contact']['fields']['denorm_account_name']['len'] = '150';
$dictionary['Contact']['fields']['denorm_account_name']['comment'] = 'Name of the Company';
$dictionary['Contact']['fields']['denorm_account_name']['unified_search'] = true;
$dictionary['Contact']['fields']['denorm_account_name']['full_text_search'] = array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['Contact']['fields']['denorm_account_name']['audited'] = false;
$dictionary['Contact']['fields']['denorm_account_name']['required'] = false;
$dictionary['Contact']['fields']['denorm_account_name']['importable'] = 'false';
$dictionary['Contact']['fields']['denorm_account_name']['duplicate_on_record_copy'] = 'always';
$dictionary['Contact']['fields']['denorm_account_name']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['denorm_account_name']['massupdate'] = false;
$dictionary['Contact']['fields']['denorm_account_name']['comments'] = 'Name of the Company';
$dictionary['Contact']['fields']['denorm_account_name']['duplicate_merge'] = 'disabled';
$dictionary['Contact']['fields']['denorm_account_name']['duplicate_merge_dom_value'] = 0;
$dictionary['Contact']['fields']['denorm_account_name']['calculated'] = '1';
$dictionary['Contact']['fields']['denorm_account_name']['formula'] = 'concat($sasa_nombres_c," ",$sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['Contact']['fields']['denorm_account_name']['enforced'] = true;
$dictionary['Contact']['fields']['denorm_account_name']['denorm_from_module'] = 'Accounts';
$dictionary['Contact']['fields']['denorm_account_name']['studio'] = false;

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_title.php

 // created: 2021-02-18 21:28:08
$dictionary['Contact']['fields']['title']['audited']=false;
$dictionary['Contact']['fields']['title']['massupdate']=false;
$dictionary['Contact']['fields']['title']['comments']='The title of the contact';
$dictionary['Contact']['fields']['title']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['title']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['title']['merge_filter']='disabled';
$dictionary['Contact']['fields']['title']['reportable']=false;
$dictionary['Contact']['fields']['title']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['title']['calculated']=false;
$dictionary['Contact']['fields']['title']['pii']=false;
$dictionary['Contact']['fields']['title']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_celular_otro_c.php

 // created: 2020-07-31 13:37:11
$dictionary['Contact']['fields']['sasa_celular_otro_c']['labelValue']='Celular 3';
$dictionary['Contact']['fields']['sasa_celular_otro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_celular_otro_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_celular_otro_c']['dependency']='not(equal($sasa_cel_alternativo_c,""))';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_codigopostal3_c.php

 // created: 2021-07-09 17:45:36
$dictionary['Contact']['fields']['sasa_codigopostal3_c']['labelValue']='Código Postal 3 ';
$dictionary['Contact']['fields']['sasa_codigopostal3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_codigopostal3_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_codigopostal3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_cel_otro_c.php

 // created: 2020-07-31 14:21:57
$dictionary['Contact']['fields']['sasa_cel_otro_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['sasa_cel_otro_c']['labelValue']='Cel 3';
$dictionary['Contact']['fields']['sasa_cel_otro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_cel_otro_c']['calculated']='true';
$dictionary['Contact']['fields']['sasa_cel_otro_c']['formula']='concat("+57",$sasa_celular_otro_c)';
$dictionary['Contact']['fields']['sasa_cel_otro_c']['enforced']='true';
$dictionary['Contact']['fields']['sasa_cel_otro_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_celular_alternativo_c.php

 // created: 2020-07-31 13:07:52
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['labelValue']='Cel 2';
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['calculated']='1';
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['formula']='concat("+57",$sasa_cel_alternativo_c)';
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['enforced']='1';
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_cel_alternativo_c.php

 // created: 2020-07-31 13:35:45
$dictionary['Contact']['fields']['sasa_cel_alternativo_c']['labelValue']='Celular 2';
$dictionary['Contact']['fields']['sasa_cel_alternativo_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_cel_alternativo_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_cel_alternativo_c']['dependency']='not(equal($sasa_phone_mobile_c,""))';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_ciudad3_c.php

 // created: 2020-07-31 09:28:43
$dictionary['Contact']['fields']['sasa_ciudad3_c']['labelValue']='Ciudad 3';
$dictionary['Contact']['fields']['sasa_ciudad3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_ciudad3_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_ciudad3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_departamento3_c.php

 // created: 2021-07-09 17:45:39
$dictionary['Contact']['fields']['sasa_departamento3_c']['labelValue']='Departamento 3';
$dictionary['Contact']['fields']['sasa_departamento3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_departamento3_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_departamento3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_extension_c.php

 // created: 2021-07-09 17:45:59
$dictionary['Contact']['fields']['sasa_extension_c']['labelValue']=' Extensión Teléfono 1';
$dictionary['Contact']['fields']['sasa_extension_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_extension_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_extension_c']['dependency']='equal($sasa_tipotel1_c,"OF")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_extension3_c.php

 // created: 2021-07-09 17:45:57
$dictionary['Contact']['fields']['sasa_extension3_c']['labelValue']='Extensión Teléfono 3';
$dictionary['Contact']['fields']['sasa_extension3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_extension3_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_extension3_c']['dependency']='equal($sasa_tipotel3_c,"OF")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_extension2_c.php

 // created: 2021-07-09 17:45:55
$dictionary['Contact']['fields']['sasa_extension2_c']['labelValue']='Extensión Teléfono 2';
$dictionary['Contact']['fields']['sasa_extension2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_extension2_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_extension2_c']['dependency']='equal($sasa_tipotel2_c,"OF")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_direccion3_c.php

 // created: 2020-07-31 09:31:18
$dictionary['Contact']['fields']['sasa_direccion3_c']['labelValue']='Dirección 3';
$dictionary['Contact']['fields']['sasa_direccion3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_direccion3_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_direccion3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2021-07-09 17:46:01
$dictionary['Contact']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['Contact']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_pais3_c.php

 // created: 2020-07-31 09:27:39
$dictionary['Contact']['fields']['sasa_pais3_c']['labelValue']='País 3';
$dictionary['Contact']['fields']['sasa_pais3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_pais3_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_pais3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_phone_home_c.php

 // created: 2020-07-31 13:04:42
$dictionary['Contact']['fields']['sasa_phone_home_c']['labelValue']='Teléfono 1';
$dictionary['Contact']['fields']['sasa_phone_home_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_phone_home_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_phone_home_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipodirec3_c.php

 // created: 2020-07-31 13:03:48
$dictionary['Contact']['fields']['sasa_tipodirec3_c']['labelValue']='Tipo Dirección 3';
$dictionary['Contact']['fields']['sasa_tipodirec3_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_tipodirec3_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipodirec2_c.php

 // created: 2020-07-31 13:02:45
$dictionary['Contact']['fields']['sasa_tipodirec2_c']['labelValue']='Tipo Dirección 2';
$dictionary['Contact']['fields']['sasa_tipodirec2_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_tipodirec2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipodirec1_c.php

 // created: 2020-07-31 13:01:36
$dictionary['Contact']['fields']['sasa_tipodirec1_c']['labelValue']='Tipo Dirección 1 ';
$dictionary['Contact']['fields']['sasa_tipodirec1_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_tipodirec1_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipotel3_c.php

 // created: 2020-07-31 13:33:33
$dictionary['Contact']['fields']['sasa_tipotel3_c']['labelValue']='Tipo Teléfono 3 ';
$dictionary['Contact']['fields']['sasa_tipotel3_c']['dependency']='not(equal($sasa_phone_work_c,""))';
$dictionary['Contact']['fields']['sasa_tipotel3_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipotel2_c.php

 // created: 2020-07-31 13:31:42
$dictionary['Contact']['fields']['sasa_tipotel2_c']['labelValue']='Tipo Teléfono 2 ';
$dictionary['Contact']['fields']['sasa_tipotel2_c']['dependency']='not(equal($sasa_phone_home_c,""))';
$dictionary['Contact']['fields']['sasa_tipotel2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipotel1_c.php

 // created: 2020-07-31 12:15:37
$dictionary['Contact']['fields']['sasa_tipotel1_c']['labelValue']='Tipo Teléfono 1';
$dictionary['Contact']['fields']['sasa_tipotel1_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_tipotel1_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_phone_work_c.php

 // created: 2020-07-31 13:30:50
$dictionary['Contact']['fields']['sasa_phone_work_c']['labelValue']='Teléfono 2';
$dictionary['Contact']['fields']['sasa_phone_work_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_phone_work_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_phone_work_c']['dependency']='not(equal($sasa_phone_home_c,""))';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_phone_mobile_c.php

 // created: 2020-07-31 13:07:15
$dictionary['Contact']['fields']['sasa_phone_mobile_c']['labelValue']='Celular 1';
$dictionary['Contact']['fields']['sasa_phone_mobile_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_phone_mobile_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_phone_mobile_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_phone_other_c.php

 // created: 2020-07-31 13:32:48
$dictionary['Contact']['fields']['sasa_phone_other_c']['labelValue']='Teléfono 3';
$dictionary['Contact']['fields']['sasa_phone_other_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_phone_other_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_phone_other_c']['dependency']='not(equal($sasa_phone_work_c,""))';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipo_documento_c.php

 // created: 2021-07-09 17:46:34

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_genero_c.php

 // created: 2022-07-20 15:44:11
$dictionary['Contact']['fields']['sasa_genero_c']['labelValue']='Genéro';
$dictionary['Contact']['fields']['sasa_genero_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_genero_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_genero_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_genero_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/customer_journey_parent.php

// created: 2023-02-08 11:09:23
VardefManager::createVardef('Contacts', 'Contact', [
                                'customer_journey_parent',
                        ]);
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/contacts_cases_1_Contacts.php

// created: 2023-03-06 03:26:09
$dictionary["Contact"]["fields"]["contacts_cases_1"] = array (
  'name' => 'contacts_cases_1',
  'type' => 'link',
  'relationship' => 'contacts_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_cases_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>

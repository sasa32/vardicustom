<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0012020051501.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contacts';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'sasa cliente fallecido c';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono casa';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_HOME_PHONE'] = 'Teléfono casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular Principal';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Teléfono Alternativo';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_paises_contacts_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_paises_contacts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PAISES_CONTACTS_1_FROM_SASA_PAISES_TITLE'] = 'Países';
$mod_strings['LBL_SASA_PAISES_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Países';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PAISES_CONTACTS_1_FROM_SASA_PAISES_TITLE'] = 'Países';
$mod_strings['LBL_SASA_PAISES_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Países';


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0012020051609.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contacts';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'sasa cliente fallecido c';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono casa';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
$mod_strings['LBL_FACEBOOK'] = 'Facebook';
$mod_strings['LBL_TWITTER'] = 'Twitter';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_HOME_PHONE'] = 'Teléfono casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular Principal';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Teléfono Alternativo';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0022020051907.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0022020051907.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contacts';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad Principal';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad Alternativa';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2 (Integración)';


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contacts';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad Principal';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad Alternativa';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2 (Integración)';



?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customaccounts_contacts_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customaccounts_contacts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Cuentas';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Cuentas';


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0012020052101.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contacts';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad Principal';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad Alternativa';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2 (Integración)';


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0012020052711.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contacts';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad Principal';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad Alternativa';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad alternativo 2(Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad principal 2 (Integración)';


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0012020060208.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad Principal';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad Alternativa';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2 (Integración)';


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0022020060210.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad Principal';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad Alternativa';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono casa';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0022020070101.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad Principal';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad Alternativa';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono casa';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0022020070111.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad Principal';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad Alternativa';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono casa';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0032020080302.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0032020080302.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GENERO_C'] = 'Genéro';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3 ';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3 ';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GENERO_C'] = 'Genéro';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3 ';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3 ';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0032020080407.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GENERO_C'] = 'Genéro';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3 ';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3 ';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0032020080609.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0032020080609.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GENERO_C'] = 'Genéro';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3 ';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3 ';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GENERO_C'] = 'Genéro';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3 ';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3 ';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad Dirección 1 (Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad Dirección 2 (Integración)';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0032020081004.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GENERO_C'] = 'Genéro';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad Teléfono 1 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad Teléfono 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad Teléfono 2 (Integración)';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3 ';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3 ';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad Dirección 1 (Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad Dirección 2 (Integración)';
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0032020081804.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión Teléfono 1';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GENERO_C'] = 'Genéro';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad Teléfono 1 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad Teléfono 3 (Integración)';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad Teléfono 2 (Integración)';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3 ';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2 ';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3 ';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1 ';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Ciudad Dirección 1 (Integración)';
$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Ciudad Dirección 2 (Integración)';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0042020093004.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.gvcontactscustomfieldsv0042020093004.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Nota';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Nota';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Reunión';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Tarea';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GENERO_C'] = 'Genéro';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Nota';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Nota';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Reunión';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Tarea';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';
$mod_strings['LBL_SASA_CARGO_C'] = 'Cargo';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel Alternativo';
$mod_strings['LBL_SASA_DEPORTES_C'] = 'Deportes';
$mod_strings['LBL_SASA_EDAD_C'] = 'Edad';
$mod_strings['LBL_SASA_ESTADO_CIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_ESTRATO_C'] = 'Nivel Socioeconómico (Estrato)';
$mod_strings['LBL_SASA_EXTENSION_C'] = 'Extensión';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GENERO_C'] = 'Genéro';
$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
$mod_strings['LBL_SASA_MASCOTA_C'] = 'Mascota';
$mod_strings['LBL_SASA_MUSICA_C'] = 'Música';
$mod_strings['LBL_SASA_NIVEL_EDUCATIVO_C'] = 'Nivel Educativo';
$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
$mod_strings['LBL_SASA_NUMERO_HIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
$mod_strings['LBL_SASA_PROFESION_C'] = 'Profesión';
$mod_strings['LBL_SASA_TIENE_HIJOS_C'] = 'Tiene Hijos';
$mod_strings['LBL_SASA_TIPO_DE_VIVIENDA_C'] = 'Tipo de Vivienda';
$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad tel principal 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad tel oficina 3';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad tel alternativo 4';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_municipios_contacts_4.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_habeas_data_contacts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_HABEAS_DATA_CONTACTS_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_SASA_HABEAS_DATA_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Habeas Data';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customcontacts_sasa_habeas_data_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_CONTACTS_TITLE'] = 'Habeas Data';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_municipios_contacts_6.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_departamentos_contacts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Departamentos';
$mod_strings['LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Departamentos';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_municipios_contacts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_paises_contacts_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PAISES_CONTACTS_2_FROM_SASA_PAISES_TITLE'] = 'Países';
$mod_strings['LBL_SASA_PAISES_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Países';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_departamentos_contacts_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Departamentos';
$mod_strings['LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Departamentos';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_municipios_contacts_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_municipios_contacts_5.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customsasa_municipios_contacts_3.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudades';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contacto';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contacto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contacto';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contactos';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contactos';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contacto desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contactos';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contacto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contactos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contactos';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Cotización-Contacto:';
$mod_strings['LBL_CONTACT'] = 'Contacto:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contactos';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contacto';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contactos';
$mod_strings['LBL_MODULE_TITLE'] = 'Contactos: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contactos Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contactos Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contactos Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Cotización:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Cotización';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Cotización';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Cotización';
$mod_strings['LBL_OPPORTUNITIES'] = 'Cotizaciones';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Caso';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Nota';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Nota';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Reunión';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Tarea';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Elementos presupuestados';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Incidencias';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campañas (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campañas';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campañas';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campañas';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaña';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad 1';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad 2';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel 1';
$mod_strings['LBL_HOME_PHONE'] = 'Tel 1';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel 3';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel 2';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad  2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_CIUDAD_TEL_PRINCIPAL_2_C'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_CIUDAD_TEL_OFICINA_2_C'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_CIUDAD_TEL_ALTERNATIV_2_C'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CONTACT_LEADS_FROM_CONTACTS_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_CONTACTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección 2';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento 2';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal 2';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País 2';
$mod_strings['LBL_SASA_CEL_OTRO_C'] = 'Cel 3';
$mod_strings['LBL_SASA_CELULAR_OTRO_C'] = 'Celular 3';
$mod_strings['LBL_SASA_PAIS3_C'] = 'País 3';
$mod_strings['LBL_SASA_DEPARTAMENTO3_C'] = 'Departamento 3';
$mod_strings['LBL_SASA_CIUDAD3_C'] = 'Ciudad 3';
$mod_strings['LBL_SASA_DIRECCION3_C'] = 'Dirección 3';
$mod_strings['LBL_SASA_CODIGOPOSTAL3_C'] = 'Código Postal 3 ';
$mod_strings['LBL_SASA_TIPOTEL1_C'] = 'Tipo Teléfono 1';
$mod_strings['LBL_SASA_TIPOTEL2_C'] = 'Tipo Teléfono 2';
$mod_strings['LBL_SASA_TIPOTEL3_C'] = 'Tipo Teléfono 3';
$mod_strings['LBL_SASA_TIPODIREC1_C'] = 'Tipo Dirección 1';
$mod_strings['LBL_SASA_TIPODIREC2_C'] = 'Tipo Dirección 2';
$mod_strings['LBL_SASA_TIPODIREC3_C'] = 'Tipo Dirección 3';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono 3';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular 1';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular 2';
$mod_strings['LBL_SASA_CELULAR_ALTERNATIVO_C'] = 'Cel 2';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_ACCOUNTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección 1';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad 1';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País 1';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal 1';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento 1';
$mod_strings['LBL_ALT_ADDRESS'] = 'Dirección 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección 1';
$mod_strings['LBL_PHONE_ALT'] = 'Tel 2';
$mod_strings['LBL_PHONE_OFFICE'] = 'Tel 1';
$mod_strings['LBL_SASA_CELULAR_PRINCIPAL_C'] = 'Cel 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono 1';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Dirección 3';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad 2';
$mod_strings['LBL_SASA_MUNICIPIOS_ACCOUNTS_3_FROM_ACCOUNTS_TITLE'] = 'Ciudad 3';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_CONTACTS_TITLE'] = 'Ciudad Teléfono 3';
$mod_strings['LBL_SASA_MUNICIPIO_3_C'] = 'Ciudad Dirección 3 (Integración)';
$mod_strings['LBL_SASA_EXTENSION_C'] = ' Extensión Teléfono 1';
$mod_strings['LBL_SASA_EXTENSION2_C'] = 'Extensión Teléfono 2';
$mod_strings['LBL_SASA_EXTENSION3_C'] = 'Extensión Teléfono 3';
$mod_strings['LBL_CALLS_SUBPANEL_TITLE'] = 'Llamadas';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_TASKS_SUBPANEL_TITLE'] = 'Tareas';
$mod_strings['LBL_CASES_SUBPANEL_TITLE'] = 'Casos';
$mod_strings['LBL_OPPORTUNITIES_SUBPANEL_TITLE'] = 'Cotizaciones';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_ES.customcontacts_cases_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CONTACTS_CASES_1_FROM_CASES_TITLE'] = 'Casos';
$mod_strings['LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE'] = 'Casos';

?>

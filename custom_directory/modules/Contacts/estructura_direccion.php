<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class estructura_direccion
{
	function after_save($bean, $event, $arguments)
	{	
		/*
		Logic Hook para poblar automaticamente la esctutura geografica de relacion
		*/
		try{
			if (!isset($bean->estructura_direccion_after_save_ignore_update) || $bean->estructura_direccion_after_save_ignore_update === false){//antiloop
				$bean->estructura_direccion_after_save_ignore_update = true;//antiloop	
				global $db;

				$tipo = array("sasa_tipotel1_c","sasa_tipotel2_c","sasa_tipotel3_c","sasa_tipodirec1_c","sasa_tipodirec2_c","sasa_tipodirec3_c");
				foreach ($tipo as $key => $value) {
					if ($bean->$value=="1"){ 
						$bean->$value = "C";
					}elseif ($bean->$value=="2") {
						$bean->$value = "OF";
					}elseif ($bean->$value=="6") {
						$bean->$value = "OT";
					}
				}

				/*require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
				$sendsigru = new Send_Data_Sigru();
				$sendsigru->data($bean->id,$bean->module_name);*/

				//Traducción de valores de tipo de telefono SIGRU no podia enviar el valor como estaba en Sugar
				
				

				//Taer el objeto del municipio relacionado al cliente potencial
				if ($bean->module_name == "Accounts") {
					$bean->sasa_ciudad_tel_oficina_2_c = $bean->sasa_ciudad_tel_3_c;
					if ($bean->load_relationship('sasa_municipios_accounts_1')){
						$ppal = $bean->sasa_municipios_accounts_1->getBeans();
						$ppal =$ppal[array_key_first($ppal)];
						$dpto_ppal = BeanFactory::retrieveBean('sasa_Departamentos', $ppal->sasa_departamentos_sasa_municipios_1sasa_departamentos_ida);
						$pais_ppal = $dpto_ppal->sasa_paises_sasa_departamentos_1_name;
					}
					
					if ($bean->load_relationship('sasa_municipios_accounts_2')){
						$alt = $bean->sasa_municipios_accounts_2->getBeans();
						$alt =$alt[array_key_first($alt)];
						$dpto_alt = BeanFactory::retrieveBean('sasa_Departamentos', $alt->sasa_departamentos_sasa_municipios_1sasa_departamentos_ida);
						$pais_alt = $dpto_alt->sasa_paises_sasa_departamentos_1_name;
					}

					//Relacion de direcciones 3 cuentas
					if ($bean->load_relationship('sasa_municipios_accounts_6')){
						$alt3 = $bean->sasa_municipios_accounts_6->getBeans();
						$alt3 =$alt3[array_key_first($alt3)];
						$dpto_alt3 = BeanFactory::retrieveBean('sasa_Departamentos', $alt3->sasa_departamentos_sasa_municipios_1sasa_departamentos_ida);
						$pais_alt3 = $dpto_alt3->sasa_paises_sasa_departamentos_1_name;
					}
				}
				
				if ($bean->module_name == "Contacts") {					
					if ($bean->load_relationship('sasa_municipios_contacts_1')){
						$ppal = $bean->sasa_municipios_contacts_1->getBeans();
						$ppal =$ppal[array_key_first($ppal)];
						$dpto_ppal = BeanFactory::retrieveBean('sasa_Departamentos', $ppal->sasa_departamentos_sasa_municipios_1sasa_departamentos_ida);
						$pais_ppal = $dpto_ppal->sasa_paises_sasa_departamentos_1_name;
					}
					
					if ($bean->load_relationship('sasa_municipios_contacts_2')){
						$alt = $bean->sasa_municipios_contacts_2->getBeans();
						$alt =$alt[array_key_first($alt)];
						$dpto_alt = BeanFactory::retrieveBean('sasa_Departamentos', $alt->sasa_departamentos_sasa_municipios_1sasa_departamentos_ida);
						$pais_alt = $dpto_alt->sasa_paises_sasa_departamentos_1_name;
					}

					//Relacion de Direcciones 3
					if ($bean->load_relationship('sasa_municipios_contacts_6')){
						$alt3 = $bean->sasa_municipios_contacts_6->getBeans();
						$alt3 =$alt3[array_key_first($alt3)];
						$dpto_alt3 = BeanFactory::retrieveBean('sasa_Departamentos', $alt3->sasa_departamentos_sasa_municipios_1sasa_departamentos_ida);
						$pais_alt3 = $dpto_alt3->sasa_paises_sasa_departamentos_1_name;
					}
				}
				
				if ($bean->module_name == "Leads") {							
					if ($bean->load_relationship('sasa_municipios_leads_1')){
						$ppal = $bean->sasa_municipios_leads_1->getBeans();
						$ppal =$ppal[array_key_first($ppal)];
						$dpto_ppal = BeanFactory::retrieveBean('sasa_Departamentos', $ppal->sasa_departamentos_sasa_municipios_1sasa_departamentos_ida);
						$pais_ppal = $dpto_ppal->sasa_paises_sasa_departamentos_1_name;
					}
					
					if ($bean->load_relationship('sasa_municipios_leads_2')){
						$alt = $bean->sasa_municipios_leads_2->getBeans();
						$alt =$alt[array_key_first($alt)];
						$dpto_alt = BeanFactory::retrieveBean('sasa_Departamentos', $alt->sasa_departamentos_sasa_municipios_1sasa_departamentos_ida);
						$pais_alt = $dpto_alt->sasa_paises_sasa_departamentos_1_name;
					}
				}		
				
				if ($bean->module_name == "Leads") {
					//lleva el departamento segun el municipio "principal"
					$bean->load_relationship("sasa_departamentos_leads_1");
					$bean->sasa_departamentos_leads_1->add($dpto_ppal->id);
					
					//departamento alternativo
					$bean->load_relationship("sasa_departamentos_leads_2");
					$bean->sasa_departamentos_leads_2->add($dpto_alt->id);
					
					//llevar el pais al campo del lead
					$bean->load_relationship("sasa_paises_leads_1");
					$bean->sasa_paises_leads_1->add($dpto_ppal->sasa_paises_sasa_departamentos_1sasa_paises_ida);
					
					//llavar el pais para alternativo
					$bean->load_relationship("sasa_paises_leads_2");
					$bean->sasa_paises_leads_2->add($dpto_alt->sasa_paises_sasa_departamentos_1sasa_paises_ida);


					//Direcciones 1
					if ($bean->sasa_municipios_leads_1sasa_municipios_ida!="" || $bean->sasa_municipios_leads_1sasa_municipios_ida != null) {
						$bean->primary_address_state = $dpto_ppal->name;
						$bean->primary_address_country = $pais_ppal;
						$bean->primary_address_city = $ppal->name;
					}else{
						$bean->primary_address_state = "";
						$bean->primary_address_country = "";
						$bean->primary_address_city = "";
					}

					//Direcciones alternativas
					if ($bean->sasa_municipios_leads_2sasa_municipios_ida!="" || $bean->sasa_municipios_leads_2sasa_municipios_ida != null) {
						$bean->alt_address_city = $alt->name;
						$bean->alt_address_state = $dpto_alt->name;
						$bean->alt_address_country = $pais_alt;
					}else{
						$bean->alt_address_city = "";
						$bean->alt_address_state = "";
						$bean->alt_address_country = "";
					}
					
				}
				
				if ($bean->module_name == "Accounts") {
					//Departamento principal
					$bean->load_relationship("sasa_departamentos_accounts_1");
					$bean->sasa_departamentos_accounts_1->add($dpto_ppal->id);
					
					//Departamento alter
					$bean->load_relationship("sasa_departamentos_accounts_2");
					$bean->sasa_departamentos_accounts_2->add($dpto_alt->id);

					//Pais Principal
					$bean->load_relationship("sasa_paises_accounts_1");
					$bean->sasa_paises_accounts_1->add($dpto_ppal->sasa_paises_sasa_departamentos_1sasa_paises_ida);
					//Pais Alternativo
					$bean->load_relationship("sasa_paises_accounts_2");
					$bean->sasa_paises_accounts_2->add($dpto_alt->sasa_paises_sasa_departamentos_1sasa_paises_ida);

					//Direcciones 1
					if ($bean->sasa_municipios_accounts_1sasa_municipios_ida!="" || $bean->sasa_municipios_accounts_1sasa_municipios_ida != null) {
						$bean->billing_address_state = $dpto_ppal->name;
						$bean->billing_address_city = $ppal->name;
						$bean->billing_address_country = $pais_ppal;
					}else{
						$bean->billing_address_state = "";
						$bean->billing_address_city = "";
						$bean->billing_address_country = "";
					}
					

					//Direcciones alternativas
					if ($bean->sasa_municipios_accounts_2sasa_municipios_ida!="" || $bean->sasa_municipios_accounts_2sasa_municipios_ida != null) {
						$bean->shipping_address_city = $alt->name;
						$bean->shipping_address_state = $dpto_alt->name;
						$bean->shipping_address_country = $pais_alt;
					}else{
						$bean->shipping_address_city = "";
						$bean->shipping_address_state = "";
						$bean->shipping_address_country = "";
					}


					//Direcciones 3
					if ($bean->sasa_municipios_accounts_6sasa_municipios_ida!="" || $bean->sasa_municipios_accounts_6sasa_municipios_ida != null) {
						$bean->sasa_ciudad3_c = $alt3->name;
						$bean->sasa_departamento3_c = $dpto_alt3->name;
						$bean->sasa_pais3_c = $pais_alt3;
					}else{
						$bean->sasa_ciudad3_c = "";
						$bean->sasa_departamento3_c = "";
						$bean->sasa_pais3_c = "";
					}
					
				}
				
				if ($bean->module_name == "Contacts") {
					//Departamento principal
					$bean->load_relationship("sasa_departamentos_contacts_1");
					$bean->sasa_departamentos_contacts_1->add($dpto_ppal->id);
					
					//departamento alternativo
					$bean->load_relationship("sasa_departamentos_contacts_2");
					$bean->sasa_departamentos_contacts_2->add($dpto_alt->id);



					//Pais principal
					$bean->load_relationship("sasa_paises_contacts_1");
					$bean->sasa_paises_contacts_1->add($dpto_ppal->sasa_paises_sasa_departamentos_1sasa_paises_ida);
					//pais alternativo
					$bean->load_relationship("sasa_paises_contacts_2");
					$bean->sasa_paises_contacts_2->add($dpto_alt->sasa_paises_sasa_departamentos_1sasa_paises_ida);
					
					//Direccion 1 contactos
					if ($bean->sasa_municipios_contacts_1sasa_municipios_ida!="" || $bean->sasa_municipios_contacts_1sasa_municipios_ida != null) {
						$bean->primary_address_city = $ppal->name;
						$bean->primary_address_state = $dpto_ppal->name;
						$bean->primary_address_country = $pais_ppal;
					}else{
						$bean->primary_address_city = "";
						$bean->primary_address_state = "";
						$bean->primary_address_country = "";
					}

					
					//Direccion 2 contactos
					if ($bean->sasa_municipios_contacts_2sasa_municipios_ida!="" || $bean->sasa_municipios_contacts_2sasa_municipios_ida != null) {
						$bean->alt_address_city = $alt->name;
						$bean->alt_address_state = $dpto_alt->name;
						$bean->alt_address_country = $pais_alt;
					}else{
						$bean->alt_address_city = "";
						$bean->alt_address_state = "";
						$bean->alt_address_country = "";
					}
					

					//Direccion 3 contactos
					if ($bean->sasa_municipios_contacts_6sasa_municipios_ida!="" || $bean->sasa_municipios_contacts_6sasa_municipios_ida != null) {
						$bean->sasa_ciudad3_c = $alt3->name;
						$bean->sasa_departamento3_c = $dpto_alt3->name;
						$bean->sasa_pais3_c = $pais_alt3;
					}else{
						$bean->sasa_ciudad3_c = "";
						$bean->sasa_departamento3_c = "";
						$bean->sasa_pais3_c = "";
					}
					
				}

				//Cambiar el estado a Cliente/Prospecto existente cuando el cliente ya esté prospectado en la misma unidad de negocio de otra manera dejarlo en asignado.
				if ($bean->module_name == "Leads" && $bean->status!="I") {
					$queryCP = "SELECT leads.id, leads_cstm.sasa_unidad_de_negocio_c, ucstm.id_c, ucstm.sasa_codunidnegocio_c FROM leads INNER JOIN leads_cstm ON leads_cstm.id_c=leads.id INNER JOIN accounts ON leads.account_id=accounts.id INNER JOIN accounts_sasa_unidadnegclienteprospect_1_c ua ON ua.accounts_sasa_unidadnegclienteprospect_1accounts_ida=accounts.id INNER JOIN sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c uu ON uu.sasa_unida14dfrospect_idb=ua.accounts_saf31rospect_idb INNER JOIN sasa_unidad_de_negocio_cstm ucstm ON ucstm.id_c=uu.sasa_unida3a6anegocio_ida WHERE leads.account_id IS NOT null AND leads.deleted=0 AND accounts.deleted=0 AND uu.deleted=0 AND ua.deleted=0 AND leads.id='{$bean->id}' AND ucstm.sasa_codunidnegocio_c='{$bean->sasa_unidad_de_negocio_c}'";
					$ResultqueryCP =  $GLOBALS['db']->query($queryCP);
					$FecthqueryCP =  $GLOBALS['db']->fetchByAssoc($ResultqueryCP);
					if ($FecthqueryCP['id']!=null || $FecthqueryCP['id']!="") {
						$bean->status = "D";
					}
				}


				$bean->save();


				if ($bean->module_name == "Contacts") {
					$Account = BeanFactory::newBean("Accounts");
					$Account->retrieve_by_string_fields( 
						array( 
							'sasa_numero_documento_c' => $bean->sasa_numero_documento_c, 
							'sasa_tipo_documento_c' => $bean->sasa_tipo_documento_c
						) 
					);
					$account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
					if ($bean->modified_by_name != "Integracion1") {
						
						$GetAccountJuridica = $GLOBALS['db']->query("SELECT accounts_cstm.sasa_tipo_persona_c from accounts_contacts INNER JOIN accounts_cstm ON accounts_contacts.account_id=accounts_cstm.id_c where accounts_contacts.contact_id='{$bean->id}' AND accounts_cstm.sasa_tipo_persona_c='J'");
						$CuentaJuridica =  $GLOBALS['db']->fetchByAssoc($GetAccountJuridica);
						if ($CuentaJuridica['sasa_tipo_persona_c'] != null || $CuentaJuridica['sasa_tipo_persona_c'] != "") {
							//Consumir SIgru Contactos
							$GLOBALS['log']->security("\nConsumir SIgru ContactosJuridica\n");
							require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
							$sendsigru = new Send_Data_Sigru();
							$sendsigru->data($bean->id,$bean->module_name);
						}elseif ($account->sasa_tipo_persona_c == "N") {
							//Consumir SIgru Contactos
							$GLOBALS['log']->security("\nConsumir SIgru ContactosNatural\n");
							require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
							$sendsigru = new Send_Data_Sigru();
							$sendsigru->data($account->id,"Accounts");
						}
					}
				}elseif ($bean->module_name == "Accounts" && $bean->sasa_tipo_persona_c == "J") {
					require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
					$sendsigru = new Send_Data_Sigru();
					$sendsigru->data($bean->id,$bean->module_name);
				}
			}
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook estructura_direccion: ".$e->getMessage()); 
		}
	}
}
?>
<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
//require_once "custom/include/api/cstmSugarApiExceptionError.php";
class logic_hooks_contacts
{		
	/*
	logic_hooks_contacts before_save
	*/
	function before_save(&$bean, $event, $arguments)
	{	
		if (!empty($bean->first_name)){
			$primernombre = $bean->first_name;
			$segundonombre = $bean->last_name;
			$bean->first_name = strtoupper($primernombre);
			$bean->last_name = strtoupper($segundonombre);
		}
		/*
		*https://sasaconsultoria.sugarondemand.com/index.php#Tasks/d0030608-74ea-11ea-86e4-02fb8f607ac4

		*/
		if (empty($bean->team_id)){
			$bean->team_id = 1;
		}	
		
		if (empty($bean->team_set_id)){
			$bean->team_set_id = 1;
		}	
		
		if (empty($bean->acl_team_set_id)){
			$bean->acl_team_set_id = 1;
		}	

		if (empty($bean->created_by)){
			$bean->created_by = 1;
		}
		
		if (empty($bean->assigned_user_id)){
			$bean->assigned_user_id = $bean->created_by;
		}
		
		/*
		*https://sasaconsultoria.sugarondemand.com/index.php#Tasks/485a65ca-74ea-11ea-b889-06ab610ac1b0
		*/
		//ciudad ppal
		if (!empty($bean->sasa_municipio_principal_2_c)){
			$result = $GLOBALS['db']->query("SELECT id FROM sasa_municipios WHERE sasa_codigomunicipio_c LIKE '{$bean->sasa_municipio_principal_2_c}' AND deleted=0 limit 1");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);	
			
			if ($bean->load_relationship('sasa_municipios_contacts_1')){
				$relatedBeans = $bean->sasa_municipios_contacts_1->add($sasa_municipios["id"]);
				$bean->sasa_municipio_principal_2_c ="";
			}
		}				
		
		//ciudad alt
		if (!empty($bean->sasa_municipio_secundario_2_c)){
			$result = $GLOBALS['db']->query("SELECT id FROM sasa_municipios WHERE sasa_codigomunicipio_c LIKE '{$bean->sasa_municipio_secundario_2_c}' AND deleted=0 limit 1");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);	
			
			if ($bean->load_relationship('sasa_municipios_contacts_2')){
				$relatedBeans = $bean->sasa_municipios_contacts_2->add($sasa_municipios["id"]);
				$bean->sasa_municipio_secundario_2_c ="";
			} 
		}

		//ciudad 3
		if (!empty($bean->sasa_municipio_3_c)){
			$result = $GLOBALS['db']->query("SELECT id FROM sasa_municipios WHERE sasa_codigomunicipio_c LIKE '{$bean->sasa_municipio_3_c}' AND deleted=0 limit 1");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);	
			
			if ($bean->load_relationship('sasa_municipios_contacts_6')){
				$relatedBeans = $bean->sasa_municipios_contacts_6->add($sasa_municipios["id"]);
				$bean->sasa_municipio_3_c ="";
			} 
		}
		
		//Ciudad telefono casa/ppal
		if (!empty($bean->sasa_ciudad_tel_principal_2_c)){
			$result = $GLOBALS['db']->query("SELECT id FROM sasa_municipios WHERE sasa_codigomunicipio_c LIKE '{$bean->sasa_ciudad_tel_principal_2_c}' AND deleted=0 limit 1");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);
			if ($bean->load_relationship('sasa_municipios_contacts_3')){
				$relatedBeans = $bean->sasa_municipios_contacts_3->add($sasa_municipios["id"]);
				$bean->sasa_ciudad_tel_principal_2_c ="";
			} 
			$bean->sasa_municipios_id2_c = $sasa_municipios["id"];
		}
		
		//ciudad telefono alternativo
		if (!empty($bean->sasa_ciudad_tel_alternativ_2_c)){
			$result = $GLOBALS['db']->query("SELECT id FROM sasa_municipios WHERE sasa_codigomunicipio_c LIKE '{$bean->sasa_ciudad_tel_alternativ_2_c}' AND deleted=0 limit 1");

			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);
			if ($bean->load_relationship('sasa_municipios_contacts_4')){
				$relatedBeans = $bean->sasa_municipios_contacts_4->add($sasa_municipios["id"]);
				$bean->sasa_ciudad_tel_alternativ_2_c ="";
			} 
			$bean->sasa_municipios_id_c = $sasa_municipios["id"];
		}

		//ciudad telefono 3
		if (!empty($bean->sasa_ciudad_tel_oficina_2_c)){
			$result = $GLOBALS['db']->query("SELECT id FROM sasa_municipios WHERE sasa_codigomunicipio_c LIKE '{$bean->sasa_ciudad_tel_oficina_2_c}' AND deleted=0 limit 1");
			$sasa_municipios =  $GLOBALS['db']->fetchByAssoc($result);
			if ($bean->load_relationship('sasa_municipios_contacts_5')){
				$relatedBeans = $bean->sasa_municipios_contacts_5->add($sasa_municipios["id"]);
				$bean->sasa_ciudad_tel_oficina_2_c ="";
			} 
			$bean->sasa_municipios_id_c = $sasa_municipios["id"];
		}
		
		
		if (!isset($bean->logic_hooks_contacts_before_save_ignore_update) || $bean->logic_hooks_contacts_before_save_ignore_update === false){//antiloop
			$bean->logic_hooks_contacts_before_save_ignore_update = true;//antiloop			
		
			/*
			* https://sasaconsultoria.sugarondemand.com/#Tasks/b3b62e86-5428-11ea-b5fd-02dfd714a754
			* Funcionalidad para validar si al guardar un registro este tiene diligenciado un dato de contactación tales como un email, celular principal o teléfono principal,
			*/
			if( empty(trim($bean->sasa_phone_mobile_c)) and empty(trim($bean->sasa_phone_home_c)) and empty($bean->emailAddress->addresses) ){	
				//throw new SugarApiExceptionInvalidParameter("Debe ingresar al menos un dato de contacto");//usar http_code 404 en lugar de 422 para que no salga error de conexion	
			}
			
			/*
			* Telefonos solo numeros (entero)
			*/
			$int_fields_error = false;
			$int_fields = array(		
				trim($bean->sasa_phone_home_c),
				trim($bean->sasa_phone_mobile_c),
				trim($bean->sasa_phone_work_c),
				trim($bean->sasa_phone_other_c),
				trim($bean->sasa_cel_alternativo_c),
				trim($bean->sasa_celular_otro_c),
			);	
			foreach( $int_fields  as $key => $int_field){  
				if(!empty($int_field)){
					if(!preg_match('/^\d+$/',$int_field)){ //https://www.codexpedia.com/php/php-checking-if-a-variable-contains-only-digits/
						$int_fields_error = true;	
					}
				}
			}	
			if($int_fields_error){ 
				throw new SugarApiExceptionInvalidParameter("Debe ingresar sólo números en los teléfonos de contactación");//usar http_code 404 en lugar de 422 para que no salga error de conexion
			}
			
			try{
				/*
				* https://sasaconsultoria.sugarondemand.com/index.php#Tasks/43e5491c-74ed-11ea-ba4c-06ab610ac1b0
				*/
				$GLOBALS['log']->security("\nBeforesave contacts\n");
				$Account = BeanFactory::newBean("Accounts");
				$Account->retrieve_by_string_fields( 
					array( 
						'sasa_numero_documento_c' => $bean->sasa_numero_documento_c, 
						'sasa_tipo_documento_c' => $bean->sasa_tipo_documento_c
					) 
				); 
				if(!empty($Account->id)){		 	
					$account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));	

					//https://onedrive.live.com/edit.aspx?cid=fde22c13c46e678b&page=view&resid=FDE22C13C46E678B!754&parId=FDE22C13C46E678B!563&app=Excel
					if($account->sasa_tipo_persona_c == "N"){//solo persona natural
						if($bean->date_modified != $contact->date_modified){ //para evitar saving loop
							$account->logic_hooks_accounts_before_save_ignore_update = true;//antiloop															
							$account->billing_address_city = $bean->primary_address_city;
							$account->billing_address_country = $bean->primary_address_country;
							$account->billing_address_postalcode = $bean->primary_address_postalcode;
							$account->billing_address_state = $bean->primary_address_state;
							$account->billing_address_street = $bean->primary_address_street;
							$account->shipping_address_city = $bean->alt_address_city;
							$account->shipping_address_country = $bean->alt_address_country;
							$account->shipping_address_postalcode = $bean->alt_address_postalcode;
							$account->shipping_address_state = $bean->alt_address_state;
							$account->shipping_address_street = $bean->alt_address_street;
							$account->description = $bean->description;
							$account->email = $bean->email;
							$account->facebook = $bean->facebook;
							$account->twitter = $bean->twitter;
							$account->sasa_celular_principal_c = $bean->phone_mobile; 
							$account->sasa_celular_alternativo_c = $bean->sasa_celular_alternativo_c;
							$account->phone_alternate = $bean->phone_other;
							$account->sasa_ciudad_tel_alternativ_c = $bean->sasa_ciudad_tel_alternativ_c;						
							$account->sasa_municipios_id_c=$bean->sasa_municipios_id1_c;//relacion ciudad
							$account->sasa_extension_c = $bean->sasa_extension_c;
							$account->sasa_tipo_documento_c = $bean->sasa_tipo_documento_c;
							$account->sasa_numero_documento_c = $bean->sasa_numero_documento_c;
							
							//ajustes https://sasaconsultoria.sugarondemand.com/#Tasks/cb4e1270-6e13-11ea-9318-065a187ca958
							$account->sasa_cel_principal_c=$bean->sasa_phone_mobile_c;
							$account->sasa_cel_alternativo_c=$bean->sasa_cel_alternativo_c;							
							$account->sasa_phone_alternate_c=$bean->sasa_phone_work_c;
							$account->sasa_phone_office_c=$bean->sasa_phone_home_c;
							$account->sasa_phone_other_c=$bean->sasa_phone_other_c;
							$account->sasa_celular_otro_c=$bean->sasa_celular_otro_c;

							$account->sasa_tipotel1_c=$bean->sasa_tipotel1_c;
							$account->sasa_tipotel2_c=$bean->sasa_tipotel2_c;
							$account->sasa_tipotel3_c=$bean->sasa_tipotel3_c;
							$account->sasa_ciudad3_c=$bean->sasa_ciudad3_c;
							$account->sasa_departamento3_c=$bean->sasa_departamento3_c;
							$account->sasa_pais3_c=$bean->sasa_pais3_c;
							$account->sasa_tipodirec1_c=$bean->sasa_tipodirec1_c;
							$account->sasa_tipodirec2_c=$bean->sasa_tipodirec2_c;
							$account->sasa_tipodirec3_c=$bean->sasa_tipodirec3_c;
							$account->sasa_direccion3_c=$bean->sasa_direccion3_c;
							$account->sasa_codigopostal3_c=$bean->sasa_codigopostal3_c;
							//Se comentaron estas lineas de mapeo de ciudades porque estaba probocando errores en las relaciones
							/*$account->sasa_municipios_accounts_6sasa_municipios_ida=$bean->sasa_municipios_contacts_6sasa_municipios_ida;
							$account->sasa_municipios_accounts_4sasa_municipios_ida=$bean->sasa_municipios_contacts_4sasa_municipios_ida;
							$account->sasa_municipios_accounts_5sasa_municipios_ida=$bean->sasa_municipios_contacts_5sasa_municipios_ida;*/
							//https://sasaconsultoria.sugarondemand.com/#Tasks/d3ae5bc8-de7a-11ea-81c5-065a187ca958
							$account->sasa_extension_c=$bean->sasa_extension_c;
							$account->sasa_extension2_c=$bean->sasa_extension2_c;
							$account->sasa_extension3_c=$bean->sasa_extension3_c;
							//@TODO Ciudad del teléfono principal del módulo clientes y prospectos sasa_municipios_accounts_3_name con sasa_municipios_contacts_3_name de contactos.
							//@TODO Ciudad del teléfono alternativo de ambos módulos, clientes y prospectos sasa_municipios_accounts_4_name  con sasa_municipios_contacts_5_name del módulo de contactos
							
							//ciudad ppal
							if ($bean->load_relationship('sasa_municipios_contacts_1')){
								$sasa_municipios_contacts_1 = array_shift($bean->sasa_municipios_contacts_1->getBeans());
								$account->sasa_municipio_principal_2_c = $sasa_municipios_contacts_1->sasa_codigomunicipio_c;
							}		
									
							//ciudad alt
							if ($bean->load_relationship('sasa_municipios_contacts_2')){
								$sasa_municipios_contacts_2 = array_shift($bean->sasa_municipios_contacts_2->getBeans());
								$account->sasa_municipio_secundario_2_c = $sasa_municipios_contacts_2->sasa_codigomunicipio_c;
							}	
									
							//Ciudad del teléfono principal
							if ($bean->load_relationship('sasa_municipios_contacts_3')){
								$sasa_municipios_contacts_3 = array_shift($bean->sasa_municipios_contacts_3->getBeans());
								$account->sasa_ciudad_tel_principal_2_c = $sasa_municipios_contacts_3->sasa_codigomunicipio_c;
							}
									
							//Ciudad del teléfono alternativo
							if ($bean->load_relationship('sasa_municipios_contacts_4')){
								$sasa_municipios_contacts_4 = array_shift($bean->sasa_municipios_contacts_4->getBeans());
								$GLOBALS['log']->security("Ciudad tel 2: ".$sasa_municipios_contacts_4->sasa_codigomunicipio_c);
								$account->sasa_ciudad_tel_alternativ_2_c = $sasa_municipios_contacts_4->sasa_codigomunicipio_c;
							}

							//ciudad 3
							if ($bean->load_relationship('sasa_municipios_contacts_5')){
								$sasa_municipios_contacts_5 = array_shift($bean->sasa_municipios_contacts_5->getBeans());
								$account->sasa_ciudad_tel_3_c = $sasa_municipios_contacts_5->sasa_codigomunicipio_c;
							}

							//ciudad telefono 3
							if ($bean->load_relationship('sasa_municipios_contacts_6')){
								$sasa_municipios_contacts_6 = array_shift($bean->sasa_municipios_contacts_6->getBeans());
								$account->sasa_municipio_3_c = $sasa_municipios_contacts_6->sasa_codigomunicipio_c;
							}	

												
													
							$account->save(false);
							
							//email
							$result = $GLOBALS['db']->query("delete FROM email_addr_bean_rel WHERE bean_id = '{$account->id}' and bean_module = '{$account->module_dir}'");
							$result = $GLOBALS['db']->query("replace into email_addr_bean_rel (SELECT uuid() as id, email_address_id, '{$account->id}' as bean_id, '{$account->module_dir}' as bean_module, primary_address,reply_to_Address,date_created, date_modified,deleted FROM email_addr_bean_rel where bean_module = '{$bean->module_dir}' and bean_id = '{$bean->id}' and deleted = 0)");
						}
					}
				}


				
				
			} 
			catch (Exception $e) {
				$GLOBALS['log']->security('Excepción capturada: ',  $e->getMessage(), "\n");
			}
		}
	}
}
?>
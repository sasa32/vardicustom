<?php
// created: 2023-02-08 11:06:10
$viewdefs['Contacts']['mobile']['view']['edit'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'sasa_nombres_c',
          'label' => 'LBL_SASA_NOMBRES_C',
        ),
        1 => 
        array (
          'name' => 'sasa_primerapellido_c',
          'label' => 'LBL_SASA_PRIMERAPELLIDO_C',
        ),
        2 => 
        array (
          'name' => 'sasa_last_name_2_c',
          'label' => 'LBL_SASA_LAST_NAME_2_C',
        ),
        3 => 'account_name',
        4 => 
        array (
          'name' => 'sasa_genero_c',
          'label' => 'LBL_SASA_GENERO_C',
        ),
        5 => 
        array (
          'name' => 'sasa_tipo_documento_c',
          'label' => 'LBL_SASA_TIPO_DOCUMENTO_C',
        ),
        6 => 
        array (
          'name' => 'sasa_numero_documento_c',
          'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
        ),
        7 => 
        array (
          'name' => 'sasa_phone_mobile_c',
          'label' => 'LBL_SASA_PHONE_MOBILE_C',
        ),
        8 => 
        array (
          'name' => 'sasa_cel_alternativo_c',
          'label' => 'LBL_SASA_CEL_ALTERNATIVO_C',
        ),
        9 => 
        array (
          'name' => 'sasa_celular_otro_c',
          'label' => 'LBL_SASA_CELULAR_OTRO_C',
        ),
        10 => 
        array (
          'name' => 'sasa_tipotel1_c',
          'label' => 'LBL_SASA_TIPOTEL1_C',
        ),
        11 => 
        array (
          'name' => 'sasa_phone_home_c',
          'label' => 'LBL_SASA_PHONE_HOME_C',
        ),
        12 => 
        array (
          'name' => 'sasa_municipios_contacts_3_name',
          'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        13 => 
        array (
          'name' => 'sasa_extension_c',
          'label' => 'LBL_SASA_EXTENSION_C',
        ),
        14 => 
        array (
          'name' => 'sasa_tipotel2_c',
          'label' => 'LBL_SASA_TIPOTEL2_C',
        ),
        15 => 
        array (
          'name' => 'sasa_phone_work_c',
          'label' => 'LBL_SASA_PHONE_WORK_C',
        ),
        16 => 
        array (
          'name' => 'sasa_municipios_contacts_4_name',
          'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        17 => 'email',
        18 => 
        array (
          'name' => 'sasa_tipotel3_c',
          'label' => 'LBL_SASA_TIPOTEL3_C',
        ),
        19 => 
        array (
          'name' => 'sasa_phone_other_c',
          'label' => 'LBL_SASA_PHONE_OTHER_C',
        ),
        20 => 
        array (
          'name' => 'sasa_municipios_contacts_5_name',
          'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        21 => 
        array (
          'name' => 'sasa_tipodirec1_c',
          'label' => 'LBL_SASA_TIPODIREC1_C',
        ),
        22 => 
        array (
          'name' => 'sasa_municipios_contacts_1_name',
          'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        23 => 'primary_address_street',
        24 => 'primary_address_state',
        25 => 'primary_address_city',
        26 => 'primary_address_postalcode',
        27 => 'primary_address_country',
        28 => 
        array (
          'name' => 'sasa_tipodirec2_c',
          'label' => 'LBL_SASA_TIPODIREC2_C',
        ),
        29 => 
        array (
          'name' => 'sasa_municipios_contacts_2_name',
          'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        30 => 
        array (
          'name' => 'alt_address_street',
          'comment' => 'Street address for alternate address',
          'label' => 'LBL_ALT_ADDRESS_STREET',
        ),
        31 => 
        array (
          'name' => 'alt_address_state',
          'comment' => 'State for alternate address',
          'readonly' => true,
          'label' => 'LBL_ALT_ADDRESS_STATE',
        ),
        32 => 
        array (
          'name' => 'alt_address_city',
          'comment' => 'City for alternate address',
          'readonly' => true,
          'label' => 'LBL_ALT_ADDRESS_CITY',
        ),
        33 => 
        array (
          'name' => 'alt_address_postalcode',
          'comment' => 'Postal code for alternate address',
          'label' => 'LBL_ALT_ADDRESS_POSTALCODE',
        ),
        34 => 
        array (
          'name' => 'alt_address_country',
          'comment' => 'Country for alternate address',
          'readonly' => true,
          'label' => 'LBL_ALT_ADDRESS_COUNTRY',
        ),
        35 => 
        array (
          'name' => 'sasa_tipodirec3_c',
          'label' => 'LBL_SASA_TIPODIREC3_C',
        ),
        36 => 
        array (
          'name' => 'sasa_municipios_contacts_6_name',
          'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE',
        ),
        37 => 
        array (
          'name' => 'sasa_direccion3_c',
          'label' => 'LBL_SASA_DIRECCION3_C',
        ),
        38 => 
        array (
          'readonly' => true,
          'name' => 'sasa_departamento3_c',
          'label' => 'LBL_SASA_DEPARTAMENTO3_C',
        ),
        39 => 
        array (
          'readonly' => true,
          'name' => 'sasa_ciudad3_c',
          'label' => 'LBL_SASA_CIUDAD3_C',
        ),
        40 => 
        array (
          'name' => 'sasa_codigopostal3_c',
          'label' => 'LBL_SASA_CODIGOPOSTAL3_C',
        ),
        41 => 
        array (
          'readonly' => true,
          'name' => 'sasa_pais3_c',
          'label' => 'LBL_SASA_PAIS3_C',
        ),
        42 => 
        array (
          'name' => 'sasa_cargo_c',
          'label' => 'LBL_SASA_CARGO_C',
        ),
        43 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        44 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        45 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
        46 => 
        array (
          'name' => 'birthdate',
          'comment' => 'The birthdate of the contact',
          'label' => 'LBL_BIRTHDATE',
        ),
        47 => 
        array (
          'name' => 'sasa_edad_c',
          'label' => 'LBL_SASA_EDAD_C',
        ),
        48 => 
        array (
          'name' => 'sasa_estado_civil_c',
          'label' => 'LBL_SASA_ESTADO_CIVIL_C',
        ),
        49 => 
        array (
          'name' => 'sasa_cliente_fallecido_c',
          'label' => 'LBL_SASA_CLIENTE_FALLECIDO_C',
        ),
        50 => 
        array (
          'name' => 'sasa_tiene_hijos_c',
          'label' => 'LBL_SASA_TIENE_HIJOS_C',
        ),
        51 => 
        array (
          'name' => 'sasa_numero_hijos_c',
          'label' => 'LBL_SASA_NUMERO_HIJOS_C',
        ),
        52 => 
        array (
          'name' => 'sasa_profesion_c',
          'label' => 'LBL_SASA_PROFESION_C',
        ),
        53 => 
        array (
          'name' => 'sasa_nivel_educativo_c',
          'label' => 'LBL_SASA_NIVEL_EDUCATIVO_C',
        ),
        54 => 
        array (
          'name' => 'sasa_tipo_de_vivienda_c',
          'label' => 'LBL_SASA_TIPO_DE_VIVIENDA_C',
        ),
        55 => 
        array (
          'name' => 'sasa_estrato_c',
          'label' => 'LBL_SASA_ESTRATO_C',
        ),
        56 => 
        array (
          'name' => 'sasa_ocupacion_c',
          'label' => 'LBL_SASA_OCUPACION_C',
        ),
        57 => 
        array (
          'name' => 'sasa_deportes_c',
          'label' => 'LBL_SASA_DEPORTES_C',
        ),
        58 => 
        array (
          'name' => 'sasa_mascota_c',
          'label' => 'LBL_SASA_MASCOTA_C',
        ),
        59 => 
        array (
          'name' => 'sasa_musica_c',
          'label' => 'LBL_SASA_MUSICA_C',
        ),
        60 => 
        array (
          'name' => 'twitter',
          'comment' => 'The twitter name of the user',
          'label' => 'LBL_TWITTER',
        ),
        61 => 
        array (
          'name' => 'facebook',
          'comment' => 'The facebook name of the user',
          'label' => 'LBL_FACEBOOK',
        ),
      ),
    ),
  ),
);
({
    plugins: [],

    initialize: function(options) {
        this._super('initialize', [options]);
    },

    _renderHtml: function() {
            this._super('_renderHtml');
    },
    fields_list:null,
    fields_count:null,
    fields_empty:null,
    fields_porcentarje:null,
    loadData: function(options) {
        if (_.isUndefined(this.model)) {
            return;
        }
        
        /*
        * Context
        */
        este = this;
        var self = this;
	self.data = {};
        self.data.cl = {};
	
	var module = 'Contacts';
	self.data.account_id = this.model.attributes.id;
	self.data.account_id = this.model.attributes.id;

	app.api.call('GET', app.api.buildURL(module+'/' + self.data.account_id), null, {
	    success: function(data) {	       
	    
	        /*
	        * Campos
	        */
	        self.fields_list = {
			sasa_cargo_c:data.sasa_cargo_c,
			//sasa_extension_c:data.sasa_extension_c,
			sasa_estado_civil_c:data.sasa_estado_civil_c,
			sasa_tiene_hijos_c:data.sasa_tiene_hijos_c,
			sasa_numero_hijos_c:data.sasa_numero_hijos_c,
			sasa_phone_mobile_c:data.sasa_phone_mobile_c,
			sasa_estrato_c:data.sasa_estrato_c,
			sasa_profesion_c:data.sasa_profesion_c,
			sasa_nivel_educativo_c:data.sasa_nivel_educativo_c,
			sasa_tipo_de_vivienda_c:data.sasa_tipo_de_vivienda_c,
			sasa_ocupacion_c:data.sasa_ocupacion_c,
			sasa_mascota_c:data.sasa_mascota_c,
			sasa_deportes_c:data.sasa_deportes_c,
			sasa_musica_c:data.sasa_musica_c,
			sasa_last_name_2_c:data.sasa_last_name_2_c,
			sasa_phone_home_c:data.sasa_phone_home_c,
			//sasa_phone_work_c:data.sasa_phone_work_c,
			//sasa_phone_other_c:data.sasa_phone_other_c,
			//sasa_cel_alternativo_c:data.sasa_cel_alternativo_c,
			
			facebook:data.facebook,
			twitter:data.twitter,
			primary_address_street:data.primary_address_street,
			birthdate:data.birthdate,
			//alt_address_street:data.alt_address_street,
			primary_address_state:data.primary_address_state,
			primary_address_city:data.primary_address_city,
			primary_address_country:data.primary_address_country,
			//alt_address_city:data.alt_address_city,
			//alt_address_state:data.alt_address_state,
			//alt_address_country:data.alt_address_country,
			email:data.email
		};
		self.fields_count =Object.keys(self.fields_list).length;
		self.fields_empty=0;
		$.each(self.fields_list, function(key, value) {
			if(!(typeof value === 'undefined' || value == null || value.length === 0)) {
				self.fields_empty++;
			}
		});
		
		if(self.fields_empty == 0 || self.fields_count == 0){
			self.fields_porcentarje = 0;
		}
		else{
			self.fields_porcentarje = (self.fields_empty/self.fields_count)*100;
		}

		console.log(" self.fields_list:", self.fields_list);
		// console.log(" self.fields_count:", self.fields_count);
		// console.log(" self.fields_empty:", self.fields_empty);
		// console.log(" self.fields_porcentarje:", self.fields_porcentarje);
	    	                
	        /*
	        * Dashlet Code
	        */                
	        self.data.cl.cursor1_margin = 10;
	        self.data.cl.cursor1 = 0; 
	        self.data.cl.cursor2 = Number(self.fields_porcentarje).toFixed(0);
	        self.data.cl.cursor3 = 100; 
	        self.data.cl.cursor2_progressbar =  100-self.data.cl.cursor2 ;
	        
	        /*
	        * ToDisplay
	        */
	        _.extend(self, self.data);
	        self.render();
	    },
	    error: function(data) {
	        console.log('Error: loadData bizcard');
	    }
	});
    },
})

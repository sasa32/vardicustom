({
    extendsFrom: 'ContactsCreateView',
    initialize: function(options) {
	var self=this;
	este = self;
	self._super('initialize', [options]);

	self.model.once("sync",
		function() {
			//console.log("olaKace");
			self.model.on(
				"change:sasa_extension_c",
				self._Validar_extension,
				self
			);
			self.model.on(
				"change:sasa_extension2_c",
				self._Validar_extension2,
				self
			);
			self.model.on(
				"change:sasa_extension3_c",
				self._Validar_extension3,
				self
			);
		},
		self
	);
    
	self.model.addValidationTask('sasa_phone_mobile_c', _.bind(self._ValidarNumeric_sasa_phone_mobile_c, self));
	self.model.addValidationTask('sasa_phone_home_c', _.bind(self._ValidarNumeric_sasa_phone_home_c, self));
	self.model.addValidationTask('sasa_phone_work_c', _.bind(self._ValidarNumeric_sasa_phone_work_c, self));
	self.model.addValidationTask('sasa_phone_other_c', _.bind(self._ValidarNumeric_sasa_phone_other_c, self));
	self.model.addValidationTask('sasa_cel_alternativo_c', _.bind(self._ValidarNumeric_sasa_cel_alternativo_c, self));
	self.model.addValidationTask('email', _.bind(self._ValidarDatosDeContactacion, self));

	self.model.addValidationTask('sasa_celular_otro_c', _.bind(self._ValidaCantidad_sasa_celular_otro_c, self));
    },

    _Validar_extension: function(fields, errors, callback) {
    	//console.log("cambio");
    	var self=this;
    	if (self.model.get('sasa_extension_c')=='' && self.model.get('sasa_tipotel1_c')=='OF') {
    		//console.log("Y es dos");
    		self.model.set('sasa_phone_home_c','');
    	}
    },
    _Validar_extension2: function(fields, errors, callback) {
    	//console.log("cambio");
    	var self=this;
    	if (self.model.get('sasa_extension2_c')=='' && self.model.get('sasa_tipotel2_c')=='OF') {
    		//console.log("Y es dos");
    		self.model.set('sasa_phone_work_c','');
    	}
    },
    _Validar_extension3: function(fields, errors, callback) {
    	//console.log("cambio");
    	var self=this;
    	if (self.model.get('sasa_extension3_c')=='' && self.model.get('sasa_tipotel3_c')=='OF') {
    		//console.log("Y es dos");
    		self.model.set('sasa_phone_other_c','');
    	}
    },
    
    _ValidarNumeric_sasa_phone_mobile_c: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarNumeric_sasa_phone_mobile_c';
		if (!_.isEmpty(self.model.get('sasa_phone_mobile_c'))){
			if(/^\d+$/.exec(self.model.get('sasa_phone_mobile_c')) === null){//https://stackoverflow.com/questions/3291289/preg-match-in-javascript			
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Debe ingresar sólo números en los teléfonos de contacto"],
					autoClose: false,
				});
			
				errors['sasa_phone_mobile_c'] = errors['sasa_phone_mobile_c'] || {};
				errors['sasa_phone_mobile_c'].required = true;
			}
			if (self.model.get('sasa_phone_mobile_c').length<10) {
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar diez dígitos en Celular 1"],
					autoClose: false,
				});
			
				errors['sasa_phone_mobile_c'] = errors['sasa_phone_mobile_c'] || {};
				errors['sasa_phone_mobile_c'].required = true;
			}
			//Validar duplicidad entre celulares
			if (!_.isEmpty(self.model.get('sasa_cel_alternativo_c'))) {
				if (self.model.get('sasa_phone_mobile_c') == self.model.get('sasa_cel_alternativo_c')) {
					app.alert.show(message_id, {
						level: 'info',
						title: 'Nota',
						messages: ["Los Celulares 1 y Celulares 2 no pueden ser iguales"],
						autoClose: false,
					});
				
					errors['sasa_phone_mobile_c'] = errors['sasa_phone_mobile_c'] || {};
					errors['sasa_phone_mobile_c'].required = true;
				}
			}
		}		
		callback(null, fields, errors);
    },    
    
    _ValidarNumeric_sasa_phone_home_c: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarNumeric_sasa_phone_home_c';
		if (!_.isEmpty(self.model.get('sasa_phone_home_c'))){
			if(/^\d+$/.exec(self.model.get('sasa_phone_home_c')) === null){//https://stackoverflow.com/questions/3291289/preg-match-in-javascript		
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Debe ingresar sólo números en los teléfonos de contacto"],
					autoClose: false,
				});
			
				errors['sasa_phone_home_c'] = errors['sasa_phone_home_c'] || {};
				errors['sasa_phone_home_c'].required = true;
			}
			if (self.model.get('sasa_phone_home_c').length<7) {
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar siete dígitos en Teléfono 1"],
					autoClose: false,
				});
			
				errors['sasa_phone_home_c'] = errors['sasa_phone_home_c'] || {};
				errors['sasa_phone_home_c'].required = true;
			}
			//Validar si los teléfonos están iguales
			if (!_.isEmpty(self.model.get('sasa_phone_work_c'))) {
				if (self.model.get('sasa_phone_home_c') == self.model.get('sasa_phone_work_c')) {
					app.alert.show(message_id, {
						level: 'info',
						title: 'Nota',
						messages: ["Los Teléfonos 1 y Teléfonos 2 no pueden ser iguales"],
						autoClose: false,
					});
				
					errors['sasa_phone_home_c'] = errors['sasa_phone_home_c'] || {};
					errors['sasa_phone_home_c'].required = true;
				}
			}
		}
		callback(null, fields, errors);
    },    
    
    _ValidarNumeric_sasa_phone_work_c: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarNumeric_sasa_phone_work_c';
		if (!_.isEmpty(self.model.get('sasa_phone_work_c'))){
			if(/^\d+$/.exec(self.model.get('sasa_phone_work_c')) === null){//https://stackoverflow.com/questions/3291289/preg-match-in-javascript		
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Debe ingresar sólo números en los teléfonos de contactación"],
					autoClose: false,
				});
			
				errors['sasa_phone_work_c'] = errors['sasa_phone_work_c'] || {};
				errors['sasa_phone_work_c'].required = true;
			}
			if (self.model.get('sasa_phone_work_c').length<7) {
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar siete dígitos en Teléfono 2"],
					autoClose: false,
				});
			
				errors['sasa_phone_work_c'] = errors['sasa_phone_work_c'] || {};
				errors['sasa_phone_work_c'].required = true;
			}
			//Validar si los teléfonos están iguales
			if (!_.isEmpty(self.model.get('sasa_phone_other_c'))) {
				if (self.model.get('sasa_phone_work_c') == self.model.get('sasa_phone_other_c')) {
					app.alert.show(message_id, {
						level: 'info',
						title: 'Nota',
						messages: ["Los Teléfonos 2 y Teléfonos 3 no pueden ser iguales"],
						autoClose: false,
					});
				
					errors['sasa_phone_work_c'] = errors['sasa_phone_work_c'] || {};
					errors['sasa_phone_work_c'].required = true;
				}
			}
		}
		callback(null, fields, errors);
    },
    
    _ValidarNumeric_sasa_phone_other_c: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarNumeric_sasa_phone_other_c';
		if (!_.isEmpty(self.model.get('sasa_phone_other_c'))){
			if(/^\d+$/.exec(self.model.get('sasa_phone_other_c')) === null){//https://stackoverflow.com/questions/3291289/preg-match-in-javascript		
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Debe ingresar sólo números en los teléfonos de contactación"],
					autoClose: false,
				});
			
				errors['sasa_phone_other_c'] = errors['sasa_phone_other_c'] || {};
				errors['sasa_phone_other_c'].required = true;
			}
			if (self.model.get('sasa_phone_other_c').length<7) {
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar siete dígitos en Teléfono 3"],
					autoClose: false,
				});
			
				errors['sasa_phone_other_c'] = errors['sasa_phone_other_c'] || {};
				errors['sasa_phone_other_c'].required = true;
			}
			//Validar si los teléfonos están iguales
			if (!_.isEmpty(self.model.get('sasa_phone_home_c'))) {
				if (self.model.get('sasa_phone_other_c') == self.model.get('sasa_phone_home_c')) {
					app.alert.show(message_id, {
						level: 'info',
						title: 'Nota',
						messages: ["Los Teléfonos 2 y Teléfonos 3 no pueden ser iguales"],
						autoClose: false,
					});
				
					errors['sasa_phone_other_c'] = errors['sasa_phone_other_c'] || {};
					errors['sasa_phone_other_c'].required = true;
				}
			}
		}
		callback(null, fields, errors);
    },
    
    _ValidarNumeric_sasa_cel_alternativo_c: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarNumeric_sasa_cel_alternativo_c';
		if (!_.isEmpty(self.model.get('sasa_cel_alternativo_c'))){
			if(/^\d+$/.exec(self.model.get('sasa_cel_alternativo_c')) === null){//https://stackoverflow.com/questions/3291289/preg-match-in-javascript		
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Debe ingresar sólo números en los teléfonos de contactación"],
					autoClose: false,
				});
			
				errors['sasa_cel_alternativo_c'] = errors['sasa_cel_alternativo_c'] || {};
				errors['sasa_cel_alternativo_c'].required = true;
			}
			if (self.model.get('sasa_cel_alternativo_c').length<10) {
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar diez dígitos en Celular 2"],
					autoClose: false,
				});
			
				errors['sasa_cel_alternativo_c'] = errors['sasa_cel_alternativo_c'] || {};
				errors['sasa_cel_alternativo_c'].required = true;
			}
			//Validar duplicidad entre celulares
			if (!_.isEmpty(self.model.get('sasa_celular_otro_c'))) {
				if (self.model.get('sasa_cel_alternativo_c') == self.model.get('sasa_celular_otro_c')) {
					app.alert.show(message_id, {
						level: 'info',
						title: 'Nota',
						messages: ["Los Celulares 2 y Celulares 3 no pueden ser iguales"],
						autoClose: false,
					});
				
					errors['sasa_cel_alternativo_c'] = errors['sasa_cel_alternativo_c'] || {};
					errors['sasa_cel_alternativo_c'].required = true;
				}
			}
		}
		callback(null, fields, errors);
    },
    
    _ValidarDatosDeContactacion: function(fields, errors, callback) {
		var self=this;
		message_id='ValidarDatosDeContactacion';
		if (_.isEmpty(self.model.get('sasa_phone_mobile_c')) && _.isEmpty(self.model.get('sasa_phone_home_c')) && _.isEmpty(self.model.get('email'))){			
			app.alert.show(message_id, {
				level: 'info',
				title: 'Nota',
				messages: ["Debe ingresar al menos un dato de contacto"],
				autoClose: false,
			});
			
			errors['sasa_phone_mobile_c'] = errors['sasa_phone_mobile_c'] || {};
			errors['sasa_phone_mobile_c'].required = true;
			
			errors['sasa_phone_home_c'] = errors['sasa_phone_home_c'] || {};
			errors['sasa_phone_home_c'].required = true;
			
			errors['email'] = errors['email'] || {};
			errors['email'].required = true;
		}
		callback(null, fields, errors);
    },


    _ValidaCantidad_sasa_celular_otro_c: function(fields, errors, callback){
    	var self=this;
		message_id='ValidarNumeric_sasa_celular_otro_c';
		if (!_.isEmpty(self.model.get('sasa_celular_otro_c'))){
			if(/^\d+$/.exec(self.model.get('sasa_celular_otro_c')) === null){//https://stackoverflow.com/questions/3291289/preg-match-in-javascript					
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Debe ingresar sólo números en los teléfonos de contactación"],
					autoClose: false,
				});
			
				errors['sasa_celular_otro_c'] = errors['sasa_celular_otro_c'] || {};
				errors['sasa_celular_otro_c'].required = true;
			}
			if (self.model.get('sasa_celular_otro_c').length<10) {
				app.alert.show(message_id, {
					level: 'info',
					title: 'Nota',
					messages: ["Ingresar diez dígitos en Celular 3"],
					autoClose: false,
				});
			
				errors['sasa_celular_otro_c'] = errors['sasa_celular_otro_c'] || {};
				errors['sasa_celular_otro_c'].required = true;
			}
			//Validar duplicidad entre celulares
			if (!_.isEmpty(self.model.get('sasa_phone_mobile_c'))) {
				if (self.model.get('sasa_celular_otro_c') == self.model.get('sasa_phone_mobile_c')) {
					app.alert.show(message_id, {
						level: 'info',
						title: 'Nota',
						messages: ["Los Celulares 3 y Celulares 1 no pueden ser iguales"],
						autoClose: false,
					});
				
					errors['sasa_celular_otro_c'] = errors['sasa_celular_otro_c'] || {};
					errors['sasa_celular_otro_c'].required = true;
				}
			}
		}		
		callback(null, fields, errors);
    },
})

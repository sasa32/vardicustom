<?php
$viewdefs['Contacts'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'dismiss_label' => true,
                'type' => 'fullname',
                'fields' => 
                array (
                  0 => 'salutation',
                  1 => 'first_name',
                  2 => 'last_name',
                ),
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'sasa_nombres_c',
                'label' => 'LBL_SASA_NOMBRES_C',
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'name' => 'sasa_primerapellido_c',
                'label' => 'LBL_SASA_PRIMERAPELLIDO_C',
              ),
              3 => 
              array (
                'name' => 'sasa_last_name_2_c',
                'label' => 'LBL_SASA_LAST_NAME_2_C',
              ),
              4 => 'account_name',
              5 => 
              array (
                'name' => 'sasa_genero_c',
                'label' => 'LBL_SASA_GENERO_C',
              ),
              6 => 
              array (
                'name' => 'sasa_tipo_documento_c',
                'label' => 'LBL_SASA_TIPO_DOCUMENTO_C',
              ),
              7 => 
              array (
                'name' => 'sasa_numero_documento_c',
                'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
              ),
              8 => 
              array (
                'name' => 'sasa_phone_mobile_c',
                'label' => 'LBL_SASA_PHONE_MOBILE_C',
              ),
              9 => 
              array (
                'name' => 'sasa_cel_alternativo_c',
                'label' => 'LBL_SASA_CEL_ALTERNATIVO_C',
              ),
              10 => 
              array (
                'name' => 'sasa_phone_home_c',
                'label' => 'LBL_SASA_PHONE_HOME_C',
              ),
              11 => 
              array (
                'name' => 'sasa_municipios_contacts_3_name',
                'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE',
              ),
              12 => 
              array (
                'name' => 'sasa_phone_work_c',
                'label' => 'LBL_SASA_PHONE_WORK_C',
              ),
              13 => 
              array (
                'name' => 'sasa_extension_c',
                'label' => 'LBL_SASA_EXTENSION_C',
              ),
              14 => 
              array (
                'name' => 'sasa_municipios_contacts_4_name',
                'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE',
              ),
              15 => 
              array (
                'name' => 'email',
              ),
              16 => 
              array (
                'name' => 'sasa_phone_other_c',
                'label' => 'LBL_SASA_PHONE_OTHER_C',
              ),
              17 => 
              array (
                'name' => 'sasa_municipios_contacts_5_name',
                'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE',
              ),
              18 => 
              array (
                'name' => 'sasa_municipios_contacts_1_name',
                'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE',
              ),
              19 => 
              array (
                'name' => 'sasa_municipios_contacts_2_name',
                'label' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE',
              ),
              20 => 
              array (
                'name' => 'primary_address',
                'type' => 'fieldset',
                'css_class' => 'address',
                'label' => 'LBL_PRIMARY_ADDRESS',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'primary_address_street',
                    'css_class' => 'address_street',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_STREET',
                  ),
                  1 => 
                  array (
                    'name' => 'primary_address_city',
                    'css_class' => 'address_city',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_CITY',
                  ),
                  2 => 
                  array (
                    'name' => 'primary_address_state',
                    'css_class' => 'address_state',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_STATE',
                  ),
                  3 => 
                  array (
                    'name' => 'primary_address_postalcode',
                    'css_class' => 'address_zip',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
                  ),
                  4 => 
                  array (
                    'name' => 'primary_address_country',
                    'css_class' => 'address_country',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
                  ),
                ),
              ),
              21 => 
              array (
                'name' => 'alt_address',
                'type' => 'fieldset',
                'css_class' => 'address',
                'label' => 'LBL_ALT_ADDRESS',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'alt_address_street',
                    'css_class' => 'address_street',
                    'placeholder' => 'LBL_ALT_ADDRESS_STREET',
                  ),
                  1 => 
                  array (
                    'name' => 'alt_address_city',
                    'css_class' => 'address_city',
                    'placeholder' => 'LBL_ALT_ADDRESS_CITY',
                  ),
                  2 => 
                  array (
                    'name' => 'alt_address_state',
                    'css_class' => 'address_state',
                    'placeholder' => 'LBL_ALT_ADDRESS_STATE',
                  ),
                  3 => 
                  array (
                    'name' => 'alt_address_postalcode',
                    'css_class' => 'address_zip',
                    'placeholder' => 'LBL_ALT_ADDRESS_POSTALCODE',
                  ),
                  4 => 
                  array (
                    'name' => 'alt_address_country',
                    'css_class' => 'address_country',
                    'placeholder' => 'LBL_ALT_ADDRESS_COUNTRY',
                  ),
                  5 => 
                  array (
                    'name' => 'copy',
                    'label' => 'NTC_COPY_PRIMARY_ADDRESS',
                    'type' => 'copy',
                    'mapping' => 
                    array (
                      'primary_address_street' => 'alt_address_street',
                      'primary_address_city' => 'alt_address_city',
                      'primary_address_state' => 'alt_address_state',
                      'primary_address_postalcode' => 'alt_address_postalcode',
                      'primary_address_country' => 'alt_address_country',
                    ),
                  ),
                ),
              ),
              22 => 
              array (
                'name' => 'sasa_cargo_c',
                'label' => 'LBL_SASA_CARGO_C',
              ),
              23 => 
              array (
              ),
              24 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
            ),
          ),
          2 => 
          array (
            'columns' => 2,
            'name' => 'panel_hidden',
            'label' => 'LBL_RECORD_SHOWMORE',
            'hide' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              1 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              2 => 
              array (
                'name' => 'birthdate',
                'comment' => 'The birthdate of the contact',
                'label' => 'LBL_BIRTHDATE',
              ),
              3 => 
              array (
                'name' => 'sasa_edad_c',
                'label' => 'LBL_SASA_EDAD_C',
              ),
              4 => 
              array (
                'name' => 'sasa_estado_civil_c',
                'label' => 'LBL_SASA_ESTADO_CIVIL_C',
              ),
              5 => 
              array (
                'name' => 'sasa_cliente_fallecido_c',
                'label' => 'LBL_SASA_CLIENTE_FALLECIDO_C',
              ),
              6 => 
              array (
                'name' => 'sasa_tiene_hijos_c',
                'label' => 'LBL_SASA_TIENE_HIJOS_C',
              ),
              7 => 
              array (
                'name' => 'sasa_numero_hijos_c',
                'label' => 'LBL_SASA_NUMERO_HIJOS_C',
              ),
              8 => 
              array (
                'name' => 'sasa_profesion_c',
                'label' => 'LBL_SASA_PROFESION_C',
              ),
              9 => 
              array (
                'name' => 'sasa_nivel_educativo_c',
                'label' => 'LBL_SASA_NIVEL_EDUCATIVO_C',
              ),
              10 => 
              array (
                'name' => 'sasa_tipo_de_vivienda_c',
                'label' => 'LBL_SASA_TIPO_DE_VIVIENDA_C',
              ),
              11 => 
              array (
                'name' => 'sasa_estrato_c',
                'label' => 'LBL_SASA_ESTRATO_C',
              ),
              12 => 
              array (
                'name' => 'sasa_ocupacion_c',
                'label' => 'LBL_SASA_OCUPACION_C',
              ),
              13 => 
              array (
                'name' => 'sasa_deportes_c',
                'label' => 'LBL_SASA_DEPORTES_C',
              ),
              14 => 
              array (
                'name' => 'sasa_mascota_c',
                'label' => 'LBL_SASA_MASCOTA_C',
              ),
              15 => 
              array (
                'name' => 'sasa_musica_c',
                'label' => 'LBL_SASA_MUSICA_C',
              ),
              16 => 'twitter',
              17 => 
              array (
                'name' => 'facebook',
                'comment' => 'The facebook name of the user',
                'label' => 'LBL_FACEBOOK',
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);

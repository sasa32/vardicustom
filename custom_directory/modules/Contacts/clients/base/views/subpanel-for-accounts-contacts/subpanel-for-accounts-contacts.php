<?php
// created: 2022-06-17 20:42:25
$viewdefs['Contacts']['base']['view']['subpanel-for-accounts-contacts'] = array (
  'type' => 'subpanel-list',
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'salutation',
            1 => 'first_name',
            2 => 'last_name',
          ),
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_tipo_documento_c',
          'label' => 'LBL_SASA_TIPO_DOCUMENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_numero_documento_c',
          'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_genero_c',
          'label' => 'LBL_SASA_GENERO_C',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_cargo_c',
          'label' => 'LBL_SASA_CARGO_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_phone_mobile_c',
          'label' => 'LBL_SASA_PHONE_MOBILE_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_phone_home_c',
          'label' => 'LBL_SASA_PHONE_HOME_C',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'sasa_phone_work_c',
          'label' => 'LBL_SASA_PHONE_WORK_C',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'email',
          'label' => 'LBL_LIST_EMAIL',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
);
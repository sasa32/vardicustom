<?php
// created: 2023-02-08 11:06:10
$viewdefs['Contacts']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'salutation',
            1 => 'first_name',
            2 => 'last_name',
          ),
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_numero_documento_c',
          'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'account_name',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_estado_civil_c',
          'label' => 'LBL_SASA_ESTADO_CIVIL_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'birthdate',
          'label' => 'LBL_BIRTHDATE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_edad_c',
          'label' => 'LBL_SASA_EDAD_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_genero_c',
          'label' => 'LBL_SASA_GENERO_C',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'sasa_phone_mobile_c',
          'label' => 'LBL_SASA_PHONE_MOBILE_C',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'sasa_phone_home_c',
          'label' => 'LBL_SASA_PHONE_HOME_C',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'sasa_phone_work_c',
          'label' => 'LBL_SASA_PHONE_WORK_C',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'email',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'date_entered',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
        ),
        12 => 
        array (
          'name' => 'date_modified',
          'enabled' => true,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'phone_mobile',
          'enabled' => true,
          'default' => true,
          'selected' => false,
        ),
        14 => 
        array (
          'name' => 'phone_other',
          'enabled' => true,
          'default' => true,
          'selected' => false,
        ),
        15 => 
        array (
          'name' => 'assistant_phone',
          'enabled' => true,
          'default' => true,
          'selected' => false,
        ),
      ),
    ),
  ),
);
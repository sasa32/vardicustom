<?php  
	
	/**
	 * 
	 */
	require_once('include/SugarQuery/SugarQuery.php');
	class Loader_Contacts{
		var $return;
		var $db;
		
		function __construct($data_array){
			$this->return = array();
			$this->db = DBManagerFactory::getInstance();
			$this->main_contactos($data_array);
		}

		public function main_contactos($data_array){
			try{
				foreach ($data_array as $key => $value) {
					$validate = $this->validate_record($value);
					if($validate['band']){
						$status = $this->create_contacto($value);
						$this->return[] = $status;
					}else{
						$this->return[] =array('Error' => '120', 'Description' => implode(' - ', $validate['error']));
					}
				}
			}catch(Exception $e){
				return array('Error' => '100', 'Description' => $e->getMessage());
			}
		}

		public function create_contacto($record){
			if ($record['id_register_c']!=null) {
				if ($record['sasa_numero_documento_c'] == "" || $record['sasa_numero_documento_c'] == null) {
					$sql_get_contact= "SELECT id FROM contacts INNER JOIN contacts_cstm ON contacts.id=contacts_cstm.id_c WHERE sasa_lote_c='{$record['sasa_lote_c']}' AND id_register_c='{$record['id_register_c']}' AND deleted=0";
				}else{
					$sql_get_contact= "SELECT id FROM contacts INNER JOIN contacts_cstm ON contacts.id=contacts_cstm.id_c WHERE contacts_cstm.sasa_numero_documento_c='{$record['sasa_numero_documento_c']}' AND deleted=0";
				}
				
			
				$contact =  $this->db->query($sql_get_contact);
				while($row = $this->db->fetchByAssoc($contact)){
					$contact_state_id = $row['id'];
				}

				if(!empty($contact_state_id)){
					$status = "Update";
					$Contacto = BeanFactory::getBean('Contacts', $contact_state_id, array('disable_row_level_security' => true));
				}else{
					$status = "Created";
					$Contacto = BeanFactory::newBean('Contacts');
				}

				$dir_address2 = array();
				foreach ($record as $key => $value) {
					if ($value == "(null)") {
						$Contacto->{$key} = null;
					}else{
						$Contacto->{$key} = $value;
						switch ($key) {
							case 'email1':
								if ($value !== "(null)") {
									$Contacto->email1 = $value;
								}
								break;
							case 'emailsecundarios':
								if ($value !== "(null)" || $value != "" || $value != null) {
									//Separando las direcciones de correo
									$dir_address = explode(";", $value);
									array_push($dir_address2, $dir_address);
								}
								break;
							case 'sasa_deportes_c':
								//Separar cada valor
								$deporlimit = explode(";", $value);
								
								$deportes = array();
								foreach ($deporlimit as $valor) {
								    //Agrega ^ a cada valor ya que el campo es selección multiple
								    $deportes[] = "^".$valor."^";
								}
								
								$deportes = implode(',', $deportes);
								$Contacto->sasa_deportes_c = $deportes;
								break;
							case 'sasa_musica_c':
								//Separar cada valor
								$musiclimit = explode(";", $value);
								
								$musica = array();
								foreach ($musiclimit as $valor) {
								    //Agrega ^ a cada valor ya que el campo es selección multiple
								    $musica[] = "^".$valor."^";
								}
								
								$musica = implode(',', $musica);
								$Contacto->sasa_musica_c = $musica;
								break;
							case 'numcuenta':
								$numeroCuenta = rtrim($value);
								$sql_get_account= $this->db->query("SELECT id FROM accounts INNER JOIN accounts_cstm ON accounts.id=accounts_cstm.id_c WHERE accounts_cstm.sasa_numero_documento_c='{$numeroCuenta}' AND deleted=0");
								$id_cuenta = $this->db->fetchByAssoc($sql_get_account);
								$Contacto->account_id = $id_cuenta['id'];
								break;
							case 'date_entered':
								$date = date("d-m-Y", strtotime($value));
								$date = new DateTime($date);
								$dateformat = $date->format('Y-m-d');
								$Contacto->creacionsigru_c = $dateformat;
								break;
							case 'date_modified':
								//$date = new DateTime($value);
								$date = date("d-m-Y", strtotime($value));
								$date = new DateTime($date);
								$dateformat = $date->format('Y-m-d');
								$Contacto->modificacionsigru_c = $dateformat;
								break;
							default:
								# code...
								break;
						}
					}	
					
				}

				$Contacto->save();

				//Relacionar Direcciones de correo secundrias con la Cuenta.
				$dir_email = new SugarEmailAddress;
				if (!empty($Contacto->email1)) {
					//Asociar el email principal
					$dir_email->addAddress($Contacto->email1,true);
				}
				foreach ($dir_address2[0] as $llave => $valor) {
					$dir_address = substr($valor, 0,-4);
					if (!empty($dir_address)) {
						if ($dir_address != $Contacto->email1) {
							//Asociar los emails secundarios
							$dir_email->addAddress($dir_address,false);
						}
					}
				}
				$dir_email->save($Contacto->id, "Contacts");

				return array('status' => $status, 'id' => $Contacto->id);
			}
			 
		}


		public function validate_record($record){
			$validate_array = array('band' => true);

			return $validate_array;
		}

		public function get_return(){
			return $this->return;
		}
	}


?>
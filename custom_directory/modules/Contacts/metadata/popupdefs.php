<?php
$popupMeta = array (
    'moduleMain' => 'Contact',
    'varName' => 'CONTACT',
    'orderBy' => 'contacts.first_name, contacts.last_name',
    'whereClauses' => array (
  'first_name' => 'contacts.first_name',
  'account_name' => 'accounts.name',
  'title' => 'contacts.title',
  'email' => 'contacts.email',
  'sasa_numero_documento_c' => 'contacts_cstm.sasa_numero_documento_c',
  'phone_home' => 'contacts.phone_home',
  'phone_mobile' => 'contacts.phone_mobile',
  'date_entered' => 'contacts.date_entered',
  'date_modified' => 'contacts.date_modified',
),
    'searchInputs' => array (
  0 => 'first_name',
  2 => 'account_name',
  3 => 'email',
  4 => 'title',
  5 => 'sasa_numero_documento_c',
  8 => 'phone_home',
  9 => 'phone_mobile',
  10 => 'date_entered',
  11 => 'date_modified',
),
    'create' => array (
  'formBase' => 'ContactFormBase.php',
  'formBaseClass' => 'ContactFormBase',
  'getFormBodyParams' => 
  array (
    0 => '',
    1 => '',
    2 => 'ContactSave',
  ),
  'createButton' => 'LNK_NEW_CONTACT',
),
    'searchdefs' => array (
  'first_name' => 
  array (
    'name' => 'first_name',
    'width' => '10',
  ),
  'account_name' => 
  array (
    'name' => 'account_name',
    'type' => 'varchar',
    'width' => '10',
  ),
  'sasa_numero_documento_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
    'width' => '10',
    'name' => 'sasa_numero_documento_c',
  ),
  'title' => 
  array (
    'name' => 'title',
    'width' => '10',
  ),
  'email' => 
  array (
    'name' => 'email',
    'width' => '10',
  ),
  'phone_home' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_HOME_PHONE',
    'width' => 10,
    'name' => 'phone_home',
  ),
  'phone_mobile' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => 10,
    'name' => 'phone_mobile',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'name' => 'date_modified',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
      2 => 'salutation',
      3 => 'account_name',
      4 => 'account_id',
    ),
    'name' => 'name',
  ),
  'SASA_NUMERO_DOCUMENTO_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_NUMERO_DOCUMENTO_C',
    'width' => 10,
    'default' => true,
  ),
  'ACCOUNT_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_LIST_ACCOUNT_NAME',
    'module' => 'Accounts',
    'id' => 'ACCOUNT_ID',
    'default' => true,
    'sortable' => true,
    'ACLTag' => 'ACCOUNT',
    'related_fields' => 
    array (
      0 => 'account_id',
    ),
    'name' => 'account_name',
  ),
  'SASA_ESTADO_CIVIL_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_ESTADO_CIVIL_C',
    'width' => 10,
    'name' => 'sasa_estado_civil_c',
  ),
  'BIRTHDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BIRTHDATE',
    'width' => 10,
    'default' => true,
    'name' => 'birthdate',
  ),
  'SASA_EDAD_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_EDAD_C',
    'width' => 10,
    'default' => true,
    'name' => 'sasa_edad_c',
  ),
  'SASA_GENERO_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_GENERO_C',
    'width' => 10,
    'name' => 'sasa_genero_c',
  ),
  'SASA_PHONE_MOBILE_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_PHONE_MOBILE_C',
    'width' => 10,
    'default' => true,
    'name' => 'sasa_phone_mobile_c',
  ),
  'SASA_PHONE_HOME_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_PHONE_HOME_C',
    'width' => 10,
    'default' => true,
    'name' => 'sasa_phone_home_c',
  ),
  'SASA_PHONE_WORK_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SASA_PHONE_WORK_C',
    'width' => 10,
    'default' => true,
    'name' => 'sasa_phone_work_c',
  ),
  'EMAIL' => 
  array (
    'type' => 'email',
    'studio' => 
    array (
      'visible' => true,
      'searchview' => true,
      'editview' => true,
      'editField' => true,
    ),
    'link' => 'email_addresses_primary',
    'label' => 'LBL_EMAIL_ADDRESS',
    'sortable' => false,
    'width' => 10,
    'default' => true,
    'name' => 'email',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
    'name' => 'date_entered',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
    'name' => 'date_modified',
  ),
),
);

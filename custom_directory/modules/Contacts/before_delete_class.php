<?php

   if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

   class before_delete_class
   {
      function before_delete_method($bean, $event, $arguments)
	  {
		require_once("custom/modules/integration_sigru/DeleteContact.php");
		$sendsigru = new DeleteContact($bean->id,$bean->module_name);
	  }
   }

?>
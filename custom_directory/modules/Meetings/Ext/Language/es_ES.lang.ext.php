<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.customsasa_puntos_de_ventas_meetings_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.customsasa_puntos_de_ventas_meetings_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Puntos de Atención';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Puntos de Atención';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE'] = 'Puntos de Atención';
$mod_strings['LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Puntos de Atención';


?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.gvmeetingscustomfieldsv0012020102010.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.gvmeetingscustomfieldsv0022020111909.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.gvmeetingscustomfieldsv0032021010509.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';
$mod_strings['LBL_CASORELACION_C'] = 'Número de Caso relacionado';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.gvmeetingscustomfieldsv0042021032901.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre de Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';
$mod_strings['LBL_CASORELACION_C'] = 'Número de Caso relacionado';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.gvmeetingscustomfieldsv0052021040508.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.gvmeetingscustomfieldsv0052021040508.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre de Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';
$mod_strings['LBL_CASORELACION_C'] = 'Número de Caso relacionado';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/temp.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre de Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';
$mod_strings['LBL_CASORELACION_C'] = 'Número de Caso relacionado';


?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LIST_CONTACT'] = 'Contacto';
$mod_strings['LBL_CREATE_CONTACT'] = 'Como Contacto';
$mod_strings['LBL_HISTORY_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Reunión';
$mod_strings['LBL_MODULE_NAME'] = 'Reuniones';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Reunión';
$mod_strings['LNK_MEETING_LIST'] = 'Ver Reuniones';
$mod_strings['LNK_IMPORT_MEETINGS'] = 'Importar Reuniones';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Reuniones';
$mod_strings['LBL_MEETING'] = 'Reunión:';
$mod_strings['LBL_MODULE_TITLE'] = 'Reuniones: Inicio';
$mod_strings['LBL_CREATE_MODULE'] = 'Programar Reunión';
$mod_strings['LBL_RELATED_RECORD_DEFAULT_NAME'] = 'Reunión con {{{this}}}';
$mod_strings['LBL_REMINDER_TITLE'] = 'Reunión:';
$mod_strings['LBL_CREATE_LEAD'] = 'Como Lead';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_MEETINGS_LEADS_FROM_MEETINGS_TITLE'] = 'Leads';
$mod_strings['LBL_CALENDAR_START_DATE'] = 'Fecha/Hora Inicio';
$mod_strings['LBL_CALENDAR_END_DATE'] = 'Fecha/Hora Fin';
$mod_strings['LBL_PARENT_TYPE'] = 'Tipo de Registro Padre';
$mod_strings['LBL_STATUS'] = 'Estado';
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre de Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';
$mod_strings['LBL_TYPE'] = 'Tipo de reunión N/A';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_DE_RECO_CITA_C'] = 'de reco cita c';
$mod_strings['LBL_CD_GRUP_C'] = 'Motivo de la Visita';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.gvmeetingscustomfieldsv0052021112609.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre de Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';
$mod_strings['LBL_CASORELACION_C'] = 'Número de Caso relacionado';
$mod_strings['LBL_SASA_NOMBREAGENTE_C'] = 'Nombre Agente';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/es_ES.GVMeetingsCustomFieldsv0072022010603.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre de Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';
$mod_strings['LBL_CASORELACION_C'] = 'Número de Caso relacionado';
$mod_strings['LBL_SASA_NOMBREAGENTE_C'] = 'Nombre Agente';
$mod_strings['LBL_SASA_USUARIO_CITA_C'] = 'Usuario creador de la cita';
$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código de la Línea';

?>

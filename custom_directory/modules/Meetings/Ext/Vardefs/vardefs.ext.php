<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sasa_puntos_de_ventas_meetings_1_Meetings.php

// created: 2020-05-15 23:49:35
$dictionary["Meeting"]["fields"]["sasa_puntos_de_ventas_meetings_1"] = array (
  'name' => 'sasa_puntos_de_ventas_meetings_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_meetings_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'side' => 'right',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_MEETINGS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'link-type' => 'one',
);
$dictionary["Meeting"]["fields"]["sasa_puntos_de_ventas_meetings_1_name"] = array (
  'name' => 'sasa_puntos_de_ventas_meetings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_meetings_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'name',
);
$dictionary["Meeting"]["fields"]["sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida"] = array (
  'name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_MEETINGS_TITLE_ID',
  'id_name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_meetings_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/rli_link_workflow.php

$dictionary['Meeting']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/audit.php

$dictionary['Meeting']['audited'] = true;
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/full_text_search_admin.php

 // created: 2020-08-28 14:29:24
$dictionary['Meeting']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_end.php

 // created: 2020-09-22 15:48:01
$dictionary['Meeting']['fields']['date_end']['audited']=false;
$dictionary['Meeting']['fields']['date_end']['options']='';
$dictionary['Meeting']['fields']['date_end']['comments']='Date meeting ends';
$dictionary['Meeting']['fields']['date_end']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_end']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['date_end']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_end']['full_text_search']=array (
);
$dictionary['Meeting']['fields']['date_end']['group_label']='';
$dictionary['Meeting']['fields']['date_end']['calculated']=false;
$dictionary['Meeting']['fields']['date_end']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_start.php

 // created: 2020-09-22 15:46:06
$dictionary['Meeting']['fields']['date_start']['audited']=true;
$dictionary['Meeting']['fields']['date_start']['options']='';
$dictionary['Meeting']['fields']['date_start']['comments']='Date of start of meeting';
$dictionary['Meeting']['fields']['date_start']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_start']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['date_start']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_start']['full_text_search']=array (
);
$dictionary['Meeting']['fields']['date_start']['calculated']=false;
$dictionary['Meeting']['fields']['date_start']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_description.php

 // created: 2020-09-22 15:41:37
$dictionary['Meeting']['fields']['description']['audited']=true;
$dictionary['Meeting']['fields']['description']['massupdate']=false;
$dictionary['Meeting']['fields']['description']['comments']='Full text of the note';
$dictionary['Meeting']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['description']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.55',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['description']['calculated']=false;
$dictionary['Meeting']['fields']['description']['rows']='6';
$dictionary['Meeting']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_displayed_url.php

 // created: 2020-09-22 15:43:36
$dictionary['Meeting']['fields']['displayed_url']['audited']=false;
$dictionary['Meeting']['fields']['displayed_url']['massupdate']=false;
$dictionary['Meeting']['fields']['displayed_url']['comments']='Meeting URL';
$dictionary['Meeting']['fields']['displayed_url']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['displayed_url']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['displayed_url']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['displayed_url']['reportable']=false;
$dictionary['Meeting']['fields']['displayed_url']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['displayed_url']['calculated']=false;
$dictionary['Meeting']['fields']['displayed_url']['gen']='';
$dictionary['Meeting']['fields']['displayed_url']['link_target']='_self';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_location.php

 // created: 2020-09-22 15:42:35
$dictionary['Meeting']['fields']['location']['audited']=false;
$dictionary['Meeting']['fields']['location']['massupdate']=false;
$dictionary['Meeting']['fields']['location']['comments']='Meeting location';
$dictionary['Meeting']['fields']['location']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['location']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['location']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['location']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.36',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['location']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_name.php

 // created: 2020-09-22 15:29:44
$dictionary['Meeting']['fields']['name']['audited']=true;
$dictionary['Meeting']['fields']['name']['massupdate']=false;
$dictionary['Meeting']['fields']['name']['comments']='Meeting name';
$dictionary['Meeting']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['name']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.43',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_parent_type.php

 // created: 2020-09-22 15:48:48
$dictionary['Meeting']['fields']['parent_type']['len']='100';
$dictionary['Meeting']['fields']['parent_type']['audited']=false;
$dictionary['Meeting']['fields']['parent_type']['massupdate']=false;
$dictionary['Meeting']['fields']['parent_type']['comments']='Module meeting is associated with';
$dictionary['Meeting']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['parent_type']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['parent_type']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_password.php

 // created: 2020-09-22 15:43:03
$dictionary['Meeting']['fields']['password']['audited']=false;
$dictionary['Meeting']['fields']['password']['massupdate']=false;
$dictionary['Meeting']['fields']['password']['comments']='Meeting password';
$dictionary['Meeting']['fields']['password']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['password']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['password']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['password']['reportable']=false;
$dictionary['Meeting']['fields']['password']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['password']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_status.php

 // created: 2020-10-16 15:45:56
$dictionary['Meeting']['fields']['status']['default']='A';
$dictionary['Meeting']['fields']['status']['required']=true;
$dictionary['Meeting']['fields']['status']['audited']=true;
$dictionary['Meeting']['fields']['status']['massupdate']=true;
$dictionary['Meeting']['fields']['status']['options']='status_reu_list_c';
$dictionary['Meeting']['fields']['status']['comments']='Meeting status (ex: Planned, Held, Not held)';
$dictionary['Meeting']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['status']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['status']['calculated']=false;
$dictionary['Meeting']['fields']['status']['dependency']=false;
$dictionary['Meeting']['fields']['status']['full_text_search']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_type.php

 // created: 2020-10-07 09:33:53
$dictionary['Meeting']['fields']['type']['len']=100;
$dictionary['Meeting']['fields']['type']['audited']=false;
$dictionary['Meeting']['fields']['type']['comments']='Meeting type (ex: WebEx, Other)';
$dictionary['Meeting']['fields']['type']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['type']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['type']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['type']['reportable']=false;
$dictionary['Meeting']['fields']['type']['calculated']=false;
$dictionary['Meeting']['fields']['type']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_cd_grup_c.php

 // created: 2022-01-06 15:48:18
$dictionary['Meeting']['fields']['cd_grup_c']['labelValue']='Motivo de la Visita';
$dictionary['Meeting']['fields']['cd_grup_c']['dependency']='';
$dictionary['Meeting']['fields']['cd_grup_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_de_obse_c.php

 // created: 2022-01-06 15:48:18
$dictionary['Meeting']['fields']['de_obse_c']['labelValue']='Detalle';
$dictionary['Meeting']['fields']['de_obse_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['de_obse_c']['enforced']='false';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_de_obse_clie_c.php

 // created: 2022-01-06 15:48:18
$dictionary['Meeting']['fields']['de_obse_clie_c']['labelValue']='Otros Servicios';
$dictionary['Meeting']['fields']['de_obse_clie_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['de_obse_clie_c']['enforced']='';
$dictionary['Meeting']['fields']['de_obse_clie_c']['required_formula']='';
$dictionary['Meeting']['fields']['de_obse_clie_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_de_reco_cita_c.php

 // created: 2022-01-06 15:48:18
$dictionary['Meeting']['fields']['de_reco_cita_c']['labelValue']='Canal de Recordatorio Cita';
$dictionary['Meeting']['fields']['de_reco_cita_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_nu_nit_empl_c.php

 // created: 2022-01-06 15:48:18
$dictionary['Meeting']['fields']['nu_nit_empl_c']['labelValue']='Asesor';
$dictionary['Meeting']['fields']['nu_nit_empl_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['nu_nit_empl_c']['enforced']='';
$dictionary['Meeting']['fields']['nu_nit_empl_c']['dependency']='equal($sasa_tipo_reunion_c,"A")';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_casorelacion_c.php

 // created: 2022-01-06 15:48:19

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_cod_taller_c.php

 // created: 2022-01-06 15:48:19
$dictionary['Meeting']['fields']['sasa_cod_taller_c']['labelValue']='Código Taller';
$dictionary['Meeting']['fields']['sasa_cod_taller_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_cod_taller_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_cod_taller_c']['dependency']='equal($sasa_tipo_reunion_c,"A")';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_fuente_c.php

 // created: 2022-01-06 15:48:19
$dictionary['Meeting']['fields']['sasa_fuente_c']['labelValue']='Fuente';
$dictionary['Meeting']['fields']['sasa_fuente_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_fuente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_origen_cita_c.php

 // created: 2022-01-06 15:48:19
$dictionary['Meeting']['fields']['sasa_origen_cita_c']['labelValue']='Origen Cita';
$dictionary['Meeting']['fields']['sasa_origen_cita_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_origen_cita_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_origen_cita_c']['dependency']='equal($sasa_tipo_reunion_c,"A")';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_tipo_reunion_c.php

 // created: 2022-01-06 15:48:19
$dictionary['Meeting']['fields']['sasa_tipo_reunion_c']['labelValue']='Tipo de Reunión';
$dictionary['Meeting']['fields']['sasa_tipo_reunion_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_tipo_reunion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_tipo_evento_c.php

 // created: 2022-01-06 15:48:19
$dictionary['Meeting']['fields']['sasa_tipo_evento_c']['labelValue']='Tipo de Evento';
$dictionary['Meeting']['fields']['sasa_tipo_evento_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_tipo_evento_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipo_reunion_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'A' => 
    array (
      0 => '',
      1 => 'A',
      2 => 'B',
      3 => 'F',
    ),
    'B' => 
    array (
      0 => '',
      1 => 'C',
      2 => 'D',
      3 => 'E',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_nombreagente_c.php

 // created: 2022-01-06 15:48:19

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_usuario_cita_c.php

 // created: 2022-01-06 15:48:19

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_cod_linea_c.php

 // created: 2022-01-06 15:48:19

 
?>

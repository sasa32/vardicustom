<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Dependencies/ReadOnlyMeetings.php

$dependencies['Meetings']['ReadOnlyMeetings'] = array(
	'hooks' => array("all"),
	'trigger' => 'equal($sasa_tipo_reunion_c,"A")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'cd_grup_c',
                'value' => 'equal($sasa_tipo_reunion_c,"A")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'de_obse_c',
                'value' => 'equal($sasa_tipo_reunion_c,"A")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'de_reco_cita_c',
                'value' => 'equal($sasa_tipo_reunion_c,"A")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'de_obse_clie_c',
                'value' => 'equal($sasa_tipo_reunion_c,"A")',
            ),
        ),
		array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'cd_grup_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'de_obse_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'de_reco_cita_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'de_obse_clie_c',
                'value' => 'true',
            ),
        ),
	),
    'notActions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'de_obse_c',
                'value' => 'false'
            )
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'cd_grup_c',
                'value' => 'false'
            )
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'de_reco_cita_c',
                'value' => 'false'
            )
        ),
    )
);

?>

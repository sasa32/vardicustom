<?php
// created: 2023-02-08 11:06:10
$viewdefs['Meetings']['mobile']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'default' => true,
          'enabled' => true,
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'date_start',
          'label' => 'LBL_DATE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_tipo_reunion_c',
          'label' => 'LBL_SASA_TIPO_REUNION_C',
          'enabled' => true,
          'default' => false,
        ),
        3 => 
        array (
          'name' => 'nu_nit_empl_c',
          'label' => 'LBL_NU_NIT_EMPL',
          'enabled' => true,
          'default' => false,
        ),
        4 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_TEAMS',
          'enabled' => true,
          'id' => 'TEAM_ID',
          'link' => true,
          'sortable' => false,
          'default' => false,
        ),
        5 => 
        array (
          'name' => 'sasa_puntos_de_ventas_meetings_1_name',
          'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
          'enabled' => true,
          'id' => 'SASA_PUNTOS_DE_VENTAS_MEETINGS_1SASA_PUNTOS_DE_VENTAS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => false,
        ),
        6 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => false,
        ),
        7 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'assigned_user_id',
          ),
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => false,
        ),
      ),
    ),
  ),
);
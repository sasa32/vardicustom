<?php
// created: 2023-02-08 11:06:10
$viewdefs['Meetings']['mobile']['view']['edit'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 'name',
        1 => 
        array (
          'name' => 'date',
          'type' => 'fieldset',
          'related_fields' => 
          array (
            0 => 'date_start',
            1 => 'date_end',
          ),
          'label' => 'LBL_START_AND_END_DATE_DETAIL_VIEW',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_start',
            ),
            1 => 
            array (
              'name' => 'date_end',
              'required' => true,
              'readonly' => false,
            ),
          ),
        ),
        2 => 'status',
        3 => 
        array (
          'name' => 'sasa_tipo_reunion_c',
          'label' => 'LBL_SASA_TIPO_REUNION_C',
        ),
        4 => 
        array (
          'name' => 'sasa_tipo_evento_c',
          'label' => 'LBL_SASA_TIPO_EVENTO_C',
        ),
        5 => 'parent_name',
        6 => 
        array (
          'name' => 'reminder',
          'type' => 'fieldset',
          'orientation' => 'horizontal',
          'related_fields' => 
          array (
            0 => 'reminder_checked',
            1 => 'reminder_time',
          ),
          'label' => 'LBL_REMINDER',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'reminder_checked',
            ),
            1 => 
            array (
              'name' => 'reminder_time',
              'type' => 'enum',
              'options' => 'reminder_time_options',
            ),
          ),
        ),
        7 => 
        array (
          'name' => 'email_reminder',
          'type' => 'fieldset',
          'orientation' => 'horizontal',
          'related_fields' => 
          array (
            0 => 'email_reminder_checked',
            1 => 'email_reminder_time',
          ),
          'label' => 'LBL_EMAIL_REMINDER',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'email_reminder_checked',
            ),
            1 => 
            array (
              'name' => 'email_reminder_time',
              'type' => 'enum',
              'options' => 'reminder_time_options',
            ),
          ),
        ),
        8 => 
        array (
          'name' => 'location',
          'comment' => 'Meeting location',
          'label' => 'LBL_LOCATION',
        ),
        9 => 'description',
        10 => 
        array (
          'name' => 'cd_grup_c',
          'label' => 'LBL_CD_GRUP',
        ),
        11 => 
        array (
          'name' => 'de_obse_c',
          'studio' => 'visible',
          'label' => 'LBL_DE_OBSE',
        ),
        12 => 
        array (
          'name' => 'de_obse_clie_c',
          'studio' => 'visible',
          'label' => 'LBL_DE_OBSE_CLIE',
        ),
        13 => 
        array (
          'name' => 'de_reco_cita_c',
          'label' => 'LBL_DE_RECO_CITA',
        ),
        14 => 
        array (
          'name' => 'sasa_origen_cita_c',
          'label' => 'LBL_SASA_ORIGEN_CITA_C',
        ),
        15 => 
        array (
          'name' => 'sasa_puntos_de_ventas_meetings_1_name',
          'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
        ),
        16 => 
        array (
          'name' => 'nu_nit_empl_c',
          'label' => 'LBL_NU_NIT_EMPL',
        ),
        17 => 'assigned_user_name',
        18 => 'team_name',
        19 => 
        array (
          'name' => 'date_modified',
          'comment' => 'Date record last modified',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_MODIFIED',
        ),
        20 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        21 => 
        array (
          'name' => 'created_by_name',
          'readonly' => true,
          'label' => 'LBL_CREATED',
        ),
      ),
    ),
  ),
);
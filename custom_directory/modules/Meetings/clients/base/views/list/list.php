<?php
// created: 2023-02-08 11:06:10
$viewdefs['Meetings']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_LIST_SUBJECT',
          'link' => true,
          'default' => true,
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'repeat_type',
          ),
        ),
        1 => 
        array (
          'name' => 'parent_name',
          'label' => 'LBL_LIST_RELATED_TO',
          'id' => 'PARENT_ID',
          'link' => true,
          'default' => true,
          'enabled' => true,
          'sortable' => false,
        ),
        2 => 
        array (
          'name' => 'date_start',
          'label' => 'LBL_LIST_DATE',
          'type' => 'datetimecombo-colorcoded',
          'css_class' => 'overflow-visible',
          'completed_status_value' => 'Held',
          'link' => false,
          'default' => true,
          'enabled' => true,
          'readonly' => true,
          'related_fields' => 
          array (
            0 => 'status',
          ),
        ),
        3 => 
        array (
          'name' => 'status',
          'type' => 'event-status',
          'label' => 'LBL_LIST_STATUS',
          'link' => false,
          'default' => true,
          'enabled' => true,
          'css_class' => 'full-width',
        ),
        4 => 
        array (
          'name' => 'sasa_tipo_reunion_c',
          'label' => 'LBL_SASA_TIPO_REUNION_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_USER',
          'id' => 'ASSIGNED_USER_ID',
          'default' => true,
          'enabled' => true,
        ),
        6 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'default' => true,
          'enabled' => true,
          'readonly' => true,
        ),
        7 => 
        array (
          'name' => 'created_by_name',
          'label' => 'LBL_CREATED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'CREATED_BY',
          'link' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'date_end',
          'link' => false,
          'default' => false,
          'enabled' => true,
        ),
        9 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_LIST_TEAM',
          'default' => false,
          'enabled' => true,
        ),
        10 => 
        array (
          'name' => 'sasa_cod_taller_c',
          'label' => 'LBL_SASA_COD_TALLER_C',
          'enabled' => true,
          'default' => false,
        ),
        11 => 
        array (
          'name' => 'email_reminder_time',
          'label' => 'LBL_EMAIL_REMINDER_TIME',
          'enabled' => true,
          'default' => false,
        ),
        12 => 
        array (
          'name' => 'location',
          'label' => 'LBL_LOCATION',
          'enabled' => true,
          'default' => false,
        ),
        13 => 
        array (
          'name' => 'sasa_puntos_de_ventas_meetings_1_name',
          'label' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
          'enabled' => true,
          'id' => 'SASA_PUNTOS_DE_VENTAS_MEETINGS_1SASA_PUNTOS_DE_VENTAS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => false,
        ),
        14 => 
        array (
          'name' => 'cd_grup_c',
          'label' => 'LBL_CD_GRUP',
          'enabled' => true,
          'default' => false,
        ),
        15 => 
        array (
          'name' => 'nu_nit_empl_c',
          'label' => 'LBL_NU_NIT_EMPL',
          'enabled' => true,
          'default' => false,
        ),
        16 => 
        array (
          'name' => 'sasa_origen_cita_c',
          'label' => 'LBL_SASA_ORIGEN_CITA_C',
          'enabled' => true,
          'default' => false,
        ),
      ),
    ),
  ),
);
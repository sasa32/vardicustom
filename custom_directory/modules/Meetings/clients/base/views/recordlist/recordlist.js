({
	extendsFrom: 'RecordlistView',
	initialize: function(options) {
	    this._super('initialize', [options]);
	    
	    // Bind "Set red Next Review Date" functionality with data syncronization
	    this.listenTo(this.collection, 'data:sync:complete', this.renderNextReviewDateColor);
	},

	renderNextReviewDateColor: function(){
		_.each(this.rowFields, function(field) {
        
	        // Get Last Revenue Date field instance
	        var obj = _.findWhere(field, {name: 'sasa_tipo_reunion_c'});
	            
	        // Fix an error when Next Review Date hidden
	        if (!_.isUndefined(obj)) {
	 			obj.$el.css({'color': '#FF0000'});
	        }
	    });
	}

})
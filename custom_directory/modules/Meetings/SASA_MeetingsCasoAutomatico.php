<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_MeetingsCasoAutomatico
{
	function before_save($bean, $event, $arguments)
	{	
		/*
		Tarea en SASA 17557 https://sasaconsultoria.sugarondemand.com/#Tasks/135d0072-6e98-11ec-8db9-06ab610ac1b0
		*/
		try{

			if (!isset($bean->logic_hooks_meetingscaso_ignore_update) || $bean->logic_hooks_meetingscaso_ignore_update === false){//antiloop

				global $db;
				global $app_list_strings;

				$bean->logic_hooks_meetingscaso_ignore_update = true;//antiloop
				
				if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) {
		            //new record
		            if ($bean->sasa_usuario_cita_c != "" || $bean->sasa_usuario_cita_c != NULL) {
		            	
		            	//Buscar El usuario
						$User = BeanFactory::newBean("Users");
						$User->retrieve_by_string_fields( 
							array( 
								'user_name' => $bean->sasa_usuario_cita_c
							)
						);
						//Si el usuario enviado existe en sugar continua con el proceso
						if (!empty($User->id)) {
							// code...
							$Case = BeanFactory::newBean("Cases");

							//Mapeo de campos según el requerimiento
							$Case->assigned_user_id = $User->id;
							$Case->status = 'Closed';
							$Case->cd_cia_c = "2";
							$Case->cd_uneg_cont_c = "210";
							$Case->cd_area_c = "21010";
							$Case->sasa_tipo_c = "F";
							$Case->sasa_motivo_c = "59";
							//GET placa, la placa siempre será el primer valor que tenga el campo description de la reunión, esto está separado por un | 
							$placaMeeting = explode("|", $bean->description);
							$Case->nu_plac_vehi_c = $placaMeeting[0];
							//Mapeo punto de atención
							if (!empty($bean->sasa_cod_taller_c)){
								$resultpunto = $GLOBALS['db']->query("SELECT id FROM sasa_puntos_de_ventas INNER JOIN sasa_puntos_de_ventas_cstm ON sasa_puntos_de_ventas.id=sasa_puntos_de_ventas_cstm.id_c WHERE sasa_puntos_de_ventas_cstm.sasa_codpuntoatencion_c='{$bean->sasa_cod_taller_c}' AND sasa_puntos_de_ventas.deleted=0");

								$sasa_cod_puntoAtencion =  $GLOBALS['db']->fetchByAssoc($resultpunto);
								$Case->sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida = $sasa_cod_puntoAtencion['id'];
							}
							//Mapeo de la observación del caso según el requerimiento
							//Traer toda la lista de motivo
							$list = array();
							if (isset($app_list_strings['motivo_visita_list']))
							{
							    $list = $app_list_strings['motivo_visita_list'];
							}
							
							//Asignar el motivo segun el valor interno
							$strreplace = str_replace("^","",$bean->cd_grup_c);
							$strreplace = explode(",", $strreplace);
							for ($i=0; $i <count($strreplace) ; $i++) {
								if ($i==0) {
									$MotivoVisita .= $list[$strreplace[$i]];
								}else{
									$MotivoVisita .= ",".$list[$strreplace[$i]];
								}
								
							}
							
							$Case->description = $MotivoVisita."|".$bean->de_obse_c."|".$bean->de_obse_clie_c;
							//Mapeo de la resolución del caso.
							$Fechameeting = new DateTime($bean->date_start);
							$Fechameeting->modify('-5 hours');
							$Case->resolution = "Cita programada para ".$Fechameeting->format('Y-m-d H:i:s');
							$Case->source = "T";
							//Mapeo de la cuenta
							$Cuenta = BeanFactory::retrieveBean("Accounts", $bean->parent_id, array('disable_row_level_security' => true));

							if (empty($Cuenta->id) || $bean->parent_id == "") {
								$Case->account_id = "458c0816-6fd5-11ec-8503-069273903c1e";
							}else{
								$Case->account_id = $Cuenta->id;
							}
							//Mapeo de la linea de vehiculo
							//Linea de vehiculo
							if (!empty($bean->sasa_cod_linea_c)){
								$resultpuntovehiculo = $GLOBALS['db']->query("SELECT id FROM sasa_vehiculos INNER JOIN sasa_vehiculos_cstm ON sasa_vehiculos.id=sasa_vehiculos_cstm.id_c WHERE sasa_vehiculos_cstm.sas_cd_line_vehi_c='{$bean->sasa_cod_linea_c}' AND sasa_vehiculos.deleted=0");

								$sasa_cod_lineavehiculo =  $GLOBALS['db']->fetchByAssoc($resultpuntovehiculo);
								$Case->sasa_vehiculos_cases_1sasa_vehiculos_ida = $sasa_cod_lineavehiculo['id'];
							}
							$Case->save();
							$bean->assigned_user_id = $User->id;
						}
		            }
		        }else{
		            //existing record
		        }
			}
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Notas: ".$e->getMessage()); 
		}
	}
}
?>
<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SASA_MeetingsAftersave
{
	function after_save($bean, $event, $arguments)
	{	
		/*
		Logic Hook para poblar automaticamente la relacion entre caso y reunión a traves del campo sasa_casorelacion_c
		*/
		try{
			if (!isset($bean->aftersavemeetings) || $bean->aftersavemeetings === false){//antiloop
				$bean->aftersavemeetings = true;//antiloop	
				if ($bean->parent_type == 'Cases') {
					if (!empty($bean->sasa_casorelacion_c)){
						$resultcaso = $GLOBALS['db']->query("SELECT id_c FROM cases_cstm INNER JOIN cases ON cases_cstm.id_c=cases.id WHERE nu_pqrf_c='{$bean->sasa_casorelacion_c}' AND deleted=0");
						$sasa_id_case =  $GLOBALS['db']->fetchByAssoc($resultcaso);
						$bean->parent_id = $sasa_id_case;
					}
				}
				if (!empty($bean->sasa_cod_taller_c)){
					$resultpunto = $GLOBALS['db']->query("SELECT id FROM sasa_puntos_de_ventas INNER JOIN sasa_puntos_de_ventas_cstm ON sasa_puntos_de_ventas.id=sasa_puntos_de_ventas_cstm.id_c WHERE sasa_puntos_de_ventas_cstm.sasa_codpuntoatencion_c='{$bean->sasa_cod_taller_c}' AND sasa_puntos_de_ventas.deleted=0");

					$sasa_cod_puntoAtencion =  $GLOBALS['db']->fetchByAssoc($resultpunto);
					$bean->sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida = $sasa_cod_puntoAtencion['id'];
				}

				if (strpos($bean->name, "Cita") !== false) {
					if ($bean->parent_type == 'Accounts') {
						$query = $GLOBALS['db']->query("SELECT id, assigned_user_id FROM cases WHERE cases.account_id='{$bean->parent_id}' AND (name LIKE '%SOLICITUD%' AND name LIKE '%CITA%') ORDER BY cases.date_entered DESC");
						$asignado =  $GLOBALS['db']->fetchByAssoc($query);
						$bean->assigned_user_id = $asignado['assigned_user_id'];
					}
				}

				$bean->save();
			}
		}
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: LogicHook Reuniones: ".$e->getMessage()); 
		}
	}
}
?>
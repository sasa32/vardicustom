<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_CN.sugar_moduleList.php

 //created: 2020-01-31 00:19:11

$app_list_strings['moduleList']['RevenueLineItems']='营收单项';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_CN.sugar_parent_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => '帐户',
  'Contacts' => '联系人',
  'Tasks' => '任务',
  'Opportunities' => '商业机会',
  'Products' => '已报价单项',
  'Quotes' => '报价',
  'Bugs' => '缺陷',
  'Cases' => '客户反馈',
  'Leads' => '潜在客户',
  'Project' => '项目',
  'ProjectTask' => '项目任务',
  'Prospects' => '目标',
  'KBContents' => '知识库',
  'Notes' => '笔记',
  'RevenueLineItems' => '营收单项',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_CN.sugar_record_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => '帐户',
  'Opportunities' => '商业机会',
  'Cases' => '客户反馈',
  'Leads' => '潜在客户',
  'Contacts' => '联系人',
  'Products' => '已报价单项',
  'Quotes' => '报价',
  'Bugs' => '错误',
  'Project' => '项目',
  'Prospects' => '目标',
  'ProjectTask' => '项目任务',
  'Tasks' => '任务',
  'KBContents' => '知识库',
  'Notes' => '笔记',
  'RevenueLineItems' => '营收单项',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_CN.sugar_record_type_display_notes.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => '帐户',
  'Contacts' => '联系人',
  'Opportunities' => '商业机会',
  'Tasks' => '任务',
  'ProductTemplates' => '产品目录',
  'Quotes' => '报价',
  'Products' => '已报价单项',
  'Contracts' => '合同',
  'Emails' => '电子邮件',
  'Bugs' => '错误',
  'Project' => '项目',
  'ProjectTask' => '项目任务',
  'Prospects' => '目标',
  'Cases' => '客户反馈',
  'Leads' => '潜在客户',
  'Meetings' => '会议',
  'Calls' => '电话',
  'KBContents' => '知识库',
  'RevenueLineItems' => '营收单项',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_CN.sugar_moduleIconList.php

// created: 2023-02-08 11:06:13
$app_list_strings['moduleIconList']['Home'] = '首页';
$app_list_strings['moduleIconList']['Contacts'] = '联系';
$app_list_strings['moduleIconList']['Accounts'] = '帐户';
$app_list_strings['moduleIconList']['Opportunities'] = '商业';
$app_list_strings['moduleIconList']['Cases'] = '客户';
$app_list_strings['moduleIconList']['Notes'] = '笔记';
$app_list_strings['moduleIconList']['Calls'] = '电话';
$app_list_strings['moduleIconList']['Emails'] = '电子';
$app_list_strings['moduleIconList']['Meetings'] = '会议';
$app_list_strings['moduleIconList']['Tasks'] = '任务';
$app_list_strings['moduleIconList']['Calendar'] = '日程';
$app_list_strings['moduleIconList']['Leads'] = '潜在';
$app_list_strings['moduleIconList']['Currencies'] = '货币';
$app_list_strings['moduleIconList']['Contracts'] = '合同';
$app_list_strings['moduleIconList']['Quotes'] = '报价';
$app_list_strings['moduleIconList']['Products'] = '已报';
$app_list_strings['moduleIconList']['WebLogicHooks'] = '网络';
$app_list_strings['moduleIconList']['ProductCategories'] = '产品';
$app_list_strings['moduleIconList']['ProductTypes'] = '产品';
$app_list_strings['moduleIconList']['ProductTemplates'] = '产品';
$app_list_strings['moduleIconList']['ProductBundles'] = '产品';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = '产品';
$app_list_strings['moduleIconList']['Reports'] = '报表';
$app_list_strings['moduleIconList']['Forecasts'] = '预测';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = '预测';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = '预测';
$app_list_strings['moduleIconList']['Quotas'] = '定额';
$app_list_strings['moduleIconList']['VisualPipeline'] = '可视';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = '控制';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = '团队';
$app_list_strings['moduleIconList']['TeamNotices'] = '团队';
$app_list_strings['moduleIconList']['Manufacturers'] = '制造';
$app_list_strings['moduleIconList']['Activities'] = '活动';
$app_list_strings['moduleIconList']['Comments'] = '备注';
$app_list_strings['moduleIconList']['Subscriptions'] = '订阅';
$app_list_strings['moduleIconList']['Bugs'] = '缺陷';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = '我的';
$app_list_strings['moduleIconList']['TimePeriods'] = '时段';
$app_list_strings['moduleIconList']['TaxRates'] = '税率';
$app_list_strings['moduleIconList']['ContractTypes'] = '合同';
$app_list_strings['moduleIconList']['Schedulers'] = '计划';
$app_list_strings['moduleIconList']['Project'] = '项目';
$app_list_strings['moduleIconList']['ProjectTask'] = '项目';
$app_list_strings['moduleIconList']['Campaigns'] = '市场';
$app_list_strings['moduleIconList']['CampaignLog'] = '市场';
$app_list_strings['moduleIconList']['CampaignTrackers'] = '市场';
$app_list_strings['moduleIconList']['Documents'] = '文档';
$app_list_strings['moduleIconList']['DocumentRevisions'] = '文档';
$app_list_strings['moduleIconList']['Connectors'] = '连接';
$app_list_strings['moduleIconList']['Notifications'] = '通知';
$app_list_strings['moduleIconList']['Sync'] = '同步';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = '外部';
$app_list_strings['moduleIconList']['ReportMaker'] = '高级';
$app_list_strings['moduleIconList']['DataSets'] = '数据';
$app_list_strings['moduleIconList']['CustomQueries'] = '自定';
$app_list_strings['moduleIconList']['pmse_Inbox'] = '流程';
$app_list_strings['moduleIconList']['pmse_Project'] = '流程';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = '流程';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = '流程';
$app_list_strings['moduleIconList']['BusinessCenters'] = '业务';
$app_list_strings['moduleIconList']['Shifts'] = '班次';
$app_list_strings['moduleIconList']['ShiftExceptions'] = '班次';
$app_list_strings['moduleIconList']['Purchases'] = '购买';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = '已购';
$app_list_strings['moduleIconList']['MobileDevices'] = '移动';
$app_list_strings['moduleIconList']['PushNotifications'] = '推送';
$app_list_strings['moduleIconList']['Escalations'] = '升级';
$app_list_strings['moduleIconList']['DocumentTemplates'] = '文档';
$app_list_strings['moduleIconList']['DocumentMerges'] = '文档';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = '云驱';
$app_list_strings['moduleIconList']['WorkFlow'] = '工作';
$app_list_strings['moduleIconList']['EAPM'] = '外部';
$app_list_strings['moduleIconList']['Worksheet'] = '工作';
$app_list_strings['moduleIconList']['Users'] = '用户';
$app_list_strings['moduleIconList']['Employees'] = '员工';
$app_list_strings['moduleIconList']['Administration'] = '管理';
$app_list_strings['moduleIconList']['ACLRoles'] = '角色';
$app_list_strings['moduleIconList']['InboundEmail'] = '入站';
$app_list_strings['moduleIconList']['Releases'] = '发布';
$app_list_strings['moduleIconList']['Prospects'] = '目标';
$app_list_strings['moduleIconList']['Queues'] = '队列';
$app_list_strings['moduleIconList']['EmailMarketing'] = '电子';
$app_list_strings['moduleIconList']['EmailTemplates'] = '电子';
$app_list_strings['moduleIconList']['SNIP'] = '电子';
$app_list_strings['moduleIconList']['ProspectLists'] = '目标';
$app_list_strings['moduleIconList']['SavedSearch'] = '保存';
$app_list_strings['moduleIconList']['UpgradeWizard'] = '升级';
$app_list_strings['moduleIconList']['Trackers'] = '跟踪';
$app_list_strings['moduleIconList']['TrackerPerfs'] = '跟踪';
$app_list_strings['moduleIconList']['TrackerSessions'] = '跟踪';
$app_list_strings['moduleIconList']['TrackerQueries'] = '跟踪';
$app_list_strings['moduleIconList']['FAQ'] = '常见';
$app_list_strings['moduleIconList']['Newsletters'] = '新闻';
$app_list_strings['moduleIconList']['SugarFavorites'] = '收藏';
$app_list_strings['moduleIconList']['PdfManager'] = 'P经';
$app_list_strings['moduleIconList']['DataArchiver'] = '数据';
$app_list_strings['moduleIconList']['ArchiveRuns'] = '归档';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'O用';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'O令';
$app_list_strings['moduleIconList']['Filters'] = '过滤';
$app_list_strings['moduleIconList']['UserSignatures'] = '电子';
$app_list_strings['moduleIconList']['Shippers'] = '运输';
$app_list_strings['moduleIconList']['Styleguide'] = '设计';
$app_list_strings['moduleIconList']['Feedbacks'] = '反馈';
$app_list_strings['moduleIconList']['Tags'] = '标签';
$app_list_strings['moduleIconList']['Categories'] = '类别';
$app_list_strings['moduleIconList']['Dashboards'] = '仪表';
$app_list_strings['moduleIconList']['OutboundEmail'] = '电子';
$app_list_strings['moduleIconList']['EmailParticipants'] = '电子';
$app_list_strings['moduleIconList']['DataPrivacy'] = '数据';
$app_list_strings['moduleIconList']['ReportSchedules'] = '报告';
$app_list_strings['moduleIconList']['CommentLog'] = '评论';
$app_list_strings['moduleIconList']['Holidays'] = '假期';
$app_list_strings['moduleIconList']['ChangeTimers'] = '更改';
$app_list_strings['moduleIconList']['Metrics'] = '量度';
$app_list_strings['moduleIconList']['Messages'] = '消息';
$app_list_strings['moduleIconList']['Audit'] = '审计';
$app_list_strings['moduleIconList']['RevenueLineItems'] = '营收';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'D信';
$app_list_strings['moduleIconList']['Geocode'] = '地理';
$app_list_strings['moduleIconList']['DRI_Workflows'] = '智能';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = '智能';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = '智能';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = '智能';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'SA';
$app_list_strings['moduleIconList']['CJ_Forms'] = '智S';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = '智能';
$app_list_strings['moduleIconList']['Library'] = '库';
$app_list_strings['moduleIconList']['EmailAddresses'] = '电子';
$app_list_strings['moduleIconList']['Words'] = '文字';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = '收藏';
$app_list_strings['moduleIconList']['KBDocuments'] = '知识';
$app_list_strings['moduleIconList']['KBContents'] = '知识';
$app_list_strings['moduleIconList']['KBArticles'] = '知识';
$app_list_strings['moduleIconList']['KBContentTemplates'] = '知识';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = '嵌入';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';

?>

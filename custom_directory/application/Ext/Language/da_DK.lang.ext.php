<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.sugar_moduleList.php

 //created: 2020-01-31 00:19:10

$app_list_strings['moduleList']['RevenueLineItems']='Revenue detaljposter';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.sugar_parent_type_display.php

 // created: 2020-01-31 00:19:10

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Virksomhed',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Opgave',
  'Opportunities' => 'Salgsmulighed',
  'Products' => 'Angiven linjepost',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Fejl',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektopgave',
  'Prospects' => 'Mål:',
  'KBContents' => 'Videnbase',
  'Notes' => 'Note',
  'RevenueLineItems' => 'Revenue detaljposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.sugar_record_type_display.php

 // created: 2020-01-31 00:19:10

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Virksomhed',
  'Opportunities' => 'Salgsmulighed',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Contacts' => 'Kontakter',
  'Products' => 'Angiven linjepost',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Fejl',
  'Project' => 'Projekt',
  'Prospects' => 'Mål:',
  'ProjectTask' => 'Projektopgave',
  'Tasks' => 'Opgave',
  'KBContents' => 'Videnbase',
  'Notes' => 'Note',
  'RevenueLineItems' => 'Revenue detaljposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.sugar_record_type_display_notes.php

 // created: 2020-01-31 00:19:10

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Virksomhed',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Salgsmulighed',
  'Tasks' => 'Opgave',
  'ProductTemplates' => 'Produktkatalog',
  'Quotes' => 'Tilbud',
  'Products' => 'Angiven linjepost',
  'Contracts' => 'Kontrakt',
  'Emails' => 'E-mail',
  'Bugs' => 'Fejl',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektopgave',
  'Prospects' => 'Mål:',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Meetings' => 'Møde',
  'Calls' => 'Opkald',
  'KBContents' => 'Videnbase',
  'RevenueLineItems' => 'Revenue detaljposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.sugar_moduleIconList.php

// created: 2023-02-08 11:06:12
$app_list_strings['moduleIconList']['Home'] = 'St';
$app_list_strings['moduleIconList']['Contacts'] = 'Ko';
$app_list_strings['moduleIconList']['Accounts'] = 'Ko';
$app_list_strings['moduleIconList']['Opportunities'] = 'Sa';
$app_list_strings['moduleIconList']['Cases'] = 'Sa';
$app_list_strings['moduleIconList']['Notes'] = 'No';
$app_list_strings['moduleIconList']['Calls'] = 'Op';
$app_list_strings['moduleIconList']['Emails'] = 'E-';
$app_list_strings['moduleIconList']['Meetings'] = 'Mø';
$app_list_strings['moduleIconList']['Tasks'] = 'Op';
$app_list_strings['moduleIconList']['Calendar'] = 'Ka';
$app_list_strings['moduleIconList']['Leads'] = 'Ku';
$app_list_strings['moduleIconList']['Currencies'] = 'Va';
$app_list_strings['moduleIconList']['Contracts'] = 'Ko';
$app_list_strings['moduleIconList']['Quotes'] = 'Ti';
$app_list_strings['moduleIconList']['Products'] = 'Pr';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'WL';
$app_list_strings['moduleIconList']['ProductCategories'] = 'Pr';
$app_list_strings['moduleIconList']['ProductTypes'] = 'Pr';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'Pr';
$app_list_strings['moduleIconList']['ProductBundles'] = 'Pr';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'Bt';
$app_list_strings['moduleIconList']['Reports'] = 'Ra';
$app_list_strings['moduleIconList']['Forecasts'] = 'Pr';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'Pr';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'PM';
$app_list_strings['moduleIconList']['Quotas'] = 'Kv';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'Vk';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'Ko';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = 'Te';
$app_list_strings['moduleIconList']['TeamNotices'] = 'Te';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Pr';
$app_list_strings['moduleIconList']['Activities'] = 'Ak';
$app_list_strings['moduleIconList']['Comments'] = 'Ko';
$app_list_strings['moduleIconList']['Subscriptions'] = 'Ab';
$app_list_strings['moduleIconList']['Bugs'] = 'Fe';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'Mw';
$app_list_strings['moduleIconList']['TimePeriods'] = 'Ti';
$app_list_strings['moduleIconList']['TaxRates'] = 'Mo';
$app_list_strings['moduleIconList']['ContractTypes'] = 'Ko';
$app_list_strings['moduleIconList']['Schedulers'] = 'Pl';
$app_list_strings['moduleIconList']['Project'] = 'Pr';
$app_list_strings['moduleIconList']['ProjectTask'] = 'Pr';
$app_list_strings['moduleIconList']['Campaigns'] = 'Ka';
$app_list_strings['moduleIconList']['CampaignLog'] = 'Ka';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'Ka';
$app_list_strings['moduleIconList']['Documents'] = 'Do';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'Do';
$app_list_strings['moduleIconList']['Connectors'] = 'Fo';
$app_list_strings['moduleIconList']['Notifications'] = 'Me';
$app_list_strings['moduleIconList']['Sync'] = 'Sy';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'Eb';
$app_list_strings['moduleIconList']['ReportMaker'] = 'Vi';
$app_list_strings['moduleIconList']['DataSets'] = 'Da';
$app_list_strings['moduleIconList']['CustomQueries'] = 'Bf';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Pr';
$app_list_strings['moduleIconList']['pmse_Project'] = 'Pr';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'Pf';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'Pe';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'Fo';
$app_list_strings['moduleIconList']['Shifts'] = 'Sh';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'Sh';
$app_list_strings['moduleIconList']['Purchases'] = 'Kø';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'Kl';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Mo';
$app_list_strings['moduleIconList']['PushNotifications'] = 'Pu';
$app_list_strings['moduleIconList']['Escalations'] = 'Es';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'Do';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'Do';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'CD';
$app_list_strings['moduleIconList']['WorkFlow'] = 'Ar';
$app_list_strings['moduleIconList']['EAPM'] = 'Ek';
$app_list_strings['moduleIconList']['Worksheet'] = 'Re';
$app_list_strings['moduleIconList']['Users'] = 'Br';
$app_list_strings['moduleIconList']['Employees'] = 'Me';
$app_list_strings['moduleIconList']['Administration'] = 'Ad';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Ro';
$app_list_strings['moduleIconList']['InboundEmail'] = 'Ie';
$app_list_strings['moduleIconList']['Releases'] = 'Ud';
$app_list_strings['moduleIconList']['Prospects'] = 'Må';
$app_list_strings['moduleIconList']['Queues'] = 'Kø';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'E-';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'E-';
$app_list_strings['moduleIconList']['SNIP'] = 'Ea';
$app_list_strings['moduleIconList']['ProspectLists'] = 'Må';
$app_list_strings['moduleIconList']['SavedSearch'] = 'Gs';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'GO';
$app_list_strings['moduleIconList']['Trackers'] = 'Sp';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'Sp';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'Sp';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'Sp';
$app_list_strings['moduleIconList']['FAQ'] = 'Os';
$app_list_strings['moduleIconList']['Newsletters'] = 'Ny';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Fa';
$app_list_strings['moduleIconList']['PdfManager'] = 'Pd';
$app_list_strings['moduleIconList']['DataArchiver'] = 'Da';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'Ar';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'Of';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'OT';
$app_list_strings['moduleIconList']['Filters'] = 'Fi';
$app_list_strings['moduleIconList']['UserSignatures'] = 'E-';
$app_list_strings['moduleIconList']['Shippers'] = 'Sp';
$app_list_strings['moduleIconList']['Styleguide'] = 'St';
$app_list_strings['moduleIconList']['Feedbacks'] = 'Fe';
$app_list_strings['moduleIconList']['Tags'] = 'Ta';
$app_list_strings['moduleIconList']['Categories'] = 'Ka';
$app_list_strings['moduleIconList']['Dashboards'] = 'Be';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'E-';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'E-';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'Ba';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'Po';
$app_list_strings['moduleIconList']['CommentLog'] = 'Ko';
$app_list_strings['moduleIconList']['Holidays'] = 'Fe';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'St';
$app_list_strings['moduleIconList']['Metrics'] = 'Ef';
$app_list_strings['moduleIconList']['Messages'] = 'Me';
$app_list_strings['moduleIconList']['Audit'] = 'Re';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'Rd';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'Do';
$app_list_strings['moduleIconList']['Geocode'] = 'Ge';
$app_list_strings['moduleIconList']['DRI_Workflows'] = 'SG';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = 'SG';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = 'SG';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = 'SG';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'SA';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'SG';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = 'SG';
$app_list_strings['moduleIconList']['Library'] = 'Bi';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'E-';
$app_list_strings['moduleIconList']['Words'] = 'Or';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Fa';
$app_list_strings['moduleIconList']['KBDocuments'] = 'Vi';
$app_list_strings['moduleIconList']['KBContents'] = 'Vi';
$app_list_strings['moduleIconList']['KBArticles'] = 'Vi';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'Sf';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'If';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';

?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_TW.sugar_moduleList.php

 //created: 2020-01-31 00:19:11

$app_list_strings['moduleList']['RevenueLineItems']='收入項目';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_TW.sugar_parent_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => '帳戶',
  'Contacts' => '連絡人',
  'Tasks' => '工作',
  'Opportunities' => '商機',
  'Products' => '報價項目',
  'Quotes' => '報價',
  'Bugs' => '錯誤',
  'Cases' => '實例',
  'Leads' => '潛在客戶',
  'Project' => '專案',
  'ProjectTask' => '專案工作',
  'Prospects' => '目標',
  'KBContents' => '知識庫',
  'Notes' => '附註',
  'RevenueLineItems' => '收入項目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_TW.sugar_record_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => '帳戶',
  'Opportunities' => '商機',
  'Cases' => '實例',
  'Leads' => '潛在客戶',
  'Contacts' => '連絡人',
  'Products' => '報價項目',
  'Quotes' => '報價',
  'Bugs' => '錯誤',
  'Project' => '專案',
  'Prospects' => '目標',
  'ProjectTask' => '專案工作',
  'Tasks' => '工作',
  'KBContents' => '知識庫',
  'Notes' => '附註',
  'RevenueLineItems' => '收入項目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_TW.sugar_record_type_display_notes.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => '帳戶',
  'Contacts' => '連絡人',
  'Opportunities' => '商機',
  'Tasks' => '工作',
  'ProductTemplates' => '產品目錄',
  'Quotes' => '報價',
  'Products' => '報價項目',
  'Contracts' => '合約',
  'Emails' => '電子郵件',
  'Bugs' => '錯誤',
  'Project' => '專案',
  'ProjectTask' => '專案工作',
  'Prospects' => '目標',
  'Cases' => '實例',
  'Leads' => '潛在客戶',
  'Meetings' => '會議',
  'Calls' => '通話',
  'KBContents' => '知識庫',
  'RevenueLineItems' => '收入項目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_TW.sugar_moduleIconList.php

// created: 2023-02-08 11:06:13
$app_list_strings['moduleIconList']['Home'] = '首頁';
$app_list_strings['moduleIconList']['Contacts'] = '連絡';
$app_list_strings['moduleIconList']['Accounts'] = '帳戶';
$app_list_strings['moduleIconList']['Opportunities'] = '商機';
$app_list_strings['moduleIconList']['Cases'] = '案件';
$app_list_strings['moduleIconList']['Notes'] = '筆記';
$app_list_strings['moduleIconList']['Calls'] = '通話';
$app_list_strings['moduleIconList']['Emails'] = '電子';
$app_list_strings['moduleIconList']['Meetings'] = '會議';
$app_list_strings['moduleIconList']['Tasks'] = '工作';
$app_list_strings['moduleIconList']['Calendar'] = '行事';
$app_list_strings['moduleIconList']['Leads'] = '潛在';
$app_list_strings['moduleIconList']['Currencies'] = '貨幣';
$app_list_strings['moduleIconList']['Contracts'] = '合約';
$app_list_strings['moduleIconList']['Quotes'] = '報價';
$app_list_strings['moduleIconList']['Products'] = '報價';
$app_list_strings['moduleIconList']['WebLogicHooks'] = '網路';
$app_list_strings['moduleIconList']['ProductCategories'] = '產品';
$app_list_strings['moduleIconList']['ProductTypes'] = '產品';
$app_list_strings['moduleIconList']['ProductTemplates'] = '產品';
$app_list_strings['moduleIconList']['ProductBundles'] = '產品';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = '產品';
$app_list_strings['moduleIconList']['Reports'] = '報表';
$app_list_strings['moduleIconList']['Forecasts'] = '預測';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = '預測';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = '預測';
$app_list_strings['moduleIconList']['Quotas'] = '配額';
$app_list_strings['moduleIconList']['VisualPipeline'] = '視覺';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = '控制';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = '小組';
$app_list_strings['moduleIconList']['TeamNotices'] = '小組';
$app_list_strings['moduleIconList']['Manufacturers'] = '製造';
$app_list_strings['moduleIconList']['Activities'] = '活動';
$app_list_strings['moduleIconList']['Comments'] = '註解';
$app_list_strings['moduleIconList']['Subscriptions'] = '訂閱';
$app_list_strings['moduleIconList']['Bugs'] = '錯誤';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = '我的';
$app_list_strings['moduleIconList']['TimePeriods'] = '時間';
$app_list_strings['moduleIconList']['TaxRates'] = '稅率';
$app_list_strings['moduleIconList']['ContractTypes'] = '合約';
$app_list_strings['moduleIconList']['Schedulers'] = '排程';
$app_list_strings['moduleIconList']['Project'] = '專案';
$app_list_strings['moduleIconList']['ProjectTask'] = '專案';
$app_list_strings['moduleIconList']['Campaigns'] = '推廣';
$app_list_strings['moduleIconList']['CampaignLog'] = '推廣';
$app_list_strings['moduleIconList']['CampaignTrackers'] = '推廣';
$app_list_strings['moduleIconList']['Documents'] = '文件';
$app_list_strings['moduleIconList']['DocumentRevisions'] = '文件';
$app_list_strings['moduleIconList']['Connectors'] = '連接';
$app_list_strings['moduleIconList']['Notifications'] = '通知';
$app_list_strings['moduleIconList']['Sync'] = '同步';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = '外部';
$app_list_strings['moduleIconList']['ReportMaker'] = '進階';
$app_list_strings['moduleIconList']['DataSets'] = '資料';
$app_list_strings['moduleIconList']['CustomQueries'] = '自訂';
$app_list_strings['moduleIconList']['pmse_Inbox'] = '流程';
$app_list_strings['moduleIconList']['pmse_Project'] = '流程';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = '流程';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = '流程';
$app_list_strings['moduleIconList']['BusinessCenters'] = '商務';
$app_list_strings['moduleIconList']['Shifts'] = '班表';
$app_list_strings['moduleIconList']['ShiftExceptions'] = '班表';
$app_list_strings['moduleIconList']['Purchases'] = '購買';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = '購買';
$app_list_strings['moduleIconList']['MobileDevices'] = '移動';
$app_list_strings['moduleIconList']['PushNotifications'] = '推播';
$app_list_strings['moduleIconList']['Escalations'] = '升級';
$app_list_strings['moduleIconList']['DocumentTemplates'] = '文件';
$app_list_strings['moduleIconList']['DocumentMerges'] = '文檔';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = '雲端';
$app_list_strings['moduleIconList']['WorkFlow'] = '工作';
$app_list_strings['moduleIconList']['EAPM'] = '外部';
$app_list_strings['moduleIconList']['Worksheet'] = '工作';
$app_list_strings['moduleIconList']['Users'] = '使用';
$app_list_strings['moduleIconList']['Employees'] = '員工';
$app_list_strings['moduleIconList']['Administration'] = '管理';
$app_list_strings['moduleIconList']['ACLRoles'] = '角色';
$app_list_strings['moduleIconList']['InboundEmail'] = '輸入';
$app_list_strings['moduleIconList']['Releases'] = '發行';
$app_list_strings['moduleIconList']['Prospects'] = '目標';
$app_list_strings['moduleIconList']['Queues'] = '佇列';
$app_list_strings['moduleIconList']['EmailMarketing'] = '電子';
$app_list_strings['moduleIconList']['EmailTemplates'] = '電子';
$app_list_strings['moduleIconList']['SNIP'] = '電子';
$app_list_strings['moduleIconList']['ProspectLists'] = '目標';
$app_list_strings['moduleIconList']['SavedSearch'] = '已儲';
$app_list_strings['moduleIconList']['UpgradeWizard'] = '升級';
$app_list_strings['moduleIconList']['Trackers'] = '追蹤';
$app_list_strings['moduleIconList']['TrackerPerfs'] = '追蹤';
$app_list_strings['moduleIconList']['TrackerSessions'] = '追蹤';
$app_list_strings['moduleIconList']['TrackerQueries'] = '追蹤';
$app_list_strings['moduleIconList']['FAQ'] = '常見';
$app_list_strings['moduleIconList']['Newsletters'] = '新聞';
$app_list_strings['moduleIconList']['SugarFavorites'] = '最愛';
$app_list_strings['moduleIconList']['PdfManager'] = 'P管';
$app_list_strings['moduleIconList']['DataArchiver'] = '數據';
$app_list_strings['moduleIconList']['ArchiveRuns'] = '歸檔';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'O使';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'O權';
$app_list_strings['moduleIconList']['Filters'] = '篩選';
$app_list_strings['moduleIconList']['UserSignatures'] = '電子';
$app_list_strings['moduleIconList']['Shippers'] = '運輸';
$app_list_strings['moduleIconList']['Styleguide'] = '風格';
$app_list_strings['moduleIconList']['Feedbacks'] = '回饋';
$app_list_strings['moduleIconList']['Tags'] = '標籤';
$app_list_strings['moduleIconList']['Categories'] = '類別';
$app_list_strings['moduleIconList']['Dashboards'] = '儀表';
$app_list_strings['moduleIconList']['OutboundEmail'] = '電子';
$app_list_strings['moduleIconList']['EmailParticipants'] = '電子';
$app_list_strings['moduleIconList']['DataPrivacy'] = '數據';
$app_list_strings['moduleIconList']['ReportSchedules'] = '報表';
$app_list_strings['moduleIconList']['CommentLog'] = '評論';
$app_list_strings['moduleIconList']['Holidays'] = '假日';
$app_list_strings['moduleIconList']['ChangeTimers'] = '更改';
$app_list_strings['moduleIconList']['Metrics'] = '量度';
$app_list_strings['moduleIconList']['Messages'] = '訊息';
$app_list_strings['moduleIconList']['Audit'] = '稽核';
$app_list_strings['moduleIconList']['RevenueLineItems'] = '收入';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'D信';
$app_list_strings['moduleIconList']['Geocode'] = '地理';
$app_list_strings['moduleIconList']['DRI_Workflows'] = '智慧';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = '智慧';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = '智慧';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = '智慧';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'SA';
$app_list_strings['moduleIconList']['CJ_Forms'] = '智S';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = '智慧';
$app_list_strings['moduleIconList']['Library'] = '庫';
$app_list_strings['moduleIconList']['EmailAddresses'] = '電子';
$app_list_strings['moduleIconList']['Words'] = '字組';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = '最愛';
$app_list_strings['moduleIconList']['KBDocuments'] = '知識';
$app_list_strings['moduleIconList']['KBContents'] = '知識';
$app_list_strings['moduleIconList']['KBArticles'] = '知識';
$app_list_strings['moduleIconList']['KBContentTemplates'] = '知識';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = '已嵌';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';

?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.sugar_moduleList.php

 //created: 2020-01-31 00:19:11

$app_list_strings['moduleList']['Products']='Elements de línies d\'oferta';
$app_list_strings['moduleList']['TaxRates']='Tipus d\'impostos';
$app_list_strings['moduleList']['ProspectLists']='Llistes d\'objectius';
$app_list_strings['moduleList']['Styleguide']='Guia d\'estil';
$app_list_strings['moduleList']['ReportSchedules']='Programes d\'informes';
$app_list_strings['moduleList']['RevenueLineItems']='Elements de línia d\'ingressos';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.sugar_parent_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contacte',
  'Tasks' => 'Tasca',
  'Opportunities' => 'Oportunitat',
  'Products' => 'Element de línia d\'oferta',
  'Quotes' => 'Pressupost',
  'Bugs' => 'Incidències',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Project' => 'Projecte',
  'ProjectTask' => 'Tasca de projecte',
  'Prospects' => 'Objectiu',
  'KBContents' => 'Knowledge Base',
  'Notes' => 'Nota',
  'RevenueLineItems' => 'Elements de línia d\'ingressos',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.sugar_record_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Compte',
  'Opportunities' => 'Oportunitat',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Contacts' => 'Contactes',
  'Products' => 'Element de línia d\'oferta',
  'Quotes' => 'Pressupost',
  'Bugs' => 'Incidència',
  'Project' => 'Projecte',
  'Prospects' => 'Objectiu',
  'ProjectTask' => 'Tasca de projecte',
  'Tasks' => 'Tasca',
  'KBContents' => 'Knowledge Base',
  'Notes' => 'Nota',
  'RevenueLineItems' => 'Elements de línia d\'ingressos',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.sugar_record_type_display_notes.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contacte',
  'Opportunities' => 'Oportunitat',
  'Tasks' => 'Tasca',
  'ProductTemplates' => 'Catàleg de productes',
  'Quotes' => 'Pressupost',
  'Products' => 'Element de línia d\'oferta',
  'Contracts' => 'Contracte',
  'Emails' => 'Adreça electrònica',
  'Bugs' => 'Incidència',
  'Project' => 'Projecte',
  'ProjectTask' => 'Tasca de projecte',
  'Prospects' => 'Objectiu',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Meetings' => 'Reunió',
  'Calls' => 'Trucada',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Elements de línia d\'ingressos',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.sugar_moduleIconList.php

// created: 2023-02-08 11:06:14
$app_list_strings['moduleIconList']['Home'] = 'In';
$app_list_strings['moduleIconList']['Contacts'] = 'Co';
$app_list_strings['moduleIconList']['Accounts'] = 'Co';
$app_list_strings['moduleIconList']['Opportunities'] = 'Op';
$app_list_strings['moduleIconList']['Cases'] = 'Ca';
$app_list_strings['moduleIconList']['Notes'] = 'No';
$app_list_strings['moduleIconList']['Calls'] = 'Tr';
$app_list_strings['moduleIconList']['Emails'] = 'Md';
$app_list_strings['moduleIconList']['Meetings'] = 'Re';
$app_list_strings['moduleIconList']['Tasks'] = 'Ta';
$app_list_strings['moduleIconList']['Calendar'] = 'Ca';
$app_list_strings['moduleIconList']['Leads'] = 'Cp';
$app_list_strings['moduleIconList']['Currencies'] = 'Mo';
$app_list_strings['moduleIconList']['Contracts'] = 'Co';
$app_list_strings['moduleIconList']['Quotes'] = 'Pr';
$app_list_strings['moduleIconList']['Products'] = 'Ed';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'WL';
$app_list_strings['moduleIconList']['ProductCategories'] = 'Cd';
$app_list_strings['moduleIconList']['ProductTypes'] = 'Td';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'Cd';
$app_list_strings['moduleIconList']['ProductBundles'] = 'Jd';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'Nd';
$app_list_strings['moduleIconList']['Reports'] = 'In';
$app_list_strings['moduleIconList']['Forecasts'] = 'Pr';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'Fd';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'Fd';
$app_list_strings['moduleIconList']['Quotas'] = 'Qu';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'Ov';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'Cd';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = 'Eq';
$app_list_strings['moduleIconList']['TeamNotices'] = 'Nd';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Pr';
$app_list_strings['moduleIconList']['Activities'] = 'Ac';
$app_list_strings['moduleIconList']['Comments'] = 'Co';
$app_list_strings['moduleIconList']['Subscriptions'] = 'Su';
$app_list_strings['moduleIconList']['Bugs'] = 'In';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'Em';
$app_list_strings['moduleIconList']['TimePeriods'] = 'Pd';
$app_list_strings['moduleIconList']['TaxRates'] = 'Td';
$app_list_strings['moduleIconList']['ContractTypes'] = 'Td';
$app_list_strings['moduleIconList']['Schedulers'] = 'Pl';
$app_list_strings['moduleIconList']['Project'] = 'Pr';
$app_list_strings['moduleIconList']['ProjectTask'] = 'Td';
$app_list_strings['moduleIconList']['Campaigns'] = 'Ca';
$app_list_strings['moduleIconList']['CampaignLog'] = 'Rd';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'Sd';
$app_list_strings['moduleIconList']['Documents'] = 'Do';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'Ve';
$app_list_strings['moduleIconList']['Connectors'] = 'Co';
$app_list_strings['moduleIconList']['Notifications'] = 'No';
$app_list_strings['moduleIconList']['Sync'] = 'Si';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'Ue';
$app_list_strings['moduleIconList']['ReportMaker'] = 'Ia';
$app_list_strings['moduleIconList']['DataSets'] = 'Fd';
$app_list_strings['moduleIconList']['CustomQueries'] = 'Cp';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Pr';
$app_list_strings['moduleIconList']['pmse_Project'] = 'Dd';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'Ne';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'Cd';
$app_list_strings['moduleIconList']['Shifts'] = 'To';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'Ed';
$app_list_strings['moduleIconList']['Purchases'] = 'Co';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'Ed';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Di';
$app_list_strings['moduleIconList']['PushNotifications'] = 'No';
$app_list_strings['moduleIconList']['Escalations'] = 'Es';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'Pd';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'Fd';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'Rd';
$app_list_strings['moduleIconList']['WorkFlow'] = 'Dd';
$app_list_strings['moduleIconList']['EAPM'] = 'Ce';
$app_list_strings['moduleIconList']['Worksheet'] = 'Fd';
$app_list_strings['moduleIconList']['Users'] = 'Us';
$app_list_strings['moduleIconList']['Employees'] = 'Em';
$app_list_strings['moduleIconList']['Administration'] = 'Ad';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Ro';
$app_list_strings['moduleIconList']['InboundEmail'] = 'Ce';
$app_list_strings['moduleIconList']['Releases'] = 'Pu';
$app_list_strings['moduleIconList']['Prospects'] = 'Ob';
$app_list_strings['moduleIconList']['Queues'] = 'Cu';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'Mp';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'Pd';
$app_list_strings['moduleIconList']['SNIP'] = 'Ad';
$app_list_strings['moduleIconList']['ProspectLists'] = 'Ld';
$app_list_strings['moduleIconList']['SavedSearch'] = 'Cd';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'Ad';
$app_list_strings['moduleIconList']['Trackers'] = 'Mo';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'Rd';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'Sd';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'Cd';
$app_list_strings['moduleIconList']['FAQ'] = 'PF';
$app_list_strings['moduleIconList']['Newsletters'] = 'Bd';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Fa';
$app_list_strings['moduleIconList']['PdfManager'] = 'Gd';
$app_list_strings['moduleIconList']['DataArchiver'] = 'Ad';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'Ed';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'Cd';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'OT';
$app_list_strings['moduleIconList']['Filters'] = 'Fi';
$app_list_strings['moduleIconList']['UserSignatures'] = 'Fd';
$app_list_strings['moduleIconList']['Shippers'] = 'Pd';
$app_list_strings['moduleIconList']['Styleguide'] = 'Gd';
$app_list_strings['moduleIconList']['Feedbacks'] = 'Co';
$app_list_strings['moduleIconList']['Tags'] = 'Et';
$app_list_strings['moduleIconList']['Categories'] = 'Ca';
$app_list_strings['moduleIconList']['Dashboards'] = 'Qd';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'Cd';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'Pd';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'Pd';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'Pd';
$app_list_strings['moduleIconList']['CommentLog'] = 'Rd';
$app_list_strings['moduleIconList']['Holidays'] = 'Fe';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'Ce';
$app_list_strings['moduleIconList']['Metrics'] = 'Mè';
$app_list_strings['moduleIconList']['Messages'] = 'Mi';
$app_list_strings['moduleIconList']['Audit'] = 'Au';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'Ed';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'Sd';
$app_list_strings['moduleIconList']['Geocode'] = 'Ge';
$app_list_strings['moduleIconList']['DRI_Workflows'] = 'SG';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = 'Fd';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'WH';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'Ad';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['Library'] = 'Bi';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'Ae';
$app_list_strings['moduleIconList']['Words'] = 'Pa';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Fa';
$app_list_strings['moduleIconList']['KBDocuments'] = 'Dd';
$app_list_strings['moduleIconList']['KBContents'] = 'KB';
$app_list_strings['moduleIconList']['KBArticles'] = 'Ad';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'Pd';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'Fi';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';

?>

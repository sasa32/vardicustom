<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/lt_LT.sugar_moduleList.php

 //created: 2020-01-31 00:19:10

$app_list_strings['moduleList']['RevenueLineItems']='Pajamų eilutės prekės';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/lt_LT.sugar_parent_type_display.php

 // created: 2020-01-31 00:19:10

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Sąskaita',
  'Contacts' => 'Kontaktas',
  'Tasks' => 'Užduotis',
  'Opportunities' => 'Galimybė',
  'Products' => 'Įvykdyto pasiūlymo eilutės prekė',
  'Quotes' => 'Pasiūlymas',
  'Bugs' => 'Triktys',
  'Cases' => 'Atvejis',
  'Leads' => 'Galimas klientas',
  'Project' => 'Projektas',
  'ProjectTask' => 'Projekto užduotis',
  'Prospects' => 'Adresatas',
  'KBContents' => 'Žinių bazė',
  'Notes' => 'Pastaba',
  'RevenueLineItems' => 'Pajamų eilutės prekės',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/lt_LT.sugar_record_type_display.php

 // created: 2020-01-31 00:19:10

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Sąskaita',
  'Opportunities' => 'Galimybė',
  'Cases' => 'Atvejis',
  'Leads' => 'Galimas klientas',
  'Contacts' => 'Kontaktai',
  'Products' => 'Įvykdyto pasiūlymo eilutės prekė',
  'Quotes' => 'Pasiūlymas',
  'Bugs' => 'Triktis',
  'Project' => 'Projektas',
  'Prospects' => 'Adresatas',
  'ProjectTask' => 'Projekto užduotis',
  'Tasks' => 'Užduotis',
  'KBContents' => 'Žinių bazė',
  'Notes' => 'Pastaba',
  'RevenueLineItems' => 'Pajamų eilutės prekės',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/lt_LT.sugar_record_type_display_notes.php

 // created: 2020-01-31 00:19:10

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Sąskaita',
  'Contacts' => 'Kontaktas',
  'Opportunities' => 'Galimybė',
  'Tasks' => 'Užduotis',
  'ProductTemplates' => 'Produktų katalogas',
  'Quotes' => 'Pasiūlymas',
  'Products' => 'Įvykdyto pasiūlymo eilutės prekė',
  'Contracts' => 'Sutartis',
  'Emails' => 'El. paštas',
  'Bugs' => 'Triktis',
  'Project' => 'Projektas',
  'ProjectTask' => 'Projekto užduotis',
  'Prospects' => 'Adresatas',
  'Cases' => 'Atvejis',
  'Leads' => 'Galimas klientas',
  'Meetings' => 'Susitikimas',
  'Calls' => 'Skambutis',
  'KBContents' => 'Žinių bazė',
  'RevenueLineItems' => 'Pajamų eilutės prekės',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/lt_LT.sugar_moduleIconList.php

// created: 2023-02-08 11:06:13
$app_list_strings['moduleIconList']['Home'] = 'Pr';
$app_list_strings['moduleIconList']['Contacts'] = 'Ko';
$app_list_strings['moduleIconList']['Accounts'] = 'Są';
$app_list_strings['moduleIconList']['Opportunities'] = 'Pa';
$app_list_strings['moduleIconList']['Cases'] = 'At';
$app_list_strings['moduleIconList']['Notes'] = 'Pa';
$app_list_strings['moduleIconList']['Calls'] = 'Sk';
$app_list_strings['moduleIconList']['Emails'] = 'El';
$app_list_strings['moduleIconList']['Meetings'] = 'Su';
$app_list_strings['moduleIconList']['Tasks'] = 'Už';
$app_list_strings['moduleIconList']['Calendar'] = 'Ka';
$app_list_strings['moduleIconList']['Leads'] = 'Pk';
$app_list_strings['moduleIconList']['Currencies'] = 'Va';
$app_list_strings['moduleIconList']['Contracts'] = 'Su';
$app_list_strings['moduleIconList']['Quotes'] = 'Pa';
$app_list_strings['moduleIconList']['Products'] = 'Įp';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'Žl';
$app_list_strings['moduleIconList']['ProductCategories'] = 'Pk';
$app_list_strings['moduleIconList']['ProductTypes'] = 'Pt';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'Pk';
$app_list_strings['moduleIconList']['ProductBundles'] = 'Pp';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'Pp';
$app_list_strings['moduleIconList']['Reports'] = 'At';
$app_list_strings['moduleIconList']['Forecasts'] = 'Pr';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'Pd';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'Pt';
$app_list_strings['moduleIconList']['Quotas'] = 'Kv';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'Vp';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'Kk';
$app_list_strings['moduleIconList']['SugarLive'] = '„S';
$app_list_strings['moduleIconList']['Teams'] = 'Ko';
$app_list_strings['moduleIconList']['TeamNotices'] = 'Kp';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Ga';
$app_list_strings['moduleIconList']['Activities'] = 'Ve';
$app_list_strings['moduleIconList']['Comments'] = 'Ko';
$app_list_strings['moduleIconList']['Subscriptions'] = 'Pr';
$app_list_strings['moduleIconList']['Bugs'] = 'Tr';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'Mt';
$app_list_strings['moduleIconList']['TimePeriods'] = 'La';
$app_list_strings['moduleIconList']['TaxRates'] = 'Mt';
$app_list_strings['moduleIconList']['ContractTypes'] = 'St';
$app_list_strings['moduleIconList']['Schedulers'] = 'Pl';
$app_list_strings['moduleIconList']['Project'] = 'Pr';
$app_list_strings['moduleIconList']['ProjectTask'] = 'Pu';
$app_list_strings['moduleIconList']['Campaigns'] = 'Ka';
$app_list_strings['moduleIconList']['CampaignLog'] = 'Ki';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'Ks';
$app_list_strings['moduleIconList']['Documents'] = 'Do';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'Dv';
$app_list_strings['moduleIconList']['Connectors'] = 'Ju';
$app_list_strings['moduleIconList']['Notifications'] = 'Pr';
$app_list_strings['moduleIconList']['Sync'] = 'Si';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Up';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Up';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Un';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Up';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'Iv';
$app_list_strings['moduleIconList']['ReportMaker'] = 'Ia';
$app_list_strings['moduleIconList']['DataSets'] = 'Df';
$app_list_strings['moduleIconList']['CustomQueries'] = 'Nu';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Pr';
$app_list_strings['moduleIconList']['pmse_Project'] = 'Pa';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'Pv';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'Pe';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'Vc';
$app_list_strings['moduleIconList']['Shifts'] = 'Pa';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'Pi';
$app_list_strings['moduleIconList']['Purchases'] = 'Pi';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'Pe';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Mp';
$app_list_strings['moduleIconList']['PushNotifications'] = '„p';
$app_list_strings['moduleIconList']['Escalations'] = 'Iš';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'Dš';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'Ds';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'Dd';
$app_list_strings['moduleIconList']['WorkFlow'] = 'De';
$app_list_strings['moduleIconList']['EAPM'] = 'Ip';
$app_list_strings['moduleIconList']['Worksheet'] = 'Da';
$app_list_strings['moduleIconList']['Users'] = 'Va';
$app_list_strings['moduleIconList']['Employees'] = 'Da';
$app_list_strings['moduleIconList']['Administration'] = 'Ad';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Va';
$app_list_strings['moduleIconList']['InboundEmail'] = 'Ge';
$app_list_strings['moduleIconList']['Releases'] = 'Ve';
$app_list_strings['moduleIconList']['Prospects'] = 'Ad';
$app_list_strings['moduleIconList']['Queues'] = 'Ei';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'Ep';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'El';
$app_list_strings['moduleIconList']['SNIP'] = 'Ep';
$app_list_strings['moduleIconList']['ProspectLists'] = 'As';
$app_list_strings['moduleIconList']['SavedSearch'] = 'Įp';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'Av';
$app_list_strings['moduleIconList']['Trackers'] = 'Sp';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'Sp';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'Sp';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'Sp';
$app_list_strings['moduleIconList']['FAQ'] = 'DU';
$app_list_strings['moduleIconList']['Newsletters'] = 'Na';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Mė';
$app_list_strings['moduleIconList']['PdfManager'] = 'Pt';
$app_list_strings['moduleIconList']['DataArchiver'] = 'Da';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'Av';
$app_list_strings['moduleIconList']['OAuthKeys'] = '„v';
$app_list_strings['moduleIconList']['OAuthTokens'] = '„a';
$app_list_strings['moduleIconList']['Filters'] = 'Fi';
$app_list_strings['moduleIconList']['UserSignatures'] = 'Se';
$app_list_strings['moduleIconList']['Shippers'] = 'Ve';
$app_list_strings['moduleIconList']['Styleguide'] = 'Sv';
$app_list_strings['moduleIconList']['Feedbacks'] = 'At';
$app_list_strings['moduleIconList']['Tags'] = 'Žy';
$app_list_strings['moduleIconList']['Categories'] = 'Ka';
$app_list_strings['moduleIconList']['Dashboards'] = 'As';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'Ep';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'Ep';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'Dp';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'Ap';
$app_list_strings['moduleIconList']['CommentLog'] = 'Kž';
$app_list_strings['moduleIconList']['Holidays'] = 'Nd';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'Kl';
$app_list_strings['moduleIconList']['Metrics'] = 'Me';
$app_list_strings['moduleIconList']['Messages'] = 'Ži';
$app_list_strings['moduleIconList']['Audit'] = 'Au';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'Pe';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = '„v';
$app_list_strings['moduleIconList']['Geocode'] = 'Ge';
$app_list_strings['moduleIconList']['DRI_Workflows'] = '„G';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = '„G';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = '„G';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = '„G';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = '„A';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'S„';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = '„G';
$app_list_strings['moduleIconList']['Library'] = 'Bi';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'Ep';
$app_list_strings['moduleIconList']['Words'] = 'Žo';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Mė';
$app_list_strings['moduleIconList']['KBDocuments'] = 'Žb';
$app_list_strings['moduleIconList']['KBContents'] = 'Žb';
$app_list_strings['moduleIconList']['KBArticles'] = 'Žb';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'Žb';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'Įf';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';

?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.Unidad_de_Negocio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_Unidad_de_Negocio'] = 'Unidad de Negocio';
$app_list_strings['moduleListSingular']['sasa_Unidad_de_Negocio'] = 'Unidad de Negocio';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.Puntos_de_Ventas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_Puntos_de_Ventas'] = 'Puntos de Atención';
$app_list_strings['moduleListSingular']['sasa_Puntos_de_Ventas'] = 'Punto de Atención';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.Companias.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_Companias'] = 'Compañias';
$app_list_strings['moduleListSingular']['sasa_Companias'] = 'Compañia';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.UnidadesdeNegporClientesyProspecto.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SASA_UnidadesdeNegporClieyPro'] = 'Unidades de Neg. por Clientes y Pros';
$app_list_strings['moduleList']['SASA_UnidadNegClienteProspect'] = 'U Negocios por Clientes y Prospectos';
$app_list_strings['moduleListSingular']['SASA_UnidadesdeNegporClieyPro'] = 'Unidad de Neg. por Cliente y Prospec';
$app_list_strings['moduleListSingular']['SASA_UnidadNegClienteProspect'] = 'U Negocio por Cliente y Prospecto';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.Habeas Data.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SASA_Habeas_Data'] = 'Habeas Data';
$app_list_strings['moduleListSingular']['SASA_Habeas_Data'] = 'Habeas Data';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_uniddeneg_c_list.php

 // created: 2020-09-23 16:27:02

$app_list_strings['sasa_uniddeneg_c_list']=array (
  '' => '',
  120 => 'MAQUINARIA',
  1 => 'N/A',
  130 => 'REPUESTOS',
  210 => 'TALLER AUTOMOTRIZ',
  220 => 'TALLER MAQUINARIA',
  240 => 'TALLER ZNA',
  315 => 'TRÁMITES',
  340 => 'VEHÍCULOS NUEVOS COM',
  110 => 'VEHÍCULOS NUEVOS NISSAN',
  310 => 'VEHÍCULOS USADOS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_subunidneg_c_list.php

 // created: 2020-09-23 16:42:15

$app_list_strings['sasa_subunidneg_c_list']=array (
  '' => '',
  11012 => 'BUSES Y CAMIONES',
  21012 => 'TALLER BUSES Y CAMIONES',
  2144 => 'TALLER CHANGAN',
  3144 => 'VEHÍCULOS NUEVOS CHANGAN',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.Tpificacion_de_Casos.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.Tpificacion_de_Casos.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_Tipificacion_de_Casos'] = 'Tipificación de Casos';
$app_list_strings['moduleListSingular']['sasa_Tipificacion_de_Casos'] = 'Tipificación de Casos';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_Tipificacion_de_Casos'] = 'Tipificación de Casos';
$app_list_strings['moduleListSingular']['sasa_Tipificacion_de_Casos'] = 'Tipificación de Casos';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_a_new_dropdown.php

 // created: 2021-03-13 16:48:09

$app_list_strings['a_new_dropdown']=array (
  'First' => 'First Text',
  'Second' => 'Second Text',
  'Third' => 'Third Text',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_case_status_dom.php

 // created: 2020-11-06 16:13:08

$app_list_strings['case_status_dom']=array (
  '' => '',
  'Assigned' => 'ASIGNADO',
  'Rejected' => 'CANCELADO',
  'Closed' => 'CERRADO',
  'Duplicate' => 'DUPLICADO',
  'New' => 'NUEVO',
  'Pending Input' => 'PENDIENTE DE INFORMACIÓN',
  'Pending Client' => 'PENDIENTE RESPUESTA CLIENTE',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_bug_status_dom.php

 // created: 2020-11-05 18:32:31

$app_list_strings['bug_status_dom']=array (
  'a' => 'acer',
  2 => 'acer',
  'Assigned' => 'Asignado',
  'b' => 'Bacer',
  'Closed' => 'Cerrado',
  'New' => 'Nuevo',
  'Pending' => 'Pendiente',
  'Rejected' => 'Rechazado',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_call_status_dom.php

 // created: 2020-09-10 16:20:35

$app_list_strings['call_status_dom']=array (
  '' => '',
  'Not Held' => 'CANCELADA',
  'Planned' => 'PLANIFICADA',
  'Held' => 'REALIZADA',
  'reprogrammed' => 'REPROGRAMADA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_campaign_type_dom.php

// created: 2020-06-17 10:08:34
$app_list_strings['campaign_type_dom'][''] = '';
$app_list_strings['campaign_type_dom']['Telesales'] = 'Televentas';
$app_list_strings['campaign_type_dom']['Mail'] = 'Correo';
$app_list_strings['campaign_type_dom']['Email'] = 'Correo electrónico';
$app_list_strings['campaign_type_dom']['Print'] = 'Imprimir';
$app_list_strings['campaign_type_dom']['Web'] = 'Web';
$app_list_strings['campaign_type_dom']['Radio'] = 'Radio';
$app_list_strings['campaign_type_dom']['Television'] = 'Televisión';
$app_list_strings['campaign_type_dom']['NewsLetter'] = 'Boletín de Noticias';
$app_list_strings['campaign_type_dom']['999'] = 'Indefinido';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_cd_grup_list.php

 // created: 2020-10-06 14:40:00

$app_list_strings['cd_grup_list']=array (
  '' => '',
  4 => 'CAMBIO DE ACEITE',
  1 => 'MANTENIMIENTO PREPAGADO',
  5 => 'OTROS',
  3 => 'REVISIÓN DE FRENOS',
  2 => 'REVISIÓN DE MANTENIMIENTO PERIODICO (RMP)',
  6 => 'SONIDOS O CITA WEB',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_industry_list.php

 // created: 2020-05-16 08:50:16

$app_list_strings['industry_list']=array (
  '' => '',
  2 => 'AGRÍCOLA',
  5 => 'ALIMENTOS',
  6 => 'ASEGURADOR',
  1 => 'AUTOMOTRIZ',
  7 => 'BEBIDAS Y TABACO',
  3 => 'COMERCIO',
  8 => 'CONSTRUCCIÓN',
  9 => 'CONSULTORÍAS  ASESORÍAS',
  10 => 'CONSUMO MASIVO',
  11 => 'CUERO Y CALZADO',
  12 => 'EDITORIAL E IMPRESIÓN',
  13 => 'EDUCATIVO',
  14 => 'ENERGÉTICO',
  15 => 'ENTRETENIMIENTO',
  16 => 'FINANCIERO',
  4 => 'GOBIERNO',
  17 => 'MANUFACTURA',
  18 => 'MEDIOS',
  19 => 'MINERIA HIERRO ACERO Y OTROS MATERIALES',
  20 => 'PLÁSTICO Y CAUCHO',
  '0_' => 'POR DEFINIR',
  21 => 'PRODUCTOS DE VIDRIO',
  22 => 'PUBLICIDAD Y MERCADEO',
  23 => 'PULPA PAPEL Y CARTÓN',
  24 => 'QUÍMICOS',
  25 => 'SALUD',
  26 => 'SERVICIOS',
  27 => 'TECNOLOGÍA',
  28 => 'TELECOMUNICACIONES',
  29 => 'TEXTILES PRENDAS DE VESTIR Y CALZADO',
  30 => 'TRANSPORTE',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_meeting_status_dom.php

 // created: 2020-11-06 17:11:23

$app_list_strings['meeting_status_dom']=array (
  '' => '',
  'Planned' => 'Planificado',
  'Held' => 'Realizada',
  'Not Held' => 'Cancelada',
  'Rescheduled' => 'REPROGRAMADA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_salutation_list.php

 // created: 2020-05-11 17:56:07

$app_list_strings['salutation_list']=array (
  '' => '',
  'A' => 'DR',
  'B' => 'DRA',
  'F' => 'ING',
  'C' => 'SR',
  'D' => 'SRA',
  'E' => 'SRTA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_auto_contactacion_c_list.php

 // created: 2020-08-20 15:03:37

$app_list_strings['sasa_auto_contactacion_c_list']=array (
  '' => '',
  1 => 'SI, SIN EXCEPCIÓN',
  2 => 'SI, CON EXCEPCIÓN',
  0 => 'NO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_canal_preferencia_c_list.php
 
$app_list_strings['sasa_canal_preferencia_c_list'] = array (
  '' => '',
  'CORREO ELECTRONICO' => 'CORREO ELECTRÓNICO',
  'DIRECCION FISICA' => 'DIRECCIÓN FÍSICA',
  'LLAMADA' => 'LLAMADA',
  'NINGUNA' => 'NINGUNA',
  'SMS' => 'SMS',
  'TELEFONO FIJO' => 'TELÉFONO FIJO',
  'WHATSAPP' => 'WHATSAPP',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_cargo_c_list.php

 // created: 2020-05-16 10:17:36

$app_list_strings['sasa_cargo_c_list']=array (
  '' => '',
  1 => 'ABOGADO',
  37 => 'ADM. AGRÍCOLA',
  35 => 'ADMINISTRADOR',
  73 => 'ADMINISTRADOR FLOTILLA',
  29 => 'AGRICULTOR',
  47 => 'AGROPECUARIO',
  20 => 'AMA DE CASA',
  39 => 'ANALISTA',
  38 => 'ANESTESIÓLOGO',
  36 => 'ARQUITECTO',
  50 => 'ARTESANO',
  3 => 'ASESOR',
  44 => 'ASIS. DE GERENCIA',
  4 => 'ASISTENTE',
  49 => 'ATENCIÓN PREHOSPITAL',
  5 => 'AUDITOR',
  45 => 'CATEDRÁTICO',
  26 => 'COMERCIANTE',
  48 => 'CONDUCTOR',
  21 => 'CONDUCTOR DE INVIPAS',
  19 => 'CONSTRUCTOR',
  6 => 'CONSULTOR',
  27 => 'CONTADOR',
  65 => 'COORDINADOR',
  53 => 'COORDINADOR ACADÉMICO',
  57 => 'DIR. DE PLANTA FÍSICA',
  42 => 'DIR. DE TALENTO HUMANO',
  60 => 'DIR.PROY.ALUMBRADO P',
  8 => 'DIRECTOR',
  55 => 'DIRECTOR DE EQUIPOS',
  51 => 'DIRECTOR DE OBRA',
  33 => 'DIS. ESPACIOS Y HABI',
  32 => 'DISEÑADOR  DE ESPACIO',
  9 => 'EJECUTIVO DE CUENTA',
  34 => 'ESTUDIANTE',
  28 => 'FINANCISTA',
  69 => 'FOTÓGRAFO',
  59 => 'GER. ADM. FINANCIERO',
  61 => 'GER. DE OPERACIONES',
  41 => 'GER. DE PROYECTOS',
  10 => 'GERENTE',
  54 => 'GERENTE ADMINISTRATIVO',
  40 => 'GERENTE DE PRODUCCIÓN',
  43 => 'GERENTE DE VENTAS',
  11 => 'GERENTE GENERAL',
  46 => 'ING. DE OLEODUCTO',
  18 => 'INGENIERO DE DESARROLLO',
  52 => 'INGENIERO FORESTAL',
  12 => 'JEFE',
  58 => 'MED. HOSPITALARIO',
  63 => 'NUTRICIONISTA',
  23 => 'OPTÓMETRA',
  64 => 'ORTOPEDIA',
  31 => 'PENSIONADO',
  68 => 'PERIODISTA',
  70 => 'PRESENTADOR',
  13 => 'PRESIDENTE',
  66 => 'RELACIONISTA PÚBLICO',
  72 => 'REPRESENTANTE LEGAL',
  14 => 'SUBDIRECTOR',
  15 => 'SUBGERENTE',
  62 => 'SUPERVISOR',
  24 => 'TÉC. ADMINISTRATIVO',
  25 => 'VENTAS',
  16 => 'VICEPRESIDENTE',
  71 => 'OTRO',
  17 => 'NINGUNO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_cd_area_list.php

 // created: 2020-11-09 14:30:10

$app_list_strings['sasa_cd_area_list']=array (
  '' => '',
  12022 => 'AGRÍCOLA',
  11012 => 'BUSES Y CAMIONES',
  34044 => 'CHANGAN',
  15353 => 'FINANCIERAS',
  12023 => 'INDUSTRIAL',
  12020 => 'MAQUINARIA',
  12021 => 'MONTACARGA',
  11011 => 'NO COMERCIALES',
  13030 => 'REPUESTOS',
  22022 => 'TALLER AGRÍCOLA',
  21010 => 'TALLER AUTOMOTRIZ',
  21011 => 'TALLER AUTOMOTRIZ (V. NUEVOS)',
  21012 => 'TALLER BUSES Y CAMIONES',
  22023 => 'TALLER INDUSTRIAL',
  22020 => 'TALLER MAQUINARIA',
  22021 => 'TALLER MONTACARGA',
  22018 => 'TALLER TOPCON',
  31515 => 'TRÁMITES',
  34040 => 'VEHÍCULOS',
  11010 => 'VEHÍCULOS NUEVOS',
  31011 => 'VEHÍCULOS USADOS',
  34041 => 'ZNA NO COMERCIALES',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_cliente_fallecido_c_list.php

 // created: 2020-03-02 10:59:46

$app_list_strings['sasa_cliente_fallecido_c_list']=array (
  '' => '',
  'S' => 'SÍ',
  'N' => 'NO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_compania_empleados_c_list.php

 // created: 2020-05-27 15:24:27

$app_list_strings['sasa_compania_empleados_c_list']=array (
  '' => '',
  5 => 'AGENCIA MILENIO',
  4 => 'AUTOVARDÍ',
  3 => 'COM AUTOMOTRIZ S.A.',
  1 => 'DISTRIBUIDORA NISSAN S.A.',
  6 => 'INVERSIONES HACIENDA CEREZOS S',
  2 => 'TALLERES AUTORIZADOS S.A',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_deportes_c_list.php

 // created: 2020-05-16 10:34:47

$app_list_strings['sasa_deportes_c_list']=array (
  '' => '',
  2 => 'ATLETISMO',
  3 => 'AUTOMOVILISMO',
  10 => 'CICLISMO',
  13 => 'FÚTBOL',
  16 => 'GOLF',
  21 => 'MONTAÑISMO',
  22 => 'MOTOCICLISMO',
  26 => 'PESCA',
  31 => 'TENIS',
  '0_' => 'NINGÚNO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_de_reco_cita_list.php

 // created: 2020-10-06 14:41:29

$app_list_strings['sasa_de_reco_cita_list']=array (
  '' => '',
  'L' => 'LLAMADA',
  'E' => 'MAIL',
  'M' => 'SMS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_estadocomercial_c_list.php
 
$app_list_strings['sasa_estadocomercial_c_list'] = array (
  '' => '',
  'COTIZADO' => 'COTIZADO',
  'EN_PROCESO_COMERCIAL' => 'EN PROCESO COMERCIAL',
  'GESTIONADO_CALL' => 'GESTIONADO CALL',
  'NO_CONTACTADO' => 'NO CONTACTADO',
  'SIN_INTERES' => 'SIN INTERÉS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_estado_autorizacion_c_list.php
 
$app_list_strings['sasa_estado_autorizacion_c_list'] = array (
	'' => '',
  	'1' => 'VIGENTE',
  	'2' => 'NO VIGENTE',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_estado_c_list.php

 // created: 2020-07-29 09:32:30

$app_list_strings['sasa_estado_c_list']=array (
  '' => '',
  'A' => 'ACTIVO',
  'I' => 'INACTIVO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_fuente_autorizacion_c_list.php
 
$app_list_strings['sasa_fuente_autorizacion_c_list'] = array (
  '' => '',
  'M' => 'E-MAIL',
  'W' => 'INTERNET',
  'L' => 'LLAMADA',
  'P' => 'PRESENCIAL',
  'T' => 'VITRINA VIRTUAL',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_genero_c_list.php

 // created: 2020-05-11 19:27:09

$app_list_strings['sasa_genero_c_list']=array (
  '' => '',
  'F' => 'FEMENINO',
  'M' => 'MASCULINO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_account_type_list.php

 // created: 2020-05-16 09:21:07

$app_list_strings['account_type_list']=array (
  '' => '',
  'C' => 'CLIENTE',
  'D' => 'CLIENTE RECOMPRA',
  'B' => 'PROSPECTO CON COTIZACIÓN',
  'A' => 'PROSPECTO SIN COTIZACIÓN',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_call_direction_dom.php

 // created: 2020-09-10 15:29:41

$app_list_strings['call_direction_dom']=array (
  'Inbound' => 'ENTRANTE ',
  'Outbound' => 'SALIENTE',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_campaign_status_dom.php

// created: 2020-11-05 19:19:12
$app_list_strings['campaign_status_dom']['a1'] = 'a1';
$app_list_strings['campaign_status_dom']['a2'] = 'a2';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_case_priority_dom.php

 // created: 2020-10-08 08:05:16

$app_list_strings['case_priority_dom']=array (
  '' => '',
  'P1' => 'ALTA',
  'P2' => 'MEDIA',
  'P3' => 'BAJA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_estado_pqrfs_list.php

 // created: 2020-09-09 20:08:41

$app_list_strings['estado_pqrfs_list']=array (
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_lead_source_list.php

 // created: 2020-11-06 11:16:00

$app_list_strings['lead_source_list']=array (
  '' => '',
  23 => 'APP',
  28 => 'APP',
  5 => 'ASESOR',
  1 => 'CAMPAÑA',
  19 => 'CAMPAÑA FACEBOOK',
  41 => 'CAMPAÑA INSTAGRAM',
  16 => 'CAMPAÑA KICKS',
  35 => 'CAMPAÑA KICKS',
  43 => 'CAMPAÑA LINKEDIN',
  30 => 'CAMPAÑA MARCH',
  26 => 'CAMPAÑA MARCH',
  21 => 'CAMPAÑA QASHQAI',
  24 => 'CAMPAÑA SENTRA',
  32 => 'CAMPAÑA SENTRA',
  42 => 'CAMPAÑA TWITTER',
  31 => 'CAMPAÑA VERSA',
  25 => 'COMENT. REDES SOCIALES',
  18 => 'CONCURSO',
  2 => 'CONVENIO',
  33 => 'COTIZACIÓN',
  6 => 'DIGITAL',
  36 => 'DISIPLAN',
  39 => 'E-MAIL',
  3 => 'EVENTO',
  22 => 'EVENTO DIGITAL',
  14 => 'FACEBOOK',
  29 => 'LLAMADA',
  17 => 'PAGINA WEB',
  15 => 'PAGINA WEB',
  11 => 'PASACALLE',
  38 => 'PLAT. ECOMMERCE',
  13 => 'PRENSA',
  4 => 'PUBLICIDAD',
  8 => 'RADIO',
  9 => 'REVISTA MOTOR',
  20 => 'SITIO WEB',
  40 => 'TEP',
  10 => 'VOLANTE',
  37 => 'WHATSAPP',
  34 => 'OTRO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_motivo_visita_list.php

 // created: 2021-04-05 14:13:45

$app_list_strings['motivo_visita_list']=array (
  '' => '',
  4 => 'CAMBIO DE ACEITE',
  1 => 'MANTENIMIENTO PREPAGADO',
  5 => 'OTROS',
  3 => 'REVISIÓN DE FRENOS',
  2 => 'REVISIÓN DE MANTEMIENTO PERIODICO (RMP)',
  6 => 'SONIDOS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_reminder_time_options.php

 // created: 2020-11-09 10:04:06

$app_list_strings['reminder_time_options']=array (
  '' => '',
  -1 => 'NINGUNA',
  60 => '1 MINUTO ANTES ',
  300 => '5 MINUTOS ANTES ',
  600 => '10 MINUTOS ANTES ',
  900 => '15 MINUTOS ANTES ',
  1800 => '30 MINUTOS ANTES ',
  3600 => '1 HORA ANTES ',
  7200 => '2 HORAS ANTES ',
  10800 => '3 HORAS ANTES ',
  18000 => '5 HORAS ANTES ',
  86400 => '1 DÍA ANTES ',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_area_solicitante_c_list.php

 // created: 2020-09-22 10:03:09

$app_list_strings['sasa_area_solicitante_c_list']=array (
  '' => '',
  'A' => 'DIRECCIÓN ADMINISTRATIVA',
  'B' => 'GERENCIA FINANCIERA',
  'C' => 'GERENCIA DE MERCADEO',
  'D' => 'GERENCIA COMERCIAL',
  'E' => 'GESTIÓN HUMANA',
  'F' => 'CAPACITACIÓN',
  'G' => 'GERENCIA POSVENTA',
  'H' => 'DIRECCIÓN DE SEGURIDAD',
  'I' => 'GERENCIA DE TECNOLOGÍA INFORMÁTICA',
  'J' => 'MERCADEO USADOS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_canales_autorizados_c_list.php

 // created: 2021-01-04 16:50:35

$app_list_strings['sasa_canales_autorizados_c_list']=array (
  '' => '',
  '1' => 'CELULAR',
  '6' => 'CORREO ELECTRÓNICO',
  '2' => 'DIRECCIÓN FÍSICA',
  '5' => 'FAX',
  '0' => 'NINGUNA',
  '3' => 'OFICINA',
  '7' => 'PERSONALMENTE',
  '8' => 'SMS',
  '4' => 'TELÉFONO FIJO',
  '9' => 'WHATSAPP',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_cantidad_empleados_c_list.php

 // created: 2020-03-11 10:32:53

$app_list_strings['sasa_cantidad_empleados_c_list']=array (
  '' => '',
  1 => 'HASTA 10',
  2 => 'DESDE 11 HASTA 50',
  3 => 'DESDE 51 HASTA 100',
  4 => 'DESDE 101 HASTA 250',
  5 => 'DESDE 251 HASTA 500',
  6 => 'DESDE 501 HASTA 1000',
  7 => 'DESDE 1001 Y MÁS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_categoria_c_list.php

 // created: 2020-05-16 09:01:28

$app_list_strings['sasa_categoria_c_list']=array (
  '' => '',
  3 => 'EMPRENDEDOR',
  2 => 'ESTÁNDAR',
  4 => 'INACTIVOS',
  1 => 'PLATINO',
  5 => 'POTENCIAL ESTÁNDAR',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_cd_uneg_cont_list.php

/*
 // created: 2020-12-28 02:27:11

$app_list_strings['sasa_cd_uneg_cont_list']=array (
  '' => '',
  120 => 'MAQUINARIA',
  130 => 'REPUESTOS',
  210 => 'TALLER AUTOMOTRIZ',
  220 => 'TALLER MAQUINARIA',
  240 => 'TALLER ZNA',
  315 => 'TRÁMITES',
  340 => 'VEHÍCULOS NUEVOS COM - ZNA',
  110 => 'VEHÍCULOS NUEVOS NISSAN',
  310 => 'VEHÍCULOS USADOS',
  410 => 'N/A',
);
*/
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_compania_c_list.php

 // created: 2020-11-09 10:21:09

$app_list_strings['sasa_compania_c_list']=array (
  '' => '',
  3 => 'COM AUTOMOTRIZ S.A.',
  1 => 'DISTRIBUIDORA NISSAN S.A.',
  2 => 'TALLERES AUTORIZADOS S.A.',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_controlproceso_c_list.php
 
$app_list_strings['sasa_controlproceso_c_list'] = array (
  '' => '',
  1 => '1',
  2 => '2',
  3 => '3',
  4 => '4',
  5 => '5',
  6 => '6',
  7 => '7',
  8 => '8',
  9 => '9',
  10 => '10',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_document_solicitado_c_list.php

 // created: 2020-10-08 08:15:51

$app_list_strings['sasa_document_solicitado_c_list']=array (
  '' => '',
  'I' => 'CERTIFICACIÓN BANCARIA',
  'D' => 'CERTIFICADO ACEITE',
  'C' => 'CERTIFICADO DE COMBUSTIBLE',
  'Q' => 'CERTIFICADO DE HOMOLOGACIÓN',
  'F' => 'CERTIFICADO DESECHO LLANTAS',
  'L' => 'CERTIFICADO DIAN',
  'J' => 'CERTIFICADO ICA',
  'O' => 'CERTIFICADO IMPORTACIÓN',
  'E' => 'CERTIFICADO RESIDUOS',
  'K' => 'CERTIFICADO RETENCIÓN EN LA FUENTE',
  'N' => 'CERTIFICADOS LABORALES PERSONAL NO ACTIVO',
  'S' => 'COMPROBANTE DE PAGO IMPUESTO VEHÍCULO',
  'H' => 'CÁMARA DE COMERCIO',
  'P' => 'DECLARACIÓN DE IMPORTACIÓN',
  'M' => 'DOCUMENTOS PROVEEDORES',
  'R' => 'FACTURA DE VENTA',
  'B' => 'FACTURA OT',
  'A' => 'HISTORIAL MANTENIMIENTO VEHÍCULO',
  'T' => 'HISTORIAL VEHÍCULO',
  'G' => 'RUT',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_estadocontacto_c_list.php
 
$app_list_strings['sasa_estadocontacto_c_list'] = array (
  '' => '',
  'CONTACTADO' => 'CONTACTADO',
  'NO CONTACTADO' => 'NO CONTACTADO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_estado_civil_c_list.php

 // created: 2020-05-16 10:32:54

$app_list_strings['sasa_estado_civil_c_list']=array (
  '' => '',
  'C' => 'CASADO',
  'D' => 'DIVORCIADO',
  'S' => 'SOLTERO',
  'U' => 'UNIÓN LIBRE',
  'V' => 'VIUDO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_estrato_c_list.php

 // created: 2020-02-26 12:27:10

$app_list_strings['sasa_estrato_c_list']=array (
  '' => '',
  1 => 'ESTRATO 1',
  2 => 'ESTRATO 2',
  3 => 'ESTRATO 3',
  4 => 'ESTRATO 4',
  5 => 'ESTRATO 5',
  6 => 'ESTRATO 6',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_fuente_c_list.php

 // created: 2020-05-16 12:25:58

$app_list_strings['sasa_fuente_c_list']=array (
  '' => '',
  'PACE' => 'PACE',
  'Sigru' => 'SIGRU',
  'Sugar' => 'SUGAR',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_gestionado_c_list.php

 // created: 2021-10-07 16:54:15

$app_list_strings['sasa_gestionado_c_list']=array (
  '' => '',
  1 => 'GESTIONADA',
  2 => 'NO GESTIONADA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_motivo_c_list.php

 // created: 2021-09-14 15:35:10

$app_list_strings['sasa_motivo_c_list']=array (
  '' => '',
  1 => 'ACABADOS, LÁMINA Y PINTURA',
  2 => 'ACCESORIOS (Q)',
  3 => 'ACCESORIOS INSTALADOS (Q)',
  4 => 'ACCESORIOS NO INSTALADOS (QR)',
  5 => 'ACTITUD DEL PERSONAL DE SERVICIO (Q)',
  6 => 'ACTIVACIÓN (TRÁFICOS - EVENTOS - LANZAMIENTOS)',
  7 => 'ACTIVACIÓN TASA',
  8 => 'ACTUALIZACIÓN DE DATOS DE PROPIETARIO',
  241 => 'ACTUALIZACIÓN DE DATOS PERSONALES',
  9 => 'ADMINISTRATIVO',
  10 => 'AGILIDAD EN LA ATENCIÓN AL CLIENTE (Q)',
  11 => 'AGILIDAD EN LA ENTREGA DE LA MÁQUINA (Q)',
  12 => 'AGILIDAD EN LA ENTREGA DEL VEHÍCULO (Q)',
  13 => 'ALTO COSTO EN LOS REPUESTOS (R)',
  14 => 'ASESORÍA E INFORMACIÓN (Q)',
  15 => 'ATENCIÓN AL CLIENTE (Q)',
  16 => 'AUTORIZACIÓN DE TRABAJOS ADICIONALES (QR)',
  17 => 'AUTORIZACIÓN DE USO DE DATOS PERSONALES (PR)',
  237 => 'BATERÍA (R)',
  18 => 'BOLSA DE AIRE (R)',
  19 => 'BOMBAS, CILINDROS (R)',
  20 => 'BUEN DESEMPEÑO DEL EMPLEADO (F)',
  21 => 'BUEN SERVICIO AL CLIENTE (F)',
  22 => 'BUENA CALIDAD DEL PRODUCTO O SERVICIO (F)',
  24 => 'CAJA DE DIRECCIÓN (QR)',
  25 => 'CAJA DIRECCIÓN (QR)',
  26 => 'CAJA MECÁNICA (QR)',
  27 => 'CAJA TRANSMISIÓN (QR)',
  28 => 'CAJA, TRANSMISIÓN Y EMBRAGUE',
  29 => 'CALIDAD DE ACCESORIOS (QR)',
  30 => 'CALIDAD DE LA REPARACIÓN (QR)',
  31 => 'CALIDAD DE LA REPARACIÓN DE LATONERÍA Y (Q)',
  32 => 'CALIDAD DE LLANTAS (QR)',
  33 => 'CALIDAD DE LOS ACCESORIOS (R)',
  34 => 'CALIDAD DE PRODUCTO (QR)',
  35 => 'CALIDAD DE PRODUCTO EN LA ENTREGA (QR)',
  36 => 'CALIDAD DE PRODUCTO EN LA ENTREGA TALLER (QR)',
  37 => 'CALIDAD DE PRODUCTO POSTERIOR ENTREGA (Q)',
  38 => 'CALIDAD DE VIDRIOS',
  39 => 'CALIDAD DEL AIRE ACONDICIONADO (R)',
  40 => 'CALIDAD DEL DIAGNÓSTICO (QR)',
  41 => 'CALIDAD DEL PRODUCTO POSTERIOR ENTREGA (Q)',
  42 => 'CALIDAD DEL REPUESTO (QR)',
  43 => 'CALIDAD DEL SERVICIO (QR)',
  44 => 'CALIDAD EN EL ALISTAMIENTO (QR)',
  45 => 'CALIDAD EN LA INSTALACIÓN DE ACCESORIOS',
  46 => 'CALIDAD EN LA REPARACIÓN DE COLISIÓN (QR)',
  47 => 'CALIDAD EN LA REV. MANTENIMIENTO PERIÓDICO (QR)',
  48 => 'CALIDAD EN TRABAJOS DE ELECTRICIDAD (Q)',
  49 => 'CAMBIO DE PROPIETARIO',
  50 => 'CAMPAÑA DE SERVICIO',
  51 => 'CANCELACIÓN CITA',
  23 => 'CAÍDA LLAMADA',
  52 => 'CHANGAN',
  53 => 'CINTURONES DE SEGURIDAD (R)',
  54 => 'CITA CAMPAÑA DE SEGURIDAD',
  55 => 'CITA CAMPAÑA DE SERVICIO',
  56 => 'CITA CARRO TALLER',
  57 => 'CITA EN VITRINA',
  58 => 'CITA GARANTÍA',
  59 => 'CITA TALLER RMP',
  60 => 'COMUNICADOS INTERNOS',
  238 => 'CONFIRMACIÓN CIERRE PQRFS',
  61 => 'CONFIRMACIÓN CITA TALLER',
  62 => 'CONSUMO DE COMBUSTIBLE (QR)',
  234 => 'CONVOCATORIA CLIENTES DE RMP',
  235 => 'CONVOCATORIA CLIENTES PRIMER SERVICIO',
  63 => 'COSTO DE LA REPARACIÓN O SERVICIO (R)',
  64 => 'COSTOS CARGADOS NO AUTORIZADOS (R)',
  65 => 'COSTOS DE LA REPARACIÓN O SERVICIO (QR)',
  66 => 'COSTOS DE REPUESTOS (Q)',
  67 => 'COSTOS TRÁMITES (R)',
  69 => 'COTIZACIÓN ACCESORIOS',
  70 => 'COTIZACIÓN MAQUINARIA',
  68 => 'COTIZACIÓN PRECIOS DE SERVICIOS',
  71 => 'COTIZACIÓN REPUESTOS',
  72 => 'COTIZACIÓN USADOS',
  73 => 'COTIZACIÓN VEHÍCULO USADO',
  74 => 'COTIZACIÓN VEHÍCULOS NUEVOS',
  75 => 'CUMPLIMIENTO EN LA ENTREGA DEL VEHÍCULO (Q)',
  76 => 'CURSO MECÁNICA',
  77 => 'DEMORA EN EL ENVÍO DE INFORMACIÓN (Q)',
  78 => 'DEMORA EN EL SUMINISTRO DE REPUESTOS (QR)',
  79 => 'DEMORA EN LA DEVOLUCIÓN DEL DINERO (QR)',
  80 => 'DEMORA EN LA ENTREGA DEL VEHÍCULO (QR)',
  81 => 'DEMORA EN LA RECEPCIÓN DEL VEHÍCULO (Q)',
  82 => 'DEMORA EN PROCESOS DE RETOMA (Q)',
  83 => 'DEMORA EN TRÁMITES INTERNOS (QR)',
  220 => 'DEMORA TRÁMITES INTERNOS',
  86 => 'DEVOLUCIONES DE REPUESTOS',
  84 => 'DEVOLUCIÓN DE DINERO (PQR)',
  240 => 'DEVOLUCIÓN POR GARANTÍA',
  85 => 'DEVOLUCIÓN SALDO A FAVOR (R)',
  87 => 'DIFICULTAD EN RESPUESTA TELEFÓNICA (Q)',
  88 => 'DIFICULTAD PARA REALIZAR PAGOS (Q)',
  89 => 'DIFICULTAD PARA SOLICITAR CITA (Q)',
  90 => 'DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES',
  91 => 'DISPONIBILIDAD DE CITAS PARA EL TALLER (QR)',
  92 => 'DISPONIBILIDAD DE REPUESTOS (PQR)',
  93 => 'ELECTRICIDAD Y ELECTRÓNICA',
  94 => 'ELIMINACIÓN DE LAS BASES DE DATOS (P)',
  95 => 'EMBRAGUE (QR)',
  96 => 'ENCENDIDO (QR)',
  233 => 'ENCUESTAS',
  97 => 'ERROR EN EL COSTO ESTIMADO DE REPARACIÓN (Q)',
  98 => 'ESPECIFICACIONES MAQUINARIA',
  99 => 'ESPECIFICACIONES VEHÍCULO NUEVO',
  100 => 'ESPECIFICACIONES VEHÍCULO USADO',
  101 => 'ESTADO DEL NEGOCIO',
  102 => 'ESTADO DEL PEDIDO DEL REPUESTO',
  103 => 'ESTADO PQRFS',
  104 => 'ESTADO VEHÍCULO EN COLISIÓN',
  105 => 'ESTADO VEHÍCULO EN TALLER',
  224 => 'ESTUDIO DE PRECIOS AUTOPARTES',
  225 => 'ESTUDIO DE PRECIOS EN MANO DE OBRA',
  223 => 'ESTUDIO DE PRECIOS LUBRICANTES',
  106 => 'ESTÁTICA',
  229 => 'EVENTO VITRINAS O TALLERES',
  107 => 'EXPLICACIÓN DE GARANTÍA (PQR)',
  108 => 'EXPLICACIÓN TRÁMITES (P)',
  109 => 'FALTA DE ACOMPAÑAMIENTO EN PROCESO COMPRA (Q)',
  110 => 'FALTA DE AGILIDAD PARA OTORGAR CITAS (Q)',
  111 => 'FALTA DE ASESORÍA (Q)',
  112 => 'FALTA DE COMUNICACIÓN CON EL CLIENTE (QR)',
  113 => 'FALTA DE CONOCIMIENTO DEL PRODUCTO (QR)',
  114 => 'FALTA DE SEGUIMIENTO (Q)',
  115 => 'FALTA EXPLICACIÓN DEL SERVICIO EFECTUADO (QR)',
  116 => 'FALTANTES EN LA ENTREGA (QR)',
  117 => 'FRENOS',
  118 => 'FUNCIONAMIENTO RADIO/SISTEMA DE AUDIO (R)',
  243 => 'GARANTÍA EXTENDIDA',
  119 => 'HISTORIAL DE SERVICIO',
  120 => 'INCONFORMIDAD POR EL PRECIO DE RETOMA (Q)',
  121 => 'INCONSISTENCIA FACTURA (QR)',
  122 => 'INCONSISTENCIAS DURANTE LA ENTREGA (Q)',
  123 => 'INCONSISTENCIAS EN COSTOS (R)',
  124 => 'INCONSISTENCIAS EN PAGOS (R)',
  125 => 'INCUMPLIMIENTO DE COMPROMISOS (QR)',
  126 => 'INCUMPLIMIENTO EN ENVÍO DE COTIZACIÓN (Q)',
  127 => 'INCUMPLIMIENTO EN LA CITA DE SERVICIO (Q)',
  128 => 'INCUMPLIMIENTO EN LA ENTREGA',
  129 => 'INCUMPLIMIENTO EN LA ENTREGA DEL VEHÍCULO',
  130 => 'INCUMPLIMIENTO EN TÉRMINOS DEL NEGOCIO (QR)',
  131 => 'INCUMPLIMIENTO FECHA DE ENTREGA',
  132 => 'INF. HISTORIAL SERV. FACTURAS VENTA ETC. (P)',
  133 => 'INFORMACIÓN ADMINISTRATIVA',
  134 => 'INFORMACIÓN BRINDADA SOBRE PRODUCTO (QR)',
  135 => 'INFORMACIÓN CAMPAÑA DE SEGURIDAD',
  136 => 'INFORMACIÓN DE UBICACIÓN DIRECCIÓN, TELÉFONOS Y HORARIOS',
  137 => 'INFORMACIÓN DESEMPEÑO Y FUNCIONAMIENTO (P)',
  138 => 'INFORMACIÓN ERRADA DEL PRODUCTO (Q)',
  246 => 'INFORMACIÓN GENERAL',
  139 => 'INFORMACIÓN SOBRE PRODUCTOS Y SERVICIOS (PQ)',
  140 => 'INFORMACIÓN SOBRE TRÁMITES (Q)',
  141 => 'INFORMACIÓN VEHÍCULO NUEVO',
  142 => 'INFORMACIÓN VEHÍCULO USADO',
  143 => 'INFORMACIÓN VEHÍCULOS',
  144 => 'INFORMACIÓN VITRINAS, CAMPAÑAS, ASESORES, LANZAMIENTOS Y RETOMAS',
  145 => 'INGRESO POR COLISIÓN',
  146 => 'INSTALACIONES (Q)',
  147 => 'INVENTARIO DEL VEHÍCULO/PERTENENCIAS DE (QR)',
  148 => 'INVENTARIO, PERTENENCIAS DEL VEHÍCULO (Q)',
  149 => 'INVESTIGACIONES DE MERCADO',
  231 => 'INVITACIÓN A EVENTOS CORPORATIVOS',
  228 => 'INVITACIÓN A PERIODISTAS',
  227 => 'INVITACIÓN DE VITRINAS A NIVEL NACIONAL',
  150 => 'INYECCIÓN, ELECTRICIDAD Y ELECTRÓNICA',
  230 => 'LANZAMIENTO PRODUCTOS',
  151 => 'LIMPIEZA DE LA MÁQUINA (QR)',
  152 => 'LIMPIEZA DEL VEHÍCULO (QR)',
  153 => 'MANDOS (R)',
  154 => 'MANTENIMIENTO PREVENTIVO',
  155 => 'MAQUINARIA',
  156 => 'MERCADEO (PQ)',
  157 => 'MOTOR (QR)',
  158 => 'MOTOR, CAJA, TRANSMISIÓN Y EMBRAGUE',
  160 => 'MOTOR, TREN DE POTENCIA',
  159 => 'MOTOR, TREN DE POTENCIA (QR)',
  161 => 'N/A TALLER: ESTADO DEL VEHÍCULO',
  162 => 'NO REALIZACIÓN TRASPASO RETOMA (R)',
  163 => 'PAGOS ELECTRÓNICOS (QR)',
  164 => 'PAGOS PSE',
  245 => 'PENDIENTE POR CLASIFICAR',
  166 => 'PERMANENCIA DE LA MÁQUINA EN EL TALLER (R)',
  167 => 'PERMANENCIA DEL VEHÍCULO EN EL TALLER (QR)',
  168 => 'POLÍTICAS FORMAS DE PAGO (Q)',
  170 => 'PRECIO DE RETOMA (Q)',
  171 => 'PRECIO DE VEHÍCULOS',
  172 => 'PRECIOS RMP',
  173 => 'PREPAGADOS',
  174 => 'PROGRAMACIÓN DE CITAS CALL CENTER (Q)',
  175 => 'PROGRAMACIÓN EMISORAS DEL RADIO (Q)',
  176 => 'PROMOCIÓN MES',
  177 => 'PROPUESTA COMERCIAL',
  222 => 'PROTOCOLO TALLER',
  221 => 'PROTOCOLO VITRINA',
  165 => 'PÉRDIDA DE POTENCIA (QR)',
  169 => 'PÓLIZA',
  178 => 'RAYONES (Q)',
  179 => 'RAYONES O GOLPES (QR)',
  232 => 'RECORDATORIO CITA TALLER PROGRAMADA',
  180 => 'REPROGRAMACIÓN CITA',
  236 => 'REPROGRAMACIÓN DE CITAS NO CUMPLIDAS',
  239 => 'REVISIÓN PQRFS A CLIENTE',
  181 => 'RUIDOS (QR)',
  182 => 'SEGUIMIENTO A GUÍAS DESPACHOS',
  183 => 'SEGUIMIENTO DESPUÉS DE LA ENTREGA (Q)',
  226 => 'SEGUIMIENTO PQRFS A REPRESENTANTES',
  184 => 'SEGUROS MILENIO',
  244 => 'SERVICIO A DOMICILIO',
  185 => 'SERVICIO DE CAJA (Q)',
  242 => 'SERVICIO DE RECOGIDA DEL VEHÍCULO',
  187 => 'SISTEMA DE NAVEGACIÓN (R)',
  188 => 'SISTEMA HIDRÁULICO (R)',
  189 => 'SOLICITUD COPIA DE DOCUMENTOS (P)',
  190 => 'SOLICITUD COPIA MANUAL DE GARANTÍAS (P)',
  191 => 'SOLICITUD COPIA MANUAL DEL CONDUCTOR (PR)',
  192 => 'SOLICITUD DE CERTIFICACIÓN (P)',
  193 => 'SOLICITUD DE COTIZACIÓN (P)',
  194 => 'SOLICITUD DE DOCUMENTOS',
  195 => 'SOLICITUD DE FICHA TÉCNICA (P)',
  196 => 'SOLICITUD DE INFORMACIÓN (QR)',
  197 => 'SOLICITUD DIAGNÓSTICO DE PIEZAS (P)',
  198 => 'SOLICITUD INFO. TRASPASO VEHÍCULO RETOMA (P)',
  199 => 'SOLICITUD INFORMACIÓN DESEMPEÑO Y FUNCIO (P)',
  200 => 'SOPORTE TÉCNICO',
  219 => 'SUGERENCIA (S)',
  201 => 'SUMINISTRO DE REPUESTOS',
  202 => 'SUSPENSIÓN',
  204 => 'TEST DRIVE (Q)',
  205 => 'TIEMPO DE SUMINISTRO DE REPUESTOS (Q)',
  206 => 'TIEMPO ESTIMADO DEL SERVICIO (Q)',
  207 => 'TRABAJO INCOMPLETO (QR)',
  211 => 'TRANSFERENCIA DE LLAMADA',
  212 => 'TRASLADO DE SALDO (PR)',
  213 => 'TRATAMIENTO DE DATOS',
  208 => 'TRÁMITE FINANCIERO',
  209 => 'TRÁMITES (PQR)',
  210 => 'TRÁMITES NO AUTORIZADOS (R)',
  203 => 'TÉRMINOS DE GARANTÍA (Q)',
  214 => 'UBICACIÓN DE LA SALA DE VENTAS (Q)',
  215 => 'UBICACIÓN DEL MOSTRADOR (Q)',
  216 => 'UBICACIÓN DEL PUNTO DE VENTA (Q)',
  217 => 'UBICACIÓN DEL TALLER (Q)',
  218 => 'VISITA DE SERVICIO (Q)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_ocupacion_c_list.php

 // created: 2020-05-16 10:26:15

$app_list_strings['sasa_ocupacion_c_list']=array (
  '' => '',
  6 => 'AMA DE CASA',
  11 => 'DEPORTISTA',
  3 => 'EMPLEADO',
  1 => 'ESTUDIANTE',
  4 => 'INDEPENDIENTE',
  5 => 'PENSIONADO',
  2 => 'PROPIETARIO DE EMPRESA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_regimen_tributario_c_list.php

 // created: 2020-05-16 09:59:20

$app_list_strings['sasa_regimen_tributario_c_list']=array (
  '' => '',
  41 => 'AGENTE DE RETENCIÓN IVA',
  9 => 'AUTORRETENEDOR',
  7 => 'GRAN CONTRIBUYENTE',
  42 => 'RÉGIMEN SIMPLE DE TRIBUTACIÓN',
  43 => 'NO APLICA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_id_ava_c_list.php
 
$app_list_strings['sasa_id_ava_c_list'] = array (
  '' => '',
  'A' => 'AVANCE PARCIAL',
  'R' => 'AVANCE SOLUCIÓN',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_id_tipo_c_list.php
 
$app_list_strings['sasa_id_tipo_c_list'] = array (
  '' => '',
  'I' => 'CONFIDENCIAL',
  'P' => 'AL CLIENTE',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_musica_c_list.php

 // created: 2020-02-26 12:48:58

$app_list_strings['sasa_musica_c_list']=array (
  '' => '',
  1 => 'BOLERO',
  2 => 'CLÁSICA',
  3 => 'COLOMBIANA',
  5 => 'JAZZ',
  6 => 'MERENGUE',
  7 => 'METAL',
  9 => 'POP',
  11 => 'RANCHERA',
  14 => 'REGUETÓN',
  16 => 'ROCK',
  17 => 'ROCK EN ESPAÑOL',
  19 => 'SALSA',
  22 => 'VALLENATO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_profesion_c_list.php

 // created: 2020-05-16 10:24:35

$app_list_strings['sasa_profesion_c_list']=array (
  '' => '',
  1 => 'ADMINISTRACIÓN',
  2 => 'ADMINISTRACIÓN DE EMPRESA',
  36 => 'AGRICULTOR',
  69 => 'ALMACENISTA GENERAL',
  53 => 'ANESTESIÓLOGO',
  3 => 'ARQUITECTURA',
  72 => 'ARTES GRÁFICAS',
  4 => 'ARTES PLÁSTICAS',
  49 => 'ASEGURADOR',
  5 => 'BIOLOGÍA',
  6 => 'CIENCIAS POLÍTICAS',
  38 => 'COMERCIANTE',
  66 => 'COMERCIO EXTERIOR',
  7 => 'COMUNICACIÓN SOCIAL',
  58 => 'CONDUCTOR',
  65 => 'CONSULTORÍA',
  8 => 'CONTADURÍA',
  9 => 'DERECHO',
  51 => 'DISEÑADOR INDUSTRIAL',
  10 => 'DISEÑO',
  43 => 'DOCENTE',
  11 => 'ECONOMÍA',
  12 => 'EDUCACIÓN PREESCOLAR',
  47 => 'EMPLEADO',
  52 => 'ESTUDIANTE',
  13 => 'FILOSOFÍA',
  45 => 'FINANCIERO',
  15 => 'HOTELERÍA Y TURISMO',
  16 => 'IDIOMAS',
  48 => 'INDEPENDIENTE',
  63 => 'ING. AERONÁUTICO',
  55 => 'ING. AGRÓNOMO',
  70 => 'ING. AMBIENTAL',
  64 => 'ING. AUTOMOTRIZ',
  17 => 'ING. CIVIL',
  18 => 'ING. DE ALIMENTOS',
  19 => 'ING. DE PETRÓLEOS',
  73 => 'ING. ELECTROMECÁNICO',
  20 => 'ING. ELECTRÓNICA',
  62 => 'ING. FORESTAL',
  21 => 'ING. INDUSTRIAL',
  42 => 'ING. INFORMÁTICO',
  22 => 'ING. MECÁNICA',
  23 => 'ING. QUÍMICA',
  24 => 'ING. SISTEMAS',
  41 => 'LIC. EN BIOLOGÍA',
  25 => 'MATEMÁTICAS',
  26 => 'MEDICINA',
  40 => 'MILITAR',
  27 => 'MÚSICA',
  28 => 'NUTRICIÓN Y DIETÉTICA',
  29 => 'ODONTOLOGÍA',
  44 => 'PENSIONADO',
  61 => 'POLITÓLOGA',
  54 => 'PRODUCTOR',
  30 => 'PSICOLOGÍA',
  31 => 'PUBLICIDAD Y MERCADEO',
  46 => 'TEC. CONTABLE Y FINANCIERO',
  60 => 'TEC. EN URG. MEDICAS',
  59 => 'TEC. PRODUC. ANIMAL',
  32 => 'TRABAJO SOCIAL',
  37 => 'TRANSPORTADOR',
  39 => 'TÉCNICO ELECTRICISTA',
  33 => 'VETERINARIA',
  50 => 'OTRA',
  35 => 'NINGUNA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_respuesta_c_list.php

 // created: 2020-09-08 14:05:46

$app_list_strings['sasa_respuesta_c_list']=array (
  '' => '',
  'S' => 'SI',
  'N' => 'NO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_id_contacto_list.php

 // created: 2020-12-28 02:26:09

$app_list_strings['sasa_id_contacto_list']=array (
  '' => '',
  'D' => 'DIRECCIÓN FÍSICA',
  'E' => 'EMAIL',
  'L' => 'LLAMADA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_mascota_c_list.php

 // created: 2020-02-26 12:32:30

$app_list_strings['sasa_mascota_c_list']=array (
  '' => '',
  'P' => 'PERRO',
  'G' => 'GATO',
  'O' => 'OTRO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_nivel_educativo_c_list.php

 // created: 2020-05-16 10:27:22

$app_list_strings['sasa_nivel_educativo_c_list']=array (
  '' => '',
  11 => 'DIPLOMADO',
  12 => 'DOCTORADO',
  1 => 'JARDÍN',
  7 => 'MAESTRÍA',
  8 => 'MBA',
  6 => 'POSGRADO',
  2 => 'PRIMARIA',
  5 => 'PROFESIONAL',
  3 => 'SECUNDARIA',
  4 => 'TÉCNICO/TECNÓLOGO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_regimen_fiscal_c_list.php

 // created: 2020-02-25 16:09:10

$app_list_strings['sasa_regimen_fiscal_c_list']=array (
  '' => '',
  'C' => 'IMPUESTO SOBRE LAS VENTAS + IVA',
  'S' => 'NO RESPONSABLE IVA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_revision_c_list.php

 // created: 2021-10-07 17:13:39

$app_list_strings['sasa_revision_c_list']=array (
  '' => '',
  'N' => 'NO VÁLIDO',
  'S' => 'SIN REVISAR',
  'V' => 'VÁLIDO',
  'A' => 'NO VALIDAR',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_de_vivienda_c_list.php

 // created: 2020-05-16 10:31:35

$app_list_strings['sasa_tipo_de_vivienda_c_list']=array (
  '' => '',
  4 => 'EN ARRIENDO',
  3 => 'FAMILIAR',
  5 => 'LEASING',
  7 => 'PROPIA',
  1 => 'PROPIA EN PAGO',
  2 => 'PROPIA YA PAGA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_evento_c_list.php

 // created: 2020-09-22 16:24:28

$app_list_strings['sasa_tipo_evento_c_list']=array (
  '' => '',
  'A' => 'CARRO TALLER',
  'B' => 'CURSO MECÁNICA',
  'E' => 'EVENTO INTERNO',
  'C' => 'EVENTO VITRINA',
  'D' => 'LANZAMIENTO DE PRODUCTOS',
  'F' => 'RECOGIDA DE VEHÍCULO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_reunion_c_list.php

 // created: 2020-09-22 16:04:47

$app_list_strings['sasa_tipo_reunion_c_list']=array (
  '' => '',
  'A' => 'CITA',
  'B' => 'EVENTO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_tipi_c_list.php

 // created: 2020-10-08 08:02:21

$app_list_strings['sasa_tipo_tipi_c_list']=array (
  '' => '',
  'H' => 'FELICITACIÓN',
  'E' => 'INFORMACIÓN',
  'G' => 'OTROS',
  'A' => 'PETICIÓN',
  'B' => 'QUEJA',
  'C' => 'RECLAMO',
  'F' => 'SOLICITUD',
  'D' => 'SUGERENCIA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_task_priority_dom.php

 // created: 2020-09-10 15:12:45

$app_list_strings['task_priority_dom']=array (
  'High' => 'ALTA',
  'Medium' => 'MEDIA',
  'Low' => 'BAJA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_solicitud_c_list.php

 // created: 2021-01-22 15:48:51

$app_list_strings['sasa_tipo_solicitud_c_list']=array (
  '' => '',
  4 => 'INFORMACIÓN COTIZACIÓN  POSVENTA',
  2 => 'INFORMACIÓN COTIZACIÓN  VEHÍCULOS NUEVOS',
  1 => 'INFORMACIÓN COTIZACIÓN USADOS',
  11 => 'INFORMACIÓN EVENTO',
  3 => 'INFORMACIÓN PRECIO DE VEHÍCULOS',
  12 => 'OTRO',
  10 => 'PENDIENTE POR CLASIFICAR',
  5 => 'SOLICITUD COTIZACIÓN REPUESTOS',
  6 => 'SOLICITUD COTIZACIÓN ACCESORIOS',
  13 => 'SOLICITUD DE SERVICIOS DE TALLER',
  9 => 'SOLICITUD INFORMACIÓN ADMINISTRATIVA',
  7 => 'SOLICITUD INFORMACIÓN VEHÍCULOS',
  8 => 'SOLICITUD TEST DRIVE',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_vendidopornosotros_c_list.php

 // created: 2020-05-11 19:24:48

$app_list_strings['sasa_vendidopornosotros_c_list']=array (
  '' => '',
  'S' => 'SÍ',
  'N' => 'NO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_status_reu_list_c.php

 // created: 2020-09-22 15:58:50

$app_list_strings['status_reu_list_c']=array (
  '' => '',
  'B' => 'CANCELADA',
  'D' => 'CUMPLIDA',
  'A' => 'PROGRAMADA',
  'C' => 'REPROGRAMADA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_template_type_list.php

 // created: 2020-07-29 09:40:58

$app_list_strings['template_type_list']=array (
  '' => '',
  'P' => 'Aviso de Privacidad',
  'F' => 'Formato Habeas Data',
  'T' => 'Tratamiento de Datos Personales',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_sector_c_list.php

 // created: 2021-03-04 15:27:59

$app_list_strings['sasa_sector_c_list']=array (
  '24_1' => 'ABONOS PLAGUICIDAS Y QUIMICOS AGROP',
  '29_1' => 'ACABADOS TEXTILES',
  '5_1' => 'ACEITES Y GRASAS COMESTIBLES',
  '8_1' => 'ACONDICIONAMIENTO Y TERMINACIÓN DE',
  '30_1' => 'AEREO',
  '22_1' => 'AGENCIAS DE PUBLICIDAD',
  '22_2' => 'AGENCIAS PROMOCIONALES',
  '2_1' => 'AGRICULTURA VARIOS',
  '4_1' => 'AGRICULTURA Y DESARROLLO RURAL',
  '6_1' => 'AJUSTADORES DE SEGUROS',
  '5_2' => 'ALIMENTOS PARA ANIMALES',
  '26_1' => 'ALIMENTOS Y BEBIDAS',
  '3_1' => 'ALMACENES DE CADENA',
  '3_2' => 'ALMACENES VARIOS',
  '1_3' => 'AREGLO FREDY',
  '25_1' => 'ARP',
  '2_2' => 'AVICOLA',
  '5_3' => 'AZÚCAR',
  '16_1' => 'BANCA',
  '22_3' => 'BROKERS DE MEDIOS',
  '5_4' => 'CAFÉ EXPORTADORES',
  '2_3' => 'CAFÉ PRODUCCIÓN',
  '16_2' => 'CAJEROS ELECTRÓNICOS',
  '11_1' => 'CALZADO',
  '1_10' => 'CARROCERIAS PARTES Y PIEZAS PARA VE',
  '28_1' => 'CELULARES',
  '8_2' => 'CEMENTO',
  '7_1' => 'CERVEZA',
  '8_3' => 'CERÁMICA Y OTROS MATERIALES NO METÁ',
  '5_5' => 'CHOCOLATE Y CONFITERIA',
  '4_2' => 'CIENCIA Y TECNOLOGÍA',
  '15_1' => 'CINE Y VIDEOS',
  '26_4' => 'CLUBES',
  '13_1' => 'COLEGIOS',
  '1_11' => 'COMERCIALIZACIÓN DE PARTES Y ACCESO',
  '27_1' => 'COMERCIO DE COMPUTADORES',
  '4_3' => 'COMERCIO EXTERIOR',
  '13_2' => 'COMPAÑIAS DE CAPACITACIÓN CONTINUAD',
  '16_3' => 'COMPAÑIAS DE FINANCIAMIENTO',
  '6_2' => 'COMPAÑIAS DE SEGUROS',
  '14_1' => 'COMPAÑIAS PETROLERAS',
  '16_4' => 'COMPAÑÍAS DE LEASING',
  '4_4' => 'COMUNICACIONES',
  '3_3' => 'CONCESIONARIOS',
  '1_12' => 'CONCESIONARIOS',
  '29_2' => 'CONFECCIONES',
  '5_6' => 'CONSERVAS CONDIMENTOS Y PASABOCAS',
  '8_4' => 'CONSTRUCCIÓN',
  '21_3' => 'CONSTRUCCIÓN',
  '9_1' => 'CONSULTORIA CONTABLE/REVISORIA FISC',
  '9_2' => 'CONSULTORIA EN RECURSOS HUMANOS',
  '10_1' => 'CONSUMO MASIVO',
  '26_5' => 'COOPERATIVAS',
  '16_5' => 'CORPORACIONES DE AHORRO Y VIVIENDA',
  '16_6' => 'CORREDORES DE BOLSA',
  '6_3' => 'CORREDORES DE SEGUROS',
  '26_6' => 'CORREO',
  '22_4' => 'CORREO DIRECTO',
  '11_2' => 'CURTIDO DE CUEROS',
  '4_5' => 'DEFENSA Y SEGURIDAD NACIONAL',
  '27_2' => 'DESARROLLO Y DISEÑO DE PÁGINA WEB',
  '14_2' => 'DISTRIBUCIÓN DE COMBUSTIBLE',
  '17_1' => 'DISTRIBUCIÓN DE ENSERES DOMESTICOS',
  '17_2' => 'DISTRIBUCIÓN DE MAQUINARIA Y EQUIPO',
  '8_5' => 'DISTRIBUCIÓN DE MATERIALES DE CONST',
  '2_4' => 'DISTRIBUCIÓN DE PRODUCTOS E INSUMOS',
  '24_3' => 'DISTRIBUCIÓN DE PRODUCTOS FARMACEUT',
  '29_3' => 'DISTRIBUCIÓN DE PRODUCTOS TEXTILES',
  '24_2' => 'DISTRIBUCIÓN/CONSUMO MASIVO  DE ELE',
  '5_7' => 'DISTRIBUCIÓN/CONSUMO MASIVO DE ALIM',
  '7_2' => 'DISTRIBUCIÓN/CONSUMO MASIVO DE BEBI',
  '3_4' => 'DROGUERIAS',
  '4_6' => 'ECONOMÍA Y DESARROLLO ECONÓMICO',
  '4_7' => 'EDUCACIÓN CULTURA Y TURISMO',
  '17_3' => 'ELECTRODOMESTICOS',
  '14_3' => 'ENERGÍA ELECTRICA',
  '1_13' => 'ENSAMBLADORA DE VEHÍCULOS',
  '21_1' => 'ENVASES',
  '25_2' => 'EPS',
  '28_2' => 'EQUIPOS DE TELECOMUNICACIÓN',
  '3_5' => 'ESTACIONES DE SERVICIO/SERVITECAS',
  '4_8' => 'ESTADÍSTICA',
  '14_4' => 'EXPLOTACIÓN DE CARBÓN',
  '3_6' => 'FERRETERIAS',
  '16_7' => 'FIDUCIARIA',
  '9_3' => 'FIRMAS DE ABOGADOS',
  '9_4' => 'FIRMAS DE CONSULTORIA EMPRESARIAL',
  '2_5' => 'FLORES',
  '16_8' => 'FONDOS DE PENSIONES',
  '4_9' => 'FUNCIÓN PUBLICA',
  '2_6' => 'GANADERIA',
  '14_5' => 'GAS',
  '7_3' => 'GASEOSAS JUGOS Y AGUAS',
  '4_10' => 'GRAMIOS Y ASOCIACIONES',
  '29_4' => 'HILANDERIAS',
  '3_7' => 'HIPERMERCADOS',
  '26_7' => 'HOTELES',
  '1_1' => 'IMPORTADOR',
  '1_14' => 'IMPORTADORES DE VEHÍCULOS',
  '12_1' => 'IMPRESIÓN EDITORIAL',
  '26_8' => 'INFORMÁTICOS',
  '8_6' => 'INFRAESTRUCTURA',
  '8_7' => 'INFRAESTRUCTURA VIAL',
  '8_8' => 'INGENIERIA CIVIL',
  '13_3' => 'INSTITUTOS TÉCNICOS',
  '4_11' => 'INTERIOR CONTROL Y ORGANIZACIÓN ELE',
  '18_1' => 'INTERNET',
  '25_3' => 'IPS',
  '4_12' => 'JUSTICIA Y DERECHO',
  '24_4' => 'LABORATORIOS FARMACEUTICOS Y OTROS',
  '5_8' => 'LACTEOS',
  '12_2' => 'LIBROS FOLLETOS Y SIMILARES',
  '7_4' => 'LICORES',
  '11_3' => 'MALETAS BOLSOS Y SIIMILARES',
  '17_4' => 'MANUFACTURAS VARIAS',
  '17_5' => 'MAQUINARIA Y EQUIPO',
  '30_2' => 'MARITIMO Y FLUVIAL',
  '25_4' => 'MEDICINA PREPAGADA',
  '4_13' => 'MEDIO AMBIENTE',
  '19_1' => 'METALES BÁSICOS HIERRO ACERO Y OTRO',
  '19_2' => 'METALES NO FERROSOS',
  '4_14' => 'MINAS Y ENERGÍA',
  '5_9' => 'MOLINERIA DE ARROZ',
  '5_10' => 'MOLINERIA Y PRODUCTOS DE TRIGO Y MA',
  '17_6' => 'MUEBLES Y ACCESORIOS',
  '30_3' => 'OPERADORES AGENTES Y TERMINALES',
  '4_15' => 'ORGANIZACIONES NO GUBERNAMENTALES',
  '15_2' => 'OTROS',
  '28_3' => 'OTROS',
  '14_6' => 'OTROS',
  '22_5' => 'OTROS',
  '21_2' => 'OTROS PRODUCTOS DE VIDRIO',
  '26_9' => 'OTROS SERVICIOS',
  '23_1' => 'PAPEL CELULOSA Y CARTON ONDULADO',
  '15_3' => 'PARQUES DE DIVERSIONES',
  '5_11' => 'PASTAS PANADERIA Y GALLETERIA',
  '18_2' => 'PERIÓDICOS',
  '2_7' => 'PESCA',
  '24_5' => 'PINTURAS BARNICES Y SIMILARES',
  '4_16' => 'PLANEACIÓN',
  '20_1' => 'PLASTICOS PRIMARIOS',
  '0_0' => 'POR DEFINIR',
  '8_9' => 'PREPARACIÓN DE TERRENO',
  '4_17' => 'PRESIDENCIA GOBERNACIONES Y ALCALDI',
  '15_4' => 'PRODUCCIÓN Y GRABACIÓN MUSICAL',
  '27_3' => 'PRODUCTORES Y DISTRIBUIDORES DE SOF',
  '5_12' => 'PRODUCTOS CARNICOS',
  '24_6' => 'PRODUCTOS DE ASEO Y COSMÉTICOS',
  '20_2' => 'PRODUCTOS DE CAUCHO',
  '17_7' => 'PRODUCTOS DE MADERA',
  '19_3' => 'PRODUCTOS DE METAL',
  '23_2' => 'PRODUCTOS DE PAPEL Y CARTON',
  '20_3' => 'PRODUCTOS DE PLÁSTICO',
  '24_7' => 'QUÍMICOS BÁSICOS',
  '24_8' => 'QUíMICOS INDUSTRIALES Y DIVERSOS',
  '18_3' => 'RADIO',
  '6_4' => 'REASEGURADORES',
  '4_18' => 'RELACIONES EXTERIORES',
  '22_6' => 'RELACIONES PÚBLICAS',
  '1_2' => 'REPUESTOS',
  '26_10' => 'RESTAURANTES',
  '18_4' => 'REVISTAS',
  '4_19' => 'SALUD TRABAJO Y SEGURIDAD SOCIAL',
  '25_5' => 'SEGURIDAD SOCIAL',
  '26_11' => 'SERVICIOS DE ASEO',
  '28_4' => 'SERVICIOS DE TELECOMUNICACIONES',
  '16_9' => 'SERVICIOS FINANCIEROS',
  '25_6' => 'SERVICIOS HOSPITALARIOS',
  '14_7' => 'SERVICIOS PETROLEROS',
  '4_20' => 'SERVICIOS PÚBLICOS',
  '26_12' => 'SERVICIOS PÚBLICOS',
  '19_4' => 'SIDERURGIA',
  '3_8' => 'SUPERMERCADOS',
  '7_5' => 'TABACO',
  '1_15' => 'TALLERES',
  '16_10' => 'TARJETAS DE CRÉDITO',
  '18_5' => 'TELEVISIÓN',
  '9_5' => 'TEMPORALES/DOTACIÓN DE PERSONAL',
  '30_4' => 'TERRESTRE',
  '29_5' => 'TEXTILES',
  '3_9' => 'TIENDAS',
  '4_21' => 'TRANSPORTE',
  '26_13' => 'TURISMO',
  '13_4' => 'UNIVERSIDADES',
  '30_5' => 'VALORES',
  '21_4' => 'VEHÍCULOS',
  '26_14' => 'VIGILANCIA Y SEGURIDAD',
  '8_10' => 'VIVIENDA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tiene_hijos_c_list.php

 // created: 2020-03-02 11:02:50

$app_list_strings['sasa_tiene_hijos_c_list']=array (
  '' => '',
  'S' => 'SÍ',
  'N' => 'NO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipotel1_c_list.php

 // created: 2020-07-31 10:09:18

$app_list_strings['sasa_tipotel1_c_list']=array (
  '' => '',
  'C' => 'CASA',
  'OF' => 'OFICINA',
  'OT' => 'OTRO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_cases_c_list.php

 // created: 2021-02-02 11:59:13

$app_list_strings['sasa_tipo_cases_c_list']=array (
  '' => '',
  'NA' => 'N/A',
  'H' => 'FELICITACIÓN',
  'E' => 'INFORMACIÓN',
  'G' => 'OTROS',
  'A' => 'PETICIÓN',
  'B' => 'QUEJA',
  'C' => 'RECLAMO',
  'F' => 'SOLICITUD',
  'D' => 'SUGERENCIA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_de_cliente_c_list.php

 // created: 2021-04-15 15:53:08

$app_list_strings['sasa_tipo_de_cliente_c_list']=array (
  '' => '',
  'C' => 'CLIENTE',
  'D' => 'CLIENTE RECOMPRA',
  'B' => 'PROSPECTO CON COTIZACIÓN',
  'A' => 'PROSPECTO SIN COTIZACIÓN',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_de_nota_c_list.php

 // created: 2020-09-10 15:40:16

$app_list_strings['sasa_tipo_de_nota_c_list']=array (
  '' => '',
  'AVANCE PARCIAL' => 'AVANCE PARCIAL',
  'AVANCE SOLUCION' => 'AVANCE SOLUCIÓN',
  'NOTIFICACION AL ASESOR' => 'NOTIFICACIÓN AL ASESOR',
  'SOLICITUD' => 'SOLICITUD',

);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_de_persona_c_list.php

 // created: 2020-02-28 08:55:53

$app_list_strings['sasa_tipo_de_persona_c_list']=array (
  '' => '',
  'N' => 'NATURAL',
  'J' => 'JURÍDICA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_de_tarea_c_list.php

 // created: 2020-09-10 15:09:44

$app_list_strings['sasa_tipo_de_tarea_c_list']=array (
  '' => '',
  1 => 'CON RESPUESTA ',
  2 => 'SIN RESPUESTA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_documento_c_list.php

 // created: 2020-05-11 18:53:24

$app_list_strings['sasa_tipo_documento_c_list']=array (
  '' => '',
  'D' => 'CARNÉ DIPLOMÁTICO',
  'C' => 'CÉDULA DE CIUDADANÍA',
  'E' => 'CÉDULA DE EXTRANJERÍA',
  'U' => 'N.U.I.P',
  'N' => 'NIT',
  'P' => 'PASAPORTE',
  'T' => 'TARJETA DE IDENTIDAD',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_persona_c_list.php

 // created: 2020-05-16 09:06:34

$app_list_strings['sasa_tipo_persona_c_list']=array (
  '' => '',
  'J' => 'JURÍDICA',
  'N' => 'NATURAL',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_sigru_c_list.php

 // created: 2020-05-16 08:44:07

$app_list_strings['sasa_tipo_sigru_c_list']=array (
  '' => '',
  'A' => 'AGENTE EN ZONA DE IN',
  'S' => 'ASEGURADORAS',
  'C' => 'CLIENTE CORRIENTE',
  'X' => 'CLIENTE DEL EXTERIOR',
  'E' => 'CLIENTE ESPECIAL',
  'F' => 'CLIENTE ESPECIAL TAS',
  'O' => 'CLIENTE OFICIAL',
  'Z' => 'CLIENTE OTRA MARCA',
  'Y' => 'CLIENTE TAXI',
  'K' => 'CONCESIONARIO',
  'G' => 'CONCESIONARIO DSCTO',
  'M' => 'EMPLEADOS',
  'V' => 'FAMILIA VARGAS',
  'J' => 'FLOTILLAS',
  'N' => 'GRUPO NOVARTIS',
  'R' => 'GUBERNAMENTAL',
  'P' => 'PROVEEDOR',
  'B' => 'SOTRAMAR',
  'I' => 'TALLER INDEPENDIENTE',
  'D' => 'TALLERES AUTORIZADOS',
  'T' => 'TRANSPORTADORES',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_unidad_de_negocio_c_list.php

 // created: 2020-11-09 15:19:16

$app_list_strings['sasa_unidad_de_negocio_c_list']=array (
  '' => '',
  153 => 'FINANCIERAS',
  120 => 'MAQUINARIA',
  130 => 'REPUESTOS',
  210 => 'TALLER AUTOMOTRIZ',
  220 => 'TALLER MAQUINARIA',
  315 => 'TRÁMITES',
  340 => 'VEHÍCULOS NUEVOS CHANGAN',
  110 => 'VEHÍCULOS NUEVOS NISSAN',
  310 => 'VEHÍCULOS USADOS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_status_list.php

 // created: 2021-10-19 11:11:08

$app_list_strings['status_list']=array (
  '' => '',
  'A' => 'NUEVO',
  'B' => 'ASIGNADO',
  'C' => 'EN PROCESO COMERCIAL',
  'D' => 'CLIENTE/PROSPECTO EXISTENTE',
  'E' => 'REASIGNADO',
  'F' => 'SIN INTERÉS',
  'G' => 'GESTIONADO CALL',
  'H' => 'COTIZADO',
  'I' => 'CONVERTIDO',
  'J' => 'NO CONTACTADO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_task_status_dom.php

 // created: 2021-05-17 16:49:57

$app_list_strings['task_status_dom']=array (
  'Not Started' => 'NO INICIADA',
  'In Progress' => 'EN PROGRESO',
  'Completed' => 'COMPLETADA ',
  'Pending Input' => 'PENDIENTE DE INFORMACIÓN ',
  'Deferred' => 'APLAZADA ',
  'Cancelada' => 'CANCELADA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_resultado_c_list.php
 
$app_list_strings['sasa_resultado_c_list'] = array (
  '' => '',
  'SI' => 'SI',
  'NO' => 'NO',
  'NO CONTACTADO' => 'NO CONTACTADO',
  'GESTIONADO' => 'GESTIONADO',
  'NUMERO EQUIVOCADO' => 'NUMERO EQUIVOCADO',
  'NO VOLVER A LLAMAR' => 'NO VOLVER A LLAMAR',
  'LLAMAR DESPUES' => 'LLAMAR DESPUÉS',
  'CONTACTO NO EFECTIVO' => 'CONTACTO NO EFECTIVO',
  'FUERA DE SERVICIO' => 'FUERA DE SERVICIO',
  'CONFIRMA' => 'CONFIRMA',
  'INTERESADO' => 'INTERESADO',
  'NO CONFIRMA' => 'NO CONFIRMA',
  'PERDIDA DE VEHICULO' => 'PÉRDIDA DE VEHÍCULO',
  'VENTA DE VEHICULO' => 'VENTA DE VEHICULO',
  'NO INFORMA' => 'NO INFORMA',
  'DECIDIO NO CAMBIAR DE MODELO' => 'DECIDIO NO CAMBIAR DE MODELO',
  'CREDITO CON REQUISITO D (CODEUDOR)' => 'CREDITO CON REQUISITO D (CODEUDOR)',
  'RETOMA' => 'RETOMA',
  'MENOS COSTO' => 'MENOS COSTO',
  'VALOR RESTANTE' => 'VALOR RESTANTE',
  'NO LE MANTUVIERON LA RETOMA' => 'NO LE MANTUVIERON LA RETOMA',
  'INVERSION DE VIVIENDA' => 'INVERSION DE VIVIENDA',
  'VENTA DE SU CARRO ACTUAL' => 'VENTA DE SU CARRO ACTUAL',
  'PRESTAMO' => 'PRÉSTAMO',
  'ENCUESTADO' => 'ENCUESTADO',
  'TELEFONO FUERA DE SERVICIO' => 'TELEFONO FUERA DE SERVICIO',
  'DESISTE DE LA ENCUESTA' => 'DESISTE DE LA ENCUESTA',
  'YA REALIZO LA ENCUESTA' => 'YA REALIZÓ LA ENCUESTA',
  'SIN DATOS' => 'SIN DATOS',
  'CONFIRMA DATOS Y ASISTENCIA ' => 'CONFIRMA DATOS Y ASISTENCIA ',
  'NO ASISTE' => 'NO ASISTE',
  'DATO ERRADO' => 'DATO ERRADO',
  'CONFIRMA ASISTENCIA' => 'CONFIRMA ASISTENCIA',
  'FALLECIDO' => 'FALLECIDO',
  'YA COMPRÓ NISSAN' => 'YA COMPRÓ NISSAN',
  'YA COMPRÓ OTRA MARCA' => 'YA COMPRÓ OTRA MARCA',
  'POSTERGA COMPRA' => 'POSTERGA COMPRA',
  'USADO OTRA MARCA' => 'USADO OTRA MARCA',
  'NO GESTIONADO' => 'NO GESTIONADO',
  'NO REALIZA SERVICIOS CON NISSAN' => 'NO REALIZA SERVICIOS CON NISSAN',
  'NO CONTESTA' => 'NO CONTESTA',
  'CASO CERRADO' => 'CASO CERRADO',
  'CASO REABIERTO' => 'CASO REABIERTO',
  'AVANCE PARCIAL' => 'AVANCE PARCIAL',
  'SEGUIMIENTO A CLIENTE' => 'SEGUIMIENTO A CLIENTE',
  'SEGUIMIENTO A REPRESENTANTE' => 'SEGUIMIENTO A REPRESENTANTE',
  'INGRESO DE CASO' => 'INGRESO DE CASO',
  'CASO REPETIDO' => 'CASO REPETIDO',
  'CASO ALTO IMPACTO' => 'CASO ALTO IMPACTO',
  'CASO REDES' => 'CASO REDES',
  'CASO LATAM' => 'CASO LATAM',
  'APRUEBA EL CIERRE' => 'APRUEBA EL CIERRE',
  'AGENDADO' => 'AGENDADO',
  'CIUDAD SIN COBERTURA' => 'CIUDAD SIN COBERTURA',
  'NO APLICA VEH OTRA MARCA' => 'NO APLICA VEH OTRA MARCA',
  'PERDIDA TOTAL' => 'PÉRDIDA TOTAL',
  'SE COMUNICARA PARA AGENDAR LA CITA' => 'SE COMUNICARA PARA AGENDAR LA CITA',
  'SIN GESTION' => 'SIN GESTIÓN',
  'YA REALIZO SERVICIO' => 'YA REALIZO SERVICIO',
  'YA TIENE CITA PROGRAMADA' => 'YA TIENE CITA PROGRAMADA',
  'NO CUMPLE KM' => 'NO CUMPLE KM',
  'REPUESTO DE IMPORTACION' => 'REPUESTO DE IMPORTACIÓN',
  'NO INTERESADO' => 'NO INTERESADO',
  'NO PERMITE VER COTIZACION' => 'NO PERMITE VER COTIZACIÓN',
  'VENTA TRASLADADA' => 'VENTA TRASLADADA',
  'NO DAN INFORMACION' => 'NO DAN INFORMACIÓN',
  'CANCELADA' => 'CANCELADA',
  'NO TIENE CITA PROGRAMADA' => 'NO TIENE CITA PROGRAMADA',
  'REPROGRAMACION' => 'REPROGRAMACIÓN',
  'DESISTE' => 'DESISTE',
  'NO CUMPLE PROTOCOLO' => 'NO CUMPLE PROTOCOLO',
  'CUMPLE PROTOCOLO' => 'CUMPLE PROTOCOLO',
  'PERDIDA DE  VEHICULO' => 'PÉRDIDA DE  VEHÍCULO',
  'NO REALIZO LA COMPRA DEL REPUESTO' => 'NO REALIZÓ LA COMPRA DEL REPUESTO',
  'NO INGRESO EL VEHICULO' => 'NO INGRESÓ EL VEHÍCULO',
  'NO APARECE EN SUGAR' => 'NO APARECE EN SUGAR',
  'AUN NO HAN TERMINADO LA REPARACION' => 'AÚN NO HAN TERMINADO LA REPARACIÓN',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_de_llamada_c_list.php
 
$app_list_strings['sasa_tipo_de_llamada_c_list'] = array (
  '' => '',
  'CONFIRMACION_CIERRE_PQRFS' => 'CONFIRMACIÓN CIERRE PQRFS',
  'CONVOCATORIA_DE_MANTENIMIENTO_RMP' => 'CONVOCATORIA DE MANTENIMIENTO RMP',
  'CONVOCATORIA_LOCAL' => 'CONVOCATORIA LOCAL',
  'CONVOCATORIA_PRIMER_SERVICIO' => 'CONVOCATORIA PRIMER SERVICIO',
  'CONVOCATORIA_REPUESTOS' => 'CONVOCATORIA REPUESTOS',
  'CURSO_MECANICA' => 'CURSO MECÁNICA',
  'DESTRATES' => 'DESTRATES',
  'ENCUESTA' => 'ENCUESTAS',
  'ESTADO_DEL_VEHICULO' => 'ESTADO DEL VEHÍCULO',
  'ESTUDIO_DE_PRECIOS_AUTOPARTES_Y_LUBRICANTES' => 'ESTUDIO DE PRECIOS AUTOPARTES Y LUBRICANTES',
  'ESTUDIO_ DE_ PRECIOS_EN_MANO_DE_OBRA' => 'ESTUDIO DE PRECIOS EN MANO DE OBRA',
  'EVENTOS_VITRINAS_O_TALLERES' => 'EVENTOS VITRINAS O TALLERES',
  'GESTION WEB' => 'GESTIÓN WEB',
  'INVESTIGACION_MERCADOS' => 'INVESTIGACIÓN MERCADOS',
  'INVITACION_A_VITRINAS_A_NIVEL_NACIONAL_PREVENTA' => 'INVITACIÓN A VITRINAS A NIVEL NACIONAL (PREVENTA)',
  'INVITACION_EVENTOS_CORPORATIVOS' => 'INVITACIÓN EVENTOS CORPORATIVOS',
  'INVITACION_PERIODISTAS' => 'INVITACIÓN PERIODISTAS',
  'LANZAMIENTO_PRODUCTOS' => 'LANZAMIENTO PRODUCTOS',
  'PROTOCOLO_ TALLERES' => 'PROTOCOLO TALLERES',
  'PROTOCOLO_VITRINAS' => 'PROTOCOLO VITRINAS',
  'RECORDATORIO_CITA_TALLER_PROGRAMADA' => 'RECORDATORIO CITA TALLER PROGRAMADA',
  'REPROGRAMACION_CITAS_NO_CUMPLIDAS' => 'REPROGRAMACIÓN CITAS NO CUMPLIDAS',
  'SEGUIMIENTO_PQRFS_ A_ CLIENTE' => 'SEGUIMIENTO PQRFS A CLIENTE',
  'SEGUIMIENTO_PQRFS_A REPRESENTANTES' => 'SEGUIMIENTO PQRFS A REPRESENTANTES',
  'INFORMACION_COMERCIAL' => '¿REQUIERE INFORMACIÓN COMERCIAL?',
  'SE_GENERO_CITA_TALLER' => '¿SE GENERÓ CITA TALLER?',
  'SE_GENERO_COTIZACION' => '¿SE GENERÓ COTIZACIÓN?',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_id_esta_c_list.php

 // created: 2020-11-09 15:13:33

$app_list_strings['sasa_id_esta_c_list']=array (
  '' => '',
  'E' => 'ENVIADO INICIAL',
  'EA' => 'ENVIADO AMARILLO',
  'EE' => 'ENVIADO DE EMERGENCIA',
  'EN' => 'ENVIADO NARANJA',
  'ER' => 'ENVIADO ROJO',
  'G' => 'GRABADO INICIAL',
  'GA' => 'GRABADO AMARILLO',
  'GE' => 'GRABADO DE EMERGENCIA',
  'GN' => 'GRABADO NARANJA',
  'GR' => 'GRABADO ROJO',
  'P' => 'PROCESO INICIAL',
  'PA' => 'PROCESO AMARILLO',
  'PE' => 'PROCESO DE EMERGENCIA',
  'PN' => 'PROCESO NARANJA',
  'PR' => 'PROCESO ROJO',
  'R' => 'CONTROL DE CALIDAD',
  'S' => 'SOLUCIONADO',
  'SE' => 'SOLUCIONADO ENVIADO',
  'SR' => 'SOLUCIONADO POR REPRESENTANTE',
  'C' => 'CERRADO SATISFECHO',
  'I' => 'CERRADO INSATISFECHO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.Campanas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SASA_Campanas'] = 'Campañas';
$app_list_strings['moduleListSingular']['SASA_Campanas'] = 'Campaña';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipo_c_list.php

// created: 2022-07-22 20:41:48
$app_list_strings['sasa_tipo_c_list'][''] = '';
$app_list_strings['sasa_tipo_c_list']['B'] = 'BODEGA';
$app_list_strings['sasa_tipo_c_list']['C'] = 'COMPETIDORES';
$app_list_strings['sasa_tipo_c_list']['M'] = 'MOSTRADOR';
$app_list_strings['sasa_tipo_c_list']['S21010295'] = 'Motivo Nuevo CINCO';
$app_list_strings['sasa_tipo_c_list']['S21010294'] = 'Motivo Nuevo CUATROOO';
$app_list_strings['sasa_tipo_c_list']['S21010293'] = 'Motivo Nuevo TRESS';
$app_list_strings['sasa_tipo_c_list']['S21010292'] = 'Motivo Nuevo Tres';
$app_list_strings['sasa_tipo_c_list']['S21010291'] = 'Motivo Nuevo dos';
$app_list_strings['sasa_tipo_c_list']['O'] = 'OTROS';
$app_list_strings['sasa_tipo_c_list']['T'] = 'TALLER';
$app_list_strings['sasa_tipo_c_list']['V'] = 'VITRINA';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.GVCentrosDeCostos.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_sasa_centrocostos_c'] = 'Centros De Costos';
$app_list_strings['moduleList']['sasa_CentrosDeCostos'] = 'Centros De Costos';
$app_list_strings['moduleListSingular']['sasa_sasa_centrocostos_c'] = 'Centros De Costos';
$app_list_strings['moduleListSingular']['sasa_CentrosDeCostos'] = 'Centros De Costos';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_parent_type_display.php

// created: 2022-11-23 10:51:57
$app_list_strings['parent_type_display']['Accounts'] = 'Cuenta';
$app_list_strings['parent_type_display']['Contacts'] = 'Contacto';
$app_list_strings['parent_type_display']['Tasks'] = 'Tarea';
$app_list_strings['parent_type_display']['Opportunities'] = 'Cotización';
$app_list_strings['parent_type_display']['Products'] = 'Línea de la Oferta';
$app_list_strings['parent_type_display']['Quotes'] = 'Presupuesto';
$app_list_strings['parent_type_display']['Bugs'] = 'Incidencias';
$app_list_strings['parent_type_display']['Cases'] = 'Caso';
$app_list_strings['parent_type_display']['Leads'] = 'Lead';
$app_list_strings['parent_type_display']['Project'] = 'Proyecto';
$app_list_strings['parent_type_display']['ProjectTask'] = 'Tarea de Proyecto';
$app_list_strings['parent_type_display']['Prospects'] = 'Público Objetivo';
$app_list_strings['parent_type_display']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['parent_type_display']['Notes'] = 'Nota';
$app_list_strings['parent_type_display']['PurchasedLineItems'] = 'Elemento comprado';
$app_list_strings['parent_type_display']['Purchases'] = 'Compra';
$app_list_strings['parent_type_display']['Escalations'] = 'Escalada';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_record_type_display_notes.php

// created: 2022-11-23 10:51:57
$app_list_strings['record_type_display_notes']['Accounts'] = 'Cuenta';
$app_list_strings['record_type_display_notes']['Contacts'] = 'Contacto';
$app_list_strings['record_type_display_notes']['Opportunities'] = 'Cotización';
$app_list_strings['record_type_display_notes']['Tasks'] = 'Tarea';
$app_list_strings['record_type_display_notes']['ProductTemplates'] = 'Catálogo de Productos';
$app_list_strings['record_type_display_notes']['Quotes'] = 'Presupuesto';
$app_list_strings['record_type_display_notes']['Products'] = 'Línea de la Oferta';
$app_list_strings['record_type_display_notes']['Contracts'] = 'Contrato';
$app_list_strings['record_type_display_notes']['Emails'] = 'Correo electrónico';
$app_list_strings['record_type_display_notes']['Bugs'] = 'Incidencia';
$app_list_strings['record_type_display_notes']['Project'] = 'Proyecto';
$app_list_strings['record_type_display_notes']['ProjectTask'] = 'Tarea de Proyecto';
$app_list_strings['record_type_display_notes']['Prospects'] = 'Público Objetivo';
$app_list_strings['record_type_display_notes']['Cases'] = 'Caso';
$app_list_strings['record_type_display_notes']['Leads'] = 'Cliente Potencial';
$app_list_strings['record_type_display_notes']['Meetings'] = 'Reunión';
$app_list_strings['record_type_display_notes']['Calls'] = 'Llamada';
$app_list_strings['record_type_display_notes']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['record_type_display_notes']['PurchasedLineItems'] = 'Elemento comprado';
$app_list_strings['record_type_display_notes']['Purchases'] = 'Compra';
$app_list_strings['record_type_display_notes']['Escalations'] = 'Escalada';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_moduleList.php

// created: 2022-11-23 10:51:57
$app_list_strings['moduleList']['Home'] = 'Inicio';
$app_list_strings['moduleList']['Contacts'] = 'Contactos';
$app_list_strings['moduleList']['Accounts'] = 'Clientes y Prospectos';
$app_list_strings['moduleList']['Opportunities'] = 'Cotizaciones';
$app_list_strings['moduleList']['Cases'] = 'Casos';
$app_list_strings['moduleList']['Notes'] = 'Notas';
$app_list_strings['moduleList']['Calls'] = 'Llamadas';
$app_list_strings['moduleList']['Emails'] = 'Correos electrónicos';
$app_list_strings['moduleList']['Meetings'] = 'Reuniones';
$app_list_strings['moduleList']['Tasks'] = 'Tareas';
$app_list_strings['moduleList']['Calendar'] = 'Calendario';
$app_list_strings['moduleList']['Leads'] = 'Leads';
$app_list_strings['moduleList']['Currencies'] = 'Monedas';
$app_list_strings['moduleList']['Contracts'] = 'Contratos';
$app_list_strings['moduleList']['Quotes'] = 'Presupuestos';
$app_list_strings['moduleList']['Products'] = 'Elementos presupuestados';
$app_list_strings['moduleList']['WebLogicHooks'] = 'Web Logic Hooks';
$app_list_strings['moduleList']['ProductCategories'] = 'Categorías de Producto';
$app_list_strings['moduleList']['ProductTypes'] = 'Tipos de Producto';
$app_list_strings['moduleList']['ProductTemplates'] = 'Catálogo de Productos';
$app_list_strings['moduleList']['ProductBundles'] = 'Juegos de Productos';
$app_list_strings['moduleList']['ProductBundleNotes'] = 'Notas sobre los juegos de productos';
$app_list_strings['moduleList']['Reports'] = 'Informes';
$app_list_strings['moduleList']['Forecasts'] = 'Previsiones';
$app_list_strings['moduleList']['ForecastWorksheets'] = 'Hojas de Previsión';
$app_list_strings['moduleList']['ForecastManagerWorksheets'] = 'Hojas de trabajo para el administrador de previsión';
$app_list_strings['moduleList']['VisualPipeline'] = 'Canalización visual';
$app_list_strings['moduleList']['ConsoleConfiguration'] = 'Configuración de la consola';
$app_list_strings['moduleList']['SugarLive'] = 'SugarLive';
$app_list_strings['moduleList']['Quotas'] = 'Cuotas';
$app_list_strings['moduleList']['Teams'] = 'Equipos';
$app_list_strings['moduleList']['TeamNotices'] = 'Noticias de Equipo';
$app_list_strings['moduleList']['Manufacturers'] = 'Fabricantes';
$app_list_strings['moduleList']['Activities'] = 'Actividades';
$app_list_strings['moduleList']['Comments'] = 'Comentarios';
$app_list_strings['moduleList']['Subscriptions'] = 'Subscripciones';
$app_list_strings['moduleList']['Bugs'] = 'Incidencias';
$app_list_strings['moduleList']['Feeds'] = 'RSS';
$app_list_strings['moduleList']['iFrames'] = 'Mis Sitios';
$app_list_strings['moduleList']['TimePeriods'] = 'Períodos de Tiempo';
$app_list_strings['moduleList']['TaxRates'] = 'Tipos de Impuestos';
$app_list_strings['moduleList']['ContractTypes'] = 'Tipos de Contrato';
$app_list_strings['moduleList']['Schedulers'] = 'Planificadores';
$app_list_strings['moduleList']['Project'] = 'Proyectos';
$app_list_strings['moduleList']['ProjectTask'] = 'Tareas de Proyecto';
$app_list_strings['moduleList']['Campaigns'] = 'Campañas';
$app_list_strings['moduleList']['CampaignLog'] = 'Registro de Campañas';
$app_list_strings['moduleList']['CampaignTrackers'] = 'Seguimientos de Campaña';
$app_list_strings['moduleList']['Documents'] = 'Documentos';
$app_list_strings['moduleList']['DocumentRevisions'] = 'Revisiones de documentos';
$app_list_strings['moduleList']['Connectors'] = 'Conectores';
$app_list_strings['moduleList']['Notifications'] = 'Notificaciones';
$app_list_strings['moduleList']['Sync'] = 'Sincronización';
$app_list_strings['moduleList']['HintAccountsets'] = 'HintAccountsets';
$app_list_strings['moduleList']['HintNotificationTargets'] = 'HintNotificationTargets';
$app_list_strings['moduleList']['HintNewsNotifications'] = 'HintNewsNotifications';
$app_list_strings['moduleList']['HintEnrichFieldConfigs'] = 'HintEnrichFieldConfigs';
$app_list_strings['moduleList']['ExternalUsers'] = 'Usuarios externos';
$app_list_strings['moduleList']['ReportMaker'] = 'Informes avanzados';
$app_list_strings['moduleList']['DataSets'] = 'Formatos de datos';
$app_list_strings['moduleList']['CustomQueries'] = 'Consultas Personalizadas';
$app_list_strings['moduleList']['pmse_Inbox'] = 'Procesos';
$app_list_strings['moduleList']['pmse_Project'] = 'Definiciones de procesos';
$app_list_strings['moduleList']['pmse_Business_Rules'] = 'Normas empresariales de procesos';
$app_list_strings['moduleList']['pmse_Emails_Templates'] = 'Plantillas de email de procesos';
$app_list_strings['moduleList']['BusinessCenters'] = 'Centros de negocios';
$app_list_strings['moduleList']['Shifts'] = 'Turnos';
$app_list_strings['moduleList']['ShiftExceptions'] = 'Excepciones de turnos';
$app_list_strings['moduleList']['Purchases'] = 'Compras';
$app_list_strings['moduleList']['PurchasedLineItems'] = 'Elementos comprados';
$app_list_strings['moduleList']['MobileDevices'] = 'DispositivosMóviles';
$app_list_strings['moduleList']['PushNotifications'] = 'NotificacionesPush';
$app_list_strings['moduleList']['Escalations'] = 'Escaladas';
$app_list_strings['moduleList']['DocumentTemplates'] = 'Plantillas de documentos';
$app_list_strings['moduleList']['DocumentMerges'] = 'Fusiones de documentos';
$app_list_strings['moduleList']['CloudDrivePaths'] = 'Rutas de Cloud Drive';
$app_list_strings['moduleList']['WorkFlow'] = 'Definiciones de Workflow';
$app_list_strings['moduleList']['EAPM'] = 'Cuentas externas';
$app_list_strings['moduleList']['Worksheet'] = 'Hoja de Trabajo';
$app_list_strings['moduleList']['Users'] = 'Usuarios';
$app_list_strings['moduleList']['Employees'] = 'Empleados';
$app_list_strings['moduleList']['Administration'] = 'Administración';
$app_list_strings['moduleList']['ACLRoles'] = 'Roles';
$app_list_strings['moduleList']['InboundEmail'] = 'Correo Entrante';
$app_list_strings['moduleList']['Releases'] = 'Publicaciones';
$app_list_strings['moduleList']['Prospects'] = 'Público Objetivo';
$app_list_strings['moduleList']['Queues'] = 'Colas';
$app_list_strings['moduleList']['EmailMarketing'] = 'Marketing por Email';
$app_list_strings['moduleList']['EmailTemplates'] = 'Plantillas de Email';
$app_list_strings['moduleList']['SNIP'] = 'Email Archiving';
$app_list_strings['moduleList']['ProspectLists'] = 'Listas de Público Objetivo';
$app_list_strings['moduleList']['SavedSearch'] = 'Búsquedas guardadas';
$app_list_strings['moduleList']['UpgradeWizard'] = 'Asistente de Actualizaciones';
$app_list_strings['moduleList']['Trackers'] = 'Monitorizaciones';
$app_list_strings['moduleList']['TrackerPerfs'] = 'Rendimiento de Monitorización';
$app_list_strings['moduleList']['TrackerSessions'] = 'Sesiones de Monitorización';
$app_list_strings['moduleList']['TrackerQueries'] = 'Consultas de Monitorización';
$app_list_strings['moduleList']['FAQ'] = 'Preguntas frecuentes';
$app_list_strings['moduleList']['Newsletters'] = 'Boletines de Noticias';
$app_list_strings['moduleList']['SugarFavorites'] = 'Favoritos';
$app_list_strings['moduleList']['PdfManager'] = 'Gestor de PDF';
$app_list_strings['moduleList']['DataArchiver'] = 'Archivador de datos';
$app_list_strings['moduleList']['ArchiveRuns'] = 'Ejecuciones del archivo';
$app_list_strings['moduleList']['OAuthKeys'] = 'Claves del Consumidor OAuth';
$app_list_strings['moduleList']['OAuthTokens'] = 'Tokens de OAuth';
$app_list_strings['moduleList']['Filters'] = 'Filtros';
$app_list_strings['moduleList']['UserSignatures'] = 'Firmas de correo electrónico';
$app_list_strings['moduleList']['Shippers'] = 'Proveedores de Transporte';
$app_list_strings['moduleList']['Styleguide'] = 'Guía de Estilo';
$app_list_strings['moduleList']['Feedbacks'] = 'Comentarios';
$app_list_strings['moduleList']['Tags'] = 'Etiquetas';
$app_list_strings['moduleList']['Categories'] = 'Categorías';
$app_list_strings['moduleList']['Dashboards'] = 'Cuadros de mando';
$app_list_strings['moduleList']['OutboundEmail'] = 'Configuración de correo electrónico';
$app_list_strings['moduleList']['EmailParticipants'] = 'Participantes por correo electrónico';
$app_list_strings['moduleList']['DataPrivacy'] = 'Privacidad de datos';
$app_list_strings['moduleList']['ReportSchedules'] = 'Planificación de informes';
$app_list_strings['moduleList']['CommentLog'] = 'Registro de comentarios';
$app_list_strings['moduleList']['Holidays'] = 'Festivos';
$app_list_strings['moduleList']['ChangeTimers'] = 'Cambiar temporizadores';
$app_list_strings['moduleList']['Metrics'] = 'Métricas';
$app_list_strings['moduleList']['Messages'] = 'Mensajes';
$app_list_strings['moduleList']['Audit'] = 'Auditoría';
$app_list_strings['moduleList']['RevenueLineItems'] = 'Revenue Line Items';
$app_list_strings['moduleList']['DocuSignEnvelopes'] = 'Sobres de DocuSign';
$app_list_strings['moduleList']['Geocode'] = 'Geocodificar';
$app_list_strings['moduleList']['Library'] = 'Biblioteca';
$app_list_strings['moduleList']['EmailAddresses'] = 'Dirección de Correo electrónico';
$app_list_strings['moduleList']['Words'] = 'Palabras';
$app_list_strings['moduleList']['Sugar_Favorites'] = 'Favoritos';
$app_list_strings['moduleList']['KBDocuments'] = 'Base de conocimientos';
$app_list_strings['moduleList']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['moduleList']['KBArticles'] = 'Artículo de Base de Conocimiento';
$app_list_strings['moduleList']['KBContentTemplates'] = 'Plantilla de la base de conocimientos';
$app_list_strings['moduleList']['EmbeddedFiles'] = 'Archivos incrustados';
$app_list_strings['moduleList']['sasa_vehiculos'] = 'Líneas de vehículos';
$app_list_strings['moduleList']['sasa_Unidad_de_Negocio'] = 'Unidades de Negocio';
$app_list_strings['moduleList']['sasa_Puntos_de_Ventas'] = 'Puntos de Atención';
$app_list_strings['moduleList']['sasa_Marcas'] = 'Marcas';
$app_list_strings['moduleList']['sasa_Companias'] = 'Compañias';
$app_list_strings['moduleList']['SASA_UnidadesdeNegporClieyPro'] = 'Unidades de Neg. por Clientes y Pros';
$app_list_strings['moduleList']['SASA_UnidadNegClienteProspect'] = 'Unidades de Neg. por Clientes y Prospectos';
$app_list_strings['moduleList']['ops_Backups'] = 'Backups';
$app_list_strings['moduleList']['SASA_Habeas_Data'] = 'Habeas Data';
$app_list_strings['moduleList']['sasa_Tipificacion_de_Casos'] = 'Tipificación de Casos';
$app_list_strings['moduleList']['sasa_Paises'] = 'Países';
$app_list_strings['moduleList']['sasa_Departamentos'] = 'Departamentos';
$app_list_strings['moduleList']['sasa_Municipios'] = 'Ciudades';
$app_list_strings['moduleList']['SASA_Campanas'] = 'Campañas';
$app_list_strings['moduleList']['sasa_sasa_centrocostos_c'] = 'Centros De Costos';
$app_list_strings['moduleList']['sasa_CentrosDeCostos'] = 'Centros De Costos';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_record_type_display.php

// created: 2022-11-23 10:51:57
$app_list_strings['record_type_display'][''] = '';
$app_list_strings['record_type_display']['Accounts'] = 'Cuenta';
$app_list_strings['record_type_display']['Opportunities'] = 'Cotización';
$app_list_strings['record_type_display']['Cases'] = 'Caso';
$app_list_strings['record_type_display']['Leads'] = 'Cliente Potencial';
$app_list_strings['record_type_display']['Contacts'] = 'Contacto';
$app_list_strings['record_type_display']['Products'] = 'Línea de la Oferta';
$app_list_strings['record_type_display']['Quotes'] = 'Presupuesto';
$app_list_strings['record_type_display']['Bugs'] = 'Incidencia';
$app_list_strings['record_type_display']['Project'] = 'Proyecto';
$app_list_strings['record_type_display']['Prospects'] = 'Público Objetivo';
$app_list_strings['record_type_display']['ProjectTask'] = 'Tarea de Proyecto';
$app_list_strings['record_type_display']['Tasks'] = 'Tarea';
$app_list_strings['record_type_display']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['record_type_display']['Notes'] = 'Nota';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_moduleListSingular.php

// created: 2022-11-23 10:51:57
$app_list_strings['moduleListSingular']['Home'] = 'Inicio';
$app_list_strings['moduleListSingular']['Dashboards'] = 'Cuadro de mando';
$app_list_strings['moduleListSingular']['Styleguide'] = 'Guía de Estilo';
$app_list_strings['moduleListSingular']['Contacts'] = 'Contacto';
$app_list_strings['moduleListSingular']['Accounts'] = 'Cliente y Prospecto';
$app_list_strings['moduleListSingular']['Opportunities'] = 'Cotización';
$app_list_strings['moduleListSingular']['Cases'] = 'Caso';
$app_list_strings['moduleListSingular']['Notes'] = 'Nota';
$app_list_strings['moduleListSingular']['Calls'] = 'Llamada';
$app_list_strings['moduleListSingular']['Emails'] = 'Correo electrónico';
$app_list_strings['moduleListSingular']['Meetings'] = 'Reunión';
$app_list_strings['moduleListSingular']['Tasks'] = 'Tarea';
$app_list_strings['moduleListSingular']['Calendar'] = 'Calendario';
$app_list_strings['moduleListSingular']['Leads'] = 'Lead';
$app_list_strings['moduleListSingular']['Manufacturers'] = 'Fabricante';
$app_list_strings['moduleListSingular']['VisualPipeline'] = 'Canalización visual';
$app_list_strings['moduleListSingular']['ConsoleConfiguration'] = 'Configuracióndelaconsola';
$app_list_strings['moduleListSingular']['MobileDevices'] = 'DispositivoMóvil';
$app_list_strings['moduleListSingular']['SugarLive'] = 'SugarLive';
$app_list_strings['moduleListSingular']['Contracts'] = 'Contrato';
$app_list_strings['moduleListSingular']['Quotes'] = 'Presupuesto';
$app_list_strings['moduleListSingular']['Products'] = 'Línea de la Oferta';
$app_list_strings['moduleListSingular']['ProductCategories'] = 'Categoría de Producto';
$app_list_strings['moduleListSingular']['ProductBundles'] = 'Juego de Producto';
$app_list_strings['moduleListSingular']['ProductBundleNotes'] = 'Notas sobre el juego de producto';
$app_list_strings['moduleListSingular']['RevenueLineItems'] = 'Revenue Line Item';
$app_list_strings['moduleListSingular']['WebLogicHooks'] = 'Web Logic Hook';
$app_list_strings['moduleListSingular']['Reports'] = 'Informe';
$app_list_strings['moduleListSingular']['Forecasts'] = 'Previsión';
$app_list_strings['moduleListSingular']['ForecastWorksheets'] = 'Hoja de Previsión';
$app_list_strings['moduleListSingular']['ForecastManagerWorksheets'] = 'Hoja de trabajo para el administrador de previsión';
$app_list_strings['moduleListSingular']['Quotas'] = 'Cuota';
$app_list_strings['moduleListSingular']['Teams'] = 'Equipo';
$app_list_strings['moduleListSingular']['TeamNotices'] = 'Notificación del Equipo';
$app_list_strings['moduleListSingular']['Activities'] = 'Actividad';
$app_list_strings['moduleListSingular']['ActivityStream'] = 'Flujo de actividad';
$app_list_strings['moduleListSingular']['Bugs'] = 'Incidencia';
$app_list_strings['moduleListSingular']['Feeds'] = 'RSS';
$app_list_strings['moduleListSingular']['iFrames'] = 'Mis Sitios';
$app_list_strings['moduleListSingular']['TimePeriods'] = 'Período de Tiempo';
$app_list_strings['moduleListSingular']['TaxRates'] = 'Tipo de Impuesto';
$app_list_strings['moduleListSingular']['ContractTypes'] = 'Tipo de Contrato';
$app_list_strings['moduleListSingular']['Schedulers'] = 'Planificador';
$app_list_strings['moduleListSingular']['Campaigns'] = 'Campaña';
$app_list_strings['moduleListSingular']['CampaignLog'] = 'Registro de Campañas';
$app_list_strings['moduleListSingular']['Project'] = 'Proyecto';
$app_list_strings['moduleListSingular']['ProjectTask'] = 'Tarea de Proyecto';
$app_list_strings['moduleListSingular']['Prospects'] = 'Público Objetivo';
$app_list_strings['moduleListSingular']['ProspectLists'] = 'Lista de Público Objetivo';
$app_list_strings['moduleListSingular']['CampaignTrackers'] = 'Seguimiento de Campaña';
$app_list_strings['moduleListSingular']['Documents'] = 'Documento';
$app_list_strings['moduleListSingular']['DocumentRevisions'] = 'Revisión del Documento';
$app_list_strings['moduleListSingular']['Connectors'] = 'Conector';
$app_list_strings['moduleListSingular']['Notifications'] = 'Notificación';
$app_list_strings['moduleListSingular']['Sync'] = 'Sincronización';
$app_list_strings['moduleListSingular']['PdfManager'] = 'Gestor de PDF';
$app_list_strings['moduleListSingular']['DataArchiver'] = 'Archivador de datos';
$app_list_strings['moduleListSingular']['ArchiveRuns'] = 'Ejecuciones del archivo';
$app_list_strings['moduleListSingular']['ExternalUsers'] = 'Usuario externo';
$app_list_strings['moduleListSingular']['ReportMaker'] = ' Informe avanzado';
$app_list_strings['moduleListSingular']['DataSets'] = 'Formato de Datos';
$app_list_strings['moduleListSingular']['CustomQueries'] = 'Consulta Personalizada';
$app_list_strings['moduleListSingular']['pmse_Inbox'] = 'Proceso';
$app_list_strings['moduleListSingular']['pmse_Project'] = 'Definición de proceso';
$app_list_strings['moduleListSingular']['pmse_Business_Rules'] = 'Norma empresarial de procesos';
$app_list_strings['moduleListSingular']['pmse_Emails_Templates'] = 'Plantilla de correo electrónico de procesos';
$app_list_strings['moduleListSingular']['BusinessCenters'] = 'Centro de negocios';
$app_list_strings['moduleListSingular']['Shifts'] = 'Turno';
$app_list_strings['moduleListSingular']['ShiftExceptions'] = 'Excepciones de turnos';
$app_list_strings['moduleListSingular']['Purchases'] = 'Compra';
$app_list_strings['moduleListSingular']['PurchasedLineItems'] = 'Elemento comprado';
$app_list_strings['moduleListSingular']['PushNotifications'] = 'NotificaciónPush';
$app_list_strings['moduleListSingular']['Escalations'] = 'Escalada';
$app_list_strings['moduleListSingular']['DocumentTemplates'] = 'Plantilla de documento';
$app_list_strings['moduleListSingular']['DocumentMerges'] = 'Fusión de documentos';
$app_list_strings['moduleListSingular']['CloudDrivePaths'] = 'Ruta de Cloud Drive';
$app_list_strings['moduleListSingular']['WorkFlow'] = 'Flujo de actividad';
$app_list_strings['moduleListSingular']['EAPM'] = 'Cuenta externa';
$app_list_strings['moduleListSingular']['Worksheet'] = 'Hoja de Trabajo';
$app_list_strings['moduleListSingular']['Users'] = 'Usuario';
$app_list_strings['moduleListSingular']['SugarFavorites'] = 'SugarFavorites';
$app_list_strings['moduleListSingular']['Employees'] = 'Empleado';
$app_list_strings['moduleListSingular']['Administration'] = 'Administración';
$app_list_strings['moduleListSingular']['ACLRoles'] = 'Rol';
$app_list_strings['moduleListSingular']['InboundEmail'] = 'Correo Entrante';
$app_list_strings['moduleListSingular']['Releases'] = 'Publicación';
$app_list_strings['moduleListSingular']['Queues'] = 'Cola';
$app_list_strings['moduleListSingular']['EmailMarketing'] = 'Marketing por Email';
$app_list_strings['moduleListSingular']['EmailTemplates'] = 'Plantilla de Email';
$app_list_strings['moduleListSingular']['SNIP'] = 'Email Archiving';
$app_list_strings['moduleListSingular']['SavedSearch'] = 'Búsqueda guardada';
$app_list_strings['moduleListSingular']['UpgradeWizard'] = 'Asistente de Actualizaciones';
$app_list_strings['moduleListSingular']['Trackers'] = 'Monitorización';
$app_list_strings['moduleListSingular']['TrackerPerfs'] = 'Rendimiento de Monitorización';
$app_list_strings['moduleListSingular']['TrackerSessions'] = 'Sesión de seguimiento';
$app_list_strings['moduleListSingular']['TrackerQueries'] = 'Consultas de seguimiento';
$app_list_strings['moduleListSingular']['FAQ'] = 'Preguntas frecuentes';
$app_list_strings['moduleListSingular']['Newsletters'] = 'Boletín de Noticias';
$app_list_strings['moduleListSingular']['OAuthKeys'] = 'Clave OAuth del consumidor';
$app_list_strings['moduleListSingular']['OAuthTokens'] = 'Identificador OAuth';
$app_list_strings['moduleListSingular']['Filters'] = 'Filtro';
$app_list_strings['moduleListSingular']['Comments'] = 'Comentario';
$app_list_strings['moduleListSingular']['CommentLog'] = 'Registro de comentarios';
$app_list_strings['moduleListSingular']['Currencies'] = 'Moneda';
$app_list_strings['moduleListSingular']['ProductTemplates'] = 'Product';
$app_list_strings['moduleListSingular']['ProductTypes'] = 'Tipo de Producto';
$app_list_strings['moduleListSingular']['Shippers'] = 'Proveedor de transporte';
$app_list_strings['moduleListSingular']['Subscriptions'] = 'Suscripción';
$app_list_strings['moduleListSingular']['UserSignatures'] = 'Firma de correo electrónico';
$app_list_strings['moduleListSingular']['Feedbacks'] = 'Comentarios';
$app_list_strings['moduleListSingular']['Tags'] = 'Etiqueta';
$app_list_strings['moduleListSingular']['Categories'] = 'Categoría';
$app_list_strings['moduleListSingular']['OutboundEmail'] = 'Configuración de correo electrónico';
$app_list_strings['moduleListSingular']['EmailParticipants'] = 'Participante por correo electrónico';
$app_list_strings['moduleListSingular']['DataPrivacy'] = 'Privacidad de datos';
$app_list_strings['moduleListSingular']['ReportSchedules'] = 'Planificación de informes';
$app_list_strings['moduleListSingular']['Holidays'] = 'Festivo';
$app_list_strings['moduleListSingular']['ChangeTimers'] = 'Cambiar temporizador';
$app_list_strings['moduleListSingular']['Metrics'] = 'Métrica';
$app_list_strings['moduleListSingular']['Messages'] = 'Mensaje';
$app_list_strings['moduleListSingular']['Audit'] = 'Auditoría';
$app_list_strings['moduleListSingular']['DocuSignEnvelopes'] = 'DocuSignEnvelope';
$app_list_strings['moduleListSingular']['HintAccountsets'] = 'HintAccountsets';
$app_list_strings['moduleListSingular']['HintNotificationTargets'] = 'HintNotificationTargets';
$app_list_strings['moduleListSingular']['HintNewsNotifications'] = 'HintNewsNotifications';
$app_list_strings['moduleListSingular']['HintEnrichFieldConfigs'] = 'HintEnrichFieldConfigs';
$app_list_strings['moduleListSingular']['Geocode'] = 'Geocodificar';
$app_list_strings['moduleListSingular']['Library'] = 'Biblioteca';
$app_list_strings['moduleListSingular']['EmailAddresses'] = 'Dirección de Correo electrónico';
$app_list_strings['moduleListSingular']['Words'] = 'Palabra';
$app_list_strings['moduleListSingular']['Sugar_Favorites'] = 'Favorito';
$app_list_strings['moduleListSingular']['KBDocuments'] = 'Base de conocimientos';
$app_list_strings['moduleListSingular']['KBContents'] = 'Artículo de Base de Conocimiento';
$app_list_strings['moduleListSingular']['KBArticles'] = 'Artículo de Base de Conocimiento';
$app_list_strings['moduleListSingular']['KBContentTemplates'] = 'Plantilla de la base de conocimientos';
$app_list_strings['moduleListSingular']['EmbeddedFiles'] = 'Archivo incrustado';
$app_list_strings['moduleListSingular']['sasa_vehiculos'] = 'Líneas de vehículos';
$app_list_strings['moduleListSingular']['sasa_Unidad_de_Negocio'] = 'Unidad de Negocio';
$app_list_strings['moduleListSingular']['sasa_Puntos_de_Ventas'] = 'Punto de Atención';
$app_list_strings['moduleListSingular']['sasa_Marcas'] = 'Marca';
$app_list_strings['moduleListSingular']['sasa_Companias'] = 'Compañia';
$app_list_strings['moduleListSingular']['SASA_UnidadesdeNegporClieyPro'] = 'Unidad de Neg. por Cliente y Prospec';
$app_list_strings['moduleListSingular']['SASA_UnidadNegClienteProspect'] = 'Unidad de Neg. por Cliente y Prospecto';
$app_list_strings['moduleListSingular']['ops_Backups'] = 'Backup';
$app_list_strings['moduleListSingular']['SASA_Habeas_Data'] = 'Habeas Data';
$app_list_strings['moduleListSingular']['sasa_Tipificacion_de_Casos'] = 'Tipificación de Casos';
$app_list_strings['moduleListSingular']['sasa_Paises'] = 'País';
$app_list_strings['moduleListSingular']['sasa_Departamentos'] = 'Departamento';
$app_list_strings['moduleListSingular']['sasa_Municipios'] = 'Ciudad';
$app_list_strings['moduleListSingular']['SASA_Campanas'] = 'Campaña';
$app_list_strings['moduleListSingular']['sasa_sasa_centrocostos_c'] = 'Centros De Costos';
$app_list_strings['moduleListSingular']['sasa_CentrosDeCostos'] = 'Centros De Costos';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipodecaja_c_list.php

 // created: 2022-11-23 11:18:13

$app_list_strings['sasa_tipodecaja_c_list']=array (
  358 => 'MECÁNICA',
  357 => 'AUTOMÁTICA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipodeservicio_c_list.php

 // created: 2022-11-23 11:21:29

$app_list_strings['sasa_tipodeservicio_c_list']=array (
  'U' => 'PÚBLICO',
  'P' => 'PARTICULAR',
  'O' => 'OFICIAL',
  'E' => 'ESPECIAL',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipocotizacion_c_list.php

 // created: 2022-11-23 15:12:13

$app_list_strings['sasa_tipocotizacion_c_list']=array (
  110 => 'VEHÍCULOS NUEVOS NISSAN',
  120 => 'MAQUINARIA',
  130 => 'REPUESTOS',
  153 => 'FINANCIERAS',
  210 => 'TALLER AUTOMOTRIZ',
  220 => 'TALLER MAQUINARIA',
  310 => 'VEHÍCULOS USADOS',
  315 => 'TRÁMITES',
  340 => 'VEHÍCULOS NUEVOS COM - ZNA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_estadopedidorep_c_list.php

 // created: 2022-11-23 15:35:55

$app_list_strings['sasa_estadopedidorep_c_list']=array (
  'B' => 'GRABADO',
  'BK' => 'BACK ORDER',
  'C' => 'CONFIRMADO',
  'A' => 'ANULADO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_estadopedido_c_list.php

 // created: 2022-11-28 20:36:16

$app_list_strings['sasa_estadopedido_c_list']=array (
  'P' => 'PEDIDO',
  'R' => 'RESERVADO',
  'E' => 'LISTA DE ESPERA',
  'A' => 'ASIGNADO',
  'F' => 'FACTURADO',
  'D' => 'DESTRATE',
  'K' => 'FÁBRICA',
  'B' => 'BLOQUEADO',
  'S' => 'DESASIGNADO',
  'X' => 'CANCELADO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_cotizacionprepagado_c_list.php

 // created: 2023-01-21 18:31:56

$app_list_strings['sasa_cotizacionprepagado_c_list']=array (
  'S' => 'SI',
  'N' => 'NO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_tipocombustible_c_list.php

 // created: 2023-01-23 20:53:26

$app_list_strings['sasa_tipocombustible_c_list']=array (
  1 => 'GASOLINA',
  2 => 'GAS NATURAL VEHICULAR (GNV)',
  3 => 'DIESEL',
  4 => 'GAS - GASOLINA',
  5 => 'ELECTRICO',
  6 => 'HIDROGENO',
  7 => 'ETANOL',
  8 => 'BIODIESEL',
  9 => 'GLP',
  10 => 'GASOLINA - ELECTRICO',
  11 => 'DIESEL - ELECTRICO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_sucursal_c_list.php

 // created: 2023-01-25 23:18:37

$app_list_strings['sasa_sucursal_c_list']=array (
  '' => '',
  1 => 'Bogota',
  2 => 'Barranquilla',
  3 => 'Cali',
  4 => 'Valledupar',
  5 => 'Medellin',
  6 => 'Cartagena',
  7 => 'Pasto',
  8 => 'Cucuta',
  9 => 'Tunja',
  10 => 'Santa Marta',
  11 => 'GerenciaNacional',
  12 => 'Villavicencio',
  13 => 'Bucaramanga',
  14 => 'Ibague',
  15 => 'Neiva',
  16 => 'Pereira',
  17 => 'Armenia',
  18 => 'Popayan',
  19 => 'Yopal',
  20 => 'Buenaventura',
  21 => 'Barrancabermeja',
  22 => 'Monteria',
  23 => 'PUERTO GAITAN R',
  24 => 'Sincelejo',
  25 => 'GIRARDOT',
  26 => 'RIOHACHA',
  27 => 'Tulua',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_estadocotizacion_c_list.php

 // created: 2023-02-01 23:08:38

$app_list_strings['sasa_estadocotizacion_c_list']=array (
  'AB' => 'ABIERTA',
  'FP' => 'FACTURADA PARCIAL',
  'FT' => 'FACTURADA TOTAL',
  'PA' => 'PEDIDO ANULADO',
  'PF' => 'PENDIENTE FACTURAR',
  'PP' => 'PEDIDO PENDIENTE',
  'RR' => 'REEMPLAZADA',
  'VP' => 'VENTA PERDIDA',
  'PD' => 'PENDIENTE DECISION',
  'I' => 'INACTIVA',
  'N' => 'NO APROBADA',
  'P' => 'CON PEDIDO',
  'S' => 'SOLICITADA',
  'X' => 'ANULADA',
  'F' => 'FACTURADA',
  'D' => 'VENTA DEVUELTA',
  'V' => 'VENCIDA',
  'A' => 'APROBADA',
  'C' => 'CERRADA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sales_stage_dom.php

 // created: 2023-02-07 21:24:51

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospección',
  'Qualification' => 'Calificación',
  'Needs Analysis' => 'Análisis de necesidades',
  'Value Proposition' => 'Propuesta de valor',
  'Id. Decision Makers' => 'Identificación de responsables',
  'Perception Analysis' => 'Análisis de percepción',
  'Proposal/Price Quote' => 'Cotización',
  'Negotiation/Review' => 'Negociación/Revisión',
  'Closed Won' => 'Cerrado',
  'Closed Lost' => 'Perdida',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_moduleIconList.php

// created: 2023-02-08 11:06:12
$app_list_strings['moduleIconList']['Home'] = 'In';
$app_list_strings['moduleIconList']['Contacts'] = 'Co';
$app_list_strings['moduleIconList']['Accounts'] = 'Cy';
$app_list_strings['moduleIconList']['Opportunities'] = 'Co';
$app_list_strings['moduleIconList']['Cases'] = 'Ca';
$app_list_strings['moduleIconList']['Notes'] = 'No';
$app_list_strings['moduleIconList']['Calls'] = 'Ll';
$app_list_strings['moduleIconList']['Emails'] = 'Ce';
$app_list_strings['moduleIconList']['Meetings'] = 'Re';
$app_list_strings['moduleIconList']['Tasks'] = 'Ta';
$app_list_strings['moduleIconList']['Calendar'] = 'Ca';
$app_list_strings['moduleIconList']['Leads'] = 'Le';
$app_list_strings['moduleIconList']['Currencies'] = 'Mo';
$app_list_strings['moduleIconList']['Contracts'] = 'Co';
$app_list_strings['moduleIconList']['Quotes'] = 'Pr';
$app_list_strings['moduleIconList']['Products'] = 'Ep';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'WL';
$app_list_strings['moduleIconList']['ProductCategories'] = 'Cd';
$app_list_strings['moduleIconList']['ProductTypes'] = 'Td';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'Cd';
$app_list_strings['moduleIconList']['ProductBundles'] = 'Jd';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'Ns';
$app_list_strings['moduleIconList']['Reports'] = 'In';
$app_list_strings['moduleIconList']['Forecasts'] = 'Pr';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'Hd';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'Hd';
$app_list_strings['moduleIconList']['Quotas'] = 'Cu';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'Cv';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'Cd';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = 'Eq';
$app_list_strings['moduleIconList']['TeamNotices'] = 'Nd';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Fa';
$app_list_strings['moduleIconList']['Activities'] = 'Ac';
$app_list_strings['moduleIconList']['Comments'] = 'Co';
$app_list_strings['moduleIconList']['Subscriptions'] = 'Su';
$app_list_strings['moduleIconList']['Bugs'] = 'In';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'MS';
$app_list_strings['moduleIconList']['TimePeriods'] = 'Pd';
$app_list_strings['moduleIconList']['TaxRates'] = 'Td';
$app_list_strings['moduleIconList']['ContractTypes'] = 'Td';
$app_list_strings['moduleIconList']['Schedulers'] = 'Pl';
$app_list_strings['moduleIconList']['Project'] = 'Pr';
$app_list_strings['moduleIconList']['ProjectTask'] = 'Td';
$app_list_strings['moduleIconList']['Campaigns'] = 'Ca';
$app_list_strings['moduleIconList']['CampaignLog'] = 'Rd';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'Sd';
$app_list_strings['moduleIconList']['Documents'] = 'Do';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'Rd';
$app_list_strings['moduleIconList']['Connectors'] = 'Co';
$app_list_strings['moduleIconList']['Notifications'] = 'No';
$app_list_strings['moduleIconList']['Sync'] = 'Si';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'Ue';
$app_list_strings['moduleIconList']['ReportMaker'] = 'Ia';
$app_list_strings['moduleIconList']['DataSets'] = 'Fd';
$app_list_strings['moduleIconList']['CustomQueries'] = 'CP';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Pr';
$app_list_strings['moduleIconList']['pmse_Project'] = 'Dd';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'Ne';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'Cd';
$app_list_strings['moduleIconList']['Shifts'] = 'Tu';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'Ed';
$app_list_strings['moduleIconList']['Purchases'] = 'Co';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'Ec';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Di';
$app_list_strings['moduleIconList']['PushNotifications'] = 'No';
$app_list_strings['moduleIconList']['Escalations'] = 'Es';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'Pd';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'Fd';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'Rd';
$app_list_strings['moduleIconList']['WorkFlow'] = 'Dd';
$app_list_strings['moduleIconList']['EAPM'] = 'Ce';
$app_list_strings['moduleIconList']['Worksheet'] = 'Hd';
$app_list_strings['moduleIconList']['Users'] = 'Us';
$app_list_strings['moduleIconList']['Employees'] = 'Em';
$app_list_strings['moduleIconList']['Administration'] = 'Ad';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Ro';
$app_list_strings['moduleIconList']['InboundEmail'] = 'CE';
$app_list_strings['moduleIconList']['Releases'] = 'Pu';
$app_list_strings['moduleIconList']['Prospects'] = 'PO';
$app_list_strings['moduleIconList']['Queues'] = 'Co';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'Mp';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'Pd';
$app_list_strings['moduleIconList']['SNIP'] = 'EA';
$app_list_strings['moduleIconList']['ProspectLists'] = 'Ld';
$app_list_strings['moduleIconList']['SavedSearch'] = 'Bg';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'Ad';
$app_list_strings['moduleIconList']['Trackers'] = 'Mo';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'Rd';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'Sd';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'Cd';
$app_list_strings['moduleIconList']['FAQ'] = 'Pf';
$app_list_strings['moduleIconList']['Newsletters'] = 'Bd';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Fa';
$app_list_strings['moduleIconList']['PdfManager'] = 'Gd';
$app_list_strings['moduleIconList']['DataArchiver'] = 'Ad';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'Ed';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'Cd';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'Td';
$app_list_strings['moduleIconList']['Filters'] = 'Fi';
$app_list_strings['moduleIconList']['UserSignatures'] = 'Fd';
$app_list_strings['moduleIconList']['Shippers'] = 'Pd';
$app_list_strings['moduleIconList']['Styleguide'] = 'Gd';
$app_list_strings['moduleIconList']['Feedbacks'] = 'Co';
$app_list_strings['moduleIconList']['Tags'] = 'Et';
$app_list_strings['moduleIconList']['Categories'] = 'Ca';
$app_list_strings['moduleIconList']['Dashboards'] = 'Cd';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'Cd';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'Pp';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'Pd';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'Pd';
$app_list_strings['moduleIconList']['CommentLog'] = 'Rd';
$app_list_strings['moduleIconList']['Holidays'] = 'Fe';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'Ct';
$app_list_strings['moduleIconList']['Metrics'] = 'Mé';
$app_list_strings['moduleIconList']['Messages'] = 'Me';
$app_list_strings['moduleIconList']['Audit'] = 'Au';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'RL';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'Sd';
$app_list_strings['moduleIconList']['Geocode'] = 'Ge';
$app_list_strings['moduleIconList']['DRI_Workflows'] = 'Gi';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = 'Fd';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'Wd';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'Ad';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['Library'] = 'Bi';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'Dd';
$app_list_strings['moduleIconList']['Words'] = 'Pa';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Fa';
$app_list_strings['moduleIconList']['KBDocuments'] = 'Bd';
$app_list_strings['moduleIconList']['KBContents'] = 'Bd';
$app_list_strings['moduleIconList']['KBArticles'] = 'Ad';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'Pd';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'Ai';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_source_cases_list_c.php

 // created: 2023-05-10 16:23:44

$app_list_strings['source_cases_list_c']=array (
  '' => '',
  'A' => 'APP',
  'C' => 'CARTA',
  'E' => 'EMAIL',
  'V' => 'FORMATO VERBAL',
  'T' => 'LLAMADA',
  'S' => 'REDES SOCIALES',
  'W' => 'SITIO WEB',
  'P' => 'WHATSAPP',
  'R' => 'TUCARRO.COM',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_sasa_detalle_origen_c_list.php

 // created: 2023-05-19 14:00:45

$app_list_strings['sasa_detalle_origen_c_list']=array (
  '' => '',
  75 => 'AUTO SHOW',
  40 => 'AUTOFINANCIERA',
  6 => 'BASE DE DATOS ASESOR',
  45 => 'CARROYA',
  23 => 'CATÁLOGO',
  53 => 'CHAT',
  73 => 'CINE',
  7 => 'CLIENTE NISSAN',
  78 => 'COT. POSV CAMIONETAS',
  77 => 'COT. POSVENTA AUTOS',
  69 => 'COT. POSVENTA GOLD',
  76 => 'COT. POSVENTA SILVER',
  31 => 'COTIZACIÓN',
  22 => 'COTIZACIÓN',
  54 => 'COTIZACIÓN',
  20 => 'COTIZACIÓN',
  63 => 'COTIZACIÓN',
  61 => 'COTIZACIÓN',
  65 => 'COTIZACIÓN',
  28 => 'COTIZACIÓN',
  58 => 'COTIZACIÓN',
  67 => 'DANOS UN DIEZ POSVENTA',
  68 => 'DANOS UN DIEZ VENTA',
  15 => 'E-MAIL',
  72 => 'EL PAIS',
  83 => 'EVENTO NO DIGITAL',
  42 => 'FACEBOOK',
  87 => 'FINANCIERA',
  88 => 'FINESA',
  25 => 'INFORMACIÓN GENERAL',
  32 => 'INFORMACIÓN GENERAL',
  56 => 'INFORMACIÓN GENERAL',
  43 => 'INSTAGRAM',
  12 => 'INTERNET',
  34 => 'LINKEDIN',
  11 => 'LLAMADA',
  84 => 'NISSAN TALKS',
  50 => 'OLX',
  39 => 'OTRO',
  57 => 'OTRO',
  49 => 'PLAYA DE VENTAS',
  59 => 'POSVENTA',
  1 => 'PRENSA',
  80 => 'PREVENTA',
  71 => 'PREVENTA SENTRA',
  3 => 'RADIO',
  13 => 'REDES SOCIALES',
  8 => 'REFERIDO',
  60 => 'REPUESTOS',
  2 => 'REVISTAS',
  16 => 'SOL. COTIZACIÓN/CATALOGO',
  79 => 'TEST DRIVE',
  64 => 'TEST DRIVE',
  21 => 'TEST DRIVE',
  62 => 'TEST DRIVE',
  66 => 'TEST DRIVE',
  55 => 'TEST DRIVE',
  52 => 'TEST DRIVE',
  24 => 'TEST DRIVE',
  9 => 'TRABAJO DE CALLE',
  47 => 'TRABAJO DE CALLE',
  46 => 'TUCARRO.COM',
  4 => 'TV',
  44 => 'TWITTER',
  18 => 'UEFA',
  74 => 'VALLAS PARADEROS ETC',
  51 => 'VENTA DIRECTA',
  48 => 'VISITA CLIENTE',
  10 => 'VISITA VITRINA',
  81 => 'VITRINA MÓVIL',
  70 => 'VITRINA VIRTUAL',
  5 => 'VOLANTEO',
  27 => 'WEBINAR',
  89 => 'CAMPAÑA',
  14 => 'BASE DE DATOS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.Aseguradoras.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SASA1_sasa_seguradoras'] = 'Aseguradoras';
$app_list_strings['moduleListSingular']['SASA1_sasa_seguradoras'] = 'Aseguradora';
$app_list_strings['moduleIconList']['SASA1_sasa_seguradoras'] = 'As';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_prueba_list.php

 // created: 2023-06-05 21:47:18

$app_list_strings['prueba_list']=array (
  '' => '',
  1 => 'Uno',
  2 => 'Dos',
  3 => 'Tres',
  4 => 'Cuatro',
);
?>

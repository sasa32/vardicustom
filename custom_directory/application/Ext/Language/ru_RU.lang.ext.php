<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ru_RU.sugar_moduleList.php

 //created: 2020-01-31 00:19:11

$app_list_strings['moduleList']['RevenueLineItems']='Доходные продукты';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ru_RU.sugar_parent_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Сделка',
  'Products' => 'Продукт коммерческого предложения',
  'Quotes' => 'Коммерческое предложение',
  'Bugs' => 'Ошибки',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Project' => 'Проект',
  'ProjectTask' => 'Проектная задача',
  'Prospects' => 'Адресат',
  'KBContents' => 'База знаний',
  'Notes' => 'Примечание',
  'RevenueLineItems' => 'Доходные продукты',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ru_RU.sugar_record_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Контрагент',
  'Opportunities' => 'Сделка',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Contacts' => 'Контакты',
  'Products' => 'Продукт коммерческого предложения',
  'Quotes' => 'Коммерческое предложение',
  'Bugs' => 'Ошибка',
  'Project' => 'Проект',
  'Prospects' => 'Адресат',
  'ProjectTask' => 'Проектная задача',
  'Tasks' => 'Задача',
  'KBContents' => 'База знаний',
  'Notes' => 'Примечание',
  'RevenueLineItems' => 'Доходные продукты',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ru_RU.sugar_record_type_display_notes.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Opportunities' => 'Сделка',
  'Tasks' => 'Задача',
  'ProductTemplates' => 'Каталог продуктов',
  'Quotes' => 'Коммерческое предложение',
  'Products' => 'Продукт коммерческого предложения',
  'Contracts' => 'Контракт',
  'Emails' => 'E-mail',
  'Bugs' => 'Ошибка',
  'Project' => 'Проект',
  'ProjectTask' => 'Проектная задача',
  'Prospects' => 'Адресат',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Meetings' => 'Встреча',
  'Calls' => 'Звонок',
  'KBContents' => 'База знаний',
  'RevenueLineItems' => 'Доходные продукты',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ru_RU.sugar_moduleIconList.php

// created: 2023-02-08 11:06:13
$app_list_strings['moduleIconList']['Home'] = 'Гл';
$app_list_strings['moduleIconList']['Contacts'] = 'Ко';
$app_list_strings['moduleIconList']['Accounts'] = 'Ко';
$app_list_strings['moduleIconList']['Opportunities'] = 'Сд';
$app_list_strings['moduleIconList']['Cases'] = 'Об';
$app_list_strings['moduleIconList']['Notes'] = 'За';
$app_list_strings['moduleIconList']['Calls'] = 'Зв';
$app_list_strings['moduleIconList']['Emails'] = 'E-';
$app_list_strings['moduleIconList']['Meetings'] = 'Вс';
$app_list_strings['moduleIconList']['Tasks'] = 'За';
$app_list_strings['moduleIconList']['Calendar'] = 'Ка';
$app_list_strings['moduleIconList']['Leads'] = 'Пк';
$app_list_strings['moduleIconList']['Currencies'] = 'Ва';
$app_list_strings['moduleIconList']['Contracts'] = 'Ко';
$app_list_strings['moduleIconList']['Quotes'] = 'Кп';
$app_list_strings['moduleIconList']['Products'] = 'Пк';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'Вл';
$app_list_strings['moduleIconList']['ProductCategories'] = 'Кп';
$app_list_strings['moduleIconList']['ProductTypes'] = 'Тп';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'Кп';
$app_list_strings['moduleIconList']['ProductBundles'] = 'Сп';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'Зс';
$app_list_strings['moduleIconList']['Reports'] = 'От';
$app_list_strings['moduleIconList']['Forecasts'] = 'Пр';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'Пп';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'Пм';
$app_list_strings['moduleIconList']['Quotas'] = 'Кв';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'Вв';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'Нк';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = 'Ко';
$app_list_strings['moduleIconList']['TeamNotices'] = 'Уд';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Пр';
$app_list_strings['moduleIconList']['Activities'] = 'Ме';
$app_list_strings['moduleIconList']['Comments'] = 'Ко';
$app_list_strings['moduleIconList']['Subscriptions'] = 'По';
$app_list_strings['moduleIconList']['Bugs'] = 'Ош';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'Мс';
$app_list_strings['moduleIconList']['TimePeriods'] = 'Оп';
$app_list_strings['moduleIconList']['TaxRates'] = 'Нс';
$app_list_strings['moduleIconList']['ContractTypes'] = 'Тк';
$app_list_strings['moduleIconList']['Schedulers'] = 'Пз';
$app_list_strings['moduleIconList']['Project'] = 'Пр';
$app_list_strings['moduleIconList']['ProjectTask'] = 'Пз';
$app_list_strings['moduleIconList']['Campaigns'] = 'Мк';
$app_list_strings['moduleIconList']['CampaignLog'] = 'Жм';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'Тм';
$app_list_strings['moduleIconList']['Documents'] = 'До';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'Вд';
$app_list_strings['moduleIconList']['Connectors'] = 'По';
$app_list_strings['moduleIconList']['Notifications'] = 'Ув';
$app_list_strings['moduleIconList']['Sync'] = 'Си';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'Вп';
$app_list_strings['moduleIconList']['ReportMaker'] = 'Оп';
$app_list_strings['moduleIconList']['DataSets'] = 'Фд';
$app_list_strings['moduleIconList']['CustomQueries'] = 'Из';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Пр';
$app_list_strings['moduleIconList']['pmse_Project'] = 'Ои';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'По';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'По';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'Би';
$app_list_strings['moduleIconList']['Shifts'] = 'См';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'Ис';
$app_list_strings['moduleIconList']['Purchases'] = 'По';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'Пп';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Му';
$app_list_strings['moduleIconList']['PushNotifications'] = 'Пу';
$app_list_strings['moduleIconList']['Escalations'] = 'Эс';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'Шд';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'Од';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'Пк';
$app_list_strings['moduleIconList']['WorkFlow'] = 'Об';
$app_list_strings['moduleIconList']['EAPM'] = 'Ву';
$app_list_strings['moduleIconList']['Worksheet'] = 'Лп';
$app_list_strings['moduleIconList']['Users'] = 'По';
$app_list_strings['moduleIconList']['Employees'] = 'Со';
$app_list_strings['moduleIconList']['Administration'] = 'Ад';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Ро';
$app_list_strings['moduleIconList']['InboundEmail'] = 'Гу';
$app_list_strings['moduleIconList']['Releases'] = 'Ве';
$app_list_strings['moduleIconList']['Prospects'] = 'Пк';
$app_list_strings['moduleIconList']['Queues'] = 'Оч';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'РE';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'Шс';
$app_list_strings['moduleIconList']['SNIP'] = 'Eа';
$app_list_strings['moduleIconList']['ProspectLists'] = 'Цс';
$app_list_strings['moduleIconList']['SavedSearch'] = 'Сп';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'Мо';
$app_list_strings['moduleIconList']['Trackers'] = 'Тр';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'Фт';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'Ст';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'Зт';
$app_list_strings['moduleIconList']['FAQ'] = 'Ви';
$app_list_strings['moduleIconList']['Newsletters'] = 'Ор';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Из';
$app_list_strings['moduleIconList']['PdfManager'] = 'МP';
$app_list_strings['moduleIconList']['DataArchiver'] = 'Ад';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'Аз';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'Oк';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'МO';
$app_list_strings['moduleIconList']['Filters'] = 'Фи';
$app_list_strings['moduleIconList']['UserSignatures'] = 'Пэ';
$app_list_strings['moduleIconList']['Shippers'] = 'Пп';
$app_list_strings['moduleIconList']['Styleguide'] = 'Sм';
$app_list_strings['moduleIconList']['Feedbacks'] = 'Ос';
$app_list_strings['moduleIconList']['Tags'] = 'Те';
$app_list_strings['moduleIconList']['Categories'] = 'Ка';
$app_list_strings['moduleIconList']['Dashboards'] = 'Ип';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'Пэ';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'Ас';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'Зд';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'Ро';
$app_list_strings['moduleIconList']['CommentLog'] = 'Жк';
$app_list_strings['moduleIconList']['Holidays'] = 'Ви';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'Ит';
$app_list_strings['moduleIconList']['Metrics'] = 'По';
$app_list_strings['moduleIconList']['Messages'] = 'Со';
$app_list_strings['moduleIconList']['Audit'] = 'Ау';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'Дп';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'КD';
$app_list_strings['moduleIconList']['Geocode'] = 'Ге';
$app_list_strings['moduleIconList']['DRI_Workflows'] = 'Ур';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = 'Шм';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = 'Эу';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = 'Шу';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'Ва';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'ДS';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = 'Шс';
$app_list_strings['moduleIconList']['Library'] = 'Би';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'Eа';
$app_list_strings['moduleIconList']['Words'] = 'Сл';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Из';
$app_list_strings['moduleIconList']['KBDocuments'] = 'Бз';
$app_list_strings['moduleIconList']['KBContents'] = 'Бз';
$app_list_strings['moduleIconList']['KBArticles'] = 'Бз';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'Шб';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'Вф';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';

?>

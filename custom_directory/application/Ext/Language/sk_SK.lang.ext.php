<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sk_SK.sugar_moduleList.php

 //created: 2020-01-31 00:19:11

$app_list_strings['moduleList']['RevenueLineItems']='Položky krivky výnosu';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sk_SK.sugar_parent_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Účet',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Úloha',
  'Opportunities' => 'Príležitosť',
  'Products' => 'Položka ponuky',
  'Quotes' => 'Ponuka',
  'Bugs' => 'Chyby',
  'Cases' => 'Prípad',
  'Leads' => 'Záujemca',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektová úloha',
  'Prospects' => 'Cieľ',
  'KBContents' => 'Báza znalostí',
  'Notes' => 'Poznámka',
  'RevenueLineItems' => 'Položky krivky výnosu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sk_SK.sugar_record_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Účet',
  'Opportunities' => 'Príležitosť',
  'Cases' => 'Prípad',
  'Leads' => 'Záujemca',
  'Contacts' => 'Kontakty',
  'Products' => 'Položka ponuky',
  'Quotes' => 'Ponuka',
  'Bugs' => 'Chyba',
  'Project' => 'Projekt',
  'Prospects' => 'Cieľ',
  'ProjectTask' => 'Projektová úloha',
  'Tasks' => 'Úloha',
  'KBContents' => 'Báza znalostí',
  'Notes' => 'Poznámka',
  'RevenueLineItems' => 'Položky krivky výnosu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sk_SK.sugar_record_type_display_notes.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Účet',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Príležitosť',
  'Tasks' => 'Úloha',
  'ProductTemplates' => 'Katalóg produktov',
  'Quotes' => 'Ponuka',
  'Products' => 'Položka ponuky',
  'Contracts' => 'Zmluva',
  'Emails' => 'E-mail',
  'Bugs' => 'Chyba',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektová úloha',
  'Prospects' => 'Cieľ',
  'Cases' => 'Prípad',
  'Leads' => 'Záujemca',
  'Meetings' => 'Schôdzka',
  'Calls' => 'Volanie',
  'KBContents' => 'Báza znalostí',
  'RevenueLineItems' => 'Položky krivky výnosu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sk_SK.sugar_moduleIconList.php

// created: 2023-02-08 11:06:14
$app_list_strings['moduleIconList']['Home'] = 'Do';
$app_list_strings['moduleIconList']['Contacts'] = 'Ko';
$app_list_strings['moduleIconList']['Accounts'] = 'Úč';
$app_list_strings['moduleIconList']['Opportunities'] = 'Pr';
$app_list_strings['moduleIconList']['Cases'] = 'Ud';
$app_list_strings['moduleIconList']['Notes'] = 'Po';
$app_list_strings['moduleIconList']['Calls'] = 'Vo';
$app_list_strings['moduleIconList']['Emails'] = 'E-';
$app_list_strings['moduleIconList']['Meetings'] = 'Sc';
$app_list_strings['moduleIconList']['Tasks'] = 'Úl';
$app_list_strings['moduleIconList']['Calendar'] = 'Ka';
$app_list_strings['moduleIconList']['Leads'] = 'Zá';
$app_list_strings['moduleIconList']['Currencies'] = 'Me';
$app_list_strings['moduleIconList']['Contracts'] = 'Ko';
$app_list_strings['moduleIconList']['Quotes'] = 'Po';
$app_list_strings['moduleIconList']['Products'] = 'Pp';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'Wl';
$app_list_strings['moduleIconList']['ProductCategories'] = 'Kp';
$app_list_strings['moduleIconList']['ProductTypes'] = 'Tp';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'Kp';
$app_list_strings['moduleIconList']['ProductBundles'] = 'Pb';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'Pk';
$app_list_strings['moduleIconList']['Reports'] = 'Hl';
$app_list_strings['moduleIconList']['Forecasts'] = 'Pr';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'Pt';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'Mp';
$app_list_strings['moduleIconList']['Quotas'] = 'Kv';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'Vp';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'Kk';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = 'Tí';
$app_list_strings['moduleIconList']['TeamNotices'] = 'To';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Vý';
$app_list_strings['moduleIconList']['Activities'] = 'Ak';
$app_list_strings['moduleIconList']['Comments'] = 'Ko';
$app_list_strings['moduleIconList']['Subscriptions'] = 'Pk';
$app_list_strings['moduleIconList']['Bugs'] = 'Ch';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'Ms';
$app_list_strings['moduleIconList']['TimePeriods'] = 'Čo';
$app_list_strings['moduleIconList']['TaxRates'] = 'Ds';
$app_list_strings['moduleIconList']['ContractTypes'] = 'Tk';
$app_list_strings['moduleIconList']['Schedulers'] = 'Pl';
$app_list_strings['moduleIconList']['Project'] = 'Pr';
$app_list_strings['moduleIconList']['ProjectTask'] = 'Pú';
$app_list_strings['moduleIconList']['Campaigns'] = 'Ka';
$app_list_strings['moduleIconList']['CampaignLog'] = 'Dk';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'Sk';
$app_list_strings['moduleIconList']['Documents'] = 'Do';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'Rd';
$app_list_strings['moduleIconList']['Connectors'] = 'Ko';
$app_list_strings['moduleIconList']['Notifications'] = 'Up';
$app_list_strings['moduleIconList']['Sync'] = 'Sy';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'Ep';
$app_list_strings['moduleIconList']['ReportMaker'] = 'Ph';
$app_list_strings['moduleIconList']['DataSets'] = 'Fú';
$app_list_strings['moduleIconList']['CustomQueries'] = 'Zo';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Pr';
$app_list_strings['moduleIconList']['pmse_Project'] = 'Dp';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'Po';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'Še';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'Oc';
$app_list_strings['moduleIconList']['Shifts'] = 'Zm';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'Vz';
$app_list_strings['moduleIconList']['Purchases'] = 'Ná';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'Zr';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Mz';
$app_list_strings['moduleIconList']['PushNotifications'] = 'Po';
$app_list_strings['moduleIconList']['Escalations'] = 'Es';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'Šd';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'Zd';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'Cc';
$app_list_strings['moduleIconList']['WorkFlow'] = 'Dp';
$app_list_strings['moduleIconList']['EAPM'] = 'Eú';
$app_list_strings['moduleIconList']['Worksheet'] = 'Ta';
$app_list_strings['moduleIconList']['Users'] = 'Po';
$app_list_strings['moduleIconList']['Employees'] = 'Za';
$app_list_strings['moduleIconList']['Administration'] = 'Ad';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Ro';
$app_list_strings['moduleIconList']['InboundEmail'] = 'Pe';
$app_list_strings['moduleIconList']['Releases'] = 'Vy';
$app_list_strings['moduleIconList']['Prospects'] = 'Ci';
$app_list_strings['moduleIconList']['Queues'] = 'Fr';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'Em';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'Eš';
$app_list_strings['moduleIconList']['SNIP'] = 'Ae';
$app_list_strings['moduleIconList']['ProspectLists'] = 'Zc';
$app_list_strings['moduleIconList']['SavedSearch'] = 'Uv';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'Si';
$app_list_strings['moduleIconList']['Trackers'] = 'St';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'Vs';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'Rs';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'Os';
$app_list_strings['moduleIconList']['FAQ'] = 'Čk';
$app_list_strings['moduleIconList']['Newsletters'] = 'No';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Ob';
$app_list_strings['moduleIconList']['PdfManager'] = 'SP';
$app_list_strings['moduleIconList']['DataArchiver'] = 'Aú';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'Sa';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'Pk';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'TO';
$app_list_strings['moduleIconList']['Filters'] = 'Fi';
$app_list_strings['moduleIconList']['UserSignatures'] = 'Ep';
$app_list_strings['moduleIconList']['Shippers'] = 'Pr';
$app_list_strings['moduleIconList']['Styleguide'] = 'Pš';
$app_list_strings['moduleIconList']['Feedbacks'] = 'Sv';
$app_list_strings['moduleIconList']['Tags'] = 'Ta';
$app_list_strings['moduleIconList']['Categories'] = 'Ka';
$app_list_strings['moduleIconList']['Dashboards'] = 'Ip';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'Ne';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'Úe';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'Oo';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'Ph';
$app_list_strings['moduleIconList']['CommentLog'] = 'Dk';
$app_list_strings['moduleIconList']['Holidays'] = 'Do';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'Zč';
$app_list_strings['moduleIconList']['Metrics'] = 'Me';
$app_list_strings['moduleIconList']['Messages'] = 'Sp';
$app_list_strings['moduleIconList']['Audit'] = 'Au';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'Pk';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'OD';
$app_list_strings['moduleIconList']['Geocode'] = 'Ge';
$app_list_strings['moduleIconList']['DRI_Workflows'] = 'Is';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = 'Ša';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = 'Fi';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = 'Ši';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'WS';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'AS';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = 'Šf';
$app_list_strings['moduleIconList']['Library'] = 'Kn';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'Ea';
$app_list_strings['moduleIconList']['Words'] = 'Sl';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Ob';
$app_list_strings['moduleIconList']['KBDocuments'] = 'Dv';
$app_list_strings['moduleIconList']['KBContents'] = 'Bz';
$app_list_strings['moduleIconList']['KBArticles'] = 'Čv';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'Šb';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'Vs';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';

?>

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.sugar_moduleList.php

 //created: 2020-01-31 00:19:11

$app_list_strings['moduleList']['RevenueLineItems']='Rreshti i llojeve të të ardhurave';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.sugar_parent_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Llogaria',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Detyrë',
  'Opportunities' => 'Mundësi:',
  'Products' => 'Artikulli i rreshtit të cituar',
  'Quotes' => 'Kuota',
  'Bugs' => 'Gabimet',
  'Cases' => 'Rast',
  'Leads' => 'Udhëheqje',
  'Project' => 'Projekti',
  'ProjectTask' => 'Detyrat e projektit',
  'Prospects' => 'Synim',
  'KBContents' => 'Baza e njohurisë',
  'Notes' => 'Shënim',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.sugar_record_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'llogaritë',
  'Opportunities' => 'Mundësi:',
  'Cases' => 'Rast',
  'Leads' => 'udhëheqje',
  'Contacts' => 'Kontaktet',
  'Products' => 'Produkti',
  'Quotes' => 'Kuota',
  'Bugs' => 'Gabim',
  'Project' => 'Projekti',
  'Prospects' => 'Synim',
  'ProjectTask' => 'Detyrat projektuese',
  'Tasks' => 'Detyrë',
  'KBContents' => 'baza e njohurisë',
  'Notes' => 'Shënim',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.sugar_record_type_display_notes.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'llogaritë',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Mundësi:',
  'Tasks' => 'Detyrë',
  'ProductTemplates' => 'Katalogu i produkteve',
  'Quotes' => 'Kuota',
  'Products' => 'Produkti',
  'Contracts' => 'Kontrata',
  'Emails' => 'Email',
  'Bugs' => 'Gabim',
  'Project' => 'Projekti',
  'ProjectTask' => 'Detyrat projektuese',
  'Prospects' => 'Synim',
  'Cases' => 'Rast',
  'Leads' => 'udhëheqje',
  'Meetings' => 'Mbledhje',
  'Calls' => 'Thirrje',
  'KBContents' => 'baza e njohurisë',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.sugar_moduleIconList.php

// created: 2023-02-08 11:06:14
$app_list_strings['moduleIconList']['Home'] = 'Ba';
$app_list_strings['moduleIconList']['Contacts'] = 'Ko';
$app_list_strings['moduleIconList']['Accounts'] = 'll';
$app_list_strings['moduleIconList']['Opportunities'] = 'Mu';
$app_list_strings['moduleIconList']['Cases'] = 'Ra';
$app_list_strings['moduleIconList']['Notes'] = 'Sh';
$app_list_strings['moduleIconList']['Calls'] = 'Th';
$app_list_strings['moduleIconList']['Emails'] = 'Em';
$app_list_strings['moduleIconList']['Meetings'] = 'Mb';
$app_list_strings['moduleIconList']['Tasks'] = 'De';
$app_list_strings['moduleIconList']['Calendar'] = 'Ka';
$app_list_strings['moduleIconList']['Leads'] = 'Ud';
$app_list_strings['moduleIconList']['Currencies'] = 'Mo';
$app_list_strings['moduleIconList']['Contracts'] = 'Ko';
$app_list_strings['moduleIconList']['Quotes'] = 'Ku';
$app_list_strings['moduleIconList']['Products'] = 'Ae';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'Ge';
$app_list_strings['moduleIconList']['ProductCategories'] = 'Ke';
$app_list_strings['moduleIconList']['ProductTypes'] = 'Le';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'Ki';
$app_list_strings['moduleIconList']['ProductBundles'] = 'Pe';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'St';
$app_list_strings['moduleIconList']['Reports'] = 'Ra';
$app_list_strings['moduleIconList']['Forecasts'] = 'Pa';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'Te';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'Tp';
$app_list_strings['moduleIconList']['Quotas'] = 'Ku';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'VP';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'Ki';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = 'Gr';
$app_list_strings['moduleIconList']['TeamNotices'] = 'Sg';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Pr';
$app_list_strings['moduleIconList']['Activities'] = 'Ak';
$app_list_strings['moduleIconList']['Comments'] = 'Ko';
$app_list_strings['moduleIconList']['Subscriptions'] = 'Ab';
$app_list_strings['moduleIconList']['Bugs'] = 'Ga';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'Fe';
$app_list_strings['moduleIconList']['TimePeriods'] = 'Ko';
$app_list_strings['moduleIconList']['TaxRates'] = 'Ne';
$app_list_strings['moduleIconList']['ContractTypes'] = 'Le';
$app_list_strings['moduleIconList']['Schedulers'] = 'Pl';
$app_list_strings['moduleIconList']['Project'] = 'Pr';
$app_list_strings['moduleIconList']['ProjectTask'] = 'De';
$app_list_strings['moduleIconList']['Campaigns'] = 'fu';
$app_list_strings['moduleIconList']['CampaignLog'] = 'ii';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'Ge';
$app_list_strings['moduleIconList']['Documents'] = 'Do';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'Ri';
$app_list_strings['moduleIconList']['Connectors'] = 'Li';
$app_list_strings['moduleIconList']['Notifications'] = 'Nj';
$app_list_strings['moduleIconList']['Sync'] = 'Si';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Ge';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Me';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Ne';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Ke';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'Pe';
$app_list_strings['moduleIconList']['ReportMaker'] = 'Rt';
$app_list_strings['moduleIconList']['DataSets'] = 'Fi';
$app_list_strings['moduleIconList']['CustomQueries'] = 'Pt';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Pr';
$app_list_strings['moduleIconList']['pmse_Project'] = 'Dp';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'Rb';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'Pe';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'Qe';
$app_list_strings['moduleIconList']['Shifts'] = 'Tu';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'Pn';
$app_list_strings['moduleIconList']['Purchases'] = 'Bl';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'Ae';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Pc';
$app_list_strings['moduleIconList']['PushNotifications'] = 'Nm';
$app_list_strings['moduleIconList']['Escalations'] = 'Pë';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'Se';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'Be';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'Se';
$app_list_strings['moduleIconList']['WorkFlow'] = 'De';
$app_list_strings['moduleIconList']['EAPM'] = 'Le';
$app_list_strings['moduleIconList']['Worksheet'] = 'Fp';
$app_list_strings['moduleIconList']['Users'] = 'pë';
$app_list_strings['moduleIconList']['Employees'] = 'Pu';
$app_list_strings['moduleIconList']['Administration'] = 'Ad';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Ro';
$app_list_strings['moduleIconList']['InboundEmail'] = 'Le';
$app_list_strings['moduleIconList']['Releases'] = 'Pu';
$app_list_strings['moduleIconList']['Prospects'] = 'sy';
$app_list_strings['moduleIconList']['Queues'] = 'Ra';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'Em';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'St';
$app_list_strings['moduleIconList']['SNIP'] = 'Ai';
$app_list_strings['moduleIconList']['ProspectLists'] = 'Le';
$app_list_strings['moduleIconList']['SavedSearch'] = 'Ke';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'Rw';
$app_list_strings['moduleIconList']['Trackers'] = 'Gj';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'Pe';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'Se';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'Pe';
$app_list_strings['moduleIconList']['FAQ'] = 'Pt';
$app_list_strings['moduleIconList']['Newsletters'] = 'Bu';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Fa';
$app_list_strings['moduleIconList']['PdfManager'] = 'Pm';
$app_list_strings['moduleIconList']['DataArchiver'] = 'Ai';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'Ee';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'ÇO';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'OS';
$app_list_strings['moduleIconList']['Filters'] = 'fi';
$app_list_strings['moduleIconList']['UserSignatures'] = 'Ne';
$app_list_strings['moduleIconList']['Shippers'] = 'Oe';
$app_list_strings['moduleIconList']['Styleguide'] = 'Us';
$app_list_strings['moduleIconList']['Feedbacks'] = 'Vë';
$app_list_strings['moduleIconList']['Tags'] = 'Et';
$app_list_strings['moduleIconList']['Categories'] = 'Ka';
$app_list_strings['moduleIconList']['Dashboards'] = 'Pa';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'Ce';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'Pm';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'Pe';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'Ri';
$app_list_strings['moduleIconList']['CommentLog'] = 'Ee';
$app_list_strings['moduleIconList']['Holidays'] = 'Pu';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'Nk';
$app_list_strings['moduleIconList']['Metrics'] = 'Me';
$app_list_strings['moduleIconList']['Messages'] = 'Me';
$app_list_strings['moduleIconList']['Audit'] = 'Au';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'Ri';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'Ze';
$app_list_strings['moduleIconList']['Geocode'] = 'Gj';
$app_list_strings['moduleIconList']['DRI_Workflows'] = 'Ui';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = 'Ui';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = 'Fe';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = 'Se';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = '&quot;t';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'VS';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = 'Se';
$app_list_strings['moduleIconList']['Library'] = 'Li';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'Ea';
$app_list_strings['moduleIconList']['Words'] = 'Wo';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Fa';
$app_list_strings['moduleIconList']['KBDocuments'] = 'be';
$app_list_strings['moduleIconList']['KBContents'] = 'be';
$app_list_strings['moduleIconList']['KBArticles'] = 'At';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'Si';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'St';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';

?>

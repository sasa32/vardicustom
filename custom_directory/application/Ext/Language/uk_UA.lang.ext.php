<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/uk_UA.sugar_moduleList.php

 //created: 2020-01-31 00:19:11

$app_list_strings['moduleList']['MergeRecords']='Об\'єднання записів';
$app_list_strings['moduleList']['Connectors']='З\'єднувачі';
$app_list_strings['moduleList']['Feedbacks']='Зворотній зв\'язок';
$app_list_strings['moduleList']['RevenueLineItems']='Доходи за продукти';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/uk_UA.sugar_parent_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Угода',
  'Products' => 'Продукт комерційної пропозиції',
  'Quotes' => 'Комерційна пропозиція',
  'Bugs' => 'Помилки',
  'Cases' => 'Звернення',
  'Leads' => 'Інтерес',
  'Project' => 'Проект',
  'ProjectTask' => 'Задача проекту',
  'Prospects' => 'Цільова аудиторія споживачів',
  'KBContents' => 'База знань',
  'Notes' => 'Примітка',
  'RevenueLineItems' => 'Доходи за продукти',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/uk_UA.sugar_record_type_display.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Контрагент',
  'Opportunities' => 'Угода',
  'Cases' => 'Звернення',
  'Leads' => 'Інтерес',
  'Contacts' => 'Контакти',
  'Products' => 'Продукт комерційної пропозиції',
  'Quotes' => 'Комерційна пропозиція',
  'Bugs' => 'Помилка',
  'Project' => 'Проект',
  'Prospects' => 'Цільова аудиторія споживачів',
  'ProjectTask' => 'Задача проекту',
  'Tasks' => 'Задача',
  'KBContents' => 'База знань',
  'Notes' => 'Примітка',
  'RevenueLineItems' => 'Доходи за продукти',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/uk_UA.sugar_record_type_display_notes.php

 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Opportunities' => 'Угода',
  'Tasks' => 'Задача',
  'ProductTemplates' => 'Каталог продукту',
  'Quotes' => 'Комерційна пропозиція',
  'Products' => 'Продукт комерційної пропозиції',
  'Contracts' => 'Контракт',
  'Emails' => 'Ел. пошта',
  'Bugs' => 'Помилка',
  'Project' => 'Проект',
  'ProjectTask' => 'Задача проекту',
  'Prospects' => 'Цільова аудиторія споживачів',
  'Cases' => 'Звернення',
  'Leads' => 'Інтерес',
  'Meetings' => 'Зустріч',
  'Calls' => 'Дзвінок',
  'KBContents' => 'База знань',
  'RevenueLineItems' => 'Доходи за продукти',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/uk_UA.sugar_moduleIconList.php

// created: 2023-02-08 11:06:14
$app_list_strings['moduleIconList']['Home'] = 'Го';
$app_list_strings['moduleIconList']['Contacts'] = 'Ко';
$app_list_strings['moduleIconList']['Accounts'] = 'Ко';
$app_list_strings['moduleIconList']['Opportunities'] = 'Уг';
$app_list_strings['moduleIconList']['Cases'] = 'Зв';
$app_list_strings['moduleIconList']['Notes'] = 'Пр';
$app_list_strings['moduleIconList']['Calls'] = 'Дз';
$app_list_strings['moduleIconList']['Emails'] = 'По';
$app_list_strings['moduleIconList']['Meetings'] = 'Зу';
$app_list_strings['moduleIconList']['Tasks'] = 'За';
$app_list_strings['moduleIconList']['Calendar'] = 'Ка';
$app_list_strings['moduleIconList']['Leads'] = 'Ін';
$app_list_strings['moduleIconList']['Currencies'] = 'Ва';
$app_list_strings['moduleIconList']['Contracts'] = 'Ко';
$app_list_strings['moduleIconList']['Quotes'] = 'Кп';
$app_list_strings['moduleIconList']['Products'] = 'Пк';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'Вл';
$app_list_strings['moduleIconList']['ProductCategories'] = 'Кп';
$app_list_strings['moduleIconList']['ProductTypes'] = 'Тп';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'Кп';
$app_list_strings['moduleIconList']['ProductBundles'] = 'Кп';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'Пк';
$app_list_strings['moduleIconList']['Reports'] = 'Зв';
$app_list_strings['moduleIconList']['Forecasts'] = 'Пр';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'Тп';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'Тм';
$app_list_strings['moduleIconList']['Quotas'] = 'Кв';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'Вв';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'Нк';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = 'Ко';
$app_list_strings['moduleIconList']['TeamNotices'] = 'Пд';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Ви';
$app_list_strings['moduleIconList']['Activities'] = 'Ак';
$app_list_strings['moduleIconList']['Comments'] = 'Ко';
$app_list_strings['moduleIconList']['Subscriptions'] = 'Пі';
$app_list_strings['moduleIconList']['Bugs'] = 'По';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'Мс';
$app_list_strings['moduleIconList']['TimePeriods'] = 'Пч';
$app_list_strings['moduleIconList']['TaxRates'] = 'Пс';
$app_list_strings['moduleIconList']['ContractTypes'] = 'Тк';
$app_list_strings['moduleIconList']['Schedulers'] = 'Пл';
$app_list_strings['moduleIconList']['Project'] = 'Пр';
$app_list_strings['moduleIconList']['ProjectTask'] = 'Зп';
$app_list_strings['moduleIconList']['Campaigns'] = 'Мк';
$app_list_strings['moduleIconList']['CampaignLog'] = 'Жм';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'Тм';
$app_list_strings['moduleIconList']['Documents'] = 'До';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'Рд';
$app_list_strings['moduleIconList']['Connectors'] = 'З&#039;';
$app_list_strings['moduleIconList']['Notifications'] = 'По';
$app_list_strings['moduleIconList']['Sync'] = 'Си';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'Зк';
$app_list_strings['moduleIconList']['ReportMaker'] = 'Рз';
$app_list_strings['moduleIconList']['DataSets'] = 'Фд';
$app_list_strings['moduleIconList']['CustomQueries'] = 'Кз';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Пр';
$app_list_strings['moduleIconList']['pmse_Project'] = 'Вп';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'Пб';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'По';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'Бі';
$app_list_strings['moduleIconList']['Shifts'] = 'Зм';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'Вз';
$app_list_strings['moduleIconList']['Purchases'] = 'По';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'Кп';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Мп';
$app_list_strings['moduleIconList']['PushNotifications'] = 'Пс';
$app_list_strings['moduleIconList']['Escalations'] = 'Ес';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'Шд';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'Од';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'Шд';
$app_list_strings['moduleIconList']['WorkFlow'] = 'Вб';
$app_list_strings['moduleIconList']['EAPM'] = 'Зо';
$app_list_strings['moduleIconList']['Worksheet'] = 'Та';
$app_list_strings['moduleIconList']['Users'] = 'Ко';
$app_list_strings['moduleIconList']['Employees'] = 'Сп';
$app_list_strings['moduleIconList']['Administration'] = 'Ад';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Ро';
$app_list_strings['moduleIconList']['InboundEmail'] = 'ВE';
$app_list_strings['moduleIconList']['Releases'] = 'Ви';
$app_list_strings['moduleIconList']['Prospects'] = 'Ца';
$app_list_strings['moduleIconList']['Queues'] = 'Че';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'Ем';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'ШE';
$app_list_strings['moduleIconList']['SNIP'] = 'АE';
$app_list_strings['moduleIconList']['ProspectLists'] = 'Сц';
$app_list_strings['moduleIconList']['SavedSearch'] = 'Зп';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'Мо';
$app_list_strings['moduleIconList']['Trackers'] = 'Тр';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'Пт';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'Ст';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'Зт';
$app_list_strings['moduleIconList']['FAQ'] = 'Зй';
$app_list_strings['moduleIconList']['Newsletters'] = 'Ір';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Об';
$app_list_strings['moduleIconList']['PdfManager'] = 'МP';
$app_list_strings['moduleIconList']['DataArchiver'] = 'Ад';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'Аз';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'Oс';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'МO';
$app_list_strings['moduleIconList']['Filters'] = 'Фі';
$app_list_strings['moduleIconList']['UserSignatures'] = 'Пе';
$app_list_strings['moduleIconList']['Shippers'] = 'Пп';
$app_list_strings['moduleIconList']['Styleguide'] = 'Сд';
$app_list_strings['moduleIconList']['Feedbacks'] = 'Зз';
$app_list_strings['moduleIconList']['Tags'] = 'Те';
$app_list_strings['moduleIconList']['Categories'] = 'Ка';
$app_list_strings['moduleIconList']['Dashboards'] = 'Іп';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'Пе';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'Ап';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'Зд';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'Гз';
$app_list_strings['moduleIconList']['CommentLog'] = 'Жк';
$app_list_strings['moduleIconList']['Holidays'] = 'Вт';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'Зт';
$app_list_strings['moduleIconList']['Metrics'] = 'По';
$app_list_strings['moduleIconList']['Messages'] = 'По';
$app_list_strings['moduleIconList']['Audit'] = 'Ау';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'Дз';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'КD';
$app_list_strings['moduleIconList']['Geocode'] = 'Ге';
$app_list_strings['moduleIconList']['DRI_Workflows'] = 'SG';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = 'Шд';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = 'СS';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = 'ШS';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'ВS';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'ДS';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = 'Шс';
$app_list_strings['moduleIconList']['Library'] = 'Бі';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'АE';
$app_list_strings['moduleIconList']['Words'] = 'Сл';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Об';
$app_list_strings['moduleIconList']['KBDocuments'] = 'Бз';
$app_list_strings['moduleIconList']['KBContents'] = 'Бз';
$app_list_strings['moduleIconList']['KBArticles'] = 'Сб';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'Шб';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'Вф';
$app_list_strings['moduleIconList']['sasa_vehiculos'] = 'Ld';
$app_list_strings['moduleIconList']['sasa_Unidad_de_Negocio'] = 'Ud';
$app_list_strings['moduleIconList']['sasa_Puntos_de_Ventas'] = 'Pd';
$app_list_strings['moduleIconList']['sasa_Marcas'] = 'Ma';
$app_list_strings['moduleIconList']['sasa_Companias'] = 'Co';
$app_list_strings['moduleIconList']['SASA_UnidadesdeNegporClieyPro'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_UnidadNegClienteProspect'] = 'Ud';
$app_list_strings['moduleIconList']['SASA_Habeas_Data'] = 'HD';
$app_list_strings['moduleIconList']['sasa_Tipificacion_de_Casos'] = 'Td';
$app_list_strings['moduleIconList']['sasa_Paises'] = 'Pa';
$app_list_strings['moduleIconList']['sasa_Departamentos'] = 'De';
$app_list_strings['moduleIconList']['sasa_Municipios'] = 'Ci';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['SASA_Campanas'] = 'Ca';
$app_list_strings['moduleIconList']['sasa_sasa_centrocostos_c'] = 'CD';
$app_list_strings['moduleIconList']['sasa_CentrosDeCostos'] = 'CD';
$app_list_strings['moduleIconList']['MergeRecords'] = 'Оз';

?>

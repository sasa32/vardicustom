<?php

require_once 'include/api/SugarApiException.php';

/**
 * Custom error.
 */
class cstmSugarApiExceptionError extends SugarApiException
{
    public $httpCode = 422;
    public $errorLabel = 'invalid_parameter';
    public $messageLabel = 'invalid_parameter';
}

<?php 
if ( !defined( 'sugarEntry' ) || !sugarEntry )die( 'Not A Valid Entry Point' );

class WebToLeadCustomAPI extends SugarApi {
	public function registerApiRest() {
		return array(
			//GET & POST
			'webtolead' => array(
				'reqType' => array( 'POST' ),
				'noLoginRequired' => false,
				'path' => array( 'webtolead' ), 
				'pathVars' => array( '' ),
				'method' => 'webtolead',
				'shortHelp' => 'webtolead',
				'longHelp' => 'custom/clients/base/api/help/webtolead_post_help.html',
			),
		);
	}

	public function webtolead($api, $args ){

		try{
			//Validación sobre el indice de data
			if(!$args) {
				return array('Error' => '103', 'Description' => 'Did not have data index in your JSON');
			}else{
				require_once( "custom/modules/Leads/Integration_classes/WebToLead.php" );
				$Lead = new Process_Lead($args);

				return $Lead->get_return();
			}
		}catch (Exception $e){
			return array('Error' => '101', 'Description' => 'FatalError: '.$e->getMessage());
		}
		
	}
}







?>
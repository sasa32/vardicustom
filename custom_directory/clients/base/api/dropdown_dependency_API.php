<?php 
//UPDATE `ws_dropdown_dependency` SET `status`='queued'
if ( !defined( 'sugarEntry' ) || !sugarEntry )die( 'Not A Valid Entry Point' );

class dropdown_dependency_API extends SugarApi {
	public function registerApiRest() {
		return array(
			'ws_dropdown_dependency_post' => array(
				'reqType' => array( 'POST'),
				'noLoginRequired' => false,
				'path' => array( 'ws_dropdown_dependency' ), 
				'pathVars' => array( '' ),
				'method' => 'ws_dropdown_dependency_post',
				'shortHelp' => 'ws_dropdown_dependency_post',
				'longHelp' => 'custom/clients/base/api/help/dropdown_dependency_help.html',
			),
			'ws_dropdown_dependency_get' => array(
				'reqType' => array( 'GET' ),
				'noLoginRequired' => false,
				'path' => array( 'ws_dropdown_dependency','get','?' ), 
				'pathVars' => array( '','','lote' ),
				'method' => 'ws_dropdown_dependency_get',
				'shortHelp' => 'ws_dropdown_dependency_get',
				'longHelp' => 'custom/clients/base/api/help/dropdown_dependency_help.html',
			),
			'ws_dropdown_dependency_set' => array(
				'reqType' => array( 'GET' ),
				'noLoginRequired' => false,
				'path' => array( 'ws_dropdown_dependency','set','?' ), 
				'pathVars' => array( '','','id' ),
				'method' => 'ws_dropdown_dependency_set',
				'shortHelp' => 'ws_dropdown_dependency_set',
				'longHelp' => 'custom/clients/base/api/help/dropdown_dependency_help.html',
			),
		);
	}
	public function ws_dropdown_dependency_post($api, $args ){		
		try{
			global $app_list_strings;
			$return = true;
			$message =array(
			);
			
			if (!isset($args) and !empty($args) and !is_array($args)) {
				$message[0]['code'] = '101';
				$message[0]['message'] = "'{$args}' is an invalid request";	
			}
			else{
				unset($args["__sugar_url"]);
				foreach($args as $key => $dependency){
					if (!isset($dependency["module"]) and !array_key_exists("module",$dependency) and empty($dependency["module"])) {	
						$message[$key]['code'] = '102';
						$message[$key]['message'] = "'{$dependency["module"]}' is an invalid module";	
					}
					else{
						$level = $this->testDepth($dependency);
						if($level != 3){
							$message[$key]['code'] = '123';
							$message[$key]['message'] = "Request level was expected to be 3 but {$level} was given";							
						}
						else{
							if (!isset($dependency["field"]) and !array_key_exists("field",$dependency) and empty($dependency["field"])) {		
								$message[$key]['code'] = '103';
								$message[$key]['message'] = "'{$dependency["field"]}' is an invalid field";
							}
							else{	
								if (!isset($dependency["parent_field"]) and !array_key_exists("parent_field",$dependency) and empty($dependency["parent_field"])) {	
									$message[$key]['code'] = '104';
									$message[$key]['message'] = "'{$dependency["parent_field"]}' is an invalid parent_field";
								}
								else{					
									if (!isset($dependency["dependency"]) and !array_key_exists("dependency",$dependency) and !is_array($dependency["dependency"]) or empty($dependency["dependency"])) {
										$message[$key]['code'] = '105';
										$message[$key]['message'] = "'{$dependency["dependency"]}' is an invalid dependency";							
									}
									else{							
										$bean_name = BeanFactory::getObjectName($dependency["module"]);//Added to fix vardef name
										$bean = BeanFactory::newBeanByName( $bean_name );
										if (empty($bean)) {
											$message[$key]['code'] = '106';
											$message[$key]['message'] = "Module '{$dependency["module"]}' doesn't exists";	
										}
										else{
											$dependency["module"] = "{$dependency["module"]}Ç|Ç{$bean_name}";//Added to fix vardef name
										    	if ( !array_key_exists($dependency["field"],$bean->field_defs ) ) {
												$message[$key]['code'] = '107';
												$message[$key]['message'] = "Field '{$dependency["field"]}' doesn't exists";	
											}
											else{	
												if($bean->field_defs[$dependency["field"]]["type"] != "enum"){
													$message[$key]['code'] = '108';
													$message[$key]['message'] = "Field '{$dependency["field"]}' is not dropdown (enum)";	
												}
												else{							
											    		if ( !array_key_exists($dependency["parent_field"],$bean->field_defs) ) {
														$message[$key]['code'] = '109';
														$message[$key]['message'] = "Parent_field '{$dependency["parent_field"]}' doesn't exists";	
													}
													else{	
														if($bean->field_defs[$dependency["parent_field"]]["type"] != "enum"){
															$message[$key]['code'] = '110';
															$message[$key]['message'] = "Parent_field '{$dependency["field"]}' is not dropdown (enum)";	
														}
														else{				
															$field_opiton = $app_list_strings[$bean->field_defs[$dependency["field"]]["options"]];	
															if(empty($field_opiton) and !is_array($field_opiton) ){
																$message[$key]['code'] = '111';
																$message[$key]['message'] = "'Field' dropdown is empty, contact Sugar administrator!";														
															}
															else{
																$parent_option = $app_list_strings[$bean->field_defs[$dependency["parent_field"]]["options"]];
																if(empty($parent_option) and !is_array($field_opiton) ){
																	$message[$key]['code'] = '112';
																	$message[$key]['message'] = "'Parent field' dropdown is empty, contact Sugar administrator!";														
																}
																else{		
																	$field_option_keys = array();		
																	foreach(array_values($dependency["dependency"]) as $element){
																		foreach(array_values($element) as $value){
																			$field_option_keys[] = $value;
																		}
																	}
																	$field_option_keys = array_values(array_unique($field_option_keys));
																	$parent_option_keys = array_unique(array_keys($dependency["dependency"]));
																	
																	$assert = true;
																	foreach($field_option_keys as $value){
																		if( !array_key_exists($value, $field_opiton)){
																			 $assert = false;
																			 break;
																		}
																	}																	
																	
																	if(!$assert){
																		$message[$key]['code'] = '113';
																		$message[$key]['message'] = "Dependency's values don't exist on 'field' dropdown";	
																	}
																	else{
																		$assert = true;
																		foreach($parent_option_keys as $value){
																			if( !array_key_exists($value, $parent_option)){
																				 $assert = false;
																				 break;
																			}
																		}
																		
																		if(!$assert){
																			$message[$key]['code'] = '114';
																			$message[$key]['message'] = "Dependency's values don't exist on 'parent_field' dropdown";	
																		}
																		else{
																			$request = json_encode($dependency);
																			$status = "queued";
																			[$message[$key]['code'], $message[$key]['message']] = $this->queue($request,$status);
																			
																		}
																	}	
																}
															}	
														}
													}
												}
											}																			
										}
									}
								}					
							}
						}
					}
				}
			}

			return $message;
		}
		catch (Exception $e){
			$message['code'] = '100';
			$message['message'] = 'FatalError: '.$e->getMessage();
			return $message;
		}		
	}
	
	function queue($request,$status) : array{	
		try{
			//$message['message'] = json_decode(  html_entity_decode($request)  );
			global $db;
			$request = $this->sanitize($request);
			$status = $this->sanitize($status);
			
			if(!$db->query("CREATE TABLE IF NOT EXISTS `ws_dropdown_dependency` (`id`  INT NOT NULL AUTO_INCREMENT, `request` text NOT NULL, `status` varchar(255) NOT NULL, PRIMARY KEY (id) ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;")){				
				$message['code'] = '115';
				$message['message'] = "FatalError: mysql error creating 'ws_dropdown_dependency' table!!!";
			}
			$lastDbError = $db->lastDbError();
			if($lastDbError){
				$message['code'] = '116';
				$message['message'] = "FatalError: mysql error!!!";
				return array($message['code'], $message['message']);
			}
			
			$db->query("INSERT INTO `ws_dropdown_dependency` (`request`, `status`) VALUES ('{$request}', '{$status}');");
			$lastDbError = $db->lastDbError();
			if($lastDbError){
				$message['code'] = '117';
				$message['message'] = "FatalError: mysql error inserting records on 'ws_dropdown_dependency' table!!!";
				return array($message['code'], $message['message']);
			}
			
			$result = $db->query("select id from `ws_dropdown_dependency` where `request` = '{$request}' and `status` = '{$status}' order by id desc limit 1");
			$lastDbError = $db->lastDbError();
			if($lastDbError){
				$message['code'] = '118';
				$message['message'] = "FatalError: mysql error!!!";
				return array($message['code'], $message['message']);
			}
			
			$row=$db->fetchByAssoc($result);
			$message['code'] = '200';
			$message['message'] = "Request queued with id: {$row["id"]}";
			return array($message['code'], $message['message']);
		}
		catch (Exception $e){
			$message['code'] = '100';
			$message['message'] = 'FatalError: '.$e->getMessage();
			return array($message['code'], $message['message']);
		}
	}
	public function ws_dropdown_dependency_get($api, $args ){
		try{
			global $db;
			$return = true;
			$message =array(
			);
			
			
			if (!isset($args["lote"]) and !array_key_exists("lote",$args) and empty($args["lote"])) {
				$limit = "";
			}
			else{
				$limit = " limit {$args["lote"]} ";
			}
			
			$result = $db->query("select `id`,`request`, `status` from `ws_dropdown_dependency` where `status` = 'queued' order by id asc {$limit}");
			$lastDbError = $db->lastDbError();
			if($lastDbError){
				$message['code'] = '119';
				$message['message'] = "FatalError: mysql error selecting records from 'ws_dropdown_dependency' table with a limit of '{$limit}'!!!";
				return array($message['code'], $message['message']);
			}
			
			$rows = array();
			while($row = $db->fetchByAssoc($result)){
				$rows[] = $row;
			}
			
			if(empty($rows)){
				$message['code'] = '120';
				$message['message'] = "No records to process";
				return array($message['code'], $message['message']);
			}
			
			$message['code'] = '200';
			$message['message'] = $rows;
			return $message;
		}
		catch (Exception $e){
			$message['code'] = '100';
			$message['message'] = 'FatalError: '.$e->getMessage();
			return $message;
		}
	}
	
	public function ws_dropdown_dependency_set($api, $args ){
		try{
			global $db;
			$return = true;
			$message =array(
			);
			
			if (!isset($args["id"]) and !array_key_exists("id",$args) and empty($args["id"])) {	
					$message['code'] = '121';
					$message['message'] = "FatalError: missing id on the request";
					return array($message['code'], $message['message']);			
			}
			else{				
				$db->query("update `ws_dropdown_dependency` set `status` = 'done' where id = '{$args["id"]}';");
				$lastDbError = $db->lastDbError();
				if($lastDbError){
					$message['code'] = '122';
					$message['message'] = "FatalError: mysql error updating status 'done' on record with id '{$args["id"]}' on table 'ws_dropdown_dependency'!!!";
					return array($message['code'], $message['message']);
				}
			}
			
			$message['code'] = '200';
			$message['message'] = "Request {$args["id"]} was mark as '{$status}'";
			return $message;
		}
		catch (Exception $e){
			$message['code'] = '100';
			$message['message'] = 'FatalError: '.$e->getMessage();
			return $message;
		}
	}
	
	function sanitize($string):string{
		return filter_var(stripslashes(trim($string)), FILTER_SANITIZE_STRING);
	}
	
	function testDepth($input, $startDepth = 0) {
		//funte https://stackoverflow.com/questions/1951241/how-to-properly-calculate-the-amount-of-levels-in-an-array
		if (is_array($input)) {
			$max = $startDepth;
			foreach ($input as $i) {
				// Check what the depth of the given element is
				$result = $this->testDepth($i, $startDepth + 1);
				// We only care about the maximum value
				if ($result > $max) {
					$max = $result;
				}
			}
			return $max;
		} 
		else {
			// This isn't an array, so it's assumed not to be a container.
			// This doesn't add any depth to the parent array, so just return $startDepth
			return $startDepth;
		}
	}
}
?>

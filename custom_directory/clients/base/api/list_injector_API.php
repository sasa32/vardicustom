<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class list_injector_API extends SugarApi
{
	var $db = null;
	var $args = null;
	public function registerApiRest()
	{
		return array(
			'list_injector' => array(
				'reqType' => array(
					'PUT'
				) ,
				'noLoginRequired' => false,
				'path' => array(
					'list_injector'
				) ,
				'pathVars' => array(
					'list'
				) ,
				'method' => 'list_injector',
				'shortHelp' => 'list_injector',
				'longHelp' => 'custom/clients/base/api/help/list_injector_API.html',
			) ,
		);
	}
	
	public function list_injector($api, $args)
	{
		$retorno = array();
		require_once('modules/Studio/DropDowns/DropDownHelper.php');	
		global $app_list_strings;	
			
		$list_injector = $args["list_injector"];
		foreach($list_injector as $key => $list){
			$app_strings = return_application_language($current_language);
			$option_list = $app_list_strings[$list["option"]];
			if(!empty($option_list)){

				if ($list["option"]=="sasa_motivo_c_list") {
					ksort($option_list);
					$ultimo = $this->endKey($option_list);
					$ultimonuevo = $ultimo+1;
					if (in_array($list["value"], $option_list)) {
						$GLOBALS['log']->security("Ya existe un valor visible asi");
						
						//Insertar en la tabla diccionario
						$this->insert_diccionary_motivo($list["key"],$list["value"],$ultimo);
						//Retorno pero no inserción en lista de motivo ya que existe un valor visible con el que se intenta isnertar
						$list["status"] = "200";
						$retorno["list_injector"][] = $list;
						return $retorno;
					}else{
						$GLOBALS['log']->security("No existe un valor visible asi");
						//Insertar en la tabla diccionario de motivos
						$this->insert_diccionary_motivo($list["key"],$list["value"],$ultimonuevo);
						/*
						* Agregando nuevo valor a motivo
						*/				
						$option_list[$ultimonuevo] = $list["value"];//Nuevo elemento de la lista
						asort($option_list);//ordenando
						$option_list_final = $option_list;
						//$GLOBALS['log']->security(print_r($option_list_final,true));

					}
				}else{
					/*
					* Agregando nuevo valor 
					*/				
					$GLOBALS['log']->security("Criterio normal");
					$option_list[$list["key"]] = $list["value"];//Nuevo elemento de la lista
					asort($option_list);//ordenando
					$option_list_final = $option_list;
				}
				
				
				/*
				* Guardando cambios a la lista
				* https://support.sugarcrm.com/Documentation/Sugar_Developer/Sugar_Developer_Guide_10.0/Architecture/Languages/Managing_Lists/
				*/

				//Proceso independiente recolectará lo enviado desde el consumo a una tabla.
				//Tarea programada se encargará de verificar cuales listas está en pending para ejecutar la función save_custom_dropdown_strings() función para insertar valores en lista desplegable
				$this->save_list($list["option"],$option_list_final);

				//$GLOBALS['log']->security("OPTIONLIST ".print_r($option_list_final,true));

				//save_custom_dropdown_strings(array($list["option"] => $option_list_final),"es_ES",true);	
				$GLOBALS['log']->security("YAAAAAAA");
				/*
				* Resultado
				*/			
				$list["status"] = "200";
			}
			else{
				$list["status"] = "No existe el 'option'";
			}
			$retorno["list_injector"][] = $list;
		}
				
		/*
		* Reparando
		*/		
		/*$repairAndClear = new RepairAndClear();
		$actions = array('rebuildExtensions','clearJsLangFiles');
		$modules = array_keys($app_list_strings['moduleList']);		
		$repairAndClear->clearLanguageCache();
		$repairAndClear->repairAndClearAll($actions, $modules, false, false, '');	*/		
		
		return $retorno;
	}

	function save_list($list,$option){
		//Crear tabla si no existe
		$Query_create_table = "CREATE TABLE IF NOT EXISTS list_custom (
			id CHAR(36) PRIMARY KEY,
			fecha datetime,
			list VARCHAR(36),
			values_list text,
			status VARCHAR(36))";

		//Ejecutar query de creación de tabla
		$GLOBALS['db']->query($Query_create_table);

		$fechaenvio = date('Y-m-d H:i:s');

		$option = json_encode($option);
		$str_option = addslashes($option);
		//$GLOBALS['log']->security("ADDSLASHES ".$str_option);
		$Query_InsertValues = "INSERT INTO list_custom (id,fecha,list,values_list,status) VALUES (UUID(),'$fechaenvio','{$list}','{$str_option}','Pending')";
		//Ejecutar para insertar datos
		$GLOBALS['db']->query($Query_InsertValues);
	}

	function endKey($array){
	    //Aquí utilizamos end() para poner el puntero
	    //en el último elemento, no para devolver su valor
	    end($array);
	    return key($array);
	}

	public function insert_diccionary_motivo($codmotivo,$etiqueta,$motivo_sugar){
		//Asigando valores segun la estuctura de relacion de motivos de SIGRU
		$tipoSIGRU = substr($codmotivo, 0, 1);
		$compania = substr($codmotivo, 1, 1);
		$unidad = substr($codmotivo, 2, 2);
		$subunidad = substr($codmotivo, 4, 2);
		$motivo = substr($codmotivo, 6, 8);
		$motivosinespacios = str_replace(" ", "", $etiqueta);
		$codigomotivoSIGRU = substr($codmotivo, 1);

		switch ($tipoSIGRU) {
			case 'F':
				$tipoSUgar = "H";
				break;
			case 'P':
				$tipoSUgar = "A";
				break;
			case 'Q':
				$tipoSUgar = "B";
				break;
			case 'R':
				$tipoSUgar = "C";
				break;
			case 'S':
				$tipoSUgar = "D";
				break;
			default:
				$tipoSUgar = $tipoSIGRU;
				break;
		}
		$codmotivoSugar = $compania.$unidad.$subunidad.$motivo_sugar;

		//Insertar motivo en tabla de diccionario
		$QueryInsert = "REPLACE INTO diccionario_motivos (CodSigru,tipo, codigo, motivo, compania, unidadnegocio, subunidad, numotivo, estado, Sigru_sin_espacios, codigo_motivo_sugar, tipo_sugar, Codigo_Sugar) VALUES ('{$codmotivo}','{$tipoSIGRU}','{$codigomotivoSIGRU}','{$etiqueta}','{$compania}','{$unidad}','{$subunidad}','{$motivo}','A','{$motivosinespacios}','{$motivo_sugar}','{$tipoSUgar}','{$codmotivoSugar}')";
		
		//Ejecutar query de inserción 
		$GLOBALS['db']->query($QueryInsert);

	}
}

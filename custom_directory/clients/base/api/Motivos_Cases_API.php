<?php 
if ( !defined( 'sugarEntry' ) || !sugarEntry )die( 'Not A Valid Entry Point' );

class Motivos_Cases_API extends SugarApi {
	public function registerApiRest() {
		return array(
			//GET & POST
			'ws_motivos_cases' => array(
				'reqType' => array( 'POST' ),
				'noLoginRequired' => true,
				'path' => array( 'ws_motivos_cases' ), 
				'pathVars' => array( '' ),
				'method' => 'ws_motivos_cases',
				'shortHelp' => 'ws_motivos_cases',
				'longHelp' => 'custom/clients/base/api/help/ws_motivos_cases_help.html',
			),
		);
	}

	public function ws_motivos_cases($api, $args ){

		try{
			global $db;
			//Mapeo de la compañia, unidad y subunidad de negocio.
			$compania = $args['compania'];
			$unidadneg = $args['unidadneg'];
			$subunidadneg = $args['subunidadneg'];
			$codigo_motivo_sugar = $args['codigo_motivo_sugar'];
			$tipo_sugar = $args['tipo_sugar'];

			$querymotivo = $db->query("SELECT * FROM diccionario_motivos WHERE compania = '{$compania}' AND unidadnegocio='{$unidadneg}' AND subunidad='{$subunidadneg}' AND codigo_motivo_sugar='{$codigo_motivo_sugar}' AND tipo_sugar='{$tipo_sugar}' AND estado='A';");
			//$GLOBALS['log']->security("Query: SELECT * FROM diccionario_motivos WHERE compania = '{$compania}' AND unidadnegocio='{$unidadneg}' AND subunidad='{$subunidadneg}' AND codigo_motivo_sugar='{$codigo_motivo_sugar}' AND tipo_sugar='{$tipo_sugar}' AND estado='A';");
			$record = array();
			while ($row = $db->fetchByAssoc($querymotivo)) {
				$record[] = array(
					"codigo" => $row['codigo']
				);
			}
			if (empty($record)) {
				$record[] = array(
					"codigo" => "No hay"
				);
			}
			return $record;
		}catch (Exception $e){
			return array('Error' => '101', 'Description' => 'FatalError: '.$e->getMessage());
		}
		
	}
}







?>
<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class vehiculos_cstm_api extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            //GET
            'MyGetEndpoint' => array(
                //request type
                'reqType' => 'POST',

                //set authentication
                'noLoginRequired' => false,

                //endpoint path
                'path' => array('Vehiculos_cstm'),

                //endpoint variables
                'pathVars' => array(''),

                //method to call
                'method' => 'Vehiculos_cstm',

                //short help string to be displayed in the help documentation
                'shortHelp' => 'Api post and put of Vehiculos_cstm',

                //long help to be displayed in the help documentation
                'longHelp' => 'custom/clients/base/api/help/MyEndPoint_MyGetEndPoint_help.html',
            ),
        );
    }

    /**
     * Method to be used for my MyEndpoint/GetExample endpoint
     */
    public function Vehiculos_cstm($api, $args)
    {
        //custom logic
        $data = array();

        try {

            $data = $this->vehiculos_method($args); 

        } catch (Exception $e) {
            
            $data = array(
                "messageError" => $e->getMessage(),
            );

        }
    
        return $data;
    }//metodo nativo

    function vehiculos_method($args){

            $GLOBALS['log']->security("SUCCESS: " . print_r($args, true));

            $response = array(
                "vin"=> "599",
                "id"=> "",
                "response" => array(
                    "status" => "OK",
                    "id" => "200",
                    "mensaje" =>"Insertado Correctamente"
                ),
            );

            /*$response['vin'] = "500";
            $response['id'] = "";
            $response['response']['status'] = "ok";
            $response['response']['id'] = "ok";
            $response['response']['message'] = "Insertado Correctamente";*/
        
            return $response;

        }//function

}

?>
<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class pedidos_cstm_api extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            //GET
            'MyGetEndpoint' => array(
                //request type
                'reqType' => 'POST',

                //set authentication
                'noLoginRequired' => false,

                //endpoint path
                'path' => array('pedidos_cstm'),

                //endpoint variables
                'pathVars' => array(''),

                //method to call
                'method' => 'pedidos_cstm',

                //short help string to be displayed in the help documentation
                'shortHelp' => 'Api post and put of pedidos_cstm',

                //long help to be displayed in the help documentation
                'longHelp' => 'custom/clients/base/api/help/MyEndPoint_MyGetEndPoint_help.html',
            ),
        );
    }

    /**
     * Method to be used for my MyEndpoint/GetExample endpoint
     */
    public function pedidos_cstm($api, $args)
    {
        //custom logic
        $data = array();

        try {

            $data = $this->pedidos_method($args); 

        } catch (Exception $e) {
            
            $data = array(
                "messageError" => $e->getMessage(),
            );

        }
    
        return $data;
    }//metodo nativo

        /*Validar si el campo viene en el json*/
    private function validate_field($args){
        
        $check_index = array(
            'required' => true,
        );

        $fieldsEmptyLine = array();
        $contador = 0;
        //Agregar los campos que son requeridos de $args['pedidos']
        $requeridos = array(
            "sasa_numero_pedido_c"=>"sasa_numero_pedido_c", 
            "account_id"=>"account_id"
        );
        
        $resultado = array_diff_key($requeridos, $args);
        $resultado =  implode(",", $resultado);

        $GLOBALS['log']->security("implode: " . $resultado);
        //**Termina de validar si faltan campos en pedidos

        if(!empty($resultado)){

            $check_index['required'] = false;
            $check_index['message'] .= "Does not exist, " .  "Pedidos: " . $resultado  . ". ";
        }
        
        /*Validar campos vacios de líneas y cotización*/
        
        $field_empty = $this->validate_field_empty($args, $requeridos);

        if(!empty($field_empty)){
        
        $GLOBALS['log']->security("Requeridos Vaciós pedidos: " . $field_empty);

            $check_index['required'] = false;
            $check_index['message'] .= "Empty fields in, " .  "Pedido: " . $field_empty . ".";
    
        }

        return $check_index;
    }//validate_field

    // valida los campos vacios de pedidos 
    function validate_field_empty($args, $requeridos){

        $requeridosEmpty = array();

        foreach ($requeridos as $key => $value) {
            // code...
            if(empty($args[$key])){
                $requeridosEmpty[] = $value;
            }
        }//validar campos vacios

        return  implode(",", $requeridosEmpty);
    }// calidate_field_empty

    function pedidos_method($args){

            //$GLOBALS['log']->security("SUCCESS: " . print_r($args, true));

            $check_index = $this->validate_field($args);

            $GLOBALS['log']->security("SUCCESS: " . print_r($check_index, true));

            require_once 'custom/modules/PurchasedLineItems/sasa_classes/class_purchasedlineitems.php';

            $purchasedLineItems = new class_purchasedLineItems($args);
        
        //Si van a realizar un Updapte de pedido 
         
        if(empty($args['id_pedido'])){

            if($check_index['required']){
                
                $id_purchasedLineItems = $purchasedLineItems->createPurchasedLineItems();

                   $response = $this->responseCode(
                        $id_purchasedLineItems, 
                        "OK", 
                        200, 
                        "Inserto correctamente el Pedido");
                
            }else{

                    $response = $this->responseCode(
                    "",  
                    "ERROR", 
                    406, 
                    "The main index are not in your json: {$check_index['message']}"
                    );  
            }
        
        }else{
            $GLOBALS['log']->security("INGRESO A ACTUALIZAR::::: ");

            //En caso de actualizar el pedido

            $id_pedido = $purchasedLineItems->updatePurchasedLineItems($args['id_pedido']);
            
            if(!empty($id_pedido["id_pedido"])){

                $response = $this->responseCode(
                    $id_pedido["id_pedido"], 
                    "OK", 
                    200, 
                    "Se Actualizó Correctamente el Pedido");

            }else if(strcmp($id_pedido, "No existe") == 0){

                $response = $this->responseCode(
                    $id_pedido, 
                    "error", 
                    404, 
                    "No existe el: id del pedido.");

            }
        }

        
            return $response;

        }//function

        //mensaje de respuesta.
    function responseCode($id_pedido, $status, $code, $mensaje){

        return  $response = array(
                        "id_pedido"=> $id_pedido,
                        "response" => array(
                            "status" => $status,
                            "code" => $code,
                            "mensaje" => $mensaje
                        ),
                    );
    }

}

?>
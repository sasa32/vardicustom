<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class contratos_cstm_api extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            //GET
            'MyGetEndpoint' => array(
                //request type
                'reqType' => 'POST',

                //set authentication
                'noLoginRequired' => false,

                //endpoint path
                'path' => array('contratos_cstm'),

                //endpoint variables
                'pathVars' => array(''),

                //method to call
                'method' => 'contratos_cstm',

                //short help string to be displayed in the help documentation
                'shortHelp' => 'Api post and put of contratos_cstm',

                //long help to be displayed in the help documentation
                'longHelp' => 'custom/clients/base/api/help/MyEndPoint_MyGetEndPoint_help.html',
            ),
        );
    }

    /**
     * Method to be used for my MyEndpoint/GetExample endpoint
     */
    public function contratos_cstm($api, $args)
    {
        //custom logic
        $data = array();

        try {

            $data = $this->contratos_method($args); 

        } catch (Exception $e) {
            
            $data = array(
                "messageError" => $e->getMessage(),
            );

        }
    
        return $data;
    }//metodo nativo

    function contratos_method($args){

        $GLOBALS['log']->security("SUCCES: " . print_r($args, true));

           $response = array(
                "name"=> "nombre del contrato",
                "id"=> "contrato_id",
                "response" => array(
                    "status" => "OK",
                    "id" => "200",
                    "mensaje" =>"Insertado Correctamente el Contrato"
                ),
            );

            /*$response['vin'] = "500";
            $response['id'] = "";
            $response['response']['status'] = "ok";
            $response['response']['id'] = "ok";
            $response['response']['message'] = "Insertado Correctamente";*/
        
            return $response;

        }//function

}

?>
<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class cotizacion_cstm_api extends SugarApi
{
    private $cantidad;

    public function registerApiRest()
    {
        return array(
            //GET
            'MyGetEndpoint' => array(
                //request type
                'reqType' => 'POST',

                //set authentication
                'noLoginRequired' => false,

                //endpoint path
                'path' => array('cotizacion_cstm'),

                //endpoint variables
                'pathVars' => array(''),

                //method to call
                'method' => 'cotizacion_cstm',

                //short help string to be displayed in the help documentation
                'shortHelp' => 'Api post and put of cotizacion_cstm',

                //long help to be displayed in the help documentation
                'longHelp' => 'custom/clients/base/api/help/MyEndPoint_MyGetEndPoint_help.html',
            ),
        );
    } //registrerApiRest

    /**
     * Method to be used for my MyEndpoint/GetExample endpoint
     */
    public function cotizacion_cstm($api, $args)
    {
        //custom logic
        $data = array();

        try {

            $data = $this->cotizacion_method($args); 

        } catch (Exception $e) {
            
            $data = array(
                "messageError" => $e->getMessage(),
            );

        }
    
        return $data;
    }//metodo nativo de respuesta

    /*Validar si el campo viene en el json*/
    private function validate_field($args){
        
        $check_index = array(
            'required' => true,
        );
        $camposLineas = array();
        $camposLineas2 = array();
        $fieldsEmptyLine = array();
        $contador = 0;
        //Agregar los campos que son requeridos de $args['cotizacion']
        $requeridos = array(
            "name"=>"name", 
            "account_id"=>"account_id"
        );
        //Agregar los campos que son requeridos de $args['linea']
        $requeridosLineas = array(
            "name"=>"name",
            "quantity"=>"quantity",
            "likely_case"=>"likely_case", 
            "book_value"=>"book_value",
            "list_price"=>"list_price"
        );

        $GLOBALS['log']->security("SUCCES: Lineas3: " . print_r($requeridosLineas, true));
       
        $cantidad = count($args['linea']);
        $GLOBALS['log']->security("Lineas cantidad: ".$cantidad);

        //**Empieza a validar si faltan campos en líneas y cotizaciones
        for ($i=0; $i < $cantidad; $i++) { 
            foreach ($args['linea'][$i] as $key => $value) {
                //$camposLineas[] = $key;
                //$GLOBALS['log']->security("key: " . $key . " valor: " . $value);
                
                //validar los campos si estan vaciós
                if(empty($value) && $key === 'name'){

                    $GLOBALS['log']->security("********Campo vacio*******" . $key);
                    $fieldsEmptyLine[] =  "[{$i}]" . $key;
                    
                }

                $resultadoLineas = array_diff_key($requeridosLineas, $args['linea'][$i]);
            }
            
            //$GLOBALS['log']->security("SUCCES: Lineas for2: " . print_r($resultadoLineas, true));
            $camposLineas[] = $resultadoLineas;
        
        }
        
        
        for ($i=0; $i < $cantidad; $i++) {
             foreach ($camposLineas[$i] as $key => $value) {
                $GLOBALS['log']->security("key: " . $key . " valor: " . $value);
                $camposLineas2[$contador] = $key;
                $contador++;
             }
        }

        $GLOBALS['log']->security("SUCCES: Lineas Resul33: " . print_r($camposLineas2, true));
        
        $resultado = array_diff_key($requeridos, $args['cotizacion']);
        $resultadoLineas = array_intersect($requeridosLineas, $camposLineas2);

        $GLOBALS['log']->security("SUCCES: Lineas Resul44: " . print_r($resultadoLineas, true));

        $resultado =  implode(",", $resultado);
        $resultadoLineas =  implode(" , ", $resultadoLineas);

        $GLOBALS['log']->security("implode: " . $resultadoLineas);
        
        //**Termina de validar si faltan campos en líneas y cotizaciones

        if(!empty($resultado) || !empty($resultadoLineas)){

            $check_index['required'] = false;
            $check_index['message'] .= "Does not exist, " .  "Cotización: " . $resultado  . " Líneas: " . $resultadoLineas . ". ";
        }

        /*Validar campos vacios de líneas y cotización*/
        
        $field_empty = $this->validate_field_empty($args, $requeridos);

        $GLOBALS['log']->security("Campos vacios Lineas: " . print_r($fieldsEmptyLine, true));

        $camposVaciosLineas = implode(" , ", $fieldsEmptyLine);

        if(!empty($field_empty) || !empty($camposVaciosLineas)){
        
        $GLOBALS['log']->security("Requeridos Vaciós Cotización: " . $field_empty);

            $check_index['required'] = false;
            $check_index['message'] .= "Empty fields in, " .  "Cotización: " . $field_empty  . " Líenas: " . $camposVaciosLineas . ".";
    
        }
        

        return $check_index;
    }//validate_field

    // valida los campos vacios de cotizaciones 
    function validate_field_empty($args, $requeridos){

        $requeridosEmpty = array();

        foreach ($requeridos as $key => $value) {
            // code...
            if(empty($args['cotizacion'][$key])){
                $requeridosEmpty[] = $value;
            }
        }

        

        return  implode(",", $requeridosEmpty);
    }// calidate_field_empty

    function cotizacion_method($args){

        $id_cotizacion = "";
        $opportunitie = null;
        $id_linea = array();

        //$GLOBALS['log']->security("SUCCES: " . print_r($args, true));
        $GLOBALS['log']->security("SUCCES: " . print_r($args['linea'], true));

        
        $check_index = $this->validate_field($args);

        require_once 'custom/modules/Opportunities/sasa_classes/class_opportunities.php';

        $opportunitie = new class_opportunities($args);

        $GLOBALS['log']->security("SUCCES id Cotización: " . $args['cotizacion']['id_cotizacion']);

        //Si van a realizar un Updapte de cotización o de las líneas 
         
        if(empty($args['cotizacion']['id_cotizacion'])){

            if(empty($args['linea'])){
                    $response = $this->responseCode(
                    "", 
                    "", 
                    "ERROR", 
                    406, 
                    "Empty entry lines."
                    );
                    return $response;  
            }

            if($check_index['required']){
                
                $id_cotizacion = $opportunitie->createOpportinitie();
                
                $GLOBALS['log']->security("SUCCES OPPORTUNITIE: " . print_r($id_cotizacion, true));

                   $response = $this->responseCode(
                        $id_cotizacion['id_cotizacion'], 
                        $id_cotizacion['linea'], 
                        "OK", 
                        200, 
                        "Inserto correctamente la cotización");
                
            }else{

                    $response = $this->responseCode(
                    "", 
                    "", 
                    "ERROR", 
                    406, 
                    "The main index are not in your json: {$check_index['message']}"
                    );  
            }

        }else{
            
            $GLOBALS['log']->security("INGRESO A ACTUALIZAR::::: ");
            $cantidad = count($args['linea']);
            $cantidaQuery = count($opportunitie->queryLine($args['cotizacion']['id_cotizacion']));
            $GLOBALS['log']->security("Cantidad Json: " . $cantidad);
            $GLOBALS['log']->security("Cantidad Consulta: " . $cantidaQuery);

            if($cantidad > 0 && $cantidad < $cantidaQuery){
                    
                    $response = $this->responseCode(
                    $args['cotizacion']['id_cotizacion'], 
                    "", 
                    "ERROR", 
                    406, 
                    "The Json contains {$cantidad} lines and the quote has {$cantidaQuery} lines"
                    );
                    return $response; 
            }
            //En caso de actualizar la cotización
            //$cantidad = count($args['linea']);
            //$opportunitie->queryLine($args['cotizacion']['id_cotizacion'])
                $id_cotizacion = $opportunitie->updateOpportinitie($args['cotizacion']['id_cotizacion']);
          
                $GLOBALS['log']->security("RESPONSE COTIZACION Y LIENASs: " . print_r($id_cotizacion, true));

            if(!empty($id_cotizacion['id_cotizacion'])){

                    $response = $this->responseCode(
                    $id_cotizacion['id_cotizacion'], 
                    $id_cotizacion['linea'], 
                    "OK", 
                    200, 
                    "Se Actualizó Correctamente la Cotización"
                    ); 
            
                    
                
            }else if(strcmp($id_cotizacion, "No existe") == 0){

                    if(strcmp($id_cotizacion, "No existe") == 0){
                        $mensaje .= "id de la cotización.";
                    }

                    $response = array(
                        "id_cotizacion"=> "{$id_cotizacion}",
                        "id_linea"=> $id_linea,
                        "response" => array(
                            "status" => "error",
                            "code" => 404,
                            "mensaje" => "No existe el: " . $mensaje
                        ),
                    );

            }

        } //else para actualizar id cotización
        
        return $response;

    }//function cotizacion_method

    function responseCode($id_cotizacion, $id_linea, $status, $code, $mensaje){


        return  $response = array(
                        "id_cotizacion"=> $id_cotizacion,
                        "id_linea"=> $id_linea,
                        "response" => array(
                            "status" => $status,
                            "code" => $code,
                            "mensaje" => $mensaje
                        ),
                    );
    }

}//Clase

?>
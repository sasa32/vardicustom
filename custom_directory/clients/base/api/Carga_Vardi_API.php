<?php 
if ( !defined( 'sugarEntry' ) || !sugarEntry )die( 'Not A Valid Entry Point' );

class Carga_Vardi_API extends SugarApi {
	public function registerApiRest() {
		return array(
			//GET & POST
			'ws_carga_vardi' => array(
				'reqType' => array( 'POST' ),
				'noLoginRequired' => true,
				'path' => array( 'ws_carga_vardi' ), 
				'pathVars' => array( '' ),
				'method' => 'ws_carga_vardi',
				'shortHelp' => 'ws_carga_vardi',
				'longHelp' => 'custom/clients/base/api/help/ws_ws_carga_vardi_post_help.html',
			),
		);
	}

	public function ws_carga_vardi($api, $args ){

		try{
			//Validación sobre el indice de metodo
			if(!$args['method']) return array('Error' => '102', 'Description' => 'Did not have method index in your JSON');
			//Validación sobre el indice de data
			if(!$args['data']) return array('Error' => '103', 'Description' => 'Did not have data index in your JSON');
			if($args['method'] == "Cuentas"){
				//Iniciando el proceso para Facturación por Rubros
				require_once( "custom/modules/Accounts/Integration_classes/loader_cuentas.php" );
				$Cuentas = new Loader_Cuentas($args['data']);
				return $Cuentas->get_return();
			}
			if($args['method'] == "Contactos" || $args['method']== "ContactosJuridica"){
				//Iniciando ejecución para Estados de Cuentas
				require_once( "custom/modules/Contacts/Integration_classes/loader_contacts.php" );
				$Contactos = new Loader_Contacts($args['data']);
				return $Contactos->get_return();
			}if($args['method'] == "Unidades"){
				//Iniciando ejecución para Estados de Cuentas
				require_once( "custom/modules/SASA_UnidadNegClienteProspect/Integration_classes/loader_unidades.php" );
				$Unidades = new Loader_Unidades($args['data']);
				return $Unidades->get_return();
			}
			if($args['method'] == "Habeas"){
				//Iniciando ejecución para Estados de Cuentas
				require_once( "custom/modules/SASA_Habeas_Data/Integration_classes/loader_habeas.php" );
				$Habeas = new Loader_Habeas($args['data']);
				return $Habeas->get_return();
			}
			if ($args['method'] == "HabeasUpdate") {
				//Iniciando ejecución para Estados de Cuentas
				require_once( "custom/modules/SASA_Habeas_Data/Integration_classes/loader_habeas.php" );
				$Habeas = new Loader_Habeas($args['data']);
				return $Habeas->get_return();
			}else{
				return array('Error' => '101', 'Description' => 'Did not specify method in your JSON');
			}
		}catch (Exception $e){
			return array('Error' => '101', 'Description' => 'FatalError: '.$e->getMessage());
		}
		
	}
}







?>
<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos Principal';
$mod_strings['LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Contactos Alternativo';
$mod_strings['LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos Principal';
$mod_strings['LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Clientes y Prospectos Principal';
$mod_strings['LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos Alternativo';
$mod_strings['LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Clientes y Prospectos Alternativo';
$mod_strings['LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Contactos Principal';
$mod_strings['LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_SASA_DEPARTAMENTOS_TITLE'] = 'Contactos Alternativo';
$mod_strings['LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_DEPARTAMENTOS_FOCUS_DRAWER_DASHBOARD'] = 'Departamentos Panel de enfoque';
$mod_strings['LBL_SASA_DEPARTAMENTOS_RECORD_DASHBOARD'] = 'Departamentos Cuadro de mando del registro';

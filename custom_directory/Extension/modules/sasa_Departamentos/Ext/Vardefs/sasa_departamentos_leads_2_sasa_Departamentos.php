<?php
// created: 2020-05-15 16:07:49
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_leads_2"] = array (
  'name' => 'sasa_departamentos_leads_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_leads_2',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

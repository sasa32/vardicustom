<?php
// created: 2020-05-15 15:42:47
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_accounts_1"] = array (
  'name' => 'sasa_departamentos_accounts_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_accounts_1sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

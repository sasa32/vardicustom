<?php
// created: 2020-05-15 16:06:23
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_contacts_2"] = array (
  'name' => 'sasa_departamentos_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_contacts_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

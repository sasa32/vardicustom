<?php
// created: 2020-05-15 15:42:24
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_sasa_municipios_1"] = array (
  'name' => 'sasa_departamentos_sasa_municipios_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_sasa_municipios_1',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

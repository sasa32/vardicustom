<?php
 // created: 2020-05-15 15:43:53
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

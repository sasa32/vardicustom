<?php
// created: 2020-05-15 16:11:54
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_4"] = array (
  'name' => 'sasa_municipios_accounts_4',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_4',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_4_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_4sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

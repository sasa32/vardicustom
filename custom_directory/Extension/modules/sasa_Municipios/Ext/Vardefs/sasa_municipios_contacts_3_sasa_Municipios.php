<?php
// created: 2020-05-15 16:12:58
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_3"] = array (
  'name' => 'sasa_municipios_contacts_3',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_3',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_3sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
// created: 2020-05-15 16:13:32
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_4"] = array (
  'name' => 'sasa_municipios_contacts_4',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_4',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_4sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

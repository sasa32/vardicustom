<?php
// created: 2020-05-15 15:44:33
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_1"] = array (
  'name' => 'sasa_municipios_accounts_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_1sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

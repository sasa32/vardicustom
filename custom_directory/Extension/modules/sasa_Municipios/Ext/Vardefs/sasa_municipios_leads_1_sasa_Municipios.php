<?php
// created: 2020-05-15 15:45:19
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_leads_1"] = array (
  'name' => 'sasa_municipios_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

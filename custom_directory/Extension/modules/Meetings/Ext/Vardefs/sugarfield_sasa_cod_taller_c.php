<?php
 // created: 2022-01-06 15:48:19
$dictionary['Meeting']['fields']['sasa_cod_taller_c']['labelValue']='Código Taller';
$dictionary['Meeting']['fields']['sasa_cod_taller_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_cod_taller_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_cod_taller_c']['dependency']='equal($sasa_tipo_reunion_c,"A")';

 ?>
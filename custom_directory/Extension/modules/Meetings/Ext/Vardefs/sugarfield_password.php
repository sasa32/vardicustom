<?php
 // created: 2020-09-22 15:43:03
$dictionary['Meeting']['fields']['password']['audited']=false;
$dictionary['Meeting']['fields']['password']['massupdate']=false;
$dictionary['Meeting']['fields']['password']['comments']='Meeting password';
$dictionary['Meeting']['fields']['password']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['password']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['password']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['password']['reportable']=false;
$dictionary['Meeting']['fields']['password']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['password']['calculated']=false;

 ?>
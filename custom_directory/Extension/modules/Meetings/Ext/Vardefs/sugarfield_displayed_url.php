<?php
 // created: 2020-09-22 15:43:36
$dictionary['Meeting']['fields']['displayed_url']['audited']=false;
$dictionary['Meeting']['fields']['displayed_url']['massupdate']=false;
$dictionary['Meeting']['fields']['displayed_url']['comments']='Meeting URL';
$dictionary['Meeting']['fields']['displayed_url']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['displayed_url']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['displayed_url']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['displayed_url']['reportable']=false;
$dictionary['Meeting']['fields']['displayed_url']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['displayed_url']['calculated']=false;
$dictionary['Meeting']['fields']['displayed_url']['gen']='';
$dictionary['Meeting']['fields']['displayed_url']['link_target']='_self';

 ?>
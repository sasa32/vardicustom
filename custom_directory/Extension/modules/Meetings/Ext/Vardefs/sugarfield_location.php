<?php
 // created: 2020-09-22 15:42:35
$dictionary['Meeting']['fields']['location']['audited']=false;
$dictionary['Meeting']['fields']['location']['massupdate']=false;
$dictionary['Meeting']['fields']['location']['comments']='Meeting location';
$dictionary['Meeting']['fields']['location']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['location']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['location']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['location']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.36',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['location']['calculated']=false;

 ?>
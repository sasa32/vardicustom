<?php
 // created: 2020-10-16 15:45:56
$dictionary['Meeting']['fields']['status']['default']='A';
$dictionary['Meeting']['fields']['status']['required']=true;
$dictionary['Meeting']['fields']['status']['audited']=true;
$dictionary['Meeting']['fields']['status']['massupdate']=true;
$dictionary['Meeting']['fields']['status']['options']='status_reu_list_c';
$dictionary['Meeting']['fields']['status']['comments']='Meeting status (ex: Planned, Held, Not held)';
$dictionary['Meeting']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['status']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['status']['calculated']=false;
$dictionary['Meeting']['fields']['status']['dependency']=false;
$dictionary['Meeting']['fields']['status']['full_text_search']=array (
);

 ?>
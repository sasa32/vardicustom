<?php
 // created: 2020-10-07 09:33:53
$dictionary['Meeting']['fields']['type']['len']=100;
$dictionary['Meeting']['fields']['type']['audited']=false;
$dictionary['Meeting']['fields']['type']['comments']='Meeting type (ex: WebEx, Other)';
$dictionary['Meeting']['fields']['type']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['type']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['type']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['type']['reportable']=false;
$dictionary['Meeting']['fields']['type']['calculated']=false;
$dictionary['Meeting']['fields']['type']['dependency']=false;

 ?>
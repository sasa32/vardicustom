<?php
 // created: 2022-01-06 15:48:18
$dictionary['Meeting']['fields']['nu_nit_empl_c']['labelValue']='Asesor';
$dictionary['Meeting']['fields']['nu_nit_empl_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['nu_nit_empl_c']['enforced']='';
$dictionary['Meeting']['fields']['nu_nit_empl_c']['dependency']='equal($sasa_tipo_reunion_c,"A")';

 ?>
<?php
 // created: 2020-09-22 15:48:48
$dictionary['Meeting']['fields']['parent_type']['len']='100';
$dictionary['Meeting']['fields']['parent_type']['audited']=false;
$dictionary['Meeting']['fields']['parent_type']['massupdate']=false;
$dictionary['Meeting']['fields']['parent_type']['comments']='Module meeting is associated with';
$dictionary['Meeting']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['parent_type']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['parent_type']['calculated']=false;

 ?>
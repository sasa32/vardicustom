<?php
 // created: 2020-09-22 15:41:37
$dictionary['Meeting']['fields']['description']['audited']=true;
$dictionary['Meeting']['fields']['description']['massupdate']=false;
$dictionary['Meeting']['fields']['description']['comments']='Full text of the note';
$dictionary['Meeting']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['description']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.55',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['description']['calculated']=false;
$dictionary['Meeting']['fields']['description']['rows']='6';
$dictionary['Meeting']['fields']['description']['cols']='80';

 ?>
<?php
 // created: 2022-01-06 15:48:19
$dictionary['Meeting']['fields']['sasa_tipo_evento_c']['labelValue']='Tipo de Evento';
$dictionary['Meeting']['fields']['sasa_tipo_evento_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_tipo_evento_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipo_reunion_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'A' => 
    array (
      0 => '',
      1 => 'A',
      2 => 'B',
      3 => 'F',
    ),
    'B' => 
    array (
      0 => '',
      1 => 'C',
      2 => 'D',
      3 => 'E',
    ),
  ),
);

 ?>
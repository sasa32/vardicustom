<?php
 // created: 2022-01-06 15:48:19
$dictionary['Meeting']['fields']['sasa_origen_cita_c']['labelValue']='Origen Cita';
$dictionary['Meeting']['fields']['sasa_origen_cita_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_origen_cita_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_origen_cita_c']['dependency']='equal($sasa_tipo_reunion_c,"A")';

 ?>
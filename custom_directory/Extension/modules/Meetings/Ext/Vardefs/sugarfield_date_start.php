<?php
 // created: 2020-09-22 15:46:06
$dictionary['Meeting']['fields']['date_start']['audited']=true;
$dictionary['Meeting']['fields']['date_start']['options']='';
$dictionary['Meeting']['fields']['date_start']['comments']='Date of start of meeting';
$dictionary['Meeting']['fields']['date_start']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_start']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['date_start']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_start']['full_text_search']=array (
);
$dictionary['Meeting']['fields']['date_start']['calculated']=false;
$dictionary['Meeting']['fields']['date_start']['enable_range_search']=false;

 ?>
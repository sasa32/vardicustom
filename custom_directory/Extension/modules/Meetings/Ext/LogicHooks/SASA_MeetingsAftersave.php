<?php
$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'SASA_MeetingsAftersave',

	//The PHP file where your class is located.
	'custom/modules/Meetings/SASA_MeetingsAftersave.php',

	//The class the method is in.
	'SASA_MeetingsAftersave',

	//The method to call.
	'after_save'
);

?>

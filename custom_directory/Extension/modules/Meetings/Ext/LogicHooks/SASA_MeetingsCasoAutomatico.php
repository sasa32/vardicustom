<?php
$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	2,

	//Label. A string value to identify the hook.
	'SASA_MeetingsCasoAutomatico',

	//The PHP file where your class is located.
	'custom/modules/Meetings/SASA_MeetingsCasoAutomatico.php',

	//The class the method is in.
	'SASA_MeetingsCasoAutomatico',

	//The method to call.
	'before_save'
);

?>

<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_TIPO_REUNION_C'] = 'Tipo de Reunión';
$mod_strings['LBL_SASA_TIPO_EVENTO_C'] = 'Tipo de Evento';
$mod_strings['LBL_SASA_COD_TALLER_C'] = 'Código Taller';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_ORIGEN_CITA_C'] = 'Origen Cita';
$mod_strings['LBL_DE_RECO_CITA'] = 'Canal de Recordatorio Cita';
$mod_strings['LBL_CD_GRUP'] = 'Motivo de la Visita';
$mod_strings['LBL_DE_OBSE'] = 'Detalle';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre de Asesor';
$mod_strings['LBL_DE_OBSE_CLIE'] = 'Otros Servicios';
$mod_strings['LBL_CASORELACION_C'] = 'Número de Caso relacionado';
$mod_strings['LBL_SASA_NOMBREAGENTE_C'] = 'Nombre Agente';

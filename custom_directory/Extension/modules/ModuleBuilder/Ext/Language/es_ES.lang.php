<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_JS_DELETE_REQUIRED_DDL_ITEM_NEW'] = '¿Está seguro de que quiere eliminar el estado de Nueva Venta? Esto puede puede provocar problemas en el workflow de Líneas de Ingreso en el módulo Cotizaciones.';
$mod_strings['LBL_JS_DELETE_REQUIRED_DDL_ITEM_IN_PROGRESS'] = '¿Está seguro de que quiere eliminar el estado de Venta en Progreso? Borrar este estado puede provocar problemas en el workflow de Líneas de Ingreso en el módulo Cotizaciones.';

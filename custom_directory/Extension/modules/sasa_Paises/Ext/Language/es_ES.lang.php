<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PAISES_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos Principal';
$mod_strings['LBL_SASA_PAISES_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Contactos Alternativo';
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos Principal';
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_1_FROM_SASA_PAISES_TITLE'] = 'Clientes y Prospectos Principal';
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_2_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos Alternativo';
$mod_strings['LBL_SASA_PAISES_ACCOUNTS_2_FROM_SASA_PAISES_TITLE'] = 'Clientes y Prospectos Alternativo';
$mod_strings['LBL_SASA_PAISES_CONTACTS_1_FROM_SASA_PAISES_TITLE'] = 'Contactos Principal';
$mod_strings['LBL_SASA_PAISES_CONTACTS_2_FROM_SASA_PAISES_TITLE'] = 'Contactos Alternativo';
$mod_strings['LBL_SASA_PAISES_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_PAISES_LEADS_2_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_SASA_PAISES_FOCUS_DRAWER_DASHBOARD'] = 'Países Panel de enfoque';
$mod_strings['LBL_DROPDOWN_FIELD'] = 'New Dropdown Field';
$mod_strings['LBL_PRUEBA_C'] = 'Prueba';
$mod_strings['LBL_SASA_PAISES_RECORD_DASHBOARD'] = 'Países Cuadro de mando del registro';

<?php
// created: 2020-05-15 16:04:53
$dictionary["sasa_Paises"]["fields"]["sasa_paises_contacts_2"] = array (
  'name' => 'sasa_paises_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_contacts_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

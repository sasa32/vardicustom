<?php
// created: 2020-05-15 15:55:44
$dictionary["sasa_Paises"]["fields"]["sasa_paises_sasa_departamentos_1"] = array (
  'name' => 'sasa_paises_sasa_departamentos_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_sasa_departamentos_1',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'vname' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

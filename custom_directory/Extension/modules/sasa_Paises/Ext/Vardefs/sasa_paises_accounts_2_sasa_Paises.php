<?php
// created: 2020-05-15 16:04:27
$dictionary["sasa_Paises"]["fields"]["sasa_paises_accounts_2"] = array (
  'name' => 'sasa_paises_accounts_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_accounts_2',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_2_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_accounts_2sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

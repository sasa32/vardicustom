<?php
 // created: 2020-05-15 16:04:27
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_accounts_2'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_ACCOUNTS_2_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'sasa_paises_accounts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

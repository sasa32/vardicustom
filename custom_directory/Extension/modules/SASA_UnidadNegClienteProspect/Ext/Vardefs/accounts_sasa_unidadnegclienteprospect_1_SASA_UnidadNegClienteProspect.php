<?php
// created: 2020-05-21 08:12:31
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["accounts_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'accounts_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'id_name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["accounts_sasa_unidadnegclienteprospect_1_name"] = array (
  'name' => 'accounts_sasa_unidadnegclienteprospect_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'link' => 'accounts_sasa_unidadnegclienteprospect_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["accounts_sasa_unidadnegclienteprospect_1accounts_ida"] = array (
  'name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE_ID',
  'id_name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'link' => 'accounts_sasa_unidadnegclienteprospect_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

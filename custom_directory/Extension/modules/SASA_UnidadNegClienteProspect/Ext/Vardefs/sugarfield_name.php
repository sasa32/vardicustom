<?php
 // created: 2020-05-27 16:21:24
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['len']='255';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['audited']=false;
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['massupdate']=false;
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['importable']='false';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['duplicate_merge']='disabled';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['merge_filter']='disabled';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['unified_search']=false;
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['calculated']='1';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['formula']='concat(related($sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1,"name")," - ",related($accounts_sasa_unidadnegclienteprospect_1,"name"))';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['name']['enforced']=true;

 ?>
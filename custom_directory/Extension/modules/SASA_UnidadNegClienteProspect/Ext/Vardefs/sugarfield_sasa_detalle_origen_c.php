<?php
 // created: 2021-04-21 14:49:43
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_detalle_origen_c']['labelValue']='Detalle de Origen';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_detalle_origen_c']['dependency']='';
$dictionary['SASA_UnidadNegClienteProspect']['fields']['sasa_detalle_origen_c']['visibility_grid']=array (
  'trigger' => 'sasa_origen_c',
  'values' => 
  array (
    1 => 
    array (
    ),
    2 => 
    array (
    ),
    3 => 
    array (
      0 => '',
      1 => '71',
    ),
    4 => 
    array (
      0 => '',
      1 => '1',
      2 => '3',
      3 => '2',
      4 => '4',
      5 => '5',
      6 => '73',
      7 => '74',
    ),
    5 => 
    array (
      0 => '',
      1 => '6',
      2 => '7',
      3 => '11',
      4 => '8',
      5 => '47',
      6 => '10',
    ),
    6 => 
    array (
      0 => '',
      1 => '15',
      2 => '12',
      3 => '13',
      4 => '16',
    ),
    8 => 
    array (
    ),
    9 => 
    array (
    ),
    10 => 
    array (
    ),
    11 => 
    array (
    ),
    13 => 
    array (
    ),
    14 => 
    array (
    ),
    15 => 
    array (
      0 => '',
      1 => '70',
    ),
    16 => 
    array (
    ),
    17 => 
    array (
    ),
    18 => 
    array (
      0 => '',
      1 => '18',
      2 => '57',
    ),
    19 => 
    array (
      0 => '',
      1 => '20',
      2 => '21',
    ),
    20 => 
    array (
      0 => '',
      1 => '23',
      2 => '22',
      3 => '79',
      4 => '69',
      5 => '67',
      6 => '68',
      7 => '25',
      8 => '24',
      9 => '76',
      10 => '77',
      11 => '78',
    ),
    21 => 
    array (
    ),
    22 => 
    array (
      0 => '',
      1 => '27',
      2 => '75',
      3 => '80',
    ),
    23 => 
    array (
      0 => '',
      1 => '28',
    ),
    24 => 
    array (
    ),
    25 => 
    array (
      0 => '',
      1 => '42',
      2 => '43',
      3 => '34',
      4 => '44',
    ),
    26 => 
    array (
    ),
    28 => 
    array (
    ),
    29 => 
    array (
      0 => '',
      1 => '31',
      2 => '32',
      3 => '52',
    ),
    30 => 
    array (
    ),
    31 => 
    array (
    ),
    32 => 
    array (
    ),
    33 => 
    array (
    ),
    34 => 
    array (
      0 => '',
      1 => '39',
    ),
    35 => 
    array (
    ),
    36 => 
    array (
      0 => '',
      1 => '40',
      2 => '51',
    ),
    37 => 
    array (
      0 => '',
      1 => '58',
      2 => '64',
    ),
    38 => 
    array (
      0 => '',
      1 => '45',
      2 => '50',
      3 => '46',
      4 => '72',
    ),
    39 => 
    array (
      0 => '',
      1 => '54',
      2 => '56',
      3 => '59',
      4 => '60',
      5 => '55',
    ),
    40 => 
    array (
      0 => '',
      1 => '49',
      2 => '9',
      3 => '81',
      4 => '48',
    ),
    41 => 
    array (
      0 => '',
      1 => '61',
      2 => '62',
    ),
    42 => 
    array (
      0 => '',
      1 => '63',
      2 => '64',
    ),
    43 => 
    array (
      0 => '',
      1 => '65',
      2 => '66',
    ),
    '' => 
    array (
    ),
  ),
);

 ?>
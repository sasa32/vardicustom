<?php
// created: 2020-05-21 08:13:25
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'sasa_Unidad_de_Negocio',
  'bean_name' => 'sasa_Unidad_de_Negocio',
  'side' => 'right',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'id_name' => 'sasa_unida3a6anegocio_ida',
  'link-type' => 'one',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name"] = array (
  'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'save' => true,
  'id_name' => 'sasa_unida3a6anegocio_ida',
  'link' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'table' => 'sasa_unidad_de_negocio',
  'module' => 'sasa_Unidad_de_Negocio',
  'rname' => 'name',
);
$dictionary["SASA_UnidadNegClienteProspect"]["fields"]["sasa_unida3a6anegocio_ida"] = array (
  'name' => 'sasa_unida3a6anegocio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE_ID',
  'id_name' => 'sasa_unida3a6anegocio_ida',
  'link' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'table' => 'sasa_unidad_de_negocio',
  'module' => 'sasa_Unidad_de_Negocio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

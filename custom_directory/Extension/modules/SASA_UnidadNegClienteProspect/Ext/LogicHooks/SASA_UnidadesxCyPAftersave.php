<?php
$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'SASA_UnidadesxCyPAftersave',

	//The PHP file where your class is located.
	'custom/modules/SASA_UnidadNegClienteProspect/SASA_UnidadesxCyPAftersave.php',

	//The class the method is in.
	'SASA_UnidadesxCyPAftersave',

	//The method to call.
	'before_save'
);

?>

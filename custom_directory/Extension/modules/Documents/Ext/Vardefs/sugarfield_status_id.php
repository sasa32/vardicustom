<?php
 // created: 2020-07-29 09:32:38
$dictionary['Document']['fields']['status_id']['default']='';
$dictionary['Document']['fields']['status_id']['audited']=true;
$dictionary['Document']['fields']['status_id']['massupdate']=true;
$dictionary['Document']['fields']['status_id']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['status_id']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['status_id']['merge_filter']='disabled';
$dictionary['Document']['fields']['status_id']['reportable']=true;
$dictionary['Document']['fields']['status_id']['calculated']=false;
$dictionary['Document']['fields']['status_id']['dependency']=false;
$dictionary['Document']['fields']['status_id']['options']='sasa_estado_c_list';

 ?>
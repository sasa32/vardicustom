<?php
 // created: 2020-07-29 09:29:39
$dictionary['Document']['fields']['subcategory_id']['audited']=false;
$dictionary['Document']['fields']['subcategory_id']['massupdate']=false;
$dictionary['Document']['fields']['subcategory_id']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['subcategory_id']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['subcategory_id']['merge_filter']='disabled';
$dictionary['Document']['fields']['subcategory_id']['reportable']=false;
$dictionary['Document']['fields']['subcategory_id']['calculated']=false;
$dictionary['Document']['fields']['subcategory_id']['dependency']=false;

 ?>
<?php
 // created: 2020-07-29 11:11:56
$dictionary['Document']['fields']['template_type']['audited']=true;
$dictionary['Document']['fields']['template_type']['massupdate']=false;
$dictionary['Document']['fields']['template_type']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['template_type']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['template_type']['merge_filter']='disabled';
$dictionary['Document']['fields']['template_type']['reportable']=true;
$dictionary['Document']['fields']['template_type']['calculated']=false;
$dictionary['Document']['fields']['template_type']['dependency']=false;
$dictionary['Document']['fields']['template_type']['options']='template_type_list';

 ?>
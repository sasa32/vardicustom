<?php
 // created: 2020-07-29 08:53:10
$dictionary['Document']['fields']['exp_date']['audited']=false;
$dictionary['Document']['fields']['exp_date']['massupdate']=false;
$dictionary['Document']['fields']['exp_date']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['exp_date']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['exp_date']['merge_filter']='disabled';
$dictionary['Document']['fields']['exp_date']['calculated']=false;
$dictionary['Document']['fields']['exp_date']['enable_range_search']=false;

 ?>
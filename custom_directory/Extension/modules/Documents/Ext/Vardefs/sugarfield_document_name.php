<?php
 // created: 2020-07-29 11:04:42
$dictionary['Document']['fields']['document_name']['audited']=true;
$dictionary['Document']['fields']['document_name']['massupdate']=false;
$dictionary['Document']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['document_name']['merge_filter']='disabled';
$dictionary['Document']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['Document']['fields']['document_name']['calculated']=false;

 ?>
<?php
 // created: 2020-07-29 11:11:27
$dictionary['Document']['fields']['date_modified']['audited']=true;
$dictionary['Document']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Document']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Document']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Document']['fields']['date_modified']['calculated']=false;
$dictionary['Document']['fields']['date_modified']['enable_range_search']='1';

 ?>
<?php
// created: 2020-07-29 16:43:27
$dictionary["Document"]["fields"]["documents_sasa_habeas_data_1"] = array (
  'name' => 'documents_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'documents_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'vname' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'documents_sasa_habeas_data_1documents_ida',
  'link-type' => 'many',
  'side' => 'left',
);

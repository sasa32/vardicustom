<?php
 // created: 2020-07-29 08:51:50
$dictionary['Document']['fields']['active_date']['audited']=false;
$dictionary['Document']['fields']['active_date']['massupdate']=true;
$dictionary['Document']['fields']['active_date']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['active_date']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['active_date']['merge_filter']='disabled';
$dictionary['Document']['fields']['active_date']['calculated']=false;
$dictionary['Document']['fields']['active_date']['enable_range_search']=false;

 ?>
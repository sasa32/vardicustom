<?php
 // created: 2020-07-29 09:28:49
$dictionary['Document']['fields']['category_id']['audited']=false;
$dictionary['Document']['fields']['category_id']['massupdate']=false;
$dictionary['Document']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['category_id']['merge_filter']='disabled';
$dictionary['Document']['fields']['category_id']['reportable']=false;
$dictionary['Document']['fields']['category_id']['calculated']=false;
$dictionary['Document']['fields']['category_id']['dependency']=false;

 ?>
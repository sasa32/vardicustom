<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DOCUMENTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE'] = 'Compañías';
$mod_strings['LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE'] = 'Habeas Data';
$mod_strings['LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_DOCUMENTS_TITLE'] = 'Habeas Data';
$mod_strings['LBL_OPPORTUNITIES_SUBPANEL_TITLE'] = 'Cotizaciones';

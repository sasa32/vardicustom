<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_PQRFS_C'] = '¿PQRFS?';
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_ID_WF_C'] = 'Identificador del Workflow';
$mod_strings['LBL_SASA_TIPIFICACION_DE_CASOS_CASES_1_FROM_CASES_TITLE'] = 'Casos';
$mod_strings['LBL_SASA_TIPIFICACION_DE_CASOS_CASES_1_FROM_SASA_TIPIFICACION_DE_CASOS_TITLE'] = 'Casos';
$mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_TIPIFICACION_DE_CASOS_FOCUS_DRAWER_DASHBOARD'] = 'Tipificación de Casos Panel de enfoque';
$mod_strings['LBL_SASA_TIPIFICACION_DE_CASOS_RECORD_DASHBOARD'] = 'Tipificación de Casos Cuadro de mando del registro';

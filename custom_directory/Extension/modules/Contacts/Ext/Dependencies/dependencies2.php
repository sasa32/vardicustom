<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependencies2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_phone_work_c','sasa_municipios_contacts_4_name'),
	//'trigger' => 'equal($sasa_phone_work_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_4_name',
				'value' => '
					not(equal($sasa_phone_work_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_4_name',
				'value' => '
					not(equal($sasa_phone_work_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_municipios_contacts_4_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_4_name',
				'value' => 'false',
			),
		),
	)
);

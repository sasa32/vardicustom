<?php

$dependencies['Contacts']['read_only_contacts'] = array(
  'hooks' => array("all"),
  'trigger' => 'true',
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_city',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_state',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_country',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'alt_address_city',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'alt_address_state',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'alt_address_country',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'sasa_ciudad3_c',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'sasa_departamento3_c',
        'value' => 'true'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'sasa_pais3_c',
        'value' => 'true'
      )
    )
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);






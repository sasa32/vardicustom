<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOTipDirec3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec3_c','sasa_direccion3_c'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_direccion3_c',
				'value' => '
					not(equal($sasa_tipodirec3_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => '
					not(equal($sasa_direccion3_c,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_direccion3_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => 'false',
			),
		),
	)
);

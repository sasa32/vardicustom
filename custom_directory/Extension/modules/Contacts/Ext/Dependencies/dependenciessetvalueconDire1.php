<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciessetvalueconDire1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('primary_address_street'),
	'trigger' => 'equal($primary_address_street,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_city',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_state',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_postalcode',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_country',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_contacts_1sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

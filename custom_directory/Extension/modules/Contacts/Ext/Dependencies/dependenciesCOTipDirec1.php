<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Contacts']['dependenciesCOTipDirec1'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipodirec1_c','primary_address_street'),
	//'trigger' => 'equal($sasa_phone_office_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_street',
				'value' => '
					not(equal($sasa_tipodirec1_c,0))
				',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec1_c',
				'value' => '
					not(equal($primary_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'primary_address_street',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec1_c',
				'value' => 'false',
			),
		),
	)
);

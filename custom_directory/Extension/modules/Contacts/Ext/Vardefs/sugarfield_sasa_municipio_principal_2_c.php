<?php
 // created: 2021-02-02 09:17:09
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['labelValue']='Ciudad Dirección 1 (Integración)';
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_municipio_principal_2_c']['required']=true;

 ?>
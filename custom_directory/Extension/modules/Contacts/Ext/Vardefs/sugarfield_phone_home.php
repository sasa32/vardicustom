<?php
 // created: 2020-07-31 13:06:13
$dictionary['Contact']['fields']['phone_home']['len']='255';
$dictionary['Contact']['fields']['phone_home']['massupdate']=false;
$dictionary['Contact']['fields']['phone_home']['comments']='';
$dictionary['Contact']['fields']['phone_home']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_home']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_home']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_home']['calculated']='1';
$dictionary['Contact']['fields']['phone_home']['pii']=false;
$dictionary['Contact']['fields']['phone_home']['audited']=false;
$dictionary['Contact']['fields']['phone_home']['importable']='false';
$dictionary['Contact']['fields']['phone_home']['formula']='concat(related($sasa_municipios_contacts_3,"sasa_indicativo_c"),$sasa_phone_home_c)';
$dictionary['Contact']['fields']['phone_home']['enforced']=true;

 ?>
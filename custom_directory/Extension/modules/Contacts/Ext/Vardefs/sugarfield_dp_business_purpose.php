<?php
 // created: 2020-07-15 17:30:43
$dictionary['Contact']['fields']['dp_business_purpose']['len']=NULL;
$dictionary['Contact']['fields']['dp_business_purpose']['audited']=false;
$dictionary['Contact']['fields']['dp_business_purpose']['massupdate']=false;
$dictionary['Contact']['fields']['dp_business_purpose']['comments']='Business purposes consented for';
$dictionary['Contact']['fields']['dp_business_purpose']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['dp_business_purpose']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['dp_business_purpose']['merge_filter']='disabled';
$dictionary['Contact']['fields']['dp_business_purpose']['calculated']=false;
$dictionary['Contact']['fields']['dp_business_purpose']['dependency']='';

 ?>
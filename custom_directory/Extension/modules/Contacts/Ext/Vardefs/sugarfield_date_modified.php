<?php
 // created: 2020-05-16 10:28:22
$dictionary['Contact']['fields']['date_modified']['audited']=false;
$dictionary['Contact']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Contact']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Contact']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Contact']['fields']['date_modified']['calculated']=false;
$dictionary['Contact']['fields']['date_modified']['enable_range_search']=false;

 ?>
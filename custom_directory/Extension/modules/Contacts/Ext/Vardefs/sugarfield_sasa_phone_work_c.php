<?php
 // created: 2020-07-31 13:30:50
$dictionary['Contact']['fields']['sasa_phone_work_c']['labelValue']='Teléfono 2';
$dictionary['Contact']['fields']['sasa_phone_work_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_phone_work_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_phone_work_c']['dependency']='not(equal($sasa_phone_home_c,""))';

 ?>
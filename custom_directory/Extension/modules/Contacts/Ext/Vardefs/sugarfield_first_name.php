<?php
 // created: 2020-07-15 17:05:51
$dictionary['Contact']['fields']['first_name']['required']=false;
$dictionary['Contact']['fields']['first_name']['massupdate']=false;
$dictionary['Contact']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Contact']['fields']['first_name']['importable']='false';
$dictionary['Contact']['fields']['first_name']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['first_name']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['first_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['first_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.99',
  'searchable' => true,
);
$dictionary['Contact']['fields']['first_name']['calculated']='1';
$dictionary['Contact']['fields']['first_name']['formula']='$sasa_nombres_c';
$dictionary['Contact']['fields']['first_name']['enforced']=true;
$dictionary['Contact']['fields']['first_name']['pii']=false;
$dictionary['Contact']['fields']['first_name']['audited']=false;

 ?>
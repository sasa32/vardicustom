<?php
 // created: 2020-05-17 00:04:41
$dictionary['Contact']['fields']['mkto_lead_score']['len']='11';
$dictionary['Contact']['fields']['mkto_lead_score']['audited']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['massupdate']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['mkto_lead_score']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['mkto_lead_score']['merge_filter']='disabled';
$dictionary['Contact']['fields']['mkto_lead_score']['reportable']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['mkto_lead_score']['calculated']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['enable_range_search']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['min']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['max']=false;
$dictionary['Contact']['fields']['mkto_lead_score']['disable_num_format']='';

 ?>
<?php
// created: 2020-05-11 19:32:57
$dictionary["Contact"]["fields"]["sasa_departamentos_contacts_2"] = array (
  'name' => 'sasa_departamentos_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_contacts_2',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'side' => 'right',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_departamentos_contacts_2_name"] = array (
  'name' => 'sasa_departamentos_contacts_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_contacts_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_departamentos_contacts_2sasa_departamentos_ida"] = array (
  'name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_departamentos_contacts_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_contacts_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

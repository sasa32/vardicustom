<?php
// created: 2023-03-06 03:26:09
$dictionary["Contact"]["fields"]["contacts_cases_1"] = array (
  'name' => 'contacts_cases_1',
  'type' => 'link',
  'relationship' => 'contacts_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_cases_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

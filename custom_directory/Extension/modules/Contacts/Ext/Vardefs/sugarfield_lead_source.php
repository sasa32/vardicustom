<?php
 // created: 2020-05-14 18:28:19
$dictionary['Contact']['fields']['lead_source']['len']=100;
$dictionary['Contact']['fields']['lead_source']['audited']=false;
$dictionary['Contact']['fields']['lead_source']['massupdate']=false;
$dictionary['Contact']['fields']['lead_source']['comments']='How did the contact come about';
$dictionary['Contact']['fields']['lead_source']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['lead_source']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Contact']['fields']['lead_source']['calculated']=false;
$dictionary['Contact']['fields']['lead_source']['dependency']=false;

 ?>
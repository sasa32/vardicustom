<?php
 // created: 2020-05-16 16:43:23
$dictionary['Contact']['fields']['birthdate']['comments']='The birthdate of the contact';
$dictionary['Contact']['fields']['birthdate']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['birthdate']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['birthdate']['merge_filter']='disabled';
$dictionary['Contact']['fields']['birthdate']['calculated']=false;
$dictionary['Contact']['fields']['birthdate']['enable_range_search']=false;

 ?>
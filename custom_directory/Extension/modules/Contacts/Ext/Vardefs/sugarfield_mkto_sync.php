<?php
 // created: 2020-05-17 00:02:10
$dictionary['Contact']['fields']['mkto_sync']['default']=false;
$dictionary['Contact']['fields']['mkto_sync']['audited']=false;
$dictionary['Contact']['fields']['mkto_sync']['massupdate']=false;
$dictionary['Contact']['fields']['mkto_sync']['comments']='Should the Lead be synced to Marketo';
$dictionary['Contact']['fields']['mkto_sync']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['mkto_sync']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['mkto_sync']['merge_filter']='disabled';
$dictionary['Contact']['fields']['mkto_sync']['reportable']=false;
$dictionary['Contact']['fields']['mkto_sync']['unified_search']=false;
$dictionary['Contact']['fields']['mkto_sync']['calculated']=false;

 ?>
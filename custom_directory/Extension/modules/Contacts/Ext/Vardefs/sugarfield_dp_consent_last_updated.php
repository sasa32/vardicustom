<?php
 // created: 2020-07-15 17:33:02
$dictionary['Contact']['fields']['dp_consent_last_updated']['massupdate']=false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['comments']='Date consent last updated';
$dictionary['Contact']['fields']['dp_consent_last_updated']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['dp_consent_last_updated']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['dp_consent_last_updated']['merge_filter']='disabled';
$dictionary['Contact']['fields']['dp_consent_last_updated']['calculated']=false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['enable_range_search']=false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['audited']=false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['options']='';

 ?>
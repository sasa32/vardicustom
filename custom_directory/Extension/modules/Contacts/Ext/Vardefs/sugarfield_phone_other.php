<?php
 // created: 2020-07-31 13:06:43
$dictionary['Contact']['fields']['phone_other']['len']='255';
$dictionary['Contact']['fields']['phone_other']['massupdate']=false;
$dictionary['Contact']['fields']['phone_other']['comments']='Other phone number for the contact';
$dictionary['Contact']['fields']['phone_other']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_other']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['phone_other']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_other']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.07',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_other']['calculated']='1';
$dictionary['Contact']['fields']['phone_other']['pii']=false;
$dictionary['Contact']['fields']['phone_other']['audited']=false;
$dictionary['Contact']['fields']['phone_other']['importable']='false';
$dictionary['Contact']['fields']['phone_other']['formula']='concat(related($sasa_municipios_contacts_4,"sasa_indicativo_c"),$sasa_phone_other_c)';
$dictionary['Contact']['fields']['phone_other']['enforced']=true;

 ?>
<?php
 // created: 2020-07-31 13:17:05
$dictionary['Contact']['fields']['primary_address_postalcode']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Contact']['fields']['primary_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_postalcode']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_postalcode']['pii']=false;

 ?>
<?php
 // created: 2020-07-07 08:47:00
$dictionary['Contact']['fields']['last_name']['massupdate']=false;
$dictionary['Contact']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Contact']['fields']['last_name']['importable']='false';
$dictionary['Contact']['fields']['last_name']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.97',
  'searchable' => true,
);
$dictionary['Contact']['fields']['last_name']['calculated']='1';
$dictionary['Contact']['fields']['last_name']['formula']='concat($sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['Contact']['fields']['last_name']['enforced']=true;
$dictionary['Contact']['fields']['last_name']['pii']=false;
$dictionary['Contact']['fields']['last_name']['audited']=false;

 ?>
<?php
 // created: 2020-07-31 14:21:57
$dictionary['Contact']['fields']['sasa_cel_otro_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['sasa_cel_otro_c']['labelValue']='Cel 3';
$dictionary['Contact']['fields']['sasa_cel_otro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_cel_otro_c']['calculated']='true';
$dictionary['Contact']['fields']['sasa_cel_otro_c']['formula']='concat("+57",$sasa_celular_otro_c)';
$dictionary['Contact']['fields']['sasa_cel_otro_c']['enforced']='true';
$dictionary['Contact']['fields']['sasa_cel_otro_c']['dependency']='';

 ?>
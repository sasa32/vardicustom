<?php
 // created: 2020-07-31 13:08:05
$dictionary['Contact']['fields']['phone_mobile']['len']='255';
$dictionary['Contact']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Contact']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.09',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_mobile']['calculated']='1';
$dictionary['Contact']['fields']['phone_mobile']['pii']=false;
$dictionary['Contact']['fields']['phone_mobile']['audited']=false;
$dictionary['Contact']['fields']['phone_mobile']['importable']='false';
$dictionary['Contact']['fields']['phone_mobile']['formula']='concat("+57",$sasa_phone_mobile_c)';
$dictionary['Contact']['fields']['phone_mobile']['enforced']=true;

 ?>
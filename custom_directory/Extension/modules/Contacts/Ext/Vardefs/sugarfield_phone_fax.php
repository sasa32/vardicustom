<?php
 // created: 2020-07-15 17:11:03
$dictionary['Contact']['fields']['phone_fax']['len']='100';
$dictionary['Contact']['fields']['phone_fax']['audited']=false;
$dictionary['Contact']['fields']['phone_fax']['massupdate']=false;
$dictionary['Contact']['fields']['phone_fax']['comments']='Contact fax number';
$dictionary['Contact']['fields']['phone_fax']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_fax']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_fax']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_fax']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.06',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_fax']['calculated']=false;
$dictionary['Contact']['fields']['phone_fax']['pii']=false;

 ?>
<?php
 // created: 2021-07-09 17:45:57
$dictionary['Contact']['fields']['sasa_extension3_c']['labelValue']='Extensión Teléfono 3';
$dictionary['Contact']['fields']['sasa_extension3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_extension3_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_extension3_c']['dependency']='equal($sasa_tipotel3_c,"OF")';

 ?>
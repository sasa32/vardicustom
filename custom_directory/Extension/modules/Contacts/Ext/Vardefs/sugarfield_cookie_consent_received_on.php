<?php
 // created: 2020-05-14 18:41:00
$dictionary['Contact']['fields']['cookie_consent_received_on']['massupdate']=false;
$dictionary['Contact']['fields']['cookie_consent_received_on']['comments']='Date cookie consent received on';
$dictionary['Contact']['fields']['cookie_consent_received_on']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['cookie_consent_received_on']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['cookie_consent_received_on']['merge_filter']='disabled';
$dictionary['Contact']['fields']['cookie_consent_received_on']['calculated']=false;
$dictionary['Contact']['fields']['cookie_consent_received_on']['enable_range_search']='1';

 ?>
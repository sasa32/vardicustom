<?php
 // created: 2021-07-09 17:45:59
$dictionary['Contact']['fields']['sasa_extension_c']['labelValue']=' Extensión Teléfono 1';
$dictionary['Contact']['fields']['sasa_extension_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_extension_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_extension_c']['dependency']='equal($sasa_tipotel1_c,"OF")';

 ?>
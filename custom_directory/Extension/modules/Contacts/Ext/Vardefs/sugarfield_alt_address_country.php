<?php
 // created: 2020-07-01 16:00:36
$dictionary['Contact']['fields']['alt_address_country']['len']='255';
$dictionary['Contact']['fields']['alt_address_country']['audited']=false;
$dictionary['Contact']['fields']['alt_address_country']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_country']['comments']='Country for alternate address';
$dictionary['Contact']['fields']['alt_address_country']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_country']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_country']['pii']=false;

 ?>
<?php
 // created: 2020-05-14 17:53:40
$dictionary['Contact']['fields']['do_not_call']['default']=false;
$dictionary['Contact']['fields']['do_not_call']['audited']=false;
$dictionary['Contact']['fields']['do_not_call']['massupdate']=false;
$dictionary['Contact']['fields']['do_not_call']['comments']='An indicator of whether contact can be called';
$dictionary['Contact']['fields']['do_not_call']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['do_not_call']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['do_not_call']['merge_filter']='disabled';
$dictionary['Contact']['fields']['do_not_call']['unified_search']=false;
$dictionary['Contact']['fields']['do_not_call']['calculated']=false;

 ?>
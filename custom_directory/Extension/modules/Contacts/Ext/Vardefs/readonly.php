<?php
/*
* Requerimiento en https://sasaconsultoria.sugarondemand.com/index.php#Tasks/5ea23328-7e7a-11ea-b3b6-02fb8f607ac4
*/
$dictionary["Contact"]["fields"]["primary_address_city"]['readonly'] = true;
$dictionary["Contact"]["fields"]["primary_address_state"]['readonly'] = true;
$dictionary["Contact"]["fields"]["primary_address_country"]['readonly'] = true;

$dictionary["Contact"]["fields"]["alt_address_city"]['readonly'] = true;
$dictionary["Contact"]["fields"]["alt_address_state"]['readonly'] = true;
$dictionary["Contact"]["fields"]["alt_address_country"]['readonly'] = true;

$dictionary["Contact"]["fields"]["sasa_ciudad3_c"]['readonly'] = true;
$dictionary["Contact"]["fields"]["sasa_departamento3_c"]['readonly'] = true;
$dictionary["Contact"]["fields"]["sasa_pais3_c"]['readonly'] = true;


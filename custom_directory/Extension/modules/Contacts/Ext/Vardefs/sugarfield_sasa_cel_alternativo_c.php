<?php
 // created: 2020-07-31 13:35:45
$dictionary['Contact']['fields']['sasa_cel_alternativo_c']['labelValue']='Celular 2';
$dictionary['Contact']['fields']['sasa_cel_alternativo_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_cel_alternativo_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_cel_alternativo_c']['dependency']='not(equal($sasa_phone_mobile_c,""))';

 ?>
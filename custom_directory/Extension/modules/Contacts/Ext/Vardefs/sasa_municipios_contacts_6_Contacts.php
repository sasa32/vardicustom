<?php
// created: 2020-07-31 14:28:57
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_6"] = array (
  'name' => 'sasa_municipios_contacts_6',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_6',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_6_name"] = array (
  'name' => 'sasa_municipios_contacts_6_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_6',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_6sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_6',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

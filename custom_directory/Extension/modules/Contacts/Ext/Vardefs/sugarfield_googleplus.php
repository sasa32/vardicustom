<?php
 // created: 2020-07-15 17:06:35
$dictionary['Contact']['fields']['googleplus']['audited']=false;
$dictionary['Contact']['fields']['googleplus']['massupdate']=false;
$dictionary['Contact']['fields']['googleplus']['comments']='The google plus id of the user';
$dictionary['Contact']['fields']['googleplus']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['googleplus']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['googleplus']['merge_filter']='disabled';
$dictionary['Contact']['fields']['googleplus']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['googleplus']['calculated']=false;
$dictionary['Contact']['fields']['googleplus']['pii']=false;

 ?>
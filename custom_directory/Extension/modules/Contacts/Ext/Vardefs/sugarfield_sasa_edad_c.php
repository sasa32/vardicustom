<?php
 // created: 2020-09-30 16:30:14
$dictionary['Contact']['fields']['sasa_edad_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['sasa_edad_c']['labelValue']='Edad';
$dictionary['Contact']['fields']['sasa_edad_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_edad_c']['calculated']='true';
$dictionary['Contact']['fields']['sasa_edad_c']['formula']='floor(divide(subtract(daysUntil(today()),daysUntil($birthdate)),365.242))';
$dictionary['Contact']['fields']['sasa_edad_c']['enforced']='true';
$dictionary['Contact']['fields']['sasa_edad_c']['dependency']='';

 ?>
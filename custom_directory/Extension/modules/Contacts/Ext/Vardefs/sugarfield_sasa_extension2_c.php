<?php
 // created: 2021-07-09 17:45:55
$dictionary['Contact']['fields']['sasa_extension2_c']['labelValue']='Extensión Teléfono 2';
$dictionary['Contact']['fields']['sasa_extension2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_extension2_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_extension2_c']['dependency']='equal($sasa_tipotel2_c,"OF")';

 ?>
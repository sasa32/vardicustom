<?php
 // created: 2020-07-31 13:07:52
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['labelValue']='Cel 2';
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['calculated']='1';
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['formula']='concat("+57",$sasa_cel_alternativo_c)';
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['enforced']='1';
$dictionary['Contact']['fields']['sasa_celular_alternativo_c']['dependency']='';

 ?>
<?php
 // created: 2020-05-16 10:27:53
$dictionary['Contact']['fields']['date_entered']['audited']=false;
$dictionary['Contact']['fields']['date_entered']['comments']='Date record created';
$dictionary['Contact']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Contact']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Contact']['fields']['date_entered']['calculated']=false;
$dictionary['Contact']['fields']['date_entered']['enable_range_search']=false;

 ?>
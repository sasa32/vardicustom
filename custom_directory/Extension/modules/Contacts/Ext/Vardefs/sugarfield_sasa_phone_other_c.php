<?php
 // created: 2020-07-31 13:32:48
$dictionary['Contact']['fields']['sasa_phone_other_c']['labelValue']='Teléfono 3';
$dictionary['Contact']['fields']['sasa_phone_other_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_phone_other_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_phone_other_c']['dependency']='not(equal($sasa_phone_work_c,""))';

 ?>
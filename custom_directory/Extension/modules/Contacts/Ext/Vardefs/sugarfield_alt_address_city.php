<?php
 // created: 2020-07-01 15:57:12
$dictionary['Contact']['fields']['alt_address_city']['audited']=true;
$dictionary['Contact']['fields']['alt_address_city']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_city']['comments']='City for alternate address';
$dictionary['Contact']['fields']['alt_address_city']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_city']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_city']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_city']['pii']=false;

 ?>
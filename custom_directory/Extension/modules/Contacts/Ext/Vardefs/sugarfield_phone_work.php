<?php
 // created: 2020-07-31 13:06:29
$dictionary['Contact']['fields']['phone_work']['len']='255';
$dictionary['Contact']['fields']['phone_work']['massupdate']=false;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_work']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.08',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_work']['calculated']='1';
$dictionary['Contact']['fields']['phone_work']['pii']=false;
$dictionary['Contact']['fields']['phone_work']['audited']=false;
$dictionary['Contact']['fields']['phone_work']['importable']='false';
$dictionary['Contact']['fields']['phone_work']['formula']='concat(related($sasa_municipios_contacts_5,"sasa_indicativo_c"),$sasa_phone_work_c)';
$dictionary['Contact']['fields']['phone_work']['enforced']=true;

 ?>
<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente y Prospecto';
$mod_strings['LNK_NEW_CONTACT'] = 'Nuevo Contact';
$mod_strings['LBL_MODULE_NAME'] = 'Contactos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Contact';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Contact';
$mod_strings['LNK_CONTACT_LIST'] = 'Ver Contacts';
$mod_strings['LNK_CONTACT_REPORTS'] = 'Ver Informes de Contacts';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Contact desde vCard';
$mod_strings['LNK_IMPORT_CONTACTS'] = 'Importar Contacts';
$mod_strings['LBL_SAVE_CONTACT'] = 'Guardar Contact';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Contacts';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Contacts';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACT_OPP_FORM_TITLE'] = 'Opportunity-Contact:';
$mod_strings['LBL_CONTACT'] = 'Contact:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Contacts';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Nombre Contact';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Vista de Contacts';
$mod_strings['LBL_MODULE_TITLE'] = 'Contacts: Inicio';
$mod_strings['LBL_RELATED_CONTACTS_TITLE'] = 'Contacts Relacionados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_LABEL'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_SELECT_CHECKED_BUTTON_TITLE'] = 'Seleccionar Contacts Marcados';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Opportunity:';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Opportunity';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Opportunity';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Case';
$mod_strings['LBL_NOTE_SUBJECT'] = 'Asunto de Note';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LNK_NEW_MEETING'] = 'Programar Meeting';
$mod_strings['LNK_NEW_TASK'] = 'Nueva Task';
$mod_strings['LBL_PRODUCTS_TITLE'] = 'Quoted Line Items';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Bugs';
$mod_strings['LBL_TARGET_OF_CAMPAIGNS'] = 'Campaigns (Objetivo de) :';
$mod_strings['LBL_CAMPAIGNS'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_LIST_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGNS_SUBPANEL_TITLE'] = 'Campaigns';
$mod_strings['LBL_CAMPAIGN_ID'] = 'ID Campaign';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tarjeta de Visita';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_1_NAME'] = 'Ciudad Principal';
$mod_strings['SASA_MUNICIPIOS_CONTACTS_2_NAME'] = 'Ciudad Alternativa';
$mod_strings['LBL_PRIMARY_ADDRESS'] = 'Dirección Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Casa';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_4_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Oficina';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Teléfono Alternativo';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE'] = 'Ciudades';
$mod_strings['LBL_MOBILE_PHONE'] = 'Cel Principal';
$mod_strings['LBL_HOME_PHONE'] = 'Tel Casa';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
$mod_strings['LBL_OFFICE_PHONE'] = 'Tel de Oficina';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Ciudad';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Principal';
$mod_strings['LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE'] = 'Ciudad Alternativa';
$mod_strings['LBL_SASA_CLIENTE_FALLECIDO_C'] = 'Cliente Fallecido';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono casa';
$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Celular Principal';
$mod_strings['LBL_SASA_PHONE_WORK_C'] = 'Teléfono de Oficina';
$mod_strings['LBL_SASA_PHONE_OTHER_C'] = 'Teléfono Alternativo';
$mod_strings['LBL_SASA_CEL_ALTERNATIVO_C'] = 'Celular Alternativo';


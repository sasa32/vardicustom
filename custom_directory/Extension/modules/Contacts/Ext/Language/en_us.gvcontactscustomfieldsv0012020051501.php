<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto Name:';
$mod_strings['LBL_ACCOUNT_ID'] = 'Cliente y Prospecto ID:';
$mod_strings['LBL_CREATED_ACCOUNT'] = 'Created a new Cliente y Prospecto';
$mod_strings['LBL_EXISTING_ACCOUNT'] = 'Used an existing Cliente y Prospecto';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente y Prospecto Name';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Select Cliente y Prospecto';
$mod_strings['NTC_OPPORTUNITY_REQUIRES_ACCOUNT'] = 'Creating an Opportunity requires an Cliente y Prospecto.\\n Please either create a new Cliente y Prospecto or select an existing one.';

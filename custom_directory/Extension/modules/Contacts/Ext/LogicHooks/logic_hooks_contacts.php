<?php
$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'before_save',

	//The PHP file where your class is located.
	'custom/modules/Contacts/logic_hooks_contacts.php',

	//The class the method is in.
	'logic_hooks_contacts',

	//The method to call.
	'before_save'
);

?>

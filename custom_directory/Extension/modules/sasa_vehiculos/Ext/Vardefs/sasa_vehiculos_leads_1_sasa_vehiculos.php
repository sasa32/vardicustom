<?php
// created: 2020-05-15 18:35:34
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_leads_1"] = array (
  'name' => 'sasa_vehiculos_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
// created: 2020-06-11 20:27:26
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'SASA_UnidadNegClienteProspect',
  'bean_name' => 'SASA_UnidadNegClienteProspect',
  'vname' => 'LBL_SASA_VEHICULOS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

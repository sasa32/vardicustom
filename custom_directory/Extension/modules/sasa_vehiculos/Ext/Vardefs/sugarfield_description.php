<?php
 // created: 2020-05-16 16:05:43
$dictionary['sasa_vehiculos']['fields']['description']['audited']=false;
$dictionary['sasa_vehiculos']['fields']['description']['massupdate']=false;
$dictionary['sasa_vehiculos']['fields']['description']['comments']='Full text of the note';
$dictionary['sasa_vehiculos']['fields']['description']['duplicate_merge']='enabled';
$dictionary['sasa_vehiculos']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['sasa_vehiculos']['fields']['description']['merge_filter']='disabled';
$dictionary['sasa_vehiculos']['fields']['description']['unified_search']=false;
$dictionary['sasa_vehiculos']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['sasa_vehiculos']['fields']['description']['calculated']=false;
$dictionary['sasa_vehiculos']['fields']['description']['rows']='6';
$dictionary['sasa_vehiculos']['fields']['description']['cols']='80';

 ?>
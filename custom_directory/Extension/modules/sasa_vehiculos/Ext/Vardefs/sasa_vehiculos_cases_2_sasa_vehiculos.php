<?php
// created: 2021-08-04 21:22:45
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_cases_2"] = array (
  'name' => 'sasa_vehiculos_cases_2',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_2',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

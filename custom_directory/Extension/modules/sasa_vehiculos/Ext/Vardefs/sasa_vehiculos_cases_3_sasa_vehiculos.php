<?php
// created: 2021-08-04 21:24:18
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_cases_3"] = array (
  'name' => 'sasa_vehiculos_cases_3',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_3',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_3_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

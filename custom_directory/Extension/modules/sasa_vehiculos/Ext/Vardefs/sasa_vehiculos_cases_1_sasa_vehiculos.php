<?php
// created: 2021-08-04 21:19:24
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_cases_1"] = array (
  'name' => 'sasa_vehiculos_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
$mod_strings['LBL_SAS_CD_LINE_VEHI_C'] = 'Código Linea Vehículo';
 $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
 $mod_strings['LBL_NAME'] = 'Nombre de Línea';
 $mod_strings['LBL_DESCRIPTION'] = 'Observaciones';
 $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
 $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
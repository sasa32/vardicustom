<?php
 $mod_strings['LBL_SAS_CD_LINE_VEHI_C'] = 'Código Linea Vehículo';
 $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
 $mod_strings['LBL_NAME'] = 'Nombre de Línea';
 $mod_strings['LBL_DESCRIPTION'] = 'Descripción';
 $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
 $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_URLBROCHURE_C'] = 'Url Brochure';
$mod_strings['LBL_SASA_ESTADOLINEA_C'] = 'Estado de línea';
$mod_strings['LBL_SASA_LINEABROCHURE_C'] = 'Línea Brochure';
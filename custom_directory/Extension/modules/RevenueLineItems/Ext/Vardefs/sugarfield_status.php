<?php
 // created: 2023-02-15 17:08:57
$dictionary['RevenueLineItem']['fields']['status']['audited']=false;
$dictionary['RevenueLineItem']['fields']['status']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['status']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['status']['comments']='Product status (ex: Quoted, Ordered, Shipped)';
$dictionary['RevenueLineItem']['fields']['status']['importable']='false';
$dictionary['RevenueLineItem']['fields']['status']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['status']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['status']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['status']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['status']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['status']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['status']['visibility_grid']=array (
);

 ?>
<?php
 // created: 2023-02-15 17:31:58
$dictionary['RevenueLineItem']['fields']['pricing_formula']['len']='100';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['audited']=false;
$dictionary['RevenueLineItem']['fields']['pricing_formula']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['pricing_formula']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['pricing_formula']['comments']='Pricing formula (ex: Fixed, Markup over Cost)';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['importable']='false';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['pricing_formula']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['pricing_formula']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['pricing_formula']['calculated']=false;

 ?>
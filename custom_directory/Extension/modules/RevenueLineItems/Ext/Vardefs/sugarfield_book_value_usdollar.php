<?php
 // created: 2023-02-15 17:09:22
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['comments']='Book value expressed in USD';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['book_value_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
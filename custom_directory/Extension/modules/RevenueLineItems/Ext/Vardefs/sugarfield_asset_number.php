<?php
 // created: 2023-02-15 17:27:57
$dictionary['RevenueLineItem']['fields']['asset_number']['len']='50';
$dictionary['RevenueLineItem']['fields']['asset_number']['audited']=false;
$dictionary['RevenueLineItem']['fields']['asset_number']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['asset_number']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['asset_number']['comments']='Asset tag number of sales item in use';
$dictionary['RevenueLineItem']['fields']['asset_number']['importable']='false';
$dictionary['RevenueLineItem']['fields']['asset_number']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['asset_number']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['asset_number']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['asset_number']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['asset_number']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['asset_number']['calculated']=false;

 ?>
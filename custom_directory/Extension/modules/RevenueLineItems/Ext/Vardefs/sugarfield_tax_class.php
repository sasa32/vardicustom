<?php
 // created: 2023-02-15 17:35:49
$dictionary['RevenueLineItem']['fields']['tax_class']['audited']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['comments']='Tax classification (ex: Taxable, Non-taxable)';
$dictionary['RevenueLineItem']['fields']['tax_class']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['tax_class']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['tax_class']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['tax_class']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['tax_class']['visibility_grid']=array (
);

 ?>
<?php
 // created: 2023-02-15 16:39:27
$dictionary['RevenueLineItem']['fields']['renewable']['default']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['audited']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['comments']='Indicates whether the sales item is renewable (e.g. a service)';
$dictionary['RevenueLineItem']['fields']['renewable']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['renewable']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['renewable']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['renewable']['unified_search']=false;
$dictionary['RevenueLineItem']['fields']['renewable']['calculated']=false;

 ?>
<?php
 // created: 2023-02-15 17:19:30
$dictionary['RevenueLineItem']['fields']['campaign_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['campaign_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['campaign_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['campaign_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['campaign_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['campaign_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['campaign_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['campaign_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['campaign_name']['calculated']=false;

 ?>
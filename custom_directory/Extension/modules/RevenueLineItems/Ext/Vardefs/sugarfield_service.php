<?php
 // created: 2023-02-15 16:40:25
$dictionary['RevenueLineItem']['fields']['service']['default']=false;
$dictionary['RevenueLineItem']['fields']['service']['audited']=false;
$dictionary['RevenueLineItem']['fields']['service']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['service']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['service']['comments']='Indicates whether the sales item is a service or a product';
$dictionary['RevenueLineItem']['fields']['service']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['service']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['service']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['service']['unified_search']=false;
$dictionary['RevenueLineItem']['fields']['service']['calculated']=false;

 ?>
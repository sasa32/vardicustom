<?php
 // created: 2023-02-15 17:34:23
$dictionary['RevenueLineItem']['fields']['support_name']['len']='50';
$dictionary['RevenueLineItem']['fields']['support_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['support_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_name']['comments']='Name of sales item for support purposes';
$dictionary['RevenueLineItem']['fields']['support_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['support_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['support_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['support_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['support_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['support_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['support_name']['calculated']=false;

 ?>
<?php
 // created: 2023-02-15 17:24:18
$dictionary['RevenueLineItem']['fields']['account_name']['audited']=true;
$dictionary['RevenueLineItem']['fields']['account_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['account_name']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['account_name']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['account_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['account_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['account_name']['calculated']=false;

 ?>
<?php
 // created: 2023-02-15 17:25:23
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_name']['calculated']=false;

 ?>
<?php
 // created: 2023-02-15 16:58:38
$dictionary['RevenueLineItem']['fields']['category_id']['name']='category_id';
$dictionary['RevenueLineItem']['fields']['category_id']['vname']='LBL_CATEGORY_ID';
$dictionary['RevenueLineItem']['fields']['category_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['category_id']['group']='category_name';
$dictionary['RevenueLineItem']['fields']['category_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['category_id']['reportable']=true;
$dictionary['RevenueLineItem']['fields']['category_id']['comment']='Product category';
$dictionary['RevenueLineItem']['fields']['category_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['category_id']['importable']='false';

 ?>
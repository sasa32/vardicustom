<?php
 // created: 2023-02-15 17:19:30
$dictionary['RevenueLineItem']['fields']['campaign_id']['name']='campaign_id';
$dictionary['RevenueLineItem']['fields']['campaign_id']['comment']='Campaign that generated lead';
$dictionary['RevenueLineItem']['fields']['campaign_id']['vname']='LBL_CAMPAIGN_ID';
$dictionary['RevenueLineItem']['fields']['campaign_id']['rname']='id';
$dictionary['RevenueLineItem']['fields']['campaign_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['campaign_id']['dbType']='id';
$dictionary['RevenueLineItem']['fields']['campaign_id']['table']='campaigns';
$dictionary['RevenueLineItem']['fields']['campaign_id']['isnull']='true';
$dictionary['RevenueLineItem']['fields']['campaign_id']['module']='Campaigns';
$dictionary['RevenueLineItem']['fields']['campaign_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['campaign_id']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['campaign_id']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['campaign_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['campaign_id']['importable']='false';

 ?>
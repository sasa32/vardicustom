<?php
 // created: 2023-02-15 17:11:35
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['comments']='PLI Name';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_name']['calculated']=false;

 ?>
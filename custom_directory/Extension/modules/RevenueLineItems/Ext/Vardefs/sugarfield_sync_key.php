<?php
 // created: 2023-02-15 17:27:11
$dictionary['RevenueLineItem']['fields']['sync_key']['audited']=false;
$dictionary['RevenueLineItem']['fields']['sync_key']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['sync_key']['importable']='false';
$dictionary['RevenueLineItem']['fields']['sync_key']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['sync_key']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 ?>
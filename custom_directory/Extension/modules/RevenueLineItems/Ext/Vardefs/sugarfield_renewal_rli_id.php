<?php
 // created: 2023-02-15 17:26:12
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['name']='renewal_rli_id';
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['vname']='LBL_RENEWAL_RLI_ID';
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['comment']='Renewal RLI of this RLI, set during renewal generation';
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_id']['importable']='false';

 ?>
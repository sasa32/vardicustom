<?php
 // created: 2023-02-15 17:17:30
$dictionary['RevenueLineItem']['fields']['probability']['len']='11';
$dictionary['RevenueLineItem']['fields']['probability']['audited']=false;
$dictionary['RevenueLineItem']['fields']['probability']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['probability']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['probability']['comments']='The probability of closure';
$dictionary['RevenueLineItem']['fields']['probability']['importable']='false';
$dictionary['RevenueLineItem']['fields']['probability']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['probability']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['probability']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['probability']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['probability']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['probability']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['probability']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['probability']['min']=0;
$dictionary['RevenueLineItem']['fields']['probability']['max']=100;
$dictionary['RevenueLineItem']['fields']['probability']['disable_num_format']='';

 ?>
<?php
 // created: 2023-02-15 17:01:30
$dictionary['RevenueLineItem']['fields']['discount_price']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['discount_price']['len']=26;
$dictionary['RevenueLineItem']['fields']['discount_price']['audited']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['comments']='Discounted price ("Unit Price" in Quote)';
$dictionary['RevenueLineItem']['fields']['discount_price']['importable']='false';
$dictionary['RevenueLineItem']['fields']['discount_price']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_price']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['discount_price']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_price']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_price']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
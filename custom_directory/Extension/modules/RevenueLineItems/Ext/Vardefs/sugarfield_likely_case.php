<?php
 // created: 2023-02-15 16:29:52
$dictionary['RevenueLineItem']['fields']['likely_case']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['likely_case']['len']=26;
$dictionary['RevenueLineItem']['fields']['likely_case']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['likely_case']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['likely_case']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['likely_case']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['likely_case']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['required']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['audited']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['formula']='';
$dictionary['RevenueLineItem']['fields']['likely_case']['readonly']=true;

 ?>
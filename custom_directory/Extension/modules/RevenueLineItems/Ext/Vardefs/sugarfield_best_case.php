<?php
 // created: 2023-02-15 17:12:51
$dictionary['RevenueLineItem']['fields']['best_case']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['best_case']['len']=26;
$dictionary['RevenueLineItem']['fields']['best_case']['audited']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['importable']='false';
$dictionary['RevenueLineItem']['fields']['best_case']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['best_case']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['best_case']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['best_case']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['best_case']['enforced']=false;
$dictionary['RevenueLineItem']['fields']['best_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['best_case']['enable_range_search']=false;

 ?>
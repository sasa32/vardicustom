<?php
 // created: 2023-02-15 17:26:12
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['renewal_rli_name']['calculated']=false;

 ?>
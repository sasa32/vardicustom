<?php
 // created: 2023-02-15 17:20:15
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['options']='';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['comments']='Rollup of included RLIs on the Opportunity';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['importable']='false';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['forecasted_likely']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
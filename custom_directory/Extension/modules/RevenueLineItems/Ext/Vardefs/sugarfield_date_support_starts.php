<?php
 // created: 2023-02-15 17:30:38
$dictionary['RevenueLineItem']['fields']['date_support_starts']['audited']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['comments']='Support start date';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['importable']='false';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_support_starts']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_support_starts']['enable_range_search']=false;

 ?>
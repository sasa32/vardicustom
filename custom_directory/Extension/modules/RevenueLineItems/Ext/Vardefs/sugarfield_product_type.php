<?php
 // created: 2023-02-15 17:22:24
$dictionary['RevenueLineItem']['fields']['product_type']['len']=100;
$dictionary['RevenueLineItem']['fields']['product_type']['audited']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['comments']='Type of product ( from opportunities opportunity_type ex: Existing, New)';
$dictionary['RevenueLineItem']['fields']['product_type']['importable']='false';
$dictionary['RevenueLineItem']['fields']['product_type']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['product_type']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['product_type']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['product_type']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['product_type']['visibility_grid']=array (
);

 ?>
<?php
 // created: 2023-02-15 17:02:29
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['audited']=false;
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['importable']='false';
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_rate_percent']['enable_range_search']=false;

 ?>
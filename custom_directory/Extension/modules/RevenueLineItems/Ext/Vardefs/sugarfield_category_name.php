<?php
 // created: 2023-02-15 16:58:38
$dictionary['RevenueLineItem']['fields']['category_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['category_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['category_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['category_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['category_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['category_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['category_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['category_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['category_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['category_name']['calculated']=false;

 ?>
<?php
 // created: 2023-02-15 17:38:15
$dictionary['RevenueLineItem']['fields']['base_rate']['audited']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['importable']='false';
$dictionary['RevenueLineItem']['fields']['base_rate']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['base_rate']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['base_rate']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['base_rate']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['base_rate']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['readonly']=false;
$dictionary['RevenueLineItem']['fields']['base_rate']['rows']='4';
$dictionary['RevenueLineItem']['fields']['base_rate']['cols']='20';

 ?>
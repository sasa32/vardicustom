<?php
 // created: 2023-02-07 21:49:10
$dictionary['RevenueLineItem']['fields']['total_amount']['audited']=false;
$dictionary['RevenueLineItem']['fields']['total_amount']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['total_amount']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['total_amount']['importable']='false';
$dictionary['RevenueLineItem']['fields']['total_amount']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['total_amount']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['total_amount']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['total_amount']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['total_amount']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['total_amount']['enable_range_search']=false;

 ?>
<?php
 // created: 2023-02-15 17:07:00
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['comments']='Discount price expressed in USD';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['discount_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
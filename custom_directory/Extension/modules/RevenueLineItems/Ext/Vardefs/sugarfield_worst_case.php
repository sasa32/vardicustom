<?php
 // created: 2023-02-15 17:13:56
$dictionary['RevenueLineItem']['fields']['worst_case']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['worst_case']['len']=26;
$dictionary['RevenueLineItem']['fields']['worst_case']['audited']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['importable']='false';
$dictionary['RevenueLineItem']['fields']['worst_case']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['worst_case']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['worst_case']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['worst_case']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['worst_case']['enforced']=false;
$dictionary['RevenueLineItem']['fields']['worst_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['worst_case']['enable_range_search']=false;

 ?>
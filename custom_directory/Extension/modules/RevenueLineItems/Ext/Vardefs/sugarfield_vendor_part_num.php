<?php
 // created: 2023-02-15 17:36:17
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['len']='50';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['audited']=false;
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['comments']='Vendor part number';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['importable']='false';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['vendor_part_num']['calculated']=false;

 ?>
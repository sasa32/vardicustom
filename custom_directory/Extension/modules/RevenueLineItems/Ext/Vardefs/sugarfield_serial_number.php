<?php
 // created: 2023-02-15 17:32:30
$dictionary['RevenueLineItem']['fields']['serial_number']['len']='50';
$dictionary['RevenueLineItem']['fields']['serial_number']['audited']=false;
$dictionary['RevenueLineItem']['fields']['serial_number']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['serial_number']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['serial_number']['comments']='Serial number of sales item in use';
$dictionary['RevenueLineItem']['fields']['serial_number']['importable']='false';
$dictionary['RevenueLineItem']['fields']['serial_number']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['serial_number']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['serial_number']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['serial_number']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['serial_number']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['serial_number']['calculated']=false;

 ?>
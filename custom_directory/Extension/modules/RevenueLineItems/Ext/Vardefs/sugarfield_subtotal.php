<?php
 // created: 2023-02-07 21:48:36
$dictionary['RevenueLineItem']['fields']['subtotal']['len']=26;
$dictionary['RevenueLineItem']['fields']['subtotal']['audited']=false;
$dictionary['RevenueLineItem']['fields']['subtotal']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['subtotal']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['subtotal']['comments']='Stores the total of the line item before any discounts are applied, taking proration into consideration';
$dictionary['RevenueLineItem']['fields']['subtotal']['importable']='false';
$dictionary['RevenueLineItem']['fields']['subtotal']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['subtotal']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['subtotal']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['subtotal']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['subtotal']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['subtotal']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['subtotal']['enable_range_search']=false;

 ?>
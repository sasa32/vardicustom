<?php
 // created: 2023-02-15 16:59:23
$dictionary['RevenueLineItem']['fields']['mft_part_num']['audited']=false;
$dictionary['RevenueLineItem']['fields']['mft_part_num']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['mft_part_num']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['mft_part_num']['comments']='Manufacturer part number';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['importable']='false';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['mft_part_num']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['mft_part_num']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['mft_part_num']['calculated']=false;

 ?>
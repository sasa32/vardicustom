<?php
 // created: 2023-02-15 16:43:01
$dictionary['RevenueLineItem']['fields']['service_start_date']['audited']=false;
$dictionary['RevenueLineItem']['fields']['service_start_date']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['service_start_date']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['service_start_date']['comments']='Start date of the service';
$dictionary['RevenueLineItem']['fields']['service_start_date']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['service_start_date']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['service_start_date']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['service_start_date']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['service_start_date']['enable_range_search']=false;

 ?>
<?php
 // created: 2023-02-15 17:29:57
$dictionary['RevenueLineItem']['fields']['date_support_expires']['audited']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['comments']='Support expiration date';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['importable']='false';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_support_expires']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_support_expires']['enable_range_search']=false;

 ?>
<?php
 // created: 2023-02-15 17:37:38
$dictionary['RevenueLineItem']['fields']['weight']['audited']=false;
$dictionary['RevenueLineItem']['fields']['weight']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['weight']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['weight']['comments']='Weight of the sales item';
$dictionary['RevenueLineItem']['fields']['weight']['importable']='false';
$dictionary['RevenueLineItem']['fields']['weight']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['weight']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['weight']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['weight']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['weight']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['weight']['enable_range_search']=false;

 ?>
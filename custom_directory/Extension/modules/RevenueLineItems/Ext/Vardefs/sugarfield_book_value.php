<?php
 // created: 2023-02-15 16:35:51
$dictionary['RevenueLineItem']['fields']['book_value']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['book_value']['len']=26;
$dictionary['RevenueLineItem']['fields']['book_value']['audited']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['comments']='Book value of sales item in use';
$dictionary['RevenueLineItem']['fields']['book_value']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['book_value']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['book_value']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['book_value']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['book_value']['readonly']=true;

 ?>
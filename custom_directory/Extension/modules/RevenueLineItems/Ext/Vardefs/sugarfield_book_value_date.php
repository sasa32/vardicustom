<?php
 // created: 2023-02-15 17:28:30
$dictionary['RevenueLineItem']['fields']['book_value_date']['audited']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['comments']='Date of book value for sales item in use';
$dictionary['RevenueLineItem']['fields']['book_value_date']['importable']='false';
$dictionary['RevenueLineItem']['fields']['book_value_date']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value_date']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['book_value_date']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['book_value_date']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['book_value_date']['enable_range_search']=false;

 ?>
<?php
 // created: 2023-02-15 17:16:54
$dictionary['RevenueLineItem']['fields']['commit_stage']['len']=100;
$dictionary['RevenueLineItem']['fields']['commit_stage']['audited']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['options']='';
$dictionary['RevenueLineItem']['fields']['commit_stage']['comments']='Forecast commit category: Include, Likely, Omit etc.';
$dictionary['RevenueLineItem']['fields']['commit_stage']['importable']='false';
$dictionary['RevenueLineItem']['fields']['commit_stage']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['commit_stage']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['commit_stage']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['commit_stage']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['formula']='';
$dictionary['RevenueLineItem']['fields']['commit_stage']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['commit_stage']['related_fields']=array (
);
$dictionary['RevenueLineItem']['fields']['commit_stage']['visibility_grid']=array (
);

 ?>
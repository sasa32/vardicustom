<?php
 // created: 2023-02-15 17:10:08
$dictionary['RevenueLineItem']['fields']['generate_purchase']['len']=100;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['audited']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['comments']='Generate Purchase Options (ex: Yes, No, Completed)';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['importable']='false';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['generate_purchase']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['generate_purchase']['visibility_grid']=array (
);

 ?>
<?php
 // created: 2023-02-07 21:46:52
$dictionary['RevenueLineItem']['fields']['deleted']['default']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['audited']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['comments']='Record deletion indicator';
$dictionary['RevenueLineItem']['fields']['deleted']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['deleted']['duplicate_merge_dom_value']=1;
$dictionary['RevenueLineItem']['fields']['deleted']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['deleted']['unified_search']=false;
$dictionary['RevenueLineItem']['fields']['deleted']['calculated']=false;

 ?>
<?php
 // created: 2023-02-15 17:21:25
$dictionary['RevenueLineItem']['fields']['quote_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['quote_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['quote_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['quote_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['quote_name']['comments']='Quote Name';
$dictionary['RevenueLineItem']['fields']['quote_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['quote_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['quote_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['quote_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['quote_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['quote_name']['calculated']=false;

 ?>
<?php
 // created: 2023-02-15 16:32:28
$dictionary['RevenueLineItem']['fields']['sales_stage']['len']=100;
$dictionary['RevenueLineItem']['fields']['sales_stage']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['RevenueLineItem']['fields']['sales_stage']['importable']='true';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['sales_stage']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['visibility_grid']=array (
);
$dictionary['RevenueLineItem']['fields']['sales_stage']['required']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['audited']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['reportable']=true;
$dictionary['RevenueLineItem']['fields']['sales_stage']['default']='';
$dictionary['RevenueLineItem']['fields']['sales_stage']['readonly']=true;

 ?>
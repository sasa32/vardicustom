<?php
 // created: 2023-02-15 16:57:31
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['name']='manufacturer_id';
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['vname']='LBL_MANUFACTURER';
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['comment']='Manufacturer of product';
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_id']['importable']='false';

 ?>
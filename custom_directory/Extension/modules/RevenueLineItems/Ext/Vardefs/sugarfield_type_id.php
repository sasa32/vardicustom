<?php
 // created: 2023-02-15 17:22:54
$dictionary['RevenueLineItem']['fields']['type_id']['name']='type_id';
$dictionary['RevenueLineItem']['fields']['type_id']['vname']='LBL_TYPE';
$dictionary['RevenueLineItem']['fields']['type_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['type_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['type_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['type_id']['comment']='Product type (ex: hardware, software)';
$dictionary['RevenueLineItem']['fields']['type_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['type_id']['importable']='false';

 ?>
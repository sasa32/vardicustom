<?php
 // created: 2023-02-15 17:18:30
$dictionary['RevenueLineItem']['fields']['lead_source']['len']=100;
$dictionary['RevenueLineItem']['fields']['lead_source']['audited']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['comments']='Source of the product';
$dictionary['RevenueLineItem']['fields']['lead_source']['importable']='false';
$dictionary['RevenueLineItem']['fields']['lead_source']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['lead_source']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['lead_source']['visibility_grid']=array (
);

 ?>
<?php
 // created: 2023-02-15 17:29:12
$dictionary['RevenueLineItem']['fields']['date_purchased']['audited']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['comments']='Date sales item purchased';
$dictionary['RevenueLineItem']['fields']['date_purchased']['importable']='false';
$dictionary['RevenueLineItem']['fields']['date_purchased']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['date_purchased']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['date_purchased']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_purchased']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_purchased']['enable_range_search']=false;

 ?>
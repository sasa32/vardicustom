<?php
 // created: 2023-02-15 16:57:31
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['manufacturer_name']['related_fields']=array (
  0 => 'manufacturer_id',
);

 ?>
<?php
 // created: 2023-02-15 17:07:32
$dictionary['RevenueLineItem']['fields']['list_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['comments']='List price expressed in USD';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['list_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['list_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
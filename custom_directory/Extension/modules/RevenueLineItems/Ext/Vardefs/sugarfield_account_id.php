<?php
 // created: 2023-02-15 17:24:18
$dictionary['RevenueLineItem']['fields']['account_id']['name']='account_id';
$dictionary['RevenueLineItem']['fields']['account_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['account_id']['vname']='LBL_ACCOUNT_ID';
$dictionary['RevenueLineItem']['fields']['account_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['account_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['account_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['account_id']['comment']='Account this product is associated with';
$dictionary['RevenueLineItem']['fields']['account_id']['formula']='ifElse(related($opportunities, "account_id"), related($opportunities, "account_id"), $account_id)';
$dictionary['RevenueLineItem']['fields']['account_id']['enforced']=true;
$dictionary['RevenueLineItem']['fields']['account_id']['calculated']=true;
$dictionary['RevenueLineItem']['fields']['account_id']['importable']='true';

 ?>
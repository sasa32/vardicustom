<?php
 // created: 2023-02-15 16:38:07
$dictionary['RevenueLineItem']['fields']['quantity']['default']='';
$dictionary['RevenueLineItem']['fields']['quantity']['len']='12';
$dictionary['RevenueLineItem']['fields']['quantity']['audited']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['comments']='Quantity in use';
$dictionary['RevenueLineItem']['fields']['quantity']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['quantity']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['quantity']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['quantity']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['readonly']=true;
$dictionary['RevenueLineItem']['fields']['quantity']['enable_range_search']=false;

 ?>
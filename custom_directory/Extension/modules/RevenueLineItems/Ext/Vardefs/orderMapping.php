<?php
// created: 2023-05-25 19:46:15
$extensionOrderMap = array (
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/rli_vardef.ext.php' => 
  array (
    'md5' => '9610a8864a6c6c53b87fd7200106b1f8',
    'mtime' => 1580429949,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/denorm_account_name.php' => 
  array (
    'md5' => '971ea4882e292b13ecd086a5cd4ec7ad',
    'mtime' => 1634962509,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_deleted.php' => 
  array (
    'md5' => 'fbbc03fbdebfa2bb2ea9f348fab485cd',
    'mtime' => 1675806412,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_product_template_id.php' => 
  array (
    'md5' => 'fc0bc64aaececd75df2a017505ebaf57',
    'mtime' => 1675806457,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_product_template_name.php' => 
  array (
    'md5' => '785e9cbe27686c83403eaf658c1ed737',
    'mtime' => 1675806457,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_subtotal.php' => 
  array (
    'md5' => 'f20436a14aa4667f2c4437bbdf91d19e',
    'mtime' => 1675806516,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_total_amount.php' => 
  array (
    'md5' => '25eee5e197f0a6572ca4936335cceb22',
    'mtime' => 1675806550,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_cost_price.php' => 
  array (
    'md5' => '7bce0da9609fb66f489255b50b8cdf08',
    'mtime' => 1676478393,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_likely_case.php' => 
  array (
    'md5' => 'a1bf4f34039e59a82e26ed83594096d2',
    'mtime' => 1676478592,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_sales_stage.php' => 
  array (
    'md5' => 'd02a9c921f82c3efcc4a7267db82ed4b',
    'mtime' => 1676478748,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_book_value.php' => 
  array (
    'md5' => '6dd0a740b9cd090ddf8c4852e40ea3a3',
    'mtime' => 1676478951,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_list_price.php' => 
  array (
    'md5' => 'acbd0e2a12df73a0b7fa0d41ddaddde3',
    'mtime' => 1676478998,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_quantity.php' => 
  array (
    'md5' => '4de5050012ed3bd89b53f0a63a336375',
    'mtime' => 1676479087,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_renewable.php' => 
  array (
    'md5' => 'f94a9bd67afb2c23dd902930af6b126e',
    'mtime' => 1676479167,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_service.php' => 
  array (
    'md5' => '7108b78e526d96aa7fc5135bb73af214',
    'mtime' => 1676479225,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_service_start_date.php' => 
  array (
    'md5' => '83708d3e676e56f92b95e52c9dc4a960',
    'mtime' => 1676479381,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_manufacturer_id.php' => 
  array (
    'md5' => '5e6c2f6714c291b21ab0f7168a532349',
    'mtime' => 1676480251,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_manufacturer_name.php' => 
  array (
    'md5' => '42743f011ec5e8380053dc328db0fecd',
    'mtime' => 1676480251,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_category_id.php' => 
  array (
    'md5' => '6c8e6c7531bf2bcdcf6663d7d5de9243',
    'mtime' => 1676480318,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_category_name.php' => 
  array (
    'md5' => '22b4c540281d3a8a3b80e970de555228',
    'mtime' => 1676480318,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_mft_part_num.php' => 
  array (
    'md5' => '20dc1261f2d2e850f44fdbbb643abfdd',
    'mtime' => 1676480363,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_discount_price.php' => 
  array (
    'md5' => '5566d0329ea44af20e98bd6ae4685eb9',
    'mtime' => 1676480490,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_discount_rate_percent.php' => 
  array (
    'md5' => 'efbeda33f6f00bf882e12bc02176fd9a',
    'mtime' => 1676480549,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_discount_amount_usdollar.php' => 
  array (
    'md5' => 'dd43326041621bb40c84974174d3156e',
    'mtime' => 1676480623,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_deal_calc.php' => 
  array (
    'md5' => '632d110a0c99037a653dd2cf8954b438',
    'mtime' => 1676480669,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_deal_calc_usdollar.php' => 
  array (
    'md5' => 'f63911e0ae72c03053353ae785ce7dec',
    'mtime' => 1676480710,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_cost_usdollar.php' => 
  array (
    'md5' => '84dfb96ac1d98ccdc93d95732e75686b',
    'mtime' => 1676480770,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_discount_usdollar.php' => 
  array (
    'md5' => '3aadf5de226c24599e447c64a3c03ed8',
    'mtime' => 1676480821,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_list_usdollar.php' => 
  array (
    'md5' => 'ae2745fced1cc7d7b55d0073c71d3796',
    'mtime' => 1676480852,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_status.php' => 
  array (
    'md5' => '2bf94c35eaa7081dda276f53b7038573',
    'mtime' => 1676480937,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_book_value_usdollar.php' => 
  array (
    'md5' => 'a2ec8f67c5a47f5aba8bab183ff05719',
    'mtime' => 1676480962,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_generate_purchase.php' => 
  array (
    'md5' => '7856c937b39a442b5879154c59159323',
    'mtime' => 1676481008,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_purchasedlineitem_id.php' => 
  array (
    'md5' => 'dfd76ece5fd6e084987fc3f7cca99053',
    'mtime' => 1676481095,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_purchasedlineitem_name.php' => 
  array (
    'md5' => 'a660e62245ceedb3d49cd29ba2ee766d',
    'mtime' => 1676481095,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_best_case.php' => 
  array (
    'md5' => 'f08eac89f7bb1885c68742b13cfe764f',
    'mtime' => 1676481171,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_worst_case.php' => 
  array (
    'md5' => '6733001a1c7433dc4c8827844b55a941',
    'mtime' => 1676481236,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_closed.php' => 
  array (
    'md5' => '3815fe441014ced316b4f2672fb6ded0',
    'mtime' => 1676481311,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_next_step.php' => 
  array (
    'md5' => '3d7e9f6b9043cbb2d88acdfbe18a6477',
    'mtime' => 1676481366,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_commit_stage.php' => 
  array (
    'md5' => 'd4dcbf869394f4965b6e472d266c94e4',
    'mtime' => 1676481414,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_probability.php' => 
  array (
    'md5' => '449ddddd056154b17d3018ccd9c1ad2c',
    'mtime' => 1676481450,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_lead_source.php' => 
  array (
    'md5' => '4293a7af02163c10be3791469ff057f0',
    'mtime' => 1676481510,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_campaign_id.php' => 
  array (
    'md5' => '9787996cbddd6e92d435e10febb2165b',
    'mtime' => 1676481570,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_campaign_name.php' => 
  array (
    'md5' => '72f84bfc4c635ed29e179ef4828e11f7',
    'mtime' => 1676481570,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_forecasted_likely.php' => 
  array (
    'md5' => '3d213de06a24d9d26309b7d06ba6447f',
    'mtime' => 1676481615,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_quote_id.php' => 
  array (
    'md5' => 'bd910dc18d30792536d33cc57db8ad80',
    'mtime' => 1676481685,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_quote_name.php' => 
  array (
    'md5' => '816584d997d2186b8c1c3c154ac589cd',
    'mtime' => 1676481685,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_product_type.php' => 
  array (
    'md5' => '174e835621f7cac9700072accaeb0af2',
    'mtime' => 1676481744,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_type_id.php' => 
  array (
    'md5' => 'ced026245ba03ef5ddfc6133154a4c7f',
    'mtime' => 1676481774,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_type_name.php' => 
  array (
    'md5' => '065bc7ca8fbcdb0eb770c6004c980f67',
    'mtime' => 1676481774,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_account_id.php' => 
  array (
    'md5' => '3e8a46059f5319b108aefe295498f3ca',
    'mtime' => 1676481858,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_account_name.php' => 
  array (
    'md5' => '3b3aac4dda6d37c19e1483c60e1f7d2a',
    'mtime' => 1676481858,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_add_on_to_id.php' => 
  array (
    'md5' => '2facc0e0bf160f09bb1ad6ddb5f5f21b',
    'mtime' => 1676481923,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_add_on_to_name.php' => 
  array (
    'md5' => 'e0278751fb47a0ce22fab91aca2524f3',
    'mtime' => 1676481923,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_renewal_rli_id.php' => 
  array (
    'md5' => '5d8f16a6b5a003e3fe2a1cf839263206',
    'mtime' => 1676481972,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_renewal_rli_name.php' => 
  array (
    'md5' => '78ea2f2198154c021e0036638073ed04',
    'mtime' => 1676481972,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_sync_key.php' => 
  array (
    'md5' => 'cf58d19f8c52fc126a259ecc83789fbe',
    'mtime' => 1676482031,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_asset_number.php' => 
  array (
    'md5' => '03a7528cd1f2cbda28b0f248f1a08c7a',
    'mtime' => 1676482077,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_book_value_date.php' => 
  array (
    'md5' => 'b4d48abfe95158c7293f8090c0681287',
    'mtime' => 1676482110,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_purchased.php' => 
  array (
    'md5' => 'c7c107e1d6d57534f391d1e5eeab0041',
    'mtime' => 1676482152,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_support_expires.php' => 
  array (
    'md5' => 'ced5fcad8fb983933bb89608e0aacc65',
    'mtime' => 1676482197,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_support_starts.php' => 
  array (
    'md5' => '1e61904a52176169910d6209c736f889',
    'mtime' => 1676482238,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_pricing_factor.php' => 
  array (
    'md5' => '4a00b830400a1df6b9b956a55bc97e5c',
    'mtime' => 1676482285,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_pricing_formula.php' => 
  array (
    'md5' => 'd8f88ebf6b21de2f91aa3abefd53c9c9',
    'mtime' => 1676482318,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_serial_number.php' => 
  array (
    'md5' => '05803dfa40d7a7a2a53bbfd52ff5ed4b',
    'mtime' => 1676482350,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_support_contact.php' => 
  array (
    'md5' => '00663ab4b271abbfc704dd6a70a22645',
    'mtime' => 1676482399,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_support_description.php' => 
  array (
    'md5' => '78e67c84951ec7e958ddbfed292561a7',
    'mtime' => 1676482433,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_support_name.php' => 
  array (
    'md5' => 'e85fc3b2572ac0c207a9669564bda179',
    'mtime' => 1676482463,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_support_term.php' => 
  array (
    'md5' => '772efe2f77173083da68390502e9b487',
    'mtime' => 1676482502,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_tax_class.php' => 
  array (
    'md5' => '4cc09da0e310f970e3b71a343957e606',
    'mtime' => 1676482549,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_vendor_part_num.php' => 
  array (
    'md5' => 'd6a00190090404e52904a43dfa5d2587',
    'mtime' => 1676482577,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_website.php' => 
  array (
    'md5' => '1509efe5ad54b898a5c52df1b3d771a9',
    'mtime' => 1676482627,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_weight.php' => 
  array (
    'md5' => 'ec136f96da9605f1a3418238bfa176bf',
    'mtime' => 1676482658,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_base_rate.php' => 
  array (
    'md5' => 'db73dcb60adf33e31eda5954eaa41300',
    'mtime' => 1676482695,
    'is_override' => false,
  ),
  'custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sasa1_sasa_seguradoras_revenuelineitems_1_RevenueLineItems.php' => 
  array (
    'md5' => '77c71b73d5da37673854fa5e7e29fc32',
    'mtime' => 1685043973,
    'is_override' => false,
  ),
);
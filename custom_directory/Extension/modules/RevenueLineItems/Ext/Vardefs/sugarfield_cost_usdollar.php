<?php
 // created: 2023-02-15 17:06:10
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['comments']='Cost expressed in USD';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
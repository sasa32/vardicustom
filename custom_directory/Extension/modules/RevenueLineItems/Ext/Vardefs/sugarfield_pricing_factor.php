<?php
 // created: 2023-02-15 17:31:25
$dictionary['RevenueLineItem']['fields']['pricing_factor']['len']='4';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['audited']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['comments']='Variable pricing factor depending on pricing_formula';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['importable']='false';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['pricing_factor']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['pricing_factor']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['min']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['max']=false;
$dictionary['RevenueLineItem']['fields']['pricing_factor']['disable_num_format']='';

 ?>
<?php
// created: 2023-05-25 19:46:01
$dictionary["RevenueLineItem"]["fields"]["sasa1_sasa_seguradoras_revenuelineitems_1"] = array (
  'name' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'type' => 'link',
  'relationship' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'source' => 'non-db',
  'module' => 'SASA1_sasa_seguradoras',
  'bean_name' => 'SASA1_sasa_seguradoras',
  'side' => 'right',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_REVENUELINEITEMS_TITLE',
  'id_name' => 'sasa1_sasabb9eradoras_ida',
  'link-type' => 'one',
);
$dictionary["RevenueLineItem"]["fields"]["sasa1_sasa_seguradoras_revenuelineitems_1_name"] = array (
  'name' => 'sasa1_sasa_seguradoras_revenuelineitems_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_SASA1_SASA_SEGURADORAS_TITLE',
  'save' => true,
  'id_name' => 'sasa1_sasabb9eradoras_ida',
  'link' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'table' => 'sasa1_sasa_seguradoras',
  'module' => 'SASA1_sasa_seguradoras',
  'rname' => 'name',
);
$dictionary["RevenueLineItem"]["fields"]["sasa1_sasabb9eradoras_ida"] = array (
  'name' => 'sasa1_sasabb9eradoras_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_REVENUELINEITEMS_TITLE_ID',
  'id_name' => 'sasa1_sasabb9eradoras_ida',
  'link' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'table' => 'sasa1_sasa_seguradoras',
  'module' => 'SASA1_sasa_seguradoras',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

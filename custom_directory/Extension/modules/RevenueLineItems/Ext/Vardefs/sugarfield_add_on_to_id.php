<?php
 // created: 2023-02-15 17:25:23
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['name']='add_on_to_id';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['comment']='Purchased line item that this is an add-on to';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['vname']='LBL_ADD_ON_TO_ID';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['rname']='id';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['dbType']='id';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['table']='purchased_line_items';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['isnull']='true';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['module']='PurchasedLineItems';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['add_on_to_id']['importable']='false';

 ?>
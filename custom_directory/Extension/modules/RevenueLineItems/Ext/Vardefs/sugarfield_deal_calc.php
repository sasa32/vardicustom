<?php
 // created: 2023-02-15 17:04:29
$dictionary['RevenueLineItem']['fields']['deal_calc']['len']=26;
$dictionary['RevenueLineItem']['fields']['deal_calc']['audited']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc']['comments']='deal_calc';
$dictionary['RevenueLineItem']['fields']['deal_calc']['importable']='false';
$dictionary['RevenueLineItem']['fields']['deal_calc']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['deal_calc']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['deal_calc']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['deal_calc']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['deal_calc']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['deal_calc']['enable_range_search']=false;

 ?>
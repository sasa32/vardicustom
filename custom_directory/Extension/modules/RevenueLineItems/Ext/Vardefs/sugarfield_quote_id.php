<?php
 // created: 2023-02-15 17:21:25
$dictionary['RevenueLineItem']['fields']['quote_id']['name']='quote_id';
$dictionary['RevenueLineItem']['fields']['quote_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['quote_id']['vname']='LBL_QUOTE_ID';
$dictionary['RevenueLineItem']['fields']['quote_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['quote_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['quote_id']['comment']='If product created via Quote, this is quote ID';
$dictionary['RevenueLineItem']['fields']['quote_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['quote_id']['importable']='false';

 ?>
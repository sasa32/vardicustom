<?php
 // created: 2023-02-15 17:16:06
$dictionary['RevenueLineItem']['fields']['next_step']['audited']=false;
$dictionary['RevenueLineItem']['fields']['next_step']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['next_step']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['next_step']['comments']='The next step in the sales process';
$dictionary['RevenueLineItem']['fields']['next_step']['importable']='false';
$dictionary['RevenueLineItem']['fields']['next_step']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['next_step']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['next_step']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['next_step']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['next_step']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.49',
  'searchable' => true,
);
$dictionary['RevenueLineItem']['fields']['next_step']['calculated']=false;

 ?>
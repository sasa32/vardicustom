<?php
 // created: 2023-02-07 21:47:37
$dictionary['RevenueLineItem']['fields']['product_template_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['product_template_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['product_template_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['product_template_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['product_template_name']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['product_template_name']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['product_template_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['product_template_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['product_template_name']['calculated']=false;

 ?>
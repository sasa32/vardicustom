<?php
 // created: 2023-02-15 17:11:35
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['name']='purchasedlineitem_id';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['vname']='LBL_PLI_ID';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['comment']='If PLI was created from this RLI, this is that PLIs ID';
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['purchasedlineitem_id']['importable']='false';

 ?>
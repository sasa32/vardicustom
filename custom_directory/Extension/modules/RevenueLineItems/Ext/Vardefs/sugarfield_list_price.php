<?php
 // created: 2023-02-15 16:36:38
$dictionary['RevenueLineItem']['fields']['list_price']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['list_price']['len']=26;
$dictionary['RevenueLineItem']['fields']['list_price']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['comments']='List price of sales item';
$dictionary['RevenueLineItem']['fields']['list_price']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['list_price']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['list_price']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['list_price']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['list_price']['audited']=false;
$dictionary['RevenueLineItem']['fields']['list_price']['readonly']=true;

 ?>
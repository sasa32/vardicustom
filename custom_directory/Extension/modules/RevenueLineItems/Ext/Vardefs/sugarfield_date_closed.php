<?php
 // created: 2023-02-15 17:15:11
$dictionary['RevenueLineItem']['fields']['date_closed']['required']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['audited']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['options']='';
$dictionary['RevenueLineItem']['fields']['date_closed']['comments']='Expected or actual date the product (for opportunity) will close';
$dictionary['RevenueLineItem']['fields']['date_closed']['importable']='false';
$dictionary['RevenueLineItem']['fields']['date_closed']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['date_closed']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['date_closed']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_closed']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['full_text_search']=array (
);
$dictionary['RevenueLineItem']['fields']['date_closed']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_closed']['related_fields']=array (
);
$dictionary['RevenueLineItem']['fields']['date_closed']['enable_range_search']=false;

 ?>
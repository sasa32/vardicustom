<?php
 // created: 2023-02-15 17:33:53
$dictionary['RevenueLineItem']['fields']['support_description']['len']='255';
$dictionary['RevenueLineItem']['fields']['support_description']['audited']=false;
$dictionary['RevenueLineItem']['fields']['support_description']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_description']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_description']['comments']='Description of sales item for support purposes';
$dictionary['RevenueLineItem']['fields']['support_description']['importable']='false';
$dictionary['RevenueLineItem']['fields']['support_description']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['support_description']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['support_description']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['support_description']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['support_description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['support_description']['calculated']=false;

 ?>
<?php
 // created: 2023-02-15 17:35:02
$dictionary['RevenueLineItem']['fields']['support_term']['len']='100';
$dictionary['RevenueLineItem']['fields']['support_term']['audited']=false;
$dictionary['RevenueLineItem']['fields']['support_term']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_term']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_term']['comments']='Term (length) of support contract';
$dictionary['RevenueLineItem']['fields']['support_term']['importable']='false';
$dictionary['RevenueLineItem']['fields']['support_term']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['support_term']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['support_term']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['support_term']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['support_term']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['support_term']['calculated']=false;

 ?>
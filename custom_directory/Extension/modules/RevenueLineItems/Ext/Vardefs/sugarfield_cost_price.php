<?php
 // created: 2023-02-15 16:26:33
$dictionary['RevenueLineItem']['fields']['cost_price']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['cost_price']['len']=26;
$dictionary['RevenueLineItem']['fields']['cost_price']['audited']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['comments']='Product cost ("Cost" in Quote)';
$dictionary['RevenueLineItem']['fields']['cost_price']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['cost_price']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['cost_price']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['cost_price']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['cost_price']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['readonly']=true;

 ?>
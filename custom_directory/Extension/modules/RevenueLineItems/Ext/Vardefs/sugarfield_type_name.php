<?php
 // created: 2023-02-15 17:22:54
$dictionary['RevenueLineItem']['fields']['type_name']['len']=255;
$dictionary['RevenueLineItem']['fields']['type_name']['audited']=false;
$dictionary['RevenueLineItem']['fields']['type_name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['type_name']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['type_name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['type_name']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['type_name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['type_name']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['type_name']['calculated']=false;

 ?>
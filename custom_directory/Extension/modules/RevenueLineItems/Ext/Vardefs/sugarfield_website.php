<?php
 // created: 2023-02-15 17:37:07
$dictionary['RevenueLineItem']['fields']['website']['len']='255';
$dictionary['RevenueLineItem']['fields']['website']['audited']=false;
$dictionary['RevenueLineItem']['fields']['website']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['website']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['website']['comments']='Sales item URL';
$dictionary['RevenueLineItem']['fields']['website']['importable']='false';
$dictionary['RevenueLineItem']['fields']['website']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['website']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['website']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['website']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['website']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['website']['calculated']=false;

 ?>
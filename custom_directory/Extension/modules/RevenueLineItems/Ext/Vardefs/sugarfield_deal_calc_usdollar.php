<?php
 // created: 2023-02-15 17:05:10
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['comments']='deal_calc_usdollar';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['deal_calc_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
<?php
 // created: 2023-02-15 17:33:19
$dictionary['RevenueLineItem']['fields']['support_contact']['len']='50';
$dictionary['RevenueLineItem']['fields']['support_contact']['audited']=false;
$dictionary['RevenueLineItem']['fields']['support_contact']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_contact']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['support_contact']['comments']='Contact for support purposes';
$dictionary['RevenueLineItem']['fields']['support_contact']['importable']='false';
$dictionary['RevenueLineItem']['fields']['support_contact']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['support_contact']['duplicate_merge_dom_value']='0';
$dictionary['RevenueLineItem']['fields']['support_contact']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['support_contact']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['support_contact']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['support_contact']['calculated']=false;

 ?>
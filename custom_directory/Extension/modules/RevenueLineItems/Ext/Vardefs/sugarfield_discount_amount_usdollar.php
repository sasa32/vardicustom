<?php
 // created: 2023-02-15 17:03:43
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['audited']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['hidemassupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['discount_amount_usdollar']['precision']=8;

 ?>
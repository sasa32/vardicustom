<?php
 // created: 2023-02-07 21:47:37
$dictionary['RevenueLineItem']['fields']['product_template_id']['name']='product_template_id';
$dictionary['RevenueLineItem']['fields']['product_template_id']['type']='id';
$dictionary['RevenueLineItem']['fields']['product_template_id']['vname']='LBL_PRODUCT_TEMPLATE_ID';
$dictionary['RevenueLineItem']['fields']['product_template_id']['required']=false;
$dictionary['RevenueLineItem']['fields']['product_template_id']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['product_template_id']['comment']='Product (in Admin Products) from which this product is derived (in user Products)';
$dictionary['RevenueLineItem']['fields']['product_template_id']['audited']=false;
$dictionary['RevenueLineItem']['fields']['product_template_id']['importable']='true';

 ?>
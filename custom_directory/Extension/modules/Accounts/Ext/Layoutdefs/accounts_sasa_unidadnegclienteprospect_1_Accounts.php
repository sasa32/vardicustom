<?php
 // created: 2020-05-21 16:21:34
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_unidadnegclienteprospect_1'] = array (
  'order' => 100,
  'module' => 'SASA_UnidadNegClienteProspect',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'get_subpanel_data' => 'accounts_sasa_unidadnegclienteprospect_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

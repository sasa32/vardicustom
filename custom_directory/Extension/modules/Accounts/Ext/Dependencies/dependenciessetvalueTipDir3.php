<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipDir3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_direccion3_c'),
	'trigger' => 'equal($sasa_direccion3_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec3_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_6sasa_municipios_ida',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

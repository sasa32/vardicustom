<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['setVisibilitydireccion2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('billing_address_street'),
	//'trigger' => 'equal($sasa_phone_alternate_c,"123")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'shipping_address',
				'value' => '
					or(not(equal($billing_address_street,0)),not(equal($shipping_address_street,0)))
				',
			),
		),
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => '
					not(equal($billing_address_street,0))
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'shipping_address',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_tipodirec2_c',
				'value' => 'false'
			)
		),
	)
);

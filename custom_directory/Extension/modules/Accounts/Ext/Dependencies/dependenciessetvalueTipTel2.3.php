<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel2.3'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_municipios_accounts_4_name'),
	'trigger' => 'equal($sasa_municipios_accounts_4_name,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_alternate_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_tipotel2_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension2_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['Accounts']['dependenciessetvalueTipTel1.2'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_tipotel1_c'),
	'trigger' => 'equal($sasa_tipotel1_c,"")',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_phone_office_c',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_municipios_accounts_3sasa_municipios_ida',
				'value' => '',
			),
		),
		array(
			'name' => 'SetValue',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_extension_c',
				'value' => '',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		
	)
);

<?php
$hook_version = 1;

$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	2,

	//Label. A string value to identify the hook.
	'AccountsRelContactsFieldGender',

	//The PHP file where your class is located.
	'custom/modules/Accounts/AccountsRelContactsFieldGender.php',

	//The class the method is in.
	'before_save_class_AccountsRelContactsFieldGender',

	//The method to call.
	'before_save_method_AccountsRelContactsFieldGender'
);

?>

<?php
$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'estructura_direccionAccounts',

	//The PHP file where your class is located.
	'custom/modules/Contacts/estructura_direccion.php',

	//The class the method is in.
	'estructura_direccion',

	//The method to call.
	'after_save'
);

?>

<?php
 // created: 2021-06-15 16:32:13
$dictionary['Account']['fields']['sasa_primerapellido_c']['labelValue']='Primer Apellido';
$dictionary['Account']['fields']['sasa_primerapellido_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_primerapellido_c']['enforced']='';
$dictionary['Account']['fields']['sasa_primerapellido_c']['dependency']='equal($sasa_tipo_persona_c,"N")';

 ?>
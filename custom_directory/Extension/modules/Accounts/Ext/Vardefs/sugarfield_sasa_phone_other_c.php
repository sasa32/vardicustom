<?php
 // created: 2021-06-15 16:32:13
$dictionary['Account']['fields']['sasa_phone_other_c']['labelValue']='Teléfono 3';
$dictionary['Account']['fields']['sasa_phone_other_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_phone_other_c']['enforced']='';
$dictionary['Account']['fields']['sasa_phone_other_c']['dependency']='not(equal($sasa_phone_alternate_c,""))';

 ?>
<?php
 // created: 2021-06-15 16:32:09
$dictionary['Account']['fields']['sasa_last_name_2_c']['labelValue']='Segundo Apellido';
$dictionary['Account']['fields']['sasa_last_name_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_last_name_2_c']['enforced']='';
$dictionary['Account']['fields']['sasa_last_name_2_c']['dependency']='equal($sasa_tipo_persona_c,"N")';

 ?>
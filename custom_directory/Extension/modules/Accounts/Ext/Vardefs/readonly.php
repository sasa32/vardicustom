<?php
/*
* Requerimiento en https://sasaconsultoria.sugarondemand.com/index.php#Tasks/5ea23328-7e7a-11ea-b3b6-02fb8f607ac4
*/
$dictionary["Account"]["fields"]["billing_address_city"]['readonly'] = true;
$dictionary["Account"]["fields"]["billing_address_state"]['readonly'] = true;
$dictionary["Account"]["fields"]["billing_address_country"]['readonly'] = true;

$dictionary["Account"]["fields"]["shipping_address_city"]['readonly'] = true;
$dictionary["Account"]["fields"]["shipping_address_state"]['readonly'] = true;
$dictionary["Account"]["fields"]["shipping_address_country"]['readonly'] = true;

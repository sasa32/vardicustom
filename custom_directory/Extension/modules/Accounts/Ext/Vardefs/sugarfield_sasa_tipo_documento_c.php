<?php
 // created: 2021-06-15 16:32:21
$dictionary['Account']['fields']['sasa_tipo_documento_c']['labelValue']='Tipo Documento';
$dictionary['Account']['fields']['sasa_tipo_documento_c']['dependency']='';
$dictionary['Account']['fields']['sasa_tipo_documento_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipo_persona_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'N' => 
    array (
      0 => '',
      1 => 'C',
      2 => 'E',
      3 => 'T',
      4 => 'P',
      5 => 'D',
      6 => 'U',
    ),
    'J' => 
    array (
      0 => 'N',
    ),
  ),
);

 ?>
<?php
 // created: 2020-07-31 10:55:08
$dictionary['Account']['fields']['shipping_address_city']['len']='100';
$dictionary['Account']['fields']['shipping_address_city']['audited']=true;
$dictionary['Account']['fields']['shipping_address_city']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_city']['comments']='The city used for the shipping address';
$dictionary['Account']['fields']['shipping_address_city']['importable']='false';
$dictionary['Account']['fields']['shipping_address_city']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['shipping_address_city']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['shipping_address_city']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_city']['calculated']=false;

 ?>
<?php
 // created: 2021-06-15 16:32:04
$dictionary['Account']['fields']['sasa_extension3_c']['labelValue']='Extensión Teléfono 3';
$dictionary['Account']['fields']['sasa_extension3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_extension3_c']['enforced']='';
$dictionary['Account']['fields']['sasa_extension3_c']['dependency']='equal($sasa_tipotel3_c,"OF")';

 ?>
<?php
 // created: 2021-06-15 16:31:55
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['labelValue']='Cel 2';
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['calculated']='1';
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['formula']='concat("+57",$sasa_cel_alternativo_c)';
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['enforced']='1';
$dictionary['Account']['fields']['sasa_celular_alternativo_c']['dependency']='';

 ?>
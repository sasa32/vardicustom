<?php
 // created: 2020-07-31 13:28:16
$dictionary['Account']['fields']['phone_alternate']['len']='100';
$dictionary['Account']['fields']['phone_alternate']['audited']=false;
$dictionary['Account']['fields']['phone_alternate']['massupdate']=false;
$dictionary['Account']['fields']['phone_alternate']['comments']='';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['phone_alternate']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_alternate']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.03',
  'searchable' => false,
);
$dictionary['Account']['fields']['phone_alternate']['calculated']='1';
$dictionary['Account']['fields']['phone_alternate']['importable']='false';
$dictionary['Account']['fields']['phone_alternate']['formula']='concat(related($sasa_municipios_accounts_4,"sasa_indicativo_c"),$sasa_phone_alternate_c)';
$dictionary['Account']['fields']['phone_alternate']['enforced']=true;

 ?>
<?php
 // created: 2020-07-31 10:55:48
$dictionary['Account']['fields']['shipping_address_country']['len']='255';
$dictionary['Account']['fields']['shipping_address_country']['audited']=true;
$dictionary['Account']['fields']['shipping_address_country']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_country']['comments']='The country used for the shipping address';
$dictionary['Account']['fields']['shipping_address_country']['importable']='false';
$dictionary['Account']['fields']['shipping_address_country']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['shipping_address_country']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['shipping_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_country']['calculated']=false;

 ?>
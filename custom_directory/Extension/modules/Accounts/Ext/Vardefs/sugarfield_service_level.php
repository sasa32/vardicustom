<?php
 // created: 2020-07-15 16:41:06
$dictionary['Account']['fields']['service_level']['audited']=false;
$dictionary['Account']['fields']['service_level']['massupdate']=true;
$dictionary['Account']['fields']['service_level']['comments']='An indication of the service level of a company';
$dictionary['Account']['fields']['service_level']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['service_level']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['service_level']['merge_filter']='disabled';
$dictionary['Account']['fields']['service_level']['calculated']=false;
$dictionary['Account']['fields']['service_level']['dependency']=false;
$dictionary['Account']['fields']['service_level']['reportable']=false;

 ?>
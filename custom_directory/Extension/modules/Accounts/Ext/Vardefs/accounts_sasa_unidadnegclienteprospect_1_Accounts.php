<?php
// created: 2020-05-21 16:21:34
$dictionary["Account"]["fields"]["accounts_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'accounts_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'SASA_UnidadNegClienteProspect',
  'bean_name' => 'SASA_UnidadNegClienteProspect',
  'vname' => 'LBL_ACCOUNTS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasa_unidadnegclienteprospect_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

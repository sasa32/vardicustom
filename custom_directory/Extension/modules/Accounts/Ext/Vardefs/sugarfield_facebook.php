<?php
 // created: 2020-05-20 15:17:11
$dictionary['Account']['fields']['facebook']['audited']=false;
$dictionary['Account']['fields']['facebook']['massupdate']=false;
$dictionary['Account']['fields']['facebook']['comments']='The facebook name of the company';
$dictionary['Account']['fields']['facebook']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['facebook']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['facebook']['merge_filter']='disabled';
$dictionary['Account']['fields']['facebook']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['facebook']['calculated']=0;
$dictionary['Account']['fields']['facebook']['importable']=0;
$dictionary['Account']['fields']['facebook']['formula']=0;
 ?>
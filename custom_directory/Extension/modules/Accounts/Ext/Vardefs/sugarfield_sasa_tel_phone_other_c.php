<?php
 // created: 2021-06-15 16:32:16
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['labelValue']='Tel 3';
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['calculated']='1';
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['formula']='concat(related($sasa_municipios_accounts_5,"sasa_indicativo_c"),$sasa_phone_other_c)';
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['enforced']='1';
$dictionary['Account']['fields']['sasa_tel_phone_other_c']['dependency']='';

 ?>
<?php
 // created: 2021-06-15 16:31:55
$dictionary['Account']['fields']['sasa_celular_principal_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_celular_principal_c']['labelValue']='Cel 1';
$dictionary['Account']['fields']['sasa_celular_principal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_celular_principal_c']['calculated']='1';
$dictionary['Account']['fields']['sasa_celular_principal_c']['formula']='concat("+57",$sasa_cel_principal_c)';
$dictionary['Account']['fields']['sasa_celular_principal_c']['enforced']='1';
$dictionary['Account']['fields']['sasa_celular_principal_c']['dependency']='';

 ?>
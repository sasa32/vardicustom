<?php
 // created: 2020-05-20 15:06:03
$dictionary['Account']['fields']['sasa_getemails_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_getemails_c']['labelValue']='sasa_getemails_c';
$dictionary['Account']['fields']['sasa_getemails_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_getemails_c']['calculated']='true';
$dictionary['Account']['fields']['sasa_getemails_c']['formula']='getEmails("olaKace")';
$dictionary['Account']['fields']['sasa_getemails_c']['enforced']='true';
$dictionary['Account']['fields']['sasa_getemails_c']['dependency']='';

 ?>
<?php
 // created: 2021-06-15 16:32:09
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['options']='numeric_range_search_dom';
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['labelValue']='Ingreso Mensual (COP)';
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['enforced']='';
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['dependency']='';
$dictionary['Account']['fields']['sasa_ingreso_mensual_c']['enable_range_search']='1';

 ?>
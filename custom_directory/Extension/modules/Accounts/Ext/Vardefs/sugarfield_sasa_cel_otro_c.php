<?php
 // created: 2021-06-15 16:31:56
$dictionary['Account']['fields']['sasa_cel_otro_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_cel_otro_c']['labelValue']='Cel 3';
$dictionary['Account']['fields']['sasa_cel_otro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_cel_otro_c']['calculated']='true';
$dictionary['Account']['fields']['sasa_cel_otro_c']['formula']='concat("+57",$sasa_celular_otro_c)';
$dictionary['Account']['fields']['sasa_cel_otro_c']['enforced']='true';
$dictionary['Account']['fields']['sasa_cel_otro_c']['dependency']='';

 ?>
<?php
 // created: 2020-07-31 10:54:10
$dictionary['Account']['fields']['billing_address_postalcode']['audited']=true;
$dictionary['Account']['fields']['billing_address_postalcode']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_postalcode']['comments']='The postal code used for billing address';
$dictionary['Account']['fields']['billing_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_postalcode']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_postalcode']['calculated']=false;

 ?>
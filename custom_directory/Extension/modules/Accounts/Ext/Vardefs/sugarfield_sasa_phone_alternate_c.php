<?php
 // created: 2021-06-15 16:32:12
$dictionary['Account']['fields']['sasa_phone_alternate_c']['labelValue']='Teléfono 2';
$dictionary['Account']['fields']['sasa_phone_alternate_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Account']['fields']['sasa_phone_alternate_c']['enforced']='';
$dictionary['Account']['fields']['sasa_phone_alternate_c']['dependency']='not(equal($sasa_phone_office_c,""))';

 ?>
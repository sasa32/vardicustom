<?php
 // created: 2021-06-15 16:32:03
$dictionary['Account']['fields']['sasa_extension2_c']['labelValue']='Extensión Teléfono 2';
$dictionary['Account']['fields']['sasa_extension2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_extension2_c']['enforced']='';
$dictionary['Account']['fields']['sasa_extension2_c']['dependency']='equal($sasa_tipotel2_c,"OF")';

 ?>
<?php
 // created: 2021-06-15 16:31:56
$dictionary['Account']['fields']['sasa_cel_alternativo_c']['labelValue']='Celular 2';
$dictionary['Account']['fields']['sasa_cel_alternativo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_cel_alternativo_c']['enforced']='';
$dictionary['Account']['fields']['sasa_cel_alternativo_c']['dependency']='not(equal($sasa_cel_principal_c,""))';

 ?>
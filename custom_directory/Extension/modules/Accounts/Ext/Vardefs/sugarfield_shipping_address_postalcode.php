<?php
 // created: 2020-07-31 10:55:28
$dictionary['Account']['fields']['shipping_address_postalcode']['len']='20';
$dictionary['Account']['fields']['shipping_address_postalcode']['audited']=true;
$dictionary['Account']['fields']['shipping_address_postalcode']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_postalcode']['comments']='The zip code used for the shipping address';
$dictionary['Account']['fields']['shipping_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_postalcode']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_postalcode']['calculated']=false;

 ?>
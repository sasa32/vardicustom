<?php
 // created: 2021-06-15 16:32:06
$dictionary['Account']['fields']['sasa_extension_c']['labelValue']='Extensión Teléfono 1';
$dictionary['Account']['fields']['sasa_extension_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_extension_c']['enforced']='';
$dictionary['Account']['fields']['sasa_extension_c']['dependency']='equal($sasa_tipotel1_c,"OF")';

 ?>
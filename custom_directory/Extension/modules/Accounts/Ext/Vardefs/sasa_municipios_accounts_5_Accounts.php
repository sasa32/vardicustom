<?php
// created: 2020-07-31 09:41:58
$dictionary["Account"]["fields"]["sasa_municipios_accounts_5"] = array (
  'name' => 'sasa_municipios_accounts_5',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_5',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE',
  'id_name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_5_name"] = array (
  'name' => 'sasa_municipios_accounts_5_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_5',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["sasa_municipios_accounts_5sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'link' => 'sasa_municipios_accounts_5',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

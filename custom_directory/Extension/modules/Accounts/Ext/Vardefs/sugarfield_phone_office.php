<?php
 // created: 2020-07-31 13:28:03
$dictionary['Account']['fields']['phone_office']['len']='100';
$dictionary['Account']['fields']['phone_office']['massupdate']=false;
$dictionary['Account']['fields']['phone_office']['comments']='';
$dictionary['Account']['fields']['phone_office']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['phone_office']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_office']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.05',
  'searchable' => false,
);
$dictionary['Account']['fields']['phone_office']['calculated']='1';
$dictionary['Account']['fields']['phone_office']['importable']='false';
$dictionary['Account']['fields']['phone_office']['formula']='concat(related($sasa_municipios_accounts_3,"sasa_indicativo_c"),$sasa_phone_office_c)';
$dictionary['Account']['fields']['phone_office']['enforced']=true;
$dictionary['Account']['fields']['phone_office']['reportable']=true;
$dictionary['Account']['fields']['phone_office']['audited']=false;

 ?>
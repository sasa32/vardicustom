<?php
 // created: 2022-12-07 14:23:53
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['labelValue']='Código Sucursal';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['enforced']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['dependency']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['required_formula']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codsucursal_c']['readonly_formula']='';

 ?>
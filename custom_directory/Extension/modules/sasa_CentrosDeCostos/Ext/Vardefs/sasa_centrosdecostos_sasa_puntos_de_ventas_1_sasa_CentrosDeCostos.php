<?php
// created: 2022-10-04 17:32:58
$dictionary["sasa_CentrosDeCostos"]["fields"]["sasa_centrosdecostos_sasa_puntos_de_ventas_1"] = array (
  'name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'type' => 'link',
  'relationship' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_CENTROSDECOSTOS_TITLE',
  'id_name' => 'sasa_centr37aaecostos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

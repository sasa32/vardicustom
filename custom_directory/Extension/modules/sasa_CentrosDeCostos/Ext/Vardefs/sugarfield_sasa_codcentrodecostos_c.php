<?php
 // created: 2022-12-05 17:33:27
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['labelValue']='Código Centro de Costos';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['enforced']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['dependency']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['required_formula']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_codcentrodecostos_c']['readonly_formula']='';

 ?>
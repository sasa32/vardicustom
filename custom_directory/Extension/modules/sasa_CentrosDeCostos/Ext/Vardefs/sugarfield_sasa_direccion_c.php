<?php
 // created: 2022-12-05 17:35:43
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['labelValue']='Dirección';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['enforced']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['dependency']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['required_formula']='';
$dictionary['sasa_CentrosDeCostos']['fields']['sasa_direccion_c']['readonly_formula']='';

 ?>
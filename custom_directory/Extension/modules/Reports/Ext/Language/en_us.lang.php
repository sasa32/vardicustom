<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Cliente y Prospecto';
$mod_strings['LBL_ACCOUNTS'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT'] = 'Cliente y Prospecto';
$mod_strings['LBL_ACCOUNT_REPORTS'] = 'Cliente y Prospecto Reports';
$mod_strings['LBL_MY_TEAM_ACCOUNT_REPORTS'] = 'My Team&#039;s Cliente y Prospecto Reports';
$mod_strings['LBL_MY_ACCOUNT_REPORTS'] = 'My Cliente y Prospecto Reports';
$mod_strings['LBL_PUBLISHED_ACCOUNT_REPORTS'] = 'Published Cliente y Prospecto Reports';
$mod_strings['DEFAULT_REPORT_TITLE_3'] = 'Partner Cliente y Prospecto List';
$mod_strings['DEFAULT_REPORT_TITLE_4'] = 'Customer Cliente y Prospecto List';
$mod_strings['DEFAULT_REPORT_TITLE_16'] = 'Clientes y Prospectos By Type By Industry';
$mod_strings['DEFAULT_REPORT_TITLE_43'] = 'Customer Cliente y Prospecto Owners';
$mod_strings['DEFAULT_REPORT_TITLE_44'] = 'My New Customer Clientes y Prospectos';

<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_EMAILS_ACCOUNTS_REL'] = 'Clientes y Prospectos';
$mod_strings['LBL_EMAILS_LEADS_REL'] = 'Leads';
$mod_strings['LBL_OPPORTUNITY_SUBPANEL_TITLE'] = 'Cotizaciones';
$mod_strings['LBL_EMAILS_OPPORTUNITIES_REL'] = 'Cotizaciones';

<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202105190505.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.gvleadscustomfieldsv009202105190505.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/temp.php

	
	//campos Nativos
	$mod_strings['LBL_HOME_PHONE'] = 'Tel Fijo';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Tel Celular';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Tel Oficina';
	$mod_strings['LBL_OTHER_PHONE'] = 'Tel Alternativo';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Origen';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Razon Social';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-mail';
	//Campos Custom
	$mod_strings['LBL_SASA_DETALLE_ORIGEN_C'] = 'Detalle de Origen';
	$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
	$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_C'] = 'Unidad de Negocio';
	$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañia';
	$mod_strings['LBL_SASA_TIPO_DE_PERSONA_C'] = 'Tipo de Persona';
	$mod_strings['LBL_SASA_TIPO_DOCUMENTO_C'] = 'Tipo de Documento';
	$mod_strings['LBL_SASA_NUMERO_DOCUMENTO_C'] = 'Número de Documento';
	$mod_strings['LBL_SASA_LAST_NAME_2_C'] = 'Segundo Apellido';
	$mod_strings['LBL_SASA_NOMBRES_C'] = 'Nombres';
	$mod_strings['LBL_SASA_PRIMERAPELLIDO_C'] = 'Primer Apellido';
	$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
	$mod_strings['LBL_SASA_MUNICIPIO_PRINCIPAL_2_C'] = 'Municipio Principal 2 (Integración)';
	$mod_strings['LBL_SASA_PHONE_HOME_C'] = 'Teléfono Fijo';
	$mod_strings['LBL_SASA_PHONE_MOBILE_C'] = 'Teléfono Celular';
	$mod_strings['LBL_SASA_MUNICIPIO_SECUNDARIO_2_C'] = 'Municipio Secundario 2 (Integración)';
	$mod_strings['LBL_SASA_NUM_DOCU_JURIDICA_C'] = 'Número de Documento Persona Jurídica';
	$mod_strings['LBL_SASA_TIPO_SOLICITUD_C'] = 'Tipo de solicitud';
	$mod_strings['LBL_SASA_COD_PUNTOATENCION_C'] = 'Código del punto de Venta';
	$mod_strings['LBL_SASA_COD_LINEA_C'] = 'Código Linea Vehículo';
	$mod_strings['LBL_SASA_HELIOSLEADID_C'] = 'ID Registro PACE';
	$mod_strings['LBL_CD_AREA_C'] = 'Subunidad de Negocio';
	$mod_strings['LBL_SASA_TIPODEFORMULARIO_C'] = 'Nombre de Formulario';
	$mod_strings['LBL_SASA_IP_HABEASDATA_C'] = 'IP Aceptación de Habeas Data';
	$mod_strings['LBL_SASA_ACEPTA_TERMINOS_C'] = 'Acepta Términos';
	
?>

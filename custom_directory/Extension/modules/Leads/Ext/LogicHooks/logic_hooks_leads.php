<?php
$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	4,

	//Label. A string value to identify the hook.
	'before_save',

	//The PHP file where your class is located.
	'custom/modules/Leads/logic_hooks_leads.php',

	//The class the method is in.
	'logic_hooks_leads',

	//The method to call.
	'before_save'
);

?>

<?php
 // created: 2023-03-06 03:28:30
$layout_defs["Leads"]["subpanel_setup"]['leads_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LEADS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'leads_cases_1',
);

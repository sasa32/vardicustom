<?php
 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_phone_mobile_c']['labelValue']='Teléfono Celular';
$dictionary['Lead']['fields']['sasa_phone_mobile_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['sasa_phone_mobile_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_phone_mobile_c']['dependency']='';

 ?>
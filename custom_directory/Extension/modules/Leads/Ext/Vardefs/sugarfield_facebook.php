<?php
 // created: 2020-05-16 20:24:58
$dictionary['Lead']['fields']['facebook']['massupdate']=false;
$dictionary['Lead']['fields']['facebook']['comments']='The facebook name of the user';
$dictionary['Lead']['fields']['facebook']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['facebook']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['facebook']['merge_filter']='disabled';
$dictionary['Lead']['fields']['facebook']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['facebook']['calculated']=false;
$dictionary['Lead']['fields']['facebook']['audited']=false;
$dictionary['Lead']['fields']['facebook']['pii']=false;

 ?>
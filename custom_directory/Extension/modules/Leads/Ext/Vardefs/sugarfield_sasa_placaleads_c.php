<?php
 // created: 2023-05-16 13:43:09
$dictionary['Lead']['fields']['sasa_placaleads_c']['labelValue']='Placa';
$dictionary['Lead']['fields']['sasa_placaleads_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_placaleads_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_placaleads_c']['dependency']='or(equal($sasa_tipo_solicitud_c,"5"),equal($sasa_unidad_de_negocio_c,"130"),equal($sasa_unidad_de_negocio_c,"210"),equal($sasa_tipo_solicitud_c,"1"),equal($sasa_unidad_de_negocio_c,"310"))';
$dictionary['Lead']['fields']['sasa_placaleads_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_placaleads_c']['readonly_formula']='';

 ?>
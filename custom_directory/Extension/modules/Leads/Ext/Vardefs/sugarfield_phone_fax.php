<?php
 // created: 2020-05-16 20:31:54
$dictionary['Lead']['fields']['phone_fax']['len']='100';
$dictionary['Lead']['fields']['phone_fax']['audited']=false;
$dictionary['Lead']['fields']['phone_fax']['massupdate']=false;
$dictionary['Lead']['fields']['phone_fax']['comments']='Contact fax number';
$dictionary['Lead']['fields']['phone_fax']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_fax']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_fax']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_fax']['reportable']=false;
$dictionary['Lead']['fields']['phone_fax']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.98',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_fax']['calculated']=false;
$dictionary['Lead']['fields']['phone_fax']['pii']=false;

 ?>
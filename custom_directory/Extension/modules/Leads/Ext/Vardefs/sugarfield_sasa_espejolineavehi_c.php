<?php
 // created: 2022-05-18 12:56:59
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['labelValue']='Espejo Linea de Vehículo';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['calculated']='1';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['formula']='related($sasa_vehiculos_leads_1,"name")';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['enforced']='1';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_espejolineavehi_c']['readonly_formula']='';

 ?>
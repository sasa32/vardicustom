<?php
 // created: 2020-05-11 19:41:32
$dictionary['Lead']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_state']['importable']='false';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_state']['calculated']=false;

 ?>
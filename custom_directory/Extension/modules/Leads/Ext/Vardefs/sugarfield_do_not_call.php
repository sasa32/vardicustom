<?php
 // created: 2020-05-11 18:00:07
$dictionary['Lead']['fields']['do_not_call']['default']=false;
$dictionary['Lead']['fields']['do_not_call']['massupdate']=false;
$dictionary['Lead']['fields']['do_not_call']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['do_not_call']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['do_not_call']['merge_filter']='disabled';
$dictionary['Lead']['fields']['do_not_call']['unified_search']=false;
$dictionary['Lead']['fields']['do_not_call']['calculated']=false;

 ?>
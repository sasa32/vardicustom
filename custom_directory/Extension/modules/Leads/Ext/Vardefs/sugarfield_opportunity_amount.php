<?php
 // created: 2020-05-16 21:12:04
$dictionary['Lead']['fields']['opportunity_amount']['audited']=false;
$dictionary['Lead']['fields']['opportunity_amount']['massupdate']=false;
$dictionary['Lead']['fields']['opportunity_amount']['comments']='Amount of the opportunity';
$dictionary['Lead']['fields']['opportunity_amount']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['opportunity_amount']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['opportunity_amount']['merge_filter']='disabled';
$dictionary['Lead']['fields']['opportunity_amount']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['opportunity_amount']['calculated']=false;
$dictionary['Lead']['fields']['opportunity_amount']['reportable']=false;

 ?>
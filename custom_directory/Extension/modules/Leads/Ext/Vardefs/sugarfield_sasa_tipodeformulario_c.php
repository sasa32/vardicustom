<?php
 // created: 2022-05-18 12:56:59
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['labelValue']='Nombre de Formulario';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['readonly']='1';
$dictionary['Lead']['fields']['sasa_tipodeformulario_c']['readonly_formula']='';

 ?>
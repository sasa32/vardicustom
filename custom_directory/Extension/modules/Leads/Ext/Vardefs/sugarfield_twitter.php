<?php
 // created: 2020-05-16 20:25:20
$dictionary['Lead']['fields']['twitter']['audited']=false;
$dictionary['Lead']['fields']['twitter']['massupdate']=false;
$dictionary['Lead']['fields']['twitter']['comments']='The twitter name of the user';
$dictionary['Lead']['fields']['twitter']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['twitter']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['twitter']['merge_filter']='disabled';
$dictionary['Lead']['fields']['twitter']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['twitter']['calculated']=false;
$dictionary['Lead']['fields']['twitter']['pii']=false;

 ?>
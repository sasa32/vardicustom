<?php
 // created: 2020-05-16 21:16:00
$dictionary['Lead']['fields']['mkto_id']['len']='11';
$dictionary['Lead']['fields']['mkto_id']['audited']=false;
$dictionary['Lead']['fields']['mkto_id']['massupdate']=false;
$dictionary['Lead']['fields']['mkto_id']['comments']='Associated Marketo Lead ID';
$dictionary['Lead']['fields']['mkto_id']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['mkto_id']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['mkto_id']['merge_filter']='disabled';
$dictionary['Lead']['fields']['mkto_id']['reportable']=false;
$dictionary['Lead']['fields']['mkto_id']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['mkto_id']['calculated']=false;
$dictionary['Lead']['fields']['mkto_id']['enable_range_search']=false;
$dictionary['Lead']['fields']['mkto_id']['min']=false;
$dictionary['Lead']['fields']['mkto_id']['max']=false;
$dictionary['Lead']['fields']['mkto_id']['disable_num_format']='';

 ?>
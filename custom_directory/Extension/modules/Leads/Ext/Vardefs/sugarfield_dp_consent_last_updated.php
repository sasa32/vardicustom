<?php
 // created: 2020-07-07 08:20:55
$dictionary['Lead']['fields']['dp_consent_last_updated']['audited']=false;
$dictionary['Lead']['fields']['dp_consent_last_updated']['massupdate']=true;
$dictionary['Lead']['fields']['dp_consent_last_updated']['comments']='Date consent last updated';
$dictionary['Lead']['fields']['dp_consent_last_updated']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['dp_consent_last_updated']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['dp_consent_last_updated']['merge_filter']='disabled';
$dictionary['Lead']['fields']['dp_consent_last_updated']['calculated']=false;
$dictionary['Lead']['fields']['dp_consent_last_updated']['enable_range_search']='1';

 ?>
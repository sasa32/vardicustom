<?php
/*
* Requerimiento en https://sasaconsultoria.sugarondemand.com/index.php#Tasks/5ea23328-7e7a-11ea-b3b6-02fb8f607ac4
*/
$dictionary["Lead"]["fields"]["primary_address_city"]['readonly'] = true;
$dictionary["Lead"]["fields"]["primary_address_state"]['readonly'] = true;
$dictionary["Lead"]["fields"]["primary_address_country"]['readonly'] = true;

$dictionary["Lead"]["fields"]["alt_address_city"]['readonly'] = true;
$dictionary["Lead"]["fields"]["alt_address_state"]['readonly'] = true;
$dictionary["Lead"]["fields"]["alt_address_country"]['readonly'] = true;

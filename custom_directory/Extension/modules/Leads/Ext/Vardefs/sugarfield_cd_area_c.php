<?php
 // created: 2022-05-18 12:56:59
$dictionary['Lead']['fields']['cd_area_c']['labelValue']='Subunidad de Negocio';
$dictionary['Lead']['fields']['cd_area_c']['dependency']='';
$dictionary['Lead']['fields']['cd_area_c']['required_formula']='';
$dictionary['Lead']['fields']['cd_area_c']['readonly_formula']='';
$dictionary['Lead']['fields']['cd_area_c']['visibility_grid']=array (
  'trigger' => 'sasa_unidad_de_negocio_c',
  'values' => 
  array (
    110 => 
    array (
      0 => '',
      1 => '11011',
    ),
    120 => 
    array (
      0 => '',
      1 => '12020',
    ),
    130 => 
    array (
      0 => '',
      1 => '13030',
    ),
    153 => 
    array (
      0 => '',
      1 => '15353',
    ),
    210 => 
    array (
      0 => '',
      1 => '21010',
      2 => '21012',
    ),
    220 => 
    array (
      0 => '',
      1 => '22020',
    ),
    310 => 
    array (
      0 => '',
      1 => '31011',
    ),
    315 => 
    array (
      0 => '',
      1 => '31515',
    ),
    340 => 
    array (
      0 => '',
      1 => '34044',
      2 => '34040',
      3 => '34041',
    ),
    '' => 
    array (
    ),
  ),
);

 ?>
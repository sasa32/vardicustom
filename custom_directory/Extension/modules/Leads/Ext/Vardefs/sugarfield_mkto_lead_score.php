<?php
 // created: 2020-05-16 21:16:48
$dictionary['Lead']['fields']['mkto_lead_score']['len']='11';
$dictionary['Lead']['fields']['mkto_lead_score']['audited']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['massupdate']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['mkto_lead_score']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['mkto_lead_score']['merge_filter']='disabled';
$dictionary['Lead']['fields']['mkto_lead_score']['reportable']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['mkto_lead_score']['calculated']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['enable_range_search']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['min']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['max']=false;
$dictionary['Lead']['fields']['mkto_lead_score']['disable_num_format']='';

 ?>
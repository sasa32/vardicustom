<?php
 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_numero_documento_c']['labelValue']='Número de Documento';
$dictionary['Lead']['fields']['sasa_numero_documento_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['sasa_numero_documento_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_numero_documento_c']['dependency']='equal($sasa_tipo_de_persona_c,"N")';

 ?>
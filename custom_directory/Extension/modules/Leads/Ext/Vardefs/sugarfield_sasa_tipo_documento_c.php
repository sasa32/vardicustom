<?php
 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_tipo_documento_c']['labelValue']='Tipo de Documento';
$dictionary['Lead']['fields']['sasa_tipo_documento_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_tipo_documento_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipo_de_persona_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'N' => 
    array (
      0 => '',
      1 => 'D',
      2 => 'C',
      3 => 'E',
      4 => 'U',
      5 => 'P',
      6 => 'T',
    ),
    'J' => 
    array (
      0 => 'N',
    ),
  ),
);

 ?>
<?php
 // created: 2023-05-19 14:01:39
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['labelValue']='Detalle de Origen';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['readonly_formula']='';
$dictionary['Lead']['fields']['sasa_detalle_origen_c']['visibility_grid']=array (
  'trigger' => 'lead_source',
  'values' => 
  array (
    1 => 
    array (
    ),
    2 => 
    array (
    ),
    3 => 
    array (
      0 => '',
      1 => '71',
    ),
    4 => 
    array (
      0 => '',
      1 => '1',
      2 => '3',
      3 => '2',
      4 => '4',
      5 => '5',
      6 => '73',
      7 => '74',
    ),
    5 => 
    array (
    ),
    6 => 
    array (
    ),
    8 => 
    array (
    ),
    9 => 
    array (
    ),
    10 => 
    array (
    ),
    11 => 
    array (
    ),
    13 => 
    array (
    ),
    14 => 
    array (
    ),
    15 => 
    array (
      0 => '',
      1 => '70',
    ),
    16 => 
    array (
    ),
    17 => 
    array (
    ),
    18 => 
    array (
      0 => '',
      1 => '18',
      2 => '57',
    ),
    19 => 
    array (
      0 => '',
      1 => '20',
      2 => '21',
    ),
    20 => 
    array (
      0 => '',
      1 => '89',
      2 => '23',
      3 => '22',
      4 => '79',
      5 => '69',
      6 => '67',
      7 => '68',
      8 => '25',
      9 => '24',
      10 => '76',
      11 => '77',
      12 => '78',
      13 => '87',
    ),
    21 => 
    array (
    ),
    22 => 
    array (
      0 => '',
      1 => '27',
      2 => '75',
      3 => '80',
    ),
    23 => 
    array (
      0 => '',
      1 => '28',
    ),
    24 => 
    array (
    ),
    25 => 
    array (
      0 => '',
      1 => '42',
      2 => '43',
      3 => '34',
      4 => '44',
    ),
    26 => 
    array (
    ),
    28 => 
    array (
    ),
    29 => 
    array (
      0 => '',
      1 => '31',
      2 => '32',
      3 => '52',
      4 => '46',
      5 => '14',
    ),
    30 => 
    array (
    ),
    31 => 
    array (
    ),
    32 => 
    array (
    ),
    33 => 
    array (
    ),
    34 => 
    array (
      0 => '',
      1 => '39',
    ),
    35 => 
    array (
    ),
    36 => 
    array (
      0 => '',
      1 => '40',
      2 => '51',
    ),
    37 => 
    array (
      0 => '',
      1 => '58',
      2 => '64',
      3 => '46',
    ),
    38 => 
    array (
      0 => '',
      1 => '45',
      2 => '72',
      3 => '88',
      4 => '46',
      5 => '50',
    ),
    39 => 
    array (
      0 => '',
      1 => '54',
      2 => '56',
      3 => '59',
      4 => '60',
      5 => '55',
    ),
    40 => 
    array (
    ),
    41 => 
    array (
      0 => '',
      1 => '61',
      2 => '62',
    ),
    42 => 
    array (
      0 => '',
      1 => '63',
      2 => '64',
    ),
    43 => 
    array (
      0 => '',
      1 => '65',
      2 => '66',
    ),
    '' => 
    array (
    ),
  ),
);

 ?>
<?php
 // created: 2020-05-16 21:13:32
$dictionary['Lead']['fields']['portal_app']['audited']=false;
$dictionary['Lead']['fields']['portal_app']['massupdate']=false;
$dictionary['Lead']['fields']['portal_app']['comments']='Portal application that resulted in created of lead';
$dictionary['Lead']['fields']['portal_app']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['portal_app']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['portal_app']['merge_filter']='disabled';
$dictionary['Lead']['fields']['portal_app']['reportable']=false;
$dictionary['Lead']['fields']['portal_app']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['portal_app']['calculated']=false;

 ?>
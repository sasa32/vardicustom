<?php
// created: 2020-05-15 15:41:37
$dictionary["Lead"]["fields"]["sasa_paises_leads_1"] = array (
  'name' => 'sasa_paises_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_leads_1',
  'source' => 'non-db',
  'module' => 'sasa_Paises',
  'bean_name' => 'sasa_Paises',
  'side' => 'right',
  'vname' => 'LBL_SASA_PAISES_LEADS_1_FROM_LEADS_TITLE',
  'id_name' => 'sasa_paises_leads_1sasa_paises_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_paises_leads_1_name"] = array (
  'name' => 'sasa_paises_leads_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_LEADS_1_FROM_SASA_PAISES_TITLE',
  'save' => true,
  'id_name' => 'sasa_paises_leads_1sasa_paises_ida',
  'link' => 'sasa_paises_leads_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_paises_leads_1sasa_paises_ida"] = array (
  'name' => 'sasa_paises_leads_1sasa_paises_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_LEADS_1_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_paises_leads_1sasa_paises_ida',
  'link' => 'sasa_paises_leads_1',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

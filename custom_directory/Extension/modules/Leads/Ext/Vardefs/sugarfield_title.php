<?php
 // created: 2020-05-11 17:59:24
$dictionary['Lead']['fields']['title']['audited']=false;
$dictionary['Lead']['fields']['title']['massupdate']=false;
$dictionary['Lead']['fields']['title']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['title']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['title']['merge_filter']='disabled';
$dictionary['Lead']['fields']['title']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['title']['calculated']=false;
$dictionary['Lead']['fields']['title']['pii']=false;

 ?>
<?php
 // created: 2020-07-07 08:07:23
$dictionary['Lead']['fields']['primary_address_postalcode']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_postalcode']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['audited']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Lead']['fields']['primary_address_postalcode']['pii']=false;

 ?>
<?php
 // created: 2020-07-07 08:35:55
$dictionary['Lead']['fields']['last_name']['massupdate']=false;
$dictionary['Lead']['fields']['last_name']['importable']='false';
$dictionary['Lead']['fields']['last_name']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['last_name']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.85',
  'searchable' => true,
);
$dictionary['Lead']['fields']['last_name']['calculated']='1';
$dictionary['Lead']['fields']['last_name']['formula']='concat($sasa_primerapellido_c," ",$sasa_last_name_2_c)';
$dictionary['Lead']['fields']['last_name']['enforced']=true;
$dictionary['Lead']['fields']['last_name']['pii']=false;
$dictionary['Lead']['fields']['last_name']['audited']=false;
$dictionary['Lead']['fields']['last_name']['comments']='Last name of the contact';

 ?>
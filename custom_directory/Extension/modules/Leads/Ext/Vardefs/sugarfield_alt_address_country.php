<?php
 // created: 2020-07-07 08:10:01
$dictionary['Lead']['fields']['alt_address_country']['len']='255';
$dictionary['Lead']['fields']['alt_address_country']['audited']=false;
$dictionary['Lead']['fields']['alt_address_country']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_country']['comments']='Country for alternate address';
$dictionary['Lead']['fields']['alt_address_country']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_country']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_country']['pii']=false;

 ?>
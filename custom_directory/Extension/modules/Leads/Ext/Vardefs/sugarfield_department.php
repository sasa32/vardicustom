<?php
 // created: 2020-05-11 17:58:24
$dictionary['Lead']['fields']['department']['audited']=false;
$dictionary['Lead']['fields']['department']['massupdate']=false;
$dictionary['Lead']['fields']['department']['importable']='false';
$dictionary['Lead']['fields']['department']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['department']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['department']['merge_filter']='disabled';
$dictionary['Lead']['fields']['department']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['department']['calculated']=false;

 ?>
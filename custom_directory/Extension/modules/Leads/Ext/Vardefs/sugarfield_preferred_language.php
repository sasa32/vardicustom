<?php
 // created: 2020-05-16 21:14:29
$dictionary['Lead']['fields']['preferred_language']['default']='en_us';
$dictionary['Lead']['fields']['preferred_language']['audited']=false;
$dictionary['Lead']['fields']['preferred_language']['massupdate']=true;
$dictionary['Lead']['fields']['preferred_language']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['preferred_language']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['preferred_language']['merge_filter']='disabled';
$dictionary['Lead']['fields']['preferred_language']['reportable']=false;
$dictionary['Lead']['fields']['preferred_language']['calculated']=false;
$dictionary['Lead']['fields']['preferred_language']['dependency']=false;

 ?>
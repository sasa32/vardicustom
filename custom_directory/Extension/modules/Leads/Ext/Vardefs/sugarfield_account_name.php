<?php
 // created: 2020-05-20 19:01:32
$dictionary['Lead']['fields']['account_name']['audited']=true;
$dictionary['Lead']['fields']['account_name']['massupdate']=false;
$dictionary['Lead']['fields']['account_name']['importable']='false';
$dictionary['Lead']['fields']['account_name']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['account_name']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['account_name']['calculated']=false;
$dictionary['Lead']['fields']['account_name']['comments']='Account name for lead';
$dictionary['Lead']['fields']['account_name']['dependency']='equal($sasa_tipo_documento_c,"N")';
<?php
 // created: 2022-05-18 12:56:58
$dictionary['Lead']['fields']['sasa_num_docu_juridica_c']['labelValue']='Número de Documento';
$dictionary['Lead']['fields']['sasa_num_docu_juridica_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_num_docu_juridica_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_num_docu_juridica_c']['dependency']='equal($sasa_tipo_de_persona_c,"J")';

 ?>
<?php
 // created: 2020-05-16 20:30:48
$dictionary['Lead']['fields']['phone_work']['len']='100';
$dictionary['Lead']['fields']['phone_work']['audited']=false;
$dictionary['Lead']['fields']['phone_work']['massupdate']=false;
$dictionary['Lead']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_work']['reportable']=false;
$dictionary['Lead']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_work']['calculated']=false;
$dictionary['Lead']['fields']['phone_work']['pii']=false;

 ?>
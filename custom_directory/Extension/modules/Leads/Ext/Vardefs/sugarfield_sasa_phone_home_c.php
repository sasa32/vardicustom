<?php
 // created: 2022-05-18 12:56:57
$dictionary['Lead']['fields']['sasa_phone_home_c']['labelValue']='Teléfono fijo';
$dictionary['Lead']['fields']['sasa_phone_home_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['sasa_phone_home_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_phone_home_c']['dependency']='';

 ?>
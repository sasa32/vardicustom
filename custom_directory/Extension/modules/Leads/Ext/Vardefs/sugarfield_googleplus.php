<?php
 // created: 2020-05-16 20:25:40
$dictionary['Lead']['fields']['googleplus']['audited']=false;
$dictionary['Lead']['fields']['googleplus']['massupdate']=false;
$dictionary['Lead']['fields']['googleplus']['comments']='The google plus id of the user';
$dictionary['Lead']['fields']['googleplus']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['googleplus']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['googleplus']['merge_filter']='disabled';
$dictionary['Lead']['fields']['googleplus']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['googleplus']['calculated']=false;
$dictionary['Lead']['fields']['googleplus']['pii']=false;

 ?>
<?php
 // created: 2020-07-07 08:09:29
$dictionary['Lead']['fields']['alt_address_city']['audited']=false;
$dictionary['Lead']['fields']['alt_address_city']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_city']['comments']='City for alternate address';
$dictionary['Lead']['fields']['alt_address_city']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_city']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_city']['pii']=false;

 ?>
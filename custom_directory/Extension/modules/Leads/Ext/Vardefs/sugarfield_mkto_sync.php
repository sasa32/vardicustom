<?php
 // created: 2020-05-16 21:15:14
$dictionary['Lead']['fields']['mkto_sync']['default']=false;
$dictionary['Lead']['fields']['mkto_sync']['audited']=false;
$dictionary['Lead']['fields']['mkto_sync']['massupdate']=false;
$dictionary['Lead']['fields']['mkto_sync']['comments']='Should the Lead be synced to Marketo';
$dictionary['Lead']['fields']['mkto_sync']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['mkto_sync']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['mkto_sync']['merge_filter']='disabled';
$dictionary['Lead']['fields']['mkto_sync']['reportable']=false;
$dictionary['Lead']['fields']['mkto_sync']['unified_search']=false;
$dictionary['Lead']['fields']['mkto_sync']['calculated']=false;

 ?>
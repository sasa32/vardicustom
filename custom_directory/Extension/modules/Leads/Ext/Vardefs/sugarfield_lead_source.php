<?php
 // created: 2021-05-13 14:25:12
$dictionary['Lead']['fields']['lead_source']['len']=100;
$dictionary['Lead']['fields']['lead_source']['massupdate']=true;
$dictionary['Lead']['fields']['lead_source']['options']='lead_source_list';
$dictionary['Lead']['fields']['lead_source']['comments']='';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Lead']['fields']['lead_source']['calculated']=false;
$dictionary['Lead']['fields']['lead_source']['dependency']=false;
$dictionary['Lead']['fields']['lead_source']['required']=true;
$dictionary['Lead']['fields']['lead_source']['default']='0';
$dictionary['Lead']['fields']['lead_source']['hidemassupdate']=false;

 ?>
<?php
 // created: 2020-06-17 15:02:40
$dictionary['Lead']['fields']['phone_home']['len']='255';
$dictionary['Lead']['fields']['phone_home']['massupdate']=false;
$dictionary['Lead']['fields']['phone_home']['comments']='';
$dictionary['Lead']['fields']['phone_home']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['phone_home']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_home']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.02',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_home']['calculated']='1';
$dictionary['Lead']['fields']['phone_home']['pii']=false;
$dictionary['Lead']['fields']['phone_home']['audited']=false;
$dictionary['Lead']['fields']['phone_home']['importable']='false';
$dictionary['Lead']['fields']['phone_home']['formula']='concat(related($sasa_municipios_leads_1,"sasa_indicativo_c"),$sasa_phone_home_c)';
$dictionary['Lead']['fields']['phone_home']['enforced']=true;

 ?>
<?php
 // created: 2020-07-07 08:35:42
$dictionary['Lead']['fields']['first_name']['massupdate']=false;
$dictionary['Lead']['fields']['first_name']['comments']='';
$dictionary['Lead']['fields']['first_name']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['first_name']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['first_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['first_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.87',
  'searchable' => true,
);
$dictionary['Lead']['fields']['first_name']['calculated']='1';
$dictionary['Lead']['fields']['first_name']['required']=true;
$dictionary['Lead']['fields']['first_name']['importable']='false';
$dictionary['Lead']['fields']['first_name']['formula']='$sasa_nombres_c';
$dictionary['Lead']['fields']['first_name']['enforced']=true;
$dictionary['Lead']['fields']['first_name']['pii']=false;
$dictionary['Lead']['fields']['first_name']['audited']=false;

 ?>
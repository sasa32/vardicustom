<?php
// created: 2020-05-15 15:45:19
$dictionary["Lead"]["fields"]["sasa_municipios_leads_1"] = array (
  'name' => 'sasa_municipios_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_leads_1',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE',
  'id_name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_municipios_leads_1_name"] = array (
  'name' => 'sasa_municipios_leads_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'link' => 'sasa_municipios_leads_1',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_municipios_leads_1sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_LEADS_1_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_municipios_leads_1sasa_municipios_ida',
  'link' => 'sasa_municipios_leads_1',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

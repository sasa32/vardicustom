<?php
 // created: 2020-05-16 20:31:06
$dictionary['Lead']['fields']['phone_other']['len']='100';
$dictionary['Lead']['fields']['phone_other']['audited']=false;
$dictionary['Lead']['fields']['phone_other']['massupdate']=false;
$dictionary['Lead']['fields']['phone_other']['comments']='Other phone number for the contact';
$dictionary['Lead']['fields']['phone_other']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_other']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_other']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_other']['reportable']=false;
$dictionary['Lead']['fields']['phone_other']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.99',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_other']['calculated']=false;
$dictionary['Lead']['fields']['phone_other']['pii']=false;

 ?>
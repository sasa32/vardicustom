<?php
 // created: 2020-07-07 08:20:40
$dictionary['Lead']['fields']['dp_business_purpose']['len']=NULL;
$dictionary['Lead']['fields']['dp_business_purpose']['audited']=false;
$dictionary['Lead']['fields']['dp_business_purpose']['massupdate']=true;
$dictionary['Lead']['fields']['dp_business_purpose']['comments']='Business purposes consented for';
$dictionary['Lead']['fields']['dp_business_purpose']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['dp_business_purpose']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['dp_business_purpose']['merge_filter']='disabled';
$dictionary['Lead']['fields']['dp_business_purpose']['calculated']=false;
$dictionary['Lead']['fields']['dp_business_purpose']['dependency']='';

 ?>
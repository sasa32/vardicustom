<?php
 // created: 2020-05-09 12:06:05
$dictionary['Lead']['fields']['sasa_municipio_secundario_2__c']['labelValue']='Municipio Alternativo 2';
$dictionary['Lead']['fields']['sasa_municipio_secundario_2__c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_municipio_secundario_2__c']['enforced']='';
$dictionary['Lead']['fields']['sasa_municipio_secundario_2__c']['dependency']='';

 ?>
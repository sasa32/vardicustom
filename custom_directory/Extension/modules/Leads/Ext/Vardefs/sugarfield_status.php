<?php
 // created: 2021-12-06 02:59:58
$dictionary['Lead']['fields']['status']['default']='A';
$dictionary['Lead']['fields']['status']['len']=100;
$dictionary['Lead']['fields']['status']['massupdate']=true;
$dictionary['Lead']['fields']['status']['hidemassupdate']=false;
$dictionary['Lead']['fields']['status']['options']='status_list';
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status']['calculated']=false;
$dictionary['Lead']['fields']['status']['dependency']=false;

 ?>
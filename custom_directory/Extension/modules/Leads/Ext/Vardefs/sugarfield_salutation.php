<?php
 // created: 2020-05-09 13:19:55
$dictionary['Lead']['fields']['salutation']['len']=100;
$dictionary['Lead']['fields']['salutation']['audited']=false;
$dictionary['Lead']['fields']['salutation']['massupdate']=true;
$dictionary['Lead']['fields']['salutation']['comments']='';
$dictionary['Lead']['fields']['salutation']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['salutation']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['salutation']['merge_filter']='disabled';
$dictionary['Lead']['fields']['salutation']['calculated']=false;
$dictionary['Lead']['fields']['salutation']['dependency']=false;
$dictionary['Lead']['fields']['salutation']['pii']=false;
$dictionary['Lead']['fields']['salutation']['options']='salutation_list';

 ?>
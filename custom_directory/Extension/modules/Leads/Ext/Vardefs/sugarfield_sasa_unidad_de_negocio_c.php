<?php
 // created: 2022-05-18 12:56:58
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['labelValue']='Unidad de Negocio';
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['readonly_formula']='';
$dictionary['Lead']['fields']['sasa_unidad_de_negocio_c']['visibility_grid']=array (
  'trigger' => 'sasa_compania_c',
  'values' => 
  array (
    1 => 
    array (
      0 => '',
      1 => '110',
      2 => '120',
      3 => '130',
      4 => '153',
    ),
    2 => 
    array (
      0 => '',
      1 => '210',
      2 => '220',
    ),
    3 => 
    array (
      0 => '',
      1 => '310',
      2 => '315',
      3 => '340',
    ),
    '' => 
    array (
    ),
  ),
);

 ?>
<?php
 // created: 2020-07-07 08:09:18
$dictionary['Lead']['fields']['alt_address_street']['audited']=false;
$dictionary['Lead']['fields']['alt_address_street']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Lead']['fields']['alt_address_street']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_street']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.3',
  'searchable' => true,
);
$dictionary['Lead']['fields']['alt_address_street']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_street']['pii']=false;
$dictionary['Lead']['fields']['alt_address_street']['rows']='4';
$dictionary['Lead']['fields']['alt_address_street']['cols']='20';

 ?>
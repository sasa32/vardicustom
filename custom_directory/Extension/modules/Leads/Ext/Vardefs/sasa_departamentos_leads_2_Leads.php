<?php
// created: 2020-05-15 16:07:49
$dictionary["Lead"]["fields"]["sasa_departamentos_leads_2"] = array (
  'name' => 'sasa_departamentos_leads_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_leads_2',
  'source' => 'non-db',
  'module' => 'sasa_Departamentos',
  'bean_name' => 'sasa_Departamentos',
  'side' => 'right',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_LEADS_TITLE',
  'id_name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'link-type' => 'one',
);
$dictionary["Lead"]["fields"]["sasa_departamentos_leads_2_name"] = array (
  'name' => 'sasa_departamentos_leads_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_leads_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["sasa_departamentos_leads_2sasa_departamentos_ida"] = array (
  'name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_2_FROM_LEADS_TITLE_ID',
  'id_name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
  'link' => 'sasa_departamentos_leads_2',
  'table' => 'sasa_departamentos',
  'module' => 'sasa_Departamentos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

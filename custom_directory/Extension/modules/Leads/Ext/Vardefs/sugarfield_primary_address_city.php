<?php
 // created: 2020-05-11 19:40:45
$dictionary['Lead']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_city']['importable']='false';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_city']['calculated']=false;

 ?>
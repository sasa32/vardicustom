<?php
 // created: 2020-07-07 08:09:52
$dictionary['Lead']['fields']['alt_address_postalcode']['audited']=false;
$dictionary['Lead']['fields']['alt_address_postalcode']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Lead']['fields']['alt_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_postalcode']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_postalcode']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_postalcode']['pii']=false;

 ?>
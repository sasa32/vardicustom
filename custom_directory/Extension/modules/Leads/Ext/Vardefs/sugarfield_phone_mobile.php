<?php
 // created: 2020-06-17 15:01:58
$dictionary['Lead']['fields']['phone_mobile']['len']='255';
$dictionary['Lead']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Lead']['fields']['phone_mobile']['comments']='';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.01',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_mobile']['calculated']='1';
$dictionary['Lead']['fields']['phone_mobile']['audited']=false;
$dictionary['Lead']['fields']['phone_mobile']['pii']=false;
$dictionary['Lead']['fields']['phone_mobile']['importable']='false';
$dictionary['Lead']['fields']['phone_mobile']['formula']='concat("+57",$sasa_phone_mobile_c)';
$dictionary['Lead']['fields']['phone_mobile']['enforced']=true;

 ?>
<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_QUOTES_BILLTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_QUOTES_SUBPANEL_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_QUOTES_SHIPTO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes y Prospectos';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nombre de Cliente y Prospecto:';
$mod_strings['LBL_ACCOUNT_ID'] = 'Id Cliente y Prospecto';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Nombre de la Cliente y Prospecto';
$mod_strings['EXCEPTION_QUOTE_ALREADY_CONVERTED'] = 'Presupuesto ya convertido en Cotización';

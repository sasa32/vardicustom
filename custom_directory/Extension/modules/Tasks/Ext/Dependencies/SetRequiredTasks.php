<?php
$dependencies['Tasks']['SetRequiredTasks'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('status'),
	//'trigger' => 'equal($sasa_auto_contactacion_c,1)',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'description',
				'value' => 'or(equal($status,"Deferred"),equal($status,"Completed"),equal($status,"Pending Input"))',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetRequired',
			'params' => array(
				'target' => 'description',
				'value' => 'false'
			)
		),
	)
);

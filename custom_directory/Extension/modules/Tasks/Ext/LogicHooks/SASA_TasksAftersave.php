<?php
$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'SASA_TasksAftersave',

	//The PHP file where your class is located.
	'custom/modules/Tasks/SASA_TasksAftersave.php',

	//The class the method is in.
	'SASA_TasksAftersave',

	//The method to call.
	'after_save'
);

?>

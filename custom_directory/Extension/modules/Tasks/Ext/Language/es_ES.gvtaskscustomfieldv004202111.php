<?php
$mod_strings['LBL_SASA_TIPO_DE_TAREA_C'] = 'Tipo de tarea';
$mod_strings['LBL_SASA_RESPUESTA_C'] = 'Respuesta';
$mod_strings['LBL_DESCRIPTION'] = 'Documentar Gestión';
$mod_strings['LBL_PARENT_NAME'] = 'Tipo de Registro Principal';
$mod_strings['LBL_SASA_CONTROLPROCESO_C'] = 'Control Proceso';
$mod_strings['LBL_SASA_FECHAINICIOGESTION_C'] = 'Fecha Inicio de Gestión';
$mod_strings['LBL_SASA_ESTADOCONTACTO_C'] = 'Estado del Contacto';
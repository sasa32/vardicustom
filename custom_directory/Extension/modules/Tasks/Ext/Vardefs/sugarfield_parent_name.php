<?php
 // created: 2020-09-11 17:11:58
$dictionary['Task']['fields']['parent_name']['audited']=false;
$dictionary['Task']['fields']['parent_name']['massupdate']=false;
$dictionary['Task']['fields']['parent_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['parent_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['parent_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['parent_name']['unified_search']=false;
$dictionary['Task']['fields']['parent_name']['calculated']=false;
$dictionary['Task']['fields']['parent_name']['related_fields']=array (
);

 ?>
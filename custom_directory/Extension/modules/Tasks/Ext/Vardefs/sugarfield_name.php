<?php
 // created: 2020-09-11 15:02:03
$dictionary['Task']['fields']['name']['audited']=true;
$dictionary['Task']['fields']['name']['massupdate']=false;
$dictionary['Task']['fields']['name']['hidemassupdate']=false;
$dictionary['Task']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['name']['merge_filter']='disabled';
$dictionary['Task']['fields']['name']['unified_search']=false;
$dictionary['Task']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.45',
  'searchable' => true,
);
$dictionary['Task']['fields']['name']['calculated']=false;

 ?>
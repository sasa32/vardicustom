<?php
 // created: 2020-09-14 16:04:39
$dictionary['Task']['fields']['status']['audited']=true;
$dictionary['Task']['fields']['status']['massupdate']=true;
$dictionary['Task']['fields']['status']['hidemassupdate']=false;
$dictionary['Task']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['status']['merge_filter']='disabled';
$dictionary['Task']['fields']['status']['unified_search']=false;
$dictionary['Task']['fields']['status']['calculated']=false;
$dictionary['Task']['fields']['status']['dependency']=false;
$dictionary['Task']['fields']['status']['full_text_search']=array (
);

 ?>
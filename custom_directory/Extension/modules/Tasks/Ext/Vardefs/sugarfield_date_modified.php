<?php
 // created: 2020-09-11 15:06:22
$dictionary['Task']['fields']['date_modified']['audited']=true;
$dictionary['Task']['fields']['date_modified']['hidemassupdate']=false;
$dictionary['Task']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Task']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_modified']['unified_search']=false;
$dictionary['Task']['fields']['date_modified']['calculated']=false;
$dictionary['Task']['fields']['date_modified']['enable_range_search']='1';

 ?>
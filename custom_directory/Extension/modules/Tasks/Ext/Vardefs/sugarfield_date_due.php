<?php
 // created: 2020-09-11 17:07:55
$dictionary['Task']['fields']['date_due']['audited']=true;
$dictionary['Task']['fields']['date_due']['massupdate']=true;
$dictionary['Task']['fields']['date_due']['hidemassupdate']=false;
$dictionary['Task']['fields']['date_due']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_due']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['date_due']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_due']['unified_search']=false;
$dictionary['Task']['fields']['date_due']['calculated']=false;
$dictionary['Task']['fields']['date_due']['enable_range_search']='1';
$dictionary['Task']['fields']['date_due']['full_text_search']=array (
);

 ?>
<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Unidad de negocio';
$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE'] = 'Unidades de Neg. por Clientes y Prospectos';
$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE'] = 'Unidades de Neg. por Clientes y Prospectos';
$mod_strings['LNK_LIST'] = 'Vista Unidades de Negocio';
$mod_strings['LBL_MODULE_NAME'] = 'Unidades de Negocio';
$mod_strings['LNK_IMPORT_SASA_UNIDAD_DE_NEGOCIO'] = 'Importar Unidades de Negocio';
$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_SUBPANEL_TITLE'] = 'Unidades de Negocio';
$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_FOCUS_DRAWER_DASHBOARD'] = 'Unidades de Negocio Panel de enfoque';
$mod_strings['LBL_SASA_UNIDAD_DE_NEGOCIO_RECORD_DASHBOARD'] = 'Unidades de Negocio Cuadro de mando del registro';
$mod_strings['LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE'] = 'Compañías';

<?php
 // created: 2020-05-21 16:23:40
$layout_defs["sasa_Unidad_de_Negocio"]["subpanel_setup"]['sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1'] = array (
  'order' => 100,
  'module' => 'SASA_UnidadNegClienteProspect',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDADNEGCLIENTEPROSPECT_TITLE',
  'get_subpanel_data' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

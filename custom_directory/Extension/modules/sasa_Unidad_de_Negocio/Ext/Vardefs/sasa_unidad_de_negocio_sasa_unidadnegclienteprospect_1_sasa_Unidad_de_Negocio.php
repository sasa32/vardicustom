<?php
// created: 2020-05-21 16:23:40
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'SASA_UnidadNegClienteProspect',
  'bean_name' => 'SASA_UnidadNegClienteProspect',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa_unida3a6anegocio_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
 // created: 2020-05-16 20:57:29
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['len']='255';
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['audited']=false;
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['massupdate']=false;
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['unified_search']=false;
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_Unidad_de_Negocio']['fields']['name']['calculated']=false;

 ?>
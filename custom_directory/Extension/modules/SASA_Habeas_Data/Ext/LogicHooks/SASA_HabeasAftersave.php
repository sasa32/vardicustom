<?php
$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'SASA_HabeasAftersave',

	//The PHP file where your class is located.
	'custom/modules/SASA_Habeas_Data/SASA_HabeasAftersave.php',

	//The class the method is in.
	'SASA_HabeasAftersave',

	//The method to call.
	'after_save'
);

?>

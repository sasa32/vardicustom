<?php
 // created: 2021-07-07 20:20:15
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['name']='documents_sasa_habeas_data_1documents_ida';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['type']='id';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['source']='non-db';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['vname']='LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE_ID';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['id_name']='documents_sasa_habeas_data_1documents_ida';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['link']='documents_sasa_habeas_data_1';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['table']='documents';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['module']='Documents';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['rname']='id';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['reportable']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['side']='right';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['massupdate']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['duplicate_merge']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['hideacl']=true;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1documents_ida']['audited']=false;

 ?>
<?php
 // created: 2020-07-28 14:37:29
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['audited']=true;
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['unified_search']=false;
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['calculated']=false;
$dictionary['SASA_Habeas_Data']['fields']['date_modified']['enable_range_search']='1';

 ?>
<?php
// created: 2020-07-29 16:58:08
$dictionary["SASA_Habeas_Data"]["fields"]["documents_sasa_habeas_data_1"] = array (
  'name' => 'documents_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'documents_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'id_name' => 'documents_sasa_habeas_data_1documents_ida',
  'link-type' => 'one',
);
$dictionary["SASA_Habeas_Data"]["fields"]["documents_sasa_habeas_data_1_name"] = array (
  'name' => 'documents_sasa_habeas_data_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_DOCUMENTS_TITLE',
  'save' => true,
  'id_name' => 'documents_sasa_habeas_data_1documents_ida',
  'link' => 'documents_sasa_habeas_data_1',
  'table' => 'documents',
  'module' => 'Documents',
  'rname' => 'document_name',
);
$dictionary["SASA_Habeas_Data"]["fields"]["documents_sasa_habeas_data_1documents_ida"] = array (
  'name' => 'documents_sasa_habeas_data_1documents_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_DOCUMENTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE_ID',
  'id_name' => 'documents_sasa_habeas_data_1documents_ida',
  'link' => 'documents_sasa_habeas_data_1',
  'table' => 'documents',
  'module' => 'Documents',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

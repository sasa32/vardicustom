<?php
// created: 2020-07-29 18:44:33
$dictionary["SASA_Habeas_Data"]["fields"]["sasa_companias_sasa_habeas_data_1"] = array (
  'name' => 'sasa_companias_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'sasa_companias_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'sasa_Companias',
  'bean_name' => 'sasa_Companias',
  'side' => 'right',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'id_name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'link-type' => 'one',
);
$dictionary["SASA_Habeas_Data"]["fields"]["sasa_companias_sasa_habeas_data_1_name"] = array (
  'name' => 'sasa_companias_sasa_habeas_data_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_COMPANIAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'link' => 'sasa_companias_sasa_habeas_data_1',
  'table' => 'sasa_companias',
  'module' => 'sasa_Companias',
  'rname' => 'name',
);
$dictionary["SASA_Habeas_Data"]["fields"]["sasa_companias_sasa_habeas_data_1sasa_companias_ida"] = array (
  'name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE_ID',
  'id_name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'link' => 'sasa_companias_sasa_habeas_data_1',
  'table' => 'sasa_companias',
  'module' => 'sasa_Companias',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

<?php
 // created: 2020-07-29 18:46:37
$dictionary['SASA_Habeas_Data']['fields']['name']['len']='255';
$dictionary['SASA_Habeas_Data']['fields']['name']['audited']=true;
$dictionary['SASA_Habeas_Data']['fields']['name']['massupdate']=false;
$dictionary['SASA_Habeas_Data']['fields']['name']['unified_search']=false;
$dictionary['SASA_Habeas_Data']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['SASA_Habeas_Data']['fields']['name']['calculated']='1';
$dictionary['SASA_Habeas_Data']['fields']['name']['importable']='false';
$dictionary['SASA_Habeas_Data']['fields']['name']['duplicate_merge']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['SASA_Habeas_Data']['fields']['name']['merge_filter']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['name']['formula']='concat(related($documents_sasa_habeas_data_1,"name")," -",related($leads_sasa_habeas_data_1,"name"),related($contacts_sasa_habeas_data_1,"name")," - ",toString($date_entered))';
$dictionary['SASA_Habeas_Data']['fields']['name']['enforced']=true;

 ?>
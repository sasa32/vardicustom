<?php
// created: 2020-07-30 11:16:31
$dictionary["SASA_Habeas_Data"]["fields"]["contacts_sasa_habeas_data_1"] = array (
  'name' => 'contacts_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'contacts_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'id_name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["SASA_Habeas_Data"]["fields"]["contacts_sasa_habeas_data_1_name"] = array (
  'name' => 'contacts_sasa_habeas_data_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'link' => 'contacts_sasa_habeas_data_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["SASA_Habeas_Data"]["fields"]["contacts_sasa_habeas_data_1contacts_ida"] = array (
  'name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE_ID',
  'id_name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'link' => 'contacts_sasa_habeas_data_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

<?php
 // created: 2021-07-07 20:20:15
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['audited']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['hidemassupdate']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['duplicate_merge']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['duplicate_merge_dom_value']=0;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['merge_filter']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['calculated']=false;
$dictionary['SASA_Habeas_Data']['fields']['documents_sasa_habeas_data_1_name']['vname']='LBL_DOCUMENTS_SASA_HABEAS_DATA_1_NAME_FIELD_TITLE';

 ?>
<?php
 // created: 2020-07-28 14:38:26
$dictionary['SASA_Habeas_Data']['fields']['description']['audited']=true;
$dictionary['SASA_Habeas_Data']['fields']['description']['required']=true;
$dictionary['SASA_Habeas_Data']['fields']['description']['massupdate']=false;
$dictionary['SASA_Habeas_Data']['fields']['description']['comments']='Full text of the note';
$dictionary['SASA_Habeas_Data']['fields']['description']['duplicate_merge']='enabled';
$dictionary['SASA_Habeas_Data']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['SASA_Habeas_Data']['fields']['description']['merge_filter']='disabled';
$dictionary['SASA_Habeas_Data']['fields']['description']['unified_search']=false;
$dictionary['SASA_Habeas_Data']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['SASA_Habeas_Data']['fields']['description']['calculated']=false;
$dictionary['SASA_Habeas_Data']['fields']['description']['rows']='6';
$dictionary['SASA_Habeas_Data']['fields']['description']['cols']='80';

 ?>
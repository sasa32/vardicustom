<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['SASA_Habeas_Data']['dependenciesCanalesAutorizados'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('sasa_auto_contactacion_c'),
	//'trigger' => 'equal($sasa_auto_contactacion_c,1)',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => '
					or(equal($sasa_auto_contactacion_c,1),equal($sasa_auto_contactacion_c,2),equal($sasa_auto_contactacion_c,0))
				',
			),
		),
		array(
			'name' => 'ReadOnly',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => 'or(equal($sasa_auto_contactacion_c,1),equal($sasa_auto_contactacion_c,0))',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => '
					equal($sasa_auto_contactacion_c,2)
				',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => 'false'
			)
		),
		array(
			'name' => 'ReadOnly',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => 'false',
			),
		),
		array(
			'name' => 'SetRequired',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_canales_autorizados_c',
				'value' => 'false',
			),
		),
	)
);

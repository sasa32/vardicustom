<?php
//requerimiento en https://sasaconsultoria.sugarondemand.com/#Tasks/b483ec22-5428-11ea-96c2-02dfd714a754
$dependencies['SASA_Habeas_Data']['dependenciesSetVisibilityContactsLead'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('contacts_sasa_habeas_data_1_name','leads_sasa_habeas_data_1_name'),
	//'trigger' => 'equal($sasa_auto_contactacion_c,1)',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'contacts_sasa_habeas_data_1_name',
				'value' => 'equal($leads_sasa_habeas_data_1_name,"")',
			),
		),
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'leads_sasa_habeas_data_1_name',
				'value' => 'equal($contacts_sasa_habeas_data_1_name,"")',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'contacts_sasa_habeas_data_1_name',
				'value' => 'false'
			)
		),
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'leads_sasa_habeas_data_1_name',
				'value' => 'false'
			)
		),
	)
);

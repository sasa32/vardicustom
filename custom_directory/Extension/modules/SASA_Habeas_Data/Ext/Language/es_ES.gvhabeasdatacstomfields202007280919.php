<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DESCRIPTION'] = 'Observaciones';
$mod_strings['LBL_SASA_ESTADO_AUTORIZACION_C'] = 'Estado de la Autorización';
$mod_strings['LBL_SASA_CANAL_PREFERENCIA_C'] = 'Canal de Preferencia';
$mod_strings['LBL_SASA_CANALES_AUTORIZADOS_C'] = 'Canales Autorizados';
$mod_strings['LBL_SASA_FUENTE_AUTORIZACION_C'] = 'Fuente de la Autorización';
$mod_strings['LBL_SASA_SOPORTE_AUTORIZACION_C'] = 'Soporte de la Autorización';
$mod_strings['LBL_SASA_REVISION_C'] = 'Revisión';

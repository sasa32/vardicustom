<?php

$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';
$mod_strings['LBL_ID_ESTA'] = 'Estado PQRF';
$mod_strings['LBL_NU_PLAC_VEHI'] = 'Placa';
$mod_strings['LBL_NU_CHAS_SERI'] = 'Chasis Vehículo/Serie Máquina';
$mod_strings['LBL_NU_KMS'] = 'Kilometraje Vehículo/Horas Máquina';
$mod_strings['LBL_NU_FACT_REPU'] = 'Número Factura Repuestos';
$mod_strings['LBL_NU_ORDE_TRAB'] = 'Número Orden Trabajo Servicio';
$mod_strings['LBL_ID_DERE_PETI'] = 'Derecho de Petición';
$mod_strings['LBL_ID_CONTACTO'] = 'Cómo Prefiere que le Contactemos?';
$mod_strings['LBL_CD_CIA'] = 'Compañía';
$mod_strings['LBL_CD_AREA'] = 'Subunidad de Negocio';
$mod_strings['LBL_NU_NIT_EMPL'] = 'Nombre Representante';
$mod_strings['LBL_NU_PQRF'] = 'Número PQRF';

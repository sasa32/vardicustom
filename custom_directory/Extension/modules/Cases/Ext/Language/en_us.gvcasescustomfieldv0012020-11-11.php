<?php

$mod_strings['LBL_SASA_INSTRUCTIVO_C'] = 'Instructivo';
$mod_strings['LBL_SASA_DOCUMENT_SOLICITADO_C'] = 'Documento Solicitado';
$mod_strings['LBL_SASA_AREA_SOLICITANTE_C'] = 'Área Solicitante';
$mod_strings['LBL_SASA_PQRFS_C'] = 'PQRFS';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_FECHA_CIERRE_PQRF_C'] = 'Fecha cierre PQRF';
$mod_strings['LBL_CD_UNEG_CONT'] = 'Unidad de Negocio';

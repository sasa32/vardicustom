<?php
$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	3,

	//Label. A string value to identify the hook.
	'beforesavecases',

	//The PHP file where your class is located.
	'custom/modules/Cases/beforesavecases.php',

	//The class the method is in.
	'beforesavecases',

	//The method to call.
	'before_save'
);

?>

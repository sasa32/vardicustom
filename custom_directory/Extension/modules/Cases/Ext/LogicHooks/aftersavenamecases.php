<?php
$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'aftersavenamecases',

	//The PHP file where your class is located.
	'custom/modules/Cases/aftersavenamecases.php',

	//The class the method is in.
	'aftersavenamecases',

	//The method to call.
	'after_save'
);

?>

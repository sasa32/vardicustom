<?php
 // created: 2022-10-21 22:07:54
$dictionary['Case']['fields']['sasa_numerogestiones_c']['labelValue']='Número de Gestiones';
$dictionary['Case']['fields']['sasa_numerogestiones_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_numerogestiones_c']['enforced']='';
$dictionary['Case']['fields']['sasa_numerogestiones_c']['dependency']='or(
equal($sasa_motivo_c,74),
equal($sasa_motivo_c,73),
equal($sasa_motivo_c,71),
equal($sasa_motivo_c,68),
equal($sasa_motivo_c,141),
equal($sasa_motivo_c,142),
equal($sasa_motivo_c,143),
equal($sasa_motivo_c,204))';
$dictionary['Case']['fields']['sasa_numerogestiones_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_numerogestiones_c']['readonly_formula']='';

 ?>
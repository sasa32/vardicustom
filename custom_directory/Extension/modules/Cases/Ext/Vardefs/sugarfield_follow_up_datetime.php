<?php
 // created: 2020-09-22 15:07:29
$dictionary['Case']['fields']['follow_up_datetime']['audited']=false;
$dictionary['Case']['fields']['follow_up_datetime']['massupdate']=false;
$dictionary['Case']['fields']['follow_up_datetime']['comments']='Deadline for following up on an issue';
$dictionary['Case']['fields']['follow_up_datetime']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['follow_up_datetime']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['follow_up_datetime']['merge_filter']='disabled';
$dictionary['Case']['fields']['follow_up_datetime']['reportable']=false;
$dictionary['Case']['fields']['follow_up_datetime']['calculated']=false;
$dictionary['Case']['fields']['follow_up_datetime']['enable_range_search']=false;

 ?>
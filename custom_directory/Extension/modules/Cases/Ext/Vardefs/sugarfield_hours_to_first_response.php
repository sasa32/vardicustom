<?php
 // created: 2020-09-22 15:08:53
$dictionary['Case']['fields']['hours_to_first_response']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['hours_to_first_response']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['hours_to_first_response']['merge_filter']='disabled';
$dictionary['Case']['fields']['hours_to_first_response']['reportable']=false;
$dictionary['Case']['fields']['hours_to_first_response']['calculated']=false;
$dictionary['Case']['fields']['hours_to_first_response']['enable_range_search']=false;
$dictionary['Case']['fields']['hours_to_first_response']['precision']=2;

 ?>
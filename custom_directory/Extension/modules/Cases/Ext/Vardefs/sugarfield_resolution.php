<?php
 // created: 2020-09-22 14:35:17
$dictionary['Case']['fields']['resolution']['required']=true;
$dictionary['Case']['fields']['resolution']['audited']=true;
$dictionary['Case']['fields']['resolution']['massupdate']=false;
$dictionary['Case']['fields']['resolution']['comments']='The resolution of the case';
$dictionary['Case']['fields']['resolution']['importable']='false';
$dictionary['Case']['fields']['resolution']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['resolution']['duplicate_merge_dom_value']='0';
$dictionary['Case']['fields']['resolution']['merge_filter']='disabled';
$dictionary['Case']['fields']['resolution']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.65',
  'searchable' => true,
);
$dictionary['Case']['fields']['resolution']['calculated']=false;
$dictionary['Case']['fields']['resolution']['rows']='4';
$dictionary['Case']['fields']['resolution']['cols']='20';
$dictionary['Case']['fields']['resolution']['dependency']='equal($status,"Closed")';

 ?>
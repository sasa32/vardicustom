<?php
 // created: 2022-12-05 19:56:06
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['audited']=true;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['massupdate']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['dependency']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1_name']['vname']='LBL_SASA_CENTROSDECOSTOS_CASES_1_NAME_FIELD_TITLE';

 ?>
<?php
// created: 2022-10-05 15:14:01
$dictionary["Case"]["fields"]["sasa_centrosdecostos_cases_1"] = array (
  'name' => 'sasa_centrosdecostos_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_centrosdecostos_cases_1',
  'source' => 'non-db',
  'module' => 'sasa_CentrosDeCostos',
  'bean_name' => 'sasa_CentrosDeCostos',
  'side' => 'right',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE',
  'id_name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_centrosdecostos_cases_1_name"] = array (
  'name' => 'sasa_centrosdecostos_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_SASA_CENTROSDECOSTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'link' => 'sasa_centrosdecostos_cases_1',
  'table' => 'sasa_centrosdecostos',
  'module' => 'sasa_CentrosDeCostos',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida"] = array (
  'name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'link' => 'sasa_centrosdecostos_cases_1',
  'table' => 'sasa_centrosdecostos',
  'module' => 'sasa_CentrosDeCostos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

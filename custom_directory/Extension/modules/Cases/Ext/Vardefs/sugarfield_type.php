<?php
 // created: 2020-09-22 15:07:18
$dictionary['Case']['fields']['type']['default']='Administration';
$dictionary['Case']['fields']['type']['len']=100;
$dictionary['Case']['fields']['type']['audited']=false;
$dictionary['Case']['fields']['type']['massupdate']=false;
$dictionary['Case']['fields']['type']['comments']='The type of issue (ex: issue, feature)';
$dictionary['Case']['fields']['type']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['type']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['type']['merge_filter']='disabled';
$dictionary['Case']['fields']['type']['reportable']=false;
$dictionary['Case']['fields']['type']['calculated']=false;
$dictionary['Case']['fields']['type']['dependency']=false;

 ?>
<?php
 // created: 2020-09-22 15:08:03
$dictionary['Case']['fields']['portal_viewable']['default']=true;
$dictionary['Case']['fields']['portal_viewable']['audited']=false;
$dictionary['Case']['fields']['portal_viewable']['massupdate']=false;
$dictionary['Case']['fields']['portal_viewable']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['portal_viewable']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['portal_viewable']['merge_filter']='disabled';
$dictionary['Case']['fields']['portal_viewable']['unified_search']=false;
$dictionary['Case']['fields']['portal_viewable']['calculated']=false;

 ?>
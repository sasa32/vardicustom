<?php
 // created: 2020-09-22 15:09:22
$dictionary['Case']['fields']['first_response_var_from_target']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['first_response_var_from_target']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['first_response_var_from_target']['merge_filter']='disabled';
$dictionary['Case']['fields']['first_response_var_from_target']['reportable']=false;
$dictionary['Case']['fields']['first_response_var_from_target']['calculated']=false;
$dictionary['Case']['fields']['first_response_var_from_target']['enable_range_search']=false;
$dictionary['Case']['fields']['first_response_var_from_target']['precision']=2;

 ?>
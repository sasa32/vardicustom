<?php
 // created: 2020-09-22 14:39:59
$dictionary['Case']['fields']['work_log']['audited']=true;
$dictionary['Case']['fields']['work_log']['massupdate']=false;
$dictionary['Case']['fields']['work_log']['comments']='Free-form text used to denote activities of interest';
$dictionary['Case']['fields']['work_log']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['work_log']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['work_log']['merge_filter']='disabled';
$dictionary['Case']['fields']['work_log']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.64',
  'searchable' => true,
);
$dictionary['Case']['fields']['work_log']['calculated']=false;
$dictionary['Case']['fields']['work_log']['rows']='4';
$dictionary['Case']['fields']['work_log']['cols']='20';

 ?>
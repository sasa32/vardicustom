<?php
 // created: 2023-03-06 03:33:06
$dictionary['Case']['fields']['contacts_cases_1_name']['audited']=true;
$dictionary['Case']['fields']['contacts_cases_1_name']['massupdate']=false;
$dictionary['Case']['fields']['contacts_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['contacts_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['contacts_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['contacts_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['contacts_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['contacts_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['contacts_cases_1_name']['vname']='LBL_CONTACTS_CASES_1_NAME_FIELD_TITLE';

 ?>
<?php
 // created: 2022-12-26 16:59:29
$dictionary['Case']['fields']['cd_area_c']['labelValue']='Subunidad de Negocio';
$dictionary['Case']['fields']['cd_area_c']['dependency']='';
$dictionary['Case']['fields']['cd_area_c']['required_formula']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['cd_area_c']['readonly_formula']='';
$dictionary['Case']['fields']['cd_area_c']['visibility_grid']=array (
  'trigger' => 'cd_uneg_cont_c',
  'values' => 
  array (
    110 => 
    array (
      0 => '',
      1 => '11011',
    ),
    120 => 
    array (
      0 => '',
      1 => '12020',
    ),
    130 => 
    array (
      0 => '',
      1 => '13030',
    ),
    153 => 
    array (
      0 => '',
      1 => '15353',
    ),
    210 => 
    array (
      0 => '',
      1 => '21010',
      2 => '21012',
    ),
    220 => 
    array (
      0 => '',
      1 => '22020',
    ),
    310 => 
    array (
      0 => '',
      1 => '31011',
    ),
    315 => 
    array (
      0 => '',
      1 => '31515',
    ),
    340 => 
    array (
      0 => '',
      1 => '34044',
    ),
    '' => 
    array (
    ),
  ),
);

 ?>
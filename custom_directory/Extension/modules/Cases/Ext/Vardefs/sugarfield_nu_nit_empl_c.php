<?php
 // created: 2022-10-21 22:07:35
$dictionary['Case']['fields']['nu_nit_empl_c']['labelValue']='No. documento Representante';
$dictionary['Case']['fields']['nu_nit_empl_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_nit_empl_c']['enforced']='';
$dictionary['Case']['fields']['nu_nit_empl_c']['dependency']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['nu_nit_empl_c']['required_formula']='';
$dictionary['Case']['fields']['nu_nit_empl_c']['readonly_formula']='';

 ?>
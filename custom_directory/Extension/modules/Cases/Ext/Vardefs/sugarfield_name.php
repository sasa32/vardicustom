<?php
 // created: 2020-12-22 15:38:09
$dictionary['Case']['fields']['name']['len']='255';
$dictionary['Case']['fields']['name']['massupdate']=false;
$dictionary['Case']['fields']['name']['comments']='The short description of the bug';
$dictionary['Case']['fields']['name']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['name']['merge_filter']='disabled';
$dictionary['Case']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.53',
  'searchable' => true,
);
$dictionary['Case']['fields']['name']['calculated']='1';
$dictionary['Case']['fields']['name']['importable']='false';
$dictionary['Case']['fields']['name']['formula']='concat(toString($case_number)," - ",getDropdownValue("sasa_tipo_cases_c_list",$sasa_tipo_c)," - ",getDropdownValue("sasa_motivo_c_list",$sasa_motivo_c))';
$dictionary['Case']['fields']['name']['enforced']=true;

 ?>
<?php
 // created: 2022-12-26 16:48:06
$dictionary['Case']['fields']['cd_uneg_cont_c']['labelValue']='Unidad de Negocio';
$dictionary['Case']['fields']['cd_uneg_cont_c']['dependency']='';
$dictionary['Case']['fields']['cd_uneg_cont_c']['required_formula']='equal($sasa_pqrf_c,"S")';
$dictionary['Case']['fields']['cd_uneg_cont_c']['readonly_formula']='';
$dictionary['Case']['fields']['cd_uneg_cont_c']['visibility_grid']=array (
  'trigger' => 'cd_cia_c',
  'values' => 
  array (
    1 => 
    array (
      0 => '',
      1 => '153',
      2 => '120',
      3 => '130',
      4 => '110',
    ),
    2 => 
    array (
      0 => '',
      1 => '210',
      2 => '220',
    ),
    3 => 
    array (
      0 => '',
      1 => '310',
      2 => '315',
      3 => '340',
    ),
    '' => 
    array (
    ),
  ),
);

 ?>
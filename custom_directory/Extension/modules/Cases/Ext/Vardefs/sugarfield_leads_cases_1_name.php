<?php
 // created: 2023-03-06 03:34:53
$dictionary['Case']['fields']['leads_cases_1_name']['audited']=true;
$dictionary['Case']['fields']['leads_cases_1_name']['massupdate']=false;
$dictionary['Case']['fields']['leads_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['leads_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['leads_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['leads_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['leads_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['leads_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['leads_cases_1_name']['vname']='LBL_LEADS_CASES_1_NAME_FIELD_TITLE';

 ?>
<?php
 // created: 2022-12-05 19:56:06
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['name']='sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['type']='id';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['source']='non-db';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['vname']='LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['id_name']='sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['link']='sasa_centrosdecostos_cases_1';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['table']='sasa_centrosdecostos';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['module']='sasa_CentrosDeCostos';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['rname']='id';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['reportable']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['side']='right';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['massupdate']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['hideacl']=true;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['audited']=false;
$dictionary['Case']['fields']['sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida']['importable']='true';

 ?>
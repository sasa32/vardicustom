<?php
 // created: 2020-09-22 14:32:01
$dictionary['Case']['fields']['status']['default']='New';
$dictionary['Case']['fields']['status']['massupdate']=true;
$dictionary['Case']['fields']['status']['comments']='The status of the case';
$dictionary['Case']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['status']['merge_filter']='disabled';
$dictionary['Case']['fields']['status']['calculated']=false;
$dictionary['Case']['fields']['status']['dependency']=false;
$dictionary['Case']['fields']['status']['required']=true;

 ?>
<?php
 // created: 2022-10-21 22:07:31
$dictionary['Case']['fields']['nu_fact_repu_c']['labelValue']='Número Factura Repuestos';
$dictionary['Case']['fields']['nu_fact_repu_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_fact_repu_c']['enforced']='';
$dictionary['Case']['fields']['nu_fact_repu_c']['dependency']='or(equal($sasa_motivo_c,"102"),equal($sasa_motivo_c,"71"))';
$dictionary['Case']['fields']['nu_fact_repu_c']['readonly_formula']='';

 ?>
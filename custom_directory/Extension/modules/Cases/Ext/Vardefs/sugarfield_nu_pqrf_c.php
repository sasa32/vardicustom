<?php
 // created: 2022-10-21 22:07:37
$dictionary['Case']['fields']['nu_pqrf_c']['labelValue']='Número PQRF';
$dictionary['Case']['fields']['nu_pqrf_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_pqrf_c']['enforced']='false';
$dictionary['Case']['fields']['nu_pqrf_c']['dependency']='equal($sasa_pqrf_c,"S")';

 ?>
<?php
 // created: 2022-12-27 20:00:15
$dictionary['Case']['fields']['priority']['default']='';
$dictionary['Case']['fields']['priority']['required']=false;
$dictionary['Case']['fields']['priority']['massupdate']=false;
$dictionary['Case']['fields']['priority']['comments']='The priority of the case';
$dictionary['Case']['fields']['priority']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['priority']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['priority']['merge_filter']='disabled';
$dictionary['Case']['fields']['priority']['calculated']=false;
$dictionary['Case']['fields']['priority']['dependency']=false;
$dictionary['Case']['fields']['priority']['hidemassupdate']=false;
$dictionary['Case']['fields']['priority']['visibility_grid']=array (
);

 ?>
<?php
 // created: 2023-05-10 16:24:00
$dictionary['Case']['fields']['source']['len']=100;
$dictionary['Case']['fields']['source']['required']=true;
$dictionary['Case']['fields']['source']['audited']=true;
$dictionary['Case']['fields']['source']['massupdate']=true;
$dictionary['Case']['fields']['source']['options']='source_cases_list_c';
$dictionary['Case']['fields']['source']['comments']='An indicator of how the bug was entered (ex: via web, email, etc.)';
$dictionary['Case']['fields']['source']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['source']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['source']['merge_filter']='disabled';
$dictionary['Case']['fields']['source']['calculated']=false;
$dictionary['Case']['fields']['source']['dependency']=false;
$dictionary['Case']['fields']['source']['default']='';
$dictionary['Case']['fields']['source']['hidemassupdate']=false;
$dictionary['Case']['fields']['source']['visibility_grid']=array (
);

 ?>
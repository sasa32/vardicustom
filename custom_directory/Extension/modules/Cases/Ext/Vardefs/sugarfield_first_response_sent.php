<?php
 // created: 2020-09-22 15:09:52
$dictionary['Case']['fields']['first_response_sent']['default']=false;
$dictionary['Case']['fields']['first_response_sent']['audited']=false;
$dictionary['Case']['fields']['first_response_sent']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['first_response_sent']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['first_response_sent']['merge_filter']='disabled';
$dictionary['Case']['fields']['first_response_sent']['reportable']=false;
$dictionary['Case']['fields']['first_response_sent']['unified_search']=false;
$dictionary['Case']['fields']['first_response_sent']['calculated']=false;

 ?>
<?php
 // created: 2020-10-08 09:03:35
$dictionary['Case']['fields']['date_entered']['audited']=true;
$dictionary['Case']['fields']['date_entered']['comments']='Date record created';
$dictionary['Case']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Case']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Case']['fields']['date_entered']['calculated']=false;
$dictionary['Case']['fields']['date_entered']['enable_range_search']='1';

 ?>
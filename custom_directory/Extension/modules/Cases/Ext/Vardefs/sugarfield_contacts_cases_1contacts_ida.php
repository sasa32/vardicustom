<?php
 // created: 2023-03-06 03:33:06
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['name']='contacts_cases_1contacts_ida';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['type']='id';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['source']='non-db';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['vname']='LBL_CONTACTS_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['id_name']='contacts_cases_1contacts_ida';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['link']='contacts_cases_1';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['table']='contacts';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['module']='Contacts';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['rname']='id';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['reportable']=false;
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['side']='right';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['massupdate']=false;
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['hideacl']=true;
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['audited']=false;
$dictionary['Case']['fields']['contacts_cases_1contacts_ida']['importable']='true';

 ?>
<?php
 // created: 2020-09-21 16:32:54
$dictionary['Case']['fields']['description']['required']=true;
$dictionary['Case']['fields']['description']['audited']=true;
$dictionary['Case']['fields']['description']['massupdate']=false;
$dictionary['Case']['fields']['description']['comments']='Full text of the note';
$dictionary['Case']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['description']['merge_filter']='disabled';
$dictionary['Case']['fields']['description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '0.66',
  'searchable' => false,
);
$dictionary['Case']['fields']['description']['calculated']=false;
$dictionary['Case']['fields']['description']['rows']='6';
$dictionary['Case']['fields']['description']['cols']='80';

 ?>
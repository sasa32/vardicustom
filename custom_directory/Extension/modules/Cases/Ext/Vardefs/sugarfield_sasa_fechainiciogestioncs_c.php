<?php
 // created: 2022-10-21 22:07:52
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['labelValue']='Fecha inicio de gestión ';
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['enforced']='';
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['dependency']='or(
equal($sasa_motivo_c,74),
equal($sasa_motivo_c,73),
equal($sasa_motivo_c,71),
equal($sasa_motivo_c,68),
equal($sasa_motivo_c,141),
equal($sasa_motivo_c,142),
equal($sasa_motivo_c,143),
equal($sasa_motivo_c,204))';
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_fechainiciogestioncs_c']['readonly_formula']='';

 ?>
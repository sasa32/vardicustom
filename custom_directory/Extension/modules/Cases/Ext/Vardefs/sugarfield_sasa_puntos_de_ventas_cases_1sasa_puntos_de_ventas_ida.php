<?php
 // created: 2022-09-30 15:21:00
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['name']='sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['type']='id';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['source']='non-db';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['vname']='LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['id_name']='sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['link']='sasa_puntos_de_ventas_cases_1';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['table']='sasa_puntos_de_ventas';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['module']='sasa_Puntos_de_Ventas';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['rname']='id';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['reportable']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['side']='right';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['massupdate']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['hideacl']=true;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['audited']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida']['importable']='true';

 ?>
<?php
 // created: 2020-09-22 15:09:11
$dictionary['Case']['fields']['business_hours_to_resolution']['audited']=false;
$dictionary['Case']['fields']['business_hours_to_resolution']['massupdate']=false;
$dictionary['Case']['fields']['business_hours_to_resolution']['comments']='How long it took to resolve this issue, in decimal business hours';
$dictionary['Case']['fields']['business_hours_to_resolution']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['business_hours_to_resolution']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['business_hours_to_resolution']['merge_filter']='disabled';
$dictionary['Case']['fields']['business_hours_to_resolution']['reportable']=false;
$dictionary['Case']['fields']['business_hours_to_resolution']['calculated']=false;
$dictionary['Case']['fields']['business_hours_to_resolution']['enable_range_search']=false;

 ?>
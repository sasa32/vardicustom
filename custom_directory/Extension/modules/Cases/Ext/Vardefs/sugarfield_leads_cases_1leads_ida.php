<?php
 // created: 2023-03-06 03:34:53
$dictionary['Case']['fields']['leads_cases_1leads_ida']['name']='leads_cases_1leads_ida';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['type']='id';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['source']='non-db';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['vname']='LBL_LEADS_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['id_name']='leads_cases_1leads_ida';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['link']='leads_cases_1';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['table']='leads';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['module']='Leads';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['rname']='id';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['reportable']=false;
$dictionary['Case']['fields']['leads_cases_1leads_ida']['side']='right';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['massupdate']=false;
$dictionary['Case']['fields']['leads_cases_1leads_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['leads_cases_1leads_ida']['hideacl']=true;
$dictionary['Case']['fields']['leads_cases_1leads_ida']['audited']=false;
$dictionary['Case']['fields']['leads_cases_1leads_ida']['importable']='true';

 ?>
<?php
 // created: 2022-10-21 22:07:50
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['labelValue']='Campo espejo Linea (Notificacion Asesor)';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['calculated']='true';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['formula']='related($sasa_vehiculos_cases_1,"name")';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['enforced']='true';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['dependency']='';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_lineanotificacionase_c']['readonly_formula']='';

 ?>
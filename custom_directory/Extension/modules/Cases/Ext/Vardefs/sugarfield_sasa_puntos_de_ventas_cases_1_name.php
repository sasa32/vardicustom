<?php
 // created: 2022-09-30 15:21:00
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['audited']=true;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['massupdate']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['dependency']='equal($sasa_pqrf_c,"N")';
$dictionary['Case']['fields']['sasa_puntos_de_ventas_cases_1_name']['vname']='LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_NAME_FIELD_TITLE';

 ?>
<?php
 // created: 2020-09-22 15:08:13
$dictionary['Case']['fields']['primary_contact_name']['len']=255;
$dictionary['Case']['fields']['primary_contact_name']['audited']=false;
$dictionary['Case']['fields']['primary_contact_name']['massupdate']=false;
$dictionary['Case']['fields']['primary_contact_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['primary_contact_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['primary_contact_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['primary_contact_name']['reportable']=false;
$dictionary['Case']['fields']['primary_contact_name']['calculated']=false;

 ?>
<?php
 // created: 2020-09-22 15:08:37
$dictionary['Case']['fields']['first_response_actual_datetime']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['first_response_actual_datetime']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['first_response_actual_datetime']['merge_filter']='disabled';
$dictionary['Case']['fields']['first_response_actual_datetime']['reportable']=false;
$dictionary['Case']['fields']['first_response_actual_datetime']['calculated']=false;
$dictionary['Case']['fields']['first_response_actual_datetime']['enable_range_search']=false;

 ?>
<?php
 // created: 2020-10-08 09:03:43
$dictionary['Case']['fields']['date_modified']['audited']=true;
$dictionary['Case']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Case']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Case']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Case']['fields']['date_modified']['calculated']=false;
$dictionary['Case']['fields']['date_modified']['enable_range_search']='1';

 ?>
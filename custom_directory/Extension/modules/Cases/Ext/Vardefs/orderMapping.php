<?php
// created: 2023-05-10 16:24:02
$extensionOrderMap = array (
  'custom/Extension/modules/Cases/Ext/Vardefs/full_text_search_admin.php' => 
  array (
    'md5' => '47d7c50bd63b9737443c8b1944db0176',
    'mtime' => 1609259821,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_case_number.php' => 
  array (
    'md5' => '9b57147276323dfb195f291fb3d00060',
    'mtime' => 1609259821,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_business_hours_to_resolution.php' => 
  array (
    'md5' => '80ff3e3b8460324a26c4543478e49f20',
    'mtime' => 1609259821,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_target_datetime.php' => 
  array (
    'md5' => '2fbf51a67f4e7a6be4b89cf71cfe3c2f',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_actual_datetime.php' => 
  array (
    'md5' => '37429afcb08088245e85505dec4ced48',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_date_entered.php' => 
  array (
    'md5' => 'ae13cd534196c7de0a49b4a2848dbe73',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_hours_to_first_response.php' => 
  array (
    'md5' => 'f623966556aba65bdd89a2ed535e0259',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_work_log.php' => 
  array (
    'md5' => '89aaf702bf1c0d3de47728bae3378f01',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_sent.php' => 
  array (
    'md5' => '559fab8469d593eee965950949e6c59e',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_hours_to_resolution.php' => 
  array (
    'md5' => 'ae7dc6545e296fa0a76ccc76084c18a5',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_portal_viewable.php' => 
  array (
    'md5' => '75da4034f39c5d4737bb4cc76c1e33d0',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_name.php' => 
  array (
    'md5' => '416026a88362c2ba8b355e8bd281ecf1',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_var_from_target.php' => 
  array (
    'md5' => '3c2b1421eb339c00f2d8a34aff50f9de',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_primary_contact_name.php' => 
  array (
    'md5' => '61feb49d23cd684592465eed5be4c0f4',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_resolution.php' => 
  array (
    'md5' => '8d501cf49a3133a03c7ecc42a2dabc2b',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_follow_up_datetime.php' => 
  array (
    'md5' => '8d1195a66d2368c8c020eb2719ac7c27',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_first_response_sla_met.php' => 
  array (
    'md5' => '0d778c771571ec7c69e5a292dddb5604',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_date_modified.php' => 
  array (
    'md5' => '8e44f3acf807adcfd4ed1979562552f0',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_status.php' => 
  array (
    'md5' => '0cadb86ffa52d0011f68ffb00c58df83',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_description.php' => 
  array (
    'md5' => '576bb0f3235dc87964708115b4418436',
    'mtime' => 1609259822,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sasa_puntos_de_ventas_cases_1_Cases.php' => 
  array (
    'md5' => 'a1cda5437e900b9a448bf194c434a821',
    'mtime' => 1615326082,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_cd_uneg_cont_c-visibility_grid.php' => 
  array (
    'md5' => 'e10f20e58a2377eed8a40be8e203ed46',
    'mtime' => 1617759339,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_type.php' => 
  array (
    'md5' => '11888325fbc54dbd27ae654e091af0c2',
    'mtime' => 1620661817,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_agendar_citau_c.php' => 
  array (
    'md5' => '45374d9b4eb9524852411838e9cfbdfb',
    'mtime' => 1626381420,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/denorm_account_name.php' => 
  array (
    'md5' => '14271126edccfab59eefa2978cf1f506',
    'mtime' => 1627808688,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sasa_vehiculos_cases_1_Cases.php' => 
  array (
    'md5' => 'a9ea3121437e875de942f78c06358ec3',
    'mtime' => 1628111981,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sasa_vehiculos_cases_2_Cases.php' => 
  array (
    'md5' => '70e8754f487f9149853e4383355064aa',
    'mtime' => 1628112175,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sasa_vehiculos_cases_3_Cases.php' => 
  array (
    'md5' => '0f396cd4b161a06d59d23b1fd46724a9',
    'mtime' => 1628112283,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sasa_vehiculos_cases_4_Cases.php' => 
  array (
    'md5' => '0630aa5bd95217c9610282e269f9c83a',
    'mtime' => 1628112323,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_resolved_datetime.php' => 
  array (
    'md5' => 'a126d04031522ed352ccdbfb607691e5',
    'mtime' => 1632840628,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_nit_empl.php' => 
  array (
    'md5' => '45245f8fce32dfe2d20d60184e57638b',
    'mtime' => 1658932261,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_pqrf.php' => 
  array (
    'md5' => 'c893e350ff205c96c8ef41a5a19f88f2',
    'mtime' => 1658932261,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_contacto.php' => 
  array (
    'md5' => '15ebb1e64e4e9563cac3044b5135ba2b',
    'mtime' => 1658932261,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_dere_peti.php' => 
  array (
    'md5' => 'd8416a233d9b2c4e50687a0a6cd7343c',
    'mtime' => 1658932261,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_esta.php' => 
  array (
    'md5' => '4cca9c5da1345978bbdf3e821a71a189',
    'mtime' => 1658932261,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida.php' => 
  array (
    'md5' => 'a8a2063aa61ab05e26fc91264d0a8563',
    'mtime' => 1664551260,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_puntos_de_ventas_cases_1_name.php' => 
  array (
    'md5' => '7cf2bf2bd87d64c3091547440b352d10',
    'mtime' => 1664551260,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sasa_centrosdecostos_cases_1_Cases.php' => 
  array (
    'md5' => '5c7842846a96033e48ea7066d80c8542',
    'mtime' => 1664982855,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_cd_cia_c.php' => 
  array (
    'md5' => '61a6d5705707afaa88d0533e27dcb6da',
    'mtime' => 1666390046,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_contacto_c.php' => 
  array (
    'md5' => '05c420bca9b803c890c8429a79bad417',
    'mtime' => 1666390048,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_dere_peti_c.php' => 
  array (
    'md5' => '80426843c1daeecd4de970956493bd35',
    'mtime' => 1666390049,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_id_esta_c.php' => 
  array (
    'md5' => 'b32b2a538c41fbb601a38991d0ee72a2',
    'mtime' => 1666390049,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_chas_seri_c.php' => 
  array (
    'md5' => '7ace7552b92583cabc713f8d20496472',
    'mtime' => 1666390050,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_fact_repu_c.php' => 
  array (
    'md5' => 'ab6592463c608d836b4174a46d3b163d',
    'mtime' => 1666390051,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_kms_c.php' => 
  array (
    'md5' => '87d40e190066d7fa1000d14018245ec7',
    'mtime' => 1666390053,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_nit_empl_c.php' => 
  array (
    'md5' => 'e48d6c57f150c880a9fe49c3957b8def',
    'mtime' => 1666390055,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_plac_vehi_c.php' => 
  array (
    'md5' => '4a4ff35d7fd6ceeb9ffa65895d2cddfc',
    'mtime' => 1666390056,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_orde_trab_c.php' => 
  array (
    'md5' => '75d258df18e5f4d91d927498a2ad102a',
    'mtime' => 1666390056,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_nu_pqrf_c.php' => 
  array (
    'md5' => 'de2c4872c48cd9239e0c73448894e450',
    'mtime' => 1666390057,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_area_solicitante_c.php' => 
  array (
    'md5' => '0f5b503fdb20481bb4e9bd4042f873dc',
    'mtime' => 1666390058,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_cod_puntoaten_c.php' => 
  array (
    'md5' => 'd5a50b6a114cee9253e68832f90af7fe',
    'mtime' => 1666390059,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_document_solicitado_c.php' => 
  array (
    'md5' => '42d91353e47b4c647c838301d11db19a',
    'mtime' => 1666390060,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fecha_cierre_pqrf_c.php' => 
  array (
    'md5' => 'b563aaddbfadc579f4f91530873b0ca6',
    'mtime' => 1666390061,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fuente_c.php' => 
  array (
    'md5' => '922d663e049bf38597a94ef7cc10d696',
    'mtime' => 1666390063,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_instructivo_c.php' => 
  array (
    'md5' => 'e7c210efbde17a4b42ad357d609fb736',
    'mtime' => 1666390064,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_pqrf_c.php' => 
  array (
    'md5' => '2cf911a0dbf66c6a8dcb4fd9cd83bafd',
    'mtime' => 1666390065,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_tipo_c.php' => 
  array (
    'md5' => 'ce2186f702b489a3f0de967288a41f9e',
    'mtime' => 1666390066,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_motivollorente_c.php' => 
  array (
    'md5' => '809ce3050c64a8187ef9fdd481ad42f8',
    'mtime' => 1666390067,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_lineanotificacionase_c.php' => 
  array (
    'md5' => '830a3cc189ce3734b48e725115878991',
    'mtime' => 1666390070,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fechafingestioncs_c.php' => 
  array (
    'md5' => '5c896997587108fc03df9143c38cf6e7',
    'mtime' => 1666390070,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fechainiciogestioncs_c.php' => 
  array (
    'md5' => 'ff7d8bcf646edee048d518abf0b5a35c',
    'mtime' => 1666390072,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_estadocontacto_c.php' => 
  array (
    'md5' => '2b67e693d909ed201a7f16aca0f7bfe3',
    'mtime' => 1666390073,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_numerogestiones_c.php' => 
  array (
    'md5' => '3c4257115fb5320f9398be291eb8ad62',
    'mtime' => 1666390074,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_estadocomercial_c.php' => 
  array (
    'md5' => 'f9ad6a906500d14aa1d1500840c7a550',
    'mtime' => 1666390075,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fechacontacto_c.php' => 
  array (
    'md5' => 'e963393430ba924c984331cb236c1e28',
    'mtime' => 1666390075,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_centrocostos_c.php' => 
  array (
    'md5' => '2ce3efe21f40378136d53364982d8b15',
    'mtime' => 1666390076,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_count_call_status_c.php' => 
  array (
    'md5' => 'acf8cd9a9c775d01d62605e9ecd64449',
    'mtime' => 1666390077,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_eval_si_c.php' => 
  array (
    'md5' => '7178e95b072f13ec3f1fee8dbc1988fc',
    'mtime' => 1666390078,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_eval_no_c.php' => 
  array (
    'md5' => '780bdda2cb91707fa1c3fb966993a547',
    'mtime' => 1666390079,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_eval_no_contacto_c.php' => 
  array (
    'md5' => '9bfdcb4ee9d623b7976b7dd79530b332',
    'mtime' => 1666390080,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida.php' => 
  array (
    'md5' => '655ba8fb77ee10062864d2d39f994a6e',
    'mtime' => 1670270166,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_centrosdecostos_cases_1_name.php' => 
  array (
    'md5' => '2cd64940673fc579ab8776077810c72c',
    'mtime' => 1670270166,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_priority.php' => 
  array (
    'md5' => 'ede366b6c1bd9eccae2552817745cf6d',
    'mtime' => 1672992717,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_cd_area_c.php' => 
  array (
    'md5' => 'ae2cc1803c00d0a768cf203a4d2301a3',
    'mtime' => 1672992717,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_cd_uneg_cont_c.php' => 
  array (
    'md5' => 'dd6534ca69be6ffed64f9eb0942a3bb0',
    'mtime' => 1672992717,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_motivo_c.php' => 
  array (
    'md5' => '884c24cba1aa4dcb49d3dbf6d0deba7a',
    'mtime' => 1672992717,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_nombrerepresencs_c.php' => 
  array (
    'md5' => 'a5077002bf55e9d1227f6dc4fd87af97',
    'mtime' => 1672992726,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/customer_journey_parent.php' => 
  array (
    'md5' => '2afec873f5b0301aa97546b34c82b00d',
    'mtime' => 1675854563,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/contacts_cases_1_Cases.php' => 
  array (
    'md5' => '21c09007f07cdb55de1a66eb0f2aee6c',
    'mtime' => 1678073196,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/leads_cases_1_Cases.php' => 
  array (
    'md5' => 'bd525dbc4a60f3cef32b608f64b859f6',
    'mtime' => 1678073327,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_contacts_cases_1_name.php' => 
  array (
    'md5' => '0bca345043013b07ae064c68093e7dd3',
    'mtime' => 1678073586,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_contacts_cases_1contacts_ida.php' => 
  array (
    'md5' => 'ed34015f7f2f14e687ee569b858887e8',
    'mtime' => 1678073586,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_leads_cases_1leads_ida.php' => 
  array (
    'md5' => '3a9616ae6c19b5d3842f36b604e9d476',
    'mtime' => 1678073693,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_leads_cases_1_name.php' => 
  array (
    'md5' => 'c4478f39e1bc81ffb4e5737395d703db',
    'mtime' => 1678073693,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_source.php' => 
  array (
    'md5' => '23ca634f6408b6f8299507f7c58d62d3',
    'mtime' => 1683735840,
    'is_override' => false,
  ),
);
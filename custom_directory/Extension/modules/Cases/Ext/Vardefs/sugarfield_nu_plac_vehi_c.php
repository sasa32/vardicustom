<?php
 // created: 2022-10-21 22:07:36
$dictionary['Case']['fields']['nu_plac_vehi_c']['labelValue']='Placa';
$dictionary['Case']['fields']['nu_plac_vehi_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['nu_plac_vehi_c']['enforced']='';
$dictionary['Case']['fields']['nu_plac_vehi_c']['dependency']='or(
	and(
		equal($sasa_pqrf_c,"N"),
		or(
			equal($sasa_motivo_c,"8"),
			equal($sasa_motivo_c,"49"),
			equal($sasa_motivo_c,"51"),
			equal($sasa_motivo_c,"56"),
			equal($sasa_motivo_c,"58"),
			equal($sasa_motivo_c,"54"),
			equal($sasa_motivo_c,"55"),
			equal($sasa_motivo_c,"59"),
			equal($sasa_motivo_c,"61"),
			equal($sasa_motivo_c,"71"),
			equal($sasa_motivo_c,"69"),
			equal($sasa_motivo_c,"76"),
			equal($sasa_motivo_c,"86"),
			equal($sasa_motivo_c,"145"),
			equal($sasa_motivo_c,"180"),
			equal($sasa_motivo_c,"194"),
			equal($sasa_motivo_c,"213"),
			equal($sasa_motivo_c,"50"),
			equal($sasa_motivo_c,"52"),
			equal($sasa_motivo_c,"68"),
			equal($sasa_motivo_c,"90"),
			equal($sasa_motivo_c,"102"),
			equal($sasa_motivo_c,"103"),
			equal($sasa_motivo_c,"104"),
			equal($sasa_motivo_c,"105"),
			equal($sasa_motivo_c,"119"),
			equal($sasa_motivo_c,"135"),
			equal($sasa_motivo_c,"154"),
			equal($sasa_motivo_c,"169"),
			equal($sasa_motivo_c,"172"),
			equal($sasa_motivo_c,"173"),
			equal($sasa_motivo_c,"176"),
			equal($sasa_motivo_c,"200")
		)
	),equal($sasa_pqrf_c,"S"),or(
			equal($cd_uneg_cont_c,"130"),
			equal($cd_uneg_cont_c,"210"),
			equal($cd_uneg_cont_c,"310")
			)
)';
$dictionary['Case']['fields']['nu_plac_vehi_c']['required_formula']='';
$dictionary['Case']['fields']['nu_plac_vehi_c']['readonly_formula']='';

 ?>
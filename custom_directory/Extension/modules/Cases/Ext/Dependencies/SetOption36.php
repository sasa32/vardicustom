<?php
$dependencies['Cases']['SetOption36'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"H"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","21","20","22")',
           'labels' => 'createList("","BUEN SERVICIO AL CLIENTE","BUEN DESEMPENO DEL EMPLEADO","BUENA CALIDAD DEL PRODUCTO O SERVICIO")'
        ),
      ),
    ),
);

<?php
$dependencies['Cases']['DIN_SAN_NA_OTR_9'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($sasa_tipo_c,"G"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","23","211")',
				'labels' => 'createList("","CAIDA LLAMADA","TRANSFERENCIA DE LLAMADA")'
			),
		),
	),
);
<?php
$dependencies['Cases']['SetOption_NoVigente03'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"340"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","28","202","150","117","1","201","4","116","34","123","130","210","84","209","80","35","38","53","17","134")',
           'labels' => 'createList("","CAJA, TRANSMISION Y EMBRAGUE","SUSPENSION","INYECCION,ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","DEVOLUCION DE DINERO","TRAMITES","DEMORA EN LA ENTREGA DEL VEHICULO","CALIDAD DE PRODUCTO EN LA ENTREGA","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","AUTORIZACION DE USO DE DATOS PERSONALES","INFORMACION BRINDADA SOBRE PRODUCTO")'
        ),
      ),
    ),
);

<?php
$dependencies['Cases']['SetOption_tipo_NA_y_compania_TAutorizados'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"NA"),equal($cd_cia_c,"2"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","232","234","235","236","233")',
           'labels' => 'createList("","RECORDATORIO CITA TALLER PROGRAMADA","CONVOCATORIA CLIENTES DE RMP","CONVOCATORIA CLIENTES PRIMER SERVICIO","REPROGRAMACIÓN DE CITAS NO CUMPLIDAS","ENCUESTAS")'
        ),
      ),
    ),
);

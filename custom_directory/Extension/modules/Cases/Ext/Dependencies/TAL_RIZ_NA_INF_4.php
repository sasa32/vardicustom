<?php
$dependencies['Cases']['TAL_RIZ_NA_INF_4'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","101","90","103","9","136","177","135","104","105","50","119","154","169","172","173","200")',
				'labels' => 'createList("","ESTADO DEL NEGOCIO","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","PROPUESTA COMERCIAL","INFORMACIÓN CAMPAÑA DE SEGURIDAD","ESTADO VEHÍCULO EN COLISIÓN","ESTADO VEHÍCULO EN TALLER","CAMPAÑA DE SERVICIO","HISTORIAL DE SERVICIO","MANTENIMIENTO PREVENTIVO","POLIZA","PRECIOS RMP","PREPAGADOS","SOPORTE TECNICO")'
			),
		),
	),
);
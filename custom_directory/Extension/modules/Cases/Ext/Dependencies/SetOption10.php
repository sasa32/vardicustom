<?php
$dependencies['Cases']['SetOption10'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","193","195","189","198","5","192","17")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","SOLICITUD DE COTIZACION","SOLICITUD DE FICHA TECNICA","SOLICITUD COPIA DE DOCUMENTOS","SOLICITUD INFO TRASPASO VEHICULO RETOMA","ACTITUD DEL PERSONAL DE SERVICIO","SOLICITUD DE CERTIFICACION","AUTORIZACION DE USO DE DATOS PERSONALES")'
        ),
      ),
    ),
);

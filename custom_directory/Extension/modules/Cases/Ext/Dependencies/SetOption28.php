<?php
$dependencies['Cases']['SetOption28'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","158","202","93","117","1","201","4","181","34","30","40","65","179","16","45","84","29","106","167","35","91","107","116","152","147","85","92","78","32","42","38","53","47","17","46","115","43","36","207","39","83","118","18","187","125","157","24","27","26","95","96","165","44","62","163","209","128","123","80","212","113","121","112")',
           'labels' => 'createList("","MOTOR, CAJA, TRANSMISION Y EMBRAGUE","SUSPENSION","ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","RUIDOS","CALIDAD DE PRODUCTO","CALIDAD DE LA REPARACION","CALIDAD DEL DIAGNOSTICO","COSTOS DE LA REPARACION O SERVICIO","RAYONES O GOLPES","AUTORIZACION DE TRABAJOS ADICIONALES","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","CALIDAD DE ACCESORIOS","ESTATICA","PERMANENCIA DEL VEHICULO EN EL TALLER","CALIDAD DE PRODUCTO EN LA ENTREGA","DISPONIBILIDAD DE CITAS PARA EL TALLER","EXPLICACION DE GARANTIA","FALTANTES EN LA ENTREGA","LIMPIEZA DEL VEHICULO","INVENTARIO DEL VEHICULO/PERTENENCIAS DE","DEVOLUCION SALDO A FAVOR","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD DE LLANTAS","CALIDAD DEL REPUESTO","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","CALIDAD EN LA REV. MANTENIMIENTO PERIODI","AUTORIZACION DE USO DE DATOS PERSONALES","CALIDAD EN LA REPARACION DE COLISION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","TRABAJO INCOMPLETO","CALIDAD DEL AIRE ACONDICIONADO","DEMORA EN TRAMITES FINTERNOS","FUNCIONAMIENTO RADIO/SISTEMA DE AUDIO","BOLSA DE AIRE","SISTEMA DE NAVEGACION","INCUMPLIMIENTO DE COMPROMISOS","MOTOR","CAJA DE DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","CALIDAD EN EL ALISTAMIENTO","CONSUMO DE COMBUSTIBLE","PAGOS ELECTRONICOS","TRAMITES","INCUMPLIMIENTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","DEMORA EN LA ENTREGA DEL VEHICULO","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIA FACTURA","FALTA DE COMUNICACIÓN CON EL CLIENTE")'
        ),
      ),
    ),
);

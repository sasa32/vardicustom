<?php
$dependencies['Cases']['SetOption_tipo_NA_y_compania_DNissan_o_TAutorizados'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"NA"),or(equal($cd_cia_c,"1"),equal($cd_cia_c,"2")))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","233")',
           'labels' => 'createList("","ENCUESTAS")'
        ),
      ),
    ),
);

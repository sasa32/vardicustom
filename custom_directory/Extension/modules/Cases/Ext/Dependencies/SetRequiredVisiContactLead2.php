<?php
$dependencies['Cases']['SetRequiredVisiContactLead2'] = array(
    'hooks' => array("edit", "view", "save"),
    'trigger' => 'equal(related($accounts,"id"),"c5f0a53e-8cc3-11eb-b3f2-0286beac7abe")',
    'onload' => false,
    //'triggerFields' => array('account_name'),
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'contacts_cases_1_name',
                'value' => 'equal(related($accounts,"id"),"c5f0a53e-8cc3-11eb-b3f2-0286beac7abe")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'contacts_cases_1_name',
                'value' => 'equal(related($accounts,"id"),"c5f0a53e-8cc3-11eb-b3f2-0286beac7abe")',
            ),
        ),
    ),
    'notActions' => array(
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'contacts_cases_1_name',
                'value' => false,
            ),
        ),
    )
);

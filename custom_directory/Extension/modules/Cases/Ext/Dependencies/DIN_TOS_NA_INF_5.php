<?php
$dependencies['Cases']['DIN_TOS_NA_INF_5'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","102","176","182","90","103","9","136")',
				'labels' => 'createList("","ESTADO DEL PEDIDO DEL REPUESTO","PROMOCIÓN MES","SEGUIMIENTO A GUIAS DESPACHOS","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS")'
			),
		),
	),
);
<?php
$dependencies['Cases']['SetOption31'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"240"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","34","38","53","201","167","43","36","83","202","93","117","1","4","181","40","65","179","16","45","84","29","106","35","91","107","116","152","147","85","92","78","32","42","47","17","46","115","207","39","118","18","125","157","24","27","26","95","96","165","62","163","209","128","123","80","212","113","121","112")',
           'labels' => 'createList("","CALIDAD DE PRODUCTO","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","SUMINISTRO DE REPUESTOS","PERMANENCIA DEL VEHICULO EN EL TALLER","CALIDAD DEL SERVICIO","CALIDAD DE PRODUCTO EN LA ENTREGA TALLER","DEMORA EN TRAMITES INTERNOS","SUSPENSION","ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","ACCESORIOS NO INSTALADOS","RUIDOS","CALIDAD DEL DIAGNOSTICO","COSTOS DE LA REPARACION O SERVICIO","RAYONES O GOLPES","AUTORIZACION DE TRABAJOS ADICIONALES","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","CALIDAD DE ACCESORIOS","ESTATICA","CALIDAD DE PRODUCTO EN LA ENTREGA","DISPONIBILIDAD DE CITAS PARA EL TALLER","EXPLICACION DE GARANTIA","FALTANTES EN LA ENTREGA","LIMPIEZA DEL VEHICULO","INVENTARIO DEL VEHICULO/PERTENENCIAS DE","DEVOLUCION SALDO A FAVOR","DISPONIBILIDAD DE REPUESTOS","DEMORA EN EL SUMINISTRO DE REPUESTOS","CALIDAD DE LLANTAS","CALIDAD DEL REPUESTO","CALIDAD EN LA REV. MANTENIMIENTO PERIODI","AUTORIZACION DE USO DE DATOS PERSONALES","CALIDAD EN LA REPARACION DE COLISION","FALTA EXPLICACION DEL SERVICIO EFECTUADO","TRABAJO INCOMPLETO","CALIDAD DEL AIRE ACONDICIONADO","FUNCIONAMIENTO RADIO/SISTEMA DE AUDIO","BOLSA DE AIRE","INCUMPLIMIENTO DE COMPROMISOS","MOTOR","CAJA DE DIRECCION","CAJA TRANSMISION","CAJA MECANICA","EMBRAGUE","ENCENDIDO","PERDIDA DE POTENCIA","CONSUMO DE COMBUSTIBLE","PAGOS ELECTRONICOS","TRAMITES","INCUMPLIMIENTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","DEMORA EN LA ENTREGA DEL VEHICULO","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIA FACTURA","FALTA DE COMUNICACIÓN CON EL CLIENTE")'
        ),
      ),
    ),
);

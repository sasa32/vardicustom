<?php
$dependencies['Cases']['SetOption_tipo_NA_y_compania_DNisan'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"NA"),equal($cd_cia_c,"1"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","227","233")',
           'labels' => 'createList("","INVITACIÓN DE VITRINAS A NIVEL NACIONAL","ENCUESTAS")'
        ),
      ),
    ),
);

<?php
$dependencies['Cases']['SetOption27'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","160","202","188","1","201","153","19","4","116","35","123","130","210","34","84","85","134","128","209","113","124","208","17","125","44")',
           'labels' => 'createList("","MOTOR,TREN DE POTENCIA","SUSPENSION","SISTEMA HIDRAULICO","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","MANDOS","BOMBAS, CILINDROS","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","CALIDAD DE PRODUCTO","DEVOLUCION DE DINERO","DEVOLUCION SALDO A FAVOR","INFORMACION BRINDADA SOBRE PRODUCTO","INCUMPLIMIENTO EN LA ENTREGA","TRAMITES","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIAS EN PAGOS","TRAMITE FINANCIERO","AUTORIZACION DE USO DE DATOS PERSONALES","INCUMPLIMIENTO DE COMPROMISOS","CALIDAD EN EL ALISTAMIENTO")'
        ),
      ),
    ),
);

<?php
$dependencies['Cases']['TAL_RIA_NA_SOL_8'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"220"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","8","49","194","213","60","6")',
				'labels' => 'createList("","ACTUALIZACION DE DATOS","CAMBIO DE PROPIETARIO","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)")'
			),
		),
	),
);
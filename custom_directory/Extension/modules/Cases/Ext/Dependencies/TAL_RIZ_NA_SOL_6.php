<?php
$dependencies['Cases']['TAL_RIZ_NA_SOL_6'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","8","49","194","213","51","54","55","56","58","59","61","145","180","60","6","7","149","76")',
				'labels' => 'createList("","PAGOS PSE","ACTUALIZACION DE DATOS","CAMBIO DE PROPIETARIO","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","CANCELACIÓN CITA","CITA CAMPAÑA DE SEGURIDAD","CITA CAMPAÑA DE SERVICIO","CITA CARRO TALLER","CITA GARANTÍA","CITA REVISIÓN TALLER","CONFIRMACIÓN CITA TALLER","INGRESO POR COLISIÓN","REPROGRAMACIÓN CITA","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)","ACTIVACIÓN TASA","INVESTIGACIONES DE MERCADO","CURSO MECÁNICA")'
			),
		),
	),
);
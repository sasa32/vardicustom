<?php
$dependencies['Cases']['SetOption26'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","201","13","123","130","210","42","85","78","107","84","33","209","92","134","17","32","191")',
           'labels' => 'createList("","SUMINISTRO DE REPUESTOS","ALTO COSTO EN LOS REPUESTOS","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","CALIDAD DEL REPUESTO","DEVOLUCION SALDO A FAVOR","DEMORA EN EL SUMINISTRO DE REPUESTOS","EXPLICACION DE GARANTIA","DEVOLUCION DE DINERO","CALIDAD DE LOS ACCESORIOS","TRAMITES","DISPONIBILIDAD DE REPUESTOS","INFORMACION BRINDADA SOBRE PRODUCTO","AUTORIZACION DE USO DE DATOS PERSONALES","CALIDAD DE LLANTAS","SOLICITUD COPIA MANUAL DEL CONDUCTOR")'
        ),
      ),
    ),
);

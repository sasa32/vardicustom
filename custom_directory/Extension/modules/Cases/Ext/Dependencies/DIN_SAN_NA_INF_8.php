<?php
$dependencies['Cases']['DIN_SAN_NA_INF_8'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","141","99","101","184","90","103","9","177","164","164","164","136","144)',
				'labels' => 'createList("","INFORMACIÓN VEHICULO NUEVO","ESPECIFICACIONES VEHÍCULO NUEVO","ESTADO DEL NEGOCIO","SEGUROS MILENIO","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","PROPUESTA COMERCIAL","PAGOS PSE","PAGOS PSE","PAGOS PSE","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","INFORMACIÓN VITRINAS, CAMPAÑAS, ASESORES, LANZAMIENTOS Y RETOMAS")'
			),
		),
	),
);

<?php
$dependencies['Cases']['COM_NA_VEH_INF_3'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','cd_area_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","52","141","99","101","144","184","90","103","9","136","177","164","154","172")',
				'labels' => 'createList("","CHANGAN","INFORMACIÓN VEHICULO NUEVO","ESPECIFICACIONES VEHÍCULO NUEVO","ESTADO DEL NEGOCIO","INFORMACIÓN VITRINAS, CAMPAÑAS, ASESORES, LANZAMIENTOS Y RETOMAS","SEGUROS MILENIO","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","PROPUESTA COMERCIAL","PAGOS PSE","MANTENIMIENTO PREVENTIVO","PRECIOS RMP")'
			),
		),
	),
);
<?php
$dependencies['Cases']['Subunidad02'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"))',
   'triggerFields' => array('cd_cia_c','cd_uneg_cont_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'cd_area_c',
           'keys' => 'createList("","3144")',
           'labels' => 'createList("","VEHICULOS NUEVOS CHANGAN")'
        ),
      ),
    ),
);

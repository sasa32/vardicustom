<?php
$dependencies['Cases']['SetOption13'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","15","146","37","43","113","112","65","216","209","203","156","131","14","130","84","79","77","109","111","170","116","10","183","5","34","151","208","125","140","87")',
           'labels' => 'createList("","ATENCIÓN AL CLIENTE","INSTALACIONES","CALIDAD DE PRODUCTO POSTERIOR ENTREGA","CALIDAD DEL SERVICIO","FALTA DE CONOCIMIENTO DEL PRODUCTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DEL PUNTO DE VENTA","TRAMITES","TERMINOS DE GARANTIA","MERCADEO","INCUMPLIMIENTO FECHA DE ENTREGA"."ASESORIA E INFORMACION","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","DEVOLUCION DE DINERO","DEMORA EN LA DEVOLUCION DEL DINERO","DEMORA EN EL ENVIO DE INFORMACION","FALTA DE ACOMPANAMIENTO EN PROCESO COMPR","FALTA DE ASESORIA","PRECIO DE RETOMA","FALTANTES EN LA ENTREGA","AGILIDAD EN LA ATENCION AL CLIENTE","SEGUIMIENTO DESPUES DE LA ENTREGA","ACTITUD DEL PERSONAL DE SERVICIO","CALIDAD DE PRODUCTO","LIMPIEZA DE LA MAQUINA ","TRAMITE FINANCIERO","INCUMPLIMIENTO DE COMPROMISOS","INFORMACION SOBRE TRAMITES","DIFICULTAD EN RESPUESTA TELEFONICA")'
        ),
      ),
    ),
);

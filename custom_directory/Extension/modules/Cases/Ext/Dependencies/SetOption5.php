<?php
$dependencies['Cases']['SetOption5'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","132","189"."212","195","198","191","190","94","137","156","209","193","192","17")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","INF. HISTORIAL SERV. FACTURAS VENTA ETC","SOLICITUD COPIA DE DOCUMENTOS","TRASLADO DE SALDO","SOLICITUD DE FICHA TECNICA","SOLICITUD INFO TRASPASO VEHICULO RETOMA","SOLICITUD COPIA MANUAL DEL CONDUCTOR"."SOLICITUD COPIA MANUAL DE GARANTIAS","ELIMINACION DE LAS BASES DE DATOS","INFORMACION DESEMPENO Y FUNCIONAMIENTO","MERCADEO","TRAMITES","SOLICITUD DE COTIZACION","SOLICITUD DE CERTIFICACION","AUTORIZACION DE USO DE DATOS PERSONALES")'
        ),
      ),
    ),
);

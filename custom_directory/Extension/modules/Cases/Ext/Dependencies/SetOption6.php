<?php
$dependencies['Cases']['SetOption6'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","197","199","193","191","190","17","107","108")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","SOLICITUD DIAGNOSTICO DE PIEZAS","SOLICITUD INFORMACION DESEMPEÑO Y FUNCIO","SOLICITUD DE COTIZACION","SOLICITUD COPIA MANUAL DEL CONDUCTOR","SOLICITUD COPIA MANUAL DE GARANTIAS","AUTORIZACION DE USO DE DATOS PERSONALES","EXPLICACION DE GARANTIA","EXPLICACION TRAMITES")'
        ),
      ),
    ),
);

<?php
$dependencies['Cases']['SetOption_tipo_SOLICITUD_y_compania_DNisan_o_CAutomotriz'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"F"),equal($cd_cia_c,"3"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","228")',
           'labels' => 'createList("","INVITACIÓN A PERIODISTAS")'
        ),
      ),
    ),
);

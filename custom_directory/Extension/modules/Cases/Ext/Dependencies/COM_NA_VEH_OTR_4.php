<?php
$dependencies['Cases']['COM_NA_VEH_OTR_4'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"),equal($sasa_tipo_c,"G"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','cd_area_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","23","211")',
				'labels' => 'createList("","CAIDA LLAMADA","TRANSFERENCIA DE LLAMADA")'
			),
		),
	),
);
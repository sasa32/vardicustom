<?php
$dependencies['Cases']['SetOption16'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","15","146","42","112","65","215","107","205","2","92","29","84","79","78","5","10","111","183","113","168","140","66","87","185","186","209","156","208","3","125","196")',
           'labels' => 'createList("","ATENCION AL CLIENTE","INSTALACIONES","CALIDAD DEL REPUESTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DEL MOSTRADOR","EXPLICACION DE GARANTIA","TIEMPO DE SUMINISTRO DE RESPUESTOS","ACCESORIOS","DISPONIBILIDAD DE REPUESTOS","CALIDAD DE ACCESORIOS","DEVOLUCION DE DINERO","DEMORA EN LA DEVOLUCION DEL DINERO","DEMORA EN EL SUMINISTRO DE REPUESTOS","ACTITUD DEL PERSONAL DE SERVICIO","AGILIDAD EN LA ATENCION AL CLIENTE","FALTA DE ASESORIA","SEGUIMIENTO DESPUES DE LA ENTREGA","FALTA DE CONOCIMIENTO DEL PRODUCTO","POLITICAS FORMAS DE PAGO","INFORMACION SOBRE TRAMITES","COSTOS DE REPUESTOS","DIFICULTAD EN RESPUESTA TELEFONICA ","SERVICIO DE CAJA","SERVICIO DE CAJA","TRAMITES","MERCADEO","TRAMITE FINANCIERO","ACCESORIOS INSTALADOS","INCUMPLIMIENTO DE COMPROMISOS","SOLICITUD DE INFORMACION")'
        ),
      ),
    ),
);

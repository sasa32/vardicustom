<?php
$dependencies['Cases']['COM_DOS_NA_INF_9'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","100","142","101","144","90","103","9","136","164")',
				'labels' => 'createList("","ESPECIFICACIONES VEHÍCULO USADO","INFORMACIÓN VEHÍCULO USADO","ESTADO DEL NEGOCIO","INFORMACIÓN VITRINAS, CAMPAÑAS, ASESORES, LANZAMIENTOS Y RETOMAS","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES ","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","PAGOS PSE")'
			),
		),
	),
);
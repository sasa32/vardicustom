<?php
$dependencies['Cases']['DIN_TOS_NA_SOL_6'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","71","69","86","8","194","213","60","6","149")',
				'labels' => 'createList("","PAGOS PSE","COTIZACIÓN REPUESTOS","COTIZACIÓN ACCESORIOS","DEVOLUCIONES DE REPUESTOS","ACTUALIZACION DE DATOS","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)","INVESTIGACIONES DE MERCADO")'
			),
		),
	),
);
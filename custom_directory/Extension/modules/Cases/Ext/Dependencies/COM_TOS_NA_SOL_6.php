<?php
$dependencies['Cases']['COM_TOS_NA_SOL_6'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"130"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","8")',
				'labels' => 'createList("","ACTUALIZACION DE DATOS")'
			),
		),
	),
);
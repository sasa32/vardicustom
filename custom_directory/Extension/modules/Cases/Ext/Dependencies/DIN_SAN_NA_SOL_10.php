<?php
$dependencies['Cases']['DIN_SAN_NA_SOL_10'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","57","74","204","8","49","194","213","60","6","149")',
				'labels' => 'createList("","PAGOS PSE","CITA EN VITRINA","COTIZACIÓN VEHÍCULOS NUEVOS","TEST DRIVE","ACTUALIZACION DE DATOS","CAMBIO DE PROPIETARIO","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)","INVESTIGACIONES DE MERCADO")'
			),
		),
	),
);
<?php
$dependencies['Cases']['SetOption3'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","139","189")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","SOLICITUD COPIA DE DOCUMENTOS")'
        ),
      ),
    ),
);

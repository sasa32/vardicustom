<?php
$dependencies['Cases']['SetOption_tipo_NA'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_tipo_c,"NA")',
   'triggerFields' => array('sasa_tipo_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","221","222","226")',
           'labels' => 'createList("","PROTOCOLO VITRINA","PROTOCOLO TALLER","SEGUIMIENTO PQRFS A REPRESENTANTES")'
        ),
      ),
    ),
);

<?php
$dependencies['Cases']['SetOption1'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","212","191","190","94","137","156","189","209","192","17","195")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","TRASLADO DE SALDO","SOLICITUD COPIA MANUAL DEL CONDUCTOR","SOLICITUD COPIA MANUAL DE GARANTIAS","ELIMINACION DE LAS BASES DE DATOS","INFORMACION DESEMPENO Y FUNCIONAMIENTO","MERCADEO","SOLICITUD COPIA DE DOCUMENTOS","TRAMITES","SOLICITUD DE CERTIFICACION","AUTORIZACION DE USO DE DATOS PERSONALES","SOLICITUD DE FICHA TECNICA")'
        ),
      ),
    ),
);

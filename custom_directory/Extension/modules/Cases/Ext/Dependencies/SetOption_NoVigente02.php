<?php
$dependencies['Cases']['SetOption_NoVigente02'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"340"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","15","146","41","113","112","65","214","209","107","204","152","156","2","129","111","208","116","84","4","122","125","82","138","140","84","5","10","183")',
           'labels' => 'createList("","ATENCION AL CLIENTE","INSTALACIONES","CALIDAD DEL PRODUCTO POSTERIOR ENTREGA","FALTA DE CONOCIMIENTO DEL PRODUCTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DE LA SALA DE VENTAS","TRAMITES","EXPLICACION DE GARANTIA","TEST DRIVE","LIMPIEZA DEL VEHICULO","MERCADEO","ACCESORIOS","INCUMPLIMIENTO EN LA ENTREGA DEL VEHICUL","FALTA DE ASESORIA","TRAMITE FINANCIERO","FALTANTES EN LA ENTREGA","DEVOLUCION DE DINERO","ACCESORIOS NO INSTALADOS","INCONSISTENCIAS DURANTE LA ENTREGA","INCUMPLIMIENTO DE COMPROMISOS","DEMORA EN PROCESOS DE RETOMA","INFORMACION ERRADA DEL PRODUCTO","INFORMACION SOBRE TRAMITES","DEVOLUCION DE DINERO","ACTITUD DEL PERSONAL DE SERVICIO","AGILIDAD EN LA ATENCION AL CLIENTE","SEGUIMIENTO DESPUES DE LA ENTREGA")'
        ),
      ),
    ),
);

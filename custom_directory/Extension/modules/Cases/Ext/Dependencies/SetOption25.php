<?php
$dependencies['Cases']['SetOption25'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","201","4","116","34","123","130","210","84","209","80","35","85","134","128","29","212","113","124","208","17","125","44","187")',
           'labels' => 'createList("","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","DEVOLUCION DE DINERO","TRAMITES","DEMORA EN LA ENTREGA DEL VEHICULO","CALIDAD DE PRODUCTO EN LA ENTREGA","DEVOLUCION SALDO A FAVOR","INFORMACION BRINDADA SOBRE PRODUCTO","INCUMPLIMIENTO EN LA ENTREGA","CALIDAD DE ACCESORIOS","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIAS EN PAGOS","TRAMITE FINANCIERO","AUTORIZACION DE USO DE DATOS PERSONALES","INCUMPLIMIENTO DE COMPROMISOS","CALIDAD EN EL ALISTAMIENTO","SISTEMA DE NAVEGACION")'
        ),
      ),
    ),
);

<?php
$dependencies['Cases']['COM_NA_VEH_SOL_5'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"3144"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','cd_area_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","57","74","204","8","49","194","213","59","60","6","149")',
				'labels' => 'createList("","PAGOS PSE","CITA EN VITRINA","COTIZACIÓN VEHÍCULOS NUEVOS","TEST DRIVE","ACTUALIZACION DE DATOS","CAMBIO DE PROPIETARIO","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","CITA REVISIÓN TALLER","COMUNICADOS INTERNOS","ACTIVACION ( TRÁFICOS - EVENTOS - LANZAMIENTOS)","INVESTIGACIONES DE MERCADO")'
			),
		),
	),
);
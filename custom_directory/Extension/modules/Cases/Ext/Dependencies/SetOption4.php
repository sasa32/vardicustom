<?php
$dependencies['Cases']['SetOption4'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"130"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","92","17","189","191","212","94","209")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","DISPONIBILIDAD DE REPUESTOS","AUTORIZACION DE USO DE DATOS PERSONALES","SOLICITUD COPIA DE DOCUMENTOS","SOLICITUD COPIA MANUAL DEL CONDUCTOR","TRASLADO DE SALDO ","ELIMINACION DE LAS BASES DE DATOS","TRAMITES")'
        ),
      ),
    ),
);

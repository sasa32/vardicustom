<?php
$dependencies['Cases']['SetOption2'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"A"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($cd_area_c,"11012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","132","139","84","191","190","94","137","189","209")',
           'labels' => 'createList("","INF. HISTORIAL SERV. FACTURAS VENTA ETC","INFORMACION SOBRE PRODUCTOS Y SERVICIOS","DEVOLUCION DE DINERO","SOLICITUD COPIA MANUAL DEL CONDUCTOR","SOLICITUD COPIA MANUAL DE GARANTIAS","ELIMINACION DE LAS BASES DE DATOS","INFORMACION DESEMPENO Y FUNCIONAMIENTO","SOLICITUD COPIA DE DOCUMENTOS","TRAMITES")'
        ),
      ),
    ),
);

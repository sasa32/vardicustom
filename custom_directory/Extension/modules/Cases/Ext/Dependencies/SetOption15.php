<?php
$dependencies['Cases']['SetOption15'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"B"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"11012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","5","146","34","113","112","65","214","209","107","204","152","156","2","131","111","208","116","84","3","122","125","82","138","140","79","170","168","35","10","183")',
           'labels' => 'createList("","ACTITUD DEL PERSONAL DE SERVICIO","INSTALACIONES","CALIDAD DE PRODUCTO","FALTA DE CONOCIMIENTO DEL PRODUCTO","FALTA DE COMUNICACION CON EL CLIENTE","COSTOS DE LA REPARACION O SERVICIO","UBICACION DE LA SALA DE VENTAS","TRAMITES","EXPLICACION DE GARANTIA","TEST DRIVE","LIMPIEZA DEL VEHICULO","MERCADEO","ACCESORIOS","INCUMPLIMIENTO FECHA DE ENTREGA","FALTA DE ASESORIA","TRAMITE FINANCIERO","FALTANTES EN LA ENTREGA","DEVOLUCION DE DINERO","ACCESORIOS INSTALADOS","INCONSISTENCIAS DURANTE LA ENTREGA","INCUMPLIMIENTO DE COMPROMISOS","DEMORA EN PROCESOS DE RETOMA","INFORMACION ERRADA DEL PRODUCTO","INFORMACION SOBRE TRAMITES","DEMORA EN LA DEVOLUCION DEL DINERO","PRECIO DE RETOMA","POLITICAS FORMAS DE PAGO","CALIDAD DE PRODUCTO EN LA ENTREGA","AGILIDAD EN LA ATENCION AL CLIENTE","SEGUIMIENTO DESPUES DE LA ENTREGA")'
        ),
      ),
    ),
);

<?php
$dependencies['Cases']['SetOption_Sugerencia03'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"D"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"340"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","219")',
           'labels' => 'createList("","SUGERENCIA")'
        ),
      ),
    ),
);

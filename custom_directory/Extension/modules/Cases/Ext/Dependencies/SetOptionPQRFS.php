<?php
$dependencies['Cases']['SetOptionPQRFS'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_pqrf_c,"N"))',
   'triggerFields' => array('sasa_pqrf_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'source',
           'keys' => 'createList("","C","E","V","T","W","P")',
           'labels' => 'createList("","CARTA","EMAIL","FORMATO VERBAL","LLAMADA","SITIO WEB","WHATSAPP")'
        ),
      ),
    ),
);

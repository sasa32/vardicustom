<?php
$dependencies['Cases']['SetRequiredVisiContactLead'] = array(
    'hooks' => array("edit", "view", "save"),
    'trigger' => 'equal(related($accounts,"id"),"a2590d86-50f6-11eb-9d24-0286beac7abe")',
    'onload' => false,
    //'triggerFields' => array('account_name'),
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'leads_cases_1_name',
                'value' => 'equal(related($accounts,"id"),"a2590d86-50f6-11eb-9d24-0286beac7abe")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'leads_cases_1_name',
                'value' => 'equal(related($accounts,"id"),"a2590d86-50f6-11eb-9d24-0286beac7abe")',
            ),
        ),
    ),
    'notActions' => array(
        array(
            'name' => 'SetVisibility',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'leads_cases_1_name',
                'value' => false,
            ),
        ),
    )
);

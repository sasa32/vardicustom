<?php
$dependencies['Cases']['SetOption22'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"410"),equal($cd_area_c,"11012"))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","28","202","150","117","1","201","4","116","35","123","130","210","34","29","80","106","46","84","85","209","134","128")',
           'labels' => 'createList("","CAJA, TRANSMISION Y EMBRAGUE","SUSPENSION","INYECCION,ELECTRICIDAD Y ELECTRONICA","FRENOS","ACABADOS, LAMINA Y PINTURA","SUMINISTRO DE REPUESTOS","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","CALIDAD DE PRODUCTO","CALIDAD DE ACCESORIOS","DEMORA EN LA ENTREGA DEL VEHICULO","ESTATICA","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","DEVOLUCION SALDO A FAVOR","TRAMITES","INFORMACION BRINDADA SOBRE PRODUCTO","INCUMPLIMIENTO EN LA ENTREGA","EXPLICACION DE GARANTIA")'
        ),
      ),
    ),
);

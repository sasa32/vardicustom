<?php
$dependencies['Cases']['SetOption21'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"110"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","134","128","4","116","35","123","130","210","34","29","80","45","84","85","209","212","113","124","208","38","53","17","125","187","44","79")',
           'labels' => 'createList("","INFORMACION BRINDADA SOBRE PRODUCTO","INCUMPLIMIENTO EN LA ENTREGA","ACCESORIOS NO INSTALADOS","FALTANTES EN LA ENTREGA","CALIDAD DE PRODUCTO EN LA ENTREGA","INCONSISTENCIAS EN COSTOS","INCUMPLIMIENTO EN TERMINOS DEL NEGOCIO","TRAMITES NO AUTORIZADOS","CALIDAD DE PRODUCTO","CALIDAD DE ACCESORIOS","DEMORA EN LA ENTREGA DEL VEHICULO","CALIDAD EN LA INSTALACION DE ACCESORIOS","DEVOLUCION DE DINERO","DEVOLUCION SALDO A FAVOR","TRAMITES","TRASLADO DE SALDO","FALTA DE CONOCIMIENTO DEL PRODUCTO","INCONSISTENCIAS EN PAGOS","TRAMITE FINANCIERO","CALIDAD DE VIDRIOS","CINTURONES DE SEGURIDAD","AUTORIZACION DE USO DE DATOS PERSONALES","INCUMPLIMIENTO DE COMPROMISOS","SISTEMA DE NAVEGACION","CALIDAD EN EL ALISTAMIENTO","DEMORA EN LA DEVOLUCION DEL DINERO")'
        ),
      ),
    ),
);

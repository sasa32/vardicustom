<?php
$dependencies['Cases']['DIN_RIA_NA_INF_2'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"1"),equal($cd_uneg_cont_c,"120"),equal($sasa_tipo_c,"E"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","98","101","144","","103","9","136","155")',
				'labels' => 'createList("","ESPECIFICACIONES MAQUINARIA","ESTADO DEL NEGOCIO","INFORMACIÓN VITRINAS, CAMPAÑAS, ASESORES, LANZAMIENTOS Y RETOMAS","DILIGENCIAMIENTO Y PROCESO DE DATOS PERSONALES","ESTADO PQRFS","ADMINISTRATIVO","INFORMACIÓN DE UBICACIÓN DIRECCIÓN,TELEFONOS Y HORARIOS","MAQUINARIA")'
			),
		),
	),
);
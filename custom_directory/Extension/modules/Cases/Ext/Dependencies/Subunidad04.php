<?php
$dependencies['Cases']['Subunidad04'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($cd_cia_c,"2"),equal($cd_uneg_cont_c,"210"))',
   'triggerFields' => array('cd_cia_c','cd_uneg_cont_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'cd_area_c',
           'keys' => 'createList("","21012")',
           'labels' => 'createList("","TALLER BUSES Y CAMIONES")'
        ),
      ),
    ),
);

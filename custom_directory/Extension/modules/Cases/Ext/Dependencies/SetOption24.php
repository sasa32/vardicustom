<?php
$dependencies['Cases']['SetOption24'] = array(
   'hooks' => array("all"),
   'trigger' => 'and(equal($sasa_tipo_c,"C"),equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"315"),equal($cd_area_c,""))',
   'triggerFields' => array('sasa_tipo_c','cd_cia_c','cd_uneg_cont_c','cd_area_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_motivo_c',
           'keys' => 'createList("","209")',
           'labels' => 'createList("","TRAMITES")'
        ),
      ),
    ),
);

<?php
$dependencies['Cases']['COM_DOS_NA_SOL_11'] = array(
	'hooks' => array("all"),
	'trigger' => 'and(equal($cd_cia_c,"3"),equal($cd_uneg_cont_c,"310"),equal($sasa_tipo_c,"F"))',
	'triggerFields' => array('cd_cia_c','cd_uneg_cont_c','sasa_tipo_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'sasa_motivo_c',
				'keys' => 'createList("","164","57","73","204","8","194","213","60","149")',
				'labels' => 'createList("","PAGOS PSE","CITA EN VITRINA","COTIZACIÓN VEHÍCULO USADO","TEST DRIVE","ACTUALIZACION DE DATOS","SOLICITUD DE DOCUMENTOS","TRATAMIENTO DE DATOS","COMUNICADOS INTERNOS","INVESTIGACIONES DE MERCADO")'
			),
		),
	),
);
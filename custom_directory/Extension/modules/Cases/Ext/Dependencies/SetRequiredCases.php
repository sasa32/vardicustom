<?php
$dependencies['Cases']['SetRequiredCases'] = array(
	'hooks' => array("all"),
	'trigger' => 'not(equal($name,""))',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_tipificacion_de_casos_cases_1_name',
                'value' => 'true',
            ),
        ),
	),
    'notActions' => array(
        
    )
);

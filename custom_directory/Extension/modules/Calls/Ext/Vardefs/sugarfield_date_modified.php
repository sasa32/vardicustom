<?php
 // created: 2020-06-05 13:45:10
$dictionary['Call']['fields']['date_modified']['audited']=true;
$dictionary['Call']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Call']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_modified']['calculated']=false;
$dictionary['Call']['fields']['date_modified']['enable_range_search']='1';

 ?>
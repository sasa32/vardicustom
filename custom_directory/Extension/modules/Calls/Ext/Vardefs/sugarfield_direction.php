<?php
 // created: 2020-09-14 19:46:06
$dictionary['Call']['fields']['direction']['default']='Outbound';
$dictionary['Call']['fields']['direction']['audited']=true;
$dictionary['Call']['fields']['direction']['massupdate']=true;
$dictionary['Call']['fields']['direction']['hidemassupdate']=false;
$dictionary['Call']['fields']['direction']['comments']='Indicates whether call is inbound or outbound';
$dictionary['Call']['fields']['direction']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['direction']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['direction']['merge_filter']='disabled';
$dictionary['Call']['fields']['direction']['calculated']=false;
$dictionary['Call']['fields']['direction']['dependency']=false;
$dictionary['Call']['fields']['direction']['required']=true;

 ?>
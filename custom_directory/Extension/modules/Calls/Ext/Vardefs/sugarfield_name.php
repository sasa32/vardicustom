<?php
 // created: 2020-06-05 13:44:42
$dictionary['Call']['fields']['name']['audited']=true;
$dictionary['Call']['fields']['name']['massupdate']=false;
$dictionary['Call']['fields']['name']['comments']='Brief description of the call';
$dictionary['Call']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['name']['merge_filter']='disabled';
$dictionary['Call']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.41',
  'searchable' => true,
);
$dictionary['Call']['fields']['name']['calculated']=false;

 ?>
<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_CALLS_LEADS_FROM_CALLS_TITLE'] = 'Leads';
$mod_strings['LBL_CREATE_LEAD'] = 'Como Lead';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente y Prospecto';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccionar Cliente y Prospecto';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente y Prospecto';
$mod_strings['LBL_DIRECTION'] = 'Origen:';
$mod_strings['LBL_CALENDAR_START_DATE'] = 'Fecha de inicio';
$mod_strings['LBL_CALENDAR_END_DATE'] = 'Fecha de finalización';
$mod_strings['LBL_LEAD_CALLS_FROM_CALLS_TITLE'] = 'Leads';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nueva Cotización';

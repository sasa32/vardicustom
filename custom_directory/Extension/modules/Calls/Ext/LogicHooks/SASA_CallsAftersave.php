<?php
$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	1,

	//Label. A string value to identify the hook.
	'SASA_CallsAftersave',

	//The PHP file where your class is located.
	'custom/modules/Calls/SASA_CallsAftersave.php',

	//The class the method is in.
	'SASA_CallsAftersave',

	//The method to call.
	'after_save'
);

?>

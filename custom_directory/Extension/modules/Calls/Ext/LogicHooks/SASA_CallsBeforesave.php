<?php
$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	2,

	//Label. A string value to identify the hook.
	'CallsBeforesave',

	//The PHP file where your class is located.
	'custom/modules/Calls/SASA_CallsBeforesave.php',

	//The class the method is in.
	'SASA_CallsBeforesave',

	//The method to call.
	'before_save'
);

?>

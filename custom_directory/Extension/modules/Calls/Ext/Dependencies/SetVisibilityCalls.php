<?php
$dependencies['Calls']['SetVisibilityCalls'] = array(
	'hooks' => array("edit", "view"),
	'triggerFields' => array('status','sasa_tipo_de_llamada_c'),
	//'trigger' => 'equal($sasa_auto_contactacion_c,1)',
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetVisibility',
			//The parameters passed in will depend on the action type set in 'name'
			'params' => array(
				'target' => 'sasa_resultado_c',
				'value' => 'or(and(equal($status,"Held"),or(equal($sasa_tipo_de_llamada_c,"INFORMACION_COMERCIAL"),equal($sasa_tipo_de_llamada_c,"SE_GENERO_COTIZACION"),equal($sasa_tipo_de_llamada_c,"SE_GENERO_CITA_TALLER"))),and(equal($status,"reprogrammed"),or(equal($sasa_tipo_de_llamada_c,"INFORMACION_COMERCIAL"),equal($sasa_tipo_de_llamada_c,"SE_GENERO_COTIZACION"),equal($sasa_tipo_de_llamada_c,"SE_GENERO_CITA_TALLER"))))',
			),
		),
	),
	//Actions fire if the trigger is false. Optional.
	'notActions' => array(
		array(
			'name' => 'SetVisibility',
			'params' => array(
				'target' => 'sasa_resultado_c',
				'value' => 'false'
			)
		),
	)
);

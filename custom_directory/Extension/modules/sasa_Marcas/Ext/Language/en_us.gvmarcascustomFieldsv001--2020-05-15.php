<?php

  $mod_strings['LBL_SASA_MA_ANO_INGRE_MERCADO_C'] = 'Vendido por Nosotros';
  $mod_strings['LBL_SASA_CD_MARCA_C'] = 'Código Marca';
  $mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
  $mod_strings['LBL_NAME'] = 'Nombre de la Marca';
  $mod_strings['LBL_DATE_ENTERED'] = 'Fecha de creación';
  $mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de modificación';
 
  
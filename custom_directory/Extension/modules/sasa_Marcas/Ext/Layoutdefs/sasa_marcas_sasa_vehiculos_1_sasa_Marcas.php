<?php
 // created: 2020-05-15 23:47:02
$layout_defs["sasa_Marcas"]["subpanel_setup"]['sasa_marcas_sasa_vehiculos_1'] = array (
  'order' => 100,
  'module' => 'sasa_vehiculos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_VEHICULOS_TITLE',
  'get_subpanel_data' => 'sasa_marcas_sasa_vehiculos_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

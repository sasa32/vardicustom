<?php
 // created: 2022-11-25 17:20:57
$dictionary['Opportunity']['fields']['commit_stage']['audited']=false;
$dictionary['Opportunity']['fields']['commit_stage']['massupdate']=false;
$dictionary['Opportunity']['fields']['commit_stage']['options']='';
$dictionary['Opportunity']['fields']['commit_stage']['comments']='Forecast commit ranges: Include, Likely, Omit etc.';
$dictionary['Opportunity']['fields']['commit_stage']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['commit_stage']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['commit_stage']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['commit_stage']['reportable']=false;
$dictionary['Opportunity']['fields']['commit_stage']['dependency']=false;
$dictionary['Opportunity']['fields']['commit_stage']['studio']=true;
$dictionary['Opportunity']['fields']['commit_stage']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['commit_stage']['calculated']=false;
$dictionary['Opportunity']['fields']['commit_stage']['len']=100;
$dictionary['Opportunity']['fields']['commit_stage']['visibility_grid']=array (
);
$dictionary['Opportunity']['fields']['commit_stage']['formula']='';
$dictionary['Opportunity']['fields']['commit_stage']['related_fields']=array (
);

 ?>
<?php
 // created: 2022-12-29 20:35:31
$dictionary['Opportunity']['fields']['sasa_asesor_c']['labelValue']='Asesor';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_asesor_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_asesor_c']['readonly_formula']='';

 ?>
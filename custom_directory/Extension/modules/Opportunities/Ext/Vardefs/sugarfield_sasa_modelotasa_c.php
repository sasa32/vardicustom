<?php
 // created: 2023-02-06 22:01:17
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['labelValue']='Modelo TASA';
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_modelotasa_c']['readonly_formula']='';

 ?>
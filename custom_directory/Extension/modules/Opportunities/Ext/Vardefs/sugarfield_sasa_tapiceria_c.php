<?php
 // created: 2022-12-29 20:41:11
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['labelValue']='Tapicería';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_tapiceria_c']['readonly_formula']='';

 ?>
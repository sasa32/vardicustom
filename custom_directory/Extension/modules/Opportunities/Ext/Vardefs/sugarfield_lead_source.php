<?php
 // created: 2022-11-25 17:17:09
$dictionary['Opportunity']['fields']['lead_source']['len']=100;
$dictionary['Opportunity']['fields']['lead_source']['audited']=false;
$dictionary['Opportunity']['fields']['lead_source']['massupdate']=false;
$dictionary['Opportunity']['fields']['lead_source']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['lead_source']['comments']='Source of the opportunity';
$dictionary['Opportunity']['fields']['lead_source']['importable']='false';
$dictionary['Opportunity']['fields']['lead_source']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['lead_source']['reportable']=false;
$dictionary['Opportunity']['fields']['lead_source']['calculated']=false;
$dictionary['Opportunity']['fields']['lead_source']['dependency']=false;
$dictionary['Opportunity']['fields']['lead_source']['visibility_grid']=array (
);

 ?>
<?php
 // created: 2022-12-29 20:40:46
$dictionary['Opportunity']['fields']['sasa_color_c']['labelValue']='Color de Vehículo';
$dictionary['Opportunity']['fields']['sasa_color_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_color_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_color_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_color_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_color_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_color_c']['readonly_formula']='';

 ?>
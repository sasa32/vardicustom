<?php
 // created: 2022-12-29 20:40:19
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['labelValue']='Año Modelo';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_anomodelo_c']['readonly_formula']='';

 ?>
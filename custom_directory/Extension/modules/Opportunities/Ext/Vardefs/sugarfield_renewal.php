<?php
 // created: 2022-11-25 17:24:40
$dictionary['Opportunity']['fields']['renewal']['default']=false;
$dictionary['Opportunity']['fields']['renewal']['audited']=false;
$dictionary['Opportunity']['fields']['renewal']['massupdate']=false;
$dictionary['Opportunity']['fields']['renewal']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['renewal']['comments']='Indicates whether the opportunity is a renewal';
$dictionary['Opportunity']['fields']['renewal']['importable']='false';
$dictionary['Opportunity']['fields']['renewal']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['renewal']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['renewal']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['renewal']['reportable']=false;
$dictionary['Opportunity']['fields']['renewal']['unified_search']=false;
$dictionary['Opportunity']['fields']['renewal']['calculated']=false;

 ?>
<?php
 // created: 2022-11-25 17:16:33
$dictionary['Opportunity']['fields']['campaign_id']['name']='campaign_id';
$dictionary['Opportunity']['fields']['campaign_id']['comment']='Campaign that generated lead';
$dictionary['Opportunity']['fields']['campaign_id']['vname']='LBL_CAMPAIGN_ID';
$dictionary['Opportunity']['fields']['campaign_id']['rname']='id';
$dictionary['Opportunity']['fields']['campaign_id']['type']='id';
$dictionary['Opportunity']['fields']['campaign_id']['dbType']='id';
$dictionary['Opportunity']['fields']['campaign_id']['table']='campaigns';
$dictionary['Opportunity']['fields']['campaign_id']['isnull']=true;
$dictionary['Opportunity']['fields']['campaign_id']['module']='Campaigns';
$dictionary['Opportunity']['fields']['campaign_id']['reportable']=false;
$dictionary['Opportunity']['fields']['campaign_id']['massupdate']=false;
$dictionary['Opportunity']['fields']['campaign_id']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['campaign_id']['audited']=false;
$dictionary['Opportunity']['fields']['campaign_id']['importable']='true';

 ?>
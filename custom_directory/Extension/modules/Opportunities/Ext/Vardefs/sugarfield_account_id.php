<?php
 // created: 2022-11-25 16:42:25
$dictionary['Opportunity']['fields']['account_id']['name']='account_id';
$dictionary['Opportunity']['fields']['account_id']['vname']='LBL_ACCOUNT_ID';
$dictionary['Opportunity']['fields']['account_id']['id_name']='account_id';
$dictionary['Opportunity']['fields']['account_id']['type']='relate';
$dictionary['Opportunity']['fields']['account_id']['link']='accounts';
$dictionary['Opportunity']['fields']['account_id']['rname']='id';
$dictionary['Opportunity']['fields']['account_id']['source']='non-db';
$dictionary['Opportunity']['fields']['account_id']['audited']=false;
$dictionary['Opportunity']['fields']['account_id']['dbType']='id';
$dictionary['Opportunity']['fields']['account_id']['module']='Accounts';
$dictionary['Opportunity']['fields']['account_id']['massupdate']=false;
$dictionary['Opportunity']['fields']['account_id']['importable']='required';

 ?>
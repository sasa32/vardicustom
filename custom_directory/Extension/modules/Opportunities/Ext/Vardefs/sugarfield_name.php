<?php
 // created: 2022-12-29 20:29:56
$dictionary['Opportunity']['fields']['name']['audited']=false;
$dictionary['Opportunity']['fields']['name']['massupdate']=false;
$dictionary['Opportunity']['fields']['name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['name']['comments']='Name of the opportunity';
$dictionary['Opportunity']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.65',
  'searchable' => true,
);
$dictionary['Opportunity']['fields']['name']['calculated']=false;

 ?>
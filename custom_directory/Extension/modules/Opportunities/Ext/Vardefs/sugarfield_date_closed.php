<?php
 // created: 2023-02-01 22:41:23
$dictionary['Opportunity']['fields']['date_closed']['required']=false;
$dictionary['Opportunity']['fields']['date_closed']['audited']=false;
$dictionary['Opportunity']['fields']['date_closed']['massupdate']=false;
$dictionary['Opportunity']['fields']['date_closed']['comments']='Expected or actual date the oppportunity will close';
$dictionary['Opportunity']['fields']['date_closed']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['date_closed']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['date_closed']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['date_closed']['calculated']='1';
$dictionary['Opportunity']['fields']['date_closed']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['date_closed']['importable']='false';
$dictionary['Opportunity']['fields']['date_closed']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['date_closed']['formula']='addDays($date_entered,$sasa_vigencia_c)';
$dictionary['Opportunity']['fields']['date_closed']['enforced']=true;
$dictionary['Opportunity']['fields']['date_closed']['options']='';
$dictionary['Opportunity']['fields']['date_closed']['full_text_search']=array (
);
$dictionary['Opportunity']['fields']['date_closed']['related_fields']=array (
);

 ?>
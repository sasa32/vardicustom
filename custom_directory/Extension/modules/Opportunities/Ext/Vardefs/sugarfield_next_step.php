<?php
 // created: 2022-11-25 17:29:48
$dictionary['Opportunity']['fields']['next_step']['audited']=false;
$dictionary['Opportunity']['fields']['next_step']['massupdate']=false;
$dictionary['Opportunity']['fields']['next_step']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['next_step']['comments']='The next step in the sales process';
$dictionary['Opportunity']['fields']['next_step']['importable']='false';
$dictionary['Opportunity']['fields']['next_step']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['next_step']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['next_step']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['next_step']['reportable']=false;
$dictionary['Opportunity']['fields']['next_step']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '0.74',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['next_step']['calculated']=false;

 ?>
<?php
 // created: 2022-11-23 16:17:14
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['name']='sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['type']='id';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['source']='non-db';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['vname']='LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['id_name']='sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['link']='sasa_puntos_de_ventas_opportunities_1';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['table']='sasa_puntos_de_ventas';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['module']='sasa_Puntos_de_Ventas';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['rname']='id';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['side']='right';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['hideacl']=true;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida']['importable']='true';

 ?>
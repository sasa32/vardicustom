<?php
 // created: 2023-02-07 20:45:04
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['labelValue']='URL Cotización Sigru';
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_cotsigru_c']['readonly_formula']='';

 ?>
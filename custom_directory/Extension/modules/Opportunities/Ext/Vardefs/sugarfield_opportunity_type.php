<?php
 // created: 2022-11-25 17:15:43
$dictionary['Opportunity']['fields']['opportunity_type']['len']=100;
$dictionary['Opportunity']['fields']['opportunity_type']['audited']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['massupdate']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['comments']='Type of opportunity (ex: Existing, New)';
$dictionary['Opportunity']['fields']['opportunity_type']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['opportunity_type']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['opportunity_type']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['opportunity_type']['reportable']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['calculated']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['dependency']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['visibility_grid']=array (
);
$dictionary['Opportunity']['fields']['opportunity_type']['importable']='false';

 ?>
<?php
 // created: 2023-01-25 23:31:31
$dictionary['Opportunity']['fields']['sasa_iva_c']['labelValue']='IVA';
$dictionary['Opportunity']['fields']['sasa_iva_c']['formula']='related($revenuelineitems,"sasa_iva_c")';
$dictionary['Opportunity']['fields']['sasa_iva_c']['enforced']='false';
$dictionary['Opportunity']['fields']['sasa_iva_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_iva_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Opportunity']['fields']['sasa_iva_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_iva_c']['readonly_formula']='';

 ?>
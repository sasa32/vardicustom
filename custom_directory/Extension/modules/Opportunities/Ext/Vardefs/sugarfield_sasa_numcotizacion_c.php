<?php
 // created: 2022-12-29 20:36:38
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['labelValue']='Número de la cotización';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_numcotizacion_c']['readonly_formula']='';

 ?>
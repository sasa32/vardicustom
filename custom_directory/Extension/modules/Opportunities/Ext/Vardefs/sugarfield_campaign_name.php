<?php
 // created: 2022-11-25 17:16:33
$dictionary['Opportunity']['fields']['campaign_name']['audited']=false;
$dictionary['Opportunity']['fields']['campaign_name']['massupdate']=false;
$dictionary['Opportunity']['fields']['campaign_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['campaign_name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['campaign_name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['campaign_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['campaign_name']['reportable']=false;
$dictionary['Opportunity']['fields']['campaign_name']['calculated']=false;
$dictionary['Opportunity']['fields']['campaign_name']['related_fields']=array (
  0 => 'campaign_id',
);

 ?>
<?php
 // created: 2022-11-25 17:32:00
$dictionary['Opportunity']['fields']['service_start_date']['audited']=false;
$dictionary['Opportunity']['fields']['service_start_date']['massupdate']=false;
$dictionary['Opportunity']['fields']['service_start_date']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['service_start_date']['comments']='Service start date field.';
$dictionary['Opportunity']['fields']['service_start_date']['importable']='true';
$dictionary['Opportunity']['fields']['service_start_date']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['service_start_date']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['service_start_date']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['service_start_date']['calculated']=false;
$dictionary['Opportunity']['fields']['service_start_date']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['service_start_date']['reportable']=false;
$dictionary['Opportunity']['fields']['service_start_date']['related_fields']=array (
);

 ?>
<?php
 // created: 2022-11-23 16:18:26
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['name']='sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['type']='id';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['source']='non-db';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['vname']='LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['id_name']='sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['link']='sasa_unidad_de_negocio_opportunities_1';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['table']='sasa_unidad_de_negocio';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['module']='sasa_Unidad_de_Negocio';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['rname']='id';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['side']='right';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['hideacl']=true;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida']['importable']='true';

 ?>
<?php
 // created: 2022-12-29 20:45:02
$dictionary['Opportunity']['fields']['sasa_chasis_c']['labelValue']='Chasis del Vehículo';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_chasis_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_chasis_c']['readonly_formula']='';

 ?>
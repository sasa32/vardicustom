<?php
 // created: 2022-11-25 17:19:54
$dictionary['Opportunity']['fields']['best_case']['audited']=false;
$dictionary['Opportunity']['fields']['best_case']['massupdate']=false;
$dictionary['Opportunity']['fields']['best_case']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['best_case']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['best_case']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['best_case']['calculated']='1';
$dictionary['Opportunity']['fields']['best_case']['formula']='rollupConditionalSum($revenuelineitems, "best_case", "sales_stage", forecastSalesStages(true, false))';
$dictionary['Opportunity']['fields']['best_case']['enforced']=true;
$dictionary['Opportunity']['fields']['best_case']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['best_case']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['best_case']['readonly']=true;
$dictionary['Opportunity']['fields']['best_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Opportunity']['fields']['best_case']['len']=26;
$dictionary['Opportunity']['fields']['best_case']['importable']='false';
$dictionary['Opportunity']['fields']['best_case']['reportable']=false;

 ?>
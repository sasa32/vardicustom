<?php
 // created: 2022-11-23 16:08:27
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['name']='sasa_vehiculos_opportunities_1sasa_vehiculos_ida';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['type']='id';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['source']='non-db';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['vname']='LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['id_name']='sasa_vehiculos_opportunities_1sasa_vehiculos_ida';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['link']='sasa_vehiculos_opportunities_1';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['table']='sasa_vehiculos';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['module']='sasa_vehiculos';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['rname']='id';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['side']='right';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['hideacl']=true;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1sasa_vehiculos_ida']['importable']='true';

 ?>
<?php
 // created: 2022-12-29 20:47:32
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['labelValue']='Número del pedido';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_numeropedido_c']['readonly_formula']='';

 ?>
<?php
 // created: 2023-03-08 22:09:59
$dictionary['Opportunity']['fields']['sasa_unidades_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['sasa_unidades_c']['labelValue']='Cantidad de unidades Cotizadas';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_unidades_c']['calculated']='1';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['formula']='rollupSum($revenuelineitems,"quantity")';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['enforced']='1';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_unidades_c']['readonly_formula']='';

 ?>
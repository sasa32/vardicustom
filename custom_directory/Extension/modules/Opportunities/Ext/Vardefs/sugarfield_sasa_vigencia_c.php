<?php
 // created: 2023-03-15 14:07:18
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['labelValue']='Días de vigencia';
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_vigencia_c']['required_formula']='';

 ?>
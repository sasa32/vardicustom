<?php
 // created: 2022-11-25 17:22:58
$dictionary['Opportunity']['fields']['renewal_parent_id']['name']='renewal_parent_id';
$dictionary['Opportunity']['fields']['renewal_parent_id']['vname']='LBL_PARENT_RENEWAL_OPPORTUNITY_ID';
$dictionary['Opportunity']['fields']['renewal_parent_id']['type']='id';
$dictionary['Opportunity']['fields']['renewal_parent_id']['required']=false;
$dictionary['Opportunity']['fields']['renewal_parent_id']['reportable']=false;
$dictionary['Opportunity']['fields']['renewal_parent_id']['audited']=false;
$dictionary['Opportunity']['fields']['renewal_parent_id']['importable']='false';

 ?>
<?php
 // created: 2023-01-25 23:33:36
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['labelValue']='Subtotal';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['formula']='related($revenuelineitems,"subtotal")';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['enforced']='false';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_subtotal_c']['readonly_formula']='';

 ?>
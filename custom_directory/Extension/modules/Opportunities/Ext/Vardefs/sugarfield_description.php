<?php
 // created: 2022-12-29 20:31:24
$dictionary['Opportunity']['fields']['description']['audited']=false;
$dictionary['Opportunity']['fields']['description']['massupdate']=false;
$dictionary['Opportunity']['fields']['description']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['description']['comments']='Full text of the note';
$dictionary['Opportunity']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['description']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.59',
  'searchable' => true,
);
$dictionary['Opportunity']['fields']['description']['calculated']=false;
$dictionary['Opportunity']['fields']['description']['readonly']=true;
$dictionary['Opportunity']['fields']['description']['rows']='6';
$dictionary['Opportunity']['fields']['description']['cols']='80';

 ?>
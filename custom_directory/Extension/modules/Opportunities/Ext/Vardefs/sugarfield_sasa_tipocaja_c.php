<?php
 // created: 2023-03-08 20:47:02
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['labelValue']='Tipo de caja';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_tipocaja_c']['readonly_formula']='';

 ?>
<?php
 // created: 2022-11-25 17:16:02
$dictionary['Opportunity']['fields']['deleted']['default']=false;
$dictionary['Opportunity']['fields']['deleted']['audited']=false;
$dictionary['Opportunity']['fields']['deleted']['massupdate']=false;
$dictionary['Opportunity']['fields']['deleted']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['deleted']['comments']='Record deletion indicator';
$dictionary['Opportunity']['fields']['deleted']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['deleted']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['deleted']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['deleted']['unified_search']=false;
$dictionary['Opportunity']['fields']['deleted']['calculated']=false;
$dictionary['Opportunity']['fields']['deleted']['importable']='false';

 ?>
<?php
 // created: 2022-11-25 17:22:58
$dictionary['Opportunity']['fields']['renewal_parent_name']['audited']=false;
$dictionary['Opportunity']['fields']['renewal_parent_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['renewal_parent_name']['importable']='false';
$dictionary['Opportunity']['fields']['renewal_parent_name']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['renewal_parent_name']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['renewal_parent_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['renewal_parent_name']['reportable']=false;
$dictionary['Opportunity']['fields']['renewal_parent_name']['calculated']=false;

 ?>
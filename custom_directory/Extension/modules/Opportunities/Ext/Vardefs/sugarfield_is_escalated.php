<?php
 // created: 2022-11-25 17:26:56
$dictionary['Opportunity']['fields']['is_escalated']['default']=false;
$dictionary['Opportunity']['fields']['is_escalated']['audited']=false;
$dictionary['Opportunity']['fields']['is_escalated']['massupdate']=true;
$dictionary['Opportunity']['fields']['is_escalated']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['is_escalated']['comments']='Is this escalated?';
$dictionary['Opportunity']['fields']['is_escalated']['importable']='false';
$dictionary['Opportunity']['fields']['is_escalated']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['is_escalated']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['is_escalated']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['is_escalated']['reportable']=false;
$dictionary['Opportunity']['fields']['is_escalated']['unified_search']=false;
$dictionary['Opportunity']['fields']['is_escalated']['calculated']=false;

 ?>
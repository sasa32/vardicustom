<?php
 // created: 2022-12-29 20:42:31
$dictionary['Opportunity']['fields']['sasa_modelo_c']['labelValue']='Modelo comercial';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_modelo_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_modelo_c']['readonly_formula']='';

 ?>
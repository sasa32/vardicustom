<?php
 // created: 2023-03-15 14:04:45
$dictionary['Opportunity']['fields']['sasa_termino2_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['sasa_termino2_c']['labelValue']='sasa termino2 c';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_termino2_c']['calculated']='true';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['formula']='daysUntil(addDays($date_entered,$sasa_vigencia_c))';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['enforced']='true';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_termino2_c']['readonly_formula']='';

 ?>
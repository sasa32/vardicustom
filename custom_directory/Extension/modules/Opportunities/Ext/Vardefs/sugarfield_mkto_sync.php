<?php
 // created: 2022-11-25 17:23:34
$dictionary['Opportunity']['fields']['mkto_sync']['default']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['audited']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['massupdate']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['comments']='Should the Lead be synced to Marketo';
$dictionary['Opportunity']['fields']['mkto_sync']['importable']='false';
$dictionary['Opportunity']['fields']['mkto_sync']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['mkto_sync']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['mkto_sync']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['mkto_sync']['reportable']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['unified_search']=false;
$dictionary['Opportunity']['fields']['mkto_sync']['calculated']=false;

 ?>
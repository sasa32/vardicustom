<?php
 // created: 2022-11-25 17:17:41
$dictionary['Opportunity']['fields']['amount_usdollar']['audited']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['massupdate']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['comments']='Formatted amount of the opportunity';
$dictionary['Opportunity']['fields']['amount_usdollar']['importable']='false';
$dictionary['Opportunity']['fields']['amount_usdollar']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['amount_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['amount_usdollar']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['amount_usdollar']['reportable']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['calculated']='1';
$dictionary['Opportunity']['fields']['amount_usdollar']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['amount_usdollar']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
<?php
 // created: 2022-12-29 20:35:55
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['labelValue']='No. documento del asesor';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['enforced']='';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['dependency']='';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['required_formula']='';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['readonly']='1';
$dictionary['Opportunity']['fields']['sasa_numasesor_c']['readonly_formula']='';

 ?>
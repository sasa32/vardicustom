<?php
 // created: 2022-11-25 17:26:23
$dictionary['Opportunity']['fields']['base_rate']['audited']=false;
$dictionary['Opportunity']['fields']['base_rate']['massupdate']=false;
$dictionary['Opportunity']['fields']['base_rate']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['base_rate']['importable']='false';
$dictionary['Opportunity']['fields']['base_rate']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['base_rate']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['base_rate']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['base_rate']['reportable']=false;
$dictionary['Opportunity']['fields']['base_rate']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['base_rate']['calculated']=false;
$dictionary['Opportunity']['fields']['base_rate']['readonly']=false;
$dictionary['Opportunity']['fields']['base_rate']['rows']='4';
$dictionary['Opportunity']['fields']['base_rate']['cols']='20';

 ?>
<?php
 // created: 2022-11-25 16:42:25
$dictionary['Opportunity']['fields']['account_name']['len']=255;
$dictionary['Opportunity']['fields']['account_name']['required']=false;
$dictionary['Opportunity']['fields']['account_name']['audited']=true;
$dictionary['Opportunity']['fields']['account_name']['massupdate']=false;
$dictionary['Opportunity']['fields']['account_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['account_name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['account_name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['account_name']['reportable']=false;
$dictionary['Opportunity']['fields']['account_name']['calculated']=false;
$dictionary['Opportunity']['fields']['account_name']['related_fields']=array (
  0 => 'account_id',
);

 ?>
<?php
 // created: 2022-11-23 16:08:28
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['audited']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['massupdate']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['reportable']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['calculated']=false;
$dictionary['Opportunity']['fields']['sasa_vehiculos_opportunities_1_name']['vname']='LBL_SASA_VEHICULOS_OPPORTUNITIES_1_NAME_FIELD_TITLE';

 ?>
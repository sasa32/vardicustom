<?php
 // created: 2022-11-25 17:25:13
$dictionary['Opportunity']['fields']['sync_key']['audited']=false;
$dictionary['Opportunity']['fields']['sync_key']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['sync_key']['importable']='false';
$dictionary['Opportunity']['fields']['sync_key']['reportable']=false;
$dictionary['Opportunity']['fields']['sync_key']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 ?>
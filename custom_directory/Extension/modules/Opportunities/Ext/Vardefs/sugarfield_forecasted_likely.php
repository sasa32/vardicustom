<?php
 // created: 2022-11-25 17:21:22
$dictionary['Opportunity']['fields']['forecasted_likely']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['forecasted_likely']['options']='';
$dictionary['Opportunity']['fields']['forecasted_likely']['comments']='Rollup of included RLIs on the Opportunity';
$dictionary['Opportunity']['fields']['forecasted_likely']['importable']='false';
$dictionary['Opportunity']['fields']['forecasted_likely']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['forecasted_likely']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['forecasted_likely']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['forecasted_likely']['reportable']=false;
$dictionary['Opportunity']['fields']['forecasted_likely']['calculated']='1';
$dictionary['Opportunity']['fields']['forecasted_likely']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['forecasted_likely']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Compañía';
$mod_strings['LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE'] = 'Unidades de Negocio';
$mod_strings['LBL_SASA_COMPANIAS_FOCUS_DRAWER_DASHBOARD'] = 'Compañías Panel de enfoque';
$mod_strings['LBL_SASA_COMPANIAS_RECORD_DASHBOARD'] = 'Compañias Cuadro de mando del registro';
$mod_strings['LNK_NEW_RECORD'] = 'Crear Compañía';
$mod_strings['LNK_LIST'] = 'Vista Compañías';
$mod_strings['LBL_MODULE_NAME'] = 'Compañías';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Compañía';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Compañía';
$mod_strings['LNK_IMPORT_VCARD'] = 'Importar Compañía vCard';
$mod_strings['LNK_IMPORT_SASA_COMPANIAS'] = 'Importar Compañías';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Compañías Lista';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Buscar Compañía';
$mod_strings['LBL_SASA_COMPANIAS_SUBPANEL_TITLE'] = 'Compañías';

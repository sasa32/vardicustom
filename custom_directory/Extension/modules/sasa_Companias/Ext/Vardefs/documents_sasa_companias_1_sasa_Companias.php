<?php
// created: 2020-07-29 16:42:01
$dictionary["sasa_Companias"]["fields"]["documents_sasa_companias_1"] = array (
  'name' => 'documents_sasa_companias_1',
  'type' => 'link',
  'relationship' => 'documents_sasa_companias_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'documents_sasa_companias_1documents_ida',
);

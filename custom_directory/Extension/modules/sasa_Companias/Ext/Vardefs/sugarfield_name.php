<?php
 // created: 2020-05-16 20:30:06
$dictionary['sasa_Companias']['fields']['name']['len']='255';
$dictionary['sasa_Companias']['fields']['name']['audited']=false;
$dictionary['sasa_Companias']['fields']['name']['massupdate']=false;
$dictionary['sasa_Companias']['fields']['name']['unified_search']=false;
$dictionary['sasa_Companias']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_Companias']['fields']['name']['calculated']=false;

 ?>
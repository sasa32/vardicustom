<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_CONTACT_ID'] = 'ID Contacto:';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_LIST_CONTACT'] = 'Contacto';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'ID de Opportunity:';
$mod_strings['LBL_CASE_ID'] = 'ID de Case:';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Note o Adjunto';
$mod_strings['LBL_MODULE_NAME'] = 'Notes';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Note';
$mod_strings['LBL_NEW_FORM_BTN'] = 'Añadir una Note';
$mod_strings['LNK_NOTE_LIST'] = 'Ver Notes';
$mod_strings['LNK_IMPORT_NOTES'] = 'Importar Notes';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Notes';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Notes';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notes y Adjuntos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Notes';
$mod_strings['LBL_MODULE_TITLE'] = 'Notes: Inicio';
$mod_strings['LBL_NOTE_STATUS'] = 'Note';
$mod_strings['LBL_NOTE'] = 'Note:';
$mod_strings['LBL_LEAD_ID'] = 'ID Lead:';
$mod_strings['LBL_QUOTE_ID'] = 'ID Quote:';
$mod_strings['LBL_DESCRIPTION'] = 'Descripción';
$mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
$mod_strings['LBL_RELATED_TO'] = 'Relacionado Con:';
$mod_strings['LBL_SASA_USUARIO_AVANCE_C'] = 'Usuario Avance';
$mod_strings['LBL_SASA_SOPORTE_ADJUNTO_C'] = 'Soporte Adjunto (URL)';

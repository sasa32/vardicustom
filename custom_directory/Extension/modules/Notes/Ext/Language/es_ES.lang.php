<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente y Prospecto:';
$mod_strings['LBL_CONTACT_ID'] = 'ID Contacto:';
$mod_strings['LBL_LIST_CONTACT_NAME'] = 'Contacto';
$mod_strings['LBL_LIST_CONTACT'] = 'Contacto';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'ID de Cotización:';
$mod_strings['LBL_CASE_ID'] = 'ID de Caso:';
$mod_strings['LNK_NEW_NOTE'] = 'Nueva Nota o Adjunto';
$mod_strings['LBL_MODULE_NAME'] = 'Notas';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Nota';
$mod_strings['LBL_NEW_FORM_BTN'] = 'Añadir una Nota';
$mod_strings['LNK_NOTE_LIST'] = 'Ver Notas';
$mod_strings['LNK_IMPORT_NOTES'] = 'Importar Notas';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Notas';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Notas';
$mod_strings['LBL_NOTES_SUBPANEL_TITLE'] = 'Notas y Adjuntos';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Notas';
$mod_strings['LBL_MODULE_TITLE'] = 'Notas: Inicio';
$mod_strings['LBL_NOTE_STATUS'] = 'Nota';
$mod_strings['LBL_NOTE'] = 'Nota:';
$mod_strings['LBL_LEAD_ID'] = 'ID Lead:';
$mod_strings['LBL_QUOTE_ID'] = 'ID Presupuesto:';
$mod_strings['LBL_DESCRIPTION'] = 'Descripción';
$mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación';
$mod_strings['LBL_RELATED_TO'] = 'Relacionado Con:';
$mod_strings['LBL_SASA_USUARIO_AVANCE_C'] = 'Usuario Avance';
$mod_strings['LBL_SASA_SOPORTE_ADJUNTO_C'] = 'Soporte Adjunto (URL)';

<?php

$mod_strings['LBL_SASA_USUARIO_AVANCE_C'] = 'Usuario avance';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_TIPO_DE_NOTA_C'] = 'Tipo de Nota';
$mod_strings['LBL_SASA_SOPORTE_ADJUNTO_C'] = 'Soporte Adjunto (URL)';
$mod_strings['LBL_SASA_ID_AVAN_C'] = 'Tipo Avance';
$mod_strings['LBL_SASA_ID_TIPO_C'] = 'Tipo Comentario';
$mod_strings['LBL_SASA_PLANNOTIFICARASESOR_C'] = 'Plantilla de notificación asesor';
$mod_strings['LBL_SASA_GESTIONADO_C'] = 'Gestión de Nota';

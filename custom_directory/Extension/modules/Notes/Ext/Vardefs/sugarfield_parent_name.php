<?php
 // created: 2020-10-19 13:46:54
$dictionary['Note']['fields']['parent_name']['audited']=false;
$dictionary['Note']['fields']['parent_name']['massupdate']=false;
$dictionary['Note']['fields']['parent_name']['options']='parent_type_display';
$dictionary['Note']['fields']['parent_name']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['parent_name']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['Note']['fields']['parent_name']['calculated']=false;

 ?>
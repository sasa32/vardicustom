<?php
 // created: 2020-10-19 13:46:54
$dictionary['Note']['fields']['parent_type']['audited']=false;
$dictionary['Note']['fields']['parent_type']['massupdate']=false;
$dictionary['Note']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['parent_type']['duplicate_merge_dom_value']=1;
$dictionary['Note']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Note']['fields']['parent_type']['calculated']=false;
$dictionary['Note']['fields']['parent_type']['len']=255;
$dictionary['Note']['fields']['parent_type']['options']='';

 ?>
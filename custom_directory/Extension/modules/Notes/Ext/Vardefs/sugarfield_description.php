<?php
 // created: 2020-09-10 15:42:59
$dictionary['Note']['fields']['description']['audited']=false;
$dictionary['Note']['fields']['description']['massupdate']=false;
$dictionary['Note']['fields']['description']['comments']='Full text of the note';
$dictionary['Note']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['description']['merge_filter']='disabled';
$dictionary['Note']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.44',
  'searchable' => true,
);
$dictionary['Note']['fields']['description']['calculated']=false;
$dictionary['Note']['fields']['description']['rows']='30';
$dictionary['Note']['fields']['description']['cols']='90';

 ?>
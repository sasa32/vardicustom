<?php
 // created: 2020-10-19 13:46:54
$dictionary['Note']['fields']['parent_id']['audited']=false;
$dictionary['Note']['fields']['parent_id']['massupdate']=false;
$dictionary['Note']['fields']['parent_id']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['parent_id']['duplicate_merge_dom_value']=1;
$dictionary['Note']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['Note']['fields']['parent_id']['calculated']=false;
$dictionary['Note']['fields']['parent_id']['len']=255;
$dictionary['Note']['fields']['parent_id']['unified_search']=false;

 ?>
<?php
 // created: 2020-10-19 13:46:26
$dictionary['Note']['fields']['date_entered']['audited']=false;
$dictionary['Note']['fields']['date_entered']['comments']='Date record created';
$dictionary['Note']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Note']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Note']['fields']['date_entered']['calculated']=false;
$dictionary['Note']['fields']['date_entered']['enable_range_search']='1';

 ?>
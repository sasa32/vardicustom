<?php
$mod_strings['LBL_SASA_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SASA_CODPUNTOATENCION_C'] = 'Código Punto Atención';
$mod_strings['LBL_SASA_ESTADO_C'] = 'Estado';
$mod_strings['LBL_SASA_FUENTE_C'] = 'Fuente';
$mod_strings['LBL_SASA_GERENTEDIRECTOR_C'] = 'Gerente/Director';
$mod_strings['LBL_SASA_CODSUCURSAL_C'] = 'Código Sucursal';
$mod_strings['LBL_SASA_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Número de Teléfono';
$mod_strings['LBL_SASA_DIRECCION_C'] = 'Dirección';
$mod_strings['LBL_SASA_PROGRAMAR_LLAMADA_C'] = 'Programar llamada';
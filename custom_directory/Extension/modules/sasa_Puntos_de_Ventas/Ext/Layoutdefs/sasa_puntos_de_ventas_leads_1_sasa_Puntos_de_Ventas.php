<?php
 // created: 2021-01-27 16:50:54
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

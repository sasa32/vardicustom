<?php
// created: 2022-11-23 15:55:57
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_opportunities_1"] = array (
  'name' => 'sasa_puntos_de_ventas_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_OPPORTUNITIES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_opportunities_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

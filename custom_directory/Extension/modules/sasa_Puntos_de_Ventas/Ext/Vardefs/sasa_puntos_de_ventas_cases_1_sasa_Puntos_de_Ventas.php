<?php
// created: 2021-03-09 21:41:14
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_cases_1"] = array (
  'name' => 'sasa_puntos_de_ventas_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

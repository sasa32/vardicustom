<?php
 // created: 2020-05-16 20:27:40
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['audited']=false;
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['massupdate']=false;
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['comments']='Full text of the note';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['duplicate_merge']='enabled';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['merge_filter']='disabled';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['unified_search']=false;
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '0.5',
  'searchable' => false,
);
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['calculated']=false;
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['rows']='6';
$dictionary['sasa_Puntos_de_Ventas']['fields']['description']['cols']='80';

 ?>
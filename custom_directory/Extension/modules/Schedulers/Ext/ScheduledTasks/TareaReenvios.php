<?php

$job_strings[] = 'TareaReenvios';
function TareaReenvios()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaReenvios. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;
    //Query para identificar los registros con error 400, este error hacia parte de la caida del servicio por parte de sigru
    $queryInicial = "SELECT log_send_sigru.*, t1.id t1_id, t1.record t1_record, t1.fechamax, t1.module t1_module,t1.status t1_status FROM log_send_sigru INNER JOIN (select id, record, MAX(CAST(fecha AS CHAR)) AS fechamax, module, status from log_send_sigru WHERE log_send_sigru.fecha BETWEEN '2021-11-12 00:00:01' AND now() GROUP BY record)t1 ON log_send_sigru.fecha=t1.fechamax AND log_send_sigru.record=t1.record WHERE (log_send_sigru.status='400' OR log_send_sigru.status='405' OR log_send_sigru.status IS NULL OR log_send_sigru.status = '' OR log_send_sigru.status='500' OR log_send_sigru.status='504' OR log_send_sigru.status='401') AND (log_send_sigru.module='Accounts' OR log_send_sigru.module='Leads' OR log_send_sigru.module='Contacts') ORDER BY `t1_status` DESC";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $Record = BeanFactory::getBean($row['module'], $row['record'], array('disable_row_level_security' => true));
            $GLOBALS['log']->security("Registro ".$Record->name." Modulo ".$row['module']." id: ".$Record->id);
            if (!empty($Record->id)) {
                //$Record->save();
                //Identificar el modulo para construir el array
                //Continuar y enviar nuevamente la petición a sigru
                require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
                $sendsigru = new Send_Data_Sigru();
                $responseSIGRU = $sendsigru->data($row['record'],$row['module']);
                
            }else{
                $GLOBALS['log']->security("El registro a enviar ya está eliminado o no existe");
            }
        }
    }
    //Ejecutar reenvios de cuentas 200 pero no recibidas por SIGRU
    reenviosstatus200();
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaReenvios. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}

//Funcion para reenvios de cuentas con status http 200 en respuesta pero no se procesó la cuenta 
function reenviosstatus200(){
    global $db;
    //
    $queryInicial = "SELECT * FROM log_send_sigru WHERE log_send_sigru.responsesigru LIKE '%No se modifico o agrego el cliente%' AND log_send_sigru.status='200' GROUP BY log_send_sigru.record LIMIT 100";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $Record = BeanFactory::getBean($row['module'], $row['record'], array('disable_row_level_security' => true));
            $GLOBALS['log']->security("Registro ".$Record->name." Modulo ".$row['module']." id: ".$Record->id);
            if (!empty($Record->id)) {
                //$Record->save();
                //Identificar el modulo para construir el array
                //Continuar y enviar nuevamente la petición a sigru
                require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
                $sendsigru = new Send_Data_Sigru();
                $responseSIGRU = $sendsigru->data($row['record'],$row['module']);
                
            }else{
                $GLOBALS['log']->security("El registro a enviar ya está eliminado o no existe");
            }
        }
    }
}




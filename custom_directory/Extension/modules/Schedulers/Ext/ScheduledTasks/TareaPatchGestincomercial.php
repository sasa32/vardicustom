<?php

$job_strings[] = 'TareaPatchGestincomercial';
function TareaPatchGestincomercial()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaPatchGestincomercial. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;

    //Query para identificar los leads que no tienen fecha fin poblado y si lo deben tener
    $queryInicialFachaFin = "SELECT leads_cstm.id_c, leads_audit.date_created FROM leads_cstm INNER JOIN leads_audit ON leads_cstm.id_c=leads_audit.parent_id WHERE (leads_cstm.sasa_fechafingestionld_c ='' OR leads_cstm.sasa_fechafingestionld_c IS NULL) AND leads_audit.field_name='status' AND (leads_audit.after_value_string='C' OR leads_audit.after_value_string='G' OR leads_audit.after_value_string='F' OR leads_audit.after_value_string='J') AND leads_audit.date_created > '2021-12-05 00:00:00'";

    $resultfechafin = $db->query($queryInicialFachaFin);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicialFachaFin}"); 
    }else{
        while ($row = $db->fetchByAssoc($resultfechafin)) {
            $db->query("UPDATE leads_cstm SET sasa_fechafingestionld_c='{$row['date_created']}' WHERE id_c='{$row['id_c']}'");
        }
    }

    //Query para identificar los leads que no tienen fecha inicio poblado y si lo deben tener
    $queryFechaInicio = "SELECT leads_cstm.id_c, calls_audit.date_created FROM leads_cstm INNER JOIN calls ON leads_cstm.id_c=calls.parent_id INNER JOIN calls_cstm ON calls.id=calls_cstm.id_c INNER JOIN calls_audit ON calls.id=calls_audit.parent_id WHERE (leads_cstm.sasa_fechainiciogestionld_c='' OR leads_cstm.sasa_fechainiciogestionld_c IS NULL) AND (calls_cstm.sasa_controlproceso_c='1' OR calls_cstm.sasa_controlproceso_c='4' OR calls_cstm.sasa_controlproceso_c='7') AND calls_audit.field_name='status' AND calls_audit.after_value_string!='Planned' AND calls.status!='Planned' AND calls.date_entered > '2021-12-05 00:00:00'";

    $resultfechainicio = $db->query($queryFechaInicio);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicialFachaFin}"); 
    }else{
        while ($row = $db->fetchByAssoc($resultfechainicio)) {
            $db->query("UPDATE leads_cstm SET sasa_fechainiciogestionld_c='{$row['date_created']}' WHERE id_c='{$row['id_c']}'");
        }
    }

    //Query para identificar los casos que no tienen fecha fin poblado y si lo deben tener
    $queryInicialFachaFinCases = "SELECT cases_cstm.id_c, cases_audit.date_created FROM cases_cstm INNER JOIN cases_audit ON cases_cstm.id_c=cases_audit.parent_id WHERE (cases_cstm.sasa_fechafingestioncs_c ='' OR cases_cstm.sasa_fechafingestioncs_c IS NULL) AND cases_audit.field_name='status' AND (cases_audit.after_value_string='Rejected' OR cases_audit.after_value_string='Closed') AND (cases_cstm.sasa_motivo_c='74' OR cases_cstm.sasa_motivo_c='73' OR cases_cstm.sasa_motivo_c='71' OR cases_cstm.sasa_motivo_c='68' OR cases_cstm.sasa_motivo_c='204') AND cases_audit.date_created > '2021-12-05 00:00:00'";

    $resultfechafincases = $db->query($queryInicialFachaFinCases);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicialFachaFin}"); 
    }else{
        while ($row = $db->fetchByAssoc($resultfechafincases)) {
            $db->query("UPDATE cases_cstm SET sasa_fechafingestioncs_c='{$row['date_created']}' WHERE id_c='{$row['id_c']}'");
        }
    }

    //Query para identificar los casos que no tienen fecha Inicio poblado y si lo deben tener
    $queryInicialFachaInicioCases = "SELECT cases_cstm.id_c, calls_audit.date_created FROM cases_cstm INNER JOIN calls ON cases_cstm.id_c=calls.parent_id INNER JOIN calls_cstm ON calls.id=calls_cstm.id_c INNER JOIN calls_audit ON calls.id=calls_audit.parent_id WHERE (cases_cstm.sasa_fechainiciogestioncs_c='' OR cases_cstm.sasa_fechainiciogestioncs_c IS NULL) AND (calls_cstm.sasa_controlproceso_c='1' OR calls_cstm.sasa_controlproceso_c='4' OR calls_cstm.sasa_controlproceso_c='7') AND (cases_cstm.sasa_motivo_c='74' OR cases_cstm.sasa_motivo_c='73' OR cases_cstm.sasa_motivo_c='71' OR cases_cstm.sasa_motivo_c='68' OR cases_cstm.sasa_motivo_c='204') AND calls_audit.field_name='status' AND calls_audit.after_value_string!='Planned' AND calls.status!='Planned' AND calls.date_entered > '2021-12-05 00:00:00'";

    $resultfechainiciocases = $db->query($queryInicialFachaInicioCases);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicialFachaFin}"); 
    }else{
        while ($row = $db->fetchByAssoc($resultfechainiciocases)) {
            $db->query("UPDATE cases_cstm SET sasa_fechainiciogestioncs_c='{$row['date_created']}' WHERE id_c='{$row['id_c']}'");
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaPatchGestincomercial. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


<?php

$job_strings[] = 'TempTareaURLHD';
function TempTareaURLHD()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TempTareaURLHD. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;

    //Query para identificar los HD que tienen un token pero aun no lo tienen en el campo url del registro, esto es porque los reenvios no mapeaban este valor
    $queryInicial = "SELECT sasa_habeas_data_cstm.id_c, sasa_habeas_data_cstm.sasa_soporte_autorizacion_c,log_send_sigru.record, log_send_sigru.jsonsent, log_send_sigru.responsesigru from sasa_habeas_data_cstm INNER JOIN log_send_sigru ON sasa_habeas_data_cstm.id_c=log_send_sigru.record WHERE sasa_habeas_data_cstm.sasa_soporte_autorizacion_c='http://templatephp71.sugarcrmcolombia.com/visualizador?nota=' AND log_send_sigru.status='200' AND log_send_sigru.responsesigru !='{\"codigo\":\"-2\",\"mensaje\":\"ERROR: null\"} ' GROUP BY sasa_habeas_data_cstm.id_c";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $jsonresponse = json_decode($row['responsesigru'],true);
            if ($jsonresponse['token']!=null) {
                $urlToken = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$jsonresponse['token'];
                $db->query("UPDATE sasa_habeas_data_cstm SET sasa_soporte_autorizacion_c='{$urlToken}' WHERE id_c='{$row['id_c']}'");
            }else{
                
            }
            
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TempTareaURLHD. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


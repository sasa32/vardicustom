<?php
$job_strings[] = 'tarea_calcula_edad_contact';
function tarea_calcula_edad_contact(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_calcula_edad_contact. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		global $db;			
		/*
		* https://sasaconsultoria.sugarondemand.com/#Tasks/ea1b09b2-9c42-11ea-a89d-02dfd714a754
		*/
		$query = "
		UPDATE
		    contacts_cstm
		LEFT JOIN contacts ON id = id_c
		SET
		    sasa_edad_c = TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) 
		";
		$result = $db->query($query);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
		}
		else{
			$row = $db->fetchByAssoc($result);
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_calcula_edad_contact. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}
?>

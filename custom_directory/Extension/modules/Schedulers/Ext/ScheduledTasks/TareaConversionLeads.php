<?php

$job_strings[] = 'TareaConversionLeads';
function TareaConversionLeads()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaConversionLeads. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;
    //Query
    $QueryNumId = "SELECT leads.id AS IdLead, accounts.id AS IdAccount, leads.date_entered CreacionLead, accounts.date_entered CreacionAccount FROM leads_cstm INNER JOIN accounts_cstm ON leads_cstm.sasa_numero_documento_c=accounts_cstm.sasa_numero_documento_c INNER JOIN leads ON leads.id=leads_cstm.id_c INNER JOIN accounts ON accounts_cstm.id_c=accounts.id WHERE leads.converted=0 AND leads_cstm.sasa_numero_documento_c IS NOT NULL AND leads.deleted=0 AND accounts.deleted=0";

    $result = $db->query($QueryNumId);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$QueryNumId}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            //$GLOBALS['log']->security("Lead: {$row['IdLead']} Account {$row['IdAccount']}");
            $Lead = BeanFactory::getBean("Leads", $row['IdLead'], array('disable_row_level_security' => true)); 
            $Account = BeanFactory::getBean("Accounts", $row['IdAccount'], array('disable_row_level_security' => true));
            /*$Lead->status = 'D';
            $Lead->converted = true;
            $Lead->account_id = $Account->id;
            $Lead->account_name = $Account->name;
            $Lead->save();*/
            $fecha_lead = strtotime($row['CreacionLead']);
            $fecha_account = strtotime($row['CreacionAccount']);

            if ($fecha_lead > $fecha_account) {
                $result1 = $db->query("UPDATE leads SET status='D', converted=1, account_id='{$Account->id}', account_name='{$Account->name}' WHERE id='{$Lead->id}'");
            }else{
                $result1 = $db->query("UPDATE leads SET status='I', converted=1, account_id='{$Account->id}', account_name='{$Account->name}' WHERE id='{$Lead->id}'");
            }

            
        }
    }

    
    ConversionEmails();
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaConversionLeads. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}

function ConversionEmails(){
    global $db;
    $querysecundario = "SELECT accounts.id IDACCOUNT, t1.IDLEAD, accounts.date_entered CreacionAccount, t1.CreacionLead FROM accounts INNER JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id=accounts.id AND email_addr_bean_rel.deleted=0 INNER JOIN (SELECT email_addr_bean_rel.email_address_id EMAILLEAD, leads.id IDLEAD, leads.first_name NOMBRE, leads.last_name APELLIDO, leads.date_entered CreacionLead FROM leads INNER JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id=leads.id AND email_addr_bean_rel.deleted=0 WHERE leads.converted=0 AND email_addr_bean_rel.primary_address=1)t1 ON t1.EMAILLEAD=email_addr_bean_rel.email_address_id WHERE accounts.deleted=0 AND email_addr_bean_rel.primary_address=1 AND CONCAT(t1.NOMBRE,' ',t1.APELLIDO)=accounts.name LIMIT 10000";
    $resultemail = $db->query($querysecundario);
    while ($row2 = $db->fetchByAssoc($resultemail)) {
        //$GLOBALS['log']->security("EMAILSLead: {$row2['IDLEAD']} Account {$row2['IDACCOUNT']}");
        $Lead = BeanFactory::getBean("Leads", $row2['IDLEAD'], array('disable_row_level_security' => true)); 
        $Account = BeanFactory::getBean("Accounts", $row2['IDACCOUNT'], array('disable_row_level_security' => true));
        /*$Lead->status = 'D';
        $Lead->converted = true;
        $Lead->account_id = $Account->id;
        $Lead->account_name = $Account->name;
        $Lead->save();*/
        $fecha_lead = strtotime($row2['CreacionLead']);
        $fecha_account = strtotime($row2['CreacionAccount']);

        if ($fecha_lead > $fecha_account) {
            $result3 = $db->query("UPDATE leads SET status='D', converted=1, account_id='{$Account->id}', account_name='{$Account->name}' WHERE id='{$Lead->id}'");
        }else{
            $result3 = $db->query("UPDATE leads SET status='I', converted=1, account_id='{$Account->id}', account_name='{$Account->name}' WHERE id='{$Lead->id}'");
        }
    }
}


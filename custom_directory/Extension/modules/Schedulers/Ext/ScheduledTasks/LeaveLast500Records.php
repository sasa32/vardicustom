<?php

$job_strings[] = 'LeaveLast500Records';


function LeaveLast500Records()
{
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Leave last 500 records Account, leads, cases. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    $limitValor = 500;

    //Ejectuo la funcion para cada modulo
    //ejecutaQuerys("cases");
    //ejecutaQuerys("accounts");
    //ejecutaQuerys("leads");

    ejecutaQueryModulos("SELECT l.id AS ID_MODULOS, l.date_modified
        FROM leads l
        LEFT JOIN(SELECT l1.id AS id_C, lc.sasa_nombres_c, lc.sasa_primerapellido_c
        FROM  leads l1 
        INNER JOIN leads_cstm lc ON lc.id_c = l1.id
        WHERE l1.deleted = 0 AND (lc.sasa_nombres_c != '' OR lc.sasa_nombres_c IS NOT NULL) AND (lc.sasa_primerapellido_c != '' OR lc.sasa_primerapellido_c IS NOT NULL)
        ORDER BY l1.date_modified DESC 
        LIMIT 500) AS t1 ON t1.id_C = l.id
        WHERE t1.id_C IS NULL AND l.deleted = 0 LIMIT {$limitValor}","Leads");

    ejecutaQueryModulos("SELECT c.id AS ID_MODULOS
        FROM cases c
        LEFT JOIN (
        SELECT c1.id AS ID_C
        FROM  cases c1 
        WHERE c1.deleted = 0 AND c1.account_id != ''
        ORDER BY c1.date_modified DESC 
        LIMIT 500 ) AS tabla ON tabla.ID_C = c.id
        WHERE tabla.ID_C IS NULL AND c.deleted = 0
        LIMIT {$limitValor}","Cases");

    /*ejecutaQueryModulos("SELECT c.id AS ID_MODULOS
        FROM contacts c
        LEFT JOIN(SELECT c1.id AS id_C
        FROM  contacts c1 
        INNER JOIN accounts_contacts ac1 ON ac1.contact_id = c1.id
        INNER JOIN accounts a1 ON a1.id = ac1.account_id
        WHERE c1.deleted = 0 AND a1.deleted = 0 AND ac1.deleted = 0
        ORDER BY c1.date_modified DESC LIMIT 500) AS t1 ON t1.id_C = c.id 
        WHERE t1.id_C IS NULL AND c.deleted = 0
        ORDER BY c.date_modified
        LIMIT {$limitValor}","Contacts");

    ejecutaQueryModulos("SELECT hd1.id AS ID_MODULOS
        FROM sasa_habeas_data hd1
        LEFT JOIN (
        (SELECT hd.id AS ID_HD, hd.name, l.first_name, hd.date_modified
        FROM sasa_habeas_data hd 
        INNER JOIN leads_sasa_habeas_data_1_c lhd ON lhd.leads_sasa_habeas_data_1sasa_habeas_data_idb = hd.id
        INNER JOIN leads l ON l.id = lhd.leads_sasa_habeas_data_1leads_ida
        WHERE hd.deleted = 0 AND l.deleted = 0 AND lhd.deleted = 0 
        ORDER BY hd.date_modified DESC
        LIMIT 500)

        UNION

        (SELECT hd.id AS ID_HD, hd.name, c.first_name, hd.date_modified
        FROM sasa_habeas_data hd 
        INNER JOIN contacts_sasa_habeas_data_1_c chd ON chd.contacts_sasa_habeas_data_1sasa_habeas_data_idb = hd.id
        INNER JOIN contacts c ON c.id = chd.contacts_sasa_habeas_data_1contacts_ida
        WHERE hd.deleted = 0 AND c.deleted = 0 AND chd.deleted = 0 
        ORDER BY hd.date_modified DESC
        LIMIT 500) 
        ) AS t1 ON t1.ID_HD = hd1.id
        WHERE t1.ID_HD IS NULL AND hd1.deleted = 0
        LIMIT {$limitValor}","SASA_Habeas_Data");*/

    ejecutaQueryModulos("SELECT t1.id AS ID_MODULOS
        FROM tasks t1
        LEFT JOIN ( 
        (SELECT t.id AS ID_T, t.name, t.parent_type, t.parent_id
        FROM tasks t
        WHERE t.parent_id != '' AND t.deleted = 0
        ORDER BY t.date_modified
        LIMIT 500)

        UNION

        (SELECT t.id AS ID_T, t.name, t.parent_type, t.parent_id
        FROM tasks t 
        INNER JOIN sasa_habeas_data_tasks_1_c hdt ON hdt.sasa_habeas_data_tasks_1tasks_idb = t.id
        WHERE t.deleted = 0 AND hdt.deleted = 0
        ORDER BY t.date_modified
        LIMIT 500) ) AS tabla1 ON tabla1.ID_T = t1.id
        WHERE tabla1.ID_T IS NULL AND t1.deleted = 0
        LIMIT {$limitValor}","Tasks");

    ejecutaQueryModulos("SELECT unc1.id AS ID_MODULOS
        FROM sasa_unidadnegclienteprospect unc1
        LEFT JOIN (
        (SELECT unc.id AS ID_UNC 
        FROM sasa_unidadnegclienteprospect unc
        INNER JOIN sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c unr ON unr.sasa_unida14dfrospect_idb = unc.id
        INNER JOIN sasa_unidad_de_negocio sun ON sun.id = unr.sasa_unida3a6anegocio_ida
        WHERE unc.deleted = 0 AND unr.deleted = 0 AND sun.deleted = 0
        ORDER BY unc.date_modified
        LIMIT 500)

        UNION

        (SELECT unc.id AS ID_UNC 
        FROM sasa_unidadnegclienteprospect unc
        INNER JOIN accounts_sasa_unidadnegclienteprospect_1_c aunc ON aunc.accounts_saf31rospect_idb = unc.id
        INNER JOIN accounts a ON a.id = aunc.accounts_sasa_unidadnegclienteprospect_1accounts_ida
        WHERE unc.deleted = 0 AND aunc.deleted = 0 AND a.deleted = 0
        ORDER BY unc.date_modified
        LIMIT 500)
        ) AS tabla1 ON tabla1.ID_UNC = unc1.id
        WHERE tabla1.ID_UNC IS NULL AND unc1.deleted = 0    
        LIMIT {$limitValor}","SASA_UnidadNegClienteProspect");

 
    

    $GLOBALS['log']->security("---------------TAREA EJECUTADA-----------------------");

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Leave last 500 records Account, leads, cases ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}

function ejecutaQuerys($tabla)
{
    $cantidad = 0;

        $GLOBALS['log']->security("TABLA=>" . $tabla);
        
        $query = "SELECT a.id AS ID_MODULOS, a.date_modified
                FROM {$tabla} a
                LEFT JOIN(SELECT a1.id AS id_C FROM  {$tabla} a1 WHERE a1.deleted = 0
                    ORDER BY a1.date_modified DESC LIMIT 500 ) AS t1 ON t1.id_C = a.id
                WHERE t1.id_C IS NULL AND a.deleted = 0 LIMIT 1000";
    
        //$GLOBALS['log']->security("QUERY=>" . $query);
   
        $result = $GLOBALS['db']->query($query);

        $cant = mysqli_num_rows($result);
        //Coloco la primera letra mayuscula para usar el metodo
        $modulo = ucfirst($tabla); 

        $GLOBALS['log']->security("MODULO=>" . $modulo);

        while ($row = $GLOBALS['db']->fetchByAssoc($result)) 
        {
            // code...
            $cantidad ++;
            //$GLOBALS['log']->security("ID MODULOS=>" . $row['ID_MODULOS']);
            
            //Retrieve bean
            $bean = BeanFactory::retrieveBean($modulo, $row['ID_MODULOS']);

            //Set deleted to true
            $bean->mark_deleted($row['ID_MODULOS']);

            //Save
            $bean->save();
        }

        $GLOBALS['log']->security("CONTADOR ID MODULOS=>" . $cantidad);
}

function ejecutaQueryModulos($query, $modulo){

        $cantidad = 0;

        $GLOBALS['log']->security("modulo=>" . $modulo);
        
        //$GLOBALS['log']->security("QUERY=>" . $query);
        
        $result = $GLOBALS['db']->query($query);

        $cant = mysqli_num_rows($result);


       while ($row = $GLOBALS['db']->fetchByAssoc($result)) 
        {
            // code...
            $cantidad ++;
            
            //Retrieve bean
            $bean = BeanFactory::retrieveBean($modulo, $row['ID_MODULOS']);

            //Set deleted to true
            $bean->mark_deleted($row['ID_MODULOS']);

            //Save
            $bean->save();
        }

        $GLOBALS['log']->security("CONTADOR ID {$modulo}=>" . $cantidad);

}

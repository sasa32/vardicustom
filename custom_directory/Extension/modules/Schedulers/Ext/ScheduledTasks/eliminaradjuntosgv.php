<?php
/*
Para dar satisfacción a la propuesta de adjuntos https://docs.google.com/document/d/1wusr7EdzZcbNCEl6g2ipYG0olQfuKkhMjCY0BXNJiTo/edit se requiere realizar la tarea programada que eliminé los adjuntos cuando ya tengamos la URL del repositorio, la frecuencia será todos los domingos.

*/
//use Sugarcrm\Sugarcrm\ProcessManager\Registry;
$job_strings[] = 'eliminaradjuntosgv';
function eliminaradjuntosgv(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-eliminaradjuntosgv. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return = true;
	try{
		global $db;
		//Query para identificar los adjutnos que se pueden eliminar
		$querynotas = "";
		$queries = array("Notes"=>"SELECT notes.id, n2.id IDPadre FROM notes INNER JOIN notes n2 ON notes.note_parent_id=n2.id INNER JOIN notes_cstm n2c ON n2.id=n2c.id_c WHERE (notes.note_parent_id!='' OR notes.note_parent_id IS NOT NULL) AND (notes.filename!='' OR notes.filename IS NOT NULL) AND (n2c.sasa_soporte_adjunto_c IS NOT NULL OR n2c.sasa_soporte_adjunto_c!='') AND n2c.sasa_soporte_adjunto_c!='http://templatephp71.sugarcrmcolombia.com/visualizador?nota='",
			"SASA_Habeas_Data" => "SELECT id_c as id FROM sasa_habeas_data_cstm INNER JOIN sasa_habeas_data ON sasa_habeas_data_cstm.id_c=sasa_habeas_data.id WHERE sasa_habeas_data.deleted=0 AND (sasa_habeas_data_cstm.sasa_adjuntohabeasdata_c!='' OR sasa_habeas_data_cstm.sasa_adjuntohabeasdata_c IS NOT NULL) AND sasa_habeas_data_cstm.sasa_soporte_autorizacion_c!='http://templatephp71.sugarcrmcolombia.com/visualizador?nota='"
		);

		foreach ($queries as $key => $value) {
			$result = $db->query($value);
			$lastDbError = $db->lastDbError();				
			if(!empty($lastDbError)){
				$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$querynotas}"); 
			}else{
				//Recorrer las notas que tienen adjuntos
				while($row = $db->fetchByAssoc($result)) {
					//Inatancias objeto de notas
					$bean = BeanFactory::getBean($key, $row["id"]);
					if ($key=='Notes') {
						$bean->mark_deleted($row["id"]);
						//$borrado = $bean->deleteAttachment() ? 'Si' : 'No';
					}else{
						//Validar si existe nota
						if(!empty($bean->id)){
							require_once("include/upload_file.php");
							$file = new UploadFile();
	                		$repuesta = $file->unlink_file($row["id"]);
	                		$UpdateFile = $db->query("UPDATE sasa_habeas_data_cstm SET sasa_adjuntohabeasdata_c=NULL WHERE id_c='{$row["id"]}'");
							//Borrar adjunto de la nota instanciada en objeto bean
							//$borrado = $bean->deleteAttachment() ? 'Si' : 'No';
							$GLOBALS['log']->security("Borrado : {$borrado} Id Nota: {$row["id"]}");
						}else{
							$GLOBALS['log']->security("ERROR: No se encontro la nota de id {$row["id"]}"); 
						}
					}
				}
			}
		}

		

	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage());
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-eliminaradjuntosgv. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n");
	return $return;
}

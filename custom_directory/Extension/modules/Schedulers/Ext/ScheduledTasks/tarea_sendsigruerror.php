<?php
/*
Para dar satisfacción a la propuesta de adjuntos https://docs.google.com/document/d/1wusr7EdzZcbNCEl6g2ipYG0olQfuKkhMjCY0BXNJiTo/edit se requiere realizar la tarea programada que eliminé los adjuntos cuando ya tengamos la URL del repositorio, la frecuencia será todos los domingos.

*/
//use Sugarcrm\Sugarcrm\ProcessManager\Registry;
$job_strings[] = 'tarea_sendsigruerror';
function tarea_sendsigruerror(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_sendsigruerror. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return = true;
	try{
		//Esta tarea programada resuleve el problema que tienen los servicios de SIGRU cuando retornan un error y se debe se intentar nuevamente en 5 minutos.
		global $db;
		//Query para identificar los adjutnos que se pueden eliminar
		$querydata = "SELECT * FROM log_error_send_sigru";

		$result = $db->query($querydata);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$querydata}"); 
		}else{
			//Recorrer las notas que tienen adjuntos
			while($row = $db->fetchByAssoc($result)) {
				$module = strtolower($row['module']);
				$querystatusrecord = "SELECT date_modified FROM {$module}";
				$resultstatusrecord = $db->query($querystatusrecord);
				$record = $db->fetchByAssoc($resultstatusrecord);
				//Ver si el registro ya tuvo una peticion luego del reporte de error
				$fechaerror = strtotime($row['fecha']);
				$datemodifiedrecord = strtotime($record['date_modified']);
				//Si la fecha de error es mayor a la de modificacion del registro, quiere decir que el registro no tuvo envios exitosos luego del reporte de error
				if ($fechaerror >= $datemodifiedrecord) {
					//Continuar y enviar nuevamente la petición a sigru
					require_once("custom/modules/integration_sigru/Send_Data_Sigru.php");
					$sendsigru = new Send_Data_Sigru();
					$responseSIGRU = $sendsigru->data($row['record'],$row['module']);
					//Si retorno nuevamente el error dejar el registro y esperar a que la tarea programada lo vuleva a ejecutar en caso contrario eleminar el registro de la tabla log_error_send_sigru
					if ($responseSIGRU['status']=='200') {
						$GLOBALS['log']->security("Registro enviado luego del error: ".$row['record']." Modulo: ".$row['module']);
						//Query para elimianr el registro de la tabla log_error_send_sigru
						$db->query("DELETE FROM log_error_send_sigru WHERE record='{$row['record']}'");
					}else{
						$GLOBALS['log']->security("Registro enviado Sigue retornando error del servicio: ".$row['record']." Modulo: ".$row['module']);
					}
				}else{
					//Eliminar el registro ya tuvo una modificación y envio a sigru luego del reporte de error
					$db->query("DELETE FROM log_error_send_sigru WHERE record='{$row['record']}'");
				}
			}
		}

	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage());
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_sendsigruerror. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n");
	return $return;
}

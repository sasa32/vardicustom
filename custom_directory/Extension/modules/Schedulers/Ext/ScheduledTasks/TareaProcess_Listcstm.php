<?php

$job_strings[] = 'TareaProcess_Listcstm';
function TareaProcess_Listcstm()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaProcess_Listcstm. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;

    //Query para buscar los valores de listas desplegables pendientes por insertar en Sugar
    $queryInicial = "SELECT * FROM list_custom WHERE status='Pending'";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        require_once('modules/Studio/DropDowns/DropDownHelper.php');
        while ($row = $db->fetchByAssoc($result)) {
            $arr_valuesList = json_decode($row['values_list'],true);
            $GLOBALS['log']->security("OPT ".$row['list']);
            $GLOBALS['log']->security("VALUES ".print_r($arr_valuesList,true));
            save_custom_dropdown_strings(array($row['list'] => $arr_valuesList),"es_ES",true);
            //Query para actualizar el estado
            $Query_Update_Status = "UPDATE list_custom SET status='Processed' WHERE id='{$row['id']}'";
            //Ejecutar para insertar datos
            $GLOBALS['db']->query($Query_Update_Status);
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaProcess_Listcstm. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


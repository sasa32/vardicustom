<?php

$job_strings[] = 'TempTareaEnviaHDcola';
function TempTareaEnviaHDcola()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TempTareaEnviaHDcola. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;
    //Query para identificar los HD que no se han enviado y que aún tienen un adjunto.
    $queryInicial = "SELECT IFNULL(sasa_habeas_data.id,'') primaryid ,IFNULL(sasa_habeas_data.name,'') sasa_habeas_data_name ,sasa_habeas_data_cstm.sasa_adjuntohabeasdata_c SASA_HABEAS_DATA_CSTM_165A8E,sasa_habeas_data_cstm.sasa_soporte_autorizacion_c SASA_HABEAS_DATA_CSTM_1521AF FROM sasa_habeas_data LEFT JOIN sasa_habeas_data_cstm sasa_habeas_data_cstm ON sasa_habeas_data.id = sasa_habeas_data_cstm.id_c WHERE ((((coalesce(LENGTH(sasa_habeas_data_cstm.sasa_adjuntohabeasdata_c), 0) <> 0)))) AND sasa_habeas_data.deleted=0 LIMIT 5000";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $Record = BeanFactory::getBean('SASA_Habeas_Data', $row['primaryid'], array('disable_row_level_security' => true));
            
            if (!empty($Record->id)) {
                //$Record->save();
                //Identificar el modulo para construir el array
                //Continuar y enviar nuevamente la petición a sigru
                require_once("custom/modules/integration_sigru/Send_Data_Sigru_Habeas.php");
                $sendsigru = new Send_Data_Sigru_Habeas();
                $repuesta = $sendsigru->data($row['primaryid'],"SASA_Habeas_Data","POST");
                $repuesta = json_decode($repuesta['Response'],true);
                if ($repuesta['token']!= null) {
                    $urlToken = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$repuesta['token'];
                    $db->query("UPDATE sasa_habeas_data_cstm SET sasa_soporte_autorizacion_c='{$urlToken}' WHERE id_c='{$row['primaryid']}'");
                }
            }
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TempTareaEnviaHDcola. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


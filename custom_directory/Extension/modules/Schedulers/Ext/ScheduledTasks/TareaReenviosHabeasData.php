<?php

$job_strings[] = 'TareaReenviosHabeasData';
function TareaReenviosHabeasData()
{
    $GLOBALS['log']->security("\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio TareaReenviosHabeasData. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    global $db;
    //Query para identificar los registros con error 400, este error hacia parte de la caida del servicio por parte de sigru
    $queryInicial = "SELECT log_send_sigru.*, t1.id t1_id, t1.record t1_record, t1.fechamax, t1.module t1_module,t1.status t1_status FROM log_send_sigru INNER JOIN (select id, record, MAX(CAST(fecha AS CHAR)) AS fechamax, module, status from log_send_sigru WHERE log_send_sigru.fecha BETWEEN '2021-11-01 00:00:01' AND now() GROUP BY record)t1 ON log_send_sigru.fecha=t1.fechamax AND log_send_sigru.record=t1.record WHERE ((log_send_sigru.status='400' OR log_send_sigru.status='405' OR log_send_sigru.status IS NULL OR log_send_sigru.status = '' OR log_send_sigru.status='504' OR log_send_sigru.status='401') AND (log_send_sigru.module='SASA_Habeas_Data')) LIMIT 5000";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        while ($row = $db->fetchByAssoc($result)) {
            $Record = BeanFactory::getBean($row['module'], $row['record'], array('disable_row_level_security' => true));
            
            if (!empty($Record->id)) {
                //$Record->save();
                //Identificar el modulo para construir el array
                //Continuar y enviar nuevamente la petición a sigru
                require_once("custom/modules/integration_sigru/Send_Data_Sigru_Habeas.php");
                $sendsigru = new Send_Data_Sigru_Habeas();
                $repuesta = $sendsigru->data($row['record'],$row['module'],"POST");
                $repuesta = json_decode($repuesta['Response'],true);
                if ($repuesta['token']!= null) {
                    $urlToken = "http://templatephp71.sugarcrmcolombia.com/visualizador?nota=".$repuesta['token'];
                    $db->query("UPDATE sasa_habeas_data_cstm SET sasa_soporte_autorizacion_c='{$urlToken}' WHERE id_c='{$row['record']}'");
                }
            }
        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin TareaReenviosHabeasData. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}


<?php

$job_strings[] = 'Temp_tarea_huerfanos';
function Temp_tarea_huerfanos()
{
    $GLOBALS['log']->security("\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Temp_tarea_huerfanos. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    global $db;

    $queryInicial = "SELECT sasa_habeas_data.id idHD FROM sasa_habeas_data LEFT JOIN leads_sasa_habeas_data_1_c l1_1 ON sasa_habeas_data.id=l1_1.leads_sasa_habeas_data_1sasa_habeas_data_idb AND l1_1.deleted=0 LEFT JOIN leads l1 ON l1.id=l1_1.leads_sasa_habeas_data_1leads_ida AND l1.deleted=0 LEFT JOIN contacts_sasa_habeas_data_1_c l2_1 ON sasa_habeas_data.id=l2_1.contacts_sasa_habeas_data_1sasa_habeas_data_idb AND l2_1.deleted=0 LEFT JOIN contacts l2 ON l2.id=l2_1.contacts_sasa_habeas_data_1contacts_ida AND l2.deleted=0 LEFT JOIN users l3 ON sasa_habeas_data.created_by=l3.id AND l3.deleted=0 WHERE ((((coalesce(LENGTH(l1.id), 0) = 0)) AND ((coalesce(LENGTH(l2.id), 0) = 0)))) AND sasa_habeas_data.deleted=0";

    $result = $db->query($queryInicial);
    $lastDbError = $db->lastDbError();              
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryInicial}"); 
    }else{
        //Recorrer los HD huerfanos
        while($row = $db->fetchByAssoc($result)) {
            $HD = BeanFactory::getBean('SASA_Habeas_Data', $row['idHD'],array('disable_row_level_security' => true));

            $querygetTasksRelated= "SELECT sasa_habeas_data_tasks_1tasks_idb IDTask from sasa_habeas_data_tasks_1_c WHERE sasa_habeas_data_tasks_1sasa_habeas_data_ida='{$row['idHD']}'";
            $resultgetTasksRelated = $db->query($querygetTasksRelated);
            while ($row2 = $db->fetchByAssoc($resultgetTasksRelated)) {
                
                $Task = BeanFactory::getBean('Tasks', $row2['IDTask'],array('disable_row_level_security' => true));
                $Task->mark_deleted($row2['IDTask']);
                $Task->save();
            }
            $HD->mark_deleted($row['idHD']);
            $HD->save();

        }
    }
    
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Temp_tarea_huerfanos. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    return true;
}
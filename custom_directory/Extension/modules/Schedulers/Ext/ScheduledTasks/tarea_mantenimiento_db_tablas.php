<?php
/*
* Funcion para planificador de tareas
* Ejecuta una seria de queries
*/
$job_strings[] = 'tarea_mantenimiento_db_tablas';

function tarea_mantenimiento_db_tablas(){
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_mantenimiento_db_tablas. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$queries = array();	
		$temp_file_location="custom/tarea_mantenimiento_db_tablas.txt";
		
		/*
		* Generando queries de limpieza staticos
		*/
		$queries = tarea_mantenimiento_db_procesar_archivo_sql_deleted($temp_file_location);
				
		/*
		* Procesando queries
		*/		
		tarea_mantenimiento_db_procesar_querys_deleted($queries);

	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_mantenimiento_db_tablas. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}

/*
* Lee un archivo *.sql, luego convierte cada query separando por ";" en un array, y ejecuta cada uno de forma individual.
*/
function tarea_mantenimiento_db_procesar_archivo_sql_deleted($temp_file_location = ""){
	$queries = array();			
	$GLOBALS['log']->security( "Cargando archivo {$temp_file_location}");
	try {
		$uploadFile = new UploadFile();
		$uploadFile->temp_file_location = $temp_file_location;
		$fieldsmetadata = explode("\n", $uploadFile->get_file_contents());
		$queries = $fieldsmetadata;

		foreach ($queries as $key => $value) {
			$GLOBALS['log']->security( "Sql: LLave: {$key} Sql: {$value}");
		}
	} 
	catch (Exception $e) {
	    	$GLOBALS['log']->security("ERROR: fallo la carga del archivo '{$temp_file_location}' ".$e->getMessage()); 
	}	
	return $queries;	
}

/*
* Ejecuta una serie de querys en un array
*/
function tarea_mantenimiento_db_procesar_querys_deleted($queries=array()){		
	try {
		$return_queries=array();
		if(!empty($queries)){
			$count=0;
			foreach ($queries as $k=> $query) {
				$rows = array();
				$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
				if(!empty(trim($query))){
					global $db;		
					$count=$count+1;	
					$getAffectedRowCount=0;
					$getRowCount=0;
					$result = $db->query($query);	
										
					if(is_bool($result) !== true){
						$lastDbError = $db->lastDbError($result);				
						if(!empty($lastDbError)){
							$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
						}
						else{													
							while($row = $db->fetchRow($result)){
								$rows[] = $row;				
							}				
							$getAffectedRowCount = "Filas Afectadas: " . print_r( $db->getAffectedRowCount($result) ,true) . " " . print_r( $db->affected_rows ,true);
							$getRowCount = "Registros Seleccionados: " . print_r( $db->getRowCount($result) ,true) . " " . print_r($db->select_rows,true);																		
						}
					}										
					$GLOBALS['log']->security("\n\nresultado query ({$k}): {$getAffectedRowCount}, {$getRowCount} "); 
				}
				$return_queries	= array_merge($return_queries,$rows);
			}
			$GLOBALS['log']->security("\n\nQueries ejecutados: $count\n\n");
		}
		return $return_queries;
	} 
	catch (Exception $e) {
	    	$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
}
?>

<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_OW_ACCOUNTNAME'] = 'Cliente y Prospecto';
$mod_strings['LBL_REVENUELINEITEM_NAME'] = 'Nombre de la Revenue Line Item';
$mod_strings['LBL_FDR_OPPORTUNITIES'] = 'Cotizaciones previstas:';
$mod_strings['LBL_DV_FORECAST_OPPORTUNITY'] = 'Cotizaciones de la Previsión';
$mod_strings['LBL_LV_OPPORTUNITIES'] = 'Cotizaciones';
$mod_strings['LBL_GRAPH_OPPS_LEGEND'] = 'Cotizaciones Cerradas';
$mod_strings['LBL_EXPECTED_OPPORTUNITIES'] = 'Cotizaciones Previstas';
$mod_strings['LBL_PIPELINE_OPPORTUNITIES'] = 'Cotizaciones en Pipeline';
$mod_strings['LBL_FORECASTS_CONFIG_GENERAL_FORECAST_BY_OPPORTUNITIES'] = 'Cotizaciones';
$mod_strings['LBL_INCLUDED_PIPELINE_HELP'] = 'Cotizaciones abiertas con una {{{forecastStage}}} de {{{commitStageValue}}}';
$mod_strings['LBL_INCLUDED_PIPELINE_HELP_CUSTOM_RANGE'] = 'Cotizaciones abiertas en una {{{forecastStage}}} incluida';
$mod_strings['LBL_UPSIDE_PIPELINE_HELP'] = 'Cotizaciones abiertas con una {{{forecastStage}}} de {{{commitStageValue}}}';
$mod_strings['LBL_EXCLUDED_PIPELINE_HELP'] = 'Cotizaciones abiertas con una {{{forecastStage}}} de {{{commitStageValue}}}';
$mod_strings['LBL_EXCLUDED_PIPELINE_HELP_CUSTOM_RANGE'] = 'Cotizaciones abiertas en una {{{forecastStage}}} no incluida';
$mod_strings['LBL_WON_HELP'] = 'Cotizaciones que ya se han ganado';
$mod_strings['LBL_LOST_HELP'] = 'Cotizaciones que ya se han perdido';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Crear Cotización';
$mod_strings['LBL_OW_OPPORTUNITIES'] = 'Cotización';
$mod_strings['LBL_OW_MODULE_TITLE'] = 'Hoja de Cotización';
$mod_strings['LBL_CHART_FOOTER'] = 'Historial de PrevisionesCantidad en Cuota vs Cantidad Prevista vs Valor de Cotización Cerrada';
$mod_strings['LBL_OPPORTUNITY_STATUS'] = 'Estado de la Cotización';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Nombre de la Cotización';

<?php
// created: 2020-07-30 15:35:59
$dictionary["Contact"]["fields"]["contacts_sasa_habeas_data_1"] = array (
  'name' => 'contacts_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'contacts_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'vname' => 'LBL_CONTACTS_SASA_HABEAS_DATA_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_sasa_habeas_data_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

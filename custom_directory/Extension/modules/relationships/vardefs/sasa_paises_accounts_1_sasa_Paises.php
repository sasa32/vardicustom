<?php
// created: 2020-05-15 15:40:50
$dictionary["sasa_Paises"]["fields"]["sasa_paises_accounts_1"] = array (
  'name' => 'sasa_paises_accounts_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_PAISES_ACCOUNTS_1_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_accounts_1sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
// created: 2020-05-15 15:43:16
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_contacts_1"] = array (
  'name' => 'sasa_departamentos_contacts_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_contacts_1sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

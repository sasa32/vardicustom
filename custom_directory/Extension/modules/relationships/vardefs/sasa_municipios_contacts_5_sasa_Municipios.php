<?php
// created: 2020-05-15 16:14:43
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_5"] = array (
  'name' => 'sasa_municipios_contacts_5',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_5',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_5_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_5sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

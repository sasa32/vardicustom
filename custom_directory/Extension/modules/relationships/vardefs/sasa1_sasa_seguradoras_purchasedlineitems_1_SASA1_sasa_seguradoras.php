<?php
// created: 2023-05-25 19:59:20
$dictionary["SASA1_sasa_seguradoras"]["fields"]["sasa1_sasa_seguradoras_purchasedlineitems_1"] = array (
  'name' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'type' => 'link',
  'relationship' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'source' => 'non-db',
  'module' => 'PurchasedLineItems',
  'bean_name' => 'PurchasedLineItem',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_SASA1_SASA_SEGURADORAS_TITLE',
  'id_name' => 'sasa1_sasa7258radoras_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
// created: 2021-01-27 16:50:54
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_leads_1"] = array (
  'name' => 'sasa_puntos_de_ventas_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_LEADS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

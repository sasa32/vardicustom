<?php
// created: 2022-11-23 16:02:16
$dictionary["sasa_vehiculos"]["fields"]["sasa_vehiculos_opportunities_1"] = array (
  'name' => 'sasa_vehiculos_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

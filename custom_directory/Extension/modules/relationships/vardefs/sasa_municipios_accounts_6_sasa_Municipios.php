<?php
// created: 2020-08-03 14:04:52
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_6"] = array (
  'name' => 'sasa_municipios_accounts_6',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_6',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_6_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_6sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
// created: 2020-07-30 15:35:23
$dictionary["Lead"]["fields"]["leads_sasa_habeas_data_1"] = array (
  'name' => 'leads_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'leads_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'vname' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_sasa_habeas_data_1leads_ida',
  'link-type' => 'many',
  'side' => 'left',
);

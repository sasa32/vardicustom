<?php
// created: 2020-05-15 16:05:56
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_accounts_2"] = array (
  'name' => 'sasa_departamentos_accounts_2',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_accounts_2',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_ACCOUNTS_2_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_accounts_2sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

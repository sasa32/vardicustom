<?php
// created: 2022-11-23 16:02:16
$dictionary["Opportunity"]["fields"]["sasa_vehiculos_opportunities_1"] = array (
  'name' => 'sasa_vehiculos_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_opportunities_1',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'side' => 'right',
  'vname' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'link-type' => 'one',
);
$dictionary["Opportunity"]["fields"]["sasa_vehiculos_opportunities_1_name"] = array (
  'name' => 'sasa_vehiculos_opportunities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_SASA_VEHICULOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_opportunities_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["sasa_vehiculos_opportunities_1sasa_vehiculos_ida"] = array (
  'name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID',
  'id_name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_opportunities_1',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

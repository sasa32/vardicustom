<?php
// created: 2023-05-25 19:59:20
$dictionary["PurchasedLineItem"]["fields"]["sasa1_sasa_seguradoras_purchasedlineitems_1"] = array (
  'name' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'type' => 'link',
  'relationship' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'source' => 'non-db',
  'module' => 'SASA1_sasa_seguradoras',
  'bean_name' => 'SASA1_sasa_seguradoras',
  'side' => 'right',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_PURCHASEDLINEITEMS_TITLE',
  'id_name' => 'sasa1_sasa7258radoras_ida',
  'link-type' => 'one',
);
$dictionary["PurchasedLineItem"]["fields"]["sasa1_sasa_seguradoras_purchasedlineitems_1_name"] = array (
  'name' => 'sasa1_sasa_seguradoras_purchasedlineitems_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_SASA1_SASA_SEGURADORAS_TITLE',
  'save' => true,
  'id_name' => 'sasa1_sasa7258radoras_ida',
  'link' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'table' => 'sasa1_sasa_seguradoras',
  'module' => 'SASA1_sasa_seguradoras',
  'rname' => 'name',
);
$dictionary["PurchasedLineItem"]["fields"]["sasa1_sasa7258radoras_ida"] = array (
  'name' => 'sasa1_sasa7258radoras_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_PURCHASEDLINEITEMS_TITLE_ID',
  'id_name' => 'sasa1_sasa7258radoras_ida',
  'link' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
  'table' => 'sasa1_sasa_seguradoras',
  'module' => 'SASA1_sasa_seguradoras',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

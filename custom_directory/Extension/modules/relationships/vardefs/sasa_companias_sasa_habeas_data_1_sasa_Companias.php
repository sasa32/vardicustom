<?php
// created: 2020-07-29 23:30:52
$dictionary["sasa_Companias"]["fields"]["sasa_companias_sasa_habeas_data_1"] = array (
  'name' => 'sasa_companias_sasa_habeas_data_1',
  'type' => 'link',
  'relationship' => 'sasa_companias_sasa_habeas_data_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_HABEAS_DATA_1_FROM_SASA_COMPANIAS_TITLE',
  'id_name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
  'link-type' => 'many',
  'side' => 'left',
);

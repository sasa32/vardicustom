<?php
// created: 2020-05-15 15:41:37
$dictionary["sasa_Paises"]["fields"]["sasa_paises_leads_1"] = array (
  'name' => 'sasa_paises_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_PAISES_LEADS_1_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_leads_1sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

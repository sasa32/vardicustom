<?php
// created: 2020-05-15 16:09:12
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_2"] = array (
  'name' => 'sasa_municipios_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_2',
  'source' => 'non-db',
  'module' => 'sasa_Municipios',
  'bean_name' => 'sasa_Municipios',
  'side' => 'right',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_2_name"] = array (
  'name' => 'sasa_municipios_contacts_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_2',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_municipios_contacts_2sasa_municipios_ida"] = array (
  'name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'link' => 'sasa_municipios_contacts_2',
  'table' => 'sasa_municipios',
  'module' => 'sasa_Municipios',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

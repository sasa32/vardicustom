<?php
// created: 2022-10-04 17:32:58
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_centrosdecostos_sasa_puntos_de_ventas_1"] = array (
  'name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'type' => 'link',
  'relationship' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'source' => 'non-db',
  'module' => 'sasa_CentrosDeCostos',
  'bean_name' => 'sasa_CentrosDeCostos',
  'side' => 'right',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_centr37aaecostos_ida',
  'link-type' => 'one',
);
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_centrosdecostos_sasa_puntos_de_ventas_1_name"] = array (
  'name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_CENTROSDECOSTOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_centr37aaecostos_ida',
  'link' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'table' => 'sasa_centrosdecostos',
  'module' => 'sasa_CentrosDeCostos',
  'rname' => 'name',
);
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_centr37aaecostos_ida"] = array (
  'name' => 'sasa_centr37aaecostos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_SASA_PUNTOS_DE_VENTAS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE_ID',
  'id_name' => 'sasa_centr37aaecostos_ida',
  'link' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
  'table' => 'sasa_centrosdecostos',
  'module' => 'sasa_CentrosDeCostos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

<?php
// created: 2020-05-15 23:20:17
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_companias_sasa_unidad_de_negocio_1"] = array (
  'name' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'type' => 'link',
  'relationship' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'source' => 'non-db',
  'module' => 'sasa_Companias',
  'bean_name' => 'sasa_Companias',
  'side' => 'right',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'link-type' => 'one',
);
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_companias_sasa_unidad_de_negocio_1_name"] = array (
  'name' => 'sasa_companias_sasa_unidad_de_negocio_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'link' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'table' => 'sasa_companias',
  'module' => 'sasa_Companias',
  'rname' => 'name',
);
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida"] = array (
  'name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE_ID',
  'id_name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'link' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'table' => 'sasa_companias',
  'module' => 'sasa_Companias',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

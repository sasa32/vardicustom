<?php
// created: 2020-11-03 19:25:16
$dictionary["SASA_Habeas_Data"]["fields"]["sasa_habeas_data_tasks_1"] = array (
  'name' => 'sasa_habeas_data_tasks_1',
  'type' => 'link',
  'relationship' => 'sasa_habeas_data_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_SASA_HABEAS_DATA_TASKS_1_FROM_SASA_HABEAS_DATA_TITLE',
  'id_name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'link-type' => 'many',
  'side' => 'left',
);

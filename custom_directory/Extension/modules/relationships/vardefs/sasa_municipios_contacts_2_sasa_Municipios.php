<?php
// created: 2020-05-15 16:09:12
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_2"] = array (
  'name' => 'sasa_municipios_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
// created: 2020-05-15 23:49:35
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_meetings_1"] = array (
  'name' => 'sasa_puntos_de_ventas_meetings_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_meetings_1',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
// created: 2020-11-03 19:25:16
$dictionary["Task"]["fields"]["sasa_habeas_data_tasks_1"] = array (
  'name' => 'sasa_habeas_data_tasks_1',
  'type' => 'link',
  'relationship' => 'sasa_habeas_data_tasks_1',
  'source' => 'non-db',
  'module' => 'SASA_Habeas_Data',
  'bean_name' => 'SASA_Habeas_Data',
  'side' => 'right',
  'vname' => 'LBL_SASA_HABEAS_DATA_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["sasa_habeas_data_tasks_1_name"] = array (
  'name' => 'sasa_habeas_data_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_HABEAS_DATA_TASKS_1_FROM_SASA_HABEAS_DATA_TITLE',
  'save' => true,
  'id_name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'link' => 'sasa_habeas_data_tasks_1',
  'table' => 'sasa_habeas_data',
  'module' => 'SASA_Habeas_Data',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["sasa_habeas_data_tasks_1sasa_habeas_data_ida"] = array (
  'name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_HABEAS_DATA_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
  'link' => 'sasa_habeas_data_tasks_1',
  'table' => 'sasa_habeas_data',
  'module' => 'SASA_Habeas_Data',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

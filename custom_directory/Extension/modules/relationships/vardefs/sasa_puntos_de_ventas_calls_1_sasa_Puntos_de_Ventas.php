<?php
// created: 2020-06-19 14:28:13
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_calls_1"] = array (
  'name' => 'sasa_puntos_de_ventas_calls_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_calls_1',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

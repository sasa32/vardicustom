<?php
// created: 2020-05-15 23:49:35
$dictionary["Meeting"]["fields"]["sasa_puntos_de_ventas_meetings_1"] = array (
  'name' => 'sasa_puntos_de_ventas_meetings_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_meetings_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'side' => 'right',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_MEETINGS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'link-type' => 'one',
);
$dictionary["Meeting"]["fields"]["sasa_puntos_de_ventas_meetings_1_name"] = array (
  'name' => 'sasa_puntos_de_ventas_meetings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_meetings_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'name',
);
$dictionary["Meeting"]["fields"]["sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida"] = array (
  'name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_MEETINGS_1_FROM_MEETINGS_TITLE_ID',
  'id_name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_meetings_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

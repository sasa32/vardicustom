<?php
// created: 2021-03-09 21:41:14
$dictionary["Case"]["fields"]["sasa_puntos_de_ventas_cases_1"] = array (
  'name' => 'sasa_puntos_de_ventas_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_cases_1',
  'source' => 'non-db',
  'module' => 'sasa_Puntos_de_Ventas',
  'bean_name' => 'sasa_Puntos_de_Ventas',
  'side' => 'right',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_puntos_de_ventas_cases_1_name"] = array (
  'name' => 'sasa_puntos_de_ventas_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_cases_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida"] = array (
  'name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_CASES_1_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
  'link' => 'sasa_puntos_de_ventas_cases_1',
  'table' => 'sasa_puntos_de_ventas',
  'module' => 'sasa_Puntos_de_Ventas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

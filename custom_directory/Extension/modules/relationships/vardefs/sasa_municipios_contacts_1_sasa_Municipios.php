<?php
// created: 2020-05-15 15:44:56
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_1"] = array (
  'name' => 'sasa_municipios_contacts_1',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_1sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
// created: 2020-05-15 16:05:13
$dictionary["sasa_Paises"]["fields"]["sasa_paises_leads_2"] = array (
  'name' => 'sasa_paises_leads_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_leads_2',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_PAISES_LEADS_2_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_leads_2sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

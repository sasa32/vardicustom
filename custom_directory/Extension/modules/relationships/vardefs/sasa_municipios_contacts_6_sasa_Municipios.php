<?php
// created: 2020-08-03 14:04:12
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_contacts_6"] = array (
  'name' => 'sasa_municipios_contacts_6',
  'type' => 'link',
  'relationship' => 'sasa_municipios_contacts_6',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_MUNICIPIOS_CONTACTS_6_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

<?php
// created: 2020-05-15 15:43:53
$dictionary["sasa_Departamentos"]["fields"]["sasa_departamentos_leads_1"] = array (
  'name' => 'sasa_departamentos_leads_1',
  'type' => 'link',
  'relationship' => 'sasa_departamentos_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_SASA_DEPARTAMENTOS_LEADS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'id_name' => 'sasa_departamentos_leads_1sasa_departamentos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

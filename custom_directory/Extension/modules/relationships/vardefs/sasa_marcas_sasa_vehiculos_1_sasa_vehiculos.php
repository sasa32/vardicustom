<?php
// created: 2020-05-15 23:47:02
$dictionary["sasa_vehiculos"]["fields"]["sasa_marcas_sasa_vehiculos_1"] = array (
  'name' => 'sasa_marcas_sasa_vehiculos_1',
  'type' => 'link',
  'relationship' => 'sasa_marcas_sasa_vehiculos_1',
  'source' => 'non-db',
  'module' => 'sasa_Marcas',
  'bean_name' => 'sasa_Marcas',
  'side' => 'right',
  'vname' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_VEHICULOS_TITLE',
  'id_name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'link-type' => 'one',
);
$dictionary["sasa_vehiculos"]["fields"]["sasa_marcas_sasa_vehiculos_1_name"] = array (
  'name' => 'sasa_marcas_sasa_vehiculos_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_MARCAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'link' => 'sasa_marcas_sasa_vehiculos_1',
  'table' => 'sasa_marcas',
  'module' => 'sasa_Marcas',
  'rname' => 'name',
);
$dictionary["sasa_vehiculos"]["fields"]["sasa_marcas_sasa_vehiculos_1sasa_marcas_ida"] = array (
  'name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_VEHICULOS_TITLE_ID',
  'id_name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'link' => 'sasa_marcas_sasa_vehiculos_1',
  'table' => 'sasa_marcas',
  'module' => 'sasa_Marcas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

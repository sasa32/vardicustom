<?php
// created: 2022-10-05 15:14:01
$dictionary["sasa_CentrosDeCostos"]["fields"]["sasa_centrosdecostos_cases_1"] = array (
  'name' => 'sasa_centrosdecostos_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_centrosdecostos_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_SASA_CENTROSDECOSTOS_TITLE',
  'id_name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

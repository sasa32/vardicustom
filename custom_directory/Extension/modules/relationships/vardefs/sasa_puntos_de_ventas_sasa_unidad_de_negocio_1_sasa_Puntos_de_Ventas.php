<?php
// created: 2020-05-15 23:48:57
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_sasa_unidad_de_negocio_1"] = array (
  'name' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
  'source' => 'non-db',
  'module' => 'sasa_Unidad_de_Negocio',
  'bean_name' => 'sasa_Unidad_de_Negocio',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa_puntoeacanegocio_idb',
);

<?php
// created: 2022-11-23 15:22:18
$dictionary["sasa_Unidad_de_Negocio"]["fields"]["sasa_unidad_de_negocio_opportunities_1"] = array (
  'name' => 'sasa_unidad_de_negocio_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_unidad_de_negocio_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'link-type' => 'many',
  'side' => 'left',
);

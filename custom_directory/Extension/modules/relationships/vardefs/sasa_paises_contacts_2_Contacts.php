<?php
// created: 2020-05-15 16:04:53
$dictionary["Contact"]["fields"]["sasa_paises_contacts_2"] = array (
  'name' => 'sasa_paises_contacts_2',
  'type' => 'link',
  'relationship' => 'sasa_paises_contacts_2',
  'source' => 'non-db',
  'module' => 'sasa_Paises',
  'bean_name' => 'sasa_Paises',
  'side' => 'right',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["sasa_paises_contacts_2_name"] = array (
  'name' => 'sasa_paises_contacts_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_SASA_PAISES_TITLE',
  'save' => true,
  'id_name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'link' => 'sasa_paises_contacts_2',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["sasa_paises_contacts_2sasa_paises_ida"] = array (
  'name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_2_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'sasa_paises_contacts_2sasa_paises_ida',
  'link' => 'sasa_paises_contacts_2',
  'table' => 'sasa_paises',
  'module' => 'sasa_Paises',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

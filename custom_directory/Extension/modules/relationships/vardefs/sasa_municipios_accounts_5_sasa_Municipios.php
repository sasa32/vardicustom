<?php
// created: 2020-08-03 14:03:29
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_5"] = array (
  'name' => 'sasa_municipios_accounts_5',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_5',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_5_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_5sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

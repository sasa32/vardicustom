<?php
// created: 2020-06-19 14:27:46
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_tasks_1"] = array (
  'name' => 'sasa_puntos_de_ventas_tasks_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_TASKS_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

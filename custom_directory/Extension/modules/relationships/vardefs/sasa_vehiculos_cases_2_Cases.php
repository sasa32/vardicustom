<?php
// created: 2021-08-04 21:22:45
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_2"] = array (
  'name' => 'sasa_vehiculos_cases_2',
  'type' => 'link',
  'relationship' => 'sasa_vehiculos_cases_2',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'side' => 'right',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_CASES_TITLE',
  'id_name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_2_name"] = array (
  'name' => 'sasa_vehiculos_cases_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_SASA_VEHICULOS_TITLE',
  'save' => true,
  'id_name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_2',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_vehiculos_cases_2sasa_vehiculos_ida"] = array (
  'name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_VEHICULOS_CASES_2_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
  'link' => 'sasa_vehiculos_cases_2',
  'table' => 'sasa_vehiculos',
  'module' => 'sasa_vehiculos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

<?php
// created: 2020-07-29 16:42:01
$dictionary["Document"]["fields"]["documents_sasa_companias_1"] = array (
  'name' => 'documents_sasa_companias_1',
  'type' => 'link',
  'relationship' => 'documents_sasa_companias_1',
  'source' => 'non-db',
  'module' => 'sasa_Companias',
  'bean_name' => 'sasa_Companias',
  'vname' => 'LBL_DOCUMENTS_SASA_COMPANIAS_1_FROM_SASA_COMPANIAS_TITLE',
  'id_name' => 'documents_sasa_companias_1sasa_companias_idb',
);

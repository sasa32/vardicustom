<?php
// created: 2021-03-05 13:31:31
$dictionary["sasa_Puntos_de_Ventas"]["fields"]["sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1"] = array (
  'name' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
  'type' => 'link',
  'relationship' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
  'source' => 'non-db',
  'module' => 'SASA_UnidadNegClienteProspect',
  'bean_name' => 'SASA_UnidadNegClienteProspect',
  'vname' => 'LBL_SASA_PUNTOS_DE_VENTAS_SASA_UNIDADNEGCLIENTEPROSPECT_1_FROM_SASA_PUNTOS_DE_VENTAS_TITLE',
  'id_name' => 'sasa_punto200b_ventas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

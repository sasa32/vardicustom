<?php
// created: 2020-05-15 15:41:17
$dictionary["sasa_Paises"]["fields"]["sasa_paises_contacts_1"] = array (
  'name' => 'sasa_paises_contacts_1',
  'type' => 'link',
  'relationship' => 'sasa_paises_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_SASA_PAISES_CONTACTS_1_FROM_SASA_PAISES_TITLE',
  'id_name' => 'sasa_paises_contacts_1sasa_paises_ida',
  'link-type' => 'many',
  'side' => 'left',
);

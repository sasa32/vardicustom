<?php
// created: 2020-05-15 23:20:17
$dictionary["sasa_Companias"]["fields"]["sasa_companias_sasa_unidad_de_negocio_1"] = array (
  'name' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'type' => 'link',
  'relationship' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'source' => 'non-db',
  'module' => 'sasa_Unidad_de_Negocio',
  'bean_name' => 'sasa_Unidad_de_Negocio',
  'vname' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_COMPANIAS_TITLE',
  'id_name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
  'link-type' => 'many',
  'side' => 'left',
);

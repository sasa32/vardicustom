<?php
// created: 2020-05-15 23:47:02
$dictionary["sasa_Marcas"]["fields"]["sasa_marcas_sasa_vehiculos_1"] = array (
  'name' => 'sasa_marcas_sasa_vehiculos_1',
  'type' => 'link',
  'relationship' => 'sasa_marcas_sasa_vehiculos_1',
  'source' => 'non-db',
  'module' => 'sasa_vehiculos',
  'bean_name' => 'sasa_vehiculos',
  'vname' => 'LBL_SASA_MARCAS_SASA_VEHICULOS_1_FROM_SASA_MARCAS_TITLE',
  'id_name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

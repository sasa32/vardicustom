<?php
// created: 2020-05-15 16:08:40
$dictionary["sasa_Municipios"]["fields"]["sasa_municipios_accounts_2"] = array (
  'name' => 'sasa_municipios_accounts_2',
  'type' => 'link',
  'relationship' => 'sasa_municipios_accounts_2',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_SASA_MUNICIPIOS_ACCOUNTS_2_FROM_SASA_MUNICIPIOS_TITLE',
  'id_name' => 'sasa_municipios_accounts_2sasa_municipios_ida',
  'link-type' => 'many',
  'side' => 'left',
);

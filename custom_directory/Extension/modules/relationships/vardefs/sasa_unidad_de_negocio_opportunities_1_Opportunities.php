<?php
// created: 2022-11-23 15:22:18
$dictionary["Opportunity"]["fields"]["sasa_unidad_de_negocio_opportunities_1"] = array (
  'name' => 'sasa_unidad_de_negocio_opportunities_1',
  'type' => 'link',
  'relationship' => 'sasa_unidad_de_negocio_opportunities_1',
  'source' => 'non-db',
  'module' => 'sasa_Unidad_de_Negocio',
  'bean_name' => 'sasa_Unidad_de_Negocio',
  'side' => 'right',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'link-type' => 'one',
);
$dictionary["Opportunity"]["fields"]["sasa_unidad_de_negocio_opportunities_1_name"] = array (
  'name' => 'sasa_unidad_de_negocio_opportunities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'save' => true,
  'id_name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'link' => 'sasa_unidad_de_negocio_opportunities_1',
  'table' => 'sasa_unidad_de_negocio',
  'module' => 'sasa_Unidad_de_Negocio',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida"] = array (
  'name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_UNIDAD_DE_NEGOCIO_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID',
  'id_name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
  'link' => 'sasa_unidad_de_negocio_opportunities_1',
  'table' => 'sasa_unidad_de_negocio',
  'module' => 'sasa_Unidad_de_Negocio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

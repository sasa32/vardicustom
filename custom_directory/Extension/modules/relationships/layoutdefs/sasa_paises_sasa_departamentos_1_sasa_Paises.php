<?php
 // created: 2020-05-15 15:55:44
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_sasa_departamentos_1'] = array (
  'order' => 100,
  'module' => 'sasa_Departamentos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_SASA_DEPARTAMENTOS_1_FROM_SASA_DEPARTAMENTOS_TITLE',
  'get_subpanel_data' => 'sasa_paises_sasa_departamentos_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

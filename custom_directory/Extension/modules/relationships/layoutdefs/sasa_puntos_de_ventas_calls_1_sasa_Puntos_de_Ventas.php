<?php
 // created: 2020-06-19 14:28:13
$layout_defs["sasa_Puntos_de_Ventas"]["subpanel_setup"]['sasa_puntos_de_ventas_calls_1'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PUNTOS_DE_VENTAS_CALLS_1_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'sasa_puntos_de_ventas_calls_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

<?php
 // created: 2023-05-25 19:46:01
$layout_defs["SASA1_sasa_seguradoras"]["subpanel_setup"]['sasa1_sasa_seguradoras_revenuelineitems_1'] = array (
  'order' => 100,
  'module' => 'RevenueLineItems',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA1_SASA_SEGURADORAS_REVENUELINEITEMS_1_FROM_REVENUELINEITEMS_TITLE',
  'get_subpanel_data' => 'sasa1_sasa_seguradoras_revenuelineitems_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

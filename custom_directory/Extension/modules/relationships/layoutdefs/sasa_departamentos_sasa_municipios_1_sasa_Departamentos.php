<?php
 // created: 2020-05-15 15:42:24
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_sasa_municipios_1'] = array (
  'order' => 100,
  'module' => 'sasa_Municipios',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_SASA_MUNICIPIOS_1_FROM_SASA_MUNICIPIOS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_sasa_municipios_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

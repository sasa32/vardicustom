<?php
 // created: 2022-10-05 15:14:01
$layout_defs["sasa_CentrosDeCostos"]["subpanel_setup"]['sasa_centrosdecostos_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_CENTROSDECOSTOS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_centrosdecostos_cases_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

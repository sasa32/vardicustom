<?php
 // created: 2020-05-15 16:06:23
$layout_defs["sasa_Departamentos"]["subpanel_setup"]['sasa_departamentos_contacts_2'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_DEPARTAMENTOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_departamentos_contacts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

<?php
 // created: 2020-07-30 15:35:23
$layout_defs["Leads"]["subpanel_setup"]['leads_sasa_habeas_data_1'] = array (
  'order' => 100,
  'module' => 'SASA_Habeas_Data',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_SASA_HABEAS_DATA_1_FROM_SASA_HABEAS_DATA_TITLE',
  'get_subpanel_data' => 'leads_sasa_habeas_data_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

<?php
 // created: 2020-05-15 16:05:13
$layout_defs["sasa_Paises"]["subpanel_setup"]['sasa_paises_leads_2'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PAISES_LEADS_2_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_paises_leads_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

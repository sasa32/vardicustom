<?php
 // created: 2020-11-03 19:25:16
$layout_defs["SASA_Habeas_Data"]["subpanel_setup"]['sasa_habeas_data_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_HABEAS_DATA_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'sasa_habeas_data_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

<?php
 // created: 2021-08-04 21:25:11
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_cases_4'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_VEHICULOS_CASES_4_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_cases_4',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

<?php
 // created: 2020-05-15 23:44:20
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_VEHICULOS_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

<?php
 // created: 2020-05-15 23:20:17
$layout_defs["sasa_Companias"]["subpanel_setup"]['sasa_companias_sasa_unidad_de_negocio_1'] = array (
  'order' => 100,
  'module' => 'sasa_Unidad_de_Negocio',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_COMPANIAS_SASA_UNIDAD_DE_NEGOCIO_1_FROM_SASA_UNIDAD_DE_NEGOCIO_TITLE',
  'get_subpanel_data' => 'sasa_companias_sasa_unidad_de_negocio_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

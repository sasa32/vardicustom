<?php
 // created: 2021-08-04 21:24:18
$layout_defs["sasa_vehiculos"]["subpanel_setup"]['sasa_vehiculos_cases_3'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_VEHICULOS_CASES_3_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_vehiculos_cases_3',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

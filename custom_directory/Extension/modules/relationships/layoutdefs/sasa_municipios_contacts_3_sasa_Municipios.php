<?php
 // created: 2020-05-15 16:12:58
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_contacts_3'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_MUNICIPIOS_CONTACTS_3_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_contacts_3',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

<?php
 // created: 2020-05-15 16:09:12
$layout_defs["sasa_Municipios"]["subpanel_setup"]['sasa_municipios_contacts_2'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_MUNICIPIOS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'sasa_municipios_contacts_2',
);

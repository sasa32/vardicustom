<?php
 // created: 2023-05-25 19:59:20
$layout_defs["SASA1_sasa_seguradoras"]["subpanel_setup"]['sasa1_sasa_seguradoras_purchasedlineitems_1'] = array (
  'order' => 100,
  'module' => 'PurchasedLineItems',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA1_SASA_SEGURADORAS_PURCHASEDLINEITEMS_1_FROM_PURCHASEDLINEITEMS_TITLE',
  'get_subpanel_data' => 'sasa1_sasa_seguradoras_purchasedlineitems_1',
);

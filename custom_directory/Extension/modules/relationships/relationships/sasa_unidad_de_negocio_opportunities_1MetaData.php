<?php
// created: 2022-11-23 15:22:18
$dictionary["sasa_unidad_de_negocio_opportunities_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_unidad_de_negocio_opportunities_1' => 
    array (
      'lhs_module' => 'sasa_Unidad_de_Negocio',
      'lhs_table' => 'sasa_unidad_de_negocio',
      'lhs_key' => 'id',
      'rhs_module' => 'Opportunities',
      'rhs_table' => 'opportunities',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_unidad_de_negocio_opportunities_1_c',
      'join_key_lhs' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
      'join_key_rhs' => 'sasa_unidad_de_negocio_opportunities_1opportunities_idb',
    ),
  ),
  'table' => 'sasa_unidad_de_negocio_opportunities_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida' => 
    array (
      'name' => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
      'type' => 'id',
    ),
    'sasa_unidad_de_negocio_opportunities_1opportunities_idb' => 
    array (
      'name' => 'sasa_unidad_de_negocio_opportunities_1opportunities_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_unidad_de_negocio_opportunities_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_unidad_de_negocio_opportunities_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_unidad_de_negocio_opportunities_1sasa_unidad_de_negocio_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_unidad_de_negocio_opportunities_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_unidad_de_negocio_opportunities_1opportunities_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_unidad_de_negocio_opportunities_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_unidad_de_negocio_opportunities_1opportunities_idb',
      ),
    ),
  ),
);
<?php
// created: 2023-05-25 19:59:20
$dictionary["sasa1_sasa_seguradoras_purchasedlineitems_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa1_sasa_seguradoras_purchasedlineitems_1' => 
    array (
      'lhs_module' => 'SASA1_sasa_seguradoras',
      'lhs_table' => 'sasa1_sasa_seguradoras',
      'lhs_key' => 'id',
      'rhs_module' => 'PurchasedLineItems',
      'rhs_table' => 'purchased_line_items',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa1_sasa_seguradoras_purchasedlineitems_1_c',
      'join_key_lhs' => 'sasa1_sasa7258radoras_ida',
      'join_key_rhs' => 'sasa1_sasae40fneitems_idb',
    ),
  ),
  'table' => 'sasa1_sasa_seguradoras_purchasedlineitems_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa1_sasa7258radoras_ida' => 
    array (
      'name' => 'sasa1_sasa7258radoras_ida',
      'type' => 'id',
    ),
    'sasa1_sasae40fneitems_idb' => 
    array (
      'name' => 'sasa1_sasae40fneitems_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa1_sasa_seguradoras_purchasedlineitems_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa1_sasa_seguradoras_purchasedlineitems_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa1_sasa7258radoras_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa1_sasa_seguradoras_purchasedlineitems_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa1_sasae40fneitems_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa1_sasa_seguradoras_purchasedlineitems_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa1_sasae40fneitems_idb',
      ),
    ),
  ),
);
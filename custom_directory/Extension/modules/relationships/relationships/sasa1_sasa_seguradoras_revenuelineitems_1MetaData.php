<?php
// created: 2023-05-25 19:46:01
$dictionary["sasa1_sasa_seguradoras_revenuelineitems_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa1_sasa_seguradoras_revenuelineitems_1' => 
    array (
      'lhs_module' => 'SASA1_sasa_seguradoras',
      'lhs_table' => 'sasa1_sasa_seguradoras',
      'lhs_key' => 'id',
      'rhs_module' => 'RevenueLineItems',
      'rhs_table' => 'revenue_line_items',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa1_sasa_seguradoras_revenuelineitems_1_c',
      'join_key_lhs' => 'sasa1_sasabb9eradoras_ida',
      'join_key_rhs' => 'sasa1_sasa_seguradoras_revenuelineitems_1revenuelineitems_idb',
    ),
  ),
  'table' => 'sasa1_sasa_seguradoras_revenuelineitems_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa1_sasabb9eradoras_ida' => 
    array (
      'name' => 'sasa1_sasabb9eradoras_ida',
      'type' => 'id',
    ),
    'sasa1_sasa_seguradoras_revenuelineitems_1revenuelineitems_idb' => 
    array (
      'name' => 'sasa1_sasa_seguradoras_revenuelineitems_1revenuelineitems_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa1_sasa_seguradoras_revenuelineitems_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa1_sasa_seguradoras_revenuelineitems_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa1_sasabb9eradoras_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa1_sasa_seguradoras_revenuelineitems_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa1_sasa_seguradoras_revenuelineitems_1revenuelineitems_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa1_sasa_seguradoras_revenuelineitems_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa1_sasa_seguradoras_revenuelineitems_1revenuelineitems_idb',
      ),
    ),
  ),
);
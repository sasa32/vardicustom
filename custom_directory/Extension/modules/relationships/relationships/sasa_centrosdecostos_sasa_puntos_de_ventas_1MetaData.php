<?php
// created: 2022-10-04 17:32:58
$dictionary["sasa_centrosdecostos_sasa_puntos_de_ventas_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_centrosdecostos_sasa_puntos_de_ventas_1' => 
    array (
      'lhs_module' => 'sasa_CentrosDeCostos',
      'lhs_table' => 'sasa_centrosdecostos',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa_Puntos_de_Ventas',
      'rhs_table' => 'sasa_puntos_de_ventas',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1_c',
      'join_key_lhs' => 'sasa_centr37aaecostos_ida',
      'join_key_rhs' => 'sasa_centr2fec_ventas_idb',
    ),
  ),
  'table' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_centr37aaecostos_ida' => 
    array (
      'name' => 'sasa_centr37aaecostos_ida',
      'type' => 'id',
    ),
    'sasa_centr2fec_ventas_idb' => 
    array (
      'name' => 'sasa_centr2fec_ventas_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_centrosdecostos_sasa_puntos_de_ventas_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_centrosdecostos_sasa_puntos_de_ventas_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_centr37aaecostos_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_centrosdecostos_sasa_puntos_de_ventas_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_centr2fec_ventas_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_centr2fec_ventas_idb',
      ),
    ),
  ),
);
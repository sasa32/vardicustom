<?php
// created: 2020-05-15 15:42:24
$dictionary["sasa_departamentos_sasa_municipios_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_departamentos_sasa_municipios_1' => 
    array (
      'lhs_module' => 'sasa_Departamentos',
      'lhs_table' => 'sasa_departamentos',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa_Municipios',
      'rhs_table' => 'sasa_municipios',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_departamentos_sasa_municipios_1_c',
      'join_key_lhs' => 'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida',
      'join_key_rhs' => 'sasa_departamentos_sasa_municipios_1sasa_municipios_idb',
    ),
  ),
  'table' => 'sasa_departamentos_sasa_municipios_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida' => 
    array (
      'name' => 'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida',
      'type' => 'id',
    ),
    'sasa_departamentos_sasa_municipios_1sasa_municipios_idb' => 
    array (
      'name' => 'sasa_departamentos_sasa_municipios_1sasa_municipios_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_departamentos_sasa_municipios_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_departamentos_sasa_municipios_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_departamentos_sasa_municipios_1sasa_departamentos_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_departamentos_sasa_municipios_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_departamentos_sasa_municipios_1sasa_municipios_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_departamentos_sasa_municipios_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_departamentos_sasa_municipios_1sasa_municipios_idb',
      ),
    ),
  ),
);
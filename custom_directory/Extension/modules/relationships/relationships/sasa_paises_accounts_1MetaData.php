<?php
// created: 2020-05-15 15:40:50
$dictionary["sasa_paises_accounts_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_paises_accounts_1' => 
    array (
      'lhs_module' => 'sasa_Paises',
      'lhs_table' => 'sasa_paises',
      'lhs_key' => 'id',
      'rhs_module' => 'Accounts',
      'rhs_table' => 'accounts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_paises_accounts_1_c',
      'join_key_lhs' => 'sasa_paises_accounts_1sasa_paises_ida',
      'join_key_rhs' => 'sasa_paises_accounts_1accounts_idb',
    ),
  ),
  'table' => 'sasa_paises_accounts_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_paises_accounts_1sasa_paises_ida' => 
    array (
      'name' => 'sasa_paises_accounts_1sasa_paises_ida',
      'type' => 'id',
    ),
    'sasa_paises_accounts_1accounts_idb' => 
    array (
      'name' => 'sasa_paises_accounts_1accounts_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_paises_accounts_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_paises_accounts_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_paises_accounts_1sasa_paises_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_paises_accounts_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_paises_accounts_1accounts_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_paises_accounts_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_paises_accounts_1accounts_idb',
      ),
    ),
  ),
);
<?php
// created: 2020-08-03 14:04:12
$dictionary["sasa_municipios_contacts_6"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_municipios_contacts_6' => 
    array (
      'lhs_module' => 'sasa_Municipios',
      'lhs_table' => 'sasa_municipios',
      'lhs_key' => 'id',
      'rhs_module' => 'Contacts',
      'rhs_table' => 'contacts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_municipios_contacts_6_c',
      'join_key_lhs' => 'sasa_municipios_contacts_6sasa_municipios_ida',
      'join_key_rhs' => 'sasa_municipios_contacts_6contacts_idb',
    ),
  ),
  'table' => 'sasa_municipios_contacts_6_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_municipios_contacts_6sasa_municipios_ida' => 
    array (
      'name' => 'sasa_municipios_contacts_6sasa_municipios_ida',
      'type' => 'id',
    ),
    'sasa_municipios_contacts_6contacts_idb' => 
    array (
      'name' => 'sasa_municipios_contacts_6contacts_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_municipios_contacts_6_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_municipios_contacts_6_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_municipios_contacts_6sasa_municipios_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_municipios_contacts_6_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_municipios_contacts_6contacts_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_municipios_contacts_6_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_municipios_contacts_6contacts_idb',
      ),
    ),
  ),
);
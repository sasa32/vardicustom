<?php
// created: 2018-12-04 16:24:59
$dictionary["accounts_contacts_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_contacts_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'Contacts',
      'rhs_table' => 'contacts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_contacts_1_c',
      'join_key_lhs' => 'account_id',
      'join_key_rhs' => 'contact_id',
    ),
  ),
  'table' => 'accounts_contacts_1_c',
  'fields' => 
  array (
    'id' => array(
            'name' => 'id',
            'type' => 'id',
        ),
        'contact_id' => array(
            'name' => 'contact_id',
            'type' => 'id',
        ),
        'account_id' => array(
            'name' => 'account_id',
            'type' => 'id',
        ),
        'date_modified' => array (
            'name' => 'date_modified',
            'type' => 'datetime',
        ),
        'primary_account' => array(
            'name' => 'primary_account',
            'type' => 'bool',
            'default' => '0',
        ),
        'deleted' => array(
            'name' => 'deleted',
            'type' => 'bool',
            'len' => '1',
            'required' => false,
            'default' => '0',
        ),
  ),
  'indices' => 
  array (
		array(
            'name' =>'accounts_contactspk',
            'type' =>'primary',
            'fields'=>array('id'),
        ),
        array(
            'name' => 'idx_account_contact',
            'type'=>'alternate_key',
            'fields'=>array('account_id','contact_id'),
        ),
        array(
            'name' => 'idx_contid_del_accid',
            'type' => 'index',
            'fields'=> array('contact_id', 'deleted', 'account_id'),
        ),
  ),
);

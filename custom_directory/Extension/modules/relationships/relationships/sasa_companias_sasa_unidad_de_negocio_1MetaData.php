<?php
// created: 2020-05-15 23:20:17
$dictionary["sasa_companias_sasa_unidad_de_negocio_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_companias_sasa_unidad_de_negocio_1' => 
    array (
      'lhs_module' => 'sasa_Companias',
      'lhs_table' => 'sasa_companias',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa_Unidad_de_Negocio',
      'rhs_table' => 'sasa_unidad_de_negocio',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_companias_sasa_unidad_de_negocio_1_c',
      'join_key_lhs' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
      'join_key_rhs' => 'sasa_compac399negocio_idb',
    ),
  ),
  'table' => 'sasa_companias_sasa_unidad_de_negocio_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida' => 
    array (
      'name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
      'type' => 'id',
    ),
    'sasa_compac399negocio_idb' => 
    array (
      'name' => 'sasa_compac399negocio_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_companias_sasa_unidad_de_negocio_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_companias_sasa_unidad_de_negocio_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_companias_sasa_unidad_de_negocio_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_compac399negocio_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_companias_sasa_unidad_de_negocio_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_compac399negocio_idb',
      ),
    ),
  ),
);
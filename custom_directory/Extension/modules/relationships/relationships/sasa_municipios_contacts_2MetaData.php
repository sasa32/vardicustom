<?php
// created: 2020-05-15 16:09:12
$dictionary["sasa_municipios_contacts_2"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_municipios_contacts_2' => 
    array (
      'lhs_module' => 'sasa_Municipios',
      'lhs_table' => 'sasa_municipios',
      'lhs_key' => 'id',
      'rhs_module' => 'Contacts',
      'rhs_table' => 'contacts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_municipios_contacts_2_c',
      'join_key_lhs' => 'sasa_municipios_contacts_2sasa_municipios_ida',
      'join_key_rhs' => 'sasa_municipios_contacts_2contacts_idb',
    ),
  ),
  'table' => 'sasa_municipios_contacts_2_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_municipios_contacts_2sasa_municipios_ida' => 
    array (
      'name' => 'sasa_municipios_contacts_2sasa_municipios_ida',
      'type' => 'id',
    ),
    'sasa_municipios_contacts_2contacts_idb' => 
    array (
      'name' => 'sasa_municipios_contacts_2contacts_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_municipios_contacts_2_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_municipios_contacts_2_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_municipios_contacts_2sasa_municipios_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_municipios_contacts_2_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_municipios_contacts_2contacts_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_municipios_contacts_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_municipios_contacts_2contacts_idb',
      ),
    ),
  ),
);
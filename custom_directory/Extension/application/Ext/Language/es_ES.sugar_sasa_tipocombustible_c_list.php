<?php
 // created: 2023-01-23 20:53:26

$app_list_strings['sasa_tipocombustible_c_list']=array (
  1 => 'GASOLINA',
  2 => 'GAS NATURAL VEHICULAR (GNV)',
  3 => 'DIESEL',
  4 => 'GAS - GASOLINA',
  5 => 'ELECTRICO',
  6 => 'HIDROGENO',
  7 => 'ETANOL',
  8 => 'BIODIESEL',
  9 => 'GLP',
  10 => 'GASOLINA - ELECTRICO',
  11 => 'DIESEL - ELECTRICO',
);
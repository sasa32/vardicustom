<?php
 // created: 2020-05-16 10:27:22

$app_list_strings['sasa_nivel_educativo_c_list']=array (
  '' => '',
  11 => 'DIPLOMADO',
  12 => 'DOCTORADO',
  1 => 'JARDÍN',
  7 => 'MAESTRÍA',
  8 => 'MBA',
  6 => 'POSGRADO',
  2 => 'PRIMARIA',
  5 => 'PROFESIONAL',
  3 => 'SECUNDARIA',
  4 => 'TÉCNICO/TECNÓLOGO',
);
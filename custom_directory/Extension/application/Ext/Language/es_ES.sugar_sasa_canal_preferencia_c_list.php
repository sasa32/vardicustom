<?php 
$app_list_strings['sasa_canal_preferencia_c_list'] = array (
  '' => '',
  'CORREO ELECTRONICO' => 'CORREO ELECTRÓNICO',
  'DIRECCION FISICA' => 'DIRECCIÓN FÍSICA',
  'LLAMADA' => 'LLAMADA',
  'NINGUNA' => 'NINGUNA',
  'SMS' => 'SMS',
  'TELEFONO FIJO' => 'TELÉFONO FIJO',
  'WHATSAPP' => 'WHATSAPP',
);
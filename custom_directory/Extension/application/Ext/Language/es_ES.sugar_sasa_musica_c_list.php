<?php
 // created: 2020-02-26 12:48:58

$app_list_strings['sasa_musica_c_list']=array (
  '' => '',
  1 => 'BOLERO',
  2 => 'CLÁSICA',
  3 => 'COLOMBIANA',
  5 => 'JAZZ',
  6 => 'MERENGUE',
  7 => 'METAL',
  9 => 'POP',
  11 => 'RANCHERA',
  14 => 'REGUETÓN',
  16 => 'ROCK',
  17 => 'ROCK EN ESPAÑOL',
  19 => 'SALSA',
  22 => 'VALLENATO',
);
<?php
 // created: 2021-02-02 11:59:13

$app_list_strings['sasa_tipo_cases_c_list']=array (
  '' => '',
  'NA' => 'N/A',
  'H' => 'FELICITACIÓN',
  'E' => 'INFORMACIÓN',
  'G' => 'OTROS',
  'A' => 'PETICIÓN',
  'B' => 'QUEJA',
  'C' => 'RECLAMO',
  'F' => 'SOLICITUD',
  'D' => 'SUGERENCIA',
);
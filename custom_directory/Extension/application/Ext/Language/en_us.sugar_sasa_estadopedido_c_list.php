<?php
 // created: 2022-11-28 20:36:16

$app_list_strings['sasa_estadopedido_c_list']=array (
  'P' => 'PEDIDO',
  'R' => 'RESERVADO',
  'E' => 'LISTA DE ESPERA',
  'A' => 'ASIGNADO',
  'F' => 'FACTURADO',
  'D' => 'DESTRATE',
  'B' => 'BLOQUEADO',
  'S' => 'DESASIGNADO',
  'X' => 'CANCELADO',
  'K' => 'FÁBRICA',
);
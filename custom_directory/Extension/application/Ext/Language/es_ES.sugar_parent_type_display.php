<?php
// created: 2022-11-23 10:51:57
$app_list_strings['parent_type_display']['Accounts'] = 'Cuenta';
$app_list_strings['parent_type_display']['Contacts'] = 'Contacto';
$app_list_strings['parent_type_display']['Tasks'] = 'Tarea';
$app_list_strings['parent_type_display']['Opportunities'] = 'Cotización';
$app_list_strings['parent_type_display']['Products'] = 'Línea de la Oferta';
$app_list_strings['parent_type_display']['Quotes'] = 'Presupuesto';
$app_list_strings['parent_type_display']['Bugs'] = 'Incidencias';
$app_list_strings['parent_type_display']['Cases'] = 'Caso';
$app_list_strings['parent_type_display']['Leads'] = 'Lead';
$app_list_strings['parent_type_display']['Project'] = 'Proyecto';
$app_list_strings['parent_type_display']['ProjectTask'] = 'Tarea de Proyecto';
$app_list_strings['parent_type_display']['Prospects'] = 'Público Objetivo';
$app_list_strings['parent_type_display']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['parent_type_display']['Notes'] = 'Nota';
$app_list_strings['parent_type_display']['PurchasedLineItems'] = 'Elemento comprado';
$app_list_strings['parent_type_display']['Purchases'] = 'Compra';
$app_list_strings['parent_type_display']['Escalations'] = 'Escalada';

<?php
 // created: 2020-01-31 00:19:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Ülesanne',
  'Opportunities' => 'Võimalus',
  'Products' => 'Pakkumuse artikkel',
  'Quotes' => 'Pakkumus',
  'Bugs' => 'Vead',
  'Cases' => 'Juhtum',
  'Leads' => 'Müügivihje',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projekti ülesanne',
  'Prospects' => 'Eesmärk',
  'KBContents' => 'Teadmusbaas',
  'Notes' => 'Märkus',
  'RevenueLineItems' => 'Tuluartiklid',
);
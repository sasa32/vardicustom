<?php
 // created: 2020-05-16 10:32:54

$app_list_strings['sasa_estado_civil_c_list']=array (
  '' => '',
  'C' => 'CASADO',
  'D' => 'DIVORCIADO',
  'S' => 'SOLTERO',
  'U' => 'UNIÓN LIBRE',
  'V' => 'VIUDO',
);
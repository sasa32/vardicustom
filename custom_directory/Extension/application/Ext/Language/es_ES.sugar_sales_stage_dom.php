<?php
 // created: 2023-02-07 21:24:51

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospección',
  'Qualification' => 'Calificación',
  'Needs Analysis' => 'Análisis de necesidades',
  'Value Proposition' => 'Propuesta de valor',
  'Id. Decision Makers' => 'Identificación de responsables',
  'Perception Analysis' => 'Análisis de percepción',
  'Proposal/Price Quote' => 'Cotización',
  'Negotiation/Review' => 'Negociación/Revisión',
  'Closed Won' => 'Cerrado',
  'Closed Lost' => 'Perdida',
);
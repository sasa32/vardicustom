<?php
 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Account',
  'Opportunities' => 'Opportunity',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Contacts' => 'Contacts',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Quote',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'Prospects' => 'Target',
  'ProjectTask' => 'Project Task',
  'Tasks' => 'Task',
  'KBContents' => 'Knowledge Base',
  'Notes' => 'Note',
  'RevenueLineItems' => 'Revenue Line Items',
);
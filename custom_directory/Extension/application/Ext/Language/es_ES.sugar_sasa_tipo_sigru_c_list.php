<?php
 // created: 2020-05-16 08:44:07

$app_list_strings['sasa_tipo_sigru_c_list']=array (
  '' => '',
  'A' => 'AGENTE EN ZONA DE IN',
  'S' => 'ASEGURADORAS',
  'C' => 'CLIENTE CORRIENTE',
  'X' => 'CLIENTE DEL EXTERIOR',
  'E' => 'CLIENTE ESPECIAL',
  'F' => 'CLIENTE ESPECIAL TAS',
  'O' => 'CLIENTE OFICIAL',
  'Z' => 'CLIENTE OTRA MARCA',
  'Y' => 'CLIENTE TAXI',
  'K' => 'CONCESIONARIO',
  'G' => 'CONCESIONARIO DSCTO',
  'M' => 'EMPLEADOS',
  'V' => 'FAMILIA VARGAS',
  'J' => 'FLOTILLAS',
  'N' => 'GRUPO NOVARTIS',
  'R' => 'GUBERNAMENTAL',
  'P' => 'PROVEEDOR',
  'B' => 'SOTRAMAR',
  'I' => 'TALLER INDEPENDIENTE',
  'D' => 'TALLERES AUTORIZADOS',
  'T' => 'TRANSPORTADORES',
);
<?php
 // created: 2021-10-19 11:11:08

$app_list_strings['status_list']=array (
  '' => '',
  'A' => 'NUEVO',
  'B' => 'ASIGNADO',
  'C' => 'EN PROCESO COMERCIAL',
  'D' => 'CLIENTE/PROSPECTO EXISTENTE',
  'E' => 'REASIGNADO',
  'F' => 'SIN INTERÉS',
  'G' => 'GESTIONADO CALL',
  'H' => 'COTIZADO',
  'I' => 'CONVERTIDO',
  'J' => 'NO CONTACTADO',
);
<?php
 // created: 2023-02-01 23:08:38

$app_list_strings['sasa_estadocotizacion_c_list']=array (
  'AB' => 'ABIERTA',
  'FP' => 'FACTURADA PARCIAL',
  'FT' => 'FACTURADA TOTAL',
  'PA' => 'PEDIDO APLICADO',
  'PF' => 'PEDIDO FACTURADO',
  'PP' => 'PEDIDO PENDIENTE',
  'RR' => 'RECHAZADA',
  'VP' => 'VENTA PERDIDA',
  'I' => 'INACTIVA',
  'N' => 'NO APROBADA',
  'P' => 'CON PEDIDO',
  'S' => 'VIGENTE',
  'X' => 'VENCIDA',
  'F' => 'FACTURADA',
  'D' => 'VENTA DEVUELTA',
  'V' => 'VENCIDA',
  'A' => 'APROBADA',
  'PD' => 'PENDIENTE DECISION',
  'C' => 'CERRADA',
);
<?php
 // created: 2023-02-01 23:08:38

$app_list_strings['sasa_estadocotizacion_c_list']=array (
  'AB' => 'ABIERTA',
  'FP' => 'FACTURADA PARCIAL',
  'FT' => 'FACTURADA TOTAL',
  'PA' => 'PEDIDO ANULADO',
  'PF' => 'PENDIENTE FACTURAR',
  'PP' => 'PEDIDO PENDIENTE',
  'RR' => 'REEMPLAZADA',
  'VP' => 'VENTA PERDIDA',
  'PD' => 'PENDIENTE DECISION',
  'I' => 'INACTIVA',
  'N' => 'NO APROBADA',
  'P' => 'CON PEDIDO',
  'S' => 'SOLICITADA',
  'X' => 'ANULADA',
  'F' => 'FACTURADA',
  'D' => 'VENTA DEVUELTA',
  'V' => 'VENCIDA',
  'A' => 'APROBADA',
  'C' => 'CERRADA',
);
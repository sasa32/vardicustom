<?php
// created: 2022-11-23 10:51:57
$app_list_strings['record_type_display_notes']['Accounts'] = 'Cuenta';
$app_list_strings['record_type_display_notes']['Contacts'] = 'Contacto';
$app_list_strings['record_type_display_notes']['Opportunities'] = 'Cotización';
$app_list_strings['record_type_display_notes']['Tasks'] = 'Tarea';
$app_list_strings['record_type_display_notes']['ProductTemplates'] = 'Catálogo de Productos';
$app_list_strings['record_type_display_notes']['Quotes'] = 'Presupuesto';
$app_list_strings['record_type_display_notes']['Products'] = 'Línea de la Oferta';
$app_list_strings['record_type_display_notes']['Contracts'] = 'Contrato';
$app_list_strings['record_type_display_notes']['Emails'] = 'Correo electrónico';
$app_list_strings['record_type_display_notes']['Bugs'] = 'Incidencia';
$app_list_strings['record_type_display_notes']['Project'] = 'Proyecto';
$app_list_strings['record_type_display_notes']['ProjectTask'] = 'Tarea de Proyecto';
$app_list_strings['record_type_display_notes']['Prospects'] = 'Público Objetivo';
$app_list_strings['record_type_display_notes']['Cases'] = 'Caso';
$app_list_strings['record_type_display_notes']['Leads'] = 'Cliente Potencial';
$app_list_strings['record_type_display_notes']['Meetings'] = 'Reunión';
$app_list_strings['record_type_display_notes']['Calls'] = 'Llamada';
$app_list_strings['record_type_display_notes']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['record_type_display_notes']['PurchasedLineItems'] = 'Elemento comprado';
$app_list_strings['record_type_display_notes']['Purchases'] = 'Compra';
$app_list_strings['record_type_display_notes']['Escalations'] = 'Escalada';

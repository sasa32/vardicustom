<?php
 // created: 2020-01-31 00:19:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Uppgift',
  'Opportunities' => 'Affärsmöjlighet',
  'Products' => 'Produkt',
  'Quotes' => 'Offert',
  'Bugs' => 'Buggar',
  'Cases' => 'Ärende',
  'Leads' => 'Möjlig kund',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektuppgift',
  'Prospects' => 'Mål',
  'KBContents' => 'Kunskapsbas',
  'Notes' => 'Anteckning',
  'RevenueLineItems' => 'Intäktsposter',
);
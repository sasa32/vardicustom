<?php
 // created: 2020-10-08 08:02:21

$app_list_strings['sasa_tipo_tipi_c_list']=array (
  '' => '',
  'A' => 'PETICIÓN',
  'B' => 'QUEJA',
  'C' => 'RECLAMO',
  'D' => 'SUGERENCIA',
  'E' => 'INFORMACIÓN',
  'F' => 'SOLICITUD',
  'G' => 'OTROS',
  'H' => 'FELICITACIÓN',
);
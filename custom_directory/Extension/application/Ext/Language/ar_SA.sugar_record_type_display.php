<?php
 // created: 2020-01-31 00:19:11

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'الحساب',
  'Opportunities' => 'الفرصة',
  'Cases' => 'الحالة',
  'Leads' => 'العميل المتوقع',
  'Contacts' => 'جهات الاتصال',
  'Products' => 'البند المسعر',
  'Quotes' => 'عرض السعر',
  'Bugs' => 'الخطأ',
  'Project' => 'المشروع',
  'Prospects' => 'الهدف',
  'ProjectTask' => 'مهمة المشروع',
  'Tasks' => 'المهمة',
  'KBContents' => 'قاعدة المعارف',
  'Notes' => 'ملاحظة',
  'RevenueLineItems' => 'بنود العائدات',
);
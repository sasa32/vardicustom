<?php
 // created: 2021-05-17 16:49:57

$app_list_strings['task_status_dom']=array (
  'Not Started' => 'NO INICIADA',
  'In Progress' => 'EN PROGRESO',
  'Completed' => 'COMPLETADA ',
  'Pending Input' => 'PENDIENTE DE INFORMACIÓN ',
  'Deferred' => 'APLAZADA ',
  'Cancelada' => 'CANCELADA',
);
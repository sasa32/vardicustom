<?php
 // created: 2023-05-10 16:23:44

$app_list_strings['source_cases_list_c']=array (
  '' => '',
  'A' => 'APP',
  'C' => 'CARTA',
  'E' => 'EMAIL',
  'V' => 'FORMATO VERBAL',
  'T' => 'LLAMADA',
  'S' => 'REDES SOCIALES',
  'W' => 'SITIO WEB',
  'P' => 'WHATSAPP',
  'R' => 'TUCARRO.COM',
);
<?php
 // created: 2020-11-06 16:13:08

$app_list_strings['case_status_dom']=array (
  '' => '',
  'Assigned' => 'ASIGNADO',
  'Rejected' => 'CANCELADO',
  'Closed' => 'CERRADO',
  'Duplicate' => 'DUPLICADO',
  'New' => 'NUEVO',
  'Pending Input' => 'PENDIENTE DE INFORMACIÓN',
  'Pending Client' => 'PENDIENTE RESPUESTA CLIENTE',
);
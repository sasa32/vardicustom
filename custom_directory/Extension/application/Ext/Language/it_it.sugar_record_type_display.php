<?php
 // created: 2020-01-31 00:19:10

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Azienda',
  'Opportunities' => 'Opportunità',
  'Cases' => 'Reclamo',
  'Leads' => 'Lead',
  'Contacts' => 'Contatti',
  'Products' => 'Prodotto',
  'Quotes' => 'Offerta',
  'Bugs' => 'Bug',
  'Project' => 'Progetto',
  'Prospects' => 'Obiettivo',
  'ProjectTask' => 'Compiti di Progetto',
  'Tasks' => 'Compito',
  'KBContents' => 'Knowledge Base',
  'Notes' => 'Nota',
  'RevenueLineItems' => 'Elementi dell´Opportunità',
);
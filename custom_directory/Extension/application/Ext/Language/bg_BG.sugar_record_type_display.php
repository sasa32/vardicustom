<?php
 // created: 2020-01-31 00:19:10

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Организация',
  'Opportunities' => 'Възможност',
  'Cases' => 'Казус',
  'Leads' => 'Потенциален клиент',
  'Contacts' => 'Контакти',
  'Products' => 'Офериран продукт',
  'Quotes' => 'Оферта',
  'Bugs' => 'Проблем',
  'Project' => 'Проекти',
  'Prospects' => 'Целеви клиент',
  'ProjectTask' => 'Задача по проект',
  'Tasks' => 'Задача',
  'KBContents' => 'База от знания',
  'Notes' => 'Бележка',
  'RevenueLineItems' => 'Приходни позиции',
);
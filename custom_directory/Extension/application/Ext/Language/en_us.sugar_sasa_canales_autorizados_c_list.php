<?php
 // created: 2021-01-04 16:50:35

$app_list_strings['sasa_canales_autorizados_c_list']=array (
  '' => '',
  '1' => 'CELULAR',
  '6' => 'CORREO ELECTRÓNICO',
  '2' => 'DIRECCIÓN FÍSICA',
  '5' => 'FAX',
  '0' => 'NINGUNA',
  '3' => 'OFICINA',
  '7' => 'PERSONALMENTE',
  '8' => 'SMS',
  '4' => 'TELÉFONO FIJO',
  '9' => 'WHATSAPP',
);
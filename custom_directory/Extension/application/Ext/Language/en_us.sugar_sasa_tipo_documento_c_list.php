<?php
 // created: 2020-05-11 18:53:24

$app_list_strings['sasa_tipo_documento_c_list']=array (
  '' => '',
  'D' => 'CARNÉ DIPLOMÁTICO',
  'C' => 'CÉDULA DE CIUDADANÍA',
  'E' => 'CÉDULA DE EXTRANJERÍA',
  'U' => 'N.U.I.P',
  'N' => 'NIT',
  'P' => 'PASAPORTE',
  'T' => 'TARJETA DE IDENTIDAD',
);
<?php 
$app_list_strings['sasa_canal_preferencia_c_list'] = array (
  '' => '',
  'NINGUNA' => 'NINGUNA',
  'LLAMADA' => 'LLAMADA',
  'DIRECCION FISICA' => 'DIRECCIÓN FÍSICA',
  'TELEFONO FIJO' => 'TELÉFONO FIJO',
  'CORREO ELECTRONICO' => 'CORREO ELECTRÓNICO',
  'SMS' => 'SMS',
  'WHATSAPP' => 'WHATSAPP',
);
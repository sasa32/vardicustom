<?php
 // created: 2020-01-31 00:19:10

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Firma',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Aufgabe',
  'Opportunities' => 'Verkaufschance',
  'Products' => 'Produkt',
  'Quotes' => 'Angebot',
  'Bugs' => 'Fehler',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektaufgabe',
  'Prospects' => 'Zielkontakt',
  'KBContents' => 'Wissensdatenbank',
  'Notes' => 'Hinweis',
  'RevenueLineItems' => 'Umsatzposten',
);
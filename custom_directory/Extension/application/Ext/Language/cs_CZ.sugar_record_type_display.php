<?php
 // created: 2020-01-31 00:19:10

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Účet',
  'Opportunities' => 'Příležitost',
  'Cases' => 'Případ:',
  'Leads' => 'Příležitost',
  'Contacts' => 'Kontakty',
  'Products' => 'Produkt',
  'Quotes' => 'Nabídka',
  'Bugs' => 'Chyba:',
  'Project' => 'Projekty',
  'Prospects' => 'Kontakt',
  'ProjectTask' => 'Projektové úkoly',
  'Tasks' => 'Úkol',
  'KBContents' => 'Znalostní báze',
  'Notes' => 'Poznámka',
  'RevenueLineItems' => 'Řádky tržby',
);
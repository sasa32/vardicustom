<?php
 // created: 2020-03-11 10:32:53

$app_list_strings['sasa_cantidad_empleados_c_list']=array (
  '' => '',
  1 => 'HASTA 10',
  2 => 'DESDE 11 HASTA 50',
  3 => 'DESDE 51 HASTA 100',
  4 => 'DESDE 101 HASTA 250',
  5 => 'DESDE 251 HASTA 500',
  6 => 'DESDE 501 HASTA 1000',
  7 => 'DESDE 1001 Y MÁS',
);
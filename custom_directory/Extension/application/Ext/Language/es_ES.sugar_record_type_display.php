<?php
// created: 2022-11-23 10:51:57
$app_list_strings['record_type_display'][''] = '';
$app_list_strings['record_type_display']['Accounts'] = 'Cuenta';
$app_list_strings['record_type_display']['Opportunities'] = 'Cotización';
$app_list_strings['record_type_display']['Cases'] = 'Caso';
$app_list_strings['record_type_display']['Leads'] = 'Cliente Potencial';
$app_list_strings['record_type_display']['Contacts'] = 'Contacto';
$app_list_strings['record_type_display']['Products'] = 'Línea de la Oferta';
$app_list_strings['record_type_display']['Quotes'] = 'Presupuesto';
$app_list_strings['record_type_display']['Bugs'] = 'Incidencia';
$app_list_strings['record_type_display']['Project'] = 'Proyecto';
$app_list_strings['record_type_display']['Prospects'] = 'Público Objetivo';
$app_list_strings['record_type_display']['ProjectTask'] = 'Tarea de Proyecto';
$app_list_strings['record_type_display']['Tasks'] = 'Tarea';
$app_list_strings['record_type_display']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['record_type_display']['Notes'] = 'Nota';

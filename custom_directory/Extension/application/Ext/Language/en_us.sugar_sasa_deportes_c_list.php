<?php
 // created: 2020-05-16 10:34:47

$app_list_strings['sasa_deportes_c_list']=array (
  '' => '',
  2 => 'ATLETISMO',
  3 => 'AUTOMOVILISMO',
  10 => 'CICLISMO',
  13 => 'FÚTBOL',
  16 => 'GOLF',
  21 => 'MONTAÑISMO',
  22 => 'MOTOCICLISMO',
  26 => 'PESCA',
  31 => 'TENIS',
  '0_' => 'NINGÚNO',
);
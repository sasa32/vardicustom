<?php
 // created: 2020-11-09 10:04:06

$app_list_strings['reminder_time_options']=array (
  '' => '',
  -1 => 'NINGUNA',
  60 => '1 MINUTO ANTES ',
  300 => '5 MINUTOS ANTES ',
  600 => '10 MINUTOS ANTES ',
  900 => '15 MINUTOS ANTES ',
  1800 => '30 MINUTOS ANTES ',
  3600 => '1 HORA ANTES ',
  7200 => '2 HORAS ANTES ',
  10800 => '3 HORAS ANTES ',
  18000 => '5 HORAS ANTES ',
  86400 => '1 DÍA ANTES ',
);
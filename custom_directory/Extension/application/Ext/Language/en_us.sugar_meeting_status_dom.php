<?php
 // created: 2020-11-06 17:11:23

$app_list_strings['meeting_status_dom']=array (
  '' => '',
  'Planned' => 'Planificado',
  'Held' => 'Realizada',
  'Not Held' => 'Cancelada',
  'Rescheduled' => 'REPROGRAMADA',
);
<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_Paises'] = 'Países';
$app_list_strings['moduleList']['sasa_Departamentos'] = 'Departamentos';
$app_list_strings['moduleList']['sasa_Municipios'] = 'Municipios';
$app_list_strings['moduleListSingular']['sasa_Paises'] = 'País';
$app_list_strings['moduleListSingular']['sasa_Departamentos'] = 'Departamento';
$app_list_strings['moduleListSingular']['sasa_Municipios'] = 'Municipio';
$app_list_strings['sasa_estado_c_list']['Activo'] = 'Activo';
$app_list_strings['sasa_estado_c_list']['Inactivo'] = 'Inactivo';
$app_list_strings['sasa_estado_c_list'][''] = '';

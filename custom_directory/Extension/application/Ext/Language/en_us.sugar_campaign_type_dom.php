<?php
// created: 2020-06-17 10:08:34
$app_list_strings['campaign_type_dom'][''] = '';
$app_list_strings['campaign_type_dom']['Telesales'] = 'Televentas';
$app_list_strings['campaign_type_dom']['Mail'] = 'Correo';
$app_list_strings['campaign_type_dom']['Email'] = 'Correo electrónico';
$app_list_strings['campaign_type_dom']['Print'] = 'Imprimir';
$app_list_strings['campaign_type_dom']['Web'] = 'Web';
$app_list_strings['campaign_type_dom']['Radio'] = 'Radio';
$app_list_strings['campaign_type_dom']['Television'] = 'Televisión';
$app_list_strings['campaign_type_dom']['NewsLetter'] = 'Boletín de Noticias';
$app_list_strings['campaign_type_dom']['999'] = 'Indefinido';

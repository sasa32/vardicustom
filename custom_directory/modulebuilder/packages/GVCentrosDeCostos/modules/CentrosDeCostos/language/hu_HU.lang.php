<?php
// created: 2023-04-25 06:24:36
$mod_strings = array (
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_TAGS_LINK' => 'Címkék',
  'LBL_TAGS' => 'Címkék',
  'LBL_ID' => 'Azonosító',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_DOC_OWNER' => 'Dokumentum tulajdonosa',
  'LBL_USER_FAVORITES' => 'Felhasználók, akik kedvelték',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Módosítva név szerint',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Név által létrehozva',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Centros De Costos Lista',
  'LBL_MODULE_NAME' => 'Centros De Costos',
  'LBL_MODULE_TITLE' => 'Centros De Costos',
  'LBL_MODULE_NAME_SINGULAR' => 'Centros De Costos',
  'LBL_HOMEPAGE_TITLE' => 'Saját Centros De Costos',
  'LNK_NEW_RECORD' => 'Új létrehozása Centros De Costos',
  'LNK_LIST' => 'Megtekintés Centros De Costos',
  'LNK_IMPORT_SASA_SASA_CENTROCOSTOS_C' => 'Importar Centros De Costos',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés Centros De Costos',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_SASA_SASA_CENTROCOSTOS_C_SUBPANEL_TITLE' => 'Centros De Costos',
  'LBL_NEW_FORM_TITLE' => 'Új Centros De Costos',
  'LNK_IMPORT_VCARD' => 'Importar Centros De Costos vCard',
  'LBL_IMPORT' => 'Importar Centros De Costos',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Centros De Costos record by importing a vCard from your file system.',
  'LBL_SASA_SASA_CENTROCOSTOS_C_FOCUS_DRAWER_DASHBOARD' => 'Centros De Costos Panel de enfoque',
  'LBL_SASA_SASA_CENTROCOSTOS_C_RECORD_DASHBOARD' => 'Centros De Costos Cuadro de mando del registro',
  'LNK_IMPORT_SASA_CENTROSDECOSTOS' => 'Importar Centros De Costos',
  'LBL_SASA_CENTROSDECOSTOS_SUBPANEL_TITLE' => 'Centros De Costos',
  'LBL_SASA_CENTROSDECOSTOS_FOCUS_DRAWER_DASHBOARD' => 'Centros De Costos Panel de enfoque',
  'LBL_SASA_CENTROSDECOSTOS_RECORD_DASHBOARD' => 'Centros De Costos Cuadro de mando del registro',
);
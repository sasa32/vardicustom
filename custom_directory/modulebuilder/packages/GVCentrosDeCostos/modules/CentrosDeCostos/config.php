<?php
// created: 2023-04-25 06:24:35
$config = array (
  'team_security' => true,
  'assignable' => true,
  'taggable' => 1,
  'acl' => true,
  'has_tab' => true,
  'studio' => true,
  'audit' => true,
  'activity_enabled' => 0,
  'templates' => 
  array (
    'basic' => 1,
  ),
  'label' => 'Centros De Costos',
  'label_singular' => 'Centros De Costos',
  'importable' => true,
);
<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χρήστη',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_TAGS_LINK' => 'Ετικέτες',
  'LBL_TAGS' => 'Ετικέτες',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Κάτοχος του εγγράφου',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε Από Χρήστη',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χρήστη',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Δημιουργήθηκε με όνομα',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Aseguradoras Λίστα',
  'LBL_MODULE_NAME' => 'Aseguradoras',
  'LBL_MODULE_TITLE' => 'Aseguradoras',
  'LBL_MODULE_NAME_SINGULAR' => 'Aseguradora',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Aseguradoras',
  'LNK_NEW_RECORD' => 'Δημιουργία Aseguradora',
  'LNK_LIST' => 'Προβολή Aseguradoras',
  'LNK_IMPORT_SASA1_SASA_SEGURADORAS' => 'Importar Aseguradoras',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Aseguradora',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_SASA1_SASA_SEGURADORAS_SUBPANEL_TITLE' => 'Aseguradoras',
  'LBL_NEW_FORM_TITLE' => 'Νέα Aseguradora',
  'LNK_IMPORT_VCARD' => 'Importar Aseguradora vCard',
  'LBL_IMPORT' => 'Importar Aseguradoras',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Aseguradora record by importing a vCard from your file system.',
  'LBL_SASA1_SASA_SEGURADORAS_FOCUS_DRAWER_DASHBOARD' => 'Aseguradoras Panel de enfoque',
  'LBL_SASA1_SASA_SEGURADORAS_RECORD_DASHBOARD' => 'Aseguradoras Cuadro de mando del registro',
);
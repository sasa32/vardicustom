<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Týmy',
  'LBL_TEAMS' => 'Týmy',
  'LBL_TEAM_ID' => 'ID týmu',
  'LBL_ASSIGNED_TO_ID' => 'Přiřazené uživateli s ID',
  'LBL_ASSIGNED_TO_NAME' => 'Přiřazeno komu',
  'LBL_TAGS_LINK' => 'Tagy',
  'LBL_TAGS' => 'Tagy',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum zahájení',
  'LBL_DATE_MODIFIED' => 'Datum poslední úpravy',
  'LBL_MODIFIED' => 'Modifikováno kym',
  'LBL_MODIFIED_ID' => 'Upraveno podle ID',
  'LBL_MODIFIED_NAME' => 'Upraveno uživatelem',
  'LBL_CREATED' => 'Vytvořil:',
  'LBL_CREATED_ID' => 'Vytvořeno podle ID',
  'LBL_DOC_OWNER' => 'Vlastník dokumentu',
  'LBL_USER_FAVORITES' => 'Uživatelé, jejichž je oblíbeným',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Odstranit',
  'LBL_NAME' => 'Název',
  'LBL_CREATED_USER' => 'Vytvořeno uživatelem',
  'LBL_MODIFIED_USER' => 'Změněno uživatelem',
  'LBL_LIST_NAME' => 'Název',
  'LBL_EDIT_BUTTON' => 'Editace',
  'LBL_REMOVE' => 'Odstranit',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Upraveno uživatelem',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Vytvořil(a) (jméno)',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Aseguradoras Celk. cena',
  'LBL_MODULE_NAME' => 'Aseguradoras',
  'LBL_MODULE_TITLE' => 'Aseguradoras',
  'LBL_MODULE_NAME_SINGULAR' => 'Aseguradora',
  'LBL_HOMEPAGE_TITLE' => 'Moje Aseguradoras',
  'LNK_NEW_RECORD' => 'Přidat Aseguradora',
  'LNK_LIST' => 'Zobrazit Aseguradoras',
  'LNK_IMPORT_SASA1_SASA_SEGURADORAS' => 'Importar Aseguradoras',
  'LBL_SEARCH_FORM_TITLE' => 'Hledat Aseguradora',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobrazit historii',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_SASA1_SASA_SEGURADORAS_SUBPANEL_TITLE' => 'Aseguradoras',
  'LBL_NEW_FORM_TITLE' => 'Nový Aseguradora',
  'LNK_IMPORT_VCARD' => 'Importar Aseguradora vCard',
  'LBL_IMPORT' => 'Importar Aseguradoras',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Aseguradora record by importing a vCard from your file system.',
  'LBL_SASA1_SASA_SEGURADORAS_FOCUS_DRAWER_DASHBOARD' => 'Aseguradoras Panel de enfoque',
  'LBL_SASA1_SASA_SEGURADORAS_RECORD_DASHBOARD' => 'Aseguradoras Cuadro de mando del registro',
);
<?php
// created: 2023-04-25 06:24:36
$mod_strings = array (
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χρήστη',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_TAGS_LINK' => 'Ετικέτες',
  'LBL_TAGS' => 'Ετικέτες',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Κάτοχος του εγγράφου',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε Από Χρήστη',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χρήστη',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Δημιουργήθηκε με όνομα',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Costos Λίστα',
  'LBL_MODULE_NAME' => 'Costos',
  'LBL_MODULE_TITLE' => 'Costos',
  'LBL_MODULE_NAME_SINGULAR' => 'Costos',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Costos',
  'LNK_NEW_RECORD' => 'Δημιουργία Costos',
  'LNK_LIST' => 'Προβολή Costos',
  'LNK_IMPORT_SASA_SASA_CENTROCOSTOS_C' => 'Importar sasa_centrocostos_c sasa_centrocostos_c',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Costos',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_SASA_SASA_CENTROCOSTOS_C_SUBPANEL_TITLE' => 'sasa_centrocostos_c',
  'LBL_NEW_FORM_TITLE' => 'Νέα Costos',
  'LNK_IMPORT_VCARD' => 'Importar Costos vCard',
  'LBL_IMPORT' => 'Importar Costos',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Costos record by importing a vCard from your file system.',
  'LBL_SASA_SASA_CENTROCOSTOS_C_FOCUS_DRAWER_DASHBOARD' => 'sasa_centrocostos_c Panel de enfoque',
  'LBL_SASA_SASA_CENTROCOSTOS_C_RECORD_DASHBOARD' => 'sasa_centrocostos_c Cuadro de mando del registro',
  'LNK_IMPORT_SASA_COSTOS' => 'Importar Costos',
  'LBL_SASA_COSTOS_SUBPANEL_TITLE' => 'Costos',
  'LBL_SASA_COSTOS_FOCUS_DRAWER_DASHBOARD' => 'Costos Panel de enfoque',
  'LBL_SASA_COSTOS_RECORD_DASHBOARD' => 'Costos Cuadro de mando del registro',
  'LBL_SASA_CODCENTRODECOSTOS_C' => 'Código Centro de Costos',
);
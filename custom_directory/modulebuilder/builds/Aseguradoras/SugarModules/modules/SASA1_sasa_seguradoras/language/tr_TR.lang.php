<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_TAGS_LINK' => 'Etiketler',
  'LBL_TAGS' => 'Etiketler',
  'LBL_ID' => 'KİMLİK',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_DOC_OWNER' => 'Doküman Sahibi',
  'LBL_USER_FAVORITES' => 'Favori Olan Kullanıcılar',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_DELETED' => 'Silindi',
  'LBL_NAME' => 'İsim',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_REMOVE' => 'Sil',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Oluşturan Adı',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Aseguradoras Liste',
  'LBL_MODULE_NAME' => 'Aseguradoras',
  'LBL_MODULE_TITLE' => 'Aseguradoras',
  'LBL_MODULE_NAME_SINGULAR' => 'Aseguradora',
  'LBL_HOMEPAGE_TITLE' => 'Benim Aseguradoras',
  'LNK_NEW_RECORD' => 'Oluştur Aseguradora',
  'LNK_LIST' => 'Göster Aseguradoras',
  'LNK_IMPORT_SASA1_SASA_SEGURADORAS' => 'Importar Aseguradoras',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Aseguradora',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_SASA1_SASA_SEGURADORAS_SUBPANEL_TITLE' => 'Aseguradoras',
  'LBL_NEW_FORM_TITLE' => 'Yeni Aseguradora',
  'LNK_IMPORT_VCARD' => 'Importar Aseguradora vCard',
  'LBL_IMPORT' => 'Importar Aseguradoras',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Aseguradora record by importing a vCard from your file system.',
  'LBL_SASA1_SASA_SEGURADORAS_FOCUS_DRAWER_DASHBOARD' => 'Aseguradoras Panel de enfoque',
  'LBL_SASA1_SASA_SEGURADORAS_RECORD_DASHBOARD' => 'Aseguradoras Cuadro de mando del registro',
);
<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favourite',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modified By Name',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Created By Name',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Aseguradoras List',
  'LBL_MODULE_NAME' => 'Aseguradoras',
  'LBL_MODULE_TITLE' => 'Aseguradoras',
  'LBL_MODULE_NAME_SINGULAR' => 'Aseguradora',
  'LBL_HOMEPAGE_TITLE' => 'My Aseguradoras',
  'LNK_NEW_RECORD' => 'Create Aseguradora',
  'LNK_LIST' => 'View Aseguradoras',
  'LNK_IMPORT_SASA1_SASA_SEGURADORAS' => 'Importar Aseguradoras',
  'LBL_SEARCH_FORM_TITLE' => 'Search Aseguradora',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_SASA1_SASA_SEGURADORAS_SUBPANEL_TITLE' => 'Aseguradoras',
  'LBL_NEW_FORM_TITLE' => 'New Aseguradora',
  'LNK_IMPORT_VCARD' => 'Importar Aseguradora vCard',
  'LBL_IMPORT' => 'Importar Aseguradoras',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Aseguradora record by importing a vCard from your file system.',
  'LBL_SASA1_SASA_SEGURADORAS_FOCUS_DRAWER_DASHBOARD' => 'Aseguradoras Panel de enfoque',
  'LBL_SASA1_SASA_SEGURADORAS_RECORD_DASHBOARD' => 'Aseguradoras Cuadro de mando del registro',
);
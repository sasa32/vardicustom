<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'קבוצות',
  'LBL_TEAMS' => 'קבוצות',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'מזהה משתמש מוקצה',
  'LBL_ASSIGNED_TO_NAME' => 'משתמש',
  'LBL_TAGS_LINK' => 'תגיות',
  'LBL_TAGS' => 'תגיות',
  'LBL_ID' => 'מזהה',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'נערך על ידי',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DOC_OWNER' => 'בעל המסמך',
  'LBL_USER_FAVORITES' => 'משתמשים שמעדיפים',
  'LBL_DESCRIPTION' => 'תיאור',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'שם',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'שם',
  'LBL_EDIT_BUTTON' => 'ערוך',
  'LBL_REMOVE' => 'הסר',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'שונה על ידי משתמש',
  'LBL_EXPORT_CREATED_BY_NAME' => 'נוצר על ידי שם',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Aseguradoras List',
  'LBL_MODULE_NAME' => 'Aseguradoras',
  'LBL_MODULE_TITLE' => 'Aseguradoras',
  'LBL_MODULE_NAME_SINGULAR' => 'Aseguradora',
  'LBL_HOMEPAGE_TITLE' => 'שלי Aseguradoras',
  'LNK_NEW_RECORD' => 'צור Aseguradora',
  'LNK_LIST' => 'View Aseguradoras',
  'LNK_IMPORT_SASA1_SASA_SEGURADORAS' => 'Importar Aseguradoras',
  'LBL_SEARCH_FORM_TITLE' => 'Search Aseguradora',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_SASA1_SASA_SEGURADORAS_SUBPANEL_TITLE' => 'Aseguradoras',
  'LBL_NEW_FORM_TITLE' => 'חדש Aseguradora',
  'LNK_IMPORT_VCARD' => 'Importar Aseguradora vCard',
  'LBL_IMPORT' => 'Importar Aseguradoras',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Aseguradora record by importing a vCard from your file system.',
  'LBL_SASA1_SASA_SEGURADORAS_FOCUS_DRAWER_DASHBOARD' => 'Aseguradoras Panel de enfoque',
  'LBL_SASA1_SASA_SEGURADORAS_RECORD_DASHBOARD' => 'Aseguradoras Cuadro de mando del registro',
);
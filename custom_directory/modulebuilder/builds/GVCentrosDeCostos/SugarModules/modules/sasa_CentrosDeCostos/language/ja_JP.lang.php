<?php
// created: 2023-04-25 06:24:37
$mod_strings = array (
  'LBL_TEAM' => 'チーム',
  'LBL_TEAMS' => 'チーム',
  'LBL_TEAM_ID' => 'チームID',
  'LBL_ASSIGNED_TO_ID' => 'アサイン先ID',
  'LBL_ASSIGNED_TO_NAME' => 'アサイン先',
  'LBL_TAGS_LINK' => 'タグ',
  'LBL_TAGS' => 'タグ',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新者ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '作成者ID',
  'LBL_DOC_OWNER' => 'ドキュメントの所有者',
  'LBL_USER_FAVORITES' => 'お気に入りのユーザー',
  'LBL_DESCRIPTION' => '詳細',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '作成者',
  'LBL_MODIFIED_USER' => '更新者',
  'LBL_LIST_NAME' => '名前',
  'LBL_EDIT_BUTTON' => '編集',
  'LBL_REMOVE' => '削除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '更新者',
  'LBL_EXPORT_CREATED_BY_NAME' => '名前で作成',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Centros De Costos 一覧',
  'LBL_MODULE_NAME' => 'Centros De Costos',
  'LBL_MODULE_TITLE' => 'Centros De Costos',
  'LBL_MODULE_NAME_SINGULAR' => 'Centros De Costos',
  'LBL_HOMEPAGE_TITLE' => '私の Centros De Costos',
  'LNK_NEW_RECORD' => '作成 Centros De Costos',
  'LNK_LIST' => '画面 Centros De Costos',
  'LNK_IMPORT_SASA_SASA_CENTROCOSTOS_C' => 'Importar Centros De Costos',
  'LBL_SEARCH_FORM_TITLE' => '検索 Centros De Costos',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'アクティビティストリーム',
  'LBL_SASA_SASA_CENTROCOSTOS_C_SUBPANEL_TITLE' => 'Centros De Costos',
  'LBL_NEW_FORM_TITLE' => '新規 Centros De Costos',
  'LNK_IMPORT_VCARD' => 'Importar Centros De Costos vCard',
  'LBL_IMPORT' => 'Importar Centros De Costos',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Centros De Costos record by importing a vCard from your file system.',
  'LBL_SASA_SASA_CENTROCOSTOS_C_FOCUS_DRAWER_DASHBOARD' => 'Centros De Costos Panel de enfoque',
  'LBL_SASA_SASA_CENTROCOSTOS_C_RECORD_DASHBOARD' => 'Centros De Costos Cuadro de mando del registro',
  'LNK_IMPORT_SASA_CENTROSDECOSTOS' => 'Importar Centros De Costos',
  'LBL_SASA_CENTROSDECOSTOS_SUBPANEL_TITLE' => 'Centros De Costos',
  'LBL_SASA_CENTROSDECOSTOS_FOCUS_DRAWER_DASHBOARD' => 'Centros De Costos Panel de enfoque',
  'LBL_SASA_CENTROSDECOSTOS_RECORD_DASHBOARD' => 'Centros De Costos Cuadro de mando del registro',
);
<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

// THIS CONTENT IS GENERATED BY MBPackage.php
$manifest = array (
  'built_in_version' => '12.2.0',
  'acceptable_sugar_versions' => 
  array (
    0 => '',
  ),
  'acceptable_sugar_flavors' => 
  array (
    0 => 'ENT',
    1 => 'ULT',
  ),
  'readme' => '',
  'key' => 'sasa',
  'author' => 'Sasa Consultoria',
  'description' => 'Modulo Para Centros de Costos',
  'icon' => '',
  'is_uninstallable' => true,
  'name' => 'GVCentrosDeCostos',
  'published_date' => '2023-01-05 20:25:02',
  'type' => 'module',
  'version' => 1672950302,
  'remove_tables' => 'prompt',
);


$installdefs = array (
  'id' => 'GVCentrosDeCostos',
  'beans' => 
  array (
    0 => 
    array (
      'module' => 'sasa_CentrosDeCostos',
      'class' => 'sasa_CentrosDeCostos',
      'path' => 'modules/sasa_CentrosDeCostos/sasa_CentrosDeCostos.php',
      'tab' => true,
    ),
  ),
  'layoutdefs' => 
  array (
  ),
  'relationships' => 
  array (
  ),
  'copy' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/modules/sasa_CentrosDeCostos',
      'to' => 'modules/sasa_CentrosDeCostos',
    ),
  ),
  'language' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/language/application/en_us.lang.php',
      'to_module' => 'application',
      'language' => 'en_us',
    ),
    1 => 
    array (
      'from' => '<basepath>/SugarModules/language/application/es_ES.lang.php',
      'to_module' => 'application',
      'language' => 'es_ES',
    ),
  ),
  'image_dir' => '<basepath>/icons',
);
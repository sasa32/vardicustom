<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1' => 
  array (
    'name' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1' => 
      array (
        'lhs_module' => 'sasa_Puntos_de_Ventas',
        'lhs_table' => 'sasa_puntos_de_ventas',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_Unidad_de_Negocio',
        'rhs_table' => 'sasa_unidad_de_negocio',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_c',
        'join_key_lhs' => 'sasa_puntoeab5_ventas_ida',
        'join_key_rhs' => 'sasa_puntoeacanegocio_idb',
      ),
    ),
    'table' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_puntoeab5_ventas_ida' => 
      array (
        'name' => 'sasa_puntoeab5_ventas_ida',
        'type' => 'id',
      ),
      'sasa_puntoeacanegocio_idb' => 
      array (
        'name' => 'sasa_puntoeacanegocio_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntoeab5_ventas_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntoeacanegocio_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_puntoeab5_ventas_ida',
          1 => 'sasa_puntoeacanegocio_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'lhs_table' => 'sasa_puntos_de_ventas',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Unidad_de_Negocio',
    'rhs_table' => 'sasa_unidad_de_negocio',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1_c',
    'join_key_lhs' => 'sasa_puntoeab5_ventas_ida',
    'join_key_rhs' => 'sasa_puntoeacanegocio_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_sasa_unidad_de_negocio_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_puntos_de_ventas_meetings_1' => 
  array (
    'name' => 'sasa_puntos_de_ventas_meetings_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_puntos_de_ventas_meetings_1' => 
      array (
        'lhs_module' => 'sasa_Puntos_de_Ventas',
        'lhs_table' => 'sasa_puntos_de_ventas',
        'lhs_key' => 'id',
        'rhs_module' => 'Meetings',
        'rhs_table' => 'meetings',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_puntos_de_ventas_meetings_1_c',
        'join_key_lhs' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
        'join_key_rhs' => 'sasa_puntos_de_ventas_meetings_1meetings_idb',
      ),
    ),
    'table' => 'sasa_puntos_de_ventas_meetings_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida' => 
      array (
        'name' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
        'type' => 'id',
      ),
      'sasa_puntos_de_ventas_meetings_1meetings_idb' => 
      array (
        'name' => 'sasa_puntos_de_ventas_meetings_1meetings_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_meetings_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_meetings_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_meetings_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_meetings_1meetings_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_puntos_de_ventas_meetings_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_meetings_1meetings_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'lhs_table' => 'sasa_puntos_de_ventas',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_puntos_de_ventas_meetings_1_c',
    'join_key_lhs' => 'sasa_puntos_de_ventas_meetings_1sasa_puntos_de_ventas_ida',
    'join_key_rhs' => 'sasa_puntos_de_ventas_meetings_1meetings_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_meetings_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_puntos_de_ventas_tasks_1' => 
  array (
    'name' => 'sasa_puntos_de_ventas_tasks_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_puntos_de_ventas_tasks_1' => 
      array (
        'lhs_module' => 'sasa_Puntos_de_Ventas',
        'lhs_table' => 'sasa_puntos_de_ventas',
        'lhs_key' => 'id',
        'rhs_module' => 'Tasks',
        'rhs_table' => 'tasks',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_puntos_de_ventas_tasks_1_c',
        'join_key_lhs' => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
        'join_key_rhs' => 'sasa_puntos_de_ventas_tasks_1tasks_idb',
      ),
    ),
    'table' => 'sasa_puntos_de_ventas_tasks_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida' => 
      array (
        'name' => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
        'type' => 'id',
      ),
      'sasa_puntos_de_ventas_tasks_1tasks_idb' => 
      array (
        'name' => 'sasa_puntos_de_ventas_tasks_1tasks_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_tasks_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_tasks_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_tasks_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_tasks_1tasks_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_puntos_de_ventas_tasks_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_tasks_1tasks_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'lhs_table' => 'sasa_puntos_de_ventas',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_puntos_de_ventas_tasks_1_c',
    'join_key_lhs' => 'sasa_puntos_de_ventas_tasks_1sasa_puntos_de_ventas_ida',
    'join_key_rhs' => 'sasa_puntos_de_ventas_tasks_1tasks_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_tasks_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_puntos_de_ventas_calls_1' => 
  array (
    'name' => 'sasa_puntos_de_ventas_calls_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_puntos_de_ventas_calls_1' => 
      array (
        'lhs_module' => 'sasa_Puntos_de_Ventas',
        'lhs_table' => 'sasa_puntos_de_ventas',
        'lhs_key' => 'id',
        'rhs_module' => 'Calls',
        'rhs_table' => 'calls',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_puntos_de_ventas_calls_1_c',
        'join_key_lhs' => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
        'join_key_rhs' => 'sasa_puntos_de_ventas_calls_1calls_idb',
      ),
    ),
    'table' => 'sasa_puntos_de_ventas_calls_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida' => 
      array (
        'name' => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
        'type' => 'id',
      ),
      'sasa_puntos_de_ventas_calls_1calls_idb' => 
      array (
        'name' => 'sasa_puntos_de_ventas_calls_1calls_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_calls_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_calls_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_calls_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_calls_1calls_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_puntos_de_ventas_calls_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_calls_1calls_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'lhs_table' => 'sasa_puntos_de_ventas',
    'lhs_key' => 'id',
    'rhs_module' => 'Calls',
    'rhs_table' => 'calls',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_puntos_de_ventas_calls_1_c',
    'join_key_lhs' => 'sasa_puntos_de_ventas_calls_1sasa_puntos_de_ventas_ida',
    'join_key_rhs' => 'sasa_puntos_de_ventas_calls_1calls_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_calls_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_puntos_de_ventas_leads_1' => 
  array (
    'name' => 'sasa_puntos_de_ventas_leads_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_puntos_de_ventas_leads_1' => 
      array (
        'lhs_module' => 'sasa_Puntos_de_Ventas',
        'lhs_table' => 'sasa_puntos_de_ventas',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_puntos_de_ventas_leads_1_c',
        'join_key_lhs' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
        'join_key_rhs' => 'sasa_puntos_de_ventas_leads_1leads_idb',
      ),
    ),
    'table' => 'sasa_puntos_de_ventas_leads_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida' => 
      array (
        'name' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
        'type' => 'id',
      ),
      'sasa_puntos_de_ventas_leads_1leads_idb' => 
      array (
        'name' => 'sasa_puntos_de_ventas_leads_1leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_leads_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_leads_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_leads_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_leads_1leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_puntos_de_ventas_leads_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_leads_1leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'lhs_table' => 'sasa_puntos_de_ventas',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_puntos_de_ventas_leads_1_c',
    'join_key_lhs' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
    'join_key_rhs' => 'sasa_puntos_de_ventas_leads_1leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_leads_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1' => 
  array (
    'name' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1' => 
      array (
        'lhs_module' => 'sasa_Puntos_de_Ventas',
        'lhs_table' => 'sasa_puntos_de_ventas',
        'lhs_key' => 'id',
        'rhs_module' => 'SASA_UnidadNegClienteProspect',
        'rhs_table' => 'sasa_unidadnegclienteprospect',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_c',
        'join_key_lhs' => 'sasa_punto200b_ventas_ida',
        'join_key_rhs' => 'sasa_puntoe384rospect_idb',
      ),
    ),
    'table' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_punto200b_ventas_ida' => 
      array (
        'name' => 'sasa_punto200b_ventas_ida',
        'type' => 'id',
      ),
      'sasa_puntoe384rospect_idb' => 
      array (
        'name' => 'sasa_puntoe384rospect_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_punto200b_ventas_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntoe384rospect_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_puntoe384rospect_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'lhs_table' => 'sasa_puntos_de_ventas',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_UnidadNegClienteProspect',
    'rhs_table' => 'sasa_unidadnegclienteprospect',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1_c',
    'join_key_lhs' => 'sasa_punto200b_ventas_ida',
    'join_key_rhs' => 'sasa_puntoe384rospect_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_sasa_unidadnegclienteprospect_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_puntos_de_ventas_cases_1' => 
  array (
    'name' => 'sasa_puntos_de_ventas_cases_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_puntos_de_ventas_cases_1' => 
      array (
        'lhs_module' => 'sasa_Puntos_de_Ventas',
        'lhs_table' => 'sasa_puntos_de_ventas',
        'lhs_key' => 'id',
        'rhs_module' => 'Cases',
        'rhs_table' => 'cases',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_puntos_de_ventas_cases_1_c',
        'join_key_lhs' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
        'join_key_rhs' => 'sasa_puntos_de_ventas_cases_1cases_idb',
      ),
    ),
    'table' => 'sasa_puntos_de_ventas_cases_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida' => 
      array (
        'name' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
        'type' => 'id',
      ),
      'sasa_puntos_de_ventas_cases_1cases_idb' => 
      array (
        'name' => 'sasa_puntos_de_ventas_cases_1cases_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_cases_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_cases_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_cases_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_cases_1cases_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_puntos_de_ventas_cases_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_cases_1cases_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'lhs_table' => 'sasa_puntos_de_ventas',
    'lhs_key' => 'id',
    'rhs_module' => 'Cases',
    'rhs_table' => 'cases',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_puntos_de_ventas_cases_1_c',
    'join_key_lhs' => 'sasa_puntos_de_ventas_cases_1sasa_puntos_de_ventas_ida',
    'join_key_rhs' => 'sasa_puntos_de_ventas_cases_1cases_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_cases_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_centrosdecostos_sasa_puntos_de_ventas_1' => 
  array (
    'name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_centrosdecostos_sasa_puntos_de_ventas_1' => 
      array (
        'lhs_module' => 'sasa_CentrosDeCostos',
        'lhs_table' => 'sasa_centrosdecostos',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_Puntos_de_Ventas',
        'rhs_table' => 'sasa_puntos_de_ventas',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1_c',
        'join_key_lhs' => 'sasa_centr37aaecostos_ida',
        'join_key_rhs' => 'sasa_centr2fec_ventas_idb',
      ),
    ),
    'table' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_centr37aaecostos_ida' => 
      array (
        'name' => 'sasa_centr37aaecostos_ida',
        'type' => 'id',
      ),
      'sasa_centr2fec_ventas_idb' => 
      array (
        'name' => 'sasa_centr2fec_ventas_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_centrosdecostos_sasa_puntos_de_ventas_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_centrosdecostos_sasa_puntos_de_ventas_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_centr37aaecostos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_centrosdecostos_sasa_puntos_de_ventas_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_centr2fec_ventas_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_centr2fec_ventas_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_CentrosDeCostos',
    'lhs_table' => 'sasa_centrosdecostos',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Puntos_de_Ventas',
    'rhs_table' => 'sasa_puntos_de_ventas',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1_c',
    'join_key_lhs' => 'sasa_centr37aaecostos_ida',
    'join_key_rhs' => 'sasa_centr2fec_ventas_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_centrosdecostos_sasa_puntos_de_ventas_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_puntos_de_ventas_modified_user' => 
  array (
    'name' => 'sasa_puntos_de_ventas_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Puntos_de_Ventas',
    'rhs_table' => 'sasa_puntos_de_ventas',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_puntos_de_ventas_created_by' => 
  array (
    'name' => 'sasa_puntos_de_ventas_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Puntos_de_Ventas',
    'rhs_table' => 'sasa_puntos_de_ventas',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_puntos_de_ventas_activities' => 
  array (
    'name' => 'sasa_puntos_de_ventas_activities',
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'lhs_table' => 'sasa_puntos_de_ventas',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_Puntos_de_Ventas',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_puntos_de_ventas_following' => 
  array (
    'name' => 'sasa_puntos_de_ventas_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Puntos_de_Ventas',
    'rhs_table' => 'sasa_puntos_de_ventas',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_Puntos_de_Ventas',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_puntos_de_ventas_favorite' => 
  array (
    'name' => 'sasa_puntos_de_ventas_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Puntos_de_Ventas',
    'rhs_table' => 'sasa_puntos_de_ventas',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'sasa_Puntos_de_Ventas',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_puntos_de_ventas_assigned_user' => 
  array (
    'name' => 'sasa_puntos_de_ventas_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Puntos_de_Ventas',
    'rhs_table' => 'sasa_puntos_de_ventas',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_puntos_de_ventas_opportunities_1' => 
  array (
    'rhs_label' => 'Cotizaciones',
    'lhs_label' => 'Puntos de Atención',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'rhs_module' => 'Opportunities',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_opportunities_1',
  ),
);
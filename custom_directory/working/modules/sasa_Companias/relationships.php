<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'sasa_companias_sasa_unidad_de_negocio_1' => 
  array (
    'name' => 'sasa_companias_sasa_unidad_de_negocio_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_companias_sasa_unidad_de_negocio_1' => 
      array (
        'lhs_module' => 'sasa_Companias',
        'lhs_table' => 'sasa_companias',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_Unidad_de_Negocio',
        'rhs_table' => 'sasa_unidad_de_negocio',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_companias_sasa_unidad_de_negocio_1_c',
        'join_key_lhs' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
        'join_key_rhs' => 'sasa_compac399negocio_idb',
      ),
    ),
    'table' => 'sasa_companias_sasa_unidad_de_negocio_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida' => 
      array (
        'name' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
        'type' => 'id',
      ),
      'sasa_compac399negocio_idb' => 
      array (
        'name' => 'sasa_compac399negocio_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_companias_sasa_unidad_de_negocio_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_companias_sasa_unidad_de_negocio_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_companias_sasa_unidad_de_negocio_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_compac399negocio_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_companias_sasa_unidad_de_negocio_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_compac399negocio_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Companias',
    'lhs_table' => 'sasa_companias',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Unidad_de_Negocio',
    'rhs_table' => 'sasa_unidad_de_negocio',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_companias_sasa_unidad_de_negocio_1_c',
    'join_key_lhs' => 'sasa_companias_sasa_unidad_de_negocio_1sasa_companias_ida',
    'join_key_rhs' => 'sasa_compac399negocio_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_companias_sasa_unidad_de_negocio_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'documents_sasa_companias_1' => 
  array (
    'name' => 'documents_sasa_companias_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'documents_sasa_companias_1' => 
      array (
        'lhs_module' => 'Documents',
        'lhs_table' => 'documents',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_Companias',
        'rhs_table' => 'sasa_companias',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'documents_sasa_companias_1_c',
        'join_key_lhs' => 'documents_sasa_companias_1documents_ida',
        'join_key_rhs' => 'documents_sasa_companias_1sasa_companias_idb',
      ),
    ),
    'table' => 'documents_sasa_companias_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'documents_sasa_companias_1documents_ida' => 
      array (
        'name' => 'documents_sasa_companias_1documents_ida',
        'type' => 'id',
      ),
      'documents_sasa_companias_1sasa_companias_idb' => 
      array (
        'name' => 'documents_sasa_companias_1sasa_companias_idb',
        'type' => 'id',
      ),
      'document_revision_id' => 
      array (
        'name' => 'document_revision_id',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_documents_sasa_companias_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_documents_sasa_companias_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'documents_sasa_companias_1documents_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_documents_sasa_companias_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'documents_sasa_companias_1sasa_companias_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'documents_sasa_companias_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'documents_sasa_companias_1documents_ida',
          1 => 'documents_sasa_companias_1sasa_companias_idb',
        ),
      ),
    ),
    'lhs_module' => 'Documents',
    'lhs_table' => 'documents',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Companias',
    'rhs_table' => 'sasa_companias',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'documents_sasa_companias_1_c',
    'join_key_lhs' => 'documents_sasa_companias_1documents_ida',
    'join_key_rhs' => 'documents_sasa_companias_1sasa_companias_idb',
    'readonly' => true,
    'relationship_name' => 'documents_sasa_companias_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_companias_modified_user' => 
  array (
    'name' => 'sasa_companias_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Companias',
    'rhs_table' => 'sasa_companias',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_companias_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_companias_created_by' => 
  array (
    'name' => 'sasa_companias_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Companias',
    'rhs_table' => 'sasa_companias',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_companias_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_companias_activities' => 
  array (
    'name' => 'sasa_companias_activities',
    'lhs_module' => 'sasa_Companias',
    'lhs_table' => 'sasa_companias',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_Companias',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'sasa_companias_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_companias_following' => 
  array (
    'name' => 'sasa_companias_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Companias',
    'rhs_table' => 'sasa_companias',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_Companias',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_companias_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_companias_favorite' => 
  array (
    'name' => 'sasa_companias_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Companias',
    'rhs_table' => 'sasa_companias',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'sasa_Companias',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_companias_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_companias_assigned_user' => 
  array (
    'name' => 'sasa_companias_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Companias',
    'rhs_table' => 'sasa_companias',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_companias_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_companias_sasa_habeas_data_1' => 
  array (
    'rhs_label' => 'Habeas Data',
    'lhs_label' => 'Compañias',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'sasa_Companias',
    'rhs_module' => 'SASA_Habeas_Data',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'sasa_companias_sasa_habeas_data_1',
  ),
);
<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'calls_leads' => 
  array (
    'name' => 'calls_leads',
    'table' => 'calls_leads',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'call_id' => 
      array (
        'name' => 'call_id',
        'type' => 'id',
      ),
      'lead_id' => 
      array (
        'name' => 'lead_id',
        'type' => 'id',
      ),
      'required' => 
      array (
        'name' => 'required',
        'type' => 'varchar',
        'len' => '1',
        'default' => '1',
      ),
      'accept_status' => 
      array (
        'name' => 'accept_status',
        'type' => 'varchar',
        'len' => '25',
        'default' => 'none',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'calls_leadspk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_lead_call_lead',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'lead_id',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_call_lead',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'call_id',
          1 => 'lead_id',
        ),
      ),
    ),
    'relationships' => 
    array (
      'calls_leads' => 
      array (
        'lhs_module' => 'Calls',
        'lhs_table' => 'calls',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'calls_leads',
        'join_key_lhs' => 'call_id',
        'join_key_rhs' => 'lead_id',
      ),
    ),
    'lhs_module' => 'Calls',
    'lhs_table' => 'calls',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'calls_leads',
    'join_key_lhs' => 'call_id',
    'join_key_rhs' => 'lead_id',
    'readonly' => true,
    'relationship_name' => 'calls_leads',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'default',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_leads' => 
  array (
    'name' => 'meetings_leads',
    'table' => 'meetings_leads',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'meeting_id' => 
      array (
        'name' => 'meeting_id',
        'type' => 'id',
      ),
      'lead_id' => 
      array (
        'name' => 'lead_id',
        'type' => 'id',
      ),
      'required' => 
      array (
        'name' => 'required',
        'type' => 'varchar',
        'len' => '1',
        'default' => '1',
      ),
      'accept_status' => 
      array (
        'name' => 'accept_status',
        'type' => 'varchar',
        'len' => '25',
        'default' => 'none',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'meetings_leadspk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_lead_meeting_lead',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'lead_id',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_meeting_lead',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'meeting_id',
          1 => 'lead_id',
        ),
      ),
    ),
    'relationships' => 
    array (
      'meetings_leads' => 
      array (
        'lhs_module' => 'Meetings',
        'lhs_table' => 'meetings',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'meetings_leads',
        'join_key_lhs' => 'meeting_id',
        'join_key_rhs' => 'lead_id',
      ),
    ),
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'meetings_leads',
    'join_key_lhs' => 'meeting_id',
    'join_key_rhs' => 'lead_id',
    'readonly' => true,
    'relationship_name' => 'meetings_leads',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'default',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'leads_dataprivacy' => 
  array (
    'name' => 'leads_dataprivacy',
    'table' => 'leads_dataprivacy',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'lead_id' => 
      array (
        'name' => 'lead_id',
        'type' => 'id',
      ),
      'dataprivacy_id' => 
      array (
        'name' => 'dataprivacy_id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'leads_dataprivacypk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_lead_dataprivacy_dataprivacy',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'dataprivacy_id',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_leads_dataprivacy',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'lead_id',
          1 => 'dataprivacy_id',
        ),
      ),
    ),
    'relationships' => 
    array (
      'leads_dataprivacy' => 
      array (
        'lhs_module' => 'Leads',
        'lhs_table' => 'leads',
        'lhs_key' => 'id',
        'rhs_module' => 'DataPrivacy',
        'rhs_table' => 'data_privacy',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'leads_dataprivacy',
        'join_key_lhs' => 'lead_id',
        'join_key_rhs' => 'dataprivacy_id',
      ),
    ),
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'DataPrivacy',
    'rhs_table' => 'data_privacy',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'leads_dataprivacy',
    'join_key_lhs' => 'lead_id',
    'join_key_rhs' => 'dataprivacy_id',
    'readonly' => true,
    'relationship_name' => 'leads_dataprivacy',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'messages_leads' => 
  array (
    'name' => 'messages_leads',
    'table' => 'messages_leads',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'message_id' => 
      array (
        'name' => 'message_id',
        'type' => 'id',
      ),
      'lead_id' => 
      array (
        'name' => 'lead_id',
        'type' => 'id',
      ),
      'required' => 
      array (
        'name' => 'required',
        'type' => 'varchar',
        'len' => '1',
        'default' => '1',
      ),
      'accept_status' => 
      array (
        'name' => 'accept_status',
        'type' => 'varchar',
        'len' => '25',
        'default' => 'none',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'messages_leadspk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_lead_message_lead',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'lead_id',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_message_lead',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'message_id',
          1 => 'lead_id',
        ),
      ),
    ),
    'relationships' => 
    array (
      'messages_leads' => 
      array (
        'lhs_module' => 'Messages',
        'lhs_table' => 'messages',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'messages_leads',
        'join_key_lhs' => 'message_id',
        'join_key_rhs' => 'lead_id',
      ),
    ),
    'lhs_module' => 'Messages',
    'lhs_table' => 'messages',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'messages_leads',
    'join_key_lhs' => 'message_id',
    'join_key_rhs' => 'lead_id',
    'readonly' => true,
    'relationship_name' => 'messages_leads',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'default',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_paises_leads_1' => 
  array (
    'name' => 'sasa_paises_leads_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_paises_leads_1' => 
      array (
        'lhs_module' => 'sasa_Paises',
        'lhs_table' => 'sasa_paises',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_paises_leads_1_c',
        'join_key_lhs' => 'sasa_paises_leads_1sasa_paises_ida',
        'join_key_rhs' => 'sasa_paises_leads_1leads_idb',
      ),
    ),
    'table' => 'sasa_paises_leads_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_paises_leads_1sasa_paises_ida' => 
      array (
        'name' => 'sasa_paises_leads_1sasa_paises_ida',
        'type' => 'id',
      ),
      'sasa_paises_leads_1leads_idb' => 
      array (
        'name' => 'sasa_paises_leads_1leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_paises_leads_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_paises_leads_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_leads_1sasa_paises_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_paises_leads_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_leads_1leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_paises_leads_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_paises_leads_1leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Paises',
    'lhs_table' => 'sasa_paises',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_paises_leads_1_c',
    'join_key_lhs' => 'sasa_paises_leads_1sasa_paises_ida',
    'join_key_rhs' => 'sasa_paises_leads_1leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_leads_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_departamentos_leads_1' => 
  array (
    'name' => 'sasa_departamentos_leads_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_departamentos_leads_1' => 
      array (
        'lhs_module' => 'sasa_Departamentos',
        'lhs_table' => 'sasa_departamentos',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_departamentos_leads_1_c',
        'join_key_lhs' => 'sasa_departamentos_leads_1sasa_departamentos_ida',
        'join_key_rhs' => 'sasa_departamentos_leads_1leads_idb',
      ),
    ),
    'table' => 'sasa_departamentos_leads_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_departamentos_leads_1sasa_departamentos_ida' => 
      array (
        'name' => 'sasa_departamentos_leads_1sasa_departamentos_ida',
        'type' => 'id',
      ),
      'sasa_departamentos_leads_1leads_idb' => 
      array (
        'name' => 'sasa_departamentos_leads_1leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_departamentos_leads_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_departamentos_leads_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_departamentos_leads_1sasa_departamentos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_departamentos_leads_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_departamentos_leads_1leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_departamentos_leads_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_departamentos_leads_1leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Departamentos',
    'lhs_table' => 'sasa_departamentos',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_departamentos_leads_1_c',
    'join_key_lhs' => 'sasa_departamentos_leads_1sasa_departamentos_ida',
    'join_key_rhs' => 'sasa_departamentos_leads_1leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_departamentos_leads_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_municipios_leads_1' => 
  array (
    'name' => 'sasa_municipios_leads_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_municipios_leads_1' => 
      array (
        'lhs_module' => 'sasa_Municipios',
        'lhs_table' => 'sasa_municipios',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_municipios_leads_1_c',
        'join_key_lhs' => 'sasa_municipios_leads_1sasa_municipios_ida',
        'join_key_rhs' => 'sasa_municipios_leads_1leads_idb',
      ),
    ),
    'table' => 'sasa_municipios_leads_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_municipios_leads_1sasa_municipios_ida' => 
      array (
        'name' => 'sasa_municipios_leads_1sasa_municipios_ida',
        'type' => 'id',
      ),
      'sasa_municipios_leads_1leads_idb' => 
      array (
        'name' => 'sasa_municipios_leads_1leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_municipios_leads_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_municipios_leads_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_municipios_leads_1sasa_municipios_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_municipios_leads_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_municipios_leads_1leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_municipios_leads_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_municipios_leads_1leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Municipios',
    'lhs_table' => 'sasa_municipios',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_municipios_leads_1_c',
    'join_key_lhs' => 'sasa_municipios_leads_1sasa_municipios_ida',
    'join_key_rhs' => 'sasa_municipios_leads_1leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_municipios_leads_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_paises_leads_2' => 
  array (
    'name' => 'sasa_paises_leads_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_paises_leads_2' => 
      array (
        'lhs_module' => 'sasa_Paises',
        'lhs_table' => 'sasa_paises',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_paises_leads_2_c',
        'join_key_lhs' => 'sasa_paises_leads_2sasa_paises_ida',
        'join_key_rhs' => 'sasa_paises_leads_2leads_idb',
      ),
    ),
    'table' => 'sasa_paises_leads_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_paises_leads_2sasa_paises_ida' => 
      array (
        'name' => 'sasa_paises_leads_2sasa_paises_ida',
        'type' => 'id',
      ),
      'sasa_paises_leads_2leads_idb' => 
      array (
        'name' => 'sasa_paises_leads_2leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_paises_leads_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_paises_leads_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_leads_2sasa_paises_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_paises_leads_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_leads_2leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_paises_leads_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_paises_leads_2leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Paises',
    'lhs_table' => 'sasa_paises',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_paises_leads_2_c',
    'join_key_lhs' => 'sasa_paises_leads_2sasa_paises_ida',
    'join_key_rhs' => 'sasa_paises_leads_2leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_leads_2',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_departamentos_leads_2' => 
  array (
    'name' => 'sasa_departamentos_leads_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_departamentos_leads_2' => 
      array (
        'lhs_module' => 'sasa_Departamentos',
        'lhs_table' => 'sasa_departamentos',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_departamentos_leads_2_c',
        'join_key_lhs' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
        'join_key_rhs' => 'sasa_departamentos_leads_2leads_idb',
      ),
    ),
    'table' => 'sasa_departamentos_leads_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_departamentos_leads_2sasa_departamentos_ida' => 
      array (
        'name' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
        'type' => 'id',
      ),
      'sasa_departamentos_leads_2leads_idb' => 
      array (
        'name' => 'sasa_departamentos_leads_2leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_departamentos_leads_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_departamentos_leads_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_departamentos_leads_2sasa_departamentos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_departamentos_leads_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_departamentos_leads_2leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_departamentos_leads_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_departamentos_leads_2leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Departamentos',
    'lhs_table' => 'sasa_departamentos',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_departamentos_leads_2_c',
    'join_key_lhs' => 'sasa_departamentos_leads_2sasa_departamentos_ida',
    'join_key_rhs' => 'sasa_departamentos_leads_2leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_departamentos_leads_2',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_vehiculos_leads_1' => 
  array (
    'name' => 'sasa_vehiculos_leads_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_vehiculos_leads_1' => 
      array (
        'lhs_module' => 'sasa_vehiculos',
        'lhs_table' => 'sasa_vehiculos',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_vehiculos_leads_1_c',
        'join_key_lhs' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
        'join_key_rhs' => 'sasa_vehiculos_leads_1leads_idb',
      ),
    ),
    'table' => 'sasa_vehiculos_leads_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_vehiculos_leads_1sasa_vehiculos_ida' => 
      array (
        'name' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
        'type' => 'id',
      ),
      'sasa_vehiculos_leads_1leads_idb' => 
      array (
        'name' => 'sasa_vehiculos_leads_1leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_vehiculos_leads_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_vehiculos_leads_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_vehiculos_leads_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_leads_1leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_vehiculos_leads_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_leads_1leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_vehiculos',
    'lhs_table' => 'sasa_vehiculos',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_vehiculos_leads_1_c',
    'join_key_lhs' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
    'join_key_rhs' => 'sasa_vehiculos_leads_1leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_leads_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'leads_sasa_habeas_data_1' => 
  array (
    'name' => 'leads_sasa_habeas_data_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'leads_sasa_habeas_data_1' => 
      array (
        'lhs_module' => 'Leads',
        'lhs_table' => 'leads',
        'lhs_key' => 'id',
        'rhs_module' => 'SASA_Habeas_Data',
        'rhs_table' => 'sasa_habeas_data',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'leads_sasa_habeas_data_1_c',
        'join_key_lhs' => 'leads_sasa_habeas_data_1leads_ida',
        'join_key_rhs' => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
      ),
    ),
    'table' => 'leads_sasa_habeas_data_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'leads_sasa_habeas_data_1leads_ida' => 
      array (
        'name' => 'leads_sasa_habeas_data_1leads_ida',
        'type' => 'id',
      ),
      'leads_sasa_habeas_data_1sasa_habeas_data_idb' => 
      array (
        'name' => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_leads_sasa_habeas_data_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_leads_sasa_habeas_data_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'leads_sasa_habeas_data_1leads_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_leads_sasa_habeas_data_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'leads_sasa_habeas_data_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
        ),
      ),
    ),
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'leads_sasa_habeas_data_1_c',
    'join_key_lhs' => 'leads_sasa_habeas_data_1leads_ida',
    'join_key_rhs' => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
    'readonly' => true,
    'relationship_name' => 'leads_sasa_habeas_data_1',
    'rhs_subpanel' => 'ForLeadsLeads_sasa_habeas_data_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_puntos_de_ventas_leads_1' => 
  array (
    'name' => 'sasa_puntos_de_ventas_leads_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_puntos_de_ventas_leads_1' => 
      array (
        'lhs_module' => 'sasa_Puntos_de_Ventas',
        'lhs_table' => 'sasa_puntos_de_ventas',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_puntos_de_ventas_leads_1_c',
        'join_key_lhs' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
        'join_key_rhs' => 'sasa_puntos_de_ventas_leads_1leads_idb',
      ),
    ),
    'table' => 'sasa_puntos_de_ventas_leads_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida' => 
      array (
        'name' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
        'type' => 'id',
      ),
      'sasa_puntos_de_ventas_leads_1leads_idb' => 
      array (
        'name' => 'sasa_puntos_de_ventas_leads_1leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_leads_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_leads_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_puntos_de_ventas_leads_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_leads_1leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_puntos_de_ventas_leads_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_puntos_de_ventas_leads_1leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Puntos_de_Ventas',
    'lhs_table' => 'sasa_puntos_de_ventas',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_puntos_de_ventas_leads_1_c',
    'join_key_lhs' => 'sasa_puntos_de_ventas_leads_1sasa_puntos_de_ventas_ida',
    'join_key_rhs' => 'sasa_puntos_de_ventas_leads_1leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_puntos_de_ventas_leads_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'leads_modified_user' => 
  array (
    'name' => 'leads_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'leads_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'leads_created_by' => 
  array (
    'name' => 'leads_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'leads_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_activities' => 
  array (
    'name' => 'lead_activities',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'lead_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_prospect' => 
  array (
    'name' => 'lead_prospect',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Prospects',
    'rhs_table' => 'prospects',
    'rhs_key' => 'lead_id',
    'relationship_type' => 'one-to-one',
    'readonly' => true,
    'relationship_name' => 'lead_prospect',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_direct_reports' => 
  array (
    'name' => 'lead_direct_reports',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'reports_to_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'lead_direct_reports',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_tasks' => 
  array (
    'name' => 'lead_tasks',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
    'readonly' => true,
    'relationship_name' => 'lead_tasks',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_notes' => 
  array (
    'name' => 'lead_notes',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Notes',
    'rhs_table' => 'notes',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
    'readonly' => true,
    'relationship_name' => 'lead_notes',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_messages' => 
  array (
    'name' => 'lead_messages',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Messages',
    'rhs_table' => 'messages',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
    'readonly' => true,
    'relationship_name' => 'lead_messages',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_meetings' => 
  array (
    'name' => 'lead_meetings',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
    'readonly' => true,
    'relationship_name' => 'lead_meetings',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_calls' => 
  array (
    'name' => 'lead_calls',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Calls',
    'rhs_table' => 'calls',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
    'readonly' => true,
    'relationship_name' => 'lead_calls',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'leads_following' => 
  array (
    'name' => 'leads_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'leads_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'leads_favorite' => 
  array (
    'name' => 'leads_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'Leads',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'leads_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'leads_assigned_user' => 
  array (
    'name' => 'leads_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'leads_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_leads' => 
  array (
    'name' => 'contact_leads',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'contact_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contact_leads',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'account_leads' => 
  array (
    'name' => 'account_leads',
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'account_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'account_leads',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'opportunity_leads' => 
  array (
    'name' => 'opportunity_leads',
    'lhs_module' => 'Opportunities',
    'lhs_table' => 'opportunities',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'opportunity_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'opportunity_leads',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'campaign_leads' => 
  array (
    'name' => 'campaign_leads',
    'lhs_module' => 'Campaigns',
    'lhs_table' => 'campaigns',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'campaign_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'campaign_leads',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'leads_cases_1' => 
  array (
    'rhs_label' => 'Casos',
    'lhs_label' => 'Leads',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'Leads',
    'rhs_module' => 'Cases',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'leads_cases_1',
  ),
);
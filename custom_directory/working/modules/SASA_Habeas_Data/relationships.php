<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'documents_sasa_habeas_data_1' => 
  array (
    'name' => 'documents_sasa_habeas_data_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'documents_sasa_habeas_data_1' => 
      array (
        'lhs_module' => 'Documents',
        'lhs_table' => 'documents',
        'lhs_key' => 'id',
        'rhs_module' => 'SASA_Habeas_Data',
        'rhs_table' => 'sasa_habeas_data',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'documents_sasa_habeas_data_1_c',
        'join_key_lhs' => 'documents_sasa_habeas_data_1documents_ida',
        'join_key_rhs' => 'documents_sasa_habeas_data_1sasa_habeas_data_idb',
      ),
    ),
    'table' => 'documents_sasa_habeas_data_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'documents_sasa_habeas_data_1documents_ida' => 
      array (
        'name' => 'documents_sasa_habeas_data_1documents_ida',
        'type' => 'id',
      ),
      'documents_sasa_habeas_data_1sasa_habeas_data_idb' => 
      array (
        'name' => 'documents_sasa_habeas_data_1sasa_habeas_data_idb',
        'type' => 'id',
      ),
      'document_revision_id' => 
      array (
        'name' => 'document_revision_id',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_documents_sasa_habeas_data_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_documents_sasa_habeas_data_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'documents_sasa_habeas_data_1documents_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_documents_sasa_habeas_data_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'documents_sasa_habeas_data_1sasa_habeas_data_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'documents_sasa_habeas_data_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'documents_sasa_habeas_data_1sasa_habeas_data_idb',
        ),
      ),
    ),
    'lhs_module' => 'Documents',
    'lhs_table' => 'documents',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'documents_sasa_habeas_data_1_c',
    'join_key_lhs' => 'documents_sasa_habeas_data_1documents_ida',
    'join_key_rhs' => 'documents_sasa_habeas_data_1sasa_habeas_data_idb',
    'readonly' => true,
    'relationship_name' => 'documents_sasa_habeas_data_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_companias_sasa_habeas_data_1' => 
  array (
    'name' => 'sasa_companias_sasa_habeas_data_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_companias_sasa_habeas_data_1' => 
      array (
        'lhs_module' => 'sasa_Companias',
        'lhs_table' => 'sasa_companias',
        'lhs_key' => 'id',
        'rhs_module' => 'SASA_Habeas_Data',
        'rhs_table' => 'sasa_habeas_data',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_companias_sasa_habeas_data_1_c',
        'join_key_lhs' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
        'join_key_rhs' => 'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb',
      ),
    ),
    'table' => 'sasa_companias_sasa_habeas_data_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_companias_sasa_habeas_data_1sasa_companias_ida' => 
      array (
        'name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
        'type' => 'id',
      ),
      'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb' => 
      array (
        'name' => 'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_companias_sasa_habeas_data_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_companias_sasa_habeas_data_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_companias_sasa_habeas_data_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_companias_sasa_habeas_data_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Companias',
    'lhs_table' => 'sasa_companias',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_companias_sasa_habeas_data_1_c',
    'join_key_lhs' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
    'join_key_rhs' => 'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_companias_sasa_habeas_data_1',
    'rhs_subpanel' => 'ForSasa_companiasSasa_companias_sasa_habeas_data_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'leads_sasa_habeas_data_1' => 
  array (
    'name' => 'leads_sasa_habeas_data_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'leads_sasa_habeas_data_1' => 
      array (
        'lhs_module' => 'Leads',
        'lhs_table' => 'leads',
        'lhs_key' => 'id',
        'rhs_module' => 'SASA_Habeas_Data',
        'rhs_table' => 'sasa_habeas_data',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'leads_sasa_habeas_data_1_c',
        'join_key_lhs' => 'leads_sasa_habeas_data_1leads_ida',
        'join_key_rhs' => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
      ),
    ),
    'table' => 'leads_sasa_habeas_data_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'leads_sasa_habeas_data_1leads_ida' => 
      array (
        'name' => 'leads_sasa_habeas_data_1leads_ida',
        'type' => 'id',
      ),
      'leads_sasa_habeas_data_1sasa_habeas_data_idb' => 
      array (
        'name' => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_leads_sasa_habeas_data_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_leads_sasa_habeas_data_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'leads_sasa_habeas_data_1leads_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_leads_sasa_habeas_data_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'leads_sasa_habeas_data_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
        ),
      ),
    ),
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'leads_sasa_habeas_data_1_c',
    'join_key_lhs' => 'leads_sasa_habeas_data_1leads_ida',
    'join_key_rhs' => 'leads_sasa_habeas_data_1sasa_habeas_data_idb',
    'readonly' => true,
    'relationship_name' => 'leads_sasa_habeas_data_1',
    'rhs_subpanel' => 'ForLeadsLeads_sasa_habeas_data_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'contacts_sasa_habeas_data_1' => 
  array (
    'name' => 'contacts_sasa_habeas_data_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'contacts_sasa_habeas_data_1' => 
      array (
        'lhs_module' => 'Contacts',
        'lhs_table' => 'contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'SASA_Habeas_Data',
        'rhs_table' => 'sasa_habeas_data',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'contacts_sasa_habeas_data_1_c',
        'join_key_lhs' => 'contacts_sasa_habeas_data_1contacts_ida',
        'join_key_rhs' => 'contacts_sasa_habeas_data_1sasa_habeas_data_idb',
      ),
    ),
    'table' => 'contacts_sasa_habeas_data_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'contacts_sasa_habeas_data_1contacts_ida' => 
      array (
        'name' => 'contacts_sasa_habeas_data_1contacts_ida',
        'type' => 'id',
      ),
      'contacts_sasa_habeas_data_1sasa_habeas_data_idb' => 
      array (
        'name' => 'contacts_sasa_habeas_data_1sasa_habeas_data_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_contacts_sasa_habeas_data_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_contacts_sasa_habeas_data_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'contacts_sasa_habeas_data_1contacts_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_contacts_sasa_habeas_data_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'contacts_sasa_habeas_data_1sasa_habeas_data_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'contacts_sasa_habeas_data_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'contacts_sasa_habeas_data_1sasa_habeas_data_idb',
        ),
      ),
    ),
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'contacts_sasa_habeas_data_1_c',
    'join_key_lhs' => 'contacts_sasa_habeas_data_1contacts_ida',
    'join_key_rhs' => 'contacts_sasa_habeas_data_1sasa_habeas_data_idb',
    'readonly' => true,
    'relationship_name' => 'contacts_sasa_habeas_data_1',
    'rhs_subpanel' => 'ForContactsContacts_sasa_habeas_data_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_habeas_data_modified_user' => 
  array (
    'name' => 'sasa_habeas_data_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_habeas_data_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_habeas_data_created_by' => 
  array (
    'name' => 'sasa_habeas_data_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_habeas_data_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_habeas_data_activities' => 
  array (
    'name' => 'sasa_habeas_data_activities',
    'lhs_module' => 'SASA_Habeas_Data',
    'lhs_table' => 'sasa_habeas_data',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'SASA_Habeas_Data',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'sasa_habeas_data_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_habeas_data_following' => 
  array (
    'name' => 'sasa_habeas_data_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'SASA_Habeas_Data',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_habeas_data_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_habeas_data_favorite' => 
  array (
    'name' => 'sasa_habeas_data_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'SASA_Habeas_Data',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_habeas_data_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_habeas_data_assigned_user' => 
  array (
    'name' => 'sasa_habeas_data_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_Habeas_Data',
    'rhs_table' => 'sasa_habeas_data',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_habeas_data_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_habeas_data_tasks_1' => 
  array (
    'rhs_label' => 'Tareas',
    'lhs_label' => 'Habeas Data',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'SASA_Habeas_Data',
    'rhs_module' => 'Tasks',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'sasa_habeas_data_tasks_1',
  ),
);
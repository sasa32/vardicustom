<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'sasa_vehiculos_leads_1' => 
  array (
    'name' => 'sasa_vehiculos_leads_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_vehiculos_leads_1' => 
      array (
        'lhs_module' => 'sasa_vehiculos',
        'lhs_table' => 'sasa_vehiculos',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_vehiculos_leads_1_c',
        'join_key_lhs' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
        'join_key_rhs' => 'sasa_vehiculos_leads_1leads_idb',
      ),
    ),
    'table' => 'sasa_vehiculos_leads_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_vehiculos_leads_1sasa_vehiculos_ida' => 
      array (
        'name' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
        'type' => 'id',
      ),
      'sasa_vehiculos_leads_1leads_idb' => 
      array (
        'name' => 'sasa_vehiculos_leads_1leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_vehiculos_leads_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_vehiculos_leads_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_vehiculos_leads_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_leads_1leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_vehiculos_leads_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_leads_1leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_vehiculos',
    'lhs_table' => 'sasa_vehiculos',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_vehiculos_leads_1_c',
    'join_key_lhs' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
    'join_key_rhs' => 'sasa_vehiculos_leads_1leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_leads_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_marcas_sasa_vehiculos_1' => 
  array (
    'name' => 'sasa_marcas_sasa_vehiculos_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_marcas_sasa_vehiculos_1' => 
      array (
        'lhs_module' => 'sasa_Marcas',
        'lhs_table' => 'sasa_marcas',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_vehiculos',
        'rhs_table' => 'sasa_vehiculos',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_marcas_sasa_vehiculos_1_c',
        'join_key_lhs' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
        'join_key_rhs' => 'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb',
      ),
    ),
    'table' => 'sasa_marcas_sasa_vehiculos_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida' => 
      array (
        'name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
        'type' => 'id',
      ),
      'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb' => 
      array (
        'name' => 'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_marcas_sasa_vehiculos_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_marcas_sasa_vehiculos_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_marcas_sasa_vehiculos_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_marcas_sasa_vehiculos_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Marcas',
    'lhs_table' => 'sasa_marcas',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_vehiculos',
    'rhs_table' => 'sasa_vehiculos',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_marcas_sasa_vehiculos_1_c',
    'join_key_lhs' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
    'join_key_rhs' => 'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_marcas_sasa_vehiculos_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_vehiculos_sasa_unidadnegclienteprospect_1' => 
  array (
    'name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_vehiculos_sasa_unidadnegclienteprospect_1' => 
      array (
        'lhs_module' => 'sasa_vehiculos',
        'lhs_table' => 'sasa_vehiculos',
        'lhs_key' => 'id',
        'rhs_module' => 'SASA_UnidadNegClienteProspect',
        'rhs_table' => 'sasa_unidadnegclienteprospect',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1_c',
        'join_key_lhs' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
        'join_key_rhs' => 'sasa_vehic0094rospect_idb',
      ),
    ),
    'table' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida' => 
      array (
        'name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
        'type' => 'id',
      ),
      'sasa_vehic0094rospect_idb' => 
      array (
        'name' => 'sasa_vehic0094rospect_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_vehiculos_sasa_unidadnegclienteprospect_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_vehiculos_sasa_unidadnegclienteprospect_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_vehiculos_sasa_unidadnegclienteprospect_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehic0094rospect_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_vehic0094rospect_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_vehiculos',
    'lhs_table' => 'sasa_vehiculos',
    'lhs_key' => 'id',
    'rhs_module' => 'SASA_UnidadNegClienteProspect',
    'rhs_table' => 'sasa_unidadnegclienteprospect',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1_c',
    'join_key_lhs' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1sasa_vehiculos_ida',
    'join_key_rhs' => 'sasa_vehic0094rospect_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_sasa_unidadnegclienteprospect_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_vehiculos_cases_1' => 
  array (
    'name' => 'sasa_vehiculos_cases_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_vehiculos_cases_1' => 
      array (
        'lhs_module' => 'sasa_vehiculos',
        'lhs_table' => 'sasa_vehiculos',
        'lhs_key' => 'id',
        'rhs_module' => 'Cases',
        'rhs_table' => 'cases',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_vehiculos_cases_1_c',
        'join_key_lhs' => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
        'join_key_rhs' => 'sasa_vehiculos_cases_1cases_idb',
      ),
    ),
    'table' => 'sasa_vehiculos_cases_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_vehiculos_cases_1sasa_vehiculos_ida' => 
      array (
        'name' => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
        'type' => 'id',
      ),
      'sasa_vehiculos_cases_1cases_idb' => 
      array (
        'name' => 'sasa_vehiculos_cases_1cases_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_1cases_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_vehiculos_cases_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_1cases_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_vehiculos',
    'lhs_table' => 'sasa_vehiculos',
    'lhs_key' => 'id',
    'rhs_module' => 'Cases',
    'rhs_table' => 'cases',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_vehiculos_cases_1_c',
    'join_key_lhs' => 'sasa_vehiculos_cases_1sasa_vehiculos_ida',
    'join_key_rhs' => 'sasa_vehiculos_cases_1cases_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_cases_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_vehiculos_cases_2' => 
  array (
    'name' => 'sasa_vehiculos_cases_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_vehiculos_cases_2' => 
      array (
        'lhs_module' => 'sasa_vehiculos',
        'lhs_table' => 'sasa_vehiculos',
        'lhs_key' => 'id',
        'rhs_module' => 'Cases',
        'rhs_table' => 'cases',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_vehiculos_cases_2_c',
        'join_key_lhs' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
        'join_key_rhs' => 'sasa_vehiculos_cases_2cases_idb',
      ),
    ),
    'table' => 'sasa_vehiculos_cases_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_vehiculos_cases_2sasa_vehiculos_ida' => 
      array (
        'name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
        'type' => 'id',
      ),
      'sasa_vehiculos_cases_2cases_idb' => 
      array (
        'name' => 'sasa_vehiculos_cases_2cases_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_2cases_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_vehiculos_cases_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_2cases_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_vehiculos',
    'lhs_table' => 'sasa_vehiculos',
    'lhs_key' => 'id',
    'rhs_module' => 'Cases',
    'rhs_table' => 'cases',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_vehiculos_cases_2_c',
    'join_key_lhs' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
    'join_key_rhs' => 'sasa_vehiculos_cases_2cases_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_cases_2',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_vehiculos_cases_3' => 
  array (
    'name' => 'sasa_vehiculos_cases_3',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_vehiculos_cases_3' => 
      array (
        'lhs_module' => 'sasa_vehiculos',
        'lhs_table' => 'sasa_vehiculos',
        'lhs_key' => 'id',
        'rhs_module' => 'Cases',
        'rhs_table' => 'cases',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_vehiculos_cases_3_c',
        'join_key_lhs' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
        'join_key_rhs' => 'sasa_vehiculos_cases_3cases_idb',
      ),
    ),
    'table' => 'sasa_vehiculos_cases_3_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_vehiculos_cases_3sasa_vehiculos_ida' => 
      array (
        'name' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
        'type' => 'id',
      ),
      'sasa_vehiculos_cases_3cases_idb' => 
      array (
        'name' => 'sasa_vehiculos_cases_3cases_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_3_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_3_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_3_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_3cases_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_vehiculos_cases_3_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_3cases_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_vehiculos',
    'lhs_table' => 'sasa_vehiculos',
    'lhs_key' => 'id',
    'rhs_module' => 'Cases',
    'rhs_table' => 'cases',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_vehiculos_cases_3_c',
    'join_key_lhs' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
    'join_key_rhs' => 'sasa_vehiculos_cases_3cases_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_cases_3',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_vehiculos_cases_4' => 
  array (
    'name' => 'sasa_vehiculos_cases_4',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_vehiculos_cases_4' => 
      array (
        'lhs_module' => 'sasa_vehiculos',
        'lhs_table' => 'sasa_vehiculos',
        'lhs_key' => 'id',
        'rhs_module' => 'Cases',
        'rhs_table' => 'cases',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_vehiculos_cases_4_c',
        'join_key_lhs' => 'sasa_vehiculos_cases_4sasa_vehiculos_ida',
        'join_key_rhs' => 'sasa_vehiculos_cases_4cases_idb',
      ),
    ),
    'table' => 'sasa_vehiculos_cases_4_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_vehiculos_cases_4sasa_vehiculos_ida' => 
      array (
        'name' => 'sasa_vehiculos_cases_4sasa_vehiculos_ida',
        'type' => 'id',
      ),
      'sasa_vehiculos_cases_4cases_idb' => 
      array (
        'name' => 'sasa_vehiculos_cases_4cases_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_4_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_4_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_4sasa_vehiculos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_vehiculos_cases_4_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_4cases_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_vehiculos_cases_4_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_cases_4cases_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_vehiculos',
    'lhs_table' => 'sasa_vehiculos',
    'lhs_key' => 'id',
    'rhs_module' => 'Cases',
    'rhs_table' => 'cases',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_vehiculos_cases_4_c',
    'join_key_lhs' => 'sasa_vehiculos_cases_4sasa_vehiculos_ida',
    'join_key_rhs' => 'sasa_vehiculos_cases_4cases_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_cases_4',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_vehiculos_opportunities_1' => 
  array (
    'name' => 'sasa_vehiculos_opportunities_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_vehiculos_opportunities_1' => 
      array (
        'lhs_module' => 'sasa_vehiculos',
        'lhs_table' => 'sasa_vehiculos',
        'lhs_key' => 'id',
        'rhs_module' => 'Opportunities',
        'rhs_table' => 'opportunities',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_vehiculos_opportunities_1_c',
        'join_key_lhs' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
        'join_key_rhs' => 'sasa_vehiculos_opportunities_1opportunities_idb',
      ),
    ),
    'table' => 'sasa_vehiculos_opportunities_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_vehiculos_opportunities_1sasa_vehiculos_ida' => 
      array (
        'name' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
        'type' => 'id',
      ),
      'sasa_vehiculos_opportunities_1opportunities_idb' => 
      array (
        'name' => 'sasa_vehiculos_opportunities_1opportunities_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_vehiculos_opportunities_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_vehiculos_opportunities_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_vehiculos_opportunities_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_opportunities_1opportunities_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_vehiculos_opportunities_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_vehiculos_opportunities_1opportunities_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_vehiculos',
    'lhs_table' => 'sasa_vehiculos',
    'lhs_key' => 'id',
    'rhs_module' => 'Opportunities',
    'rhs_table' => 'opportunities',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_vehiculos_opportunities_1_c',
    'join_key_lhs' => 'sasa_vehiculos_opportunities_1sasa_vehiculos_ida',
    'join_key_rhs' => 'sasa_vehiculos_opportunities_1opportunities_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_opportunities_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_vehiculos_modified_user' => 
  array (
    'name' => 'sasa_vehiculos_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_vehiculos',
    'rhs_table' => 'sasa_vehiculos',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_vehiculos_created_by' => 
  array (
    'name' => 'sasa_vehiculos_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_vehiculos',
    'rhs_table' => 'sasa_vehiculos',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_vehiculos_activities' => 
  array (
    'name' => 'sasa_vehiculos_activities',
    'lhs_module' => 'sasa_vehiculos',
    'lhs_table' => 'sasa_vehiculos',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_vehiculos',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_vehiculos_following' => 
  array (
    'name' => 'sasa_vehiculos_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_vehiculos',
    'rhs_table' => 'sasa_vehiculos',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_vehiculos',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_vehiculos_favorite' => 
  array (
    'name' => 'sasa_vehiculos_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_vehiculos',
    'rhs_table' => 'sasa_vehiculos',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'sasa_vehiculos',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_vehiculos_assigned_user' => 
  array (
    'name' => 'sasa_vehiculos_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_vehiculos',
    'rhs_table' => 'sasa_vehiculos',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_vehiculos_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
);
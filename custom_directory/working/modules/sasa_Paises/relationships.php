<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'sasa_paises_accounts_1' => 
  array (
    'name' => 'sasa_paises_accounts_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_paises_accounts_1' => 
      array (
        'lhs_module' => 'sasa_Paises',
        'lhs_table' => 'sasa_paises',
        'lhs_key' => 'id',
        'rhs_module' => 'Accounts',
        'rhs_table' => 'accounts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_paises_accounts_1_c',
        'join_key_lhs' => 'sasa_paises_accounts_1sasa_paises_ida',
        'join_key_rhs' => 'sasa_paises_accounts_1accounts_idb',
      ),
    ),
    'table' => 'sasa_paises_accounts_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_paises_accounts_1sasa_paises_ida' => 
      array (
        'name' => 'sasa_paises_accounts_1sasa_paises_ida',
        'type' => 'id',
      ),
      'sasa_paises_accounts_1accounts_idb' => 
      array (
        'name' => 'sasa_paises_accounts_1accounts_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_paises_accounts_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_paises_accounts_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_accounts_1sasa_paises_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_paises_accounts_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_accounts_1accounts_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_paises_accounts_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_paises_accounts_1accounts_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Paises',
    'lhs_table' => 'sasa_paises',
    'lhs_key' => 'id',
    'rhs_module' => 'Accounts',
    'rhs_table' => 'accounts',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_paises_accounts_1_c',
    'join_key_lhs' => 'sasa_paises_accounts_1sasa_paises_ida',
    'join_key_rhs' => 'sasa_paises_accounts_1accounts_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_accounts_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_paises_contacts_1' => 
  array (
    'name' => 'sasa_paises_contacts_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_paises_contacts_1' => 
      array (
        'lhs_module' => 'sasa_Paises',
        'lhs_table' => 'sasa_paises',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_paises_contacts_1_c',
        'join_key_lhs' => 'sasa_paises_contacts_1sasa_paises_ida',
        'join_key_rhs' => 'sasa_paises_contacts_1contacts_idb',
      ),
    ),
    'table' => 'sasa_paises_contacts_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_paises_contacts_1sasa_paises_ida' => 
      array (
        'name' => 'sasa_paises_contacts_1sasa_paises_ida',
        'type' => 'id',
      ),
      'sasa_paises_contacts_1contacts_idb' => 
      array (
        'name' => 'sasa_paises_contacts_1contacts_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_paises_contacts_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_paises_contacts_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_contacts_1sasa_paises_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_paises_contacts_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_contacts_1contacts_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_paises_contacts_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_paises_contacts_1contacts_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Paises',
    'lhs_table' => 'sasa_paises',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_paises_contacts_1_c',
    'join_key_lhs' => 'sasa_paises_contacts_1sasa_paises_ida',
    'join_key_rhs' => 'sasa_paises_contacts_1contacts_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_contacts_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_paises_leads_1' => 
  array (
    'name' => 'sasa_paises_leads_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_paises_leads_1' => 
      array (
        'lhs_module' => 'sasa_Paises',
        'lhs_table' => 'sasa_paises',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_paises_leads_1_c',
        'join_key_lhs' => 'sasa_paises_leads_1sasa_paises_ida',
        'join_key_rhs' => 'sasa_paises_leads_1leads_idb',
      ),
    ),
    'table' => 'sasa_paises_leads_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_paises_leads_1sasa_paises_ida' => 
      array (
        'name' => 'sasa_paises_leads_1sasa_paises_ida',
        'type' => 'id',
      ),
      'sasa_paises_leads_1leads_idb' => 
      array (
        'name' => 'sasa_paises_leads_1leads_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_paises_leads_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_paises_leads_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_leads_1sasa_paises_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_paises_leads_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_leads_1leads_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_paises_leads_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_paises_leads_1leads_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Paises',
    'lhs_table' => 'sasa_paises',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_paises_leads_1_c',
    'join_key_lhs' => 'sasa_paises_leads_1sasa_paises_ida',
    'join_key_rhs' => 'sasa_paises_leads_1leads_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_leads_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_paises_modified_user' => 
  array (
    'name' => 'sasa_paises_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Paises',
    'rhs_table' => 'sasa_paises',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_paises_created_by' => 
  array (
    'name' => 'sasa_paises_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Paises',
    'rhs_table' => 'sasa_paises',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_paises_activities' => 
  array (
    'name' => 'sasa_paises_activities',
    'lhs_module' => 'sasa_Paises',
    'lhs_table' => 'sasa_paises',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_Paises',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'sasa_paises_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_paises_following' => 
  array (
    'name' => 'sasa_paises_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Paises',
    'rhs_table' => 'sasa_paises',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_Paises',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_paises_favorite' => 
  array (
    'name' => 'sasa_paises_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Paises',
    'rhs_table' => 'sasa_paises',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'sasa_Paises',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_paises_assigned_user' => 
  array (
    'name' => 'sasa_paises_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Paises',
    'rhs_table' => 'sasa_paises',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_paises_sasa_departamentos_1' => 
  array (
    'name' => 'sasa_paises_sasa_departamentos_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_paises_sasa_departamentos_1' => 
      array (
        'lhs_module' => 'sasa_Paises',
        'lhs_table' => 'sasa_paises',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_Departamentos',
        'rhs_table' => 'sasa_departamentos',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_paises_sasa_departamentos_1_c',
        'join_key_lhs' => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
        'join_key_rhs' => 'sasa_paises_sasa_departamentos_1sasa_departamentos_idb',
      ),
    ),
    'table' => 'sasa_paises_sasa_departamentos_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_paises_sasa_departamentos_1sasa_paises_ida' => 
      array (
        'name' => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
        'type' => 'id',
      ),
      'sasa_paises_sasa_departamentos_1sasa_departamentos_idb' => 
      array (
        'name' => 'sasa_paises_sasa_departamentos_1sasa_departamentos_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_paises_sasa_departamentos_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_paises_sasa_departamentos_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_paises_sasa_departamentos_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_sasa_departamentos_1sasa_departamentos_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_paises_sasa_departamentos_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_paises_sasa_departamentos_1sasa_departamentos_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Paises',
    'lhs_table' => 'sasa_paises',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_Departamentos',
    'rhs_table' => 'sasa_departamentos',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_paises_sasa_departamentos_1_c',
    'join_key_lhs' => 'sasa_paises_sasa_departamentos_1sasa_paises_ida',
    'join_key_rhs' => 'sasa_paises_sasa_departamentos_1sasa_departamentos_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_sasa_departamentos_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_paises_accounts_2' => 
  array (
    'name' => 'sasa_paises_accounts_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_paises_accounts_2' => 
      array (
        'lhs_module' => 'sasa_Paises',
        'lhs_table' => 'sasa_paises',
        'lhs_key' => 'id',
        'rhs_module' => 'Accounts',
        'rhs_table' => 'accounts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_paises_accounts_2_c',
        'join_key_lhs' => 'sasa_paises_accounts_2sasa_paises_ida',
        'join_key_rhs' => 'sasa_paises_accounts_2accounts_idb',
      ),
    ),
    'table' => 'sasa_paises_accounts_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_paises_accounts_2sasa_paises_ida' => 
      array (
        'name' => 'sasa_paises_accounts_2sasa_paises_ida',
        'type' => 'id',
      ),
      'sasa_paises_accounts_2accounts_idb' => 
      array (
        'name' => 'sasa_paises_accounts_2accounts_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_paises_accounts_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_paises_accounts_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_accounts_2sasa_paises_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_paises_accounts_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_accounts_2accounts_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_paises_accounts_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_paises_accounts_2accounts_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Paises',
    'lhs_table' => 'sasa_paises',
    'lhs_key' => 'id',
    'rhs_module' => 'Accounts',
    'rhs_table' => 'accounts',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_paises_accounts_2_c',
    'join_key_lhs' => 'sasa_paises_accounts_2sasa_paises_ida',
    'join_key_rhs' => 'sasa_paises_accounts_2accounts_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_accounts_2',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_paises_contacts_2' => 
  array (
    'name' => 'sasa_paises_contacts_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_paises_contacts_2' => 
      array (
        'lhs_module' => 'sasa_Paises',
        'lhs_table' => 'sasa_paises',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_paises_contacts_2_c',
        'join_key_lhs' => 'sasa_paises_contacts_2sasa_paises_ida',
        'join_key_rhs' => 'sasa_paises_contacts_2contacts_idb',
      ),
    ),
    'table' => 'sasa_paises_contacts_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_paises_contacts_2sasa_paises_ida' => 
      array (
        'name' => 'sasa_paises_contacts_2sasa_paises_ida',
        'type' => 'id',
      ),
      'sasa_paises_contacts_2contacts_idb' => 
      array (
        'name' => 'sasa_paises_contacts_2contacts_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_paises_contacts_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_paises_contacts_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_contacts_2sasa_paises_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_paises_contacts_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_paises_contacts_2contacts_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_paises_contacts_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_paises_contacts_2contacts_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Paises',
    'lhs_table' => 'sasa_paises',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_paises_contacts_2_c',
    'join_key_lhs' => 'sasa_paises_contacts_2sasa_paises_ida',
    'join_key_rhs' => 'sasa_paises_contacts_2contacts_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_paises_contacts_2',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_paises_leads_2' => 
  array (
    'rhs_label' => 'Leads',
    'lhs_label' => 'Países',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'sasa_Paises',
    'rhs_module' => 'Leads',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'sasa_paises_leads_2',
  ),
);
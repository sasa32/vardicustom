<?php
// created: 2020-05-15 23:47:02
$dictionary["sasa_marcas_sasa_vehiculos_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_marcas_sasa_vehiculos_1' => 
    array (
      'lhs_module' => 'sasa_Marcas',
      'lhs_table' => 'sasa_marcas',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa_vehiculos',
      'rhs_table' => 'sasa_vehiculos',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_marcas_sasa_vehiculos_1_c',
      'join_key_lhs' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
      'join_key_rhs' => 'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb',
    ),
  ),
  'table' => 'sasa_marcas_sasa_vehiculos_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida' => 
    array (
      'name' => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
      'type' => 'id',
    ),
    'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb' => 
    array (
      'name' => 'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_marcas_sasa_vehiculos_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_marcas_sasa_vehiculos_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_marcas_sasa_vehiculos_1sasa_marcas_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_marcas_sasa_vehiculos_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_marcas_sasa_vehiculos_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_marcas_sasa_vehiculos_1sasa_vehiculos_idb',
      ),
    ),
  ),
);
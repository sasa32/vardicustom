<?php
// created: 2020-05-15 16:11:28
$dictionary["sasa_municipios_accounts_3"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_municipios_accounts_3' => 
    array (
      'lhs_module' => 'sasa_Municipios',
      'lhs_table' => 'sasa_municipios',
      'lhs_key' => 'id',
      'rhs_module' => 'Accounts',
      'rhs_table' => 'accounts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_municipios_accounts_3_c',
      'join_key_lhs' => 'sasa_municipios_accounts_3sasa_municipios_ida',
      'join_key_rhs' => 'sasa_municipios_accounts_3accounts_idb',
    ),
  ),
  'table' => 'sasa_municipios_accounts_3_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_municipios_accounts_3sasa_municipios_ida' => 
    array (
      'name' => 'sasa_municipios_accounts_3sasa_municipios_ida',
      'type' => 'id',
    ),
    'sasa_municipios_accounts_3accounts_idb' => 
    array (
      'name' => 'sasa_municipios_accounts_3accounts_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_municipios_accounts_3_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_municipios_accounts_3_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_municipios_accounts_3sasa_municipios_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_municipios_accounts_3_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_municipios_accounts_3accounts_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_municipios_accounts_3_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_municipios_accounts_3accounts_idb',
      ),
    ),
  ),
);
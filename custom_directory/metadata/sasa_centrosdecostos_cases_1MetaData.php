<?php
// created: 2022-10-05 15:14:01
$dictionary["sasa_centrosdecostos_cases_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_centrosdecostos_cases_1' => 
    array (
      'lhs_module' => 'sasa_CentrosDeCostos',
      'lhs_table' => 'sasa_centrosdecostos',
      'lhs_key' => 'id',
      'rhs_module' => 'Cases',
      'rhs_table' => 'cases',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_centrosdecostos_cases_1_c',
      'join_key_lhs' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
      'join_key_rhs' => 'sasa_centrosdecostos_cases_1cases_idb',
    ),
  ),
  'table' => 'sasa_centrosdecostos_cases_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida' => 
    array (
      'name' => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
      'type' => 'id',
    ),
    'sasa_centrosdecostos_cases_1cases_idb' => 
    array (
      'name' => 'sasa_centrosdecostos_cases_1cases_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_centrosdecostos_cases_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_centrosdecostos_cases_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_centrosdecostos_cases_1sasa_centrosdecostos_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_centrosdecostos_cases_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_centrosdecostos_cases_1cases_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_centrosdecostos_cases_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_centrosdecostos_cases_1cases_idb',
      ),
    ),
  ),
);
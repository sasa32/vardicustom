<?php
// created: 2020-07-29 16:42:01
$dictionary["documents_sasa_companias_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'documents_sasa_companias_1' => 
    array (
      'lhs_module' => 'Documents',
      'lhs_table' => 'documents',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa_Companias',
      'rhs_table' => 'sasa_companias',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'documents_sasa_companias_1_c',
      'join_key_lhs' => 'documents_sasa_companias_1documents_ida',
      'join_key_rhs' => 'documents_sasa_companias_1sasa_companias_idb',
    ),
  ),
  'table' => 'documents_sasa_companias_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'documents_sasa_companias_1documents_ida' => 
    array (
      'name' => 'documents_sasa_companias_1documents_ida',
      'type' => 'id',
    ),
    'documents_sasa_companias_1sasa_companias_idb' => 
    array (
      'name' => 'documents_sasa_companias_1sasa_companias_idb',
      'type' => 'id',
    ),
    'document_revision_id' => 
    array (
      'name' => 'document_revision_id',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_documents_sasa_companias_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_documents_sasa_companias_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'documents_sasa_companias_1documents_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_documents_sasa_companias_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'documents_sasa_companias_1sasa_companias_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'documents_sasa_companias_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'documents_sasa_companias_1documents_ida',
        1 => 'documents_sasa_companias_1sasa_companias_idb',
      ),
    ),
  ),
);
<?php
// created: 2020-07-29 23:30:52
$dictionary["sasa_companias_sasa_habeas_data_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_companias_sasa_habeas_data_1' => 
    array (
      'lhs_module' => 'sasa_Companias',
      'lhs_table' => 'sasa_companias',
      'lhs_key' => 'id',
      'rhs_module' => 'SASA_Habeas_Data',
      'rhs_table' => 'sasa_habeas_data',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_companias_sasa_habeas_data_1_c',
      'join_key_lhs' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
      'join_key_rhs' => 'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb',
    ),
  ),
  'table' => 'sasa_companias_sasa_habeas_data_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_companias_sasa_habeas_data_1sasa_companias_ida' => 
    array (
      'name' => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
      'type' => 'id',
    ),
    'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb' => 
    array (
      'name' => 'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_companias_sasa_habeas_data_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_companias_sasa_habeas_data_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_companias_sasa_habeas_data_1sasa_companias_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_companias_sasa_habeas_data_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_companias_sasa_habeas_data_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_companias_sasa_habeas_data_1sasa_habeas_data_idb',
      ),
    ),
  ),
);
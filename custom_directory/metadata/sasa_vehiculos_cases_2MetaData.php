<?php
// created: 2021-08-04 21:22:45
$dictionary["sasa_vehiculos_cases_2"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_vehiculos_cases_2' => 
    array (
      'lhs_module' => 'sasa_vehiculos',
      'lhs_table' => 'sasa_vehiculos',
      'lhs_key' => 'id',
      'rhs_module' => 'Cases',
      'rhs_table' => 'cases',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_vehiculos_cases_2_c',
      'join_key_lhs' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
      'join_key_rhs' => 'sasa_vehiculos_cases_2cases_idb',
    ),
  ),
  'table' => 'sasa_vehiculos_cases_2_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_vehiculos_cases_2sasa_vehiculos_ida' => 
    array (
      'name' => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
      'type' => 'id',
    ),
    'sasa_vehiculos_cases_2cases_idb' => 
    array (
      'name' => 'sasa_vehiculos_cases_2cases_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_vehiculos_cases_2_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_vehiculos_cases_2_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_vehiculos_cases_2sasa_vehiculos_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_vehiculos_cases_2_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_vehiculos_cases_2cases_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_vehiculos_cases_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_vehiculos_cases_2cases_idb',
      ),
    ),
  ),
);
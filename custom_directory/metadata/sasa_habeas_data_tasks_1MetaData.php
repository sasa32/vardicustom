<?php
// created: 2020-11-03 19:25:16
$dictionary["sasa_habeas_data_tasks_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_habeas_data_tasks_1' => 
    array (
      'lhs_module' => 'SASA_Habeas_Data',
      'lhs_table' => 'sasa_habeas_data',
      'lhs_key' => 'id',
      'rhs_module' => 'Tasks',
      'rhs_table' => 'tasks',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_habeas_data_tasks_1_c',
      'join_key_lhs' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
      'join_key_rhs' => 'sasa_habeas_data_tasks_1tasks_idb',
    ),
  ),
  'table' => 'sasa_habeas_data_tasks_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_habeas_data_tasks_1sasa_habeas_data_ida' => 
    array (
      'name' => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
      'type' => 'id',
    ),
    'sasa_habeas_data_tasks_1tasks_idb' => 
    array (
      'name' => 'sasa_habeas_data_tasks_1tasks_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_habeas_data_tasks_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_habeas_data_tasks_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_habeas_data_tasks_1sasa_habeas_data_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_habeas_data_tasks_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_habeas_data_tasks_1tasks_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_habeas_data_tasks_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_habeas_data_tasks_1tasks_idb',
      ),
    ),
  ),
);
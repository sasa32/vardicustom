<?php
// created: 2020-05-21 16:23:40
$dictionary["sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1' => 
    array (
      'lhs_module' => 'sasa_Unidad_de_Negocio',
      'lhs_table' => 'sasa_unidad_de_negocio',
      'lhs_key' => 'id',
      'rhs_module' => 'SASA_UnidadNegClienteProspect',
      'rhs_table' => 'sasa_unidadnegclienteprospect',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c',
      'join_key_lhs' => 'sasa_unida3a6anegocio_ida',
      'join_key_rhs' => 'sasa_unida14dfrospect_idb',
    ),
  ),
  'table' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_unida3a6anegocio_ida' => 
    array (
      'name' => 'sasa_unida3a6anegocio_ida',
      'type' => 'id',
    ),
    'sasa_unida14dfrospect_idb' => 
    array (
      'name' => 'sasa_unida14dfrospect_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_unida3a6anegocio_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_unida14dfrospect_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_unidad_de_negocio_sasa_unidadnegclienteprospect_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_unida14dfrospect_idb',
      ),
    ),
  ),
);
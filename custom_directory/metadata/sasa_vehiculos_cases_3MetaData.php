<?php
// created: 2021-08-04 21:24:18
$dictionary["sasa_vehiculos_cases_3"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_vehiculos_cases_3' => 
    array (
      'lhs_module' => 'sasa_vehiculos',
      'lhs_table' => 'sasa_vehiculos',
      'lhs_key' => 'id',
      'rhs_module' => 'Cases',
      'rhs_table' => 'cases',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_vehiculos_cases_3_c',
      'join_key_lhs' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
      'join_key_rhs' => 'sasa_vehiculos_cases_3cases_idb',
    ),
  ),
  'table' => 'sasa_vehiculos_cases_3_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_vehiculos_cases_3sasa_vehiculos_ida' => 
    array (
      'name' => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
      'type' => 'id',
    ),
    'sasa_vehiculos_cases_3cases_idb' => 
    array (
      'name' => 'sasa_vehiculos_cases_3cases_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_vehiculos_cases_3_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_vehiculos_cases_3_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_vehiculos_cases_3sasa_vehiculos_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_vehiculos_cases_3_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_vehiculos_cases_3cases_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_vehiculos_cases_3_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_vehiculos_cases_3cases_idb',
      ),
    ),
  ),
);
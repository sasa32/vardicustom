<?php
// created: 2020-05-15 23:44:20
$dictionary["sasa_vehiculos_leads_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_vehiculos_leads_1' => 
    array (
      'lhs_module' => 'sasa_vehiculos',
      'lhs_table' => 'sasa_vehiculos',
      'lhs_key' => 'id',
      'rhs_module' => 'Leads',
      'rhs_table' => 'leads',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_vehiculos_leads_1_c',
      'join_key_lhs' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
      'join_key_rhs' => 'sasa_vehiculos_leads_1leads_idb',
    ),
  ),
  'table' => 'sasa_vehiculos_leads_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_vehiculos_leads_1sasa_vehiculos_ida' => 
    array (
      'name' => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
      'type' => 'id',
    ),
    'sasa_vehiculos_leads_1leads_idb' => 
    array (
      'name' => 'sasa_vehiculos_leads_1leads_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_vehiculos_leads_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_vehiculos_leads_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_vehiculos_leads_1sasa_vehiculos_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_vehiculos_leads_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_vehiculos_leads_1leads_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_vehiculos_leads_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_vehiculos_leads_1leads_idb',
      ),
    ),
  ),
);